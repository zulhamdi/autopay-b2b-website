﻿using OMGLiveDemo.Models;
using OMGLiveDemo.Models.OMG;
using OMGLiveDemo.Scripts;
using Oppal.Sec;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.Setting
{
    public partial class UpdatePassword : System.Web.UI.Page
    {
        OMG omg = new OMG();
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    //bind();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if (txtOldPassword.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Old password cannot be empty!";
                return;
            }

            if (txtNewPassword.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "New Password cannot be empty!";
                return;
            }

            if (txtReNewPassword.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Retype new password cannot be empty!";
                return;
            }

            if (txtReNewPassword.Text != txtNewPassword.Text)
            {
                diverror.Visible = true;
                lblError.Text = "New password and retype new password not match!";
                return;
            }

            if (txtNewPassword.Text.Length < 8)
            {
                diverror.Visible = true;
                lblError.Text = "Password should be more than 8 characters!";
                return;
            }

            var id = Int64.Parse(SessionLib.Current.AdminID);
            if (SessionLib.Current.Role == Roles.Agent)
            {
                var agent = db.tblM_Agent.Where(c => c.AgentID == id).FirstOrDefault();
                if (agent != null)
                {
                    agent.Password = Crypto.EncryptDB(txtNewPassword.Text.Trim());
                    db.SaveChanges();
                    diverror.Visible = true;
                    lblError.Text = "Success update password";
                    //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Success update password')", true);
                    return;
                }
                else
                {
                    diverror.Visible = true;
                    lblError.Text = "Failed update password";
                    //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Failed update password')", true);
                }
            }
            else if (SessionLib.Current.Role == Roles.MasterArea)
            {
                var master = db.tblM_Master_Area.Where(c => c.MasterAreaID == id).FirstOrDefault();
                if (master != null)
                {
                    master.Password = Crypto.EncryptDB(txtNewPassword.Text.Trim());
                    db.SaveChanges();
                    diverror.Visible = true;
                    lblError.Text = "Success update password";
                    //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Success update password')", true);
                    return;
                }
                else
                {
                    diverror.Visible = true;
                    lblError.Text = "Failed update password";
                    //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Failed update password')", true);
                }
            }
            else
            {
                var admin = db.tblM_Admin.Where(c => c.AdminID == id).FirstOrDefault();
                if (admin != null)
                {
                    admin.Password = Crypto.EncryptDB(txtNewPassword.Text.Trim());
                    db.SaveChanges();
                    diverror.Visible = true;
                    lblError.Text = "Success update password";
                    //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Success update password')", true);
                    return;
                }
                else
                {
                    diverror.Visible = true;
                    lblError.Text = "Failed update password";
                    //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Failed update password')", true);
                }
            }
        }

        protected void btnError_ServerClick(object sender, EventArgs e)
        {
            diverror.Visible = false;
        }

        protected void ddlRole_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}