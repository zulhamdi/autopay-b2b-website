﻿using OMGLiveDemo.Models;
using OMGLiveDemo.Models.OMG;
using OMGLiveDemo.Scripts;
using Oppal.Sec;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.Setting
{
    public partial class UpdateAdmin : System.Web.UI.Page
    {
        OMG omg = new OMG();
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    bind();
                    populate();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        void bind()
        {
            var role = db.tblM_Admin_Role.Where(p => p.Role != "Super Admin").ToList();
            ddlRole.DataSource = null;
            ddlRole.DataSource = role;
            ddlRole.DataValueField = "RoleID";
            ddlRole.DataTextField = "Role";
            ddlRole.DataBind();
        }

        void populate()
        {
            var userID = long.Parse(Session["ModifyUserID"].ToString());

            var user = db.tblM_Admin.SingleOrDefault(p => p.AdminID == userID);
            if(user != null)
            {
                txtName.Text = user.Fullname;
                txtEmail.Text = user.Email;
                txtPhone.Text = user.Phone;
                ddlRole.SelectedValue = user.RoleID.ToString();

                if(user.isActive == 1)
                {
                    cbEnable.Checked = true;
                    //cbTest.Checked = true;
                }
                else
                {
                    cbEnable.Checked = false;
                    //cbTest.Checked = false;
                }
            }
            else
            {
                //can not found the user
                Session["ModifyUserID"] = "";
                Response.Redirect("~/Setting/Admin");
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            var userID = long.Parse(Session["ModifyUserID"].ToString());
            var targetUser = db.tblM_Admin.SingleOrDefault(p => p.AdminID == userID);
            String eventData = "Data update on user [" + userID + "]" + targetUser.Fullname +" by [" + SessionLib.Current.AdminID + "]" + SessionLib.Current.Name + ".";

            if (targetUser != null)
            {
                if(cbEnable.Checked)
                {
                    if (txtName.Text == "")
                    {
                        diverror.Visible = true;
                        lblError.Text = "Name cannot be empty!";
                        return;
                    }

                    if (cbChangePassword.Checked)
                    {
                        if (txtPassword.Text == "")
                        {
                            diverror.Visible = true;
                            lblError.Text = "You've select to change the password. Password cannot be empty!";
                            return;
                        }
                    }

                    if (txtPhone.Text == "")
                    {
                        diverror.Visible = true;
                        lblError.Text = "Phone cannot be empty!";
                        return;
                    }

                    if(targetUser.isActive == 0)
                    {
                        targetUser.isActive = 1;
                        eventData += " Activating user " + targetUser.AdminID + " from disabled state.";
                    }

                    targetUser.Fullname = txtName.Text.Trim();
                    targetUser.Phone = txtPhone.Text.Trim();
                    targetUser.Email = txtEmail.Text.Trim();
                    targetUser.RoleID = Convert.ToInt64(ddlRole.SelectedValue);

                    if (cbChangePassword.Checked)
                    {
                        targetUser.Password = Crypto.EncryptDB(txtPassword.Text.Trim());
                        eventData += " Changing user " + targetUser.AdminID + " password.";
                    }
                }
                else
                {
                    // disable the user (set isActive = 0)
                    targetUser.isActive = 0;
                    eventData = "Deactivating user [" + userID + "]" + targetUser.Fullname + " by [" + SessionLib.Current.AdminID + "]" + SessionLib.Current.Name + ".";
                }

                targetUser.UpdatedBy = Convert.ToInt64(SessionLib.Current.AdminID);
                var eDate = DateTime.Now;
                targetUser.UpdatedDate = eDate;

                db.SaveChanges();

                if (cbChangePassword.Checked)
                {
                    omg.sendEmailPasswordChanged(targetUser, txtPassword.Text.Trim());
                }

                tblS_Log eLog = new tblS_Log {
                    EventDate = eDate,
                    EventBy = Convert.ToInt64(SessionLib.Current.AdminID),
                    EventType = "administration",
                    EventData = eventData,
                };

                db.tblS_Log.Add(eLog);
                db.SaveChanges();

                Session["SuccessSender"] = "Update user " + targetUser.Fullname + " change made by " + SessionLib.Current.Name;

                Response.Redirect("~/Setting/Admin");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + targetUser.Fullname + " updated')", true);
            }
            else
            {
                diverror.Visible = true;
                lblError.Text = "Unable to lock on the target user for update. User information can not be updated";
            }
        }

        protected void btnError_ServerClick(object sender, EventArgs e)
        {
            diverror.Visible = false;
        }

        protected void ddlRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            //TO-DO
        }
    }
}