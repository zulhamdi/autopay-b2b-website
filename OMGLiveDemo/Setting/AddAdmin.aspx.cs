﻿using OMGLiveDemo.Models;
using OMGLiveDemo.Models.OMG;
using OMGLiveDemo.Scripts;
using Oppal.Sec;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.Setting
{
    public partial class AddAdmin : System.Web.UI.Page
    {
        OMG omg = new OMG();
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    bind();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        void bind()
        {
            var role = db.tblM_Admin_Role.Where(p => p.Role != "Super Admin").ToList();
            ddlRole.DataSource = null;
            ddlRole.DataSource = role;
            ddlRole.DataValueField = "RoleID";
            ddlRole.DataTextField = "Role";
            ddlRole.DataBind();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            if (txtName.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Name cannot be empty!";
                return;
            }

            if (txtPassword.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Password cannot be empty!";
                return;
            }

            if (txtPhone.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Phone cannot be empty!";
                return;
            }

            var check = db.tblM_Admin.Where(p => p.Phone == txtPhone.Text.Trim()).FirstOrDefault();
            if (check == null)
            {

                tblM_Admin bnf = new tblM_Admin();
                bnf.Fullname = txtName.Text.Trim();
                bnf.Password = Crypto.EncryptDB(txtPassword.Text.Trim());
                bnf.Phone = txtPhone.Text.Trim();
                bnf.Email = txtEmail.Text.Trim();
                bnf.JoinDate = DateTime.Now;
                bnf.AddedBy = Convert.ToInt64(SessionLib.Current.AdminID);
                bnf.Email = txtEmail.Text.Trim();
                bnf.RoleID = Convert.ToInt64(ddlRole.SelectedValue);
                bnf.isActive = 1;
                db.tblM_Admin.Add(bnf);
                db.SaveChanges();

                String eventData = "Create user [" + bnf.AdminID + "]" + bnf.Fullname + " by [" + SessionLib.Current.AdminID + "]" + SessionLib.Current.Name + ".";

                tblS_Log eLog = new tblS_Log
                {
                    EventDate = bnf.JoinDate.Value,
                    EventBy = Convert.ToInt64(SessionLib.Current.AdminID),
                    EventType = "administration",
                    EventData = eventData,
                };

                db.tblS_Log.Add(eLog);
                db.SaveChanges();

                //send email
                omg.sendEmailUserAccountCreated(bnf, txtPassword.Text.Trim());

                Session["SuccessSender"] = "Success add " + bnf.Fullname + " by " + SessionLib.Current.Name;

                Response.Redirect("~/Setting/Admin");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + bnf.Fullname + " added to database')", true);
            }
            else
            {
                diverror.Visible = true;
                lblError.Text = "This phone number already registered";
            }
        }

        protected void btnError_ServerClick(object sender, EventArgs e)
        {
            diverror.Visible = false;
        }

        protected void ddlRole_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}