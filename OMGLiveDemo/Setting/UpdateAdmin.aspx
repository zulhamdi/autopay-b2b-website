﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UpdateAdmin.aspx.cs" Inherits="OMGLiveDemo.Setting.UpdateAdmin" MaintainScrollPositionOnPostback="true" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" />

    <div class="row">
        <div class="row x_title">
            <div class="col-md-12">
                <h3>Add Admin</h3>
            </div>
        </div>

        <div class="col-md-3 col-sm-12 col-xs-12"></div>
        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="dashboard_graph">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div style="width: 100%;">
                        <div id="canvas_dahs" class="demo-placeholder" style="width: 100%; height: 100%;">
                            <div class="form-group" style="height: 30px"></div>

                            <div class="alert alert-danger alert-dismissible fade in" runat="server" id="diverror" visible="false">
                                <button type="button" runat="server" id="btnError" onserverclick="btnError_ServerClick" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <div class="centered"><asp:Label runat="server" ID="lblError" /></div>
                            </div>

                            <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" id="cbEnable" runat="server" data-toggle="toggle" data-on="User Enabled" data-off="User disabled" data-onstyle="success" data-offstyle="danger">
                                    </label>
                                </div>
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblName" CssClass="control-label" Text="Full Name *" />
                                <asp:TextBox runat="server" ID="txtName" CssClass="form-control" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:CheckBox ID="cbChangePassword" runat="server" Text="Change Password" />
                                <asp:TextBox runat="server" ID="txtPassword" CssClass="form-control" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblPhone" CssClass="control-label" Text="Phone *" />
                                <asp:TextBox runat="server" ID="txtPhone" CssClass="form-control" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblEmail" CssClass="control-label" Text="Email " />
                                <asp:TextBox runat="server" ID="txtEmail" CssClass="form-control" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblRole" CssClass="control-label" Text="Role *" />
                                <asp:DropDownList runat="server" ID="ddlRole" CssClass="form-control" OnSelectedIndexChanged="ddlRole_SelectedIndexChanged" AutoPostBack="true" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <div class="centered"><asp:Button runat="server" ID="btnUpdate" Text="Update User" CssClass="btn btn-success" OnClick="btnUpdate_Click" Width="100%" /></div>
                            </div>

                            <div class="form-group" style="height: 30px"></div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12"></div>
    </div>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Scripts" runat="server">
</asp:Content>
