﻿using OMGLiveDemo.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.Setting
{
    public partial class BankAccount : System.Web.UI.Page
    {
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    BindGV();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        void BindGV()
        {
            if (Session["SuccessBankAccount"] != null)
            {
                lblAlertSuccess.Text = Session["SuccessBankAccount"].ToString();
                divsuccess.Visible = true;
                Session["SuccessBankAccount"] = null;
            }

            if (Session["ErrorBankAccount"] != null)
            {
                lblAlertFailed.Text = Session["ErrorBankAccount"].ToString();
                divfailed.Visible = true;
                Session["ErrorBankAccount"] = null;
            }

            var dt = db.vw_Bank_Account.ToList();
            for (int i = 0; i < dt.Count; i++)
            {
                if (dt[i].CountryID == 133)
                {
                    dt[i].Country = "Malaysia";
                }
                else
                {
                    dt[i].Country = "Indonesia";
                }
                    

                var adid = dt[i].CreatedBy;
                dt[i].CreatedName = db.tblM_Admin.Where(p => p.AdminID == adid).Select(p => p.Fullname).FirstOrDefault();
            }
            gvListItem.DataSource = dt;
            gvListItem.DataBind();
        }

        protected void btnAddAdmin_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddAdmin");
        }

        protected void btnAddBankAccount_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddBankAccount");
        }

        protected void btnAlertFailed_ServerClick(object sender, EventArgs e)
        {
            divfailed.Visible = false;
        }

        protected void btnAlertSuccess_ServerClick(object sender, EventArgs e)
        {
            divsuccess.Visible = false;
        }
    }
}