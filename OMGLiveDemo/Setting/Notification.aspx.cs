﻿using OMGLiveDemo.Models;
using OMGLiveDemo.Scripts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.Setting
{
    public partial class Notification : System.Web.UI.Page
    {
        OMG omg = new OMG();
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (rolesAllowed())
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    check();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        void check()
        {
            var data = omg.getProfile();
            if (data != null)
            {
                if (data.CallbackURL == null)
                    lblURL.Text = "* no url available *";
                else
                {
                    lblURL.Text = data.CallbackURL;
                    txtURL.Text = data.CallbackURL;
                }
            }
            else
            {
                lblURL.Text = "* error *";
            }

            lblURL.Visible = true;
            txtURL.Visible = false;
            btnEdit.Visible = true;
            btnUpdate.Visible = false;
        }

        protected void btnError_ServerClick(object sender, EventArgs e)
        {
            clearAlert();
        }

        protected void btnSuccess_ServerClick(object sender, EventArgs e)
        {
            clearAlert();
        }

        void clearAlert() {
            divsuccess.Visible = false;
            diverror.Visible = false;
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            if (txtURL.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "URL cannot be empty!";
                return;
            }

            clearAlert();

            if (omg.updateProfileCallbackURL(txtURL.Text))
            {
                divsuccess.Visible = true;
                lblSuccess.Text = "Success update notification endpoint URL";
                check();
            }
            else
            {
                diverror.Visible = true;
                lblError.Text = "Failed update notification endpoint URL";
            }
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            lblURL.Visible = false;
            txtURL.Visible = true;
            btnEdit.Visible = false;
            btnUpdate.Visible = true;
        }
    }
}