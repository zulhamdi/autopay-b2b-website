﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Admin.aspx.cs" Inherits="OMGLiveDemo.Setting.Admin" MaintainScrollPositionOnPostback="true" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" />

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph">

                <div class="row x_title">
                    <div class="col-md-12">
                        <h3>User Administration</h3>
                    </div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div style="width: 100%;">
                        <div id="canvas_dahs" class="demo-placeholder" style="width: 100%; height: 100%;">
                             <div class="alert alert-success alert-dismissible fade in" runat="server" id="divsuccess" visible="false">
                                <button type="button" runat="server" id="btnAlertSuccess" onserverclick="btnAlertSuccess_ServerClick" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <center><asp:Label runat="server" ID="lblAlertSuccess" Font-Size="14pt" Text="test error" /></center>
                            </div>

                            <div class="alert alert-danger alert-dismissible fade in" runat="server" id="divfailed" visible="false">
                                <button type="button" runat="server" id="btnAlertFailed" onserverclick="btnAlertFailed_ServerClick" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <center><asp:Label runat="server" ID="lblAlertFailed" Font-Size="14pt" Text="test error" /></center>
                            </div>

                            <asp:GridView CssClass="table table-striped jambo_table bulk_action" ID="gvListItem" runat="server" AutoGenerateColumns="false" DataKeyNames="AdminID" onRowDataBound="gvListItem_RowDataBound">
                                <Columns>
                                    <asp:TemplateField HeaderText="No.">
                                        <ItemTemplate>
                                            <asp:HiddenField runat="server" ID="hfAdminID" Value='<%# Eval("AdminID") %>' />
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="AdminID" HeaderText="User ID" />
                                    <asp:BoundField DataField="Fullname" HeaderText="Full Name" />
                                    <asp:BoundField DataField="Phone" HeaderText="Phone" />
                                    <asp:BoundField DataField="Role" HeaderText="Role" />
                                    <asp:BoundField DataField="JoinDate" HeaderText="Join Date" />
                                    <asp:BoundField DataField="isActive" HeaderText="Status" />
                                    <asp:TemplateField HeaderText="Action">
                                        <ItemTemplate>
                                            <asp:Button ID="btnModifyUser" CssClass="form-control" runat="server" Text="Modify" OnClick="btnModifyUser_Click" CausesValidation="false" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <asp:Button runat="server" ID="btnAddAdmin" OnClick="btnAddAdmin_Click" Text="Add User" CssClass="btn btn-success" />
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Scripts" runat="server">
</asp:Content>
