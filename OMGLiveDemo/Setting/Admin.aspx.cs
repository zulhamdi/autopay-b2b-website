﻿using OMGLiveDemo.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.Setting
{
    public partial class Admin : System.Web.UI.Page
    {
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    BindGV();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        void BindGV()
        {
            if (Session["SuccessSender"] != null)
            {
                lblAlertSuccess.Text = Session["SuccessSender"].ToString();
                divsuccess.Visible = true;
                Session["SuccessSender"] = null;
            }

            if (Session["ErrorSender"] != null)
            {
                lblAlertFailed.Text = Session["ErrorSender"].ToString();
                divfailed.Visible = true;
                Session["ErrorSender"] = null;
            }

            var dt = db.vw_Admin_Role.Where(p => p.AdminID != 1).ToList();
            gvListItem.DataSource = dt;
            gvListItem.DataBind();
        }

        protected void btnAddAdmin_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddAdmin");
        }

        protected void btnModifyUser_Click(object sender, EventArgs e)
        {
            System.Web.UI.WebControls.Button btn = (System.Web.UI.WebControls.Button)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            Session["ModifyUserID"] = ((HiddenField)row.FindControl("hfAdminID")).Value;
            Response.Redirect("UpdateAdmin");
        }

        protected void gvListItem_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[6].Text == "1")
                {
                    e.Row.Cells[6].Text = "Enabled";
                }
                else
                {
                    e.Row.Cells[6].Text = "Disabled";
                }
            }
        }

        protected void btnAlertSuccess_ServerClick(object sender, EventArgs e)
        {
            divsuccess.Visible = false;
        }

        protected void btnAlertFailed_ServerClick(object sender, EventArgs e)
        {
            divfailed.Visible = false;
        }
    }
}