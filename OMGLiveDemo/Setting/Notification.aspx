﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Notification.aspx.cs" Inherits="OMGLiveDemo.Setting.Notification" MaintainScrollPositionOnPostback="true" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" />

    <div class="row">
        <div class="row x_title">
            <div class="col-md-12">
                <h3>URL Notification</h3>
            </div>
        </div>

        <div class="col-md-3 col-sm-12 col-xs-12"></div>
        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="dashboard_graph">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div style="width: 100%;">
                        <div id="canvas_dahs" class="demo-placeholder" style="width: 100%; height: 100%;">
                            <div class="form-group" style="height: 30px"></div>

                            <div class="alert alert-danger alert-dismissible fade in" runat="server" id="diverror" visible="false">
                                <button type="button" runat="server" id="btnError" onserverclick="btnError_ServerClick" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <center><asp:Label runat="server" ID="lblError" /></center>
                            </div>

                            <div class="alert alert-success alert-dismissible fade in" runat="server" id="divsuccess" visible="false">
                                <button type="button" runat="server" id="btnSuccess" onserverclick="btnSuccess_ServerClick" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <center><asp:Label runat="server" ID="lblSuccess" /></center>
                            </div>

                            <div class="form-group">
                                <asp:Label runat="server" CssClass="control-label" Text="Notification endpoint URL * :" />
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" ID="lblURL" CssClass="control-label" Font-Size="Large" />
                                <asp:TextBox runat="server" ID="txtURL" CssClass="form-control" Visible="false" />
                            </div>
                            <div class="form-group" style="height:5px"></div>

                            <hr />
                            
                            <div class="form-group" style="height:5px"></div>
                            <div class="form-group">
                                <center><asp:Button runat="server" ID="btnUpdate" Visible="false" Text="Update URL" CssClass="btn btn-success" OnClick="btnUpdate_Click" /></center>
                                <center><asp:Button runat="server" ID="btnEdit" Text="Edit URL" CssClass="btn btn-warning" OnClick="btnEdit_Click" /></center>
                                <%--<asp:Button runat="server" ID="btnThumbnail" Text="Buat Thumbnail" CssClass="btn btn-success" OnClick="btnThumbnail_Click" />--%>
                            </div>

                            <div class="form-group" style="height: 30px"></div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12"></div>
    </div>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Scripts" runat="server">
</asp:Content>