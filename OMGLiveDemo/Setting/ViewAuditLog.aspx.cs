﻿using OMGLiveDemo.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.Setting
{
    public partial class ViewAuditLog : System.Web.UI.Page
    {
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    bindUserID();
                    BindGV();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        void BindGV()
        {
            if(ddlFilterByDate.SelectedValue == "date")
            {
                doSingleDateDisplay();
            }
            else if (ddlFilterByDate.SelectedValue == "daterange")
            {
                doDateRangeDisplay();
            }
            else
            {
                List<tblS_Log> logs = new List<tblS_Log>();

                if (ddlEventType.SelectedValue == "All")
                {
                    if(ddlUserID.SelectedValue == "All")
                    {
                        logs = db.tblS_Log.OrderByDescending(o => o.EventDate).ToList();
                    }
                    else
                    {
                        long uid = Convert.ToInt64(ddlUserID.SelectedValue);
                        logs = db.tblS_Log.Where(p => p.EventBy == uid).OrderByDescending(o => o.EventDate).ToList();
                    }
                }
                else
                {
                    if (ddlUserID.SelectedValue == "All")
                    {
                        logs = db.tblS_Log.Where(p => p.EventType == ddlEventType.SelectedValue).OrderByDescending(o => o.EventDate).ToList();
                    }
                    else
                    {
                        long uid = Convert.ToInt64(ddlUserID.SelectedValue);
                        logs = db.tblS_Log.Where(p => p.EventBy == uid && p.EventType == ddlEventType.SelectedValue).OrderByDescending(o => o.EventDate).ToList();
                    }
                }

                gvListItem.DataSource = null;
                gvListItem.DataSource = logs;
                gvListItem.DataBind();
            }
        }

        void bindUserID()
        {
            var userlist = db.tblM_Admin.ToList();

            ddlUserID.DataSource = userlist;
            ddlUserID.DataValueField = "AdminID";
            ddlUserID.DataTextField = "Fullname";
            ddlUserID.DataBind();
        }

        protected void gvListItem_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvListItem.PageIndex = e.NewPageIndex;
            BindGV();
        }

        protected void ddlEventType_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGV();
        }

        protected void ddlUserID_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGV();
        }

        protected void btnSingleDate_Click(object sender, EventArgs e)
        {
            doSingleDateDisplay();
        }

        void doSingleDateDisplay()
        {
            List<tblS_Log> logs = new List<tblS_Log>();
            DateTime dS = new DateTime();
            DateTime dE = new DateTime();

            if (searchdate.Text.Trim() != "")
            {
                dS = Convert.ToDateTime(searchdate.Text.ToString().Trim());
                dE = dS.AddHours(23).AddMinutes(59).AddSeconds(59);
            }
            else
            {
                return;
            }

            if(ddlEventType.SelectedValue == "All")
            {
                if(ddlUserID.SelectedValue == "All")
                {
                    logs = db.tblS_Log.Where(p => p.EventDate >= dS && p.EventDate <= dE).OrderByDescending(o => o.EventDate).ToList();
                }
                else
                {
                    long uid = Convert.ToInt64(ddlUserID.SelectedValue);
                    logs = db.tblS_Log.Where(p => p.EventBy == uid && p.EventDate >= dS && p.EventDate <= dE).OrderByDescending(o => o.EventDate).ToList();
                }

            }
            else
            {
                if (ddlUserID.SelectedValue == "All")
                {

                    logs = db.tblS_Log.Where(p => p.EventType == ddlEventType.SelectedValue && p.EventDate >= dS && p.EventDate <= dE).OrderByDescending(o => o.EventDate).ToList();
                }
                else
                {
                    long uid = Convert.ToInt64(ddlUserID.SelectedValue);
                    logs = db.tblS_Log.Where(p => p.EventType == ddlEventType.SelectedValue && p.EventBy == uid && p.EventDate >= dS && p.EventDate <= dE).OrderByDescending(o => o.EventDate).ToList();
                }
            }

            gvListItem.DataSource = null;
            gvListItem.DataSource = logs;
            gvListItem.DataBind();
        }

        protected void btnDateRange_Click(object sender, EventArgs e)
        {
            doDateRangeDisplay();
        }

        void doDateRangeDisplay()
        {
            List<tblS_Log> logs = new List<tblS_Log>();

            DateTime dS = new DateTime();
            DateTime dE = new DateTime();

            if (startdate.Text.Trim() != "" && enddate.Text.Trim() != "")
            {
                dS = Convert.ToDateTime(startdate.Text.ToString().Trim());
                dE = Convert.ToDateTime(enddate.Text.ToString().Trim()).AddHours(23).AddMinutes(59).AddSeconds(59);
            }
            else
            {
                return;
            }

            if (ddlEventType.SelectedValue == "All")
            {
                if (ddlUserID.SelectedValue == "All")
                {
                    logs = db.tblS_Log.Where(p => p.EventDate >= dS && p.EventDate <= dE).OrderByDescending(o => o.EventDate).ToList();
                }
                else
                {
                    long uid = Convert.ToInt64(ddlUserID.SelectedValue);
                    logs = db.tblS_Log.Where(p => p.EventBy == uid && p.EventDate >= dS && p.EventDate <= dE).OrderByDescending(o => o.EventDate).ToList();
                }

            }
            else
            {
                if (ddlUserID.SelectedValue == "All")
                {

                    logs = db.tblS_Log.Where(p => p.EventType == ddlEventType.SelectedValue && p.EventDate >= dS && p.EventDate <= dE).OrderByDescending(o => o.EventDate).ToList();
                }
                else
                {
                    long uid = Convert.ToInt64(ddlUserID.SelectedValue);
                    logs = db.tblS_Log.Where(p => p.EventType == ddlEventType.SelectedValue && p.EventBy == uid && p.EventDate >= dS && p.EventDate <= dE).OrderByDescending(o => o.EventDate).ToList();
                }
            }

            gvListItem.DataSource = null;
            gvListItem.DataSource = logs;
            gvListItem.DataBind();
        }

        protected void ddlFilterByDate_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFilterByDate.SelectedValue == "date")
            {
                divsingledate.Visible = true;
                divdaterange.Visible = false;
                startdate.Text = "";
                enddate.Text = "";
            }
            else if (ddlFilterByDate.SelectedValue == "daterange")
            {
                divsingledate.Visible = false;
                divdaterange.Visible = true;
                searchdate.Text = "";
            }
            else
            {
                divsingledate.Visible = false;
                divdaterange.Visible = false;
                startdate.Text = "";
                enddate.Text = "";
                searchdate.Text = "";
            }
        }
    }
}