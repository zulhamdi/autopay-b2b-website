﻿using OMGLiveDemo.Models;
using OMGLiveDemo.Models.OMG;
using OMGLiveDemo.Scripts;
using Oppal.Sec;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.Setting
{
    public partial class AddBankAccount : System.Web.UI.Page
    {
        OMG omg = new OMG();
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    bind();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        void bind()
        {
            getBankList();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            if (txtAccountHolder.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Account holder cannot be empty!";
                return;
            }

            if (txtAccountNo.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Account number cannot be empty!";
                return;
            }

            if (ddlBank.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Bank cannot be empty!";
                return;
            }
            tblM_Bank_Account bank = new tblM_Bank_Account();
            bank.CountryID = Convert.ToInt64(ddlCountry.SelectedValue);
            bank.BankID = Convert.ToInt64(ddlBank.SelectedValue);
            bank.Account = txtAccountNo.Text.Trim();
            bank.AccountHolder = txtAccountHolder.Text.Trim().ToUpper();
            bank.CreatedDate = DateTime.Now;
            bank.CreatedBy = Convert.ToInt64(SessionLib.Current.AdminID);
            bank.isActive = 1;
            db.tblM_Bank_Account.Add(bank);
            db.SaveChanges();

            Session["SuccessBankAccount"] = "Success add bank account (" + bank.AccountHolder + " - " + bank.Account + " - "+ ddlBank.SelectedItem +") to database";
            Response.Redirect("~/Setting/BankAccount");
        }

        protected void btnError_ServerClick(object sender, EventArgs e)
        {
            diverror.Visible = false;
        }

        protected void ddlRole_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void ddlBank_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        void getBankList()
        {
            var cid = Convert.ToInt64(ddlCountry.SelectedValue);
            var bank = db.tblM_Bank.Where(p => p.CountryID == cid && p.isActive == 1).ToList();
            ddlBank.DataSource = null;
            ddlBank.DataSource = bank;
            ddlBank.DataValueField = "BankID";
            ddlBank.DataTextField = "Name";
            ddlBank.DataBind();
        }

        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            getBankList();
        }
    }
}