﻿using OMGLiveDemo.Models;
using OMGLiveDemo.Scripts;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo
{
    public partial class RateAgent : System.Web.UI.Page
    {
        OMG omg = new OMG();
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();
        tblM_Rate rate = new tblM_Rate();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    getRemittanceLine();
                    //countMYR();
                    //countIDR();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        void getRemittanceLine() {
            long id = Convert.ToInt64(SessionLib.Current.AdminID);
            if (SessionLib.Current.Role == Roles.Agent)
            {
                var data = db.tblM_Agent_Rate.Where(p => p.AgentID == id).ToList();
                for (int i = 0; i < data.Count; i++)
                {
                    if (data[i].RemittanceID == 1)
                    {
                        ddlChooseRemittance.Items.Insert(i, new ListItem("Malaysia to Indonesia", data[i].RemittanceID.ToString()));
                        countMYR();
                    }
                    else if (data[i].RemittanceID == 2)
                    {
                        ddlChooseRemittance.Items.Insert(i, new ListItem("Indonesia to Malaysia", data[i].RemittanceID.ToString()));
                        countIDR();
                    }
                }
            }
            else if (SessionLib.Current.Role == Roles.MasterArea)
            {
                var data = db.tblM_Master_Area_Rate.Where(p => p.MasterAreaID == id).ToList();
                for (int i = 0; i < data.Count; i++)
                {
                    if (data[i].RemittanceID == 1)
                    {
                        ddlChooseRemittance.Items.Insert(i, new ListItem("Malaysia to Indonesia", data[i].RemittanceID.ToString()));
                        countMYR();
                    }
                    else if (data[i].RemittanceID == 2)
                    {
                        ddlChooseRemittance.Items.Insert(i, new ListItem("Indonesia to Malaysia", data[i].RemittanceID.ToString()));
                        countIDR();
                    }
                }
            }
            checkRemttanceLine();
        }

        void countMYR()
        {
            var cur = omg.getRateMYR2IDR();
            if (cur != null)
            {
                hfRateMYR.Value = cur.Rate;
            }

            var id = Int64.Parse(SessionLib.Current.AdminID);

            if (SessionLib.Current.Role == Roles.Agent)
            {
                var rateagent = db.tblM_Agent_Rate.Where(p => p.AgentID == id && p.RemittanceID == 1).FirstOrDefault();
                rate = db.tblM_Rate.Where(p => p.RateID == rateagent.RateID).FirstOrDefault();
                hfRateMYR.Value = (Double.Parse(hfRateMYR.Value) - Double.Parse(rate.Margin)).ToString();
                if (rateagent != null)
                {
                    lblCurrencyMYR.Text = rate.Currency;
                    txtMarginMYR.Text = rateagent.Margin;

                    lblBaseRateMYR.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(hfRateMYR.Value)).Replace("Rp", "Rp ");
                    lblRateMYR.Text = "1 MYR = " + String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(hfRateMYR.Value)).Replace("Rp", "") + " IDR";
                    lblRateUpdateMYR.Text = "Last update : " + cur.DateModified.ToLongTimeString() + ", " + cur.DateModified.ToLongDateString();
                }
            }
            else if (SessionLib.Current.Role == Roles.MasterArea)
            {
                var rateagent = db.tblM_Master_Area_Rate.Where(p => p.MasterAreaID == id && p.RemittanceID == 1).FirstOrDefault();
                rate = db.tblM_Rate.Where(p => p.RateID == rateagent.RateID).FirstOrDefault();
                hfRateMYR.Value = (Double.Parse(hfRateMYR.Value) - Double.Parse(rate.Margin)).ToString();
                if (rateagent != null)
                {
                    lblCurrencyMYR.Text = rate.Currency;
                    txtMarginMYR.Text = rateagent.Margin;

                    lblBaseRateMYR.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(hfRateMYR.Value)).Replace("Rp", "Rp ");
                    lblRateMYR.Text = "1 MYR = " + String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(hfRateMYR.Value)).Replace("Rp", "") + " IDR";
                    lblRateUpdateMYR.Text = "Last update : " + cur.DateModified.ToLongTimeString() + ", " + cur.DateModified.ToLongDateString();
                }
            }
            lblCountryMYR.Text = "Malaysia to Indonesia";
            countRateMYR();
        }

        void countRateMYR()
        {
            lblRetailRateMYR.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64((Int32.Parse(hfRateMYR.Value) - Int32.Parse(txtMarginMYR.Text)))).Replace("Rp", "Rp ");
        }

        protected void txtMarginMYR_TextChanged(object sender, EventArgs e)
        {
            if (txtMarginMYR.Text != "")
            {
                if (checkMarginMYR())
                {
                    lblRetailRateMYR.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64((Int32.Parse(hfRateMYR.Value) - Int32.Parse(txtMarginMYR.Text)))).Replace("Rp", "Rp ");
                }
                else
                {
                    txtMarginMYR.Text = "0";
                }
            }
            else
            {
                lblRetailRateMYR.Text = "Undefined";
            }
        }

        protected void btnUpdateMYR_Click(object sender, EventArgs e)
        {
            if (txtMarginMYR.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Margin cannot be empty!";
                return;
            }

            if (checkMarginMYR())
            {
                var id = Int64.Parse(SessionLib.Current.AdminID);

                if (SessionLib.Current.Role == Roles.Agent)
                {
                    var arate = db.tblM_Agent_Rate.Where(p => p.RemittanceID == 1 && p.AgentID == id).FirstOrDefault();
                    if (arate != null)
                    {
                        arate.Margin = txtMarginMYR.Text;
                        arate.UpdatedDate = DateTime.Now;
                        //rate.UpdatedBy = Int64.Parse(SessionLib.Current.AdminID);
                        db.SaveChanges();

                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Success update margin become " + String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(txtMarginMYR.Text)).Replace("Rp", "Rp ") + "')", true);
                    }
                }
                else if (SessionLib.Current.Role == Roles.MasterArea)
                {
                    var arate = db.tblM_Master_Area_Rate.Where(p => p.RemittanceID == 1 && p.MasterAreaID == id).FirstOrDefault();
                    if (arate != null)
                    {
                        arate.Margin = txtMarginMYR.Text;
                        arate.UpdatedDate = DateTime.Now;
                        //rate.UpdatedBy = Int64.Parse(SessionLib.Current.AdminID);
                        db.SaveChanges();

                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Success update margin become " + String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(txtMarginMYR.Text)).Replace("Rp", "Rp ") + "')", true);
                    }
                }
            }
            else
            {
                return;
            }
        }

        bool checkMarginMYR() {
            var mar = Convert.ToDouble(txtMarginMYR.Text.Trim());
            if (mar > 50 || mar < -50)
            {
                diverror.Visible = true;
                lblError.Text = "Margin cannot more and less than 50 point!";
                return false;
            }
            else
            {
                return true;
            }
        }

        void countIDR()
        {
            var cur = omg.getRateIDRMYR();
            if (cur != null)
            {
                hfRateIDR.Value = cur.Rate;
            }

            var id = Int64.Parse(SessionLib.Current.AdminID);

            if (SessionLib.Current.Role == Roles.Agent)
            {
                var rateagent = db.tblM_Agent_Rate.Where(p => p.AgentID == id && p.RemittanceID == 2).FirstOrDefault();
                rate = db.tblM_Rate.Where(p => p.RateID == rateagent.RateID).FirstOrDefault();
                hfRateIDR.Value = (Double.Parse(hfRateIDR.Value) + Double.Parse(rate.Margin)).ToString();
                if (rateagent != null)
                {
                    lblCurrencyIDR.Text = rate.Currency;
                    txtMarginIDR.Text = rateagent.Margin;

                    lblBaseRateIDR.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(hfRateIDR.Value)).Replace("Rp", "Rp ");
                    lblRateIDR.Text = "1 MYR = " + String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(hfRateIDR.Value)).Replace("Rp", "") + " IDR";
                    lblRateUpdateIDR.Text = "Last update : " + rate.UpdatedDate.Value.ToLongTimeString() + ", " + rate.UpdatedDate.Value.ToLongDateString();
                }
            }
            else if (SessionLib.Current.Role == Roles.MasterArea)
            {
                var rateagent = db.tblM_Master_Area_Rate.Where(p => p.MasterAreaID == id && p.RemittanceID == 2).FirstOrDefault();
                rate = db.tblM_Rate.Where(p => p.RateID == rateagent.RateID).FirstOrDefault();
                hfRateIDR.Value = (Double.Parse(hfRateIDR.Value) + Double.Parse(rate.Margin)).ToString();
                if (rateagent != null)
                {
                    lblCurrencyIDR.Text = rate.Currency;
                    txtMarginIDR.Text = rateagent.Margin;

                    lblBaseRateIDR.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(hfRateIDR.Value)).Replace("Rp", "Rp ");
                    lblRateIDR.Text = "1 MYR = " + String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(hfRateIDR.Value)).Replace("Rp", "") + " IDR";
                    lblRateUpdateIDR.Text = "Last update : " + rate.UpdatedDate.Value.ToLongTimeString() + ", " + rate.UpdatedDate.Value.ToLongDateString();
                }
            }
            lblCountryIDR.Text = "Indonesia to Malaysia";
            countRateIDR();
            
        }

        void countRateIDR()
        {
            lblRetailRateIDR.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64((Int32.Parse(hfRateIDR.Value) + Int32.Parse(txtMarginIDR.Text)))).Replace("Rp", "Rp ");
        }

        protected void txtMarginIDR_TextChanged(object sender, EventArgs e)
        {
            if (txtMarginIDR.Text != "")
            {
                if (checkMarginIDR())
                {
                    lblRetailRateIDR.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64((Int32.Parse(hfRateIDR.Value) + Int32.Parse(txtMarginIDR.Text)))).Replace("Rp", "Rp ");
                }
                else
                {
                    txtMarginIDR.Text = "0";
                }
            }
            else
            {
                lblRetailRateIDR.Text = "Undefined";
            }
        }

        protected void btnUpdateIDR_Click(object sender, EventArgs e)
        {
            if (txtMarginIDR.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Margin cannot be empty!";
                return;
            }

            if (checkMarginIDR())
            {
                var id = Int64.Parse(SessionLib.Current.AdminID);

                if (SessionLib.Current.Role == Roles.Agent)
                {
                    var arate = db.tblM_Agent_Rate.Where(p => p.RemittanceID == 2 && p.AgentID == id).FirstOrDefault();
                    if (arate != null)
                    {
                        arate.Margin = txtMarginIDR.Text;
                        arate.UpdatedDate = DateTime.Now;
                        //rate.UpdatedBy = Int64.Parse(SessionLib.Current.AdminID);
                        db.SaveChanges();

                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Success update margin become " + String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(txtMarginIDR.Text)).Replace("Rp", "Rp ") + "')", true);
                    }
                }
                else if (SessionLib.Current.Role == Roles.MasterArea)
                {
                    var arate = db.tblM_Master_Area_Rate.Where(p => p.RemittanceID == 2 && p.MasterAreaID == id).FirstOrDefault();
                    if (arate != null)
                    {
                        arate.Margin = txtMarginIDR.Text;
                        arate.UpdatedDate = DateTime.Now;
                        //rate.UpdatedBy = Int64.Parse(SessionLib.Current.AdminID);
                        db.SaveChanges();

                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Success update margin become " + String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(txtMarginIDR.Text)).Replace("Rp", "Rp ") + "')", true);
                    }
                }
            }
            else
            {
                return;
            }
        }

        bool checkMarginIDR()
        {
            var mar = Convert.ToDouble(txtMarginIDR.Text.Trim());
            if (mar > 50 || mar < -50)
            {
                diverror.Visible = true;
                lblError.Text = "Margin cannot more and less than 50 point!";
                return false;
            }
            else
            {
                return true;
            }
        }

        protected void btnError_ServerClick(object sender, EventArgs e)
        {
            diverror.Visible = false;
        }

        protected void btnSuccess_ServerClick(object sender, EventArgs e)
        {
            divsuccess.Visible = false;
        }

        protected void ddlChooseRemittance_SelectedIndexChanged(object sender, EventArgs e)
        {
            checkRemttanceLine();
        }
        void checkRemttanceLine()
        {
            if (ddlChooseRemittance.SelectedValue == "1")
            {
                divmyr.Visible = true;
                dividr.Visible = false;
            }
            else if (ddlChooseRemittance.SelectedValue == "2")
            {
                divmyr.Visible = false;
                dividr.Visible = true;
            }
        }
    }
}