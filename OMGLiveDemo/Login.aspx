﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="OMGLiveDemo.Login" %>

<%@ MasterType virtualpath="~/Site.Master" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="apple-touch-icon" sizes="57x57" href="~/Content/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="~/Content/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="~/Content/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="~/Content/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="~/Content/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="~/Content/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="~/Content/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="~/Content/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="~/Content/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="~/Content/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="~/Content/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="~/Content/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="~/Content/favicon/favicon-16x16.png">
    <link rel="manifest" href="~/Content/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="~/Content/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Live Demo | Login</title>

    <!-- Bootstrap -->
    <link href="Content/bootstrap.min.css" rel="stylesheet"/>
    <!-- Font Awesome -->
    <link href="Content/font-awesome.min.css" rel="stylesheet"/>
    <!-- NProgress -->
    <link href="Content/nprogress.css" rel="stylesheet"/>
    <!-- Animate.css -->
    <link href="Content/animate.min.css" rel="stylesheet"/>

    <!-- Custom Theme Style -->
    <link href="Content/custom.min.css" rel="stylesheet"/>
</head>
<body class="login" style="background-image: url('Content/images/bg-landing.jpg')">

    <div>
        <a class="hiddenanchor" id="signup"></a>
        <a class="hiddenanchor" id="signin"></a>

        <div class="login_wrapper">
            <div class="animate form login_form">
                <section class="login_content">
                    <center>
                    <img src='<%= ResolveUrl("~/Content/images/autopay.png")%>' alt="..." width="350px" height="221px"></center>
                    <form id="form1" runat="server">
                        <h1>Login</h1>
                        <div>
                            <%--<input type="text" class="form-control" placeholder="Username" required="" />--%>
                            <asp:TextBox runat="server" CssClass="form-control" placeholder="HP" ID="txtHP" />
                        </div>
                        <div>
                            <%--<input type="password" class="form-control" placeholder="Password" required="" />--%>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="Password" placeholder="Password" ID="txtPassword" />
                        </div>
                        <div>
                            <%--<a class="btn btn-default submit" href="index.html">Log in</a>--%>
                            <asp:Button CssClass="btn btn-default submit" Text="Log In" runat="server" ID="btnLogin" OnClick="btnLogin_Click" />
                            <%--<a class="reset_pass" href="#">Lost your password?</a>--%>

                           
                        </div>

                        <div class="clearfix"></div>

                        <div class="separator">
                            <%--<p class="change_link">
                                New to site?
                     
                                <a href="#signup" class="to_register">Create Account </a>
                            </p>--%>

                            <div class="clearfix"></div>
                            <br />
                           
                            <div>
                                <h1>
                                    <%--<i class="fa fa-paw"></i>--%>
                                    AUTOPAY Live Demo</h1>
                                <p>©2019 All Rights Reserved. Designed by PT Dataprep Teknologi Indonesia. Privacy and Terms</p>
                            </div>

                             <a href="http://demo.malindogateway.com:8880/support" target="_blank"> Customer Support</a>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>
</body>
</html>
