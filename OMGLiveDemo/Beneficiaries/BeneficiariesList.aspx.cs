﻿using OMGLiveDemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.Beneficiaries
{
    public partial class BeneficiariesList : System.Web.UI.Page
    {
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    BindGV();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        void BindGV()
        {
            if (Session["SuccessBeneficiary"] != null)
            {
                lblAlertSuccess.Text = Session["SuccessBeneficiary"].ToString();
                divsuccess.Visible = true;
                Session["SuccessBeneficiary"] = null;
            }

            if (Session["ErrorBeneficiary"] != null)
            {
                lblAlertFailed.Text = Session["ErrorBeneficiary"].ToString();
                divfailed.Visible = true;
                Session["ErrorBeneficiary"] = null;
            }
            if (Session["Status"] != null)
            {
                ddlFilter.SelectedValue = Session["Status"].ToString();
            }

            var id = Int64.Parse(SessionLib.Current.AdminID);
            var cid = Int64.Parse(ddlChooseCountry.SelectedValue);
            var status = ddlFilter.SelectedValue;
            var search = txtName.Text;

            List<tblM_Beneficiary> dt;
            if (SessionLib.Current.Role == Roles.Agent)
            {
                dt = db.tblM_Beneficiary.Where(p => p.AgentID == id && p.isAgent == 1 && p.CountryID == cid && p.Status == status && (!string.IsNullOrEmpty(search) ? p.Name.Contains(search) : true)).ToList();
            }
            else if (SessionLib.Current.Role == Roles.MasterArea)
            {
                dt = db.tblM_Beneficiary.Where(p => p.AgentID == id && p.isAgent == 2 && p.CountryID == cid && p.Status == status && (!string.IsNullOrEmpty(search) ? p.Name.Contains(search) : true)).ToList();
            }
            else if (SessionLib.Current.Role == Roles.TravelAgent)
            {
                dt = db.tblM_Beneficiary.Where(p => p.AgentID == id && p.isAgent == 3 && p.CountryID == cid && p.Status == status && (!string.IsNullOrEmpty(search) ? p.Name.Contains(search) : true)).ToList();
            }
            else if (SessionLib.Current.Role == Roles.Operator)
            {
                dt = db.tblM_Beneficiary.Where(p => p.isAgent == 0 && p.CountryID == cid && p.Status == status && (!string.IsNullOrEmpty(search) ? p.Name.Contains(search) : true)).ToList();
            }
            else
            {
                dt = db.tblM_Beneficiary.Where(p => p.isAgent == 0 && p.CountryID == cid && p.Status == status).ToList();
            }
            gvListItem.DataSource = dt;
            gvListItem.DataBind();
        }

        protected void btnAlertFailed_ServerClick(object sender, EventArgs e)
        {
            divfailed.Visible = false;
        }

        protected void btnAlertSuccess_ServerClick(object sender, EventArgs e)
        {
            divsuccess.Visible = false;
        }

        protected void SelectedIndexChanged(object sender, EventArgs e)
        {
            txtName.Text = string.Empty;
            BindGV();
        }
        protected void TextChanged(object sender, EventArgs e)
        {
            BindGV();
        }
        protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (ddlFilter.SelectedValue == "approved")
                e.Row.Cells[7].Visible = true;
            else
                e.Row.Cells[7].Visible = false;
        }

        protected void btnViewBeneficiary_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            Session["ViewInfoBeneficiaryID"] = ((HiddenField)row.FindControl("hfBeneficiaryID")).Value;

            string queryString = "BeneficiaryDetail.aspx";
            string newWin = "window.open('" + queryString + "','Beneficiary Detail','width=900, height=800, titlebar=yes');";
            ClientScript.RegisterStartupScript(this.GetType(), "pop", newWin, true);
        }
    }
}