﻿using OMGLiveDemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.Beneficiaries
{
    public partial class ApproveBeneficiaryList : System.Web.UI.Page
    {
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    BindGV();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        void BindGV()
        {
            if (Session["SuccessApproveBeneficiary"] != null)
            {
                lblAlertSuccess.Text = Session["SuccessApproveBeneficiary"].ToString();
                divsuccess.Visible = true;
                Session["SuccessApproveBeneficiary"] = null;
            }

            if (Session["ErrorApproveBeneficiary"] != null)
            {
                lblAlertFailed.Text = Session["ErrorApproveBeneficiary"].ToString();
                divfailed.Visible = true;
                Session["ErrorApproveBeneficiary"] = null;
            }

            /*
            var dt = db.vw_Sender.Where(p => p.isActive == 1 && (p.Status == "created" || p.Status == "recreated") && p.Status != null).ToList();

            List<vw_Sender> dtnew = new List<vw_Sender>();
            for (int i = 0; i < dt.Count; i++)
            {
                if (dtnew.Where(p => p.SenderID == dt[i].SenderID).FirstOrDefault() == null)
                {
                    dtnew.Add(dt[i]);
                }
            }

            gvListItem.DataSource = dtnew;
            gvListItem.DataBind();
            */

            var dt = db.tblM_Beneficiary.Where(p => p.isActive == 1 && p.Status == "created").ToList();
            gvListItem.DataSource = dt;
            gvListItem.DataBind();
        }

        protected void lbtViewFiles_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            Session["BeneficiaryID"] = ((HiddenField)row.FindControl("hfBeneficiaryID")).Value;
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "colorBox", "var isColorbox = true", true);

            Response.Redirect("~/Beneficiaries/ApproveBeneficiary");
        }

        protected void btnAlertFailed_ServerClick(object sender, EventArgs e)
        {
            divfailed.Visible = false;
        }

        protected void btnAlertSuccess_ServerClick(object sender, EventArgs e)
        {
            divsuccess.Visible = false;
        }
    }
}