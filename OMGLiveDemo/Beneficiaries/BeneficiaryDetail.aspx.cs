﻿using OMGLiveDemo.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.Beneficiaries
{
    public partial class BeneficiaryDetail : System.Web.UI.Page
    {
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();

        protected void Page_Load(object sender, EventArgs e)
        {
            //tmp
            //Session["ViewInfoBeneficiaryID"] = "16";
            //if (!IsPostBack)
            //{
            //    Do();
            //}

            
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    Do();
                }
            }
            else
            {
                Master.checkRole();
            }
            
        }

        void Do()
        {
            long beneficiaryID = 0;
            if(Session["ViewInfoBeneficiaryID"] != null)
            {
                if (Session["ViewInfoBeneficiaryID"].ToString().Trim() == "")
                {

                }
                else
                {
                    beneficiaryID = Convert.ToInt64(Session["ViewInfoBeneficiaryID"].ToString());
                }
            }

            if(beneficiaryID > 0)
            {
                tblM_Beneficiary bene = db.tblM_Beneficiary.Where(p => p.BeneficiaryID == beneficiaryID).FirstOrDefault();
                if(bene != null)
                {
                    populateBeneficiaryDetail(bene);
                }
                else
                {
                    showBlank();
                }
                
            }
            else
            {
                showBlank();
            }
            
        }

        void showBlank()
        {
            txtName.Text = "Shareholder Not Found!";
        }

        void populateBeneficiaryDetail(tblM_Beneficiary beneficiary)
        {
            txtName.Text = beneficiary.Name;
            txtBank.Text = beneficiary.Bank;
            txtAccount.Text = beneficiary.Account;

            txtAlias.Text = beneficiary.Alias;
            txtAddress.Text = beneficiary.Address;

            txtCity.Text = "";
            if (beneficiary.CityID != 0)
            {
                tblM_City city = db.tblM_City.Where(p => p.ID == beneficiary.CityID).FirstOrDefault();
                if (city != null)
                {
                    txtCity.Text = city.City;
                }

                divCity.Visible = true;
            }

            txtCountry.Text = beneficiary.Country;
            txtPhone.Text = beneficiary.Phone;
            txtEmail.Text = beneficiary.Email;

        }

    }
}