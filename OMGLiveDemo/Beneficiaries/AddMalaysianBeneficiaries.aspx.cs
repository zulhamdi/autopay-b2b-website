﻿using OMGLiveDemo.Models;
using OMGLiveDemo.Models.OMG;
using OMGLiveDemo.Scripts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.Beneficiaries
{
    public partial class AddMalaysianBeneficiaries : System.Web.UI.Page
    {
        OMG omg = new OMG();
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    bind();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        void bind()
        {
            var bank = omg.getListBank("malaysia");
            if (bank != null)
            {
                ddlBank.DataSource = null;
                ddlBank.DataSource = bank;
                ddlBank.DataValueField = "Code";
                ddlBank.DataTextField = "Name";
                ddlBank.DataBind();
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            var id = Int64.Parse(SessionLib.Current.AdminID);
            if (txtName.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Beneficiary Name cannot be empty!";
                return;
            }

            if (txtAccount.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Beneficiary Account cannot be empty!";
                return;
            }

            if (txtAddress.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Address cannot be empty!";
                return;
            }

            if (txtPhone.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Phone cannot be empty!";
                return;
            }

            if (txtEmail.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Email cannot be empty!";
                return;
            }

            tblM_Beneficiary check;

            if (SessionLib.Current.Role == Roles.Agent)
            {
                check = db.tblM_Beneficiary.Where(p => p.AgentID == id && p.CountryID == 133 && p.BankCode == ddlBank.SelectedValue && p.Account == txtAccount.Text.Trim() && p.isAgent == 1).FirstOrDefault();
            }
            else if (SessionLib.Current.Role == Roles.MasterArea)
            {
                check = db.tblM_Beneficiary.Where(p => p.AgentID == id && p.CountryID == 133 && p.BankCode == ddlBank.SelectedValue && p.Account == txtAccount.Text.Trim() && p.isAgent == 2).FirstOrDefault();
            }
            else
            {
                check = db.tblM_Beneficiary.Where(p => p.CountryID == 133 && p.BankCode == ddlBank.SelectedValue && p.Account == txtAccount.Text.Trim() && p.isAgent == 0).FirstOrDefault();
            }

            if (check != null)
            {
                diverror.Visible = true;
                lblError.Text = "Account Number " + check.Account + " is already exist for Beneficiary Name: " + check.Name;
                return;
            }

            tblM_Beneficiary bnf = new tblM_Beneficiary();
            bnf.Name = txtName.Text.Trim();
            bnf.Account = txtAccount.Text;
            bnf.CityID = 0;
            bnf.CountryID = 133;
            bnf.Country = "Malaysia";
            bnf.Bank = ddlBank.SelectedItem.Text;
            bnf.BankCode = ddlBank.SelectedValue;
            bnf.Alias = txtAlias.Text.Trim();
            bnf.Phone = txtPhone.Text.Trim();
            bnf.Email = txtEmail.Text.Trim();
            bnf.Address = txtAddress.Text.Trim();
            bnf.FileID = txtAccount.Text.Trim();
            bnf.isActive = 1;
            bnf.CreatedDate = DateTime.Now;
            bnf.CreatedBy = id;
            bnf.Status = "created";
            if (SessionLib.Current.Role == Roles.Agent)
            {
                bnf.AgentID = id;
                bnf.isAgent = 1;
            }
            else if (SessionLib.Current.Role == Roles.MasterArea)
            {
                bnf.AgentID = id;
                bnf.isAgent = 2;
            }
            else
            {
                bnf.AgentID = 0;
                bnf.isAgent = 0;
            }
            db.tblM_Beneficiary.Add(bnf);
            db.SaveChanges();

            string eMsg = "New beneficiary: [" + bnf.BeneficiaryID + "] " + bnf.Name + "created by user [" + id + "] " + SessionLib.Current.Name + ".";
            tblS_Log sLog = new tblS_Log();
            sLog.EventDate = bnf.CreatedDate.Value;
            sLog.EventBy = id;
            sLog.EventType = "operation";
            sLog.EventData = eMsg.Trim();

            db.tblS_Log.Add(sLog);
            db.SaveChanges();

            Session["SuccessBeneficiary"] = "Success add " + bnf.Name + " to database";
            Response.Redirect("~/Beneficiaries/BeneficiariesList");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + bnf.Name + " added to database')", true);
        }

        protected void btnValidate_Click(object sender, EventArgs e)
        {
            BankAccount ba = new BankAccount();
            ba.AccountNo = txtAccount.Text.Trim();
            ba.BankCode = ddlBank.SelectedValue;
            var acc = omg.validateAccount(ba);
            if (acc != null)
            {
                txtName.Text = acc.AccountName;
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Found " + acc.AccountName + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('The account of " + ba.AccountNo + " and bank of " + ddlBank.SelectedItem + " is not found')", true);
            }
        }

        protected void btnError_ServerClick(object sender, EventArgs e)
        {
            diverror.Visible = false;
        }

        protected void ddlBank_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}