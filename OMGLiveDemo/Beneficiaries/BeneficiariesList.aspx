﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BeneficiariesList.aspx.cs" Inherits="OMGLiveDemo.Beneficiaries.BeneficiariesList" MaintainScrollPositionOnPostback="true" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" />

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph">

                <div class="row x_title">
                    <div class="col-md-12">
                        <h3>Beneficiaries List</h3>
                    </div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div style="width: 100%; margin-top: 5px">
                        <div id="canvas_dahs" class="demo-placeholder" style="width: 100%; height: 100%;">

                            <div class="alert alert-success alert-dismissible fade in" runat="server" id="divsuccess" visible="false">
                                <button type="button" runat="server" id="btnAlertSuccess" onserverclick="btnAlertSuccess_ServerClick" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <center><asp:Label runat="server" ID="lblAlertSuccess" Font-Size="14pt" Text="test error" /></center>
                            </div>

                            <div class="alert alert-danger alert-dismissible fade in" runat="server" id="divfailed" visible="false">
                                <button type="button" runat="server" id="btnAlertFailed" onserverclick="btnAlertFailed_ServerClick" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <center><asp:Label runat="server" ID="lblAlertFailed" Font-Size="14pt" Text="test error" /></center>
                            </div>

                            <div class="row" style="margin-bottom: 13px">
                                <div class="col-md-2">
                                    <asp:Label runat="server" CssClass="control-label" Text="Choose Country" />
                                    <asp:DropDownList runat="server" ID="ddlChooseCountry" CssClass="form-control" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true">
                                        <asp:ListItem Text="Indonesia" Value="103"></asp:ListItem>
                                        <asp:ListItem Text="Malaysia" Value="133"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="col-md-2">
                                    <asp:Label runat="server" CssClass="control-label" Text="Status Registration" />
                                    <asp:DropDownList runat="server" ID="ddlFilter" CssClass="form-control" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true">
                                        <asp:ListItem Text="Approved" Value="approved"></asp:ListItem>
                                        <asp:ListItem Text="Pending" Value="created"></asp:ListItem>
                                        <asp:ListItem Text="Amend" Value="amend"></asp:ListItem>
                                        <asp:ListItem Text="Rejected" Value="rejected"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>

                                <%-- <div class="col-md-2">
                                    <asp:Label runat="server" CssClass="control-label" Text="Start date" />
                                    <asp:TextBox ID="startdate" CssClass="form-control" runat="server" OnTextChanged="TextChanged"/>
                                </div>

                                 <div class="col-md-2">
                                    <asp:Label runat="server" CssClass="control-label" Text="End date" />
                                    <asp:TextBox ID="enddate" CssClass="form-control" runat="server" OnTextChanged="TextChanged"/>
                                </div>--%>

                                <div class="col-md-8">
                                    <asp:Label runat="server" CssClass="control-label" Text="Name" />
                                    <asp:TextBox runat="server" CssClass="form-control" ID="txtName" OnTextChanged="TextChanged" AutoPostBack="true"></asp:TextBox>
                                </div>
                            </div>

                            <asp:GridView CssClass="table table-striped jambo_table bulk_action" ID="gvListItem" runat="server" AutoGenerateColumns="false" DataKeyNames="BeneficiaryID" OnRowDataBound="GridView_RowDataBound">
                                <Columns>
                                    <asp:TemplateField HeaderText="No.">
                                        <ItemTemplate>
                                            <asp:HiddenField runat="server" ID="hfBeneficiaryID" Value='<%# Eval("BeneficiaryID") %>' />
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Name" HeaderText="Beneficiaries Name" />
                                    <asp:BoundField DataField="Country" HeaderText="Country" />
                                    <asp:BoundField DataField="Bank" HeaderText="Bank" />
                                    <asp:BoundField DataField="Account" HeaderText="Account Number" />
                                    <asp:BoundField DataField="Phone" HeaderText="Phone" />
                                    <asp:BoundField DataField="CreatedDate" HeaderText="Registration Date" />
                                    <asp:BoundField DataField="ApprovedDate" HeaderText="Approved Date" />
                                    <asp:TemplateField HeaderText="Action">
                                        <ItemTemplate>
                                            <asp:Button ID="btnViewBeneficiary" CssClass="btn btn-default" runat="server" Text="View" OnClick="btnViewBeneficiary_Click" CausesValidation="false" />
                                            <% if(OMGLiveDemo.Models.SessionLib.Current.Role == OMGLiveDemo.Models.Roles.Operator && (ddlFilter.SelectedValue == "created" || ddlFilter.SelectedValue == "amend")) { %>
                                                <asp:Button runat="server" ID="btnUpdateBeneficiary" CssClass="btn btn-info" runat="server" Text="Update" CausesValidation="false" />
                                            <% } %>
                                            <% if(OMGLiveDemo.Models.SessionLib.Current.Role == OMGLiveDemo.Models.Roles.Operator && (ddlFilter.SelectedValue == "amend")) { %>
                                                <asp:Button runat="server" ID="btnReSubmitBeneficiary" CssClass="btn btn-success" runat="server" Text="Re-Submit" CausesValidation="false" />
                                            <% } %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Scripts" runat="server">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" />
    <script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
   

    <script type="text/javascript">
         $("input[id*='startdate']").datepicker({
                format: "yyyy/mm/dd"
            });
            $("input[id*='enddate']").datepicker({
                format: "yyyy/mm/dd"
            });
    </script>
</asp:Content>
