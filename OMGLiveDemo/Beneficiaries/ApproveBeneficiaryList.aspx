﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ApproveBeneficiaryList.aspx.cs" Inherits="OMGLiveDemo.Beneficiaries.ApproveBeneficiaryList" MaintainScrollPositionOnPostback="true" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" />

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph">

                <div class="row x_title">
                    <div class="col-md-12">
                        <h3>Approve Beneficiary</h3>
                    </div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div style="width: 100%;">
                        <div id="canvas_dahs" class="demo-placeholder" style="width: 100%; height: 100%;">

                            <div class="alert alert-success alert-dismissible fade in" runat="server" id="divsuccess" visible="false">
                                <button type="button" runat="server" id="btnAlertSuccess" onserverclick="btnAlertSuccess_ServerClick" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <center><asp:Label runat="server" ID="lblAlertSuccess" Font-Size="14pt" Text="test error" /></center>
                            </div>

                            <div class="alert alert-danger alert-dismissible fade in" runat="server" id="divfailed" visible="false">
                                <button type="button" runat="server" id="btnAlertFailed" onserverclick="btnAlertFailed_ServerClick" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <center><asp:Label runat="server" ID="lblAlertFailed" Font-Size="14pt" Text="test error" /></center>
                            </div>

                            <asp:GridView CssClass="table table-striped jambo_table bulk_action" ID="gvListItem" runat="server" ShowHeaderWhenEmpty="true" AutoGenerateColumns="false" DataKeyNames="BeneficiaryID">
                                <Columns>
                                    <asp:TemplateField HeaderText="No.">
                                        <ItemTemplate>
                                            <asp:HiddenField runat="server" ID="hfBeneficiaryID" Value='<%# Eval("BeneficiaryID") %>' />
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Name" HeaderText="Beneficiary Name" />
                                    <asp:BoundField DataField="Bank" HeaderText="Bank" />
                                    <asp:BoundField DataField="Account" HeaderText="Account" />
                                    <asp:BoundField DataField="Phone" HeaderText="Phone" />
                                    <asp:BoundField DataField="Email" HeaderText="Email" />
                                    <asp:BoundField DataField="Status" HeaderText="Status" />
                                    <asp:BoundField DataField="CreatedDate" HeaderText="Registration Date" />
                                    <asp:TemplateField HeaderText="Actions">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtViewFiles" runat="server" Text="Action" CausesValidation="false" OnClick="lbtViewFiles_Click" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Scripts" runat="server">
    <script type="text/javascript">
        function ShowProgressBar() {
            document.getElementById('dvProcessing').style.visibility = 'visible';
            document.getElementById('dvBtnProcess').style.visibility = 'hidden';
        }

        function HideProgressBar() {
            document.getElementById('dvProgressBar').style.visibility = "hidden";
        }
    </script>

    <script type="text/javascript">
        $(window).load(function () {
            if (window.isColorbox) {
                $.colorbox({ href: "../Payout/ViewImage.aspx", iframe: true, width: "80%", height: "80%" });
            }
        });
        function OpenCBox() {
            $.colorbox({ href: "../Payout/ViewImage.aspx", iframe: true, width: "80%", height: "80%" });
        }

    </script>
</asp:Content>
