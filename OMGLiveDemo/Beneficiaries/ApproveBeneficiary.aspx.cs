﻿using OMGLiveDemo.Models;
using OMGLiveDemo.Models.OMG;
using OMGLiveDemo.Scripts;
using Oppal.Sec;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.Beneficiaries
{
    public partial class ApproveBeneficiary : System.Web.UI.Page
    {
        OMG omg = new OMG();
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    bind();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        void bind()
        {
            hfBeneficiaryID.Value = Session["BeneficiaryID"].ToString();
            var id = Convert.ToInt64(hfBeneficiaryID.Value);
            var benef = db.tblM_Beneficiary.Where(p => p.BeneficiaryID == id).FirstOrDefault();
            txtName.Text = benef.Name;
            txtBank.Text = benef.Bank;
            txtAccount.Text = benef.Account;
            txtAddress.Text = benef.Address;
            txtPhone.Text = benef.Phone;
            txtEmail.Text = benef.Email;
        }

        protected void btnError_ServerClick(object sender, EventArgs e)
        {
            diverror.Visible = false;
        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            diverror.Visible = false;

            var id = Convert.ToInt64(hfBeneficiaryID.Value);
            var aaa = db.tblM_Beneficiary.Where(p => p.BeneficiaryID == id).FirstOrDefault();
            aaa.Status = "approved";
            aaa.ApprovedBy = Convert.ToInt64(SessionLib.Current.AdminID);
            aaa.ApprovedDate = DateTime.Now;
            db.SaveChanges();

            string eMsg = "Approve beneficiary: [" + aaa.BeneficiaryID + "] " + aaa.Name + " by user [" + aaa.ApprovedBy + "] " + SessionLib.Current.Name + ".";
            tblS_Log sLog = new tblS_Log();
            sLog.EventDate = aaa.ApprovedDate.Value;
            sLog.EventBy = aaa.ApprovedBy.Value;
            sLog.EventType = "operation";
            sLog.EventData = eMsg.Trim();

            db.tblS_Log.Add(sLog);
            db.SaveChanges();

            Session["SuccessApproveBeneficiary"] = "Success approve beneficiary : " + txtName.Text;
            Response.Redirect("~/Beneficiaries/ApproveBeneficiaryList");
        }

        protected void btnAmend_Click(object sender, EventArgs e)
        {
            diverror.Visible = false;

            var id = Convert.ToInt64(hfBeneficiaryID.Value);
            var aaa = db.tblM_Beneficiary.Where(p => p.BeneficiaryID == id).FirstOrDefault();
            aaa.Status = "amend";
            aaa.AmendBy = Convert.ToInt64(SessionLib.Current.AdminID);
            aaa.AmendDate = DateTime.Now;
            db.SaveChanges();

            string eMsg = "Amend beneficiary: [" + aaa.BeneficiaryID + "] " + aaa.Name + " by user [" + aaa.AmendBy + "] " + SessionLib.Current.Name + ".";
            tblS_Log sLog = new tblS_Log();
            sLog.EventDate = aaa.AmendDate.Value;
            sLog.EventBy = aaa.AmendBy.Value;
            sLog.EventType = "operation";
            sLog.EventData = eMsg.Trim();

            db.tblS_Log.Add(sLog);
            db.SaveChanges();

            Session["SuccessApproveBeneficiary"] = "Success amend beneficiary : " + txtName.Text;
            Response.Redirect("~/Beneficiaries/ApproveBeneficiaryList");
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            diverror.Visible = false;
            if (txtRejectReason.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Please fill the reason why this sender rejected";
                return;
            }

            var id = Convert.ToInt64(hfBeneficiaryID.Value);
            var aaa = db.tblM_Beneficiary.Where(p => p.BeneficiaryID == id).FirstOrDefault();
            aaa.Status = "rejected";
            aaa.RejectedBy = Convert.ToInt64(SessionLib.Current.AdminID);
            aaa.RejectedDate = DateTime.Now;
            aaa.Message = txtRejectReason.Text.Trim();
            db.SaveChanges();

            string eMsg = "Reject sender: [" + aaa.BeneficiaryID + "] " + aaa.Name + " by user [" + aaa.RejectedBy + "] " + SessionLib.Current.Name + ".";
            tblS_Log sLog = new tblS_Log();
            sLog.EventDate = aaa.RejectedDate.Value;
            sLog.EventBy = aaa.RejectedBy.Value;
            sLog.EventType = "operation";
            sLog.EventData = eMsg.Trim();

            db.tblS_Log.Add(sLog);
            db.SaveChanges();


            Session["SuccessApproveBeneficiary"] = "Success reject beneficiary with reason : " + txtRejectReason.Text;
            Response.Redirect("~/Beneficiaries/ApproveBeneficiaryList");
        }
    }
}