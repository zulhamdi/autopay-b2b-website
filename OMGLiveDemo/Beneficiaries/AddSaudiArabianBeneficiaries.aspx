﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddSaudiArabianBeneficiaries.aspx.cs" Inherits="OMGLiveDemo.Beneficiaries.AddSaudiArabianBeneficiaries" MaintainScrollPositionOnPostback="true" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" />

    <div class="row">
        <div class="row x_title">
            <div class="col-md-12">
                <h3>Add Saudi Arabian Beneficiary</h3>
            </div>
        </div>

        <div class="col-md-3 col-sm-12 col-xs-12"></div>
        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="dashboard_graph">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div style="width: 100%;">
                        <div id="canvas_dahs" class="demo-placeholder" style="width: 100%; height: 100%;">
                            <div class="form-group" style="height: 30px"></div>

                            <div class="alert alert-danger alert-dismissible fade in" runat="server" id="diverror" visible="false">
                                <button type="button" runat="server" id="btnError" onserverclick="btnError_ServerClick" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <center><asp:Label runat="server" ID="lblError" /></center>
                            </div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblName" CssClass="control-label" Text="Beneficiary Name *" />
                                <asp:TextBox runat="server" ID="txtName" CssClass="form-control" Enabled="true" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblBank" CssClass="control-label" Text="Beneficiary Bank *" />
                                <asp:DropDownList runat="server" ID="ddlBank" CssClass="form-control" OnSelectedIndexChanged="ddlBank_SelectedIndexChanged" AutoPostBack="true" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblAccount" CssClass="control-label" Text="Beneficiary Account *" />
                                <asp:TextBox runat="server" ID="txtAccount" CssClass="form-control" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblAlias" CssClass="control-label" Text="Beneficiary Alias " />
                                <asp:TextBox runat="server" ID="txtAlias" CssClass="form-control" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="Label1" CssClass="control-label" Text="Beneficiary Address *" />
                                <asp:TextBox runat="server" ID="txtAddress" CssClass="form-control" Rows="3" TextMode="MultiLine" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblPhone" CssClass="control-label" Text="Beneficiary Phone *" />
                                <asp:TextBox runat="server" ID="txtPhone" CssClass="form-control" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblEmail" CssClass="control-label" Text="Beneficiary Email *" />
                                <asp:TextBox runat="server" ID="txtEmail" CssClass="form-control" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <center><asp:Button runat="server" ID="btnAdd" Text="Add Beneficiaries" CssClass="btn btn-success btn-lg" OnClick="btnAdd_Click" Width="100%" /></center>
                            </div>

                            <div class="form-group" style="height: 30px"></div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12"></div>
    </div>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Scripts" runat="server">
</asp:Content>
