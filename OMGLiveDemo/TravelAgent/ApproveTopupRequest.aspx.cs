﻿using OMGLiveDemo.Models;
using OMGLiveDemo.Models.OMG;
using OMGLiveDemo.Scripts;
using Oppal.Sec;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.TravelAgent
{
    public partial class ApproveTopupRequest : System.Web.UI.Page
    {
        OMG omg = new OMG();
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    bind();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        void bind()
        {
            hfTopupID.Value = Session["TopupID"].ToString();
            var id = hfTopupID.Value;
            var tup = db.vw_Topup.Where(p => p.TopupID == id).FirstOrDefault();
            hfBankAccountID.Value = tup.BankAccountID.ToString();
            hfCurrency.Value = tup.Currency;
            txtName.Text = db.tblM_Travel_Agent.Where(p => p.TravelAgentID == tup.UserID).Select(p => p.Name).FirstOrDefault();
            lblTotalAmount.Text = tup.Currency + " " + String.Format(new CultureInfo("id-ID"), "{0:n}", tup.TotalAmount).Replace("Rp", "Rp ");
            txtBankName.Text = tup.BankName;
            txtAccount.Text = tup.Account;
            txtAccountHolder.Text = tup.AccountHolder;
            hfCountryID.Value = tup.CountryID.ToString();
            hfTopupReceiptURL.Value = tup.ReceiptURL;
            hfTravelAgentID.Value = Convert.ToString(tup.UserID);
            hfBalanceID.Value = Convert.ToString(tup.BalanceID);
        }

        protected void lkbViewImage_Click(object sender, EventArgs e)
        {
            if (hfTopupReceiptURL.Value != null)
            {
                Session["ImageURL"] = hfTopupReceiptURL.Value;
                Session["From"] = "Image";
                string txt = lblTotalAmount.Text;
                Session["Text"] = txtName.Text + " - " + txt;
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "colorBox", "var isColorbox = true", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('File not found!')", true);
            }
        }

        protected void btnError_ServerClick(object sender, EventArgs e)
        {
            diverror.Visible = false;
        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            diverror.Visible = false;

            try
            {
                var adminid = Convert.ToInt64(SessionLib.Current.AdminID);
                var id = hfTopupID.Value;

                
                var aaa = db.tblT_Topup.Where(p => p.TopupID == id).FirstOrDefault();
                aaa.Status = "approved";
                aaa.ApprovedBy = adminid;
                aaa.ApprovedDate = DateTime.Now;
                aaa.ModifiedDate = aaa.ApprovedDate;

                TopupModel tup = new TopupModel();
                tup.Amount = aaa.Amount.Value;
                tup.Notes = "Travel agent topup : " + aaa.UserID + " - " + aaa.TopupID;
                tup.BankAccountID = Int64.Parse(hfBankAccountID.Value);
                tup.Currency = hfCurrency.Value;
                if (omg.createAndApproveTopupMalindo(tup))
                {

                    db.SaveChanges();

                    var balid = Convert.ToInt64(hfBalanceID.Value);
                    var maid = Convert.ToInt64(hfTravelAgentID.Value);

                    var bal = db.tblM_Travel_Agent_Balance.Where(p => p.TravelAgentID == maid && p.BalanceID == balid).FirstOrDefault();
                    var balpref = bal.Balance;
                    bal.Balance += aaa.TotalAmount;
                    bal.UpdatedBy = adminid;
                    bal.UpdatedDate = DateTime.Now;
                    db.SaveChanges();

                    var bacc = db.vw_Bank_Account.Where(p => p.BankAccountID == aaa.BankAccountID).FirstOrDefault();

                    tblH_Travel_Agent_Balance_Update hbal = new tblH_Travel_Agent_Balance_Update();
                    hbal.TravelAgentID = Convert.ToInt64(hfTravelAgentID.Value);
                    hbal.BalanceID = Convert.ToInt64(hfBalanceID.Value);
                    hbal.RemittanceID = 0;
                    hbal.Reference = aaa.TopupID;
                    hbal.Type = "Topup";
                    hbal.BankID = bacc.BankID.Value;
                    hbal.Beneficiary = bacc.AccountHolder;
                    hbal.Account = bacc.Account;
                    hbal.BankName = bacc.BankName;
                    hbal.BalancePrevious = balpref.Value;
                    hbal.Amount = aaa.Amount.Value;
                    hbal.AdminFee = aaa.AdminFee.Value;
                    hbal.Balance = bal.Balance.Value;
                    hbal.DateCreated = DateTime.Now;
                    db.tblH_Travel_Agent_Balance_Update.Add(hbal);
                    db.SaveChanges();


                    Session["SuccessApproveTopup"] = "Success approve topup : " + hfTopupID.Value;
                    Response.Redirect("~/TravelAgent/PendingTopupRequest");
                }
                else
                {
                    diverror.Visible = true;
                    lblError.Text = "Failed create and approve topup in partner.malindogateway.com. Please ask IT for more.";
                    return;
                }
            }
            catch (Exception ex)
            {
                diverror.Visible = true;
                lblError.Text = ex.Message;
                return;
            }
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            diverror.Visible = false;
            if (txtRejectReason.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Please fill the reason why this topup request rejected";
                return;
            }

            var id = hfTopupID.Value;
            var aaa = db.tblT_Topup.Where(p => p.TopupID == id).FirstOrDefault();
            aaa.Status = "rejected";
            aaa.RejectedBy = Convert.ToInt64(SessionLib.Current.AdminID);
            aaa.RejectedDate = DateTime.Now;
            aaa.StatusMessage = txtRejectReason.Text.Trim();
            db.SaveChanges();


            Session["FailedApproveTopup"] = "Success reject topup request for Topup ID " + hfTopupID.Value + " : " + txtRejectReason.Text;
            Response.Redirect("~/TravelAgent/PendingTopupRequest");
        }
    }
}