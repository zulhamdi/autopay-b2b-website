﻿using OMGLiveDemo.Models;
using OMGLiveDemo.Models.OMG;
using OMGLiveDemo.Scripts;
using Oppal.Sec;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.TravelAgent
{
    public partial class TopupRequest : System.Web.UI.Page
    {
        OMG omg = new OMG();
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    bind();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        void bind()
        {
            getMasterAreaList();
        }

        void getMasterAreaList()
        {
            var partner = db.tblM_Travel_Agent.Where(p => p.isActive == 1).ToList();
            ddlTravelAgent.DataSource = null;
            ddlTravelAgent.DataSource = partner;
            ddlTravelAgent.DataValueField = "TravelAgentID";
            ddlTravelAgent.DataTextField = "Fullname";
            ddlTravelAgent.DataBind();
            getBalanceList();
        }

        void getBalanceList()
        {
            long pid = Convert.ToInt64(ddlTravelAgent.SelectedValue);
            var dt = db.tblM_Travel_Agent_Balance.Where(p => p.TravelAgentID == pid).ToList();
            ddlBalance.DataSource = null;
            ddlBalance.DataSource = dt;
            ddlBalance.DataValueField = "BalanceID";
            ddlBalance.DataTextField = "Currency";
            ddlBalance.DataBind();
            getBankAccount();
        }

        void getBankAccount()
        {
            long cid = 0;
            if (ddlBalance.SelectedItem.Text == "IDR")
            {
                lblAmount.Text = "Amount (in IDR) *";
                cid = 103;
            }
            else if (ddlBalance.SelectedItem.Text == "MYR")
            {
                lblAmount.Text = "Amount (in MYR) *";
                cid = 133;
            }
            var bankacc = db.vw_Bank_Account.Where(p => p.CountryID == cid).ToList();
            if (bankacc != null)
            {
                for (int i = 0; i < bankacc.Count; i++)
                {
                    bankacc[i].NameBank = bankacc[i].BankName + " - " + bankacc[i].Account + " - " + bankacc[i].AccountHolder;
                }

                ddlBankAccount.DataSource = null;
                ddlBankAccount.DataSource = bankacc;
                ddlBankAccount.DataValueField = "BankAccountID";
                ddlBankAccount.DataTextField = "NameBank";
                ddlBankAccount.DataBind();
            }
        }

        protected void btnError_ServerClick(object sender, EventArgs e)
        {
            diverror.Visible = false;
        }

        protected void btnRequest_Click(object sender, EventArgs e)
        {
            try
            {
                diverror.Visible = false;
                var id = Int64.Parse(SessionLib.Current.AdminID);
                if (txtAmount.Text == "")
                {
                    diverror.Visible = true;
                    lblError.Text = "Amount cannot be empty!";
                    return;
                }

                if (ddlBalance.SelectedItem.Text == "IDR")
                {
                    if (Convert.ToDouble(txtAmount.Text) < 10000000)
                    {
                        diverror.Visible = true;
                        lblError.Text = "Minimum topup amount is IDR 10.000.000";
                        return;
                    }
                }
                else if (ddlBalance.SelectedItem.Text == "MYR")
                {
                    if (Convert.ToDouble(txtAmount.Text) < 10000)
                    {
                        diverror.Visible = true;
                        lblError.Text = "Minimum topup amount is MYR 10.000";
                        return;
                    }
                }

                if (txtNotes.Text == "")
                {
                    diverror.Visible = true;
                    lblError.Text = "Notes cannot be empty!";
                    return;
                }

                if (!fuReceipt.HasFile)
                {
                    lblError.Text = "Please choose your receipt of your topup transfer";
                    diverror.Visible = true;
                    return;
                }
                else
                {
                    if (fuReceipt.PostedFile.ContentType != "image/jpg" && fuReceipt.PostedFile.ContentType != "image/jpeg" && fuReceipt.PostedFile.ContentType != "image/png")
                    {
                        lblError.Text = "Only jpeg, jpg and png format are allowed";
                        diverror.Visible = true;
                        return;
                    }
                }

                string filename = ddlTravelAgent.SelectedValue + "_" + ddlTravelAgent.SelectedItem.Text.Replace(" ", "-") + "_Topup_" + Common.getTimeSpan() + "." + fuReceipt.PostedFile.ContentType.Replace("image/", "");
                string targetPath = Server.MapPath("../Content/images/Topup/Receipt/" + filename);
                Stream strm = fuReceipt.PostedFile.InputStream;
                Common.SaveImage(0.5, strm, targetPath);

                var taid = Convert.ToInt64(ddlTravelAgent.SelectedValue);
                var balid = Convert.ToInt64(ddlBalance.SelectedValue);
                var bal = db.tblM_Travel_Agent_Balance.Where(p => p.TravelAgentID == taid && p.BalanceID == balid).FirstOrDefault();
                tblT_Topup tup = new tblT_Topup();
                tup.TopupID = "TUP-" + (db.tblT_Topup.Count() + 1).ToString("0000000");
                db.tblT_Topup.Add(tup);
                db.SaveChanges();

                tup.UserID = taid;
                tup.UserType = "TA";
                tup.BankAccountID = Convert.ToInt64(ddlBankAccount.SelectedValue);
                tup.BalanceID = balid;
                tup.Currency = bal.Currency;
                tup.Amount = Convert.ToDouble(txtAmount.Text);
                tup.AdminFee = 0;
                tup.UniqueCode = 0;
                tup.TotalAmount = Convert.ToDouble(txtAmount.Text) + tup.AdminFee + tup.UniqueCode;
                tup.Status = "created";
                tup.Notes = txtNotes.Text.Trim();
                tup.CreatedBy = id;
                tup.CreatedDate = DateTime.Now;
                tup.isCreatedByAdmin = 0;
                tup.isActive = 1;
                tup.ReceiptURL = ConfigurationManager.AppSettings["URL_RECEIPT_TOPUP_REQUEST"].ToString() + filename;

                db.SaveChanges();

                var admin = db.tblM_Admin.Where(p => (p.RoleID == 1 || p.RoleID == 2 || p.RoleID == 3) && p.Email != null && p.Email != "" && p.isActive == 1).ToList();
                omg.sendEmailTopupApproval(admin, ddlTravelAgent.SelectedItem.Text);

                Session["TopupID"] = tup.TopupID;
                Session["SuccessTopup"] = "Success request topup balance for " + ddlTravelAgent.SelectedItem.Text + " and need your or others admin approval";
                Response.Redirect("~/TravelAgent/ApproveTopupRequest");
            }
            catch (Exception ex)
            {
                lblError.Text = ex.ToString();
                diverror.Visible = true;
                return;
            }
        }

        protected void ddlBankAccount_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void ddlBalance_SelectedIndexChanged(object sender, EventArgs e)
        {
            getBankAccount();
        }

        protected void ddlTravelAgent_SelectedIndexChanged(object sender, EventArgs e)
        {
            getBalanceList();
        }
    }
}