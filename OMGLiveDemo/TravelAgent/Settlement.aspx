﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Settlement.aspx.cs" Inherits="OMGLiveDemo.TravelAgent.Settlement" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" />

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph">

                <div class="row x_title">
                    <div class="col-md-12">
                        <h3>Settlement Report</h3>
                    </div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div style="width: 100%;">
                        <div id="canvas_dahs" class="demo-placeholder" style="width: 100%; height: 100%;">
                            <div class="col-md-12 col-sm-12 col-xs-12" style="background-color: #f8f8f8; margin-bottom: 20px; padding-top: 10px">
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group" runat="server" id="divpartner" visible="false">
                                        <asp:Label runat="server" ID="lblPartner" CssClass="control-label" Text="Travel Agent : " />
                                        <div style="margin-top: 5px">
                                            <asp:DropDownList runat="server" ID="ddlPartner" CssClass="form-control" OnSelectedIndexChanged="ddlPartner_SelectedIndexChanged" AutoPostBack="true" />
                                        </div>
                                        <div class="form-group" style="height: 5px"></div>
                                    </div>
                                    <div class="form-group" runat="server" id="div1" visible="true">
                                        <asp:Label runat="server" ID="Label7" CssClass="control-label" Text="Balance : " />
                                        <div style="margin-top: 5px">
                                            <asp:DropDownList runat="server" ID="ddlBalance" CssClass="form-control" OnSelectedIndexChanged="ddlBalance_SelectedIndexChanged" AutoPostBack="true" />
                                        </div>
                                        <div class="form-group" style="height: 5px"></div>
                                    </div>
                                    <div class="form-group" runat="server" id="divsortby" visible="true">
                                        <asp:Label runat="server" ID="Label1" CssClass="control-label" Text="Sort by : " />
                                        <div style="margin-top: 5px">
                                            <asp:DropDownList runat="server" ID="ddlSortBy" CssClass="form-control" OnSelectedIndexChanged="ddlSortBy_SelectedIndexChanged" AutoPostBack="true" CausesValidation="true">
                                                <asp:ListItem Enabled="true" Text="All" Value="all"></asp:ListItem>
                                                <asp:ListItem Text="Date" Value="date"></asp:ListItem>
                                                <asp:ListItem Text="Date Range" Value="daterange"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="form-group" style="height: 5px"></div>
                                    </div>

                                    <div class="form-group" runat="server" id="divsingledate" visible="false">
                                        <asp:Label runat="server" ID="Label2" CssClass="control-label" Text="Choose Date : " />
                                        <div style="margin-top: 5px">
                                            <asp:TextBox ID="datepurchased" CssClass="form-control" runat="server" />
                                            <div style="height: 2px"></div>
                                            <asp:Button runat="server" ID="btnSingleDate" Text="Check" CssClass="btn btn-success btn-sm" OnClick="btnSingleDate_Click" />
                                        </div>
                                        <div class="form-group" style="height: 5px"></div>
                                    </div>

                                    <div class="form-group" runat="server" id="divdaterange" visible="false">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-6 col-sm-12 col-xs-12">
                                                <asp:Label runat="server" ID="Label4" CssClass="control-label" Text="Start Date : " />
                                                <div style="margin-top: 5px">
                                                    <asp:TextBox ID="startdate" CssClass="form-control" runat="server" />
                                                    <div style="height: 2px"></div>
                                                    <asp:Button runat="server" ID="btnDateRange" Text="Check" CssClass="btn btn-success btn-sm" OnClick="btnDateRange_Click" />
                                                </div>
                                                <div class="form-group" style="height: 5px"></div>
                                            </div>
                                            <div class="col-md-6 col-sm-12 col-xs-12">
                                                <asp:Label runat="server" ID="Label5" CssClass="control-label" Text="End Date : " />
                                                <div style="margin-top: 5px">
                                                    <asp:TextBox ID="enddate" CssClass="form-control" runat="server" />
                                                </div>
                                                <div class="form-group" style="height: 5px"></div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <asp:Label runat="server" ID="lblTotal" CssClass="control-label" Text="Total Topup" Width="100%" Style="text-align: right;" />
                                        <br />
                                        <asp:Label runat="server" ID="lblTotalTopup" Font-Bold="true" Font-Size="X-Large" ForeColor="Red" Width="100%" Style="text-align: right;" />
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label3" CssClass="control-label" Text="Total Transfer" Width="100%" Style="text-align: right;" />
                                        <br />
                                        <asp:Label runat="server" ID="lblTotalTransfer" Font-Bold="true" Font-Size="X-Large" ForeColor="Red" Width="100%" Style="text-align: right;" />
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label6" CssClass="control-label" Text="Total Refund" Width="100%" Style="text-align: right;" />
                                        <br />
                                        <asp:Label runat="server" ID="lblTotalRefund" Font-Bold="true" Font-Size="X-Large" ForeColor="Red" Width="100%" Style="text-align: right;" />
                                    </div>
                                    <div class="form-group" style="height: 5px"></div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <asp:GridView CssClass="table table-striped jambo_table bulk_action" ID="gvListItem" runat="server" AutoGenerateColumns="false" DataKeyNames="Reference" AllowPaging="True" PageSize="20" AllowCustomPaging="False" OnPageIndexChanging="gvListItem_PageIndexChanging">
                                    <PagerStyle HorizontalAlign="Center" CssClass="bs4-aspnet-pager" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="No.">
                                            <ItemTemplate>
                                                <asp:HiddenField runat="server" ID="hfReference" Value='<%# Eval("Reference") %>' />
                                                <asp:HiddenField runat="server" ID="hfAmount" Value='<%# Eval("Amount") %>' />
                                                <%# Container.DataItemIndex + 1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Reference" HeaderText="Reference" />
                                        <asp:BoundField DataField="Type" HeaderText="Type" />
                                        <asp:BoundField DataField="DateCreated" HeaderText="Created Date" />
                                        <asp:BoundField DataField="Beneficiary" HeaderText="Beneficiary Name" />
                                        <asp:BoundField DataField="BankName" HeaderText="Beneficiary Bank" />
                                        <asp:BoundField DataField="BalancePreviousText" HeaderText="Balance Previous" />
                                        <asp:BoundField DataField="AmountText" HeaderText="Amount" />
                                        <asp:BoundField DataField="AdminFeeText" HeaderText="Admin Fee" />
                                        <asp:BoundField DataField="BalanceText" HeaderText="Balance" />
                                        <asp:BoundField DataField="Status" HeaderText="Status" />
                                    </Columns>
                                </asp:GridView>
                                <asp:Button runat="server" ID="btnDownload" OnClick="btnDownload_Click" Text="Download Report" CssClass="btn btn-success" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Scripts" runat="server">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" />
    <script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript">
        // When the document is ready
        $(document).ready(function () {

            $("input[id*='datepurchased']").datepicker({
                format: "yyyy/mm/dd"
            });
            $("input[id*='startdate']").datepicker({
                format: "yyyy/mm/dd"
            });
            $("input[id*='enddate']").datepicker({
                format: "yyyy/mm/dd"
            });

        });
    </script>
</asp:Content>
