﻿using Newtonsoft.Json;
using OMGLiveDemo.Models;
using OMGLiveDemo.Models.OMG;
using OMGLiveDemo.Scripts;
using Oppal.Sec;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.TravelAgent
{
    public partial class UpdateTravelAgent : System.Web.UI.Page
    {
        OMG omg = new OMG();
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    bind();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        void bind()
        {
            var agentid = Int64.Parse(Session["TravelAgentID"].ToString());
            var ratelev = Session["RateLevel"].ToString();
            var rid = Convert.ToInt64(Session["RemittanceID"].ToString());
            hfRemittanceID.Value = rid.ToString();
            string rateback = "";

            var agent = db.tblM_Travel_Agent.Where(p => p.TravelAgentID == agentid).First();
            if (agent != null)
            {
                txtName.Text = agent.Name;
                txtUsername.Text = agent.Username;
                txtPhone.Text = agent.Phone;
                txtEmail.Text = agent.Email;

                txtNameInCharge.Text = agent.ResponsibleName;
                txtPhoneInCharge.Text = agent.ResponsiblePhone;
                txtNationalID.Text = agent.ResponsibleKTP;

                txtSIUP.Text = agent.SIUP;
                txtNPWP.Text = agent.NPWP;
                txtTDP.Text = agent.TDP;
                txtSKMenkumham.Text = agent.SKMenkumham;
            }

            if (rid == 1)
                bindMYRtoIDR(agentid, rid, ratelev, rateback);
            else if (rid == 2)
                bindIDRtoMYR(agentid, rid, ratelev, rateback);
            else if (rid == 3)
                bindIDRtoSAR(agentid, rid, ratelev, rateback);
        }

        void bindMYRtoIDR(long agentid, long rid, string ratelev, string rateback)
        {
            lblRemittanceName.Text = "Malaysia to Indonesia";
            lblFee.Text = "Admin Fee (in IDR) *";
            string rts = null;
            var cur = omg.getRateMYR2IDR();
            if (cur != null)
            {
                rts = cur.Rate;
            }

            List<tblM_Rate> rate = new List<tblM_Rate>();
            rate = omg.getListRateMYRIDR();
            if (rate.Count > 0)
            {
                for (int i = 0; i < rate.Count; i++)
                {
                    rate[i].Rate = (Int32.Parse(rts) - Int32.Parse(rate[i].Margin)).ToString();
                    rate[i].NameRate = rate[i].Name + " - Rp " + String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(Int32.Parse(rate[i].Rate))).Replace("Rp", "");
                    if (ratelev == rate[i].Name)
                        rateback = rate[i].RateID.ToString();
                    if (i == 0)
                        rts = rate[i].Rate;
                }
                ddlRate.DataSource = null;
                ddlRate.DataSource = rate;
                ddlRate.DataValueField = "RateID";
                ddlRate.DataTextField = "NameRate";
                ddlRate.DataBind();

                var marate = db.tblM_Master_Area_Rate.Where(p => p.MasterAreaID == agentid && p.RemittanceID == rid).FirstOrDefault();
                hfRateID.Value = Convert.ToString(marate.RateID);
                ddlRate.SelectedValue = hfRateID.Value;
                var mafee = db.tblM_Master_Area_Fee.Where(p => p.RateID == marate.ID).FirstOrDefault();
                hfFee.Value = Convert.ToString(mafee.Fee);
                txtFee.Text = hfFee.Value;
            }

            ddlRate.Text = rateback;
        }

        void bindIDRtoMYR(long agentid, long rid, string ratelev, string rateback)
        {
            lblRemittanceName.Text = "Indonesia to Malaysia";
            lblFee.Text = "Admin Fee (in IDR) *";
            string rts = null;
            var cur = omg.getRateIDRMYR();
            if (cur != null)
            {
                rts = cur.Rate;
            }

            List<tblM_Rate> rate = new List<tblM_Rate>();
            rate = omg.getListRateIDRMYR();
            if (rate.Count > 0)
            {
                for (int i = 0; i < rate.Count; i++)
                {
                    rate[i].Rate = (Int32.Parse(rts) + Int32.Parse(rate[i].Margin)).ToString();
                    rate[i].NameRate = rate[i].Name + " - Rp " + String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(Int32.Parse(rate[i].Rate))).Replace("Rp", "");
                    if (ratelev == rate[i].Name)
                        rateback = rate[i].RateID.ToString();
                    if (i == 0)
                        rts = rate[i].Rate;
                }
                ddlRate.DataSource = null;
                ddlRate.DataSource = rate;
                ddlRate.DataValueField = "RateID";
                ddlRate.DataTextField = "NameRate";
                ddlRate.DataBind();

                var marate = db.tblM_Master_Area_Rate.Where(p => p.MasterAreaID == agentid && p.RemittanceID == rid).FirstOrDefault();
                hfRateID.Value = Convert.ToString(marate.RateID);
                ddlRate.SelectedValue = hfRateID.Value;
                var mafee = db.tblM_Master_Area_Fee.Where(p => p.RateID == marate.ID).FirstOrDefault();
                hfFee.Value = Convert.ToString(mafee.Fee);
                txtFee.Text = hfFee.Value;
            }

            ddlRate.Text = rateback;
        }

        void bindIDRtoSAR(long agentid, long rid, string ratelev, string rateback)
        {
            lblRemittanceName.Text = "Indonesia to Saudi Arabia";
            lblFee.Text = "Admin Fee (in IDR) *";
            string rts = null;
            var cur = omg.getRateIDRSAR();
            if (cur != null)
            {
                rts = cur.Rate;
            }

            List<tblM_Rate> rate = new List<tblM_Rate>();
            rate = omg.getListRateIDRSAR();
            if (rate.Count > 0)
            {
                for (int i = 0; i < rate.Count; i++)
                {
                    rate[i].Rate = (Int32.Parse(rts) + Int32.Parse(rate[i].Margin)).ToString();
                    rate[i].NameRate = rate[i].Name + " - Rp " + String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(Int32.Parse(rate[i].Rate))).Replace("Rp", "");
                    if (ratelev == rate[i].Name)
                        rateback = rate[i].RateID.ToString();
                    if (i == 0)
                        rts = rate[i].Rate;
                }
                ddlRate.DataSource = null;
                ddlRate.DataSource = rate;
                ddlRate.DataValueField = "RateID";
                ddlRate.DataTextField = "NameRate";
                ddlRate.DataBind();

                var marate = db.tblM_Travel_Agent_Rate.Where(p => p.TravelAgentID == agentid && p.RemittanceID == rid).FirstOrDefault();
                hfRateID.Value = Convert.ToString(marate.RateID);
                ddlRate.SelectedValue = hfRateID.Value;
                var mafee = db.tblM_Travel_Agent_Fee.Where(p => p.RateID == marate.ID).FirstOrDefault();
                hfFee.Value = Convert.ToString(mafee.Fee);
                txtFee.Text = hfFee.Value;
            }

            ddlRate.Text = rateback;
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            if (txtName.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Name cannot be empty!";
                return;
            }

            if (txtPhone.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Phone cannot be empty!";
                return;
            }

            if (hfRemittanceID.Value == "1")
            {
                if (txtFee.Text == "")
                {
                    diverror.Visible = true;
                    lblError.Text = "Admin fee cannot be empty!";
                    return;
                }

                if (Convert.ToDouble(txtFee.Text) < 7500)
                {
                    diverror.Visible = true;
                    lblError.Text = "Minimum admin fee is IDR 7500!";
                    return;
                }
            }
            else if (hfRemittanceID.Value == "2")
            {
                if (txtFee.Text == "")
                {
                    diverror.Visible = true;
                    lblError.Text = "Admin fee cannot be empty!";
                    return;
                }

                if (Convert.ToDouble(txtFee.Text) < 7500)
                {
                    diverror.Visible = true;
                    lblError.Text = "Minimum admin fee is IDR 7500!";
                    return;
                }
            }
            else if (hfRemittanceID.Value == "3")
            {
                if (txtFee.Text == "")
                {
                    diverror.Visible = true;
                    lblError.Text = "Admin fee cannot be empty!";
                    return;
                }

                if (Convert.ToDouble(txtFee.Text) < 7500)
                {
                    diverror.Visible = true;
                    lblError.Text = "Minimum admin fee is IDR 7500!";
                    return;
                }
            }

            string result = "";

            var agent = db.tblM_Travel_Agent.Where(p => p.Phone == txtPhone.Text.Trim()).FirstOrDefault();
            if (agent != null)
            {
                agent.Name = txtName.Text.Trim();
                agent.Email = txtEmail.Text.Trim();

                long rid = Convert.ToInt64(hfRemittanceID.Value);

                var rate = db.tblM_Travel_Agent_Rate.Where(p => p.TravelAgentID == agent.TravelAgentID && p.RemittanceID == rid).FirstOrDefault();
                if (hfRateID.Value != ddlRate.SelectedValue)
                {
                    tblH_Travel_Agent_Rate_Update hrate = new tblH_Travel_Agent_Rate_Update();
                    hrate.TravelAgentID = agent.TravelAgentID;
                    hrate.RateID = rate.ID;

                    hrate.PayloadBefore = JsonConvert.SerializeObject(rate);
                    rate.RateID = Int64.Parse(ddlRate.SelectedValue);
                    hrate.PayloadAfter = JsonConvert.SerializeObject(rate);
                    hrate.UpdatedDate = DateTime.Now;
                    hrate.UpdatedBy = Convert.ToInt64(SessionLib.Current.AdminID);

                    db.tblH_Travel_Agent_Rate_Update.Add(hrate);
                    result = "rate level updated to " + ddlRate.SelectedItem.Text + ", ";
                }

                if (hfFee.Value != txtFee.Text.Trim())
                {
                    tblH_Travel_Agent_Fee_Update hfee = new tblH_Travel_Agent_Fee_Update();
                    hfee.TravelAgentID = agent.TravelAgentID;
                    hfee.RateID = rate.ID;

                    var fee = db.tblM_Travel_Agent_Fee.Where(p => p.RateID == rate.ID && p.RemittanceID == rid).FirstOrDefault();
                    hfee.FeeID = fee.ID;
                    hfee.PayloadBefore = JsonConvert.SerializeObject(fee);
                    fee.Fee = Convert.ToDouble(txtFee.Text.Trim());
                    hfee.PayloadAfter = JsonConvert.SerializeObject(fee);
                    hfee.UpdatedDate = DateTime.Now;
                    hfee.UpdatedBy = Convert.ToInt64(SessionLib.Current.AdminID);

                    db.tblH_Travel_Agent_Fee_Update.Add(hfee);
                    result += "admin fee updated to IDR " + txtFee.Text;
                }

                db.SaveChanges();

                Session["SuccessTravelAgent"] = "Successfully updated for " + agent.Name + " : " + result;

                Response.Redirect("~/TravelAgent/ListTravelAgent");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + agent.Name + " updated')", true);
            }
            else
            {
                diverror.Visible = true;
                lblError.Text = "No travel agent found with this phone number";
            }
        }

        protected void btnError_ServerClick(object sender, EventArgs e)
        {
            diverror.Visible = false;
        }

        protected void ddlRate_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}