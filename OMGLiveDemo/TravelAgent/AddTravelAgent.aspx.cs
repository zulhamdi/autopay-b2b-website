﻿using Newtonsoft.Json;
using OMGLiveDemo.Models;
using OMGLiveDemo.Models.OMG;
using OMGLiveDemo.Scripts;
using Oppal.Sec;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.TravelAgent
{
    public partial class AddTravelAgent : System.Web.UI.Page
    {
        OMG omg = new OMG();
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    bind();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        void bind()
        {
            bindIDRtoMYR();
            bindIDRtoSAR();
        }
        
        void bindIDRtoMYR()
        {
            string rts = null;
            var cur = omg.getRateIDRMYR();
            if (cur != null)
            {
                rts = cur.Rate;
                
            }

            List<tblM_Rate> rate = new List<tblM_Rate>();
            rate = omg.getListRateIDRMYR();
            if (rate.Count > 0)
            {
                for (int i = 0; i < rate.Count; i++)
                {
                    rate[i].Rate = (Int32.Parse(rts) + Int32.Parse(rate[i].Margin)).ToString();
                    rate[i].NameRate = rate[i].Name + " - Rp " + String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(Int32.Parse(rate[i].Rate))).Replace("Rp", "");
                    if (i == 0)
                        rts = rate[i].Rate;
                }
                ddlRateIDRMYR.DataSource = null;
                ddlRateIDRMYR.DataSource = rate;
                ddlRateIDRMYR.DataValueField = "RateID";
                ddlRateIDRMYR.DataTextField = "NameRate";
                ddlRateIDRMYR.DataBind();
            }
        }

        void bindIDRtoSAR()
        {
            string rts = null;
            var cur = omg.getRateIDRSAR();
            if (cur != null)
            {
                rts = cur.Rate;
            }

            List<tblM_Rate> rate = new List<tblM_Rate>();
            rate = omg.getListRateIDRSAR();
            if (rate.Count > 0)
            {
                for (int i = 0; i < rate.Count; i++)
                {
                    rate[i].Rate = (Int32.Parse(rts) + Int32.Parse(rate[i].Margin)).ToString();
                    rate[i].NameRate = rate[i].Name + " - Rp " + String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(Int32.Parse(rate[i].Rate))).Replace("Rp", "");
                    if (i == 0)
                        rts = rate[i].Rate;
                }
                ddlRateIDRSAR.DataSource = null;
                ddlRateIDRSAR.DataSource = rate;
                ddlRateIDRSAR.DataValueField = "RateID";
                ddlRateIDRSAR.DataTextField = "NameRate";
                ddlRateIDRSAR.DataBind();
            }
        }

        bool checkData()
        {
            if (txtName.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Name cannot be empty!";
                return false;
            }
            if (txtUsername.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Username cannot be empty!";
                return false;
            }
            else
            {
                string us = txtUsername.Text.Trim().Replace(" ", "");
                var check = db.tblM_Travel_Agent.Where(p => p.Username == us).FirstOrDefault();
                if (check != null)
                {
                    diverror.Visible = true;
                    lblError.Text = "Username already exist. Please set other Username";
                    return false;
                }
            }

            if (txtPassword.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Password cannot be empty!";
                return false;
            }

            if (txtPhone.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Phone cannot be empty!";
                return false;
            }

            if (txtEmail.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Email cannot be empty!";
                return false;
            }

            if (txtNameInCharge.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Name of person in charge cannot be empty!";
                return false;
            }

            if (txtPhoneInCharge.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Phone of person in charge cannot be empty!";
                return false;
            }

            if (txtNationalID.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "KTP / Passport of person in charge cannot be empty!";
                return false;
            }

            if (!fuNationalID.HasFile)
            {
                lblError.Text = "Please choose photo of person in charge KTP / Passport first";
                diverror.Visible = true;
                return false;
            }
            return true;
        }

        bool checkFile()
        {
            if (txtTDP.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "TDP Number cannot be empty!";
                return false;
            }

            if (!fuTDP.HasFile)
            {
                lblError.Text = "Please choose photo of your TDP first";
                diverror.Visible = true;
                return false;
            }
            else
            {
                if (fuTDP.PostedFile.ContentType != "image/jpg" && fuTDP.PostedFile.ContentType != "image/jpeg" && fuTDP.PostedFile.ContentType != "image/png")
                {
                    lblError.Text = "TDP : Only jpeg, jpg and png format are allowed";
                    diverror.Visible = true;
                    return false;
                }
            }

            if (txtSKMenkumham.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "SK Menkumham Number cannot be empty!";
                return false;
            }

            if (!fuSKMenkumham.HasFile)
            {
                lblError.Text = "Please choose photo of your SK Menkumham first";
                diverror.Visible = true;
                return false;
            }
            else
            {
                if (fuSKMenkumham.PostedFile.ContentType != "image/jpg" && fuSKMenkumham.PostedFile.ContentType != "image/jpeg" && fuSKMenkumham.PostedFile.ContentType != "image/png")
                {
                    lblError.Text = "SK Menkumham : Only jpeg, jpg and png format are allowed";
                    diverror.Visible = true;
                    return false;
                }
            }

            if (txtNPWP.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "NPWP Number cannot be empty!";
                return false;
            }

            if (!fuNPWP.HasFile)
            {
                lblError.Text = "Please choose photo of your NPWP first";
                diverror.Visible = true;
                return false;
            }
            else
            {
                if (fuNPWP.PostedFile.ContentType != "image/jpg" && fuNPWP.PostedFile.ContentType != "image/jpeg" && fuNPWP.PostedFile.ContentType != "image/png")
                {
                    lblError.Text = "NPWP : Only jpeg, jpg and png format are allowed";
                    diverror.Visible = true;
                    return false;
                }
            }

            if (txtSIUP.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "SIUP Number cannot be empty!";
                return false;
            }

            if (!fuSIUP.HasFile)
            {
                lblError.Text = "Please choose photo of your SIUP first";
                diverror.Visible = true;
                return false;
            }
            else
            {
                if (fuSIUP.PostedFile.ContentType != "image/jpg" && fuSIUP.PostedFile.ContentType != "image/jpeg" && fuSIUP.PostedFile.ContentType != "image/png")
                {
                    lblError.Text = "SIUP : Only jpeg, jpg and png format are allowed";
                    diverror.Visible = true;
                    return false;
                }
            }
            return true;
        }

        bool checkRemittance()
        {
            if (ddlIDRtoMYR.SelectedValue == "no" && ddlIDRtoSAR.SelectedValue == "no")
            {
                diverror.Visible = true;
                lblError.Text = "You should choose minimum one remittance line to add master area!";
                return false;
            }
            

            if (ddlIDRtoMYR.SelectedValue == "yes")
            {
                if (Convert.ToDouble(txtFeeIDRMYR.Text) < 7500)
                {
                    diverror.Visible = true;
                    lblError.Text = "Minimum admin fee is IDR 7500!";
                    return false;
                }

                if (txtFeeIDRMYR.Text == "")
                {
                    diverror.Visible = true;
                    lblError.Text = "Admin fee cannot be empty!";
                    return false;
                }
            }

            if (ddlIDRtoSAR.SelectedValue == "yes")
            {
                if (Convert.ToDouble(txtFeeIDRMYR.Text) < 7500)
                {
                    diverror.Visible = true;
                    lblError.Text = "Minimum admin fee is IDR 7500!";
                    return false;
                }

                if (txtFeeIDRMYR.Text == "")
                {
                    diverror.Visible = true;
                    lblError.Text = "Admin fee cannot be empty!";
                    return false;
                }
            }
            return true;
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            if (!checkData()) return;
            if (!checkFile()) return;

            
            var adm = db.tblM_Admin.Where(p => p.Phone == txtPhone.Text.Trim() && p.isActive == 1).FirstOrDefault();
            var check = db.tblM_Agent.Where(p => p.Phone == txtPhone.Text.Trim() && p.isActive == 1).FirstOrDefault();
            var checkma = db.tblM_Master_Area.Where(p => p.Phone == txtPhone.Text.Trim() && p.isActive == 1).FirstOrDefault();
            var checkad = db.tblM_Admin.Where(p => p.Phone == txtPhone.Text.Trim() && p.isActive == 1).FirstOrDefault();
            var checkta = db.tblM_Travel_Agent.Where(p => p.Phone == txtPhone.Text.Trim() && p.isActive == 1).FirstOrDefault();
            if (check == null && checkma == null && adm == null && checkta == null)
            {
                long adminid = Convert.ToInt64(SessionLib.Current.AdminID);

                tblM_Travel_Agent bnf = new tblM_Travel_Agent();
                bnf.Name = txtName.Text.Trim();
                bnf.Username = txtUsername.Text.Trim().Replace(" ", "");
                bnf.Phone = txtPhone.Text.Trim();
                bnf.Password = Crypto.EncryptDB(txtPassword.Text.Trim());
                bnf.Email = txtEmail.Text;
                bnf.JoinDate = DateTime.Now;
                bnf.AddedBy = adminid;
                bnf.Email = txtEmail.Text.Trim();
                bnf.isActive = 1;

                bnf.ResponsibleName = txtNameInCharge.Text.Trim();
                bnf.ResponsiblePhone = txtPhoneInCharge.Text.Trim();

                if (fuNationalID.HasFile)
                {
                    bnf.ResponsibleKTP = txtNationalID.Text.Trim();
                    string filename = bnf.ResponsibleName.Replace(" ", "") + "_KTP_" + Common.getTimeSpan() + "." + fuNationalID.PostedFile.ContentType.Replace("image/", "");
                    string targetPath = Server.MapPath("../TravelAgent/Picture/KTP/" + filename);
                    Stream strm = fuNationalID.PostedFile.InputStream;
                    Common.SaveImage(0.5, strm, targetPath);
                    bnf.ResponsibleKTPURL = ConfigurationManager.AppSettings["URL_TRAVEL_AGENT_INDONESIA_KTP"].ToString() + filename;
                }

                if (fuSIUP.HasFile)
                {
                    bnf.SIUP = txtSIUP.Text.Trim();
                    string filename = bnf.Name.Replace(".", "-").Replace(" ", "") + "_SIUP_" + Common.getTimeSpan() + "." + fuSIUP.PostedFile.ContentType.Replace("image/", "");
                    string targetPath = Server.MapPath("../TravelAgent/Picture/SIUP/" + filename);
                    Stream strm = fuSIUP.PostedFile.InputStream;
                    Common.SaveImage(0.5, strm, targetPath);
                    bnf.SIUPURL = ConfigurationManager.AppSettings["URL_TRAVEL_AGENT_INDONESIA_SIUP"].ToString() + filename;
                }

                if (fuTDP.HasFile)
                {
                    bnf.TDP = txtTDP.Text.Trim();
                    string filename = bnf.Name.Replace(".", "-").Replace(" ", "") + "_TDP_" + Common.getTimeSpan() + "." + fuTDP.PostedFile.ContentType.Replace("image/", "");
                    string targetPath = Server.MapPath("../TravelAgent/Picture/TDP/" + filename);
                    Stream strm = fuTDP.PostedFile.InputStream;
                    Common.SaveImage(0.5, strm, targetPath);
                    bnf.TDPURL = ConfigurationManager.AppSettings["URL_TRAVEL_AGENT_INDONESIA_TDP"].ToString() + filename;
                }

                if (fuSKMenkumham.HasFile)
                {
                    bnf.SKMenkumham = txtSKMenkumham.Text.Trim();
                    string filename = bnf.Name.Replace(".", "-").Replace(" ", "") + "_SK_" + Common.getTimeSpan() + "." + fuSKMenkumham.PostedFile.ContentType.Replace("image/", "");
                    string targetPath = Server.MapPath("../TravelAgent/Picture/SK/" + filename);
                    Stream strm = fuSKMenkumham.PostedFile.InputStream;
                    Common.SaveImage(0.5, strm, targetPath);
                    bnf.SKMenkumhamURL = ConfigurationManager.AppSettings["URL_TRAVEL_AGENT_INDONESIA_SK"].ToString() + filename;
                }

                if (fuNPWP.HasFile)
                {
                    bnf.NPWP = txtNPWP.Text.Trim();
                    string filename = bnf.Name.Replace(".", "-").Replace(" ", "") + "_NPWP_" + Common.getTimeSpan() + "." + fuNPWP.PostedFile.ContentType.Replace("image/", "");
                    string targetPath = Server.MapPath("../TravelAgent/Picture/NPWP/" + filename);
                    Stream strm = fuNPWP.PostedFile.InputStream;
                    Common.SaveImage(0.5, strm, targetPath);
                    bnf.NPWPURL = ConfigurationManager.AppSettings["URL_TRAVEL_AGENT_INDONESIA_NPWP"].ToString() + filename;
                }

                db.tblM_Travel_Agent.Add(bnf);
                db.SaveChanges();

                if (ddlIDRtoMYR.SelectedValue == "yes")
                {
                    createIDRtoMYR(adminid, bnf);
                }
                if (ddlIDRtoSAR.SelectedValue == "yes")
                {
                    createIDRtoSAR(adminid, bnf);
                }

                // create bni va
                CreateVAModel vas = new CreateVAModel();
                vas.name = bnf.Name;
                vas.phone = bnf.Phone;
                vas.trxid = (db.tblM_Travel_Agent_VA.Count() + 1).ToString();
                var trx = omg.createBNIVA(vas);
                Console.WriteLine(trx);
                if (trx.status == "success")
                {
                    var result = JsonConvert.DeserializeObject<CreateVAResponse>(trx.data.ToString());
                    tblM_Travel_Agent_VA va = new tblM_Travel_Agent_VA();
                    va.VA = result.VA;
                    va.TravelAgentID = bnf.TravelAgentID;
                    va.CreatedDate = result.CreatedDate;
                    va.ExpiredDate = result.ExpiredDate;
                    va.BankCode = result.BankCode;
                    va.Name = result.Name;
                    db.tblM_Travel_Agent_VA.Add(va);
                    db.SaveChanges();
                }

                Response.Redirect("~/TravelAgent/ListTravelAgent");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + bnf.Name + " added to database')", true);
            }
            else
            {
                diverror.Visible = true;
                lblError.Text = "This phone number already registered";
            }
        }
        
        void createIDRtoMYR(long adminid, tblM_Travel_Agent bnf)
        {
            var check = db.tblM_Travel_Agent_Balance.Where(p => p.TravelAgentID == bnf.TravelAgentID && p.CountryID == 103).FirstOrDefault();
            if (check == null)
            {
                tblM_Travel_Agent_Balance bal = new tblM_Travel_Agent_Balance();
                bal.TravelAgentID = bnf.TravelAgentID;
                bal.CountryID = 103;
                bal.Currency = "IDR";
                bal.Balance = 0;
                bal.isActive = 1;
                bal.CreatedBy = adminid;
                bal.CreatedDate = DateTime.Now;
                db.tblM_Travel_Agent_Balance.Add(bal);
                db.SaveChanges();
            }

            tblM_Travel_Agent_Rate art = new tblM_Travel_Agent_Rate();
            art.TravelAgentID = bnf.TravelAgentID;
            art.RemittanceID = 2;
            art.RateID = Int64.Parse(ddlRateIDRMYR.SelectedValue);
            art.Margin = "0";
            art.CreatedDate = bnf.JoinDate;
            art.UpdatedDate = bnf.JoinDate;
            art.CreatedBy = bnf.AddedBy;
            db.tblM_Travel_Agent_Rate.Add(art);
            db.SaveChanges();

            tblM_Travel_Agent_Fee fee = new tblM_Travel_Agent_Fee();
            fee.TravelAgentID = bnf.TravelAgentID;
            fee.RateID = art.ID;
            fee.RemittanceID = 2;
            fee.Currency = "IDR";
            fee.Fee = float.Parse(txtFeeIDRMYR.Text);
            fee.isActive = 1;
            fee.DateCreated = DateTime.Now;
            db.tblM_Travel_Agent_Fee.Add(fee);
            db.SaveChanges();
        }

        void createIDRtoSAR(long adminid, tblM_Travel_Agent bnf)
        {
            var check = db.tblM_Travel_Agent_Balance.Where(p => p.TravelAgentID == bnf.TravelAgentID && p.CountryID == 103).FirstOrDefault();
            if (check == null)
            {
                tblM_Travel_Agent_Balance bal = new tblM_Travel_Agent_Balance();
                bal.TravelAgentID = bnf.TravelAgentID;
                bal.CountryID = 103;
                bal.Currency = "IDR";
                bal.Balance = 0;
                bal.isActive = 1;
                bal.CreatedBy = adminid;
                bal.CreatedDate = DateTime.Now;
                db.tblM_Travel_Agent_Balance.Add(bal);
                db.SaveChanges();
            }

            tblM_Travel_Agent_Rate art = new tblM_Travel_Agent_Rate();
            art.TravelAgentID = bnf.TravelAgentID;
            art.RemittanceID = 3;
            art.RateID = Int64.Parse(ddlRateIDRSAR.SelectedValue);
            art.Margin = "0";
            art.CreatedDate = bnf.JoinDate;
            art.UpdatedDate = bnf.JoinDate;
            art.CreatedBy = bnf.AddedBy;
            db.tblM_Travel_Agent_Rate.Add(art);
            db.SaveChanges();

            tblM_Travel_Agent_Fee fee = new tblM_Travel_Agent_Fee();
            fee.TravelAgentID = bnf.TravelAgentID;
            fee.RateID = art.ID;
            fee.RemittanceID = 3;
            fee.Currency = "IDR";
            fee.Fee = float.Parse(txtFeeIDRSAR.Text);
            fee.isActive = 1;
            fee.DateCreated = DateTime.Now;
            db.tblM_Travel_Agent_Fee.Add(fee);
            db.SaveChanges();
        }

        protected void btnError_ServerClick(object sender, EventArgs e)
        {
            diverror.Visible = false;
        }

        protected void ddlRate_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void ddlMYRtoIDR_SelectedIndexChanged(object sender, EventArgs e)
        {
            checkRate();
        }

        protected void ddlIDRtoMYR_SelectedIndexChanged(object sender, EventArgs e)
        {
            checkRate();
        }

        protected void ddlIDRtoSAR_SelectedIndexChanged(object sender, EventArgs e)
        {
            checkRate();
        }

        void checkRate()
        {
            if (ddlIDRtoMYR.SelectedValue == "yes")
                dividrtomyr.Visible = true;
            else
                dividrtomyr.Visible = false;

            if (ddlIDRtoSAR.SelectedValue == "yes")
                dividrtosar.Visible = true;
            else
                dividrtosar.Visible = false;
        }

        protected void ddlRateIDR_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void ddlRateIDRSAR_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}