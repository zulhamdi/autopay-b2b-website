﻿using OMGLiveDemo.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.TravelAgent
{
    public partial class ListTravelAgent : System.Web.UI.Page
    {
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    BindGV();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        void BindGV()
        {
            if (Session["SuccessTravelAgent"] != null)
            {
                lblAlertSuccess.Text = Session["SuccessTravelAgent"].ToString();
                divsuccess.Visible = true;
                Session["SuccessTravelAgent"] = null;
            }

            if (Session["ErrorTravelAgent"] != null)
            {
                lblAlertFailed.Text = Session["ErrorTravelAgent"].ToString();
                divfailed.Visible = true;
                Session["ErrorTravelAgent"] = null;
            }

            var rid = Convert.ToInt64(ddlRemittanceLine.SelectedValue);
            gvListItem.DataSource = null;
            var dt = db.vw_Travel_Agent.Where(p => p.RemittanceID == rid).ToList();
            for (int i = 0; i < dt.Count; i++)
            {
                if (dt[i].RemittanceID == 1)
                    dt[i].BalanceText = "IDR " + String.Format(new CultureInfo("id-ID"), "{0:n}", dt[i].Balance).Replace("Rp", "Rp ");
                else if (dt[i].RemittanceID == 2)
                    dt[i].BalanceText = "IDR " + String.Format(new CultureInfo("id-ID"), "{0:n}", dt[i].Balance).Replace("Rp", "Rp ");
                else if (dt[i].RemittanceID == 3)
                    dt[i].BalanceText = "IDR " + String.Format(new CultureInfo("id-ID"), "{0:n}", dt[i].Balance).Replace("Rp", "Rp ");
            }
            gvListItem.DataSource = dt;
            gvListItem.DataBind();
        }

        protected void btnAddAgent_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddTravelAgent");
        }

        protected void lblEdit_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            Session["TravelAgentID"] = ((HiddenField)row.FindControl("hfTravelAgentID")).Value;
            Session["RemittanceID"] = ((HiddenField)row.FindControl("hfRemittanceID")).Value;
            Session["RateLevel"] = row.Cells[6].Text;
            Response.Redirect("UpdateTravelAgent");
        }

        protected void btnAlertFailed_ServerClick(object sender, EventArgs e)
        {
            divfailed.Visible = false;
        }

        protected void btnAlertSuccess_ServerClick(object sender, EventArgs e)
        {
            divsuccess.Visible = false;
        }

        protected void ddlRemittanceLine_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGV();
        }
    }
}