﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OMGLiveDemo.Models.OMG
{
    public class CreateVAModel
    {
        public string name { get; set; }
        public string phone { get; set; }
        public string trxid { get; set; }
    }
}