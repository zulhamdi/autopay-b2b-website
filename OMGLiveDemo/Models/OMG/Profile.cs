﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OMGLiveDemo.Models.OMG
{
    public class Profile
    {
        public long PartnerID { get; set; }
        public string Name { get; set; }
        public Nullable<long> CountryID { get; set; }
        public string Country { get; set; }
        public string Username { get; set; }
        //public string Password { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public Nullable<System.DateTime> DateJoin { get; set; }
        //public Nullable<int> isActive { get; set; }
        //public Nullable<System.DateTime> DateActivated { get; set; }
        //public Nullable<int> isBanned { get; set; }
        public string ImageURL { get; set; }
        public string ThumbnailURL { get; set; }
        public string CallbackURL { get; set; }
    }
}