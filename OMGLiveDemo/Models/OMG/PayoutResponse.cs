﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OMGLiveDemo.Models.OMG
{
    public class PayoutResponse
    {
        public string ReferenceID { get; set; }
        public string PartnerReferenceID { get; set; }
        public string BeneficiaryName { get; set; }
        public string BeneficiaryAccount { get; set; }
        public string BeneficiaryBank { get; set; }
        public string BeneficiaryEmail { get; set; }
        public string Currency { get; set; }
        public string Amount { get; set; }
        public string AmountIDR { get; set; }
        public string Notes { get; set; }
        public Nullable<long> BankID { get; set; }
        public string Status { get; set; }
        public DateTime DateModified { get; set; }
    }
}