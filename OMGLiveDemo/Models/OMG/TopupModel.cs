﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OMGLiveDemo.Models.OMG
{
    public class TopupModel
    {
        public long PartnerID { get; set; }
        public double Amount { get; set; }
        public long BankAccountID { get; set; }
        public string Notes { get; set; }
        public long CreatedBy { get; set; }
        public string Currency { get; set; }
        public string VA { get; set; }
        public string VAName { get; set; }
    }
}