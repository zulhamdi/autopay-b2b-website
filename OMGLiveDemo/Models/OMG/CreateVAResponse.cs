﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OMGLiveDemo.Models.OMG
{
    public class CreateVAResponse
    {
        public long ID { get; set; }
        public Nullable<long> PartnerID { get; set; }
        public string Reference { get; set; }
        public string VA { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string BankCode { get; set; }
        public Nullable<int> isActive { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ExpiredDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string Payload { get; set; }
        public string Param1 { get; set; }
        public string Param2 { get; set; }
        public string Param3 { get; set; }
    }
}