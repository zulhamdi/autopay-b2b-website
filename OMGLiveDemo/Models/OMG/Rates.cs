﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OMGLiveDemo.Models.OMG
{
    public class UpdateRate
    {
        public int RateID { get; set; }
        public string RateBase { get; set; }

        public string Margin = "0";
        public string Rate { get; set; }
        public int ModifiedBy { get; set; }
    }
}