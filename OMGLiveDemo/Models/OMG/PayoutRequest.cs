﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OMGLiveDemo.Models.OMG
{
    public class PayoutRequest
    {
        public string PartnerReferenceID { get; set; }
        public long RemittanceID { get; set; }
        public Nullable<long> BankID { get; set; }
        public string BeneficiaryName { get; set; }
        public string BeneficiaryAccount { get; set; }
        public string BeneficiaryBank { get; set; }
        public string BeneficiaryEmail { get; set; }
        public string Currency { get; set; }
        public string Amount { get; set; }
        public string AmountIDR { get; set; }
        public string Notes { get; set; }
        public Beneficiary Beneficiary { get; set; }
        public Sender Sender { get; set; }
    }
}