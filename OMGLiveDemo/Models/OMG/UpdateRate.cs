﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OMGLiveDemo.Models.OMG
{
    public class Rates
    {
        //public string Rate { get; set; }
        //public string Currency { get; set; }
        //public DateTime DateModified { get; set; }

        public long RateID { get; set; }
        public Nullable<long> RemittanceID { get; set; }
        public string Rate { get; set; }
        public string Currency { get; set; }
        public DateTime DateModified { get; set; }
    }
}