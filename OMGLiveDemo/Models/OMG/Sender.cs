﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OMGLiveDemo.Models.OMG
{
    public class Sender
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Identity { get; set; }

        public int IdentityTypeID = 1;

        public string IdentityType = "CITIZEN PERSONAL ID";
    }
}