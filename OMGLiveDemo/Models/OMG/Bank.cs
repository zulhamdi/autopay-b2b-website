﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OMGLiveDemo.Models.OMG
{
    public class Bank
    {
        public long BankID { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public int isRealtime { get; set; }
    }
}