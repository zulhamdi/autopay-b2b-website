﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;

namespace OMGLiveDemo.Models
{
    public class Image64
    {
        public string ImageToBase64(Stream xxx)
        {
            // string path = "D:\\SampleImage.jpg";
            string base64String = "null";
            //using (System.Drawing.Image image = System.Drawing.Image.FromStream(fuMiniImage.PostedFile.InputStream))
            using (System.Drawing.Image image = System.Drawing.Image.FromStream(xxx))
            {
                using (MemoryStream m = new MemoryStream())
                {
                    image.Save(m, image.RawFormat);
                    byte[] imageBytes = m.ToArray();
                    base64String = Convert.ToBase64String(imageBytes);
                    return base64String;
                }
            }
        }

        public string ImageToBase64Compress(Stream xxx)
        {
            string base64String = "null";
            using (System.Drawing.Image image = System.Drawing.Image.FromStream(xxx))
            {
                int imageHeight = image.Height;
                int imageWidth = image.Width;
                int maxHeight = imageHeight / 2;
                int maxWidth = imageWidth / 2;
                imageHeight = (imageHeight * maxWidth) / imageWidth;
                imageWidth = maxWidth;
                if (imageHeight > maxHeight)
                {
                    imageWidth = (imageWidth * maxHeight) / imageHeight;
                    imageHeight = maxHeight;
                }
                Bitmap bitmap = new Bitmap(image, imageWidth, imageHeight);
                using (MemoryStream m = new MemoryStream())
                {
                    bitmap.Save(m, System.Drawing.Imaging.ImageFormat.Jpeg);
                    m.Position = 0;
                    byte[] newimage = new byte[m.Length + 1];
                    m.Read(newimage, 0, newimage.Length);
                    return base64String = Convert.ToBase64String(newimage);
                }
            }
        }

        //public Image Base64toImage(string base64)
        //{
        //    // Convert Base64 String to byte[]
        //    byte[] imageBytes = Convert.FromBase64String(base64);
        //    MemoryStream ms = new MemoryStream(imageBytes, 0,
        //      imageBytes.Length);

        //    // Convert byte[] to Image
        //    ms.Write(imageBytes, 0, imageBytes.Length);
        //    Image image = Image.FromStream(ms, true);
        //    return image;
        //}
    }
}