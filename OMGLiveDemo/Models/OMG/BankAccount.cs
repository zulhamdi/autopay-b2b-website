﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OMGLiveDemo.Models.OMG
{
    public class BankAccount
    {
        public long CountryID { get; set; }
        public string AccountName { get; set; }
        public string AccountNo { get; set; }
        public string BankCode { get; set; }
    }
}