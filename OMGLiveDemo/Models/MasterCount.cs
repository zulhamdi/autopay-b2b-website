﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OMGLiveDemo.Models
{
    public class MasterCount
    {
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();

        long getId() {
            return Convert.ToInt64(SessionLib.Current.AdminID);
        }

        public int PayoutAgentCount(string status)
        {
            var id = getId();
            return db.tblT_Payout.Where(p => p.isAgent == 1 && p.CreatedBy == id && p.Status == status).Count();
        }

        public int PayoutMasterAreaCount(string status)
        {
            var id = getId();
            return db.tblT_Payout.Where(p => p.isAgent == 2 && p.CreatedBy == id && p.Status == status).Count();
        }

        public int PayoutTravelAgentCount(string status)
        {
            var id = getId();
            return db.tblT_Payout.Where(p => p.isAgent == 3 && p.CreatedBy == id && p.Status == status).Count();
        }

        public int PayoutAdminCount(string status)
        {
            return db.tblT_Payout.Where(p => p.isAgent == 0 && p.Status == status).Count();
        }

        public int PayoutAllCount(string status)
        {
            var id = getId();
            return db.tblT_Payout.Where(p => p.Status == status).Count();
        }

        public double AgentBalance(long id)
        {
            return db.tblM_Agent_Balance.Where(p => p.AgentID == id).Select(p => p.Balance).FirstOrDefault().Value;
        }

        public double MasterAreaBalance(long id)
        {
            return db.tblM_Master_Area_Balance.Where(p => p.MasterAreaID == id).Select(p => p.Balance).FirstOrDefault().Value;
        }

        public int SenderAllPending()
        {
            return db.tblM_Sender.Where(p => p.Status == "created" || p.Status == "recreated").Count();
        }

        public int SenderAllAmend()
        {
            return db.tblM_Sender.Where(p => p.Status == "amend").Count();
        }

        public int SenderAllRejected()
        {
            return db.tblM_Sender.Where(p => p.Status == "rejected").Count();
        }
        public int SenderAllApproved()
        {
            return db.tblM_Sender.Where(p => p.Status == "approved").Count();
        }

        public int BeneficiaryAllPending()
        {
            return db.tblM_Beneficiary.Where(p => p.Status == "created" || p.Status == "recreated").Count();
        }

        public int BeneficiaryAllAmend()
        {
            return db.tblM_Beneficiary.Where(p => p.Status == "amend").Count();
        }
        public int BeneficiaryAllRejected()
        {
            return db.tblM_Beneficiary.Where(p => p.Status == "rejected").Count();
        }
        public int BeneficiaryAllApproved()
        {
            return db.tblM_Beneficiary.Where(p => p.Status == "approved").Count();
        }

        public int TopupMAAllPending()
        {
            return db.tblT_Topup.Where(p => p.UserType == "MA" && (p.Status == "created" || p.Status == "recreated")).Count();
        }

        public int TopupTAAllPending()
        {
            return db.tblT_Topup.Where(p => p.UserType == "TA" && (p.Status == "created" || p.Status == "recreated")).Count();
        }

        public List<vw_Master_Area_Rate> getMasterAreaRemittanceList(long id)
        {
            return db.vw_Master_Area_Rate.Where(p => p.MasterAreaID == id).ToList();
        }

        public List<vw_Travel_Agent_Rate> getTravelAgentRemittanceList(long id)
        {
            return db.vw_Travel_Agent_Rate.Where(p => p.TravelAgentID == id).ToList();
        }

        public List<vw_Agent_Rate> getAgentRemittanceList(long id)
        {
            return db.vw_Agent_Rate.Where(p => p.AgentID == id).ToList();
        }
    }
}