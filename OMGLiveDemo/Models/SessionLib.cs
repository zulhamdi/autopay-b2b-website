﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OMGLiveDemo.Models
{
    public class SessionLib
    {
        // private constructor
        private SessionLib()
        {
            AdminID = string.Empty;
            Name = string.Empty;
            Email = string.Empty;
            HP = string.Empty;
            Role = string.Empty;
            Password = string.Empty;
        }
        public static SessionLib Current
        {
            get
            {
                SessionLib session =
                  (SessionLib)HttpContext.Current.Session["__MySession__"];
                if (session == null)
                {
                    session = new SessionLib();
                    HttpContext.Current.Session["__MySession__"] = session;
                }
                return session;
            }
        }
        public string AdminID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string HP { get; set; }
        public string Role { get; set; }
        public string Password { get; set; }
    }
}