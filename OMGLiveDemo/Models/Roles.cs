﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OMGLiveDemo.Models
{
    public class Roles
    {
        public const string SuperAdmin = "Super Admin";
        public const string Admin = "Admin";
        public const string Operator = "Operator";
        public const string Checker = "Checker";
        public const string Approver = "Approver";
        public const string Finance = "Finance";
        public const string Agent = "Agent";
        public const string MasterArea = "Master Area";
        public const string TravelAgent = "Travel Agent";
        public const string IT = "IT";
    }
}