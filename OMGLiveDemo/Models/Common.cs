﻿using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using Oppal.Sec;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.Models
{
    public class Common
    {
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();
        public bool checkLogin(string ph, string passw)
        {
            try
            {
                passw = Crypto.EncryptDB(passw);
                var admin = db.vw_Admin_Role.Where(p => p.Phone == ph && p.Password == passw && p.isActive == 1).FirstOrDefault();
                if (admin != null)
                {
                    SessionLib.Current.AdminID = admin.AdminID.ToString();
                    SessionLib.Current.Name = admin.Fullname.ToString();
                    SessionLib.Current.Email = admin.Email.ToString();
                    SessionLib.Current.HP = admin.Phone.ToString();
                    SessionLib.Current.Role = admin.Role.ToString();
                    SessionLib.Current.Password = passw;
                    return true;
                }
                else
                {
                    var agent = db.tblM_Agent.Where(p => p.Phone == ph && p.Password == passw && p.isActive == 1).FirstOrDefault();
                    if (agent != null)
                    {
                        SessionLib.Current.AdminID = agent.AgentID.ToString();
                        SessionLib.Current.Name = agent.Fullname.ToString();
                        SessionLib.Current.Email = agent.Email.ToString();
                        SessionLib.Current.HP = agent.Phone.ToString();
                        SessionLib.Current.Role = Roles.Agent;
                        SessionLib.Current.Password = passw;
                        return true;
                    }
                    else
                    {
                        var ma = db.tblM_Master_Area.Where(p => p.Phone == ph && p.Password == passw && p.isActive == 1).FirstOrDefault();
                        if (ma != null)
                        {
                            SessionLib.Current.AdminID = ma.MasterAreaID.ToString();
                            SessionLib.Current.Name = ma.Fullname.ToString();
                            SessionLib.Current.Email = ma.Email.ToString();
                            SessionLib.Current.HP = ma.Phone.ToString();
                            SessionLib.Current.Role = Roles.MasterArea;
                            SessionLib.Current.Password = passw;
                            return true;
                        }
                        else
                        {
                            var ta = db.tblM_Travel_Agent.Where(p => (p.Username == ph || p.Phone == ph) && p.Password == passw && p.isActive == 1).FirstOrDefault();
                            if (ta != null)
                            {
                                SessionLib.Current.AdminID = ta.TravelAgentID.ToString();
                                SessionLib.Current.Name = ta.Name.ToString();
                                SessionLib.Current.Email = ta.Email.ToString();
                                SessionLib.Current.HP = ta.Phone.ToString();
                                SessionLib.Current.Role = Roles.TravelAgent;
                                SessionLib.Current.Password = passw;
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public class Admin
        {
            public long Id { get; set; }
            public string Fullname { get; set; }
            public string Email { get; set; }
            public string Phone { get; set; }
            public string Role { get; set; }
            public int isActive { get; set; }
        }

        public static bool allowedPage(string page)
        {
            if (SessionLib.Current.Role == Roles.SuperAdmin || SessionLib.Current.Role == Roles.Admin)
            {
                string[] sa = new string[] {
                    "UpdateTravelAgent",
                    "ListTravelAgent",
                    "AddTravelAgent",
                    "PPATK",
                    "BankIndonesia",
                    "MalaysiaPayout",
                    "AddMalaysianBeneficiaries",
                    "AddIndonesianSender",
                    "Settlement",
                    "ApproveTopupRequest",
                    "PendingTopupRequest",
                    "TopupHistory",
                    "TopupRequest",
                    "UpdateMasterArea",
                    "ListMasterArea",
                    "AddMasterArea",
                    "AddBankAccount",
                    "BankAccount",
                    "Receipt",
                    "_Default",
                    "Rate",
                    "RateHistory",
                    "CreatePayout",
                    "PayoutHistory",
                    "AddBeneficiaries",
                    "BeneficiariesList",
                    "Notification",
                    "Receipt",
                    "Admin",
                    "AddAdmin",
                    "UpdateAdmin",
                    "ListAgent",
                    "AddAgent",
                    "UpdateAgent",
                    "UpdatePassword",
                    "WaitingForReceipt",
                    "PendingTask",
                    "CustomerNote",
                    "ApproveOrReject",
                    "IndonesiaPayout",
                    "AdministrationFee",
                    "AddMalaysianSender",
                    "AddIndonesianBeneficiaries",
                    "SenderList",
                    "ViewAuditLog",
                    "UmrahHajj",
                    "OFAC",
                    "UNSanction",
                    "PEP",
                    "TransactionDetail",
                    "BeneficiaryDetail",
                    "SenderDetail",
                    "DirectorDetail",
                    "ShareholderDetail"
                };
                if (sa.Any(s => page.Contains(s)))
                    return true;
                else
                    return false;
            }
            else if (SessionLib.Current.Role == Roles.Agent)
            {
                string[] sa = new string[] {
                    "MalaysiaPayout",
                    "AddMalaysianBeneficiaries",
                    "AddIndonesianSender",
                    "BalanceHistory",
                    "Receipt",
                    "AgentDashboard",
                    "AddBeneficiaries",
                    "BeneficiariesList",
                    "RateAgent",
                    "RateHistory",
                    "UpdatePassword",
                    "CustomerNote",
                    "WaitingForReceipt",
                    "Calculator",
                    "AddMalaysianSender",
                    "AddIndonesianBeneficiaries",
                    "SenderList",
                    "IndonesiaPayout",
                    "PayoutHistory",
                    "WaitingForReceipt",
                    "UploadReceipt",
                    "TransactionDetail",
                    "BeneficiaryDetail",
                    "SenderDetail",
                    "DirectorDetail",
                    "ShareholderDetail"
                };
                if (sa.Any(s => page.Contains(s)))
                    return true;
                else
                    return false;
            }
            else if (SessionLib.Current.Role == Roles.MasterArea)
            {
                string[] sa = new string[] {
                    "MalaysiaPayout",
                    "AddMalaysianBeneficiaries",
                    "AddIndonesianSender",
                    "Settlement",
                    "TopupHistory",
                    "RequestTopup",
                    "BalanceHistory",
                    "Receipt",
                    "AgentDashboard",
                    "AddBeneficiaries",
                    "BeneficiariesList",
                    "RateAgent",
                    "RateHistory",
                    "UpdatePassword",
                    "WaitingForReceipt",
                    "Calculator",
                    "AddMalaysianSender",
                    "AddIndonesianBeneficiaries",
                    "SenderList",
                    "IndonesiaPayout",
                    "PayoutHistory",
                    "WaitingForReceipt",
                    "CustomerNote",
                    "UploadReceipt",
                    "TransactionDetail",
                    "BeneficiaryDetail",
                    "SenderDetail",
                    "DirectorDetail",
                    "ShareholderDetail"
                };
                if (sa.Any(s => page.Contains(s)))
                    return true;
                else
                    return false;
            }
            else if (SessionLib.Current.Role == Roles.TravelAgent)
            {
                string[] sa = new string[] {
                    "SaudiArabiaPayout",
                    "MalaysiaPayout",
                    "AddMalaysianBeneficiaries",
                    "AddIndonesianSender",
                    "Settlement",
                    "TopupHistory",
                    "RequestTopup",
                    "BalanceHistory",
                    "Receipt",
                    "AgentDashboard",
                    "AddBeneficiaries",
                    "BeneficiariesList",
                    "RateAgent",
                    "RateHistory",
                    "UpdatePassword",
                    "WaitingForReceipt",
                    "Calculator",
                    "AddMalaysianSender",
                    "AddIndonesianBeneficiaries",
                    "SenderList",
                    "IndonesiaPayout",
                    "PayoutHistory",
                    "WaitingForReceipt",
                    "CustomerNote",
                    "UploadReceipt",
                    "TransactionDetail",
                    "BeneficiaryDetail",
                    "SenderDetail",
                    "DirectorDetail",
                    "ShareholderDetail"
                };
                if (sa.Any(s => page.Contains(s)))
                    return true;
                else
                    return false;
            }
            else if (SessionLib.Current.Role == Roles.Operator)
            {
                string[] sa = new string[] {
                    "OperatorDashboard",
                    "AddIndonesianSender",
                    "AddMalaysianSender",
                    "SenderList",
                    "Receipt",
                    "AddBeneficiaries",
                    "AddIndonesianBeneficiaries",
                    "AddMalaysianBeneficiaries",
                    "BeneficiariesList",
                    "UpdatePassword",
                    "Calculator",
                    "IndonesiaPayout",
                    "MalaysiaPayout",
                    "PendingTask",
                    "CustomerNote",
                    "PayoutHistory",
                    "Rate",
                    "RateHistory",
                    "TransactionDetail",
                    "BeneficiaryDetail",
                    "SenderDetail",
                    "DirectorDetail",
                    "ShareholderDetail",
                    "UmrahHajj",
                    "OFAC",
                    "UNSanction",
                    "PEP"
                };
                if (sa.Any(s => page.Contains(s)))
                    return true;
                else
                    return false;
            }
            else if (SessionLib.Current.Role == Roles.Checker)
            {
                string[] sa = new string[] {
                    "CheckerDashboard",
                    "AddIndonesianSender",
                    "AddMalaysianSender",
                    "SenderList",
                    "ApproveSender",
                    "ApproveSenderList",
                    "PendingSenderList",
                    "AddBeneficiaries",
                    "AddIndonesianBeneficiaries",
                    "AddMalaysianBeneficiaries",
                    "ApproveBeneficiaryList",
                    "ApproveBeneficiary",
                    "BeneficiariesList",
                    "PayoutHistory",
                    "UpdatePassword",
                    "Calculator",
                    "Rate",
                    "UmrahHajj",
                    "OFAC",
                    "UNSanction",
                    "PEP",
                    "TransactionDetail",
                    "BeneficiaryDetail",
                    "SenderDetail",
                    "DirectorDetail",
                    "ShareholderDetail"
                };
                if (sa.Any(s => page.Contains(s)))
                    return true;
                else
                    return false;
            }
            else if (SessionLib.Current.Role == Roles.Approver)
            {
                string[] sa = new string[] {
                    "ApproverDashboard",
                    "ApproveSenderList",
                    "SenderList",
                    "ApproveBeneficiaryList",
                    "BeneficiariesList",
                    "PendingTask",
                    "ApproveOrReject",
                    "Receipt",
                    "ViewImage",
                    "PayoutHistory",
                    "ApproveTopupRequest",
                    "PendingTopupRequest",
                    "BankIndonesia",
                    "PPATK",
                    "Calculator",
                    "Rate",
                    "RateHistory",
                    "UpdatePassword",
                    "TransactionDetail",
                    "BeneficiaryDetail",
                    "SenderDetail",
                    "DirectorDetail",
                    "ShareholderDetail",
                    "UmrahHajj",
                    "OFAC",
                    "UNSanction",
                    "PEP",
                    "ReleaseAryadana",
                    "CustomerNote",
                };

                if (sa.Any(s => page.Contains(s)))
                    return true;
                else
                    return false;
            }
            else if (SessionLib.Current.Role == String.Empty)
                return false;
            return false;
        }

        public static void SaveImage(double scaleFactor, Stream sourcePath, string targetPath)
        {
            using (var image = System.Drawing.Image.FromStream(sourcePath))
            {
                var newWidth = (int)(image.Width * scaleFactor);
                var newHeight = (int)(image.Height * scaleFactor);
                var thumbnailImg = new Bitmap(newWidth, newHeight);
                var thumbGraph = Graphics.FromImage(thumbnailImg);
                thumbGraph.CompositingQuality = CompositingQuality.HighQuality;
                thumbGraph.SmoothingMode = SmoothingMode.HighQuality;
                thumbGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;
                var imageRectangle = new Rectangle(0, 0, newWidth, newHeight);
                thumbGraph.DrawImage(image, imageRectangle);
                thumbnailImg.Save(targetPath, image.RawFormat);
            }
        }

        public static string getTimeSpan()
        {
            var sd = DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + DateTime.Now.Hour + DateTime.Now.Minute + DateTime.Now.Second + DateTime.Now.Millisecond + "";
            return sd;
        }

        public static void ExportExcel(GridView gv, string title)
        {
            IWorkbook workbook = new XSSFWorkbook();

            ISheet sheet1 = workbook.CreateSheet("Sheet 1");

            //make a header row
            IRow row1 = sheet1.CreateRow(0);
            row1.Height = (short)600;

            // create font style
            XSSFFont myFont = (XSSFFont)workbook.CreateFont();
            myFont.FontHeightInPoints = (short)13;
            //myFont.FontName = "Tahoma";
            myFont.IsBold = true;

            // create bordered cell style
            XSSFCellStyle headerStyle = (XSSFCellStyle)workbook.CreateCellStyle();
            headerStyle.SetFont(myFont);
            headerStyle.BorderLeft = NPOI.SS.UserModel.BorderStyle.Medium;
            headerStyle.BorderTop = NPOI.SS.UserModel.BorderStyle.Medium;
            headerStyle.BorderRight = NPOI.SS.UserModel.BorderStyle.Medium;
            headerStyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.Medium;
            headerStyle.Alignment = HorizontalAlignment.Left;
            headerStyle.VerticalAlignment = VerticalAlignment.Center;

            // create bordered cell style
            XSSFCellStyle valueStyle = (XSSFCellStyle)workbook.CreateCellStyle();
            valueStyle.BorderLeft = NPOI.SS.UserModel.BorderStyle.Medium;
            valueStyle.BorderTop = NPOI.SS.UserModel.BorderStyle.Medium;
            valueStyle.BorderRight = NPOI.SS.UserModel.BorderStyle.Medium;
            valueStyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.Medium;
            valueStyle.Alignment = HorizontalAlignment.Left;
            valueStyle.VerticalAlignment = VerticalAlignment.Center;


            int[] maxNumCharacters = new int[gv.Columns.Count];

            for (int j = 0; j < gv.Columns.Count; j++)
            {
                if (j == 0)
                    maxNumCharacters[j] = 0;

                ICell cell = row1.CreateCell(j);
                String columnName = gv.Columns[j].ToString();
                cell.SetCellValue(columnName);
                cell.CellStyle = headerStyle;

                if (columnName.Count() > maxNumCharacters[j])
                    maxNumCharacters[j] = columnName.Count();
            }


            //loops through data
            for (int i = 0; i < gv.Rows.Count; i++)
            {
                IRow row = sheet1.CreateRow(i + 1);
                row.Height = (short)400;
                for (int j = 0; j < gv.Columns.Count; j++)
                {
                    ICell cell = row.CreateCell(j);
                    String columnName = gv.Columns[j].ToString();
                    if (j == 0)
                        cell.SetCellValue((i + 1).ToString());
                    else
                        cell.SetCellValue(gv.Rows[i].Cells[j].Text.ToString().Replace("&nbsp;", ""));
                    cell.CellStyle = valueStyle;

                    if (gv.Rows[i].Cells[j].Text.Count() > maxNumCharacters[j])
                        maxNumCharacters[j] = gv.Rows[i].Cells[j].Text.Count();
                }
            }

            for (int i = 0; i < maxNumCharacters.Count(); i++)
            {
                int width = ((int)(maxNumCharacters[i] * 1.3)) * 256;
                sheet1.SetColumnWidth(i, width);
            }

            using (var exportData = new MemoryStream())
            {
                System.Web.HttpContext.Current.Response.Clear();
                workbook.Write(exportData);
                System.Web.HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                System.Web.HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", title+".xlsx"));
                System.Web.HttpContext.Current.Response.BinaryWrite(exportData.ToArray());
                System.Web.HttpContext.Current.Response.End();
            }
        }
    }
}