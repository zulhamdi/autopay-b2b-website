﻿using OMGLiveDemo.Models;
using OMGLiveDemo.Scripts;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.Report
{
    public partial class PPATK : System.Web.UI.Page
    {
        OMG omg = new OMG();
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    bindMonthYear();
                    getFirstMonthYear();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        protected void btnAction_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            var refs = ((HiddenField)row.FindControl("hfPayoutID")).Value;

            Session["PayoutID"] = refs;
            Response.Redirect("Payout/ApproveOrReject");
        }

        DateTime getFirstDate(int mon, int year)
        {
            return new DateTime(year, mon, 1);
        }

        DateTime getLastDate(DateTime fd)
        {
            return fd.AddMonths(1).AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59);
        }

        void getFirstMonthYear()
        {
            var date = DateTime.Now;
            var mon = date.Month;
            var year = date.Year;

            ddlMonth.SelectedValue = mon.ToString();
            ddlYear.SelectedItem.Text = year.ToString();

            var firstdate = getFirstDate(mon, year);
            var lastdate = getLastDate(firstdate);

            bind(firstdate, lastdate);
        }

        void getMonthYear()
        {
            var firstdate = getFirstDate(Convert.ToInt32(ddlMonth.SelectedValue), Convert.ToInt32(ddlYear.SelectedValue));
            var lastdate = getLastDate(firstdate);

            bind(firstdate, lastdate);
        }

        void bind(DateTime fd, DateTime ld)
        {
            if (ddlRemittanceLine.SelectedValue == "1")
            {
                
                var data = db.vw_Payout.Where(p => p.RemittanceID == 1 && p.Status == "completed" && p.CreatedDate >= fd && p.CreatedDate <= ld)
                    .Select(p =>
                     new
                     {
                         LocalID = "",
                         LTKL = "",
                         RemittanceID = 1,
                         ReportType = 1,
                         PJK = 1,
                         CountryRegion = 34,
                         CountryFrom = 1,
                         SenderAccount = "8001489612",
                         SenderBankName = "CIMB",
                         SenderName = "AKBAR MONEY CHANGER. SDN BHD",
                         SourceOfFund = 1,
                         SenderAddress = "No. 19, Aras G, Wisma Prowara, Jalan Melaka, 50100 Kuala Lumpur.",
                         BeneficiaryIdentityType = 9,
                         BeneficiaryIdentity = "3601340508830000",
                         BeneficiaryBank = p.BankCode.ToUpper(),
                         BeneficiaryAccount = p.Account,
                         BeneficiaryName = "MM INDONESIA",
                         BeneficiaryAddress = "Indonesia",
                         BeneficiaryProvince = 31,
                         BeneficiaryCity = "3171",
                         TransactionDate = p.CreatedDate,
                         ModifiedDate = p.ModifiedDate,
                         CurrencyFrom = 13,
                         AmountFrom = p.Amount,
                         CurrencyTo = 74,
                         AmountTo = p.TotalTransfer,
                         Rate = p.Rate,
                         Purpose = p.Purpose,
                         SourceOfFundTo = "HASIL USAHA",
                     })
                    .ToList();

                gvListItem.DataSource = data;
                gvListItem.DataBind();
            }
            else if (ddlRemittanceLine.SelectedValue == "2")
            {
                var data = db.vw_Payout.Where(p => p.RemittanceID == 2 && p.Status == "completed" && p.CreatedDate >= fd && p.CreatedDate <= ld)
                    .Select(p =>
                     new
                     {
                         LocalID = "",
                         LTKL = "",
                         RemittanceID = 2,
                         ReportType = 1,
                         PJK = 1,
                         CountryRegion = 31,
                         CountryFrom = 1,
                         SenderAccount = p.Account,
                         SenderBankName = p.BankCode.ToUpper(),
                         SenderName = "MM Indonesia",
                         SourceOfFund = 1,
                         SenderAddress = "Indonesia",
                         BeneficiaryIdentityType = 9,
                         BeneficiaryIdentity = "3601340508830000",
                         BeneficiaryBank = "CIMB",
                         BeneficiaryAccount = "8001489612",
                         BeneficiaryName = "MMAKBAR MONEY CHANGER. SDN BHD",
                         BeneficiaryAddress = "No. 19, Aras G, Wisma Prowara, Jalan Melaka, 50100 Kuala Lumpur.",
                         BeneficiaryProvince = 34,
                         BeneficiaryCity = "3171",
                         TransactionDate = p.CreatedDate,
                         ModifiedDate = p.ModifiedDate,
                         CurrencyFrom = 74,
                         AmountFrom = p.TotalTransfer,
                         CurrencyTo = 13,
                         AmountTo = p.Amount,
                         Rate = p.Rate,
                         Purpose = p.Purpose,
                         SourceOfFundTo = "HASIL USAHA",
                     })
                    .ToList();

                gvListItem.DataSource = data;
                gvListItem.DataBind();
            }
        }

        void bindMonthYear()
        {
            var mon = db.tblM_Month.ToList();
            ddlMonth.DataSource = null;
            ddlMonth.DataSource = mon;
            ddlMonth.DataValueField = "Value";
            ddlMonth.DataTextField = "Name";
            ddlMonth.DataBind();

            var yea = db.tblM_Year.ToList();
            ddlYear.DataSource = null;
            ddlYear.DataSource = yea;
            ddlYear.DataValueField = "Value";
            ddlYear.DataTextField = "Name";
            ddlYear.DataBind();
        }

        protected void btnReceipt_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            Session["PayoutID"] = ((HiddenField)row.FindControl("hfPayoutID")).Value;
            Page.ClientScript.RegisterStartupScript(this.GetType(), "OpenWindow", "window.open('/Payout/Receipt','_newtab');", true);
        }

        void RedirectToTransferHistory(string status)
        {
            Session["PayoutHistoryStatus"] = status;
            Response.Redirect("Payout/PayoutHistory");
        }
        protected void btnView_Click(object sender, EventArgs e)
        {

        }

        protected void gvListItem_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvListItem.PageIndex = e.NewPageIndex;
            getMonthYear();
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            gvListItem.AllowPaging = false;
            getMonthYear();

            Common.ExportExcel(gvListItem, ddlRemittanceLine.SelectedItem.Text + " BI Report");

            gvListItem.AllowPaging = true;
            getMonthYear();
        }

        
        void checkDownload()
        {
            if (gvListItem.Rows.Count != 0)
                btnDownload.Visible = true;
            else
                btnDownload.Visible = false;
        }

        protected void ddlMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            getMonthYear();
        }

        protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            getMonthYear();
        }

        protected void ddlRemittanceLine_SelectedIndexChanged(object sender, EventArgs e)
        {
            getMonthYear();
        }
    }
}