﻿using OMGLiveDemo.Models;
using OMGLiveDemo.Scripts;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.Report
{
    public partial class BankIndonesia : System.Web.UI.Page
    {
        OMG omg = new OMG();
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    //bindMonthYear();
                    //getFirstMonthYear();
                    BindGV();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        protected void btnAction_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            var refs = ((HiddenField)row.FindControl("hfPayoutID")).Value;

            Session["PayoutID"] = refs;
            Response.Redirect("Payout/ApproveOrReject");
        }

        DateTime getFirstDate(int mon, int year)
        {
            return new DateTime(year, mon, 1);
        }

        DateTime getLastDate(DateTime fd)
        {
            return fd.AddMonths(1).AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59);
        }

        //void getFirstMonthYear()
        //{
        //    var date = DateTime.Now;
        //    var mon = date.Month;
        //    var year = date.Year;

        //    ddlMonth.SelectedValue = mon.ToString();
        //    ddlYear.SelectedItem.Text = year.ToString();

        //    var firstdate = getFirstDate(mon, year);
        //    var lastdate = getLastDate(firstdate);

        //    //bind(firstdate, lastdate);
        //}

        //void getMonthYear()
        //{
        //    var firstdate = getFirstDate(Convert.ToInt32(ddlMonth.SelectedValue), Convert.ToInt32(ddlYear.SelectedValue));
        //    var lastdate = getLastDate(firstdate);

        //    //bind(firstdate, lastdate);
        //}

        void BindGV()
        {
            if (ddlFilterByDate.SelectedValue == "date")
            {
                doSingleDateDisplay();
            }
            else if (ddlFilterByDate.SelectedValue == "daterange")
            {
                doDateRangeDisplay();
            }
            else
            {
                bind();
            }
        }


        void bind()
        {
            if (ddlRemittanceLine.SelectedValue == "1")
            {
                /*var data = db.vw_Payout.Where(p => p.RemittanceID == 1 && p.Status == "completed" && p.CreatedDate >= fd && p.CreatedDate <= ld)
                    .GroupBy(p => new { p.RemittanceID, p.BeneficiaryAccount, p.BeneficiaryName, p.SenderName, p.BeneficiaryCodeBI, p.BeneficiaryCity, p.Purpose })
                    .Select(p =>
                     new
                     {
                         RemittanceID = 1,
                         CountryFrom = "MY-Malaysia",
                         CountryTo = p.Key.BeneficiaryCodeBI + "-" + p.Key.BeneficiaryCity,
                         BeneficiaryAccount = p.Key.BeneficiaryAccount,
                         BeneficiaryName = p.Key.BeneficiaryName,
                         SenderName = p.Key.SenderName,
                         TotalTransfer = p.Sum(s => s.Amount), //"MYR " + String.Format("{0:n}", p.Sum(s => s.TotalTransfer)),
                         Volume = p.Count(),
                         Purpose = p.Key.Purpose,
                     })
                    .ToList();*/
                var data = db.vw_Payout_IDR.Where(p => p.RemittanceID == 1 && p.Status == "completed" )
                    .GroupBy(p => new { p.RemittanceID, p.BeneficiaryAccount, p.BeneficiaryName, p.SenderName, p.BeneficiaryCodeBI, p.BeneficiaryCity, p.Purpose })
                    .Select(p =>
                     new
                     {
                         RemittanceID = 1,
                         CountryFrom = "MY-Malaysia",
                         CountryTo = p.Key.BeneficiaryCodeBI + "-" + p.Key.BeneficiaryCity,
                         BeneficiaryAccount = p.Key.BeneficiaryAccount,
                         BeneficiaryName = p.Key.BeneficiaryName,
                         SenderName = p.Key.SenderName,
                         TotalTransfer = p.Sum(s => s.IDRTotalTransfer), //"MYR " + String.Format("{0:n}", p.Sum(s => s.TotalTransfer)),
                         Volume = p.Count(),
                         Purpose = p.Key.Purpose,
                     })
                    .ToList();

                gvListItem.DataSource = null;
                gvListItem.DataSource = data;
                gvListItem.DataBind();
            }
            else if (ddlRemittanceLine.SelectedValue == "2")
            {
                /*var data = db.vw_Payout.Where(p => p.RemittanceID == 2 && p.Status == "completed" && p.CreatedDate >= fd && p.CreatedDate <= ld)
                    .GroupBy(p => new { p.RemittanceID, p.BeneficiaryAccount, p.BeneficiaryName, p.SenderName, p.SenderCodeBI, p.SenderCity, p.Purpose })
                    .Select(p =>
                     new
                     {
                         RemittanceID = 2,
                         CountryFrom = p.Key.SenderCodeBI + "-" + p.Key.SenderCity,
                         CountryTo = "MY-Malaysia",
                         BeneficiaryAccount = p.Key.BeneficiaryAccount,
                         BeneficiaryName = p.Key.BeneficiaryName,
                         SenderName = p.Key.SenderName,
                         TotalTransfer = p.Sum(s => s.TotalTransfer), // "IDR " + String.Format("{0:n}", p.Sum(s => s.Amount)),
                         Volume = p.Count(),
                         Purpose = p.Key.Purpose,
                     })
                    .ToList();*/

                var data = db.vw_Payout_IDR.Where(p => p.RemittanceID == 2 && p.Status == "completed" )
                    .GroupBy(p => new { p.RemittanceID, p.BeneficiaryAccount, p.BeneficiaryName, p.SenderName, p.SenderCodeBI, p.SenderCity, p.Purpose })
                    .Select(p =>
                     new
                     {
                         RemittanceID = 2,
                         CountryFrom = p.Key.SenderCodeBI + "-" + p.Key.SenderCity,
                         CountryTo = "MY-Malaysia",
                         BeneficiaryAccount = p.Key.BeneficiaryAccount,
                         BeneficiaryName = p.Key.BeneficiaryName,
                         SenderName = p.Key.SenderName,
                         TotalTransfer = p.Sum(s => s.IDRTotalTransfer), // "IDR " + String.Format("{0:n}", p.Sum(s => s.Amount)),
                         Volume = p.Count(),
                         Purpose = p.Key.Purpose,
                     })
                    .ToList();

                gvListItem.DataSource = null;
                gvListItem.DataSource = data;
                gvListItem.DataBind();
            }
        }

        protected void btnSingleDate_Click(object sender, EventArgs e)
        {
            doSingleDateDisplay();
        }

        void doSingleDateDisplay()
        {
            DateTime dS = new DateTime();
            DateTime dE = new DateTime();

            if (searchdate.Text.Trim() != "")
            {
                dS = Convert.ToDateTime(searchdate.Text.ToString().Trim());
                dE = dS.AddHours(23).AddMinutes(59).AddSeconds(59);
            }
            else
            {
                return;
            }

            if (ddlRemittanceLine.SelectedValue == "1")
            {

                var data = db.vw_Payout_IDR.Where(p => p.RemittanceID == 1 && p.Status == "completed" && (p.CreatedDate >= dS && p.CreatedDate <= dE))
                   .GroupBy(p => new { p.RemittanceID, p.BeneficiaryAccount, p.BeneficiaryName, p.SenderName, p.BeneficiaryCodeBI, p.BeneficiaryCity, p.Purpose })
                   .Select(p =>
                    new
                    {
                        RemittanceID = 1,
                        CountryFrom = "MY-Malaysia",
                        CountryTo = p.Key.BeneficiaryCodeBI + "-" + p.Key.BeneficiaryCity,
                        BeneficiaryAccount = p.Key.BeneficiaryAccount,
                        BeneficiaryName = p.Key.BeneficiaryName,
                        SenderName = p.Key.SenderName,
                        TotalTransfer = p.Sum(s => s.IDRTotalTransfer), //"MYR " + String.Format("{0:n}", p.Sum(s => s.TotalTransfer)),
                         Volume = p.Count(),
                        Purpose = p.Key.Purpose,
                    })
                   .ToList();


                gvListItem.DataSource = null;
                gvListItem.DataSource = data;
                gvListItem.DataBind();
            }
            else
            {
                var data = db.vw_Payout_IDR.Where(p => p.RemittanceID == 2 && p.Status == "completed" && (p.CreatedDate >= dS && p.CreatedDate <= dE))
                   .GroupBy(p => new { p.RemittanceID, p.BeneficiaryAccount, p.BeneficiaryName, p.SenderName, p.SenderCodeBI, p.SenderCity, p.Purpose })
                   .Select(p =>
                    new
                    {
                        RemittanceID = 2,
                        CountryFrom = p.Key.SenderCodeBI + "-" + p.Key.SenderCity,
                        CountryTo = "MY-Malaysia",
                        BeneficiaryAccount = p.Key.BeneficiaryAccount,
                        BeneficiaryName = p.Key.BeneficiaryName,
                        SenderName = p.Key.SenderName,
                        TotalTransfer = p.Sum(s => s.IDRTotalTransfer), // "IDR " + String.Format("{0:n}", p.Sum(s => s.Amount)),
                         Volume = p.Count(),
                        Purpose = p.Key.Purpose,
                    })
                   .ToList();

                gvListItem.DataSource = null;
                gvListItem.DataSource = data;
                gvListItem.DataBind();
            }

        }

        protected void btnDateRange_Click(object sender, EventArgs e)
        {
            doDateRangeDisplay();
        }

        void doDateRangeDisplay()
        {
            List<tblS_Log> logs = new List<tblS_Log>();

            DateTime dS = new DateTime();
            DateTime dE = new DateTime();

           

            if (ddlRemittanceLine.SelectedValue == "1")
            {
               

            }
            else
            {
                
            }

            gvListItem.DataSource = null;
            gvListItem.DataSource = logs;
            gvListItem.DataBind();
        }

        //void bindMonthYear()
        //{
        //    var mon = db.tblM_Month.ToList();
        //    ddlMonth.DataSource = null;
        //    ddlMonth.DataSource = mon;
        //    ddlMonth.DataValueField = "Value";
        //    ddlMonth.DataTextField = "Name";
        //    ddlMonth.DataBind();

        //    var yea = db.tblM_Year.ToList();
        //    ddlYear.DataSource = null;
        //    ddlYear.DataSource = yea;
        //    ddlYear.DataValueField = "Value";
        //    ddlYear.DataTextField = "Name";
        //    ddlYear.DataBind();
        //}

        protected void btnReceipt_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            Session["PayoutID"] = ((HiddenField)row.FindControl("hfPayoutID")).Value;
            //Response.Redirect("EditCurrencyRate");
            Page.ClientScript.RegisterStartupScript(this.GetType(), "OpenWindow", "window.open('/Payout/Receipt','_newtab');", true);
        }

        void RedirectToTransferHistory(string status)
        {
            Session["PayoutHistoryStatus"] = status;
            Response.Redirect("Payout/PayoutHistory");
        }
        protected void btnView_Click(object sender, EventArgs e)
        {

        }

        protected void gvListItem_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvListItem.PageIndex = e.NewPageIndex;
            //getMonthYear();
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            gvListItem.AllowPaging = false;
            //getMonthYear();

            Common.ExportExcel(gvListItem, ddlRemittanceLine.SelectedItem.Text + " BI Report");

            gvListItem.AllowPaging = true;
            //getMonthYear();
        }

        void checkDownload()
        {
            if (gvListItem.Rows.Count != 0)
                btnDownload.Visible = true;
            else
                btnDownload.Visible = false;
        }

        protected void ddlMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            //getMonthYear();
        }

        protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            //getMonthYear();
        }

        protected void ddlRemittanceLine_SelectedIndexChanged(object sender, EventArgs e)
        {
            //getMonthYear();
        }

        protected void ddlFilterByDate_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlFilterByDate.SelectedValue == "date")
            {
                divsingledate.Visible = true;
                divdaterange.Visible = false;
                startdate.Text = "";
                enddate.Text = "";
            }
            else if (ddlFilterByDate.SelectedValue == "daterange")
            {
                divsingledate.Visible = false;
                divdaterange.Visible = true;
                searchdate.Text = "";
            }
            else
            {
                divsingledate.Visible = false;
                divdaterange.Visible = false;
                startdate.Text = "";
                enddate.Text = "";
                searchdate.Text = "";

                bind();

            }
        }
    }
}