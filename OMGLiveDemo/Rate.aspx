﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Rate.aspx.cs" Inherits="OMGLiveDemo.Rate" MaintainScrollPositionOnPostback="true" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" />
    <div class="row">
        <div class="row x_title">
            <div class="col-md-12">
                <h3>Rate</h3>
            </div>
        </div>

        <div class="col-md-3 col-sm-12 col-xs-12"></div>
        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="dashboard_graph">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div style="width: 100%;">
                        <div id="canvas_dahs" class="demo-placeholder" style="width: 100%; height: 100%;">
                            <div class="form-group" style="height: 30px"></div>
                            
                            <asp:HiddenField runat="server" ID="hfRate" />
                            <asp:HiddenField runat="server" ID="hfRateIDR" />
                            <asp:HiddenField runat="server" ID="hfRateSAR" />
                            <asp:HiddenField runat="server" ID="hfRateIDMYRIDR" />
                            <asp:HiddenField runat="server" ID="hfRateIDIDRMYR" />
                            <asp:HiddenField runat="server" ID="hfRateIDIDRSAR" />
                            <%--<asp:HiddenField runat="server" ID="hfTotalAmount" />--%>
                            
                            <div class="alert alert-danger alert-dismissible fade in" runat="server" id="diverror" visible="false">
                                <button type="button" runat="server" id="btnError" onserverclick="btnError_ServerClick" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <div class="centered"><asp:Label runat="server" ID="lblError" /></div>
                            </div>

                            <div class="alert alert-success alert-dismissible fade in" runat="server" id="divsuccess" visible="false">
                                <button type="button" runat="server" id="btnSuccess" onserverclick="btnSuccess_ServerClick" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <div class="centered"><asp:Label runat="server" ID="lblSuccess" /></div>
                            </div>

                            <div class="form-group">
                                <asp:Label runat="server" CssClass="control-label" Text="Choose Remittance" />
                                <asp:DropDownList runat="server" ID="ddlChooseRemittance" CssClass="form-control" OnSelectedIndexChanged="ddlChooseRemittance_SelectedIndexChanged" AutoPostBack="true">
                                    <asp:ListItem Text="Malaysia to Indonesia" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Indonesia to Malaysia" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Indonesia to Saudi Arabia" Value="3"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="form-group" style="height: 5px"></div>


                            <div runat="server" id="divMYR2IDR" visible="true">
                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="control-label" Text="Country : " />
                                    <br />
                                    <asp:Label runat="server" ID="lblCountryMYR2IDR" Font-Bold="True" Font-Size="X-Large" Text="Malaysia to Indonesia" />
                                </div>
                                <div class="form-group" style="height: 5px"></div>

                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="control-label" Text="Currency : " />
                                    <br />
                                    <asp:Label runat="server" ID="lblCurrencyMYR2IDR" Font-Bold="True" Font-Size="X-Large" />
                                </div>
                                <div class="form-group" style="height: 5px"></div>

                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="control-label" Text="Base Rate : " />
                                    <br />
                                    <asp:Label runat="server" ID="lblBaseRateMYR2IDR" Font-Bold="True" Font-Size="X-Large" />
                                </div>
                                <div class="form-group">
                                    <div class="alert alert-danger fade in">
                                        <div class="centered">
                                            <asp:Label runat="server" ID="lblRateMYR2IDR" Font-Bold="True" Font-Size="Medium" />
                                        </div>
                                        <div class="centered">
                                            <asp:Label runat="server" ID="lblRateUpdateDateMYR2IDR"/>
                                        </div>
                                    </div>
                                </div>

                                <div runat="server" id="divUpdateMYR2IDRBaseRate" visible="false" class="alert alert-success fade in">
                                    <div class="form-group">
                                        <asp:Label runat="server" CssClass="control-label" Text="New Base Rate :" />
                                        <div style="margin-bottom: 10px"></div>
                                        <asp:TextBox runat="server" ID="txtUpdateRateMYR2IDR" readonly="false" CssClass="form-control" TextMode="Number" />
                                    </div>

                                    <div class="form-group">
                                        <asp:Button runat="server" ID="btnUpdateBaseRateMYR2IDR" Text="Update" CssClass="btn btn-default" OnClick="btnUpdateBaseRateMYR_Click" />
                                    </div>
                                </div>
                                <div class="form-group" style="height: 5px"></div>

                                <div runat="server" id="divUpdateMYR2IDRPlatinumMargin" visible="false" class="form-group">
                                    <asp:Label runat="server" ID="lblMYR2IDRMargin" CssClass="control-label" Text="Platinum Margin (in IDR) *" />
                                    <asp:TextBox runat="server" ID="txtPlatinumMarginMYR2IDR" CssClass="form-control" OnTextChanged="txtPlatinumMargin_TextChanged" AutoPostBack="true" />
                                </div>

                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="control-label" Text="Platinum Retail Rate : " />
                                    <br />
                                    <asp:Label runat="server" ID="lblPlatinumRetailRateMYR2IDR" Font-Bold="True" Font-Size="XX-Large" ForeColor="Red" />
                                </div>

                                <hr />
                                <div class="form-group" style="height: 5px"></div>

                                <div runat="server" id="divUpdateMYR2IDRGoldMargin" visible="false" class="form-group">
                                    <asp:Label runat="server" CssClass="control-label" Text="Gold Margin (in IDR) *" />
                                    <asp:TextBox runat="server" ID="txtGoldMarginMYR2IDR" CssClass="form-control" OnTextChanged="txtGoldMargin_TextChanged" AutoPostBack="true" />
                                </div>

                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="control-label" Text="Gold Retail Rate : " />
                                    <br />
                                    <asp:Label runat="server" ID="lblGoldRetailRateMYR2IDR" Font-Bold="True" Font-Size="XX-Large" ForeColor="Red" />
                                </div>

                                <hr />
                                <div class="form-group" style="height: 10px"></div>

                                <div runat="server" id="divUpdateMYR2IDRSilverMargin" visible="false" class="form-group">
                                    <asp:Label runat="server" ID="Label2" CssClass="control-label" Text="Silver Margin (in IDR) *" />
                                    <asp:TextBox runat="server" ID="txtSilverMarginMYR2IDR" CssClass="form-control" OnTextChanged="txtSilverMargin_TextChanged" AutoPostBack="true" />
                                </div>

                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="control-label" Text="Silver Retail Rate : " />
                                    <br />
                                    <asp:Label runat="server" ID="lblSilverRetailRateMYR2IDR" Font-Bold="True" Font-Size="XX-Large" ForeColor="Red" />
                                </div>

                                <hr />
                                <div class="form-group" style="height: 10px"></div>

                                <div runat="server" id="divUpdateMYR2IDRBronzeMargin" visible="false" class="form-group">
                                    <asp:Label runat="server" ID="Label3" CssClass="control-label" Text="Bronze Margin (in IDR) *" />
                                    <asp:TextBox runat="server" ID="txtBronzeMarginMYR2IDR" CssClass="form-control" OnTextChanged="txtBronzeMargin_TextChanged" AutoPostBack="true" />
                                </div>

                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="control-label" Text="Bronze Retail Rate : " />
                                    <br />
                                    <asp:Label runat="server" ID="lblBronzeRetailRateMYR2IDR" Font-Bold="True" Font-Size="XX-Large" ForeColor="Red" />
                                </div>

                                <hr />
                                <div class="form-group" style="height: 10px"></div>

                                <div runat="server" id="divUpdateMYR2IDRCustomMargin" visible="false" class="form-group">
                                    <asp:Label runat="server" CssClass="control-label" Text="Customize Margin (in IDR) *" />
                                    <asp:TextBox runat="server" ID="txtCustomizeMarginMYR2IDR" CssClass="form-control" OnTextChanged="txtCustomizeMargin_TextChanged" AutoPostBack="true" />
                                </div>

                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="control-label" Text="Customize Retail Rate : " />
                                    <br />
                                    <asp:Label runat="server" ID="lblCustomizeRetailRateMYR2IDR" Font-Bold="True" Font-Size="XX-Large" ForeColor="Red" />
                                </div>

                                <hr />
                            </div>

                            <div runat="server" id="divIDR2MYR" visible="false">
                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="control-label" Text="Country : " />
                                    <br />
                                    <asp:Label runat="server" ID="lblCountryIDR2MYR" Font-Bold="True" Font-Size="X-Large" Text="Indonesia to Malaysia" />
                                </div>
                                <div class="form-group" style="height: 5px"></div>

                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="control-label" Text="Currency : " />
                                    <br />
                                    <asp:Label runat="server" ID="lblCurrencyIDR2MYR" Font-Bold="True" Font-Size="X-Large" />
                                </div>
                                <div class="form-group" style="height: 5px"></div>

                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="control-label" Text="Base Rate : " />
                                    <br />
                                    <asp:Label runat="server" ID="lblBaseRateIDR2MYR" Font-Bold="True" Font-Size="X-Large" />
                                </div>

                                <div class="form-group">
                                    <div class="alert alert-danger fade in">
                                        <div class="centered">
                                            <asp:Label runat="server" ID="lblRateIDR2MYR" Font-Bold="True" Font-Size="Medium" />
                                        </div>
                                        <div class="centered">
                                            <asp:Label runat="server" ID="lblRateUpdateDateIDR2MYR"/>
                                        </div>
                                    </div>
                                </div>

                                <div runat="server" id="divUpdateIDR2MYRBaseRate" visible="false" class="alert alert-success fade in">
                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label6" CssClass="control-label" Text="New Base Rate :" />
                                        <div style="margin-bottom: 10px"></div>
                                        <asp:TextBox runat="server" ID="txtUpdateRateIDR2MYR" CssClass="form-control" TextMode="Number" />
                                    </div>

                                    <div class="form-group">
                                        <asp:Button runat="server" ID="btnUpdateBaseRateIDR2MYR" Text="Update" CssClass="btn btn-default" OnClick="btnUpdateBaseRateIDR_Click" />
                                    </div>
                                </div>
                                <div class="form-group" style="height: 5px"></div>


                                <div runat="server" id="divUpdateIDR2MYRPlatinumMargin" visible="false" class="form-group">
                                    <asp:Label runat="server" ID="lblMarginIDR2MYR" CssClass="control-label" Text="Platinum Margin (in IDR) *" />
                                    <asp:TextBox runat="server" ID="txtPlatinumMarginIDR2MYR" CssClass="form-control" OnTextChanged="txtPlatinumMarginIDR_TextChanged" AutoPostBack="true" />
                                </div>

                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="control-label" Text="Platinum Retail Rate : " />
                                    <br />
                                    <asp:Label runat="server" ID="lblPlatinumRetailRateIDR2MYR" Font-Bold="True" Font-Size="XX-Large" ForeColor="Red" />
                                </div>

                                <hr />
                                <div class="form-group" style="height: 5px"></div>

                                <div runat="server" id="divUpdateIDR2MYRGoldMargin" visible="false" class="form-group">
                                    <asp:Label runat="server" ID="Label1" CssClass="control-label" Text="Gold Margin (in IDR) *" />
                                    <asp:TextBox runat="server" ID="txtGoldMarginIDR2MYR" CssClass="form-control" OnTextChanged="txtGoldMarginIDR_TextChanged" AutoPostBack="true" />
                                </div>

                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="control-label" Text="Gold Retail Rate : " />
                                    <br />
                                    <asp:Label runat="server" ID="lblGoldRetailRateIDR2MYR" Font-Bold="True" Font-Size="XX-Large" ForeColor="Red" />
                                </div>

                                <hr />
                                <div class="form-group" style="height: 10px"></div>

                                <div runat="server" id="divUpdateIDR2MYRCustomMargin" visible="false" class="form-group">
                                    <asp:Label runat="server" ID="Label4" CssClass="control-label" Text="Customize Margin (in IDR) *" />
                                    <asp:TextBox runat="server" ID="txtCustomizeMarginIDR2MYR" CssClass="form-control" OnTextChanged="txtCustomizeMarginIDR_TextChanged" AutoPostBack="true" />
                                </div>

                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="control-label" Text="Customize Retail Rate : " />
                                    <br />
                                    <asp:Label runat="server" ID="lblCustomizeRetailRateIDR2MYR" Font-Bold="True" Font-Size="XX-Large" ForeColor="Red" />
                                </div>

                                <hr />
                            </div>

                            <div runat="server" id="divIDR2SAR" visible="false">
                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="control-label" Text="Country : " />
                                    <br />
                                    <asp:Label runat="server" ID="lblCountryIDR2SAR" Font-Bold="True" Font-Size="X-Large" Text="Indonesia to Saudi Arabia" />
                                </div>
                                <div class="form-group" style="height: 5px"></div>

                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="control-label" Text="Currency : " />
                                    <br />
                                    <asp:Label runat="server" ID="lblCurrencyIDR2SAR" Font-Bold="True" Font-Size="X-Large" />
                                </div>
                                <div class="form-group" style="height: 5px"></div>

                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="control-label" Text="Base Rate : " />
                                    <br />
                                    <asp:Label runat="server" ID="lblBaseRateIDR2SAR" Font-Bold="True" Font-Size="X-Large" />
                                </div>
                                <div class="form-group">
                                    <div class="alert alert-danger fade in">
                                        <div class="centered">
                                            <asp:Label runat="server" ID="lblRateIDR2SAR" Font-Bold="True" Font-Size="Medium" />
                                        </div>
                                        <div class="centered">
                                            <asp:Label runat="server" ID="lblRateUpdateDateIDR2SAR"/>
                                        </div>
                                    </div>
                                </div>

                                <div runat="server" id="divUpdateIDR2SARBaseRate" visible="false" class="alert alert-success fade in">
                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label61" CssClass="control-label" Text="New Base Rate :" />
                                        <div style="margin-bottom: 10px"></div>
                                        <asp:TextBox runat="server" ID="txtUpdateRateIDR2SAR" CssClass="form-control" TextMode="Number" />
                                    </div>

                                    <div class="form-group">
                                        <asp:Button runat="server" ID="btnUpdateBaseRateIDR2SAR" Text="Update" CssClass="btn btn-default" OnClick="btnUpdateBaseRateSAR_Click" />
                                    </div>
                                </div>
                                <div class="form-group" style="height: 5px"></div>


                                <div runat="server" id="divUpdateIDR2SARPublicMargin" visible="false" class="form-group">
                                    <asp:Label runat="server" ID="lblMarginIDR2SAR" CssClass="control-label" Text="Public Margin (in IDR) *" />
                                    <asp:TextBox runat="server" ID="txtPublicMarginIDR2SAR" CssClass="form-control" OnTextChanged="txtPublicMarginSAR_TextChanged" AutoPostBack="true" />
                                </div>

                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="control-label" Text="Public Retail Rate : " />
                                    <br />
                                    <asp:Label runat="server" ID="lblPublicRetailRateIDR2SAR" Font-Bold="True" Font-Size="XX-Large" ForeColor="Red" />
                                </div>

                                <hr />
                            </div>

                            <div runat="server" id="divUpdateMarginBtn" visible="false" class="form-group">
                                <div class="centered">
                                    <asp:Button runat="server" ID="btnUpdate" Text="Update" CssClass="btn btn-success btn-lg" OnClick="btnUpdate_Click" Width="100%" />
                                </div>
                                <%--<asp:Button runat="server" ID="btnThumbnail" Text="Buat Thumbnail" CssClass="btn btn-success" OnClick="btnThumbnail_Click" />--%>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12"></div>
    </div>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Scripts" runat="server">
</asp:Content>
