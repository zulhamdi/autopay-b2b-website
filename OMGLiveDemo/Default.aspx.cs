﻿using OMGLiveDemo.Models;
using OMGLiveDemo.Scripts;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo
{
    public partial class _Default : Page
    {
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();
        OMG omg = new OMG();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    //BindGVPendingTask();
                    BindGV();
                    count();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        /*
        void BindGVPendingTask()
        {
            var dt = db.tblT_Payout.Where(p => p.Status == "queued").ToList();
            if (dt.Count == 0)
            {
                divpendingtask.Visible = false;
            }
            else
            {
                for (int i = 0; i < dt.Count; i++)
                {
                    var id = dt[i].CreatedBy;
                    if (dt[i].isAgent == 1)
                        dt[i].CreatedName = db.tblM_Agent.Where(p => p.AgentID == id).Select(p => p.Fullname).FirstOrDefault();
                    else
                        dt[i].CreatedName = db.tblM_Admin.Where(p => p.AdminID == id).Select(p => p.Fullname).FirstOrDefault();
                    if (dt[i].RemittanceID == 1)
                    {
                        dt[i].AmountText = "RM " + String.Format(new CultureInfo("ms-MY"), "{0:n}", dt[i].Amount.Value).Replace("RM", "RM ");
                        if (dt[i].isAgent == 2)
                            dt[i].AdminFee = "IDR " + dt[i].AdminFee;
                        else
                            dt[i].AdminFee = "RM " + dt[i].AdminFee;
                        dt[i].RateGivenText = "Rp " + String.Format(new CultureInfo("id-ID"), "{0:n}", float.Parse(dt[i].RateGiven)).Replace("Rp", "Rp ");
                        dt[i].RateText = "Rp " + String.Format(new CultureInfo("id-ID"), "{0:n}", float.Parse(dt[i].Rate)).Replace("Rp", "Rp ");
                        dt[i].TotalAmountText = "RM " + String.Format(new CultureInfo("ms-MY"), "{0:n}", dt[i].TotalAmount).Replace("RM", "RM ");
                        dt[i].TotalTransferText = "Rp " + String.Format(new CultureInfo("id-ID"), "{0:n}", dt[i].TotalTransfer).Replace("Rp", "Rp ");
                    }
                }
            }

            gvPendingTask.DataSource = dt;
            gvPendingTask.DataBind();
        }
        */

        protected void btnAction_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            var refs = ((HiddenField)row.FindControl("hfPayoutID")).Value;

            Session["PayoutID"] = refs;
            Response.Redirect("Payout/ApproveOrReject");
        }

        void BindGV()
        {
            var dt = db.vw_Payout.OrderByDescending(p => p.PayoutID).Take(20).ToList();
            for (int i = 0; i < dt.Count; i++)
            {
                if(dt[i].RemittanceID == 1)
                {
                    dt[i].AmountText = "RM " + String.Format(new CultureInfo("ms-MY"), "{0:n}", dt[i].Amount.Value);

                    if (dt[i].isAgent == 2)
                        dt[i].AdminFee = "Rp " + String.Format(new CultureInfo("id-ID"), "{0:n}", double.Parse(dt[i].AdminFee)); // fee in Rp
                    else
                        dt[i].AdminFee = "RM " + String.Format(new CultureInfo("ms-MY"), "{0:n}", double.Parse(dt[i].AdminFee));     // fee in RM

                    dt[i].RateText = "Rp " + String.Format(new CultureInfo("id-ID"), "{0:n}", float.Parse(dt[i].Rate));
                    dt[i].RateGivenText = "Rp " + String.Format(new CultureInfo("id-ID"), "{0:n}", float.Parse(dt[i].RateGiven));
                    //dt[i].TotalAmountText = "Rp " + String.Format(new CultureInfo("id-ID"), "{0:n}", dt[i].TotalAmount).Replace("RM", "RM ");
                    dt[i].TotalAmountText = "Rp " + String.Format(new CultureInfo("id-ID"), "{0:n}", dt[i].Amount.Value * double.Parse(dt[i].Rate)); //DB TotalAmount in RM
                    dt[i].TotalTransferText = "Rp " + String.Format(new CultureInfo("id-ID"), "{0:n}", dt[i].TotalTransfer);
                }
                else
                {
                    dt[i].AmountText = "Rp " + String.Format(new CultureInfo("id-ID"), "{0:n}", dt[i].Amount.Value);

                    if (dt[i].isAgent == 2)
                        dt[i].AdminFee = "RM " + String.Format(new CultureInfo("ms-MY"), "{0:n}", double.Parse(dt[i].AdminFee));    // fee in RM
                    else
                        dt[i].AdminFee = "Rp " + String.Format(new CultureInfo("id-ID"), "{0:n}", double.Parse(dt[i].AdminFee)); // fee in Rp

                    dt[i].RateText = "Rp " + String.Format(new CultureInfo("id-ID"), "{0:n}", double.Parse(dt[i].Rate));
                    dt[i].RateGivenText = "Rp " + String.Format(new CultureInfo("id-ID"), "{0:n}", double.Parse(dt[i].RateGiven));
                    //dt[i].TotalAmountText = "RM " + String.Format(new CultureInfo("ms-MY"), "{0:n}", dt[i].TotalAmount).Replace("RM", "RM ");
                    dt[i].TotalAmountText = "Rp " + String.Format(new CultureInfo("id-ID"), "{0:n}", dt[i].TotalAmount);
                    dt[i].TotalTransferText = "Rp " + String.Format(new CultureInfo("id-ID"), "{0:n}", dt[i].TotalTransfer*double.Parse(dt[i].Rate)); // DB TotalTransfer in RM
                }
                
            }
            gvListItem.DataSource = dt;
            gvListItem.DataBind();
        }
        

        public void count()
        {
            var curmyr = omg.getRateMYR2IDR();
            if (curmyr != null)
            {
                string tempRate = "";
                if (curmyr.Rate.Contains(".00")) { curmyr.Rate = curmyr.Rate.Replace(".00", "");  }
                lblRate.Text = "1 MYR = " + String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(curmyr.Rate)).Replace("Rp", "") + " IDR";
                lblRateUpdate.Text = "Last update : " + curmyr.DateModified.ToLongTimeString() + ", " + curmyr.DateModified.ToLongDateString();
            }

            // rate idr //

            var curidr = omg.getRateIDRMYR();
            if (curidr != null)
            {
                lblRateIDR.Text = "1 MYR = " + String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(curidr.Rate)).Replace("Rp", "") + " IDR";
                lblRateUpdateIDR.Text = "Last update : " + curidr.DateModified.ToLongTimeString() + ", " + curidr.DateModified.ToLongDateString();
            }

            // rate idr //
            
            var tra = db.tblT_Payout.Count();
            var agents = db.tblM_Agent.Count();
            lbkAgents.Text = agents.ToString();
            lbkMasterArea.Text = db.tblM_Master_Area.Count().ToString();
            lbkTotalTransferCompleted.Text = db.tblT_Payout.Where(p => p.Status == "completed").Count().ToString();

            if (db.tblT_Payout.Where(p => p.Status == "created" || p.Status == "queued" || p.Status == "approved").Count() != 0)
            {
                lbkTraPending.Text = "Rp " + String.Format(new CultureInfo("id-ID"), "{0:n}", db.tblT_Payout.Where(p => p.Status == "created" || p.Status == "queued" || p.Status == "approved").Select(p => p.TotalTransfer).Sum()).Replace("Rp", "Rp ");
                lblTraPendingPrecent.Text = ((db.tblT_Payout.Where(p => p.Status == "created" || p.Status == "queued" || p.Status == "approved").Count() * 100) / tra).ToString();
            }
            else
            {
                lbkTraPending.Text = "Rp 0";
            }

            if (db.tblT_Payout.Where(p => p.Status == "completed").Count() != 0)
            {
                lbkTraSuccess.Text = "Rp " + String.Format(new CultureInfo("id-ID"), "{0:n}", db.tblT_Payout.Where(p => p.Status == "completed").Select(p => p.TotalTransfer).Sum()).Replace("Rp", "Rp ");
                lblTraSuccessPrecent.Text = ((db.tblT_Payout.Where(p => p.Status == "completed").Count() * 100) / tra).ToString();
            }
            else
            {
                lbkTraSuccess.Text = "Rp 0";
            }

            if (db.tblT_Payout.Where(p => p.Status == "processed").Count() != 0)
            {
                lbkTraProcess.Text = "Rp " + String.Format(new CultureInfo("id-ID"), "{0:n}", db.tblT_Payout.Where(p => p.Status == "processed").Select(p => p.TotalTransfer).Sum()).Replace("Rp", "Rp ");
                lblTraProcessPrecent.Text = ((db.tblT_Payout.Where(p => p.Status == "processed").Count() * 100) / tra).ToString();
            }
            else
            {
                lbkTraProcess.Text = "Rp 0";
            }

            if (db.tblT_Payout.Where(p => p.Status == "rejected").Count() != 0)
            {
                lbkTraRejected.Text = "Rp " + String.Format(new CultureInfo("id-ID"), "{0:n}", db.tblT_Payout.Where(p => p.Status == "rejected").Select(p => p.TotalTransfer).Sum()).Replace("Rp", "Rp ");
                lblTraRejectPercent.Text = ((db.tblT_Payout.Where(p => p.Status == "rejected").Count() * 100) / tra).ToString();
            }
            else
            {
                lbkTraRejected.Text = "Rp 0";
            }

            if (db.tblT_Payout.Where(p => p.Status == "failed").Count() != 0)
            {
                lbkTraFailed.Text = "Rp " + String.Format(new CultureInfo("id-ID"), "{0:n}", db.tblT_Payout.Where(p => p.Status == "failed").Select(p => p.TotalTransfer).Sum()).Replace("Rp", "Rp ");
                lblTraFailedPercent.Text = ((db.tblT_Payout.Where(p => p.Status == "failed").Count() * 100) / tra).ToString();
            }
            else
            {
                lbkTraFailed.Text = "Rp 0";
            }

            if (db.tblT_Payout.Where(p => p.Status == "cancelled").Count() != 0)
            {
                lbkTraCancelled.Text = "Rp " + String.Format(new CultureInfo("id-ID"), "{0:n}", db.tblT_Payout.Where(p => p.Status == "cancelled").Select(p => p.TotalTransfer).Sum()).Replace("Rp", "Rp ");
                lblTraCancelledPercent.Text = ((db.tblT_Payout.Where(p => p.Status == "cancelled").Count() * 100) / tra).ToString();
            }
            else
            {
                lbkTraCancelled.Text = "Rp 0";
            }
        }

        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Payout/PayoutHistory");
        }

        protected void btnUpdateMargin_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Rate");
        }

        protected void btnReceipt_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            Session["PayoutID"] = ((HiddenField)row.FindControl("hfPayoutID")).Value;
            //Response.Redirect("EditCurrencyRate");
            Page.ClientScript.RegisterStartupScript(this.GetType(), "OpenWindow", "window.open('/Payout/Receipt','_newtab');", true);
        }

        protected Boolean IsCompleted(string status)
        {
            if (status == "completed")
                return true;
            else
                return false;
        }

        void RedirectToTransferHistory(string status)
        {
            Session["PayoutHistoryStatus"] = status;
            Response.Redirect("Payout/PayoutHistory");
        }

        protected void btnDivPendingTransaction_Click(object sender, EventArgs e)
        {
            RedirectToTransferHistory("pending");
        }

        protected void btnDivSuccessTransaction_Click(object sender, EventArgs e)
        {
            RedirectToTransferHistory("completed");
        }

        protected void btnDivProcessTransaction_Click(object sender, EventArgs e)
        {
            RedirectToTransferHistory("processed");
        }

        protected void btnDivRejectedTransaction_Click(object sender, EventArgs e)
        {
            RedirectToTransferHistory("rejected");
        }

        protected void btnDivFailedTransaction_Click(object sender, EventArgs e)
        {
            RedirectToTransferHistory("failed");
        }

        protected void btnDivListAgent_Click(object sender, EventArgs e)
        {
            Response.Redirect("Agent/ListAgent");
        }

        protected void btnEmail_Click(object sender, EventArgs e)
        {
            var pay = db.tblT_Payout.Where(p => p.PayoutID == 46).FirstOrDefault();
            omg.sendEmailAgent(pay);
        }
    }
}