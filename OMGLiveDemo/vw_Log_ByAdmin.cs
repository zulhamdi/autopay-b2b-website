//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OMGLiveDemo
{
    using System;
    using System.Collections.Generic;
    
    public partial class vw_Log_ByAdmin
    {
        public long ID { get; set; }
        public System.DateTime EventDate { get; set; }
        public string EventType { get; set; }
        public long EventBy { get; set; }
        public string AdminName { get; set; }
        public string EventData { get; set; }
    }
}
