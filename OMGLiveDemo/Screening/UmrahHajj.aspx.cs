﻿using OMGLiveDemo.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.Screening
{
    public partial class UmrahHajj : System.Web.UI.Page
    {
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    BindGV();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        void BindGV()
        {
            List<tblS_Umrah_Hajj> dt = new List<tblS_Umrah_Hajj>();
            dt = db.tblS_Umrah_Hajj.ToList();

            gvListItem.DataSource = null;
            gvListItem.DataSource = dt;
            gvListItem.DataBind();

        }

        protected void gvListItem_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvListItem.PageIndex = e.NewPageIndex;
            BindGV();
        }

        protected void ddlEventType_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGV();
        }

        protected void btnSearchID_Click(object sender, EventArgs e)
        {
            if (txtSearchID.Text.Trim() != "")
            {
                var dt = db.tblS_Umrah_Hajj.Where(p => p.IDNo == txtSearchID.Text.Trim()).FirstOrDefault();

                if (dt != null)
                {
                    gvListItem.DataSource = dt;
                    gvListItem.DataBind();
                }
            }
        }


        protected void btnSearchName_Click(object sender, EventArgs e)
        {
            if (txtSearchName.Text.Trim() != "")
            {
                var qr = from result in db.tblS_Umrah_Hajj where result.Name.Contains(txtSearchName.Text.Trim()) select result;
                var dt = qr.ToList();

                if (dt != null)
                {
                    gvListItem.DataSource = dt;
                    gvListItem.DataBind();
                }
            }
        }

        protected void btnSearchByDOB_Click(object sender, EventArgs e)
        {
            if (searchDOB.Text.Trim() != "")
            {
                DateTime dS = new DateTime();
                DateTime dE = new DateTime();

                dS = Convert.ToDateTime(searchDOB.Text.Trim());
                dE = dS.AddHours(23).AddMinutes(59).AddSeconds(59);
                var dt = db.tblS_Umrah_Hajj.Where(p => p.DOB >= dS && p.DOB <= dE).FirstOrDefault();

                if (dt != null)
                {
                    gvListItem.DataSource = dt;
                    gvListItem.DataBind();
                }
            }
        }
    }
}