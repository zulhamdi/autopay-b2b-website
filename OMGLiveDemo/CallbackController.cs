﻿using Newtonsoft.Json;
using OMGLiveDemo.Scripts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace OMGLiveDemo
{
    //[RoutePrefix("v1/callback")]
    public class CallbackController : ApiController
    {
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();
        OMG omg = new OMG();

        //[Route("omg")]
        [HttpPost]
        public async Task<HttpResponseMessage> OMG([FromBody]Payout pay)
        {
            var response = new HttpResponseMessage();

            try
            {
                if (pay != null)
                {
                    var data = db.tblT_Payout.Where(p => p.Reference == pay.ReferenceID).FirstOrDefault();
                    if (data != null)
                    {
                        if (pay.Status == "completed")
                        {
                            if (data.isAgent == 1)
                            {
                                var agentid = data.CreatedBy;
                                double margin = 0, pend = 0;
                                long cid = 0;
                                if (data.RemittanceID == 1)
                                {
                                    margin = (Convert.ToDouble(data.RateGiven) - Convert.ToDouble(data.Rate));
                                    pend = margin * Convert.ToDouble(pay.Amount);
                                    cid = 103;
                                }
                                else if (data.RemittanceID == 2)
                                {
                                    margin = (Convert.ToDouble(data.Rate) - Convert.ToDouble(data.RateGiven));
                                    pend = margin * Convert.ToDouble(data.TotalTransfer);
                                    cid = 133;
                                }

                                var bal = db.tblM_Agent_Balance.Where(p => p.AgentID == agentid && p.CountryID == cid).FirstOrDefault();
                                if (bal != null)
                                {
                                    bal.Balance = bal.Balance + pend;
                                    bal.UpdatedDate = DateTime.Now;
                                    bal.UpdatedBy = 0;
                                    db.SaveChanges();

                                    tblH_Agent_Balance_Update upbal = new tblH_Agent_Balance_Update();
                                    upbal.BalanceID = bal.BalanceID;
                                    upbal.AgentID = agentid;
                                    upbal.Margin = margin;
                                    upbal.Reference = data.Reference;
                                    upbal.PayoutID = data.PayoutID.ToString();
                                    upbal.Amount = pend;
                                    upbal.UpdatedDate = bal.UpdatedDate;
                                    upbal.Status = "Transaction";
                                    upbal.UpdateBy = 0;
                                    db.tblH_Agent_Balance_Update.Add(upbal);
                                    db.SaveChanges();
                                }

                                omg.sendEmailAgent(data);
                            }
                        }

                        if (data.isAgent == 2)
                        {
                            if (pay.Status == "failed" || pay.Status == "rejected")
                            {
                                refundMasterArea(data.CreatedBy.Value, data.PayoutID, data);
                            }
                        }

                        data.Status = pay.Status;
                        data.ModifiedDate = DateTime.Now;

                        db.SaveChanges();

                        return response;
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return response;
        }

        void refundMasterArea(long maid, long payid, tblT_Payout pay)
        {
            var id = pay.PayoutID;
            var check = db.tblH_Master_Area_Balance_Update.Where(p => p.PayoutID == id && p.Type == "Refund").FirstOrDefault();
            if (check == null)
            {
                tblM_Master_Area_Balance bal = new tblM_Master_Area_Balance();
                if (pay.RemittanceID == 1)
                    bal = db.tblM_Master_Area_Balance.Where(p => p.MasterAreaID == maid && p.CountryID == 103).FirstOrDefault();
                else if (pay.RemittanceID == 2)
                    bal = db.tblM_Master_Area_Balance.Where(p => p.MasterAreaID == maid && p.CountryID == 133).FirstOrDefault();

                tblH_Master_Area_Balance_Update hbal = new tblH_Master_Area_Balance_Update();
                hbal.Type = "Refund";
                hbal.Reference = pay.Reference;
                hbal.RemittanceID = pay.RemittanceID;
                hbal.MasterAreaID = maid;
                hbal.PayoutID = payid;
                hbal.BankID = 0;
                hbal.BankName = pay.BankName;
                hbal.Account = pay.BeneficiaryAccount;
                hbal.Beneficiary = pay.BeneficiaryName;
                hbal.Amount = pay.TotalTransfer;
                hbal.AdminFee = Convert.ToDouble(pay.AdminFee);
                hbal.DateCreated = DateTime.Now;

                hbal.BalancePrevious = bal.Balance;
                hbal.Balance = bal.Balance + (pay.TotalTransfer + Convert.ToDouble(pay.AdminFee));
                bal.Balance = hbal.Balance;
                bal.UpdatedDate = DateTime.Now;
                hbal.BalanceID = bal.BalanceID;

                db.tblH_Master_Area_Balance_Update.Add(hbal);
                db.SaveChanges();

                omg.sendEmailMasterArea(pay);
            }
        }
        
        public class Payout
        {
            public long RemittanceID { get; set; }
            public string ReferenceID { get; set; }
            public string Amount { get; set; }
            public string Status { get; set; }
            public Nullable<System.DateTime> DateModified { get; set; }
        }
    }
}