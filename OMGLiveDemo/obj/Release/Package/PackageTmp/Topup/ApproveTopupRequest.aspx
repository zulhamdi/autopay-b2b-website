﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ApproveTopupRequest.aspx.cs" Inherits="OMGLiveDemo.Topup.ApproveTopupRequest" MaintainScrollPositionOnPostback="true" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" />

    <div class="row">
        <div class="row x_title">
            <div class="col-md-12">
                <h3>Approve Topup Request</h3>
            </div>
        </div>

        <div class="col-md-3 col-sm-12 col-xs-12"></div>
        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="dashboard_graph">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div style="width: 100%;">
                        <div id="canvas_dahs" class="demo-placeholder" style="width: 100%; height: 100%;">
                            <div class="form-group" style="height: 30px"></div>
                            
                            <asp:HiddenField runat="server" ID="hfTopupReceiptURL" />
                            <asp:HiddenField runat="server" ID="hfTopupID" />
                            <asp:HiddenField runat="server" ID="hfCountryID" />
                            <asp:HiddenField runat="server" ID="hfBalanceID" />
                            <asp:HiddenField runat="server" ID="hfMasterAreaID" />
                            <asp:HiddenField runat="server" ID="hfTravelAgentID" />
                            <asp:HiddenField runat="server" ID="hfTopupName" />

                            <div class="alert alert-danger alert-dismissible fade in" runat="server" id="diverror" visible="false">
                                <button type="button" runat="server" id="btnError" onserverclick="btnError_ServerClick" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <center><asp:Label runat="server" ID="lblError" /></center>
                            </div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblName" CssClass="control-label" Text="Master Area Name" />
                                <asp:TextBox runat="server" ID="txtName" CssClass="form-control" Enabled="false" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblTotal" CssClass="control-label" Text="Total Amount " />
                                <br />
                                <asp:Label runat="server" ID="lblTotalAmount" Font-Bold="true" Font-Size="XX-Large" ForeColor="Red" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblPhone" CssClass="control-label" Text="Transfer to " />
                                <asp:TextBox runat="server" ID="txtBankName" CssClass="form-control" Enabled="false" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblEmail" CssClass="control-label" Text="Account No. " />
                                <asp:TextBox runat="server" ID="txtAccount" CssClass="form-control" Enabled="false" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblSenderType" CssClass="control-label" Text="Account Holder " />
                                <asp:TextBox runat="server" ID="txtAccountHolder" CssClass="form-control" Enabled="false" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <u><asp:LinkButton ID="lblReceipt" runat="server" Text="View Receipt" ForeColor="Green" OnClick="lkbViewImage_Click" /></u>
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="Label4" CssClass="control-label" Text="Reject reason* : " />
                                <asp:TextBox runat="server" ID="txtRejectReason" CssClass="form-control" Enabled="true" />
                            </div>

                            <div class="form-group" id="dvBtnProcess">
                                <asp:Button runat="server" ID="btnApprove" Text="Approve" CssClass="btn btn-success btn-lg" OnClick="btnApprove_Click" OnClientClick="javascript:ShowProgressBar()" />
                                <asp:Button runat="server" ID="btnReject" Text="Reject" CssClass="btn btn-danger btn-lg" OnClick="btnReject_Click" OnClientClick="javascript:ShowProgressBar()" />
                            </div>

                            <div class="form-group" id="dvProcessing" style="display: none;">
                                <center><asp:Label runat="server" ID="Label1" Text="Processing" Font-Size="X-Large" ForeColor="Green" /></center>
                            </div>

                            <div class="form-group" style="height: 30px"></div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12"></div>
    </div>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Scripts" runat="server">
    <script type="text/javascript">
        function ShowProgressBar() {
            document.getElementById('dvProcessing').style.display = 'normal';
            document.getElementById('dvBtnProcess').style.display = 'none';
        }

        function HideProgressBar() {
            document.getElementById('dvProgressBar').style.display = "none";
        }
    </script>

    <script type="text/javascript">
        $(window).load(function () {
            if (window.isColorbox) {
                $.colorbox({ href: "../Payout/ViewImage.aspx", iframe: true, width: "80%", height: "80%" });
            }
        });
        function OpenCBox() {
            $.colorbox({ href: "../Payout/ViewImage.aspx", iframe: true, width: "80%", height: "80%" });
        }

    </script>

</asp:Content>
