﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SenderDetail.aspx.cs" Inherits="OMGLiveDemo.Sender.SenderDetail" %>
<%@ MasterType VirtualPath="~/Site.Master" %>

<!DOCTYPE html>

<html lang="en">
<head runat="server" aria-expanded="false">
    <title>AutoPay - Sender's Detail View</title>

    <!-- Bootstrap -->
    <link href="../Content/bootstrap.min.css" rel="stylesheet"/>
    <!-- Font Awesome -->
    <link href="../Content/font-awesome.min.css" rel="stylesheet"/>

    <!-- Custom Theme Style 
    <link href="../Content/base-custom.min.css" rel="stylesheet" />
    -->
    <link href="../Content/custom-alt.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server" class="form-horizontal form-label-left">
        <asp:ScriptManager runat="server" />
        <div class="container body">
            <div class="main_container">
                <div class="right_col" role="main">
                    <div class="page-title">
                        <div class="title_left">
                            <h3>Sender's Details</h3>
                        </div>

                    </div>

                    <div class="clearfix"></div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h4>Sender Basic Information:</h4>
                                </div>
                                <div class="x_content">

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label5" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="Sender Type :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtSenderType" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                        
                                    </div>
                                    
                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label9" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="Sender Name :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtName" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                        
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label1" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="Phone :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtPhone" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                        
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label2" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="Email :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtEmail" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                        
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label3" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="Address :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtAddress" CssClass="form-control" TextMode="MultiLine" Rows="3" Enabled="false" />
                                        </div>
                                        
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label4" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="City :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtCity" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                        
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label16" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="Province :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtProvince" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                        
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label17" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="Postcode :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtPostcode" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                        
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label10" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="Country :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtCountry" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                        
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label15" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="Account Status :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtIsActive" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="form-group" style="height: 30px"></div>

                            <div class="x_panel" runat="server" id="divPersonal" visible="false">
                                <div class="x_title">
                                    <h4>Detail Personal Information:</h4>
                                </div>
                                <div class="x_content">
                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label6" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="ID Type :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtPersonalIDType" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label7" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="ID No :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtPersonalIDNo" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                            <asp:HiddenField runat="server" ID="hfPersonalIDFileURL" />
                                            <asp:HiddenField runat="server" ID="hfPictureWithIDURL" />
                                            <asp:LinkButton ID="lkbPersonalIDFile" runat="server" Text="View ID File" ForeColor="Green" OnClick="lkbPersonalIDFile_Click" />
                                            <br />
                                            <asp:LinkButton ID="lkbPictureWithIDFile" runat="server" Text="View Picture with ID File" ForeColor="Green" OnClick="lkbPictureWithIDFile_Click" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label8" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="Validity :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtPersonalIDValidity" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                    </div>

                                     <div class="form-group">
                                        <asp:Label runat="server" ID="Label12" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="Gender :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtPersonalGender" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label13" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="Occupation :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtPersonalOccupation" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label11" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="Date of Birth :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtPersonalDOB" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label14" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="Place of Birth :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtPersonalPOB" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="x_panel" runat="server" id="divCompany" visible="false">
                                <div class="x_title">
                                    <h4>Detail Company Information:</h4>
                                </div>
                                <div class="x_content">
                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label18" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="Legalitas Type :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtLegalitasType" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label19" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="Legalitas No :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtLegalitasNo" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                            <asp:HiddenField runat="server" ID="hfLegalitasFile" />
                                            <asp:LinkButton ID="lkbLegalitasFile" runat="server" Text="View Legalitas Document File" ForeColor="Green" OnClick="lkbLegalitasFile_Click" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label20" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="NPWP No :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtCompanyNPWP" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                            <asp:HiddenField runat="server" ID="hfCompanyNPWPFile" />
                                            <asp:LinkButton ID="lkbCompanyNPWPFile" runat="server" Text="View NPWP Document File" ForeColor="Green" OnClick="lkbCompanyNPWPFile_Click" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label21" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="Bentuk Badan Hukum :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtCompanyBBH" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label22" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="Business Type :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtBusinessType" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label23" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="Reference :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtBusinessReference" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label24" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="Place of Incorporation :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtCompanyPOI" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label25" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="Date of Incorporation :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtCompanyDOI" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label26" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="Bank :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtBank" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label27" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="Bank Account No :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtBankAccountNo" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label28" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="PIC Name :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtPICName" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label29" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="Department :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtPICDepartment" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label30" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="PIC ID Type :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtPICIDType" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label31" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="PIC ID No :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtPICIDNo" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                            <asp:HiddenField runat="server" ID="hfPICIDFile" />
                                            <asp:LinkButton ID="lkbPICIDFile" runat="server" Text="View PIC ID File" ForeColor="Green" OnClick="lkbPICIDFile_Click" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label32" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="PIC ID Validity :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtPICIDValidity" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label33" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="Company Director/s :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:GridView CssClass="table" ID="gvCDI" runat="server" AutoGenerateColumns="false" DataKeyNames="DirectorID" AllowPaging="True" PageSize="20" AllowCustomPaging="False" ShowHeaderWhenEmpty="true" OnPageIndexChanging="gvCDI_PageIndexChanging" OnRowDataBound="gvCDI_RowDataBound">
                                                <PagerStyle HorizontalAlign="Center" CssClass="bs4-aspnet-pager" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="No.">
                                                        <ItemTemplate>
                                                            <asp:HiddenField runat="server" ID="hfCDICompanyID" Value='<%# Eval("CompanyID") %>' />
                                                            <asp:HiddenField runat="server" ID="hfCDIDirectorID" Value='<%# Eval("DirectorID") %>' />
                                                            <%# Container.DataItemIndex + 1 %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="Name" HeaderText="Director Name" />
                                                    <asp:BoundField DataField="IdentityTypeName" HeaderText="ID Type" />
                                                    <asp:BoundField DataField="IDNo" HeaderText="ID No." />
                                                    <asp:BoundField DataField="Phone" HeaderText="Phone No." />
                                                    <asp:TemplateField HeaderText="Action">
                                                        <ItemTemplate>
                                                            <asp:Button ID="btnViewDirector" CssClass="form-control" runat="server" Text="View" OnClick="btnViewDirector_Click" CausesValidation="false" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label34" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="Company Major Shareholder/s :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:GridView CssClass="table" ID="gvSHI" runat="server" AutoGenerateColumns="false" DataKeyNames="ShareholderID" AllowPaging="True" PageSize="20" AllowCustomPaging="False" ShowHeaderWhenEmpty="true" OnPageIndexChanging="gvSHI_PageIndexChanging" OnRowDataBound="gvSHI_RowDataBound">
                                                <PagerStyle HorizontalAlign="Center" CssClass="bs4-aspnet-pager" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="No.">
                                                        <ItemTemplate>
                                                            <asp:HiddenField runat="server" ID="hfSHICompanyID" Value='<%# Eval("CompanyID") %>' />
                                                            <asp:HiddenField runat="server" ID="hfSHIShareholderID" Value='<%# Eval("ShareholderID") %>' />
                                                            <%# Container.DataItemIndex + 1 %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="Name" HeaderText="Shareholder Name" />
                                                    <asp:BoundField DataField="IdentityTypeName" HeaderText="ID Type" />
                                                    <asp:BoundField DataField="IDNo" HeaderText="ID No." />
                                                    <asp:BoundField DataField="Phone" HeaderText="Phone No." />
                                                    <asp:TemplateField HeaderText="Action">
                                                        <ItemTemplate>
                                                            <asp:Button ID="btnViewShareholder" CssClass="form-control" runat="server" Text="View" OnClick="btnViewShareholder_Click" CausesValidation="false" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
