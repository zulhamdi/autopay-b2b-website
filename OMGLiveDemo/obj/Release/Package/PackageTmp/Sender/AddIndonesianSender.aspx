﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddIndonesianSender.aspx.cs" Inherits="OMGLiveDemo.Sender.AddIndonesianSender" MaintainScrollPositionOnPostback="true" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManagerAddIndonesianSender" runat="server" />

    <div class="row">
        <div class="row x_title">
            <div class="col-md-12">
                <h3>Add Indonesian Sender</h3>
            </div>
        </div>

        <div class="col-md-2 col-sm-12 col-xs-12"></div>
        <div class="col-md-8 col-sm-12 col-xs-12">
            <div class="dashboard_graph">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div style="width: 100%;">
                        <div id="canvas_dahs" class="demo-placeholder" style="width: 100%; height: 100%;">
                            <div class="form-group" style="height: 30px"></div>

                            <div class="alert alert-danger alert-dismissible fade in" runat="server" id="diverror" visible="false">
                                <button type="button" runat="server" id="btnError" onserverclick="btnError_ServerClick" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <div class="centered"><asp:Label runat="server" ID="lblError" /></div>
                            </div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblSenderType" CssClass="control-label" Text="Sender Type * " />
                                <asp:DropDownList runat="server" ID="ddlSenderType" CssClass="form-control" OnSelectedIndexChanged="ddlSenderType_SelectedIndexChanged" AutoPostBack="true" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblName" CssClass="control-label" Text="Full Name * " />
                                <asp:TextBox runat="server" ID="txtName" CssClass="form-control" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblCity" CssClass="control-label" Text="City * " />
                                <asp:TextBox runat="server" ID="txtCity" CssClass="form-control" Enabled="false" />
                                <div style="margin-top: 5px;">
                                    <asp:Button runat="server" ID="btnCheckCity" Text="Find" CssClass="btn btn-success btn-sm" OnClick="btnCheckCity_Click" />
                                </div>
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblAddress" CssClass="control-label" Text="Address * " />
                                <asp:TextBox runat="server" ID="txtAddress" CssClass="form-control" TextMode="MultiLine" Rows="3"/>
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblProvince" CssClass="control-label" Text="Province * " />
                                <asp:TextBox runat="server" ID="txtProvince" CssClass="form-control" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblPostcode" CssClass="control-label" Text="Postcode * " />
                                <asp:TextBox runat="server" ID="txtPostcode" CssClass="form-control" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblPhone" CssClass="control-label" Text="Phone * " />
                                <asp:TextBox runat="server" ID="txtPhone" CssClass="form-control" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblEmail" CssClass="control-label" Text="Email * " />
                                <asp:TextBox runat="server" ID="txtEmail" CssClass="form-control" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <%--================================ PERSONAL SENDER DEFINITION =======================================--%>
                            <%--================================ PERSONAL SENDER DEFINITION =======================================--%>

                            <div id="divpersonal" runat="server" class="form-horizontal">

                                <div class="form-group bottom-border" style="height: 5px"></div>

                                <h2>Sender Detail Information</h2>

                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblNationalIDType" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="ID Type * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:DropDownList runat="server" ID="ddlNationalIDType" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlNationalIDType_SelectedIndexChanged">
                                            <asp:ListItem Text="Select ID Type" Value="-1" />
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblNationalID" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="ID No * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:TextBox runat="server" ID="txtNationalID" CssClass="form-control" />
                                    </div>
                                </div>
                                

                                <div class="form-group"></div>
                                <div class="form-group" id="divNationalIDValidityType" runat="server" visible="false">
                                    <asp:Label runat="server" ID="lblNationalIDValidityType" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Validity Type * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:CheckBox ID="cbNationalIDValidityType" runat="server" Text="Seumur Hidup" Checked="false" AutoPostBack="True" OnCheckedChanged="cbNationalIDValidityType_CheckedChanged"/>
                                    </div>
                                </div>

                                <div class="form-group" id="divNationalIDValidityDate" runat="server">
                                    <asp:Label runat="server" ID="lblNationalIDExpiry" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="ID Expiry * "/>
                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                        <asp:TextBox ID="nationalIDExpiry" CssClass="form-control" runat="server" />
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblNationalIDUpload" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Image of ID * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:FileUpload ID="fuNationalID" runat="server" CssClass="form-control" />
                                        <asp:Label runat="server" ID="lblUploadedNationalIDFile" Text="Uploaded File = NONE"/>
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblSenderWithID" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Sender picture with ID * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:FileUpload ID="fuSenderWithID" runat="server" CssClass="form-control" />
                                        <asp:Label runat="server" ID="lblUploadedSenderWithIDFile" Text="Uploaded File = NONE"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblSenderNationality" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Nationality * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:DropDownList runat="server" ID="ddlSenderNationality" CssClass="form-control" AutoPostBack="false" AppendDataBoundItems="true">
                                            <asp:ListItem Text="Select Nationaliy" Value="-1" />
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblSenderOccupation" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Occupation * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:TextBox ID="txtSenderOccupation" CssClass="form-control" runat="server" />
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblSenderSex" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Sex * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:DropDownList runat="server" ID="ddlSenderSex" CssClass="form-control" AutoPostBack="false" AppendDataBoundItems="true">
                                            <asp:ListItem Selected="True" Text="Male" Value="1" />
                                            <asp:ListItem Text="Female" Value="2" />
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblSenderDOB" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Date of Birth * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:TextBox ID="senderDOB" CssClass="form-control" runat="server" />
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblPOB" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Place of Birth * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:TextBox ID="txtPOB" CssClass="form-control" runat="server" />
                                    </div>
                                </div>
                            </div>

                            <%--================================ COMPANY DEFINITION =======================================--%>
                            <%--================================ COMPANY DEFINITION =======================================--%>

                            <div id="divcompany" runat="server" class="form-horizontal" visible="false">
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblLegalitasType" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Legalitas Type * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:DropDownList runat="server" ID="ddlLegalitasType" CssClass="form-control" AutoPostBack="false" AppendDataBoundItems="true">
                                            <asp:ListItem Text="Select Legalitas Type" Value="" />
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblLegalitasNo" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Legalitas No. * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:TextBox runat="server" ID="txtLegalitasNo" CssClass="form-control" />
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblUploadLegalitasFile" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Legalitas Attachment * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:FileUpload ID="fuLegalitas" runat="server" CssClass="form-control" />
                                        <asp:Label runat="server" ID="lblUploadedLegalitasFile" Text="Uploaded File = NONE"/>
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblCompanyNPWP" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="NPWP No. * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:TextBox runat="server" ID="txtCompanyNPWPNo" CssClass="form-control" />
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblUploadCompanyNPWP" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="NPWP Attachment * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:FileUpload ID="fuCompanyNPWP" runat="server" CssClass="form-control" /> 
                                        <asp:Label runat="server" ID="lblUploadedCompanyNPWP" Text="Uploaded File = NONE"/>
                                    </div>
                                </div>

                                <!--
                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblTDPNo" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="TDP No. * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:TextBox runat="server" ID="txtTDP" CssClass="form-control" />
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lbUploadTDP" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="TDP Attachment * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:FileUpload ID="fuTDP" runat="server" CssClass="form-control" /> 
                                    </div>
                                </div>
                                -->

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblBentukBadanHukum" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Bentuk Badan Hukum" />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:DropDownList runat="server" ID="ddlBBH" CssClass="form-control" AutoPostBack="false" AppendDataBoundItems="true">
                                            <asp:ListItem Text="Select Bentuk Badan Hukum" Value="" />
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblBusinessType" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Business Type" />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:DropDownList runat="server" ID="ddlBusinessField" CssClass="form-control" AutoPostBack="false" AppendDataBoundItems="true">
                                            <asp:ListItem Text="Select business type" Value="" />
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblBusinessReference" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Reference" />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:TextBox runat="server" ID="txtBusinessReference" CssClass="form-control" />
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblPlaceOfIncorporation" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Place of Incorporation * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:TextBox runat="server" ID="txtPlaceOfIncorporation" CssClass="form-control" />
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblDateOfIncorporation" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Date of Incorporation * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:TextBox runat="server" ID="incorporationDate" CssClass="form-control" />
                                    </div>
                                </div>

                                <!--

                                <div class="form-group" style="height: 5px"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="Label1" CssClass="control-label" Text="SK Menkumham No. * " />
                                    <asp:TextBox runat="server" ID="txtSKMenkumham" CssClass="form-control" />
                                    <asp:FileUpload ID="fuSKMenkumham" runat="server" CssClass="form-control" /> 
                                </div>
                                <div class="form-group" style="height: 5px"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="Label2" CssClass="control-label" Text="NPWP No. * " />
                                    <asp:TextBox runat="server" ID="txtNPWP" CssClass="form-control" />
                                    <asp:FileUpload ID="fuNPWP" runat="server" CssClass="form-control" /> 
                                </div>
                                <div class="form-group" style="height: 5px"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="Label3" CssClass="control-label" Text="SIUP No. * " />
                                    <asp:TextBox runat="server" ID="txtSIUP" CssClass="form-control" />
                                    <asp:FileUpload ID="fuSIUP" runat="server" CssClass="form-control" /> 
                                </div>

                                -->

                                <%--================================ COMPANY BANK INFORMATION =======================================--%>
                                <%--================================ COMPANY BANK INFORMATION =======================================--%>

                                <div class="form-group bottom-border" style="height: 5px"></div>

                                <h2>Bank Information</h2>

                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblBank" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Bank Name * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:DropDownList runat="server" ID="ddlBank" CssClass="form-control" AutoPostBack="false" AppendDataBoundItems="true">
                                            <asp:ListItem Text="Select Bank" Value="" />
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblBankAccount" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Account No. * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:TextBox runat="server" ID="txtBankAccount" CssClass="form-control" />
                                    </div>
                                </div>

                                <%--================================ COMPANY PIC INFORMATION =======================================--%>
                                <%--================================ COMPANY PIC INFORMATION =======================================--%>

                                <div class="form-group bottom-border" style="height: 5px"></div>

                                <h2>Person-in-Charge Information</h2>

                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblPICName" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="PIC Name * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:TextBox runat="server" ID="txtPICName" CssClass="form-control" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblJabatanPIC" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Jabatan : " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:TextBox runat="server" ID="txtJabatanPIC" CssClass="form-control" />
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblPICIDType" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="ID Type * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:DropDownList runat="server" ID="ddlPICIDType" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlPICIDType_SelectedIndexChanged">
                                            <asp:ListItem Text="Select ID Type" Value="" />
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblPICIDNo" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="ID No. * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:TextBox runat="server" ID="txtPICIDNo" CssClass="form-control" />
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group" id="divPICIDValidityType" runat="server" visible="false">
                                    <asp:Label runat="server" ID="lblPICIDValidityType" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Validity Type * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:CheckBox ID="cbPICIDValidityType" runat="server" Text="Seumur Hidup" Checked="false" AutoPostBack="True" OnCheckedChanged="cbPICIDValidityType_CheckedChanged"/>
                                    </div>
                                </div>

                                <div class="form-group" id="divPICIDValidityDate" runat="server">
                                    <asp:Label runat="server" ID="lblPICIDExpiry" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="PIC ID Expiry * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:TextBox ID="picIDExpiry" CssClass="form-control" runat="server" />
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblPICIDUpload" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="ID Attachment * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:FileUpload ID="fuPICID" runat="server" CssClass="form-control" />
                                        <asp:Label runat="server" ID="lblUploadedPICIDFile" Text="Uploaded File = NONE"/>
                                    </div>
                                </div>

                                <%--================================ COMPANY DIRECTOR INFORMATION =======================================--%>
                                <%--================================ COMPANY DIRECTOR INFORMATION =======================================--%>
                                <div class="form-group bottom-border" style="height: 5px"></div>

                                <h2>Company Director Information</h2>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:GridView CssClass="table table-striped jambo_table bulk_action" ID="gvCDI" runat="server" AutoGenerateColumns="false" DataKeyNames="DirectorID" AllowPaging="True" PageSize="20" AllowCustomPaging="False" ShowHeaderWhenEmpty="true" OnPageIndexChanging="gvCDI_PageIndexChanging">
                                        <PagerStyle HorizontalAlign="Center" CssClass="bs4-aspnet-pager" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="No.">
                                                <ItemTemplate>
                                                    <asp:HiddenField runat="server" ID="hfCDICompanyID" Value='<%# Eval("TMPCompanyID") %>' />
                                                    <asp:HiddenField runat="server" ID="hfCDIDirectorID" Value='<%# Eval("DirectorID") %>' />
                                                    <%# Container.DataItemIndex + 1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Name" HeaderText="Director Name" />
                                            <asp:BoundField DataField="IdentityTypeName" HeaderText="Identity Type" />
                                            <asp:BoundField DataField="IDNo" HeaderText="Identity No." />
                                            <asp:BoundField DataField="Phone" HeaderText="Phone No." />
                                            <asp:TemplateField HeaderText="Action">
                                                <ItemTemplate>
                                                    <asp:Button ID="btnRemoveDirector" CssClass="form-control" runat="server" Text="Remove" OnClick="btnRemoveDirector_Click" CausesValidation="false" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>

                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblDirectorName" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Director Name * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:TextBox runat="server" ID="txtDirectorName" CssClass="form-control" />
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblDirectorIDType" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="ID Type * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:DropDownList runat="server" ID="ddlDirectorIDType" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlDirectorIDType_SelectedIndexChanged">
                                            <asp:ListItem Text="Select ID Type" Value="" />
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblDirectorIDNo" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="ID No. * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:TextBox runat="server" ID="txtDirectorIdentityNo" CssClass="form-control" />
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group" id="divDirectoryIDValidityType" runat="server" visible="false">
                                    <asp:Label runat="server" ID="lblDirectorIDValidityType" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Validity Type * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:CheckBox ID="cbDirectorIDValidityType" runat="server" Text="Seumur Hidup" Checked="false" AutoPostBack="True" OnCheckedChanged="cbDirectorIDValidityType_CheckedChanged"/>
                                    </div>
                                </div>

                                <div class="form-group" id="divDirectorIDValidityDate" runat="server">
                                    <asp:Label runat="server" ID="lblCompanyDirectorIDExpiry" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Director ID Expiry * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:TextBox ID="companyDirectorIDExpiry" CssClass="form-control" runat="server" />
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblCompanyDirectorIDAttachment" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="ID Attachment * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:FileUpload ID="fuCompanyDirectorIDFile" runat="server" CssClass="form-control" />
                                        <asp:Label runat="server" ID="lblUploadedCompanyDirectorIDFile" Text="Uploaded File = NONE"/>
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblCompanyDirectorNPWPNo" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="NPWP No. : " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:TextBox runat="server" ID="txtDirectorNPWPNo" CssClass="form-control" />
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblCompanyDirectorSex" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Sex * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:DropDownList runat="server" ID="ddlDirectorSex" CssClass="form-control" AutoPostBack="false" AppendDataBoundItems="true">
                                            <asp:ListItem Selected="True" Text="Male" Value="1" />
                                            <asp:ListItem Text="Female" Value="2" />
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblDirectorDOB" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Date of Birth * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:TextBox runat="server" ID="companyDirectorDOB" CssClass="form-control" />
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblDirectorPOB" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Place of Birth : " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:TextBox runat="server" ID="txtDirectorPOB" CssClass="form-control" />
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblDirectorNationality" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Nationality * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:DropDownList runat="server" ID="ddlDirectorNationality" CssClass="form-control" AutoPostBack="false" AppendDataBoundItems="true">
                                            <asp:ListItem Text="Select Nationaliy" Value="-1" />
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblDirectorReligion" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Religion * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:DropDownList runat="server" ID="ddlDirectorReligion" CssClass="form-control" AutoPostBack="false" AppendDataBoundItems="true">
                                            <asp:ListItem Text="Select Religion" Value="-1" />
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblDirectorMaritalStatus" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Marital Status * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:DropDownList runat="server" ID="ddlDirectorMaritalStatus" CssClass="form-control" AutoPostBack="false" AppendDataBoundItems="true">
                                            <asp:ListItem Text="Select Marital Status" Value="-1" />
                                            <asp:ListItem Text="Single" Value="1" />
                                            <asp:ListItem Text="Married" Value="2" />
                                            <asp:ListItem Text="Divorced" Value="1" />
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblDirectorEducationLevel" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Education * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:DropDownList runat="server" ID="ddlDirectorEducationLevel" CssClass="form-control" AutoPostBack="false" AppendDataBoundItems="true">
                                            <asp:ListItem Text="Select Education Level" Value="-1" />
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblDirectorAddressStatus" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Address Status * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:DropDownList runat="server" ID="ddlDirectorAddressStatus" CssClass="form-control" AutoPostBack="false" AppendDataBoundItems="true">
                                            <asp:ListItem Text="Select Address Status" Value="-1" />
                                            <asp:ListItem Text="Own" Value="1" />
                                            <asp:ListItem Text="Rental" Value="2" />
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblDirectorAddress" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Address * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                       <asp:TextBox runat="server" ID="txtDirectorAddress" CssClass="form-control" TextMode="MultiLine" Rows="3"/>
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblDirectorCity" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="City : " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:TextBox runat="server" ID="txtDirectorCity" CssClass="form-control" />
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblDirectorProvince" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Province : " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:TextBox runat="server" ID="txtDirectorProvince" CssClass="form-control" />
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblDirectorCountry" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Country * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:DropDownList runat="server" ID="ddlDirectorCountryM49" CssClass="form-control" AutoPostBack="false" AppendDataBoundItems="true">
                                            <asp:ListItem Text="Select Country " Value="-1" />
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblDirectorPostcode" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Postcode * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:TextBox runat="server" ID="txtDirectorPostcode" CssClass="form-control" />
                                        </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblDirectorPhone" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Phone * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:TextBox runat="server" ID="txtDirectorPhone" CssClass="form-control" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="centered"><asp:Button runat="server" ID="btnAddDirector" Text="Add Director" CssClass="btn btn-success btn-lg" OnClick="btnAddDirector_Click" /></div>
                                </div>

                                <%--================================ COMPANY SIGNIFICANT SHAREHOLDER INFORMATION =======================================--%>
                                <%--================================ COMPANY SIGNIFICANT SHAREHOLDER INFORMATION =======================================--%>
                                <div class="form-group bottom-border" style="height: 5px"></div>

                                <h2>Share Holder Information ( > 25% Shareholding )</h2>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:GridView CssClass="table table-striped jambo_table bulk_action" ID="gvSHI" runat="server" AutoGenerateColumns="false" DataKeyNames="ShareholderID" AllowPaging="True" PageSize="20" AllowCustomPaging="False" ShowHeaderWhenEmpty="true" OnPageIndexChanging="gvSHI_PageIndexChanging">
                                        <PagerStyle HorizontalAlign="Center" CssClass="bs4-aspnet-pager" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="No.">
                                                <ItemTemplate>
                                                    <asp:HiddenField runat="server" ID="hfSHICompanyID" Value='<%# Eval("TMPCompanyID") %>' />
                                                    <asp:HiddenField runat="server" ID="hfSHIShareholderID" Value='<%# Eval("ShareholderID") %>' />
                                                    <%# Container.DataItemIndex + 1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Name" HeaderText="Shareholder Name" />
                                            <asp:BoundField DataField="IdentityTypeName" HeaderText="Identity Type" />
                                            <asp:BoundField DataField="IDNo" HeaderText="Identity No." />
                                            <asp:BoundField DataField="Phone" HeaderText="Phone No." />
                                            <asp:TemplateField HeaderText="Action">
                                                <ItemTemplate>
                                                    <asp:Button ID="btnRemoveShareholder" CssClass="form-control" runat="server" Text="Remove" OnClick="btnRemoveShareholder_Click" CausesValidation="false" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                                
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblShareholderName" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Shareholder Name * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:TextBox runat="server" ID="txtShareholderName" CssClass="form-control" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblShareholdingType" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Shareholder Type * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:DropDownList runat="server" ID="ddlHoldingType" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true">
                                            <asp:ListItem Text="Individual" Value="1" />
                                            <asp:ListItem Text="Company" Value="2" />
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblShareholderIDType" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="ID Type * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:DropDownList runat="server" ID="ddlShareholderIDType" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlShareholderIDType_SelectedIndexChanged">
                                            <asp:ListItem Text="Select ID Type" Value="" />
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblShareholderIDNo" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="ID No. * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:TextBox runat="server" ID="txtShareholderIDNo" CssClass="form-control" />
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group" id="divShareholderIDValidityType" runat="server" visible="false">
                                    <asp:Label runat="server" ID="lblShareholderIDValidityType" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Validity Type * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:CheckBox ID="cbShareholderIDValidityType" runat="server" Text="Seumur Hidup" Checked="false" AutoPostBack="True" OnCheckedChanged="cbShareholderIDValidityType_CheckedChanged"/>
                                    </div>
                                </div>
                                <div class="form-group" id="divShareholderIDValidityDate" runat="server">
                                    <asp:Label runat="server" ID="lblShareholderIDExpiry" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Shareholder ID Expiry * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:TextBox ID="companyShareholderIDExpiry" CssClass="form-control" runat="server" />
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblShareholderIDAttachment" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="ID Attachment * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:FileUpload ID="fuCompanyShareholderIDFile" runat="server" CssClass="form-control" />
                                        <asp:Label runat="server" ID="lblUploadedShareholderIDFile" Text="Uploaded File = NONE"/>
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblShareholderNPWPNo" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="NPWP No. : " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:TextBox runat="server" ID="txtShareholderNPWPNo" CssClass="form-control" />
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblShareholderSex" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Sex * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:DropDownList runat="server" ID="ddlShareholderSex" CssClass="form-control" AutoPostBack="false" AppendDataBoundItems="true">
                                            <asp:ListItem Selected="True" Text="Male" Value="1" />
                                            <asp:ListItem Text="Female" Value="2" />
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblShareholderDOB" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Date of Birth * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:TextBox runat="server" ID="companyShareholderDOB" CssClass="form-control" />
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblShareholderPOB" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Place of Birth : " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:TextBox runat="server" ID="txtShareholderPOB" CssClass="form-control" />
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblShareholderNationality" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Nationality * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:DropDownList runat="server" ID="ddlShareholderNationality" CssClass="form-control" AutoPostBack="false" AppendDataBoundItems="true">
                                            <asp:ListItem Text="Select Nationaliy" Value="-1" />
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblShareholderReligion" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Religion * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:DropDownList runat="server" ID="ddlShareholderReligion" CssClass="form-control" AutoPostBack="false" AppendDataBoundItems="true">
                                            <asp:ListItem Text="Select Religion" Value="-1" />
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblShareholderMaritalStatus" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Marital Status * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:DropDownList runat="server" ID="ddlShareholderMaritalStatus" CssClass="form-control" AutoPostBack="false" AppendDataBoundItems="true">
                                            <asp:ListItem Text="Select Marital Status" Value="-1" />
                                            <asp:ListItem Text="Single" Value="1" />
                                            <asp:ListItem Text="Married" Value="2" />
                                            <asp:ListItem Text="Divorced" Value="1" />
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblShareholderEducationLevel" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Education * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:DropDownList runat="server" ID="ddlShareholderEducationLevel" CssClass="form-control" AutoPostBack="false" AppendDataBoundItems="true">
                                            <asp:ListItem Text="Select Education Level" Value="-1" />
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblShareholderAddressStatus" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Address Status * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:DropDownList runat="server" ID="ddlShareholderAddressStatus" CssClass="form-control" AutoPostBack="false" AppendDataBoundItems="true">
                                            <asp:ListItem Text="Select Address Status" Value="-1" />
                                            <asp:ListItem Text="Own" Value="1" />
                                            <asp:ListItem Text="Rental" Value="2" />
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblShareholderAddress" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Address * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:TextBox runat="server" ID="txtShareholderAddress" CssClass="form-control" TextMode="MultiLine" Rows="3"/>
                                    </div>
                                </div>
                                <div class="form-group" style="height: 5px"></div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblShareholderCity" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="City : " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:TextBox runat="server" ID="txtShareholderCity" CssClass="form-control" />
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblShareholderProvince" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Province : " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:TextBox runat="server" ID="txtShareholderProvince" CssClass="form-control" />
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblShareholderCountry" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Country * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:DropDownList runat="server" ID="ddlShareholderCountryM49" CssClass="form-control" AutoPostBack="false" AppendDataBoundItems="true">
                                            <asp:ListItem Text="Select Country " Value="-1" />
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblShareholderPostcode" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Postcode * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:TextBox runat="server" ID="txtShareholderPostcode" CssClass="form-control" />
                                    </div>
                                </div>

                                <div class="form-group"></div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblShareholderPhone" CssClass="right-padding-6 control-label col-md-3 col-sm-3 col-xs-12" Text="Phone * " />
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <asp:TextBox runat="server" ID="txtShareholderPhone" CssClass="form-control" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="centered"><asp:Button runat="server" ID="btnAddShareholder" Text="Add Shareholder" CssClass="btn btn-success btn-lg" OnClick="btnAddShareholder_Click" /></div>
                                </div>

                                <div class="form-group" style="height: 5px"></div>
                            </div>

                            <div class="form-group">
                                <div class="centered"><asp:Button runat="server" ID="btnAdd" Text="Add Sender" CssClass="btn btn-success btn-lg" OnClick="btnAdd_Click" Width="100%" /></div>
                                <%--<asp:Button runat="server" ID="btnThumbnail" Text="Buat Thumbnail" CssClass="btn btn-success" OnClick="btnThumbnail_Click" />--%>
                            </div>

                            <div class="form-group" style="height: 30px"></div>
                        </div>
                    </div>

                    <%--================================ M O D A L   C I T Y =======================================--%>
                    <%--================================ M O D A L   C I T Y =======================================--%>
                    <%--================================ M O D A L   C I T Y =======================================--%>
                    <%--================================ M O D A L   C I T Y =======================================--%>

                    <div class="modal fade bs-example-modal-lg" id="myModalCity" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">

                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                    <h4 class="modal-title" id="myModalLabel1">Choose City</h4>
                                </div>
                                <div class="modal-body">
                                    <asp:HiddenField runat="server" ID="hfCityID" />
                                    <asp:HiddenField runat="server" ID="hfCityName" />
                                    <asp:HiddenField runat="server" ID="hfModalCityOpen" />
                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label18" CssClass="control-label" Text="City * " />
                                        <asp:TextBox runat="server" ID="txtFindCity" CssClass="form-control" />
                                        <div style="margin-top: 5px">
                                            <asp:Button runat="server" ID="btnFindCity" Text="Find" CssClass="btn btn-success btn-sm" OnClick="btnFindCity_Click" />
                                        </div>
                                    </div>
                                    <div class="form-group" style="height: 5px"></div>

                                    <div id="canvas_dahs11" class="demo-placeholder" style="width: 100%; height: 100%;">
                                        <asp:GridView CssClass="table table-striped jambo_table bulk_action" ID="gvListCity" Visible="false" runat="server" AutoGenerateColumns="false" DataKeyNames="ID">
                                            <Columns>
                                                <asp:TemplateField HeaderText="No.">
                                                    <ItemTemplate>
                                                        <asp:HiddenField runat="server" ID="hfCityIDModal" Value='<%# Eval("ID") %>' />
                                                        <asp:HiddenField runat="server" ID="hfCityNameModal" Value='<%# Eval("City") %>' />
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="City" HeaderText="City" />
                                                <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <asp:Button ID="btnChooseCity" CssClass="form-control" runat="server" Text="Choose" OnClick="btnChooseCity_Click" UseSubmitBehavior="false" CausesValidation="false" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-md-2 col-sm-12 col-xs-12"></div>
    </div>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Scripts" runat="server">
    <script type="text/javascript">
        $(window).load(function () {
            if (window.isModalCity) {
                $('#myModalCity').modal('show');
            }
            else {
                $('#myModalCity').modal('hide');
            }
        });
    </script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" />
    <script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript">
        // When the document is ready
        $(document).ready(function () {
            $("input[id*='nationalIDExpiry']").datepicker({
                format: "yyyy/mm/dd"
            });
            $("input[id*='picIDExpiry']").datepicker({
                format: "yyyy/mm/dd"
            });
            $("input[id*='senderDOB']").datepicker({
                format: "yyyy/mm/dd"
            });
            $("input[id*='companyDirectorIDExpiry']").datepicker({
                format: "yyyy/mm/dd"
            });
            $("input[id*='companyDirectorDOB']").datepicker({
                format: "yyyy/mm/dd"
            });
            $("input[id*='companyShareholderIDExpiry']").datepicker({
                format: "yyyy/mm/dd"
            });
            $("input[id*='companyShareholderDOB']").datepicker({
                format: "yyyy/mm/dd"
            });
            $("input[id*='incorporationDate']").datepicker({
                format: "yyyy/mm/dd"
            });
        });
    </script>
</asp:Content>
