﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AgentDashboard.aspx.cs" Inherits="OMGLiveDemo.AgentDashboard" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" />
    
    <hr />

    <div>
        <h3>Dashboard</h3>
        <div class="form-group">
            <%--<asp:Label runat="server" ID="lblPartner" CssClass="control-label" Text="Partner : " />--%>
            <asp:DropDownList runat="server" ID="ddlBalance" CssClass="form-control" OnSelectedIndexChanged="ddlBalance_SelectedIndexChanged" AutoPostBack="true" />
        </div>
    </div>

    <div class="row top_tiles">
        <div class="animated flipInY col-lg-4 col-md-4 col-sm-4 col-xs-12" runat="server" id="divbalance" onclick="javascript:divbalanceclicked(); return true;">
            <asp:Button runat="server" ID="btnDivBalance" Style="display: none" OnClick="btnDivBalance_Click" />
            <div class="tile-stats">
                <div class="icon"><i class="fa fa-money"></i></div>
                <div class="count success"><asp:LinkButton runat="server" ID="lkbBalance" Text="0" OnClick="btnDivBalance_Click" /></div>
                <h3><asp:Label runat="server" ID="lblBalance" Text="Fee Agent" /></h3>
                <p><i><asp:Label runat="server" ID="Label1" Text="-" /></i></p>
            </div>
        </div>
        <div class="animated flipInY col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="tile-stats">
                <div class="icon"><i class="fa fa-money"></i></div>
                <div class="count"><asp:LinkButton runat="server" ID="lbkTraPending" Text="0" OnClick="btnDivPendingTransaction_Click" /></div>
                <h3>Transfer Pending</h3>
                <p><i><asp:Label runat="server" ID="lblTraPendingPercent" Text="0" />%</i> From all transfer</p>
            </div>
        </div>
        <div class="animated flipInY col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="tile-stats">
                <div class="icon"><i class="fa fa-money"></i></div>
                <div class="count green"><asp:LinkButton runat="server" ForeColor="#009900" ID="lbkTraSuccess" Text="0" OnClick="btnDivSuccessTransaction_Click" /></div>
                <h3>Transfer Success</h3>
                <p><i class="green"><asp:Label runat="server" ForeColor="#009900" ID="lblTraSuccessPrecent" Text="0" />%</i> From all transfer</p>
            </div>
        </div>
    </div>

    

    <div class="row top_tiles">
        <div class="animated flipInY col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="tile-stats">
                <div class="icon"><i class="fa fa-money"></i></div>
                <div class="count blue"><asp:LinkButton runat="server" ForeColor="#0066ff" ID="lbkTraProcess" Text="0" OnClick="btnDivProcessTransaction_Click" /></div>
                <h3>Transfer Processed</h3>
                <p><i class="blue"><asp:Label runat="server" ID="lblTraProcessPrecent" Text="0" />%</i> From all transfer</p>
            </div>
        </div>
        <div class="animated flipInY col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="tile-stats">
                <div class="icon"><i class="fa fa-money"></i></div>
                <div class="count red"><asp:LinkButton runat="server" ForeColor="#cc0000" ID="lbkTraRejected" Text="0" OnClick="btnDivRejectedTransaction_Click" /></div>
                <h3>Transfer Rejected</h3>
                <p><i class="red"><asp:Label runat="server" ID="lblTraRejectPercent" Text="0" />%</i> From all transfer</p>
            </div>
        </div>
        <div class="animated flipInY col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="tile-stats">
                <div class="icon"><i class="fa fa-money"></i></div>
                <div class="count red"><asp:LinkButton runat="server" ForeColor="#cc0000" ID="lbkTraFailed" Text="0" OnClick="btnDivFailedTransaction_Click" /></div>
                <h3>Transfer Failed</h3>
                <p><i class="red"><asp:Label runat="server" ID="lblTraFailedPercent" Text="0" />%</i> From all transfer</p>
            </div>
        </div>
    </div>

    <div class="row">
        <div visible="false" runat="server" id="divva">
            <div class="form-group">
                <div class="alert alert-warning fade in">
                    <div class="form-group"></div>
                    <center><img src='<%= ResolveUrl("~/Content/images/bank_bni.png")%>' alt="..." width="100px" height="33px"></center>
                    <center><asp:Label runat="server" ID="lblVA" Font-Bold="true" Text="BNI Virtual Account" Font-Size="XX-Large" /></center>
                    <%--<hr  />
                    <div style="height: 2px; background-color:white" />--%>
                    <center><asp:Label runat="server" ID="lblVAName" Font-Bold="true" Text="" Font-Size="Large" /></center>
                    <center><asp:Label runat="server" ID="lblVABank" Font-Bold="true" Text="BNI Virtual Account" Font-Size="Medium" /></center>
                    <div class="form-group"></div>
                </div>
            </div>
        </div>
        <div visible="false" runat="server" id="divrate1">
            <div class="form-group">
                <div class="alert alert-danger fade in">
                    <center><asp:Label runat="server" ID="lblRate1" Font-Bold="true" Text="Malaysia to Indonesia" Font-Size="X-Large" /></center>
                    <center>Current Rate</center>
                    <center><asp:Label runat="server" ID="lblRate" Font-Bold="true" Font-Size="Medium" /></center>
                    <center><asp:Label runat="server" ID="lblRateUpdate"/></center>
                    <%--<center><asp:Label runat="server" ID="lblMargin" Font-Bold="true" Font-Size="Medium"/></center>--%>
                    <div class="form-group" style="height: 5px"></div>
                    <center>
                        <asp:Button runat="server" ID="btnUpdateMarginMYR" Text="Update Margin" CssClass="btn btn-default" OnClick="btnUpdateMargin_Click" />
                        <asp:Button runat="server" ID="btnCalc" Text="Calculator" CssClass="btn btn-success" OnClick="btnCalc_Click" />
                    </center>
                </div>
            </div>
        </div>
        <div visible="false" runat="server" id="divrate2">
            <div class="form-group">
                <div class="alert alert-danger fade in">
                    <center><asp:Label runat="server" ID="Label1x" Font-Bold="true" Text="Indonesia to Malaysia" Font-Size="X-Large" /></center>
                    <center>Current Rate</center>
                    <center><asp:Label runat="server" ID="lblRateIDR" Font-Bold="true" Font-Size="Medium" /></center>
                    <center><asp:Label runat="server" ID="lblRateUpdateIDR"/></center>
                    <div class="form-group" style="height: 5px"></div>
                    <center>
                        <asp:Button runat="server" ID="btnUpdateMargin" Text="Update Margin" CssClass="btn btn-default" OnClick="btnUpdateMargin_Click" />
                        <asp:Button runat="server" ID="btnCalc2" Text="Calculator" CssClass="btn btn-success" OnClick="btnCalc_Click" />

                    </center>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph" style="margin-bottom: 30px">

                <div class="row x_title">
                    <div class="col-md-12">
                        <h3>Last 20 Transfer</h3>
                    </div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div style="width: 100%;">
                        <div id="canvas_dahs" class="demo-placeholder" style="width: 100%; height: 100%;">
                            <asp:GridView CssClass="table table-striped jambo_table bulk_action" ID="gvListItem" runat="server" AutoGenerateColumns="false" DataKeyNames="PayoutID">
                                <Columns>
                                    <asp:TemplateField HeaderText="No.">
                                        <ItemTemplate>
                                            <asp:HiddenField runat="server" ID="hfPayoutID" Value='<%# Eval("PayoutID") %>' />
                                            <%--<asp:HiddenField runat="server" ID="hfReference" Value='<%# Eval("Reference") %>' />--%>
                                            <asp:HiddenField runat="server" ID="hfAmount" Value='<%# Eval("Amount") %>' />
                                            <asp:HiddenField runat="server" ID="hfRate" Value='<%# Eval("Rate") %>' />
                                            <asp:HiddenField runat="server" ID="hfTotalAmount" Value='<%# Eval("TotalAmount") %>' />
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="SenderName" HeaderText="Sender Name" />
                                    <asp:BoundField DataField="BeneficiaryName" HeaderText="Beneficiary Name" />
                                    <asp:BoundField DataField="BeneficiaryAccount" HeaderText="Beneficiary Account" />
                                    <asp:BoundField DataField="BankName" HeaderText="Beneficiary Bank" />
                                    <asp:BoundField DataField="RateText" HeaderText="Rate" />
                                    <asp:BoundField DataField="AmountText" HeaderText="Amount" />
                                    <asp:BoundField DataField="AdminFee" HeaderText="Administration Fee" />
                                    <asp:BoundField DataField="TotalAmountText" HeaderText="Total Amount" />
                                    <asp:BoundField DataField="TotalTransferText" HeaderText="Total Transfer" />
                                    <asp:BoundField DataField="Notes" HeaderText="Notes" />
                                    <asp:BoundField DataField="Status" HeaderText="Status" />
                                    <asp:BoundField DataField="CreatedDate" HeaderText="Created Date" />
                                    <asp:BoundField DataField="ModifiedDate" HeaderText="Updated Date" />
                                    <asp:TemplateField HeaderText="Receipt">
                                        <ItemTemplate>
                                            <asp:Button ID="btnReceipt" CssClass="form-control" Visible='<%# IsCompleted((string)Eval("Status")) %>' runat="server" Text="Print Receipt" OnClick="btnReceipt_Click" CausesValidation="false" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <asp:Button runat="server" ID="btnShowAll" OnClick="btnShowAll_Click" Text="Show All Transfer" CssClass="btn btn-success" />
                    <div class="form-group" style="height: 5px"></div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="Scripts" runat="server">
    
    <script>
        function divbalanceclicked() {
            var btn = document.getElementById('btnDivBalance');
            if (btn != null) {
                btn.click();
            }
        }
    </script>

    <script src='<%= ResolveUrl("~/Scripts/Gentellela/js/moment.min.js")%>'></script>
    <script src='<%= ResolveUrl("~/Scripts/Gentellela/js/daterangepicker.js")%>'></script>
    <link href="Content/daterangepicker.css" rel="stylesheet">

</asp:Content>
