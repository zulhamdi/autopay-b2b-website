﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AdministrationFee.aspx.cs" Inherits="OMGLiveDemo.AdministrationFee" MaintainScrollPositionOnPostback="true" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" />
    <div class="row">
        <div class="row x_title">
            <div class="col-md-12">
                <h3>Administration Fee</h3>
            </div>
        </div>

        <div class="col-md-3 col-sm-12 col-xs-12">
        </div>

        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="dashboard_graph">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div style="width: 100%;">
                        <div id="canvas_dahs" class="demo-placeholder" style="width: 100%; height: 100%;">
                            <div class="form-group" style="height: 30px"></div>
                            
                            <div class="alert alert-danger alert-dismissible fade in" runat="server" id="diverrorMYR" visible="false">
                                <button type="button" runat="server" id="btnErrorMYR" onserverclick="btnErrorMYR_ServerClick" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <center><asp:Label runat="server" ID="lblErrorMYR" /></center>
                            </div>
                            
                            <div class="alert alert-success alert-dismissible fade in" runat="server" id="divsuccessMYR" visible="false">
                                <button type="button" runat="server" id="btnSuccessMYR" onserverclick="btnSuccessMYR_ServerClick" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <center><asp:Label runat="server" ID="lblSuccessMYR" /></center>
                            </div>

                            <div class="form-group">
                                <asp:Label runat="server" CssClass="control-label" Text="Country : " />
                                <br />
                                <asp:Label runat="server" ID="lblCountryMYR" Font-Bold="true" Font-Size="X-Large" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" CssClass="control-label" Text="Currency : " />
                                <br />
                                <asp:Label runat="server" ID="lblCurrencyMYR" Font-Bold="true" Font-Size="X-Large" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblAdminFeeMYR" CssClass="control-label" Text="Admin Fee (in MYR) *" />
                                <asp:TextBox runat="server" ID="txtAdminFeeMYR" CssClass="form-control" AutoPostBack="true" Font-Size="20pt" TextMode="MultiLine" Rows="1"/>
                            </div>
                            <div class="form-group" style="height: 10px"></div>

                            <div class="form-group">
                                <center><asp:Button runat="server" ID="btnUpdateMYR" Text="Update" CssClass="btn btn-success btn-lg" OnClick="btnUpdateMYR_Click" Width="100%" /></center>
                            </div>

                            <div class="form-group" style="height: 25px"></div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="col-md-3 col-sm-12 col-xs-12">
        </div>
    </div>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Scripts" runat="server">
</asp:Content>
