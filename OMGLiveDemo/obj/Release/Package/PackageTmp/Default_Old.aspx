﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default_Old.aspx.cs" Inherits="OMGLiveDemo._DefaultOld" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" />
    <hr />

    <h3>Dashboard</h3>

    <div class="row top_tiles">
        <div class="animated flipInY col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="tile-stats">
                <div class="icon"><i class="fa fa-user-secret"></i></div>
                <div class="count"><asp:LinkButton runat="server" ID="lbkMasterArea" Text="0" OnClick="btnDivListAgent_Click" /></div>
                <h3>Total Master Area</h3>
                <p><i><asp:Label runat="server" ID="Label1" Text="-" /></i></p>
            </div>
        </div>
        <div class="animated flipInY col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="tile-stats">
                <div class="icon"><i class="fa fa-user-secret"></i></div>
                <div class="count"><asp:LinkButton runat="server" ID="lbkAgents" Text="0" OnClick="btnDivListAgent_Click" /></div>
                <h3>Total Agent</h3>
                <p><i><asp:Label runat="server" ID="Label123" Text="-" /></i></p>
            </div>
        </div>
        <div class="animated flipInY col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="tile-stats">
                <div class="icon"><i class="fa fa-calculator"></i></div>
                <div class="count"><asp:LinkButton runat="server" ID="lbkTotalTransferCompleted" Text="0" /></div>
                <h3>Total Transfer Completed</h3>
                <p><i><asp:Label runat="server" ID="lblTotalTransfer" Text="-" /></i></p>
            </div>
        </div>
    </div>

    <div class="row top_tiles">
        <div class="animated flipInY col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="tile-stats">
                <div class="icon"><i class="fa fa-money"></i></div>
                <div class="count green"><asp:LinkButton runat="server" ID="lbkTraPending" Text="0" OnClick="btnDivSuccessTransaction_Click" /></div>
                <h3>Transfer Pending</h3>
                <p><i class="green"><asp:Label runat="server" ID="lblTraPendingPrecent" Text="0" />%</i> From all transfer</p>
            </div>
        </div>
        <div class="animated flipInY col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="tile-stats">
                <div class="icon"><i class="fa fa-money"></i></div>
                <div class="count green"><asp:LinkButton runat="server" ForeColor="#009900" ID="lbkTraSuccess" Text="0" OnClick="btnDivSuccessTransaction_Click" /></div>
                <h3>Transfer Success</h3>
                <p><i class="green"><asp:Label runat="server" ID="lblTraSuccessPrecent" Text="0" />%</i> From all transfer</p>
            </div>
        </div>
        <div class="animated flipInY col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="tile-stats">
                <div class="icon"><i class="fa fa-money"></i></div>
                <div class="count green"><asp:LinkButton runat="server" ForeColor="#0066ff" ID="lbkTraProcess" Text="0" OnClick="btnDivProcessTransaction_Click" /></div>
                <h3>Transfer Processed</h3>
                <p><i class="blue"><asp:Label runat="server" ID="lblTraProcessPrecent" Text="0" />%</i> From all transfer</p>
            </div>
        </div>
    </div>

    <div class="row top_tiles">
        <div class="animated flipInY col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="tile-stats">
                <div class="icon"><i class="fa fa-money"></i></div>
                <div class="count red"><asp:LinkButton runat="server" ForeColor="#cc0000" ID="lbkTraCancelled" Text="0" OnClick="btnDivRejectedTransaction_Click" /></div>
                <h3>Transfer Cancelled</h3>
                <p><i class="red"><asp:Label runat="server" ID="lblTraCancelledPercent" Text="0" />%</i> From all transfer</p>
            </div>
        </div>
        <div class="animated flipInY col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="tile-stats">
                <div class="icon"><i class="fa fa-money"></i></div>
                <div class="count red"><asp:LinkButton runat="server" ForeColor="#cc0000" ID="lbkTraRejected" Text="0" OnClick="btnDivRejectedTransaction_Click" /></div>
                <h3>Transfer Rejected</h3>
                <p><i class="red"><asp:Label runat="server" ID="lblTraRejectPercent" Text="0" />%</i> From all transfer</p>
            </div>
        </div>
        <div class="animated flipInY col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="tile-stats">
                <div class="icon"><i class="fa fa-money"></i></div>
                <div class="count red"><asp:LinkButton runat="server" ForeColor="#cc0000" ID="lbkTraFailed" Text="0" OnClick="btnDivFailedTransaction_Click" /></div>
                <h3>Transfer Failed</h3>
                <p><i class="red"><asp:Label runat="server" ID="lblTraFailedPercent" Text="0" />%</i> From all transfer</p>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
                <div class="alert alert-danger fade in">
                    <center><asp:Label runat="server" ID="lblRate1" Font-Bold="true" Text="Malaysia to Indonesia" Font-Size="X-Large" /></center>
                    <center>Current Rate</center>
                    <center><asp:Label runat="server" ID="lblRate" Font-Bold="true" Font-Size="Medium" /></center>
                    <center><asp:Label runat="server" ID="lblRateUpdate"/></center>
                    <%--<center><asp:Label runat="server" ID="lblMargin" Font-Bold="true" Font-Size="Medium"/></center>--%>
                    <div class="form-group" style="height: 5px"></div>
                    <center><asp:Button runat="server" ID="btnUpdateMarginMYR" Text="Update Margin" CssClass="btn btn-default" OnClick="btnUpdateMargin_Click" /></center>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="form-group">
                <div class="alert alert-danger fade in">
                    <center><asp:Label runat="server" ID="lblRate2" Font-Bold="true" Text="Indonesia to Malaysia" Font-Size="X-Large" /></center>
                    <center>Current Rate</center>
                    <center><asp:Label runat="server" ID="lblRateIDR" Font-Bold="true" Font-Size="Medium" /></center>
                    <center><asp:Label runat="server" ID="lblRateUpdateIDR"/></center>
                    <div class="form-group" style="height: 5px"></div>
                    <center><asp:Button runat="server" ID="btnUpdateMargin" Text="Update Margin" CssClass="btn btn-default" OnClick="btnUpdateMargin_Click" /></center>
                </div>
            </div>
        </div>
    </div>

    <div class="row" runat="server" id="divpendingtask">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph">

                <div class="row x_title">
                    <div class="col-md-12">
                        <h3>Waiting Approval</h3>
                    </div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div style="width: 100%;">
                        <div id="canvas_dahs" class="demo-placeholder" style="width: 100%; height: 100%;">

                            <asp:GridView CssClass="table table-striped jambo_table bulk_action" ID="gvPendingTask" runat="server" AutoGenerateColumns="false" DataKeyNames="PayoutID">
                                <Columns>
                                    <asp:TemplateField HeaderText="No.">
                                        <ItemTemplate>
                                            <asp:HiddenField runat="server" ID="hfPayoutID" Value='<%# Eval("PayoutID") %>' />
                                            <asp:HiddenField runat="server" ID="hfReference" Value='<%# Eval("Reference") %>' />
                                            <asp:HiddenField runat="server" ID="hfAmount" Value='<%# Eval("Amount") %>' />
                                            <asp:HiddenField runat="server" ID="hfRate" Value='<%# Eval("Rate") %>' />
                                            <asp:HiddenField runat="server" ID="hfTotalAmount" Value='<%# Eval("TotalAmount") %>' />
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="CreatedName" HeaderText="Agent/Admin" />
                                    <asp:BoundField DataField="BeneficiaryName" HeaderText="Beneficiary Name" />
                                    <asp:BoundField DataField="BeneficiaryAccount" HeaderText="Beneficiary Account" />
                                    <asp:BoundField DataField="BankName" HeaderText="Beneficiary Bank" />
                                    <asp:BoundField DataField="RateText" HeaderText="Rate" />
                                    <asp:BoundField DataField="AmountText" HeaderText="Amount" />
                                    <asp:BoundField DataField="AdminFee" HeaderText="Administration Fee" />
                                    <asp:BoundField DataField="TotalAmountText" HeaderText="Total Amount" />
                                    <asp:BoundField DataField="TotalTransferText" HeaderText="Total Transfer" />
                                    <asp:BoundField DataField="Notes" HeaderText="Notes" />
                                    <asp:BoundField DataField="Status" HeaderText="Status" />
                                    <asp:BoundField DataField="CreatedDate" HeaderText="Created Date" />
                                    <asp:BoundField DataField="ModifiedDate" HeaderText="Updated Date" />
                                    <asp:TemplateField HeaderText="Action">
                                        <ItemTemplate>
                                            <asp:Button ID="btnAction" CssClass="form-control" runat="server" Text="Action" OnClick="btnAction_Click" CausesValidation="false"/>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph" style="margin-bottom: 30px">

                <div class="row x_title">
                    <div class="col-md-12">
                        <h3>Last 20 Transfer</h3>
                    </div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div style="width: 100%;">
                        <div id="canvas_dahs" class="demo-placeholder" style="width: 100%; height: 100%;">
                            <asp:GridView CssClass="table table-striped jambo_table bulk_action font11" ID="gvListItem" runat="server" AutoGenerateColumns="false" DataKeyNames="PayoutID">
                                <Columns>
                                    <asp:TemplateField HeaderText="No.">
                                        <ItemTemplate>
                                            <asp:HiddenField runat="server" ID="hfPayoutID" Value='<%# Eval("PayoutID") %>' />
                                            <asp:HiddenField runat="server" ID="hfReference" Value='<%# Eval("Reference") %>' />
                                            <asp:HiddenField runat="server" ID="hfAmount" Value='<%# Eval("Amount") %>' />
                                            <asp:HiddenField runat="server" ID="hfRate" Value='<%# Eval("Rate") %>' />
                                            <asp:HiddenField runat="server" ID="hfTotalAmount" Value='<%# Eval("TotalAmount") %>' />
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="SenderName" HeaderText="Sender Name" />
                                    <asp:BoundField DataField="BeneficiaryName" HeaderText="Beneficiary Name" />
                                    <asp:BoundField DataField="BeneficiaryAccount" HeaderText="Beneficiary Account" />
                                    <asp:BoundField DataField="BankName" HeaderText="Beneficiary Bank" />
                                    <asp:BoundField DataField="RateText" HeaderText="Rate" />
                                    <asp:BoundField DataField="AmountText" HeaderText="Amount" />
                                    <asp:BoundField DataField="AdminFee" HeaderText="Administration Fee" />
                                    <asp:BoundField DataField="TotalAmountText" HeaderText="Total Amount" />
                                    <asp:BoundField DataField="TotalTransferText" HeaderText="Total Transfer" />
                                    <asp:BoundField DataField="Notes" HeaderText="Notes" />
                                    <asp:BoundField DataField="Status" HeaderText="Status" />
                                    <asp:BoundField DataField="CreatedDate" HeaderText="Created Date" />
                                    <asp:BoundField DataField="ModifiedDate" HeaderText="Updated Date" />
                                    <asp:TemplateField HeaderText="Receipt">
                                        <ItemTemplate>
                                            <asp:Button ID="btnReceipt" CssClass="form-control" runat="server" Visible='<%# IsCompleted((string)Eval("Status")) %>' Text="Print Receipt" OnClick="btnReceipt_Click" CausesValidation="false" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <asp:Button runat="server" ID="btnShowAll" OnClick="btnShowAll_Click" Text="Show All Transfer" CssClass="btn btn-success" />
                    <asp:Button runat="server" ID="btnEmail" OnClick="btnEmail_Click" Text="Test Email" CssClass="btn btn-danger" Visible="false" />
                    <div class="form-group" style="height: 5px"></div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="Scripts" runat="server">
    <script src='<%= ResolveUrl("~/Scripts/Gentellela/js/moment.min.js")%>'></script>
    <script src='<%= ResolveUrl("~/Scripts/Gentellela/js/daterangepicker.js")%>'></script>
    <link href="Content/daterangepicker.css" rel="stylesheet">

    <script>
        $(document).ready(function () {
            var cb = function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
                $('#reportrange_right span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            };

            var optionSet1 = {
                startDate: moment().subtract(29, 'days'),
                endDate: moment(),
                minDate: '01/01/2012',
                maxDate: '12/31/2020',
                dateLimit: {
                    days: 60
                },
                showDropdowns: true,
                showWeekNumbers: true,
                timePicker: false,
                timePickerIncrement: 1,
                timePicker12Hour: true,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                opens: 'right',
                buttonClasses: ['btn btn-default'],
                applyClass: 'btn-small btn-primary',
                cancelClass: 'btn-small',
                format: 'MM/DD/YYYY',
                separator: ' to ',
                locale: {
                    applyLabel: 'Submit',
                    cancelLabel: 'Clear',
                    fromLabel: 'From',
                    toLabel: 'To',
                    customRangeLabel: 'Custom',
                    daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                    monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                    firstDay: 1
                }
            };

            $('#reportrange_right span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));

            $('#reportrange_right').daterangepicker(optionSet1, cb);

            $('#reportrange_right').on('show.daterangepicker', function () {
                console.log("show event fired");
            });
            $('#reportrange_right').on('hide.daterangepicker', function () {
                console.log("hide event fired");
            });
            $('#reportrange_right').on('apply.daterangepicker', function (ev, picker) {
                console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
            });
            $('#reportrange_right').on('cancel.daterangepicker', function (ev, picker) {
                console.log("cancel event fired");
            });

            $('#options1').click(function () {
                $('#reportrange_right').data('daterangepicker').setOptions(optionSet1, cb);
            });

            $('#options2').click(function () {
                $('#reportrange_right').data('daterangepicker').setOptions(optionSet2, cb);
            });

            $('#destroy').click(function () {
                $('#reportrange_right').data('daterangepicker').remove();
            });

        });
    </script>

    <script>
        $(document).ready(function () {
            var cb = function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            };

            var optionSet1 = {
                startDate: moment().subtract(29, 'days'),
                endDate: moment(),
                minDate: '01/01/2012',
                maxDate: '12/31/2020',
                dateLimit: {
                    days: 60
                },
                showDropdowns: true,
                showWeekNumbers: true,
                timePicker: false,
                timePickerIncrement: 1,
                timePicker12Hour: true,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                opens: 'left',
                buttonClasses: ['btn btn-default'],
                applyClass: 'btn-small btn-primary',
                cancelClass: 'btn-small',
                format: 'MM/DD/YYYY',
                separator: ' to ',
                locale: {
                    applyLabel: 'Submit',
                    cancelLabel: 'Clear',
                    fromLabel: 'From',
                    toLabel: 'To',
                    customRangeLabel: 'Custom',
                    daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                    monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                    firstDay: 1
                }
            };
            $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
            $('#reportrange').daterangepicker(optionSet1, cb);
            $('#reportrange').on('show.daterangepicker', function () {
                console.log("show event fired");
            });
            $('#reportrange').on('hide.daterangepicker', function () {
                console.log("hide event fired");
            });
            $('#reportrange').on('apply.daterangepicker', function (ev, picker) {
                console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
            });
            $('#reportrange').on('cancel.daterangepicker', function (ev, picker) {
                console.log("cancel event fired");
            });
            $('#options1').click(function () {
                $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
            });
            $('#options2').click(function () {
                $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
            });
            $('#destroy').click(function () {
                $('#reportrange').data('daterangepicker').remove();
            });
        });
    </script>

    <script>
        $(document).ready(function () {
            $('#single_cal1').daterangepicker({
                singleDatePicker: true,
                singleClasses: "picker_1"
            }, function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });
            $('#single_cal2').daterangepicker({
                singleDatePicker: true,
                singleClasses: "picker_2"
            }, function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });
            $('#single_cal3').daterangepicker({
                singleDatePicker: true,
                singleClasses: "picker_3"
            }, function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });
            $('#single_cal4').daterangepicker({
                singleDatePicker: true,
                singleClasses: "picker_4"
            }, function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });
        });
    </script>

    
    <!-- Skycons -->
    <script>
        $(document).ready(function () {
            var icons = new Skycons({
                "color": "#73879C"
            }),
              list = [
                "clear-day", "clear-night", "partly-cloudy-day",
                "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
                "fog"
              ],
              i;

            for (i = list.length; i--;)
                icons.set(list[i], list[i]);

            icons.play();
        });
    </script>
    <!-- /Skycons -->

    <!-- Doughnut Chart -->
    <script>
        $(document).ready(function () {
            var options = {
                legend: false,
                responsive: false
            };

            new Chart(document.getElementById("canvas1"), {
                type: 'doughnut',
                tooltipFillColor: "rgba(51, 51, 51, 0.55)",
                data: {
                    labels: [
                      "Symbian",
                      "Blackberry",
                      "Other",
                      "Android",
                      "IOS"
                    ],
                    datasets: [{
                        data: datas,
                        backgroundColor: [
                          "#BDC3C7",
                          "#9B59B6",
                          "#E74C3C",
                          "#26B99A",
                          "#3498DB"
                        ],
                        hoverBackgroundColor: [
                          "#CFD4D8",
                          "#B370CF",
                          "#E95E4F",
                          "#36CAAB",
                          "#49A9EA"
                        ]
                    }]
                },
                options: options
            });
        });
    </script>
    <!-- /Doughnut Chart -->

    <!-- bootstrap-daterangepicker -->
    <script>
        $(document).ready(function () {

            var cb = function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            };

            var optionSet1 = {
                startDate: moment().subtract(29, 'days'),
                endDate: moment(),
                minDate: '01/01/2012',
                maxDate: '12/31/2015',
                dateLimit: {
                    days: 60
                },
                showDropdowns: true,
                showWeekNumbers: true,
                timePicker: false,
                timePickerIncrement: 1,
                timePicker12Hour: true,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                opens: 'left',
                buttonClasses: ['btn btn-default'],
                applyClass: 'btn-small btn-primary',
                cancelClass: 'btn-small',
                format: 'MM/DD/YYYY',
                separator: ' to ',
                locale: {
                    applyLabel: 'Submit',
                    cancelLabel: 'Clear',
                    fromLabel: 'From',
                    toLabel: 'To',
                    customRangeLabel: 'Custom',
                    daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                    monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                    firstDay: 1
                }
            };
            $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
            $('#reportrange').daterangepicker(optionSet1, cb);
            $('#reportrange').on('show.daterangepicker', function () {
                console.log("show event fired");
            });
            $('#reportrange').on('hide.daterangepicker', function () {
                console.log("hide event fired");
            });
            $('#reportrange').on('apply.daterangepicker', function (ev, picker) {
                console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
            });
            $('#reportrange').on('cancel.daterangepicker', function (ev, picker) {
                console.log("cancel event fired");
            });
            $('#options1').click(function () {
                $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
            });
            $('#options2').click(function () {
                $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
            });
            $('#destroy').click(function () {
                $('#reportrange').data('daterangepicker').remove();
            });
        });
    </script>
    <!-- /bootstrap-daterangepicker -->

    <!-- gauge.js -->
    <script>
        var opts = {
            lines: 12,
            angle: 0,
            lineWidth: 0.4,
            pointer: {
                length: 0.75,
                strokeWidth: 0.042,
                color: '#1D212A'
            },
            limitMax: 'false',
            colorStart: '#1ABC9C',
            colorStop: '#1ABC9C',
            strokeColor: '#F0F3F3',
            generateGradient: true
        };
        var target = document.getElementById('foo'),
            gauge = new Gauge(target).setOptions(opts);

        gauge.maxValue = 6000;
        gauge.animationSpeed = 32;
        gauge.set(3200);
        gauge.setTextField(document.getElementById("gauge-text"));
    </script>
    <!-- /gauge.js -->

</asp:Content>
