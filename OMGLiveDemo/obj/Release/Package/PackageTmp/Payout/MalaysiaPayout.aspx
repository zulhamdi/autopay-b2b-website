﻿<%@ Page Title="" Async="true" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MalaysiaPayout.aspx.cs" Inherits="OMGLiveDemo.Payout.MalaysiaPayout" MaintainScrollPositionOnPostback="true" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" />
    <div class="row">
        <a class="hiddenanchor" id="process"></a>
        <div class="row x_title">
            <div class="col-md-12">
                <h3>Transfer to Malaysia</h3>
            </div>
        </div>

        <div class="col-md-3 col-sm-12 col-xs-12"></div>
        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="col-md-12 col-sm-12 col-xs-12">

                <div class="alert alert-danger alert-dismissible fade in" runat="server" id="diverror" visible="false">
                    <button type="button" runat="server" id="btnError" onserverclick="btnError_ServerClick" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <div class="centered"><asp:Label runat="server" ID="lblError" Font-Size="14pt" Text="test error" /></div>
                </div>

                <div runat="server" id="divstep1">
                    <div class="dashboard_graph">
                        <div style="width: 100%;">
                            <div id="canvas_dahsa" class="demo-placeholder" style="width: 95%; height: 100%; margin-left: 2.5%; margin-right: 2.5%">
                                <div class="form-group" style="height: 30px"></div>

                                <div class="form-group">
                                    <asp:Label runat="server" ID="Label4" CssClass="control-label" Text="Choose Sender *" />
                                    <asp:DropDownList runat="server" ID="ddlChooseSender" CssClass="form-control" OnSelectedIndexChanged="ddlChooseSender_SelectedIndexChanged" AutoPostBack="true">
                                        <asp:ListItem Enabled="true" Text="Select Sender" Value="-1"></asp:ListItem>
                                        <asp:ListItem Text="Use Saved Sender" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Input New Sender" Value="2"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="form-group" style="height: 5px"></div>

                                <div runat="server" id="divsender" visible="false">
                                    <div class="form-group" runat="server" id="divchoosesender" visible="false">
                                        <asp:Label runat="server" ID="Label5" CssClass="control-label" Text="Sender :" />
                                        <asp:DropDownList runat="server" ID="ddlSender" CssClass="form-control" OnSelectedIndexChanged="ddlSender_SelectedIndexChanged" AutoPostBack="true" />
                                    </div>
                                    <div class="form-group" style="height: 5px"></div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label3" CssClass="control-label" Text="Sender Full Name :" />
                                        <asp:TextBox runat="server" ID="txtSenderName" CssClass="form-control" />
                                    </div>
                                    <div class="form-group" style="height: 5px"></div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label21" CssClass="control-label" Text="City :" />
                                        <asp:TextBox runat="server" ID="txtCity" CssClass="form-control" Enabled="false" />
                                        <div style="margin-top: 5px;">
                                            <asp:Button runat="server" ID="btnCheckCity" Text="Find" CssClass="btn btn-success btn-sm" OnClick="btnCheckCity_Click" />
                                        </div>
                                    </div>
                                    <%--<div class="form-group" runat="server" id="divvalidate">
                                        <asp:Button runat="server" ID="btnCheckCity" Text="Validate" CssClass="btn btn-warning" OnClick="btnCheckCity_Click" />
                                    </div>--%>
                                    <div class="form-group" style="height: 5px"></div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label10" CssClass="control-label" Text="Sender Address :" />
                                        <asp:TextBox runat="server" ID="txtSenderAddress" CssClass="form-control" TextMode="MultiLine" Rows="3" />
                                    </div>
                                    <div class="form-group" style="height: 5px"></div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label6" CssClass="control-label" Text="Sender Phone :" />
                                        <asp:TextBox runat="server" ID="txtSenderPhone" CssClass="form-control" />
                                    </div>
                                    <div class="form-group" style="height: 5px"></div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label7" CssClass="control-label" Text="Sender Email :" />
                                        <asp:TextBox runat="server" ID="txtSenderEmail" CssClass="form-control" />
                                    </div>
                                    <div class="form-group" style="height: 5px"></div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="lblSenderType" CssClass="control-label" Text="Sender Type :" />
                                        <asp:DropDownList runat="server" ID="ddlSenderType" CssClass="form-control" OnSelectedIndexChanged="ddlSenderType_SelectedIndexChanged" AutoPostBack="true" />
                                    </div>
                                    <div class="form-group" style="height: 5px"></div>

                                    <div id="divpersonal" runat="server" class="form-group">
                                        <div class="form-group">
                                            <asp:Label runat="server" ID="lblNationalID" CssClass="control-label" Text="Sender ID No :" />
                                            <asp:TextBox runat="server" ID="txtNationalID" CssClass="form-control" />
                                            <asp:FileUpload ID="fuNationalID" runat="server" CssClass="form-control" />
                                            <u><asp:LinkButton ID="lkbNationalID" runat="server" Text="View Sender ID Image" Visible="false" ForeColor="Green" OnClick="lkbViewSenderIDImage_Click" /></u>
                                            <br />
                                            <u><asp:LinkButton ID="lkbSenderWithID" runat="server" Text="View Sender With ID Picture" Visible="false" ForeColor="Green" OnClick="lkbSenderWithID_Click" /></u>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label runat="server" ID="lblNationalIDType" CssClass="control-label" Text="Sender ID Type :" />
                                            <asp:TextBox runat="server" ID="txtNationalIDType" CssClass="form-control" />
                                        </div>
                                        <div class="form-group">
                                            <asp:Label runat="server" ID="lblNationalIDValidity" CssClass="control-label" Text="Sender ID Validity :" />
                                            <asp:TextBox runat="server" ID="txtNationalIDValidity" CssClass="form-control" />
                                        </div>

                                        <div class="form-group" style="height: 5px"></div>
                                    </div>

                                    <div id="divcompany" runat="server" class="form-group" visible="false">
                                        <div class="form-group">
                                            <asp:Label runat="server" ID="lblLegalitasType" CssClass="control-label" Text="Jenis Legalitas :" />
                                            <asp:TextBox runat="server" ID="txtLegalitasType" CssClass="form-control" />
                                        </div>

                                        <div class="form-group">
                                            <asp:Label runat="server" ID="lblLegalitasNo" CssClass="control-label" Text="Legalitas No. :" />
                                            <asp:TextBox runat="server" ID="txtLegalitas" CssClass="form-control" />
                                            <asp:FileUpload ID="fuLegalitasFile" runat="server" CssClass="form-control" />
                                            <u>
                                                <asp:LinkButton ID="lkbLegalitasFile" runat="server" Text="View Company Legalitas Document Image" Visible="false" ForeColor="Green" OnClick="lkbViewImageLegalitas_Click" /></u>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label runat="server" ID="Label17" CssClass="control-label" Text="NPWP No. :" />
                                            <asp:TextBox runat="server" ID="txtCompanyNPWP" CssClass="form-control" />
                                            <asp:FileUpload ID="fuCompanyNPWPFile" runat="server" CssClass="form-control" />
                                            <u><asp:LinkButton ID="lkbCompanyNPWP" runat="server" Text="View Company NPWP Document Image" Visible="false" ForeColor="Green" OnClick="lkbViewImageCompanyNPWP_Click" /></u>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label runat="server" ID="lblCompanyBBH" CssClass="control-label" Text="Bentuk Badan Hukum :" />
                                            <asp:TextBox runat="server" ID="txtCompanyBBH" CssClass="form-control" />
                                        </div>

                                        <div class="form-group">
                                            <asp:Label runat="server" ID="lblCompanyBusinessField" CssClass="control-label" Text="Business Type :" />
                                            <asp:TextBox runat="server" ID="txtCompanyBusinessType" CssClass="form-control" />
                                        </div>

                                        <div class="form-group">
                                            <asp:Label runat="server" ID="lblPICName" CssClass="control-label" Text="PIC Name :" />
                                            <asp:TextBox runat="server" ID="txtPICName" CssClass="form-control" />
                                        </div>

                                        <div class="form-group">
                                            <asp:Label runat="server" ID="lblPCIDNo" CssClass="control-label" Text="PIC ID No. :" />
                                            <asp:TextBox runat="server" ID="txtPICIDNo" CssClass="form-control" />
                                            <u><asp:LinkButton ID="lkbPICIDImage" runat="server" Text="View PIC ID Image" Visible="false" ForeColor="Green" OnClick="lkbPICIDImage_Click" /></u>
                                        </div>
                                    </div>

                                    <%--<div class="form-group">
                                    <asp:CheckBox runat="server" ID="cbSaveSender" CssClass="checkbox-inline" Text="  Save Sender" />
                                    <asp:CheckBox runat="server" ID="cbUpdateSender" CssClass="checkbox-inline" Text="  Update Sender" Visible="false" />
                                </div>
                                <div class="form-group" style="height: 5px"></div>--%>

                                    <div class="form-group">
                                        <right><asp:Button runat="server" ID="btnNext1" Text="Next" CssClass="btn btn-warning" OnClick="btnNext1_Click" /></right>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br />
                </div>


                <div runat="server" id="divstep2" visible="false">
                    <div class="dashboard_graph">
                        <div style="width: 100%;">
                            <div id="canvas_dahs" class="demo-placeholder" style="width: 95%; height: 100%; margin-left: 2.5%; margin-right: 2.5%">
                                <div class="form-group" style="height: 30px"></div>

                                <asp:HiddenField runat="server" ID="hfAdminFee" />
                                <asp:HiddenField runat="server" ID="hfRate" />
                                <asp:HiddenField runat="server" ID="hfRateOriginal" />
                                <asp:HiddenField runat="server" ID="hfSenderImageURL" />
                                <asp:HiddenField runat="server" ID="hfSenderWithIDImageURL" />
                                <asp:HiddenField runat="server" ID="hfMargin" />
                                <asp:HiddenField runat="server" ID="hfAmount" />
                                <asp:HiddenField runat="server" ID="hfTotalAmount" />
                                <asp:HiddenField runat="server" ID="hfTotalTransfer" />
                                <asp:HiddenField runat="server" ID="hfBank" />
                                <asp:HiddenField runat="server" ID="hfBalance" />

                                <asp:HiddenField runat="server" ID="hfLegalitasImageURL" />
                                <asp:HiddenField runat="server" ID="hfCompanyNPWPImageURL" />
                                <asp:HiddenField runat="server" ID="hfPICIDImageURL" />

                                <asp:HiddenField runat="server" ID="hfSenderID" />
                                <asp:HiddenField runat="server" ID="hfBeneficiaryID" />

                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblBeneficiaryType" CssClass="control-label" Text="Choose Beneficiary" />
                                    <asp:DropDownList runat="server" ID="ddlBeneficiaryType" CssClass="form-control" OnSelectedIndexChanged="ddlBeneficiaryType_SelectedIndexChanged" AutoPostBack="true">
                                        <asp:ListItem Enabled="true" Text="Select Beneficiary" Value="-1"></asp:ListItem>
                                        <asp:ListItem Text="Use Saved Beneficiary" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Input New Beneficiary" Value="2"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="form-group" style="height: 5px"></div>

                                <div runat="server" id="divbenf" visible="false">
                                    <div class="form-group" runat="server" id="divbeneficiary" visible="false">
                                        <asp:Label runat="server" ID="lblBeneficiary" CssClass="control-label" Text="Beneficiary *" />
                                        <asp:DropDownList runat="server" ID="ddlBeneficiary" CssClass="form-control" OnSelectedIndexChanged="ddlBeneficiary_SelectedIndexChanged" AutoPostBack="true" />
                                    </div>
                                    <div class="form-group" style="height: 5px"></div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="lblBeneficiaryName" CssClass="control-label" Text="Beneficiary Name *" />
                                        <asp:TextBox runat="server" ID="txtBeneficiaryName" CssClass="form-control" Enabled="true" />
                                    </div>
                                    <div class="form-group" style="height: 5px"></div>

                                    <%--<div class="form-group">
                                        <asp:Label runat="server" ID="Label15" CssClass="control-label" Text="Realtime Transfer?" />
                                        <asp:DropDownList runat="server" ID="ddlRealtime" CssClass="form-control" OnSelectedIndexChanged="ddlRealtime_SelectedIndexChanged" AutoPostBack="true">
                                            <asp:ListItem Enabled="true" Text="Yes" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="form-group" style="height: 5px"></div>--%>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="lblBeneficiaryBank" CssClass="control-label" Text="Beneficiary Bank *" />
                                        <asp:DropDownList runat="server" ID="ddlBank" CssClass="form-control" OnSelectedIndexChanged="ddlBank_SelectedIndexChanged" AutoPostBack="true" />
                                    </div>
                                    <div class="form-group" style="height: 5px"></div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="lblBeneficiaryAccount" CssClass="control-label" Text="Beneficiary Account *" />
                                        <asp:TextBox runat="server" ID="txtBeneficiaryAccount" CssClass="form-control" />
                                    </div>
                                    <%--<div class="form-group" runat="server" id="divvalidate">
                                        <asp:Button runat="server" ID="btnValidate" Text="Validate" CssClass="btn btn-warning" OnClick="btnValidate_Click" />
                                    </div>--%>
                                    <div class="form-group" style="height: 5px"></div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="lblBeneficiaryAlias" CssClass="control-label" Text="Beneficiary Alias " />
                                        <asp:TextBox runat="server" ID="txtBeneficiaryAlias" CssClass="form-control" />
                                    </div>
                                    <div class="form-group" style="height: 5px"></div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="lblBeneficiaryAddress" CssClass="control-label" Text="Beneficiary Address *" />
                                        <asp:TextBox runat="server" ID="txtBeneficiaryAddress" CssClass="form-control" TextMode="MultiLine" Rows="3" />
                                    </div>
                                    <div class="form-group" style="height: 5px"></div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="lblBeneficiaryPhone" CssClass="control-label" Text="Beneficiary Phone *" />
                                        <asp:TextBox runat="server" ID="txtBeneficiaryPhone" CssClass="form-control" />
                                    </div>
                                    <div class="form-group" style="height: 5px"></div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="lblBeneficiaryEmail" CssClass="control-label" Text="Beneficiary Email *" />
                                        <asp:TextBox runat="server" ID="txtBeneficiaryEmail" CssClass="form-control" />
                                    </div>
                                    <%--<div class="form-group" style="height: 5px"></div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label12" CssClass="control-label" Text="Beneficiary National ID / Passport No. *" />
                                        <asp:TextBox runat="server" ID="txtBeneficiaryNationalID" CssClass="form-control" />
                                    </div>--%>
                                    <div class="form-group">
                                        <asp:CheckBox runat="server" ID="cbSaveBenfeficiary" CssClass="checkbox-inline" Text="  Save Beneficiary" Visible="false" />
                                        <asp:CheckBox runat="server" ID="cbUpdateBeneficiary" CssClass="checkbox-inline" Text="  Update Beneficiary" Visible="false" />
                                    </div>
                                    <div class="form-group" style="height: 5px"></div>

                                    <div class="form-group">
                                        <right><asp:Button runat="server" ID="btnBack1" Text="Back" CssClass="btn btn-danger" OnClick="btnBack1_Click" /></right>
                                        <right><asp:Button runat="server" ID="btnNext2" Text="Next" CssClass="btn btn-warning" OnClick="btnNext2_Click" /></right>
                                    </div>
                                    <div class="form-group" style="height: 5px"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br />
                </div>

                <div runat="server" id="divstep3" visible="false">
                    <div class="dashboard_graph">
                        <div style="width: 100%;">
                            <div id="canvas_dahs1" class="demo-placeholder" style="width: 95%; height: 100%; margin-left: 2.5%; margin-right: 2.5%">

                                <div class="form-group" style="height: 30px"></div>

                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblAmountType" CssClass="control-label" Text="Amount Type" />
                                    <asp:DropDownList runat="server" ID="ddlAmountType" CssClass="form-control" OnSelectedIndexChanged="ddlAmountType_SelectedIndexChanged" AutoPostBack="true">
                                        <asp:ListItem Enabled="true" Text="Select Amount Type" Value="-1"></asp:ListItem>
                                        <asp:ListItem Text="in MYR" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="in IDR" Value="2"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="form-group" style="height: 5px"></div>

                                <div class="form-group" runat="server" id="divrateall">
                                    <%--<asp:Label runat="server" ID="Label2" CssClass="control-label" Text="Choose Rate" />
                                    <asp:DropDownList runat="server" ID="ddlRate" CssClass="form-control" OnSelectedIndexChanged="ddlRate_SelectedIndexChanged" AutoPostBack="true">
                                    </asp:DropDownList>--%>
                                    <div class="alert alert-danger fade in">
                                            <div class="centered"> <asp:Label runat="server" ID="lblRatesMitra" CssClass="control-label" Text="Rates" /> :</div>
                                            <div class="centered"><asp:Label runat="server" ID="lblRatesMitraValue"/></div>
                                        </div>
                                </div>
                                <div class="form-group" style="height: 5px"></div>

                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblAmount" CssClass="control-label" Text="Amount *" />
                                    <asp:TextBox runat="server" ID="txtAmount" CssClass="form-control" OnTextChanged="txtAmount_TextChanged" AutoPostBack="true" TextMode="MultiLine" Rows="1" Font-Size="20pt" />
                                </div>
                                <div class="form-group" runat="server" id="divrateagent">
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <div class="alert alert-warning fade in">
                                            <div class="centered"><asp:Label runat="server" ID="lblCurrency" Font-Bold="true" Font-Size="Medium" /></div>
                                            <div class="centered"><asp:Label runat="server" ID="lblCurrencyUpdate"/></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <div class="alert alert-warning fade in">
                                            <div class="centered"><asp:Label runat="server" ID="lbl1" Text="Administrasion Fee"/></div>
                                            <div class="centered"><asp:Label runat="server" ID="lblAdminFee" Font-Bold="true" Font-Size="Medium" /></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" runat="server" id="divbalance" visible="false">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="alert alert-warning fade in">
                                            <div class="centered"><asp:Label runat="server" ID="Label12" CssClass="control-label" Font-Size="Large" Text="Balance (in MYR) : " />
                                            <br />
                                            <asp:Label runat="server" ID="lblBalance" Font-Bold="true" Font-Size="Large" ForeColor="White" /></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" runat="server" id="divadminfee" visible="false">
                                    <asp:Label runat="server" ID="Label9" CssClass="control-label" Text="Administration Fee (in IDR) : " />
                                    <br />
                                    <asp:Label runat="server" ID="lblAdminFees" Font-Bold="true" Font-Size="Large" ForeColor="Red" />
                                    <div class="form-group" style="height: 5px"></div>
                                </div>

                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblTotal" CssClass="control-label" Text="Total Amount : " />
                                    <br />
                                    <asp:Label runat="server" ID="lblTotalAmount" Font-Bold="true" Font-Size="XX-Large" ForeColor="Red" />
                                </div>
                                <div class="form-group" style="height: 5px"></div>

                                <div class="form-group">
                                    <asp:Label runat="server" ID="Label8" CssClass="control-label" Text="Total Transfer : " />
                                    <br />
                                    <asp:Label runat="server" ID="lblTotalTransfer" Font-Bold="true" Font-Size="XX-Large" ForeColor="Red" />
                                </div>
                                <div class="form-group" style="height: 5px"></div>
                                <div class="form-group" style="height: 5px"></div>

                                <div class="form-group" runat="server" id="divtransferto">
                                    <asp:Label runat="server" ID="Label11" CssClass="control-label" Text="Transfer Ke : *" />
                                    <asp:DropDownList runat="server" ID="ddlBankAccount" CssClass="form-control" OnSelectedIndexChanged="ddlBankAccount_SelectedIndexChanged" AutoPostBack="true" />
                                    <div class="form-group" style="height: 5px"></div>
                                </div>

                                <div class="form-group" runat="server">
                                    <asp:Label runat="server" ID="Label15" CssClass="control-label" Text="Transfer Purpose *" />
                                    <asp:DropDownList runat="server" ID="ddlPurpose" CssClass="form-control" OnSelectedIndexChanged="ddlPurpose_SelectedIndexChanged" AutoPostBack="true" />
                                </div>
                                <div class="form-group" style="height: 5px"></div>

                                <div class="form-group" runat="server" id="divSupportingDocument">
                                    <asp:Label runat="server" ID="Label19" CssClass="control-label" Text="Supporting Document :" />
                                    <asp:FileUpload ID="fuSupportingDocument" runat="server" CssClass="form-control" />
                                    <asp:Label runat="server" ID="lblSuportingDocument" Text="Uploaded File = NONE"/>
                                </div>
                                <div class="form-group" style="height: 5px"></div>

                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblNotes" CssClass="control-label" Text="Notes *" />
                                    <asp:TextBox runat="server" ID="txtNotes" CssClass="form-control" />
                                    <%--TextMode="MultiLine" Rows="3"--%>
                                </div>
                                <div class="form-group" style="height: 5px"></div>


                                <right><asp:Button runat="server" ID="btnBack2" Text="Back" CssClass="btn btn-danger" OnClick="btnBack2_Click" /></right>
                                <div class="form-group" id="dvBtnProcess">
                                    <center><asp:Button runat="server" ID="btnProcess" Text="Process" CssClass="btn btn-success btn-lg" OnClick="btnConfirmTransafer_Click" Width="100%" /></center>
                                    <%--<center><asp:Button runat="server" ID="Button1" Text="Process" CssClass="btn btn-success btn-lg" OnClick="btnProcess_Click" OnClientClick="javascript:ShowProgressBar()" Width="100%" /></center>--%>
                                </div>

                                <div class="form-group" id="dvProcessing" runat="server" visible="false">
                                    <%--<div class="form-group" id="dvProcessing" style="visibility: hidden">--%>
                                    <center><asp:Label runat="server" ID="Label1" Text="Processing" Font-Size="X-Large" ForeColor="Green" /></center>
                                </div>

                                <div class="form-group" style="height: 30px"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title" id="myModalLabel">Process Transfer</h4>
                            </div>
                            <div class="modal-body">
                                <br />
                                <div class="form-group">
                                    <asp:Label runat="server" ID="Label13" CssClass="control-label" Text="Total Transfer : " />
                                    <asp:Label runat="server" ID="lblSummaryTotalTransfer" Font-Bold="true" Font-Size="small" ForeColor="Red" />
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="Label14" CssClass="control-label" Text="Total Amount : " />
                                    <asp:Label runat="server" ID="lblSummaryTotalAmount" Font-Bold="true" Font-Size="small" ForeColor="Red" />
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="Label16" CssClass="control-label" Text="Sender Name : " />
                                    <asp:Label runat="server" ID="lblSummarySenderName" Font-Bold="true" Font-Size="small" ForeColor="Blue" />
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="Label20" CssClass="control-label" Text="Beneficiary Name : " />
                                    <asp:Label runat="server" ID="lblSummaryBeneficiaryName" Font-Bold="true" Font-Size="small" ForeColor="Blue" />
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="Label22" CssClass="control-label" Text="Beneficiary Account : " />
                                    <asp:Label runat="server" ID="lblSummaryBeneficiaryAccount" Font-Bold="true" Font-Size="small" ForeColor="Blue" />
                                </div>
                                <br />
                                <h4>
                                    <asp:Label runat="server" ID="lblCancelPayoutID" /></h4>
                                <p>Are you sure want to process this transfer?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal" runat="server" id="btnCancelTranser" onserverclick="btnCancelTransafer_Click">Go back</button>
                                <button type="button" class="btn btn-success" data-dismiss="modal" runat="server" id="btnProcessTransfer" onserverclick="btnProcess_Click">Yes, Process Transfer</button>
                                <%--<asp:Button runat="server" ID="btnProcessPayout" CssClass="btn btn-success" Text="Yes, Process Transfer" OnClick="btnProcess_Click" />--%>
                            </div>
                        </div>
                    </div>
                </div>

                
                <%--================================ M O D A L   C I T Y =======================================--%>
                <%--================================ M O D A L   C I T Y =======================================--%>
                <%--================================ M O D A L   C I T Y =======================================--%>
                <%--================================ M O D A L   C I T Y =======================================--%>

                <div class="modal fade bs-example-modal-lg" id="myModalCity" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title" id="myModalLabel1">Choose City</h4>
                            </div>
                            <div class="modal-body">
                                 <asp:HiddenField runat="server" ID="hfCityID" />
                                 <asp:HiddenField runat="server" ID="hfCityName" />
                                 <asp:HiddenField runat="server" ID="hfModalCityOpen" />
                                <div class="form-group">
                                    <asp:Label runat="server" ID="Label18" CssClass="control-label" Text="City *" />
                                    <asp:TextBox runat="server" ID="txtFindCity" CssClass="form-control" />
                                    <div style="margin-top: 5px">
                                        <asp:Button runat="server" ID="btnFindCity" Text="Find" CssClass="btn btn-success btn-sm" OnClick="btnFindCity_Click" />
                                    </div>
                                </div>
                                <div class="form-group" style="height: 5px"></div>

                                <div id="canvas_dahs11" class="demo-placeholder" style="width: 100%; height: 100%;">
                                    <asp:GridView CssClass="table table-striped jambo_table bulk_action" ID="gvListCity" Visible="false" runat="server" AutoGenerateColumns="false" DataKeyNames="ID">
                                        <Columns>
                                            <asp:TemplateField HeaderText="No.">
                                                <ItemTemplate>
                                                    <asp:HiddenField runat="server" ID="hfCityIDModal" Value='<%# Eval("ID") %>' />
                                                    <asp:HiddenField runat="server" ID="hfCityNameModal" Value='<%# Eval("City") %>' />
                                                    <%# Container.DataItemIndex + 1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="City" HeaderText="City" />
                                            <asp:TemplateField HeaderText="">
                                                <ItemTemplate>
                                                    <asp:Button ID="btnChooseCity" CssClass="form-control" runat="server" Text="Choose" OnClick="btnChooseCity_Click" UseSubmitBehavior="false" CausesValidation="false" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                            <%--<div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal" runat="server" id="Button1" onserverclick="btnCancelTransafer_Click">Go back</button>
                                <button type="button" class="btn btn-success" data-dismiss="modal" runat="server" id="Button2" onserverclick="btnProcess_Click">Yes, Process Transfer</button>
                            </div>--%>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12"></div>
    </div>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Scripts" runat="server">
    <script type="text/javascript">
        function ShowProgressBar() {
            document.getElementById('dvProcessing').style.visibility = 'visible';
            document.getElementById('dvBtnProcess').style.visibility = 'hidden';
        }

        function HideProgressBar() {
            document.getElementById('dvProgressBar').style.visibility = "hidden";
        }
    </script>

    <script type="text/javascript">
        $(window).load(function () {
            if (window.isColorbox) {
                $.colorbox({ href: "ViewImage.aspx", iframe: true, width: "80%", height: "80%" });
            }
        });
        function OpenCBox() {
            $.colorbox({ href: "ViewImage.aspx", iframe: true, width: "80%", height: "80%" });
        }

    </script>

    <script type="text/javascript">
        $(window).load(function () {
            if (window.isModal) {
                $('#myModal').modal('show');
            }
            else {
                $('#myModal').modal('hide');
            }
        });

        $(window).load(function () {
            if (window.isModalCity) {
                $('#myModalCity').modal('show');
            }
            else {
                $('#myModalCity').modal('hide');
            }
        });

        function openModalCancel() {
            document.getElementById('myModal').modal('show');
        }

    </script>

</asp:Content>
