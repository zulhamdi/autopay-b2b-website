﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewImage.aspx.cs" Inherits="OMGLiveDemo.Payout.ViewImage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <!-- Bootstrap -->
    <link href="../Content/bootstrap.min.css" rel="stylesheet" />
    <!-- Font Awesome -->
    <link href="../Content/font-awesome.min.css" rel="stylesheet" />
    <!-- NProgress -->
    <link href="../Content/nprogress.css" rel="stylesheet" />
    <!-- iCheck -->
    <link href="../Content/green.css" rel="stylesheet" />
    <!-- bootstrap-progressbar -->
    <link href="../Content/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" />
    <!-- JQVMap -->
    <link href="../Content/jqvmap.min.css" rel="stylesheet" />
    <!-- bootstrap-daterangepicker -->
    <link href="../Content/daterangepicker.css" rel="stylesheet" />
    <!-- colorbox -->
    <link href="../Content/colorbox.css" rel="stylesheet" />

    <!-- Custom Theme Style -->
    <link href="../Content/custom.min.css" rel="stylesheet" />
    <style type="text/css">
        .auto-style3 {
            width: 1px;
        }

        .auto-style4 {
            width: 15px;
        }

        .auto-style5 {
            width: 169px;
        }

        .auto-style6 {
            width: 18px;
        }

        .auto-style7 {
            width: 101px;
        }

        .auto-style8 {
            width: 41px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container body">
            <div class="main_container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div runat="server" id="image" class="dashboard_graph" style="display: normal">
                            <div class="row x_title">
                                <div class="col-md-12">
                                    <h3>File
                                        <asp:Label runat="server" ID="lblTraID" Text="asfsdgs" /></h3>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div style="width: 100%;">
                                    <div id="canvas_dahs1" class="demo-placeholder" style="width: 100%; height: 100%;">
                                        <center>
                                            <asp:Image ID="imgURL" runat="server" ImageAlign="Middle" />
                                        </center>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>

                </div>
                <br />
            </div>
        </div>
    </form>
    <script src='<%= ResolveUrl("~/Scripts/Gentellela/js/jquery.min.js")%>'></script>
    <script src='<%= ResolveUrl("~/Scripts/Gentellela/js/bootstrap.min.js")%>'></script>
    <script src='<%= ResolveUrl("~/Scripts/Gentellela/js/fastclick.js")%>'></script>
    <script src='<%= ResolveUrl("~/Scripts/Gentellela/js/nprogress.js")%>'></script>
    <script src='<%= ResolveUrl("~/Scripts/Gentellela/js/Chart.min.js")%>'></script>
    <script src='<%= ResolveUrl("~/Scripts/Gentellela/js/gauge.min.js")%>'></script>
    <script src='<%= ResolveUrl("~/Scripts/Gentellela/js/bootstrap-progressbar.min.js")%>'></script>
    <script src='<%= ResolveUrl("~/Scripts/Gentellela/js/icheck.min.js")%>'></script>
    <script src='<%= ResolveUrl("~/Scripts/Gentellela/js/skycons.js")%>'></script>
    <script src='<%= ResolveUrl("~/Scripts/Gentellela/js/jquery.flot.js")%>'></script>
    <script src='<%= ResolveUrl("~/Scripts/Gentellela/js/jquery.flot.pie.js")%>'></script>
    <script src='<%= ResolveUrl("~/Scripts/Gentellela/js/jquery.flot.time.js")%>'></script>
    <script src='<%= ResolveUrl("~/Scripts/Gentellela/js/jquery.flot.stack.js")%>'></script>
    <script src='<%= ResolveUrl("~/Scripts/Gentellela/js/jquery.flot.resize.js")%>'></script>
    <script src='<%= ResolveUrl("~/Scripts/Gentellela/js/jquery.flot.orderBars.js")%>'></script>
    <script src='<%= ResolveUrl("~/Scripts/Gentellela/js/jquery.flot.spline.min.js")%>'></script>
    <script src='<%= ResolveUrl("~/Scripts/Gentellela/js/curvedLines.js")%>'></script>
    <script src='<%= ResolveUrl("~/Scripts/Gentellela/js/date.js")%>'></script>
    <script src='<%= ResolveUrl("~/Scripts/Gentellela/js/jquery.vmap.js")%>'></script>
    <script src='<%= ResolveUrl("~/Scripts/Gentellela/js/jquery.vmap.world.js")%>'></script>
    <script src='<%= ResolveUrl("~/Scripts/Gentellela/js/jquery.vmap.sampledata.js")%>'></script>
    <script src='<%= ResolveUrl("~/Scripts/Gentellela/js/moment.min.js")%>'></script>
    <script src='<%= ResolveUrl("~/Scripts/Gentellela/js/daterangepicker.js")%>'></script>
    <script src='<%= ResolveUrl("~/Scripts/colorbox/jquery.colorbox-min.js")%>'></script>

    <script src='<%= ResolveUrl("~/Scripts/Gentellela/js/custom.min.js")%>'></script>
</body>
</html>
