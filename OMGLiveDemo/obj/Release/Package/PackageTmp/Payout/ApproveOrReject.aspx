﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ApproveOrReject.aspx.cs" Inherits="OMGLiveDemo.Payout.ApproveOrReject" MaintainScrollPositionOnPostback="true" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" />
    <div class="row">
        <a class="hiddenanchor" id="process"></a>
        <div class="row x_title">
            <div class="col-md-12">
                <h3>Pending Task</h3>
            </div>
        </div>

        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="alert alert-danger alert-dismissible fade in" runat="server" id="diverror" visible="false">
                    <button type="button" runat="server" id="btnError" onserverclick="btnError_ServerClick" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <center><asp:Label runat="server" ID="lblError" Font-Size="14pt" Text="test error" /></center>
                </div>

                <div class="dashboard_graph">
                    <div style="width: 100%;">
                        <div id="canvas_dahs12" class="demo-placeholder" style="width: 95%; height: 100%; margin-left: 2.5%; margin-right: 2.5%">

                            <div class="form-group" style="height: 25px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="Label2" CssClass="control-label" Text="Receipt : *" />
                                <br />
                                <center>
                                            <asp:Image ID="imgURL" runat="server" ImageAlign="Middle" />
                                        </center>
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="Label4" CssClass="control-label" Text="Reject reason* : " />
                                <asp:TextBox runat="server" ID="txtRejectReason" CssClass="form-control" Enabled="true" />
                            </div>

                            <div class="form-group" id="dvBtnProcess">
                                <asp:Button runat="server" ID="btnApprove" Text="Approve" CssClass="btn btn-success btn-lg" OnClick="btnApprove_Click" OnClientClick="javascript:ShowProgressBar()" />
                                <asp:Button runat="server" ID="btnReject" Text="Reject" CssClass="btn btn-danger btn-lg" OnClick="btnReject_Click" OnClientClick="javascript:ShowProgressBar()" />
                            </div>

                            <div class="form-group" id="dvProcessing" style="display: none;">
                                <center><asp:Label runat="server" ID="Label1" Text="Processing" Font-Size="X-Large" ForeColor="Green" /></center>
                            </div>
                            <div class="form-group" style="height: 10px"></div>

                        </div>
                    </div>
                </div>

                <br />
            </div>
        </div>
        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="col-md-12 col-sm-12 col-xs-12">

                <div class="dashboard_graph">
                    <div style="width: 100%;">
                        <div id="canvas_dahs1" class="demo-placeholder" style="width: 95%; height: 100%; margin-left: 2.5%; margin-right: 2.5%">

                            <div class="form-group" style="height: 30px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblAgent" CssClass="control-label" Text="Agent Name" />
                                <asp:TextBox runat="server" ID="txtAgent" CssClass="form-control" Enabled="false" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>
                            
                            <div class="form-group">
                                <asp:Label runat="server" ID="lblTotal" CssClass="control-label" Text="Total Amount : " />
                                <br />
                                <asp:Label runat="server" ID="lblTotalAmount" Font-Bold="true" Font-Size="XX-Large" ForeColor="Red" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>
                            <div class="form-group">
                                <asp:Label runat="server" ID="Label5" CssClass="control-label" Text="Total Transfer : " />
                                <br />
                                <asp:Label runat="server" ID="lblTotalTransfer" Font-Bold="true" Font-Size="XX-Large" ForeColor="Red" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblNotes" CssClass="control-label" Text="Notes *" />
                                <asp:TextBox runat="server" ID="txtNotes" CssClass="form-control" Enabled="false" />
                                <%--TextMode="MultiLine" Rows="3"--%>
                            </div>

                            <div class="form-group" runat="server" id="divSupportingDocument" visible="false">
                                    <asp:Label runat="server" ID="lblSupportingDocument" CssClass="control-label" Text="Supporting Document :" />
                                    <u>
                                        <asp:LinkButton ID="lkbSupportingDocument" runat="server" Text="View Company Legalitas Document" Visible="true" ForeColor="Green" OnClick="lkbSupportingDocument_Click" /></u>
                                </div>
                            <div class="form-group" style="height: 5px"></div>

                        </div>
                    </div>
                </div>

                <br />

                <div class="dashboard_graph">
                    <div style="width: 100%;">
                        <div id="canvas_dahsa" class="demo-placeholder" style="width: 95%; height: 100%; margin-left: 2.5%; margin-right: 2.5%">
                            <div class="form-group" style="height: 30px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="Label3" CssClass="control-label" Text="Sender Full Name *" />
                                <asp:TextBox runat="server" ID="txtSenderName" CssClass="form-control" Enabled="false" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="Label6" CssClass="control-label" Text="Sender Phone *" />
                                <asp:TextBox runat="server" ID="txtSenderPhone" CssClass="form-control" Enabled="false" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="Label7" CssClass="control-label" Text="Email " />
                                <asp:TextBox runat="server" ID="txtSenderEmail" CssClass="form-control" Enabled="false" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblSenderType" CssClass="control-label" Text="Sender Type *" />
                                <asp:TextBox runat="server" ID="txtSenderType" CssClass="form-control" Enabled="false" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div id="divpersonal" runat="server" class="form-group">
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblNationalID" CssClass="control-label" Text="National ID / Passport *" />
                                    <asp:TextBox runat="server" ID="txtNationalID" CssClass="form-control" Enabled="false" />
                                    <u>
                                        <asp:LinkButton ID="lkbNationalID" runat="server" Text="View Sender ID Image" Visible="true" ForeColor="Green" OnClick="lkbSenderIDFile_Click" /></u>
                                </div>
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div id="divcompany" runat="server" class="form-group" visible="false">
                                <div class="form-group" runat="server" id="divCompanySSM">
                                    <asp:Label runat="server" ID="lblSSM" CssClass="control-label" Text="SSM No. *" />
                                    <asp:TextBox runat="server" ID="txtSSM" CssClass="form-control" Enabled="false" />
                                    <u>
                                        <asp:LinkButton ID="lkbSSM" runat="server" Text="View Company SSM Document" Visible="true" ForeColor="Green" OnClick="lkbSSM_Click" /></u>
                                </div>
                                <div class="form-group" runat="server" id="divCompanyLegalitas">
                                    <asp:Label runat="server" ID="Label8" CssClass="control-label" Text="Legalitas No. *" />
                                    <asp:TextBox runat="server" ID="txtCompanyLegalitasNo" CssClass="form-control" Enabled="false" />
                                    <u>
                                        <asp:LinkButton ID="lkbCompanyLegalitasFile" runat="server" Text="View Company Legalitas Document" Visible="true" ForeColor="Green" OnClick="lkbCompanyLegalitasFile_Click" /></u>
                                </div>
                            </div>
                            <div class="form-group" style="height: 5px"></div>
                        </div>
                    </div>
                </div>

                <br />

                <div class="dashboard_graph">
                    <div style="width: 100%;">
                        <div id="canvas_dahs" class="demo-placeholder" style="width: 95%; height: 100%; margin-left: 2.5%; margin-right: 2.5%">
                            <div class="form-group" style="height: 30px"></div>

                            <asp:HiddenField runat="server" ID="hfRate" />
                            <asp:HiddenField runat="server" ID="hfSenderIDImageURL" />
                            <asp:HiddenField runat="server" ID="hfSenderWithIDImageURL" />
                            <asp:HiddenField runat="server" ID="hfCompanySSMFileURL" />
                            <asp:HiddenField runat="server" ID="hfCompanyLegalitasFileURL" />
                            <asp:HiddenField runat="server" ID="hfSupportingDocumentFileURL" />
                            <asp:HiddenField runat="server" ID="hfMargin" />
                            <asp:HiddenField runat="server" ID="hfTotalAmount" />
                            <asp:HiddenField runat="server" ID="hfBank" />
                            <asp:HiddenField runat="server" ID="hfPayoutID" />

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblName" CssClass="control-label" Text="Beneficiary Name *" />
                                <asp:TextBox runat="server" ID="txtBenfName" CssClass="form-control" Enabled="false" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblBank" CssClass="control-label" Text="Beneficiary Bank *" />
                                <asp:TextBox runat="server" ID="txtBenfBank" CssClass="form-control" Enabled="false" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblAccount" CssClass="control-label" Text="Beneficiary Account *" />
                                <asp:TextBox runat="server" ID="txtBenfAccount" CssClass="form-control" Enabled="false" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblBenfPhone" CssClass="control-label" Text="Beneficiary Phone " />
                                <asp:TextBox runat="server" ID="txtBenfPhone" CssClass="form-control" Enabled="false" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblEmail" CssClass="control-label" Text="Beneficiary Email " />
                                <asp:TextBox runat="server" ID="txtBenfEmail" CssClass="form-control" Enabled="false" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Scripts" runat="server">
    <script type="text/javascript">
        function ShowProgressBar() {
            document.getElementById('dvProcessing').style.display = 'normal';
            document.getElementById('dvBtnProcess').style.display = 'none';
        }

        function HideProgressBar() {
            document.getElementById('dvProgressBar').style.display = "none";
        }
    </script>

    <script type="text/javascript">
        $(window).load(function () {
            if (window.isColorbox) {
                $.colorbox({ href: "ViewImage.aspx", iframe: true, width: "80%", height: "80%" });
            }
        });
        function OpenCBox() {
            $.colorbox({ href: "ViewImage.aspx", iframe: true, width: "80%", height: "80%" });
        }

    </script>


</asp:Content>
