﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CreatePayout.aspx.cs" Inherits="OMGLiveDemo.Payout.CreatePayout" MaintainScrollPositionOnPostback="true" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" />
    <div class="row">
        <a class="hiddenanchor" id="process"></a>
        <div class="row x_title">
            <div class="col-md-12">
                <h3>Create payout</h3>
            </div>
        </div>

        <div class="col-md-3 col-sm-12 col-xs-12"></div>
        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="dashboard_graph">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div style="width: 100%;">
                        <div id="canvas_dahs" class="demo-placeholder" style="width: 100%; height: 100%;">
                            <div class="form-group" style="height: 30px"></div>

                            <asp:HiddenField runat="server" ID="hfRate" />
                            <asp:HiddenField runat="server" ID="hfMargin" />
                            <asp:HiddenField runat="server" ID="hfTotalAmount" />
                            <asp:HiddenField runat="server" ID="hfBank" />

                            <div class="alert alert-danger alert-dismissible fade in" runat="server" id="diverror" visible="false">
                                <button type="button" runat="server" id="btnError" onserverclick="btnError_ServerClick" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <center><asp:Label runat="server" ID="lblError" /></center>
                            </div>
                            <div class="form-group">
                                <asp:Label runat="server" ID="lblBeneficiaryType" CssClass="control-label" Text="Beneficiary Type" />
                                <asp:DropDownList runat="server" ID="ddlBeneficiaryType" CssClass="form-control" OnSelectedIndexChanged="ddlBeneficiaryType_SelectedIndexChanged" AutoPostBack="true">
                                    <asp:ListItem Enabled="true" Text="Select Beneficiary Type" Value="-1"></asp:ListItem>
                                    <asp:ListItem Text="Use Saved Beneficiary" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Input New Beneficiary" Value="2"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div runat="server" id="divall" visible="false">
                                <div class="form-group" runat="server" id="divbeneficiary" visible="false">
                                    <asp:Label runat="server" ID="lblBeneficiary" CssClass="control-label" Text="Beneficiary *" />
                                    <asp:DropDownList runat="server" ID="ddlBeneficiary" CssClass="form-control" OnSelectedIndexChanged="ddlBeneficiary_SelectedIndexChanged" AutoPostBack="true" />
                                </div>
                                <div class="form-group" style="height: 5px"></div>

                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblName" CssClass="control-label" Text="Beneficiary Name *" />
                                    <asp:TextBox runat="server" ID="txtName" CssClass="form-control" Enabled="false" />
                                </div>
                                <div class="form-group" style="height: 5px"></div>

                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblBank" CssClass="control-label" Text="Beneficiary Bank *" />
                                    <asp:DropDownList runat="server" ID="ddlBank" CssClass="form-control" OnSelectedIndexChanged="ddlBank_SelectedIndexChanged" AutoPostBack="true" />
                                </div>
                                <div class="form-group" style="height: 5px"></div>

                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblAccount" CssClass="control-label" Text="Beneficiary Account *" />
                                    <asp:TextBox runat="server" ID="txtAccount" CssClass="form-control" />
                                </div>
                                <div class="form-group" runat="server" id="divvalidate">
                                    <asp:Button runat="server" ID="btnValidate" Text="Validate" CssClass="btn btn-warning" OnClick="btnValidate_Click" />
                                </div>
                                <div class="form-group" style="height: 5px"></div>

                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblAlias" CssClass="control-label" Text="Beneficiary Alias " />
                                    <asp:TextBox runat="server" ID="txtAlias" CssClass="form-control" />
                                </div>
                                <div class="form-group" style="height: 5px"></div>

                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblPhone" CssClass="control-label" Text="Beneficiary Phone " />
                                    <asp:TextBox runat="server" ID="txtPhone" CssClass="form-control" />
                                </div>
                                <div class="form-group" style="height: 5px"></div>

                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblEmail" CssClass="control-label" Text="Beneficiary Email " />
                                    <asp:TextBox runat="server" ID="txtEmail" CssClass="form-control" />
                                </div>
                                <div class="form-group">
                                    <asp:CheckBox runat="server" ID="cbSaveBenfeficiary" CssClass="checkbox-inline" Text="  Save Beneficiary" />
                                    <asp:CheckBox runat="server" ID="cbUpdateBeneficiary" CssClass="checkbox-inline" Text="  Update Beneficiary" Visible="false" />
                                </div>
                                <div class="form-group" style="height: 5px"></div>

                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblAmountType" CssClass="control-label" Text="Amount Type" />
                                    <asp:DropDownList runat="server" ID="ddlAmountType" CssClass="form-control" OnSelectedIndexChanged="ddlAmountType_SelectedIndexChanged" AutoPostBack="true">
                                        <asp:ListItem Enabled="true" Text="Select Amount Type" Value="-1"></asp:ListItem>
                                        <asp:ListItem Text="in MYR" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="in IDR" Value="2"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="form-group" style="height: 5px"></div>

                                <div class="form-group">
                                    <asp:Label runat="server" ID="Label2" CssClass="control-label" Text="Choose Rate" />
                                    <asp:DropDownList runat="server" ID="ddlRate" CssClass="form-control" OnSelectedIndexChanged="ddlRate_SelectedIndexChanged" AutoPostBack="true">
                                    </asp:DropDownList>
                                </div>
                                <div class="form-group" style="height: 5px"></div>

                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblAmount" CssClass="control-label" Text="Amount *" />
                                    <asp:TextBox runat="server" ID="txtAmount" CssClass="form-control" OnTextChanged="txtAmount_TextChanged" AutoPostBack="true" />
                                </div>
                                <%--<div class="form-group">
                                    <div class="alert alert-danger fade in">
                                        <center><asp:Label runat="server" ID="lblCurrency" Font-Bold="true" Font-Size="Medium" /></center>
                                        <center><asp:Label runat="server" ID="lblCurrencyUpdate"/></center>
                                    </div>
                                </div>--%>
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblTotal" CssClass="control-label" Text="Total Amount : " />
                                    <br />
                                    <asp:Label runat="server" ID="lblTotalAmount" Font-Bold="true" Font-Size="XX-Large" ForeColor="Red" />
                                </div>
                                <div class="form-group" style="height: 5px"></div>
                                <div class="form-group" style="height: 5px"></div>


                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblNotes" CssClass="control-label" Text="Notes *" />
                                    <asp:TextBox runat="server" ID="txtNotes" CssClass="form-control" />
                                    <%--TextMode="MultiLine" Rows="3"--%>
                                </div>
                                <div class="form-group" style="height: 5px"></div>

                                <div class="form-group" id="dvBtnProcess">
                                    <center><%--<a id="linkProcess" href="#process" runat="server" class="btn btn-success btn-lg" style="width:100%" onclick="javascript:ShowProgressBar()" onserverclick="linkProcess_ServerClick">Process</a>--%></center>
                                    <center><asp:Button runat="server" ID="btnProcess" Text="Process" CssClass="btn btn-success btn-lg" OnClick="btnProcess_Click" OnClientClick="javascript:ShowProgressBar()" Width="100%" /></center>
                                    <%--<center><asp:Label runat="server" ID="Label1" Text="Processing" ForeColor="Green" /></center>--%>
                                    <%--<asp:Button runat="server" ID="btnThumbnail" Text="Buat Thumbnail" CssClass="btn btn-success" OnClick="btnThumbnail_Click" />--%>
                                </div>

                                <div class="form-group" id="dvProcessing" style="visibility: hidden">
                                    <center><asp:Label runat="server" ID="Label1" Text="Processing" Font-Size="X-Large" ForeColor="Green" /></center>
                                </div>

                                <div class="form-group" style="height: 30px"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12"></div>
    </div>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Scripts" runat="server">
    <script type="text/javascript">
        function ShowProgressBar() {
            document.getElementById('dvProcessing').style.visibility = 'visible';
            document.getElementById('dvBtnProcess').style.visibility = 'hidden';
        }

        function HideProgressBar() {
            document.getElementById('dvProgressBar').style.visibility = "hidden";
        }
    </script>
</asp:Content>
