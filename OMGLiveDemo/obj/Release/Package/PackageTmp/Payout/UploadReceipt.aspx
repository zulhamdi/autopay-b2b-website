﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UploadReceipt.aspx.cs" Inherits="OMGLiveDemo.Payout.UploadReceipt" MaintainScrollPositionOnPostback="true" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" />
    <div class="row">
        <a class="hiddenanchor" id="process"></a>
        <div class="row x_title">
            <div class="col-md-12">
                <h3>Upload Receipt</h3>
            </div>
        </div>

        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="alert alert-danger alert-dismissible fade in" runat="server" id="diverror" visible="false">
                    <button type="button" runat="server" id="btnError" onserverclick="btnError_ServerClick" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <center><asp:Label runat="server" ID="lblError" Font-Size="14pt" Text="test error" /></center>
                </div>



                <asp:HiddenField runat="server" ID="hfRate" />
                <asp:HiddenField runat="server" ID="hfSenderImageURL" />
                <asp:HiddenField runat="server" ID="hfMargin" />
                <asp:HiddenField runat="server" ID="hfTotalAmount" />
                <asp:HiddenField runat="server" ID="hfBank" />
                <asp:HiddenField runat="server" ID="hfPayoutID" />
                <asp:HiddenField runat="server" ID="hfSenderFileID" />
                <asp:HiddenField runat="server" ID="hfSenderName" />

                <div class="dashboard_graph">
                    <div style="width: 100%;">
                        <div id="canvas_dahs12" class="demo-placeholder" style="width: 95%; height: 100%; margin-left: 2.5%; margin-right: 2.5%">

                            <div class="form-group" style="height: 25px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="Label2" CssClass="control-label" Text="Upload Receipt : *" />
                                <asp:FileUpload ID="fuReceipt" runat="server" CssClass="form-control" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group" id="dvBtnProcess" runat="server">
                                <div class="centered"><%--<a id="linkProcess" href="#process" runat="server" class="btn btn-success btn-lg" style="width:100%" onclick="javascript:ShowProgressBar()" onserverclick="linkProcess_ServerClick">Process</a>--%></div>
                                <div class="centered"><asp:Button runat="server" ID="btnProcess" Text="Upload Receipt" CssClass="btn btn-success btn-lg" OnClick="btnProcess_Click" OnClientClick="javascript:ShowProgressBar()" Width="100%" /></div>
                                <%--<center><asp:Label runat="server" ID="Label1" Text="Processing" ForeColor="Green" /></center>--%>
                                <%--<asp:Button runat="server" ID="btnThumbnail" Text="Buat Thumbnail" CssClass="btn btn-success" OnClick="btnThumbnail_Click" />--%>
                            </div>

                            <div class="form-group" runat="server" id="dvProcessing" style="display: none;">
                                <div class="centered"><asp:Label runat="server" ID="Label1" Text="Processing" Font-Size="X-Large" ForeColor="Green" /></div>
                            </div>
                            <div class="form-group" style="height: 5px"></div>
                        </div>
                    </div>
                </div>

                <br />

                <div class="dashboard_graph">
                    <div style="width: 100%;">
                        <div id="canvas_dahs3" class="demo-placeholder" style="width: 95%; height: 100%; margin-left: 2.5%; margin-right: 2.5%">
                            <div class="form-group" id="divCustomerNote" runat="server">
                                <div class="centered">
                                    <asp:Button runat="server" ID="btnViewCustomerNote" Text="Print Customer Note" CssClass="btn btn-success btn-lg" OnClick="btnViewCustomerNote_Click" Width="100%" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <br />

                <div class="dashboard_graph">
                    <div style="width: 100%;">
                        <div id="canvas_dahs1" class="demo-placeholder" style="width: 95%; height: 100%; margin-left: 2.5%; margin-right: 2.5%">

                            <div class="form-group" style="height: 30px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblAmount" CssClass="control-label" Text="Amount (in MYR)" />
                                <br />
                                <asp:Label runat="server" ID="lblAmountX" Font-Bold="true" Font-Size="XX-Large" ForeColor="Red" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblTotal" CssClass="control-label" Text="Total Amount : " />
                                <br />
                                <asp:Label runat="server" ID="lblTotalAmount" Font-Bold="true" Font-Size="XX-Large" ForeColor="Red" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="Label4" CssClass="control-label" Text="Total Transfer : " />
                                <br />
                                <asp:Label runat="server" ID="lblTotalTransfer" Font-Bold="true" Font-Size="XX-Large" ForeColor="Red" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblNotes" CssClass="control-label" Text="Notes *" />
                                <br />
                                <asp:Label runat="server" ID="lblNotesX" Font-Bold="true" Font-Size="Medium" />
                                <%--TextMode="MultiLine" Rows="3"--%>
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="col-md-12 col-sm-12 col-xs-12">



                <div class="dashboard_graph">
                    <div style="width: 100%;">
                        <div id="canvas_dahsaa" class="demo-placeholder" style="width: 95%; height: 100%; margin-left: 2.5%; margin-right: 2.5%">
                            <div class="form-group" style="height: 30px"></div>

                            <div class="centered"><h2>Please transfer the total amount to :</h2></div>

                            <br />

                            <div class="form-group">
                                <asp:Label runat="server" ID="Label5" CssClass="control-label" Text="Bank Name :" />
                                <br />
                                <asp:Label runat="server" ID="txtBankName" Font-Bold="true" Font-Size="Large" ForeColor="Red" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="Label8" CssClass="control-label" Text="Account No : " />
                                <br />
                                <asp:Label runat="server" ID="txtAccountNo" Font-Bold="true" Font-Size="Large" ForeColor="Red" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="Label9" CssClass="control-label" Text="Account Holder : " />
                                <br />
                                <asp:Label runat="server" ID="txtAccountHolder" Font-Bold="true" Font-Size="Large" ForeColor="Red" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Scripts" runat="server">

    <script type="text/javascript">
        function ShowProgressBar() {
            document.getElementById('dvProcessing').style.display = 'normal';
            document.getElementById('dvBtnProcess').style.display = 'none';
        }

        function HideProgressBar() {
            document.getElementById('dvProgressBar').style.display = "none";
        }
    </script>

    <script type="text/javascript">
        $(window).load(function () {
            if (window.isColorbox) {
                $.colorbox({ href: "ViewImage.aspx", iframe: true, width: "80%", height: "80%" });
            }
        });
        function OpenCBox() {
            $.colorbox({ href: "ViewImage.aspx", iframe: true, width: "80%", height: "80%" });
        }

    </script>


</asp:Content>
