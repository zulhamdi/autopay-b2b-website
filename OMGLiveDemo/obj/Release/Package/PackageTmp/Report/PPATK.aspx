﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PPATK.aspx.cs" Inherits="OMGLiveDemo.Report.PPATK" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" />

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph">

                <div class="row x_title">
                    <div class="col-md-12">
                        <h3>PPATK Report</h3>
                    </div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div style="width: 100%;">
                        <div id="canvas_dahs" class="demo-placeholder" style="width: 100%; height: 100%;">
                            <div class="col-md-12 col-sm-12 col-xs-12" style="background-color: #f8f8f8; margin-bottom: 20px; padding-top: 10px">
                                <div class="col-md-3 col-sm-12 col-xs-12" style="margin: 5px">
                                    <div class="form-group" runat="server" id="div2" visible="true">
                                        <asp:Label runat="server" ID="Label1" CssClass="control-label" Text="Remittance Line : " />
                                        <div style="margin-top: 5px">
                                            <asp:DropDownList runat="server" ID="ddlRemittanceLine" CssClass="form-control" OnSelectedIndexChanged="ddlRemittanceLine_SelectedIndexChanged" AutoPostBack="true">
                                                <asp:ListItem Selected="True" Text="Malaysia to Indonesia" Value="1" />
                                                <asp:ListItem Text="Indonesia to Malaysia" Value="2" />
                                            </asp:DropDownList>
                                        </div>
                                        <div class="form-group" style="height: 5px"></div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-12 col-xs-12" style="margin: 5px">
                                    <div class="form-group" runat="server" id="divpartner" visible="true">
                                        <asp:Label runat="server" ID="lblMonth" CssClass="control-label" Text="Month : " />
                                        <div style="margin-top: 5px">
                                            <asp:DropDownList runat="server" ID="ddlMonth" CssClass="form-control" OnSelectedIndexChanged="ddlMonth_SelectedIndexChanged" AutoPostBack="true" />
                                        </div>
                                        <div class="form-group" style="height: 5px"></div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-12 col-xs-12" style="margin: 5px">
                                    <div class="form-group" runat="server" id="div1" visible="true">
                                        <asp:Label runat="server" ID="Label7" CssClass="control-label" Text="Year : " />
                                        <div style="margin-top: 5px">
                                            <asp:DropDownList runat="server" ID="ddlYear" CssClass="form-control" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged" AutoPostBack="true" />
                                        </div>
                                        <div class="form-group" style="height: 5px"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12" style="overflow-x: auto">
                                <asp:GridView CssClass="table table-striped jambo_table bulk_action" ID="gvListItem" runat="server" AutoGenerateColumns="false" AllowPaging="True" PageSize="20" AllowCustomPaging="False" OnPageIndexChanging="gvListItem_PageIndexChanging">
                                    <PagerStyle HorizontalAlign="Center" CssClass="bs4-aspnet-pager" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="No.">
                                            <ItemTemplate>
                                                <asp:HiddenField runat="server" ID="hfReference" Value='<%# Eval("RemittanceID") %>' />
                                                <%# Container.DataItemIndex + 1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="LocalID" HeaderText="LocalID" />
                                        <asp:BoundField DataField="LTKL" HeaderText="No. LTKL Koreksi" />
                                        <asp:BoundField DataField="ReportType" HeaderText="Jenis Laporan" />
                                        <asp:BoundField DataField="PJK" HeaderText="PJK bertindak sebagai" />
                                        <asp:BoundField DataField="CountryRegion" HeaderText="Negara Bagian/Kota" />
                                        <asp:BoundField DataField="CountryFrom" HeaderText="Negara" />
                                        <asp:BoundField DataField="SenderAccount" HeaderText="NoRek" />
                                        <asp:BoundField DataField="SenderBankName" HeaderText="Nama Bank" />
                                        <asp:BoundField DataField="SenderName" HeaderText="Nama Korporasi" />
                                        <asp:BoundField DataField="SenderAddress" HeaderText="Alamat" />
                                        <asp:TemplateField HeaderStyle-Width="250px" HeaderText="Apakah sumber dana tunai?">
                                            <ItemTemplate>
                                                <span style="width: 250px"><%#Eval("SourceOfFund")%></span>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="medium" />
                                            <ItemStyle CssClass="medium" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="BeneficiaryIdentityType" HeaderText="Jenis" />
                                        <asp:BoundField DataField="BeneficiaryIdentity" HeaderText="No Identitas" />
                                        <asp:BoundField DataField="BeneficiaryAccount" HeaderText="No Rek" />
                                        <asp:BoundField DataField="BeneficiaryBank" HeaderText="Nama Bank" />
                                        <asp:BoundField DataField="BeneficiaryName" HeaderText="Nama Korporasi" />
                                        <asp:BoundField DataField="BeneficiaryAddress" HeaderText="Alamat" />
                                        <asp:BoundField DataField="BeneficiaryProvince" HeaderText="Provinsi" />
                                        <asp:BoundField DataField="BeneficiaryCity" HeaderText="Kabupaten/Kota" />
                                        <asp:BoundField DataField="TransactionDate" HeaderText="Tanggal Transaksi" />
                                        <asp:TemplateField HeaderStyle-Width="250px" HeaderText="Tanggal efektif penyelesaian transfer LN">
                                            <ItemTemplate>
                                                <span style="width: 250px"><%#Eval("ModifiedDate")%></span>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="medium" />
                                            <ItemStyle CssClass="medium" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="CurrencyFrom" HeaderText="Mata Uang asal yang diinstruksikan" />
                                        <asp:BoundField DataField="AmountFrom" HeaderText="Nilai uang asal yang diinstruksikan" />
                                        <asp:BoundField DataField="Rate" HeaderText="Kurs Nilai Tukar" />
                                        <asp:TemplateField HeaderStyle-Width="250px" HeaderText="Mata Uang yang diterima">
                                            <ItemTemplate>
                                                <span style="width: 250px"><%#Eval("CurrencyTo")%></span>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="medium" />
                                            <ItemStyle CssClass="medium" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderStyle-Width="250px" HeaderText="Nilai Uang yang diterima dalam Rupiah">
                                            <ItemTemplate>
                                                <span style="width: 250px"><%#Eval("AmountTo")%></span>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="medium" />
                                            <ItemStyle CssClass="medium" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Purpose" HeaderText="Tujuan transaksi" />
                                        <asp:BoundField DataField="SourceOfFundTo" HeaderText="Sumber penggunaan dana" />
                                    </Columns>
                                </asp:GridView>
                                <asp:Button runat="server" ID="btnDownload" OnClick="btnDownload_Click" Text="Download Report" CssClass="btn btn-success" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Scripts" runat="server">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" />
    <script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript">
        // When the document is ready
        $(document).ready(function () {

            $("input[id*='datepurchased']").datepicker({
                format: "yyyy/mm/dd"
            });
            $("input[id*='startdate']").datepicker({
                format: "yyyy/mm/dd"
            });
            $("input[id*='enddate']").datepicker({
                format: "yyyy/mm/dd"
            });

        });
    </script>

    <style>
        .small {
            width: 50px;
            min-width: 50px;
            max-width: 50px;
        }

        .medium {
            width: 150px;
            min-width: 150px;
            max-width: 150px;
        }

        .wide {
            width: 250px;
            min-width: 250px;
            max-width: 250px;
        }
    </style>
</asp:Content>
