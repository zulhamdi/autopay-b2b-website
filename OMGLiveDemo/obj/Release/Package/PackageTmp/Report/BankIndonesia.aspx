﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BankIndonesia.aspx.cs" Inherits="OMGLiveDemo.Report.BankIndonesia" EnableEventValidation="false" UICulture="id-ID" Culture="id-ID" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" />

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph">

                <div class="row x_title">
                    <div class="col-md-12">
                        <h3>Bank Indonesia Report</h3>
                    </div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div style="width: 100%;">
                        <div id="canvas_dahs" class="demo-placeholder" style="width: 100%; height: 100%;">
                            <div class="row col-md-12 col-sm-12 col-xs-12" style="background-color: #f8f8f8; margin-bottom: 20px; padding-top: 10px">
                                
                                <div class="col-md-3 " style="margin: 5px">
                                    <div class="form-group" runat="server" id="div2" visible="true">
                                        <asp:Label runat="server" ID="Label1" CssClass="control-label" Text="Remittance Line : " />
                                        <div style="margin-top: 5px">
                                            <asp:DropDownList runat="server" ID="ddlRemittanceLine" CssClass="form-control" OnSelectedIndexChanged="ddlRemittanceLine_SelectedIndexChanged" AutoPostBack="true">
                                                <asp:ListItem Selected="True" Text="Malaysia to Indonesia" Value="1" />
                                                <asp:ListItem Text="Indonesia to Malaysia" Value="2" />
                                            </asp:DropDownList>
                                        </div>
                                        <div class="form-group" style="height: 5px"></div>
                                    </div>
                                </div>

                                <div class="col-md-2 " style="margin: 5px">
                                    <%--<div class="form-group" runat="server" id="divpartner" visible="true">
                                        <asp:Label runat="server" ID="lblMonth" CssClass="control-label" Text="Month : " />
                                        <div style="margin-top: 5px">
                                            <asp:DropDownList runat="server" ID="ddlMonth" CssClass="form-control" OnSelectedIndexChanged="ddlMonth_SelectedIndexChanged" AutoPostBack="true" />
                                        </div>
                                        <div class="form-group" style="height: 5px"></div>
                                    </div>--%>

                                    <div class="form-group" runat="server" id="divfilterbydate" visible="true">
                                        <asp:Label runat="server" ID="Label2" CssClass="control-label" Text="Filter by : " />
                                        <div style="margin-top: 5px">
                                            <asp:DropDownList runat="server" ID="ddlFilterByDate" CssClass="form-control" OnSelectedIndexChanged="ddlFilterByDate_SelectedIndexChanged" AutoPostBack="true" CausesValidation="true">
                                                <asp:ListItem Text="All" Value="all" Selected="True"></asp:ListItem>
                                                <asp:ListItem Text="Date" Value="date"></asp:ListItem>
                                                <asp:ListItem Text="Date Range" Value="daterange"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-2 " >
                                  <%--  <div class="form-group" runat="server" id="div1" visible="true">
                                        <asp:Label runat="server" ID="Label7" CssClass="control-label" Text="Year : " />
                                        <div style="margin-top: 5px">
                                            <asp:DropDownList runat="server" ID="ddlYear" CssClass="form-control" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged" AutoPostBack="true" />
                                        </div>
                                        <div class="form-group" style="height: 5px"></div>
                                    </div>--%>

                                    
                                    <div class="form-group" runat="server" id="divsingledate" visible="false">
                                        <asp:Label runat="server" ID="Label3" CssClass="control-label" Text="Choose Date : " />
                                        <div style="margin-top: 5px">
                                            <asp:TextBox ID="searchdate" CssClass="form-control" runat="server" />
                                            <div style="height: 2px"></div>
                                            <asp:Button runat="server" ID="btnSingleDate" Text="Search" CssClass="btn btn-success btn-sm" OnClick="btnSingleDate_Click" />
                                        </div>
                                    </div>
                                    
                                   
                                    <div class="form-group" runat="server" id="divdaterange" visible="false">
                                        <div class="row col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <asp:Label runat="server" ID="Label4" CssClass="control-label" Text="Start Date : " />
                                                <div style="margin-top: 5px">
                                                    <asp:TextBox ID="startdate" CssClass="form-control" runat="server" />
                                                    <div style="height: 2px"></div>
                                                    <asp:Button runat="server" ID="btnDateRange" Text="Search" CssClass="btn btn-success btn-sm" OnClick="btnDateRange_Click" />
                                                </div>
                                                <div class="form-group" style="height: 5px"></div>
                                            </div>

                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <asp:Label runat="server" ID="Label5" CssClass="control-label" Text="End Date : " />
                                                <div style="margin-top: 5px">
                                                    <asp:TextBox ID="enddate" CssClass="form-control" runat="server" />
                                                </div>
                                                <div class="form-group" style="height: 5px"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12" style="overflow-x: auto">
                                <asp:GridView
                                    CssClass="table table-striped jambo_table bulk_action"
                                    ID="gvListItem" runat="server"
                                    AutoGenerateColumns="False"
                                    AllowPaging="True"
                                    PageSize="20"
                                    OnPageIndexChanging="gvListItem_PageIndexChanging">
                                    <PagerStyle HorizontalAlign="Center" CssClass="bs4-aspnet-pager" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="No.">
                                            <ItemTemplate>
                                                <asp:HiddenField runat="server" ID="hfReference" Value='<%# Eval("RemittanceID") %>' />
                                                <%# Container.DataItemIndex + 1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="CountryFrom" HeaderText="Country From" />
                                        <asp:BoundField DataField="CountryTo" HeaderText="Country To" />
                                        <asp:BoundField DataField="SenderName" HeaderText="Sender Name" />
                                        <asp:BoundField DataField="BeneficiaryName" HeaderText="Beneficiary Name" />
                                        <asp:BoundField DataField="TotalTransfer" HeaderText="Total Transfer" DataFormatString="{0:C}" />
                                        <asp:BoundField DataField="Volume" HeaderText="Volume Transaction" />
                                        <asp:BoundField DataField="Purpose" HeaderText="Purpose of Transaction" />
                                    </Columns>
                                </asp:GridView>
                                <asp:Button runat="server" ID="btnDownload" OnClick="btnDownload_Click" Text="Download Report" CssClass="btn btn-success" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Scripts" runat="server">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" />
    <script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript">
        // When the document is ready
        $(document).ready(function () {

             $("input[id*='searchdate']").datepicker({
                format: "yyyy/mm/dd"
             });
             //$("input[id*='searchdate']").datepicker({
             //   format: "yyyy/mm/dd"
             //}).change(function () {
             //    window.location.href = '../Report/BankIndonesia.aspk/doSingleDateDisplay';
             //});
            $("input[id*='datepurchased']").datepicker({
                format: "yyyy/mm/dd"
            });
            $("input[id*='startdate']").datepicker({
                format: "yyyy/mm/dd"
            });
            $("input[id*='enddate']").datepicker({
                format: "yyyy/mm/dd"
            });

        });
    </script>
</asp:Content>
