﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ViewAuditLog.aspx.cs" Inherits="OMGLiveDemo.Setting.ViewAuditLog" MaintainScrollPositionOnPostback="true" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" />

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph">

                <div class="row x_title">
                    <div class="col-md-12">
                        <h3>Audit Logs</h3>
                    </div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div style="width: 100%;">
                        <div class="col-md-12 col-sm-12 col-xs-12" id="divSearch">
                            <div class="col-md-12 col-sm-12 col-xs-12" style="background-color: #f8f8f8; margin-bottom: 20px; padding-top: 10px">
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <asp:Label runat="server" CssClass="control-label" Text="Event type : " />
                                        <asp:DropDownList runat="server" ID="ddlEventType" CssClass="form-control" OnSelectedIndexChanged="ddlEventType_SelectedIndexChanged" AutoPostBack="true">
                                            <asp:ListItem Text="All" Value="All" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="Administration" Value="administration"></asp:ListItem>
                                            <asp:ListItem Text="Operation" Value="operation"></asp:ListItem>
                                            <asp:ListItem Text="Transaction" Value="transaction"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" CssClass="control-label" Text="Filter By User : " />
                                        <asp:DropDownList runat="server" ID="ddlUserID" CssClass="form-control" OnSelectedIndexChanged="ddlUserID_SelectedIndexChanged" 
                                            AutoPostBack="true" AppendDataBoundItems="true">
                                            <asp:ListItem Text="All" Value="All" Selected="True"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    
                                    <div class="form-group"></div>
                                    <div class="form-group" runat="server" id="divfilterbydate" visible="true">
                                        <asp:Label runat="server" ID="Label1" CssClass="control-label" Text="Filter by : " />
                                        <div style="margin-top: 5px">
                                            <asp:DropDownList runat="server" ID="ddlFilterByDate" CssClass="form-control" OnSelectedIndexChanged="ddlFilterByDate_SelectedIndexChanged" AutoPostBack="true" CausesValidation="true">
                                                <asp:ListItem Text="All" Value="all" Selected="True"></asp:ListItem>
                                                <asp:ListItem Text="Date" Value="date"></asp:ListItem>
                                                <asp:ListItem Text="Date Range" Value="daterange"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group"></div>
                                    <div class="form-group" runat="server" id="divsingledate" visible="false">
                                        <asp:Label runat="server" ID="Label2" CssClass="control-label" Text="Choose Date : " />
                                        <div style="margin-top: 5px">
                                            <asp:TextBox ID="searchdate" CssClass="form-control" runat="server" />
                                            <div style="height: 2px"></div>
                                            <asp:Button runat="server" ID="btnSingleDate" Text="Search" CssClass="btn btn-success btn-sm" OnClick="btnSingleDate_Click" />
                                        </div>
                                    </div>
                                    
                                    <div class="form-group"></div>
                                    <div class="form-group" runat="server" id="divdaterange" visible="false">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="col-md-6 col-sm-12 col-xs-12">
                                                <asp:Label runat="server" ID="Label4" CssClass="control-label" Text="Start Date : " />
                                                <div style="margin-top: 5px">
                                                    <asp:TextBox ID="startdate" CssClass="form-control" runat="server" />
                                                    <div style="height: 2px"></div>
                                                    <asp:Button runat="server" ID="btnDateRange" Text="Search" CssClass="btn btn-success btn-sm" OnClick="btnDateRange_Click" />
                                                </div>
                                                <div class="form-group" style="height: 5px"></div>
                                            </div>

                                            <div class="col-md-6 col-sm-12 col-xs-12">
                                                <asp:Label runat="server" ID="Label5" CssClass="control-label" Text="End Date : " />
                                                <div style="margin-top: 5px">
                                                    <asp:TextBox ID="enddate" CssClass="form-control" runat="server" />
                                                </div>
                                                <div class="form-group" style="height: 5px"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div id="canvas_dahs" class="demo-placeholder" style="width: 100%; height: 100%;">
                                <asp:GridView CssClass="table table-striped jambo_table bulk_action" 
                                    ID="gvListItem" runat="server" 
                                    AutoGenerateColumns="false" 
                                    DataKeyNames="ID" 
                                    AllowPaging="True" 
                                    PageSize="20" AllowCustomPaging="False" 
                                    OnPageIndexChanging="gvListItem_PageIndexChanging">
                                     <PagerStyle HorizontalAlign="Center" CssClass="bs4-aspnet-pager" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="No." ItemStyle-Width="2%" ItemStyle-Wrap="false" >
                                            <ItemTemplate>
                                                <asp:HiddenField runat="server" ID="hfLogID" Value='<%# Eval("ID") %>' />
                                                <asp:HiddenField runat="server" ID="hfEventBy" Value='<%# Eval("EventBy") %>' />
                                                <%# Container.DataItemIndex + 1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="EventDate" HeaderText="Date & Time" ItemStyle-Width="10%" ItemStyle-Wrap="false" />
                                        <asp:BoundField DataField="EventType" HeaderText="Event Type" ItemStyle-Width="10%" ItemStyle-Wrap="false" />
                                        <asp:BoundField DataField="EventData" HeaderText="Activity" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Scripts" runat="server">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" />
    <script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript">
        // When the document is ready
        $(document).ready(function () {

            $("input[id*='searchdate']").datepicker({
                format: "yyyy/mm/dd"
            });
            $("input[id*='startdate']").datepicker({
                format: "yyyy/mm/dd"
            });
            $("input[id*='enddate']").datepicker({
                format: "yyyy/mm/dd"
            });

        });
    </script>
</asp:Content>
