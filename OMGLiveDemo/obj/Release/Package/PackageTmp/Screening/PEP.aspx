﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PEP.aspx.cs" Inherits="OMGLiveDemo.Screening.PEP" MaintainScrollPositionOnPostback="true" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" />

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph">

                <div class="row x_title">
                    <div class="col-md-12">
                        <h3>Umrah & Hajj List</h3>
                    </div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div style="width: 100%;">
                        <!--
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="col-md-12 col-sm-12 col-xs-12" style="background-color: #f8f8f8; margin-bottom: 20px; padding-top: 10px">
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <asp:Label runat="server" CssClass="control-label" Text="Event type" />
                                        <asp:DropDownList runat="server" ID="ddlEventType" CssClass="form-control" OnSelectedIndexChanged="ddlEventType_SelectedIndexChanged" AutoPostBack="true">
                                            <asp:ListItem Text="All" Value="1" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="Administration" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="Transaction" Value="3"></asp:ListItem>
                                        </asp:DropDownList>
                                     </div>
                                     <div class="form-group" style="height: 5px"></div>
                                </div>
                            </div>
                        </div>
                        -->

                        <div id="divSearch" class="demo-placeholder" style="width: 100%; height: 100%;">
                            <div class="col-md-12 col-sm-12 col-xs-12">

                                <div class="col-md-12 col-sm-12 col-xs-12" style="background-color: #f8f8f8; margin-bottom: 20px; padding-top: 10px">
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        
                                        <div class="form-group" runat="server" id="divSearchByID">
                                            <asp:Label runat="server" ID="Label3" CssClass="control-label" Text="Search by ID : " />
                                            <div style="margin-top: 5px">
                                                <asp:TextBox ID="txtSearchID" CssClass="form-control" runat="server" />
                                                <div style="height: 2px"></div>
                                                <asp:Button runat="server" ID="btnSearchID" Text="Search" CssClass="btn btn-success btn-sm" OnClick="btnSearchID_Click" />
                                            </div>
                                            <div class="form-group" style="height: 2px"></div>
                                        </div>

                                        <div class="form-group" runat="server" id="divSearchByName">
                                            <asp:Label runat="server" ID="Label6" CssClass="control-label" Text="Search by Name : " />
                                            <div style="margin-top: 5px">
                                                <asp:TextBox ID="txtSearchName" CssClass="form-control" runat="server" />
                                                <div style="height: 2px"></div>
                                                <asp:Button runat="server" ID="btnSearchName" Text="Search" CssClass="btn btn-success btn-sm" OnClick="btnSearchName_Click" />
                                            </div>
                                            <div class="form-group" style="height: 2px"></div>
                                        </div>

                                        <div class="form-group" runat="server" id="divSearchByDOB">
                                            <asp:Label runat="server" ID="Label7" CssClass="control-label" Text="Search by Date of Birth : " />
                                            <div style="margin-top: 5px">
                                                <asp:TextBox ID="searchDOB" CssClass="form-control" runat="server" />
                                                <div style="height: 2px"></div>
                                                <asp:Button runat="server" ID="btnSearchByDOB" Text="Search" CssClass="btn btn-success btn-sm" OnClick="btnSearchByDOB_Click" />
                                            </div>
                                            <div class="form-group" style="height: 2px"></div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div id="canvas_dahs" class="demo-placeholder" style="width: 100%; height: 100%;">
                                <asp:GridView CssClass="table table-striped jambo_table bulk_action" ID="gvListItem" runat="server" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" DataKeyNames="IDNo" AllowPaging="True" PageSize="20" AllowCustomPaging="False" OnPageIndexChanging="gvListItem_PageIndexChanging">
                                    <Columns>
                                        <asp:TemplateField HeaderText="No.">
                                            <ItemTemplate>
                                                <asp:HiddenField runat="server" ID="hfIDNo" Value='<%# Eval("IDNo") %>' />
                                                <%# Container.DataItemIndex + 1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="IDNo" HeaderText="Identification No" />
                                        <asp:BoundField DataField="Name" HeaderText="Name" />
                                        <asp:BoundField DataField="DOB" HeaderText="Date of Birth" DataFormatString="{0:d}" />
                                        <asp:BoundField DataField="Address" HeaderText="Address" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Scripts" runat="server">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" />
    <script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript">
        // When the document is ready
        $(document).ready(function () {

            $("input[id*='searchDOB']").datepicker({
                format: "yyyy/mm/dd"
            });
        });
    </script>
</asp:Content>
