﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RateAgent.aspx.cs" Inherits="OMGLiveDemo.RateAgent" MaintainScrollPositionOnPostback="true" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" />
    <div class="row">
        <div class="row x_title">
            <div class="col-md-12">
                <h3>Agent Rate</h3>
            </div>
        </div>

        <div class="col-md-3 col-sm-12 col-xs-12">
        </div>

        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="dashboard_graph">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div style="width: 100%;">
                        <div id="canvas_dahs" class="demo-placeholder" style="width: 100%; height: 100%;">
                            <div class="form-group" style="height: 30px"></div>

                            <asp:HiddenField runat="server" ID="hfRateMYR" />

                            <div class="alert alert-danger alert-dismissible fade in" runat="server" id="diverror" visible="false">
                                <button type="button" runat="server" id="btnError" onserverclick="btnError_ServerClick" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <center><asp:Label runat="server" ID="lblError" /></center>
                            </div>

                            <div class="alert alert-success alert-dismissible fade in" runat="server" id="divsuccess" visible="false">
                                <button type="button" runat="server" id="btnSuccess" onserverclick="btnSuccess_ServerClick" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <center><asp:Label runat="server" ID="lblSuccess" /></center>
                            </div>

                            <div class="form-group">
                                <asp:Label runat="server" CssClass="control-label" Text="Choose Remittance" />
                                <asp:DropDownList runat="server" ID="ddlChooseRemittance" CssClass="form-control" OnSelectedIndexChanged="ddlChooseRemittance_SelectedIndexChanged" AutoPostBack="true">
                                    <%--<asp:ListItem Text="Malaysia to Indonesia" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Indonesia to Malaysia" Value="2"></asp:ListItem>--%>
                                </asp:DropDownList>
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div runat="server" id="divmyr" visible="true">
                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="control-label" Text="Country : " />
                                    <br />
                                    <asp:Label runat="server" ID="lblCountryMYR" Font-Bold="true" Font-Size="X-Large" />
                                </div>
                                <div class="form-group" style="height: 5px"></div>

                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="control-label" Text="Currency : " />
                                    <br />
                                    <asp:Label runat="server" ID="lblCurrencyMYR" Font-Bold="true" Font-Size="X-Large" />
                                </div>
                                <div class="form-group" style="height: 5px"></div>

                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="control-label" Text="Base Rate : " />
                                    <br />
                                    <asp:Label runat="server" ID="lblBaseRateMYR" Font-Bold="true" Font-Size="X-Large" />
                                </div>
                                <div class="form-group" style="height: 5px"></div>


                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblMarginMYR" CssClass="control-label" Text="Margin (in IDR) *" />
                                    <asp:TextBox runat="server" ID="txtMarginMYR" CssClass="form-control" OnTextChanged="txtMarginMYR_TextChanged" AutoPostBack="true" Font-Size="20pt" TextMode="MultiLine" Rows="1" />
                                </div>
                                <div class="form-group">
                                    <div class="alert alert-danger fade in">
                                        <center><asp:Label runat="server" ID="lblRateMYR" Font-Bold="true" Font-Size="Medium" /></center>
                                        <center><asp:Label runat="server" ID="lblRateUpdateMYR"/></center>
                                    </div>
                                </div>
                                <div class="form-group" style="height: 5px"></div>

                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="control-label" Text="Retail Rate : " />
                                    <br />
                                    <asp:Label runat="server" ID="lblRetailRateMYR" Font-Bold="true" Font-Size="XX-Large" ForeColor="Red" />
                                </div>
                                <div class="form-group" style="height: 5px"></div>

                                <div class="form-group">
                                    <center><asp:Button runat="server" ID="btnUpdateMYR" Text="Update" CssClass="btn btn-success btn-lg" OnClick="btnUpdateMYR_Click" Width="100%" /></center>
                                </div>
                            </div>

                            <div runat="server" id="dividr" visible="false">
                                <asp:HiddenField runat="server" ID="hfRateIDR" />

                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="control-label" Text="Country : " />
                                    <br />
                                    <asp:Label runat="server" ID="lblCountryIDR" Font-Bold="true" Font-Size="X-Large" />
                                </div>
                                <div class="form-group" style="height: 5px"></div>

                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="control-label" Text="Currency : " />
                                    <br />
                                    <asp:Label runat="server" ID="lblCurrencyIDR" Font-Bold="true" Font-Size="X-Large" />
                                </div>
                                <div class="form-group" style="height: 5px"></div>

                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="control-label" Text="Base Rate : " />
                                    <br />
                                    <asp:Label runat="server" ID="lblBaseRateIDR" Font-Bold="true" Font-Size="X-Large" />
                                </div>
                                <div class="form-group" style="height: 5px"></div>


                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblMarginIDR" CssClass="control-label" Text="Margin (in IDR) *" />
                                    <asp:TextBox runat="server" ID="txtMarginIDR" CssClass="form-control" OnTextChanged="txtMarginIDR_TextChanged" AutoPostBack="true" Font-Size="20pt" TextMode="MultiLine" Rows="1" />
                                </div>
                                <div class="form-group">
                                    <div class="alert alert-danger fade in">
                                        <center><asp:Label runat="server" ID="lblRateIDR" Font-Bold="true" Font-Size="Medium" /></center>
                                        <center><asp:Label runat="server" ID="lblRateUpdateIDR"/></center>
                                    </div>
                                </div>
                                <div class="form-group" style="height: 5px"></div>

                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="control-label" Text="Retail Rate : " />
                                    <br />
                                    <asp:Label runat="server" ID="lblRetailRateIDR" Font-Bold="true" Font-Size="XX-Large" ForeColor="Red" />
                                </div>
                                <div class="form-group" style="height: 5px"></div>

                                <div class="form-group">
                                    <center><asp:Button runat="server" ID="btnUpdateIDR" Text="Update" CssClass="btn btn-success btn-lg" OnClick="btnUpdateIDR_Click" Width="100%" /></center>
                                </div>
                            </div>
                            <div class="form-group" style="height: 30px"></div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="col-md-3 col-sm-12 col-xs-12">
        </div>
    </div>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Scripts" runat="server">
</asp:Content>
