﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ApproveBeneficiary.aspx.cs" Inherits="OMGLiveDemo.Beneficiaries.ApproveBeneficiary" MaintainScrollPositionOnPostback="true" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" />

    <div class="row">
        <div class="row x_title">
            <div class="col-md-12">
                <h3>Approve Sender</h3>
            </div>
        </div>

        <div class="col-md-3 col-sm-12 col-xs-12"></div>
        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="dashboard_graph">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div style="width: 100%;">
                        <div id="canvas_dahs" class="demo-placeholder" style="width: 100%; height: 100%;">
                            <div class="form-group" style="height: 30px"></div>
                            
                            <asp:HiddenField runat="server" ID="hfBeneficiaryID" />
                            <asp:HiddenField runat="server" ID="hfBeneficiaryImageURL" />
                            <asp:HiddenField runat="server" ID="hfSenderPicURL" />

                            <div class="alert alert-danger alert-dismissible fade in" runat="server" id="diverror" visible="false">
                                <button type="button" runat="server" id="btnError" onserverclick="btnError_ServerClick" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <center><asp:Label runat="server" ID="lblError" /></center>
                            </div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblName" CssClass="control-label" Text="Full Name *" />
                                <asp:TextBox runat="server" ID="txtName" CssClass="form-control" Enabled="false" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblBank" CssClass="control-label" Text="Bank *" />
                                <asp:TextBox runat="server" ID="txtBank" CssClass="form-control" Enabled="false" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblAccount" CssClass="control-label" Text="Beneficiary Account *" />
                                <asp:TextBox runat="server" ID="txtAccount" CssClass="form-control" Enabled="false" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblAddress" CssClass="control-label" Text="Address * " />
                                <asp:TextBox runat="server" ID="txtAddress" CssClass="form-control" TextMode="MultiLine" Rows="3" Enabled="false"/>
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblPhone" CssClass="control-label" Text="Phone *" />
                                <asp:TextBox runat="server" ID="txtPhone" CssClass="form-control" Enabled="false" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblEmail" CssClass="control-label" Text="Email " />
                                <asp:TextBox runat="server" ID="txtEmail" CssClass="form-control" Enabled="false" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="Label4" CssClass="control-label" Text="Reject reason* : " />
                                <asp:TextBox runat="server" ID="txtRejectReason" CssClass="form-control" Enabled="true" />
                            </div>

                            <div class="form-group" id="dvBtnProcess">
                                <asp:Button runat="server" ID="btnApprove" Text="Approve" CssClass="btn btn-success btn-lg" OnClick="btnApprove_Click" OnClientClick="javascript:ShowProgressBar()" />
                                <asp:Button runat="server" ID="btnAmend" Text="Amend" CssClass="btn btn-info btn-lg" OnClick="btnAmend_Click" OnClientClick="javascript:ShowProgressBar()" />
                                <asp:Button runat="server" ID="btnReject" Text="Reject" CssClass="btn btn-danger btn-lg" OnClick="btnReject_Click" OnClientClick="javascript:ShowProgressBar()" />
                            </div>

                            <div class="form-group" id="dvProcessing" style="display: none;">
                                <center><asp:Label runat="server" ID="Label1" Text="Processing" Font-Size="X-Large" ForeColor="Green" /></center>
                            </div>

                            <div class="form-group" style="height: 30px"></div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12"></div>
    </div>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Scripts" runat="server">
    <script type="text/javascript">
        function ShowProgressBar() {
            document.getElementById('dvProcessing').style.display = 'normal';
            document.getElementById('dvBtnProcess').style.display = 'none';
        }

        function HideProgressBar() {
            document.getElementById('dvProgressBar').style.display = "none";
        }
    </script>

    <script type="text/javascript">
        $(window).load(function () {
            if (window.isColorbox) {
                $.colorbox({ href: "../Payout/ViewImage.aspx", iframe: true, width: "80%", height: "80%" });
            }
        });
        function OpenCBox() {
            $.colorbox({ href: "../Payout/ViewImage.aspx", iframe: true, width: "80%", height: "80%" });
        }

    </script>

</asp:Content>
