﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddIndonesianBeneficiaries.aspx.cs" Inherits="OMGLiveDemo.Beneficiaries.AddIndonesianBeneficiaries" MaintainScrollPositionOnPostback="true" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" />

    <div class="row">
        <div class="row x_title">
            <div class="col-md-12">
                <h3>Add Indonesian Beneficiary</h3>
            </div>
        </div>

        <div class="col-md-3 col-sm-12 col-xs-12"></div>
        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="dashboard_graph">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div style="width: 100%;">
                        <div id="canvas_dahs" class="demo-placeholder" style="width: 100%; height: 100%;">
                            <div class="form-group" style="height: 30px"></div>

                            <div class="alert alert-danger alert-dismissible fade in" runat="server" id="diverror" visible="false">
                                <button type="button" runat="server" id="btnError" onserverclick="btnError_ServerClick" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <center><asp:Label runat="server" ID="lblError" /></center>
                            </div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblName" CssClass="control-label" Text="Beneficiary Name *" />
                                <asp:TextBox runat="server" ID="txtName" CssClass="form-control" Enabled="false" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblBank" CssClass="control-label" Text="Beneficiary Bank *" />
                                <asp:DropDownList runat="server" ID="ddlBank" CssClass="form-control" OnSelectedIndexChanged="ddlBank_SelectedIndexChanged" AutoPostBack="true" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblAccount" CssClass="control-label" Text="Beneficiary Account *" />
                                <asp:TextBox runat="server" ID="txtAccount" CssClass="form-control" />
                            </div>
                            <div class="form-group" runat="server" id="divvalidate">
                                <asp:Button runat="server" ID="btnValidate" Text="Validate" CssClass="btn btn-warning" OnClick="btnValidate_Click" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblAlias" CssClass="control-label" Text="Beneficiary Alias " />
                                <asp:TextBox runat="server" ID="txtAlias" CssClass="form-control" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="Label21" CssClass="control-label" Text="City *" />
                                <asp:TextBox runat="server" ID="txtCity" CssClass="form-control" Enabled="false" />
                                <div style="margin-top: 5px;">
                                    <asp:Button runat="server" ID="btnCheckCity" Text="Find" CssClass="btn btn-success btn-sm" OnClick="btnCheckCity_Click" />
                                </div>
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="Label1" CssClass="control-label" Text="Beneficiary Address *" />
                                <asp:TextBox runat="server" ID="txtAddress" CssClass="form-control" Rows="3" TextMode="MultiLine" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblPhone" CssClass="control-label" Text="Beneficiary Phone *" />
                                <asp:TextBox runat="server" ID="txtPhone" CssClass="form-control" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblEmail" CssClass="control-label" Text="Beneficiary Email *" />
                                <asp:TextBox runat="server" ID="txtEmail" CssClass="form-control" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                           <div class="form-group">
                                <center><asp:Button runat="server" ID="btnAdd" Text="Add Beneficiaries" CssClass="btn btn-success btn-lg" OnClick="btnAdd_Click" Width="100%" /></center>
                            </div>

                            <div class="form-group" style="height: 30px"></div>
                        </div>
                    </div>
                    <%--================================ M O D A L   C I T Y =======================================--%>
                    <%--================================ M O D A L   C I T Y =======================================--%>
                    <%--================================ M O D A L   C I T Y =======================================--%>
                    <%--================================ M O D A L   C I T Y =======================================--%>

                    <div class="modal fade bs-example-modal-lg" id="myModalCity" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">

                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                    <h4 class="modal-title" id="myModalLabel1">Choose City</h4>
                                </div>
                                <div class="modal-body">
                                    <asp:HiddenField runat="server" ID="hfCityID" />
                                    <asp:HiddenField runat="server" ID="hfCityName" />
                                    <asp:HiddenField runat="server" ID="hfModalCityOpen" />
                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label18" CssClass="control-label" Text="City *" />
                                        <asp:TextBox runat="server" ID="txtFindCity" CssClass="form-control" />
                                        <div style="margin-top: 5px">
                                            <asp:Button runat="server" ID="btnFindCity" Text="Find" CssClass="btn btn-success btn-sm" OnClick="btnFindCity_Click" />
                                        </div>
                                    </div>
                                    <div class="form-group" style="height: 5px"></div>

                                    <div id="canvas_dahs11" class="demo-placeholder" style="width: 100%; height: 100%;">
                                        <asp:GridView CssClass="table table-striped jambo_table bulk_action" ID="gvListCity" Visible="false" runat="server" AutoGenerateColumns="false" DataKeyNames="ID">
                                            <Columns>
                                                <asp:TemplateField HeaderText="No.">
                                                    <ItemTemplate>
                                                        <asp:HiddenField runat="server" ID="hfCityIDModal" Value='<%# Eval("ID") %>' />
                                                        <asp:HiddenField runat="server" ID="hfCityNameModal" Value='<%# Eval("City") %>' />
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="City" HeaderText="City" />
                                                <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <asp:Button ID="btnChooseCity" CssClass="form-control" runat="server" Text="Choose" OnClick="btnChooseCity_Click" UseSubmitBehavior="false" CausesValidation="false" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12"></div>
    </div>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Scripts" runat="server">
    <script type="text/javascript">
        $(window).load(function () {
            if (window.isModalCity) {
                $('#myModalCity').modal('show');
            }
            else {
                $('#myModalCity').modal('hide');
            }
        });
    </script>
</asp:Content>
