﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BeneficiaryDetail.aspx.cs" Inherits="OMGLiveDemo.Beneficiaries.BeneficiaryDetail" %>
<%@ MasterType VirtualPath="~/Site.Master" %>

<!DOCTYPE html>

<html lang="en">
<head runat="server" aria-expanded="false">
    <title>AutoPay - Sender's Detail View</title>

    <!-- Bootstrap -->
    <link href="../Content/bootstrap.min.css" rel="stylesheet"/>
    <!-- Font Awesome -->
    <link href="../Content/font-awesome.min.css" rel="stylesheet"/>

    <!-- Custom Theme Style 
    <link href="../Content/base-custom.min.css" rel="stylesheet" />
    -->
    <link href="../Content/custom-alt.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server" class="form-horizontal form-label-left">
        <asp:ScriptManager runat="server" />
        <div class="container body">
            <div class="main_container">
                <div class="right_col" role="main">
                    <div class="page-title">
                        <div class="title_left">
                            <h3>Beneficiary's Details</h3>
                        </div>

                    </div>

                    <div class="clearfix"></div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h4>Beneficiary Information:</h4>
                                </div>
                                <div class="x_content">
                                    
                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label9" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="Sender Name :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtName" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                        
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label15" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="Bank :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtBank" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                        
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label2" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="Account No :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtAccount" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                        
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label5" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="Alias :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtAlias" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                        
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label3" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="Address :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtAddress" CssClass="form-control" TextMode="MultiLine" Rows="3" Enabled="false" />
                                        </div>
                                        
                                    </div>

                                    <div class="form-group" runat="server" id="divCity" visible="false">
                                        <asp:Label runat="server" ID="Label6" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="City :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtCity" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                        
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label10" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="Country :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtCountry" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                        
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label1" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="Phone :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtPhone" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                        
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label4" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="Email :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtEmail" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                        
                                    </div>

                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
