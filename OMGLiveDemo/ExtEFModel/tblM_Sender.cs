﻿namespace OMGLiveDemo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class tblM_Sender
    {
        [NotMapped]
        public string SenderType { get; set; }
        [NotMapped]
        public string IDType { get; set; }
        [NotMapped]
        public string IDNumber { get; set; }
        [NotMapped]
        public string LegalitasType { get; set; }
        [NotMapped]
        public string LegalitasNumber { get; set; }
    }
}
