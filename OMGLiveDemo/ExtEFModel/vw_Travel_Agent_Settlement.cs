﻿namespace OMGLiveDemo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class vw_Travel_Agent_Settlement
    {
        [NotMapped]
        public string AmountText { get; set; }

        [NotMapped]
        public string BalancePreviousText { get; set; }

        [NotMapped]
        public string AdminFeeText { get; set; }

        [NotMapped]
        public string BalanceText { get; set; }

        [NotMapped]
        public string RemittanceLine { get; set; }
    }
}
