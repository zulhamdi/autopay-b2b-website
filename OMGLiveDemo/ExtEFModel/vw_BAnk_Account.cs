﻿namespace OMGLiveDemo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class vw_Bank_Account
    {
        [NotMapped]
        public string NameBank { get; set; }

        [NotMapped]
        public string Country { get; set; }

        [NotMapped]
        public string CreatedName { get; set; }
    }
}
