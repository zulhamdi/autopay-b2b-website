﻿namespace OMGLiveDemo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class vw_Agent_Rate
    {
        [NotMapped]
        public string RemittanceLine { get; set; }
    }
}
