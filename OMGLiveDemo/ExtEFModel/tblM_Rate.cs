﻿namespace OMGLiveDemo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class tblM_Rate
    {
        [NotMapped]
        public string Rate { get; set; }

        [NotMapped]
        public string NameRate { get; set; }
    }
}
