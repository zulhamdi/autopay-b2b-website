﻿namespace OMGLiveDemo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class XClass
    {
        [NotMapped]
        public string XProperty { get; set; }
    }
}
