﻿namespace OMGLiveDemo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class vw_Payout
    {
        [NotMapped]
        public string AmountText { get; set; }

        [NotMapped]
        public string TotalAmountText { get; set; }

        [NotMapped]
        public string TotalTransferText { get; set; }

        [NotMapped]
        public string RateText { get; set; }

        [NotMapped]
        public string RateGivenText { get; set; }
    }
}
