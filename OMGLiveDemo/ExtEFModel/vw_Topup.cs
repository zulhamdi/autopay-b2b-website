﻿namespace OMGLiveDemo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class vw_Topup
    {
        [NotMapped]
        public string AmountText { get; set; }

        [NotMapped]
        public string Fullname { get; set; }
    }
}
