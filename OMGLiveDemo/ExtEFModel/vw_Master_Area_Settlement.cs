﻿namespace OMGLiveDemo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class vw_Master_Area_Settlement
    {
        [NotMapped]
        public string AmountText { get; set; }

        [NotMapped]
        public string BalancePreviousText { get; set; }

        [NotMapped]
        public string BalanceText { get; set; }

        [NotMapped]
        public string AdminFeeText { get; set; }
    }
}
