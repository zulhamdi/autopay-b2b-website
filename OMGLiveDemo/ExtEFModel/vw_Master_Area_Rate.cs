﻿namespace OMGLiveDemo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class vw_Master_Area_Rate
    {
        [NotMapped]
        public string RemittanceLine { get; set; }
    }
}
