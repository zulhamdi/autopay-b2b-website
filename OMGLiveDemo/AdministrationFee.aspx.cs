﻿using OMGLiveDemo.Models;
using OMGLiveDemo.Scripts;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo
{
    public partial class AdministrationFee : System.Web.UI.Page
    {
        OMG omg = new OMG();
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();
        tblM_Rate rate = new tblM_Rate();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    countMYR();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        void countMYR()
        {
            var adminfee = db.tblM_Admin_Fee.Where(p => p.RemittanceID == 1).FirstOrDefault();

            lblCurrencyMYR.Text = adminfee.Currency;
            txtAdminFeeMYR.Text = adminfee.Amount;
            lblCountryMYR.Text = "Malaysia to Indonesia";
        }

        protected void btnSuccessMYR_ServerClick(object sender, EventArgs e)
        {
            divsuccessMYR.Visible = false;
        }

        protected void btnErrorMYR_ServerClick(object sender, EventArgs e)
        {
            diverrorMYR.Visible = false;
        }

        protected void btnUpdateMYR_Click(object sender, EventArgs e)
        {
            if (txtAdminFeeMYR.Text == "")
            {
                diverrorMYR.Visible = true;
                lblErrorMYR.Text = "Administration fee cannot be empty!";
                return;
            }

            var id = Int64.Parse(SessionLib.Current.AdminID);
            var arate = db.tblM_Admin_Fee.Where(p => p.RemittanceID == 1).FirstOrDefault();
            if (arate != null)
            {
                arate.Amount = txtAdminFeeMYR.Text;
                arate.UpdatedDate = DateTime.Now;
                arate.UpdatedBy = Int64.Parse(SessionLib.Current.AdminID);
                db.SaveChanges();

                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Success update margin become " + String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(txtMarginMYR.Text)).Replace("Rp", "Rp ") + "')", true);

                divsuccessMYR.Visible = true;
                lblSuccessMYR.Text = "Success update administration fee become " + String.Format(new CultureInfo("ms-MY"), "{0:c}", Convert.ToInt64(txtAdminFeeMYR.Text)).Replace("RM", "RM ") + "";
                return;
            }
        }

        
    }
}