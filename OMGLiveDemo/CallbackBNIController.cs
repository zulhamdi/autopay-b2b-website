﻿using OMGLiveDemo.Models.OMG;
using OMGLiveDemo.Scripts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace OMGLiveDemo
{
    public class CallbackBNIController : ApiController
    {
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();
        OMG omg = new OMG();

        [HttpPost]
        public async Task<HttpResponseMessage> BNIVA([FromBody]bniEcollModel pay)
        {
            var response = new HttpResponseMessage();

            try
            {
                if (pay != null)
                {
                    var data = db.tblM_Travel_Agent_VA.Where(p => p.VA == pay.virtual_account).FirstOrDefault();
                    if (data != null)
                    {
                        var balid = db.tblM_Travel_Agent_Balance.Where(p => p.TravelAgentID == data.TravelAgentID).Select(p => p.BalanceID).FirstOrDefault();
                        var bal = db.tblM_Travel_Agent_Balance.Where(p => p.TravelAgentID == data.TravelAgentID && p.BalanceID == balid).FirstOrDefault();
                        tblT_Topup tp = new tblT_Topup();
                        tp.TopupID = "TUP-" + (db.tblT_Topup.Count() + 1).ToString("0000000");
                        db.tblT_Topup.Add(tp);
                        db.SaveChanges();

                        tp.UserID = data.TravelAgentID;
                        tp.UserType = "TA";
                        tp.BankAccountID = 0;
                        tp.BalanceID = balid;
                        tp.Currency = bal.Currency;
                        tp.Amount = Convert.ToDouble(pay.payment_amount);
                        tp.AdminFee = 0;
                        tp.UniqueCode = 0;
                        tp.TotalAmount = Convert.ToDouble(tp.Amount) + tp.AdminFee + tp.UniqueCode;
                        tp.Status = "created";
                        tp.Notes = "va " + pay.customer_name;
                        tp.CreatedBy = 0;
                        tp.CreatedDate = DateTime.Now;
                        tp.isCreatedByAdmin = 0;
                        tp.isActive = 1;
                        tp.ReceiptURL = pay.trx_id;
                        db.SaveChanges();


                        var aaa = db.tblT_Topup.Where(p => p.TopupID == tp.TopupID).FirstOrDefault();
                        aaa.Status = "approved";
                        aaa.ApprovedBy = 0;
                        aaa.ApprovedDate = DateTime.Now;
                        aaa.ModifiedDate = aaa.ApprovedDate;
                        db.SaveChanges();

                        TopupModel tup = new TopupModel();
                        tup.Amount = aaa.Amount.Value;
                        tup.Notes = "Travel agent topup : " + aaa.UserID + " - " + aaa.TopupID;
                        tup.BankAccountID = 0;
                        tup.Currency = tp.Currency;
                        tup.VA = pay.virtual_account;
                        tup.VAName = pay.customer_name;
                        bool asd = omg.createAndApproveTopupMalindo(tup);
                        if (asd)
                        {

                            db.SaveChanges();

                            var balance = db.tblM_Travel_Agent_Balance.Where(p => p.TravelAgentID == data.TravelAgentID && p.BalanceID == balid).FirstOrDefault();
                            var balpref = balance.Balance;
                            balance.Balance += aaa.TotalAmount;
                            balance.UpdatedBy = 0;
                            balance.UpdatedDate = DateTime.Now;
                            db.SaveChanges();

                            var bank = db.tblM_Bank.Where(p => p.Code == "bni").FirstOrDefault();

                            tblH_Travel_Agent_Balance_Update hbal = new tblH_Travel_Agent_Balance_Update();
                            hbal.TravelAgentID = data.TravelAgentID.Value;
                            hbal.BalanceID = balid;
                            hbal.RemittanceID = 0;
                            hbal.Reference = aaa.TopupID;
                            hbal.Type = "Topup";
                            hbal.BankID = bank.BankID;
                            hbal.Beneficiary = data.Name;
                            hbal.Account = pay.virtual_account;
                            hbal.BankName = bank.Name;
                            hbal.BalancePrevious = balpref.Value;
                            hbal.Amount = aaa.Amount.Value;
                            hbal.AdminFee = aaa.AdminFee.Value;
                            hbal.Balance = balance.Balance.Value;
                            hbal.DateCreated = DateTime.Now;
                            db.tblH_Travel_Agent_Balance_Update.Add(hbal);
                            db.SaveChanges();
                        }
                    }
                    response.Content = new StringContent("done");
                    response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Content = new StringContent("no body");
                    response.StatusCode = HttpStatusCode.BadRequest;
                }
            }
            catch (Exception ex)
            {
                response.Content = new StringContent(ex.ToString());
                response.StatusCode = HttpStatusCode.InternalServerError;
            }

            return response;
        }

        public class bniEcollModel
        {
            public string virtual_account { get; set; }
            public string customer_name { get; set; }
            public string trx_id { get; set; }
            public string trx_amount { get; set; }
            public string payment_amount { get; set; }
            public string cumulative_payment_amount { get; set; }
            public string payment_ntb { get; set; }
            public Nullable<DateTime> datetime_payment { get; set; }
            public Nullable<DateTime> datetime_payment_iso8601 { get; set; }
        }
    }
}