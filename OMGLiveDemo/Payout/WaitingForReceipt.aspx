﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WaitingForReceipt.aspx.cs" Inherits="OMGLiveDemo.Payout.WaitingForReceipt" MaintainScrollPositionOnPostback="true" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" />

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph">

                <div class="row x_title">
                    <div class="col-md-12">
                        <h3>Waiting for Receipt</h3>
                    </div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <%--<div class="form-group">
                        <asp:Label runat="server" ID="lblPartner" CssClass="control-label" Text="Partner : " />
                        <asp:DropDownList runat="server" ID="ddlPartner" CssClass="form-control" OnSelectedIndexChanged="ddlPartner_SelectedIndexChanged" AutoPostBack="true" />
                    </div>
                    <br />--%>
                    <div style="width: 100%;">
                        <div id="canvas_dahs" class="demo-placeholder" style="width: 100%; height: 100%;">
                            <div class="alert alert-success alert-dismissible fade in" runat="server" id="divsuccess" visible="false">
                                <button type="button" runat="server" id="btnAlertSuccess" onserverclick="btnAlertSuccess_ServerClick" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <center><asp:Label runat="server" ID="lblAlertSuccess" Font-Size="14pt" Text="test error" /></center>
                            </div>

                            <div class="alert alert-danger alert-dismissible fade in" runat="server" id="divfailed" visible="false">
                                <button type="button" runat="server" id="btnALertFailed" onserverclick="btnALertFailed_ServerClick" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <center><asp:Label runat="server" ID="lblAlertFailed" Font-Size="14pt" Text="test error" /></center>
                            </div>
                            <div style="overflow-x: auto">
                                <asp:GridView CssClass="table table-striped jambo_table bulk_action" ID="gvListItem" runat="server" AutoGenerateColumns="false" DataKeyNames="PayoutID">
                                    <Columns>
                                        <asp:TemplateField HeaderText="No.">
                                            <ItemTemplate>
                                                <asp:HiddenField runat="server" ID="hfPayoutID" Value='<%# Eval("PayoutID") %>' />
                                                <asp:HiddenField runat="server" ID="hfAmount" Value='<%# Eval("Amount") %>' />
                                                <asp:HiddenField runat="server" ID="hfRate" Value='<%# Eval("Rate") %>' />
                                                <asp:HiddenField runat="server" ID="hfTotalAmount" Value='<%# Eval("TotalAmount") %>' />
                                                <%# Container.DataItemIndex + 1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="SenderName" HeaderText="Sender Name" />
                                        <asp:BoundField DataField="BeneficiaryName" HeaderText="Beneficiary Name" />
                                        <asp:BoundField DataField="BeneficiaryAccount" HeaderText="Beneficiary Account" />
                                        <asp:BoundField DataField="RemittanceTo" HeaderText="Transfer To" />
                                        <asp:BoundField DataField="BankName" HeaderText="Beneficiary Bank" />
                                        <asp:BoundField DataField="RateText" HeaderText="Rate" />
                                        <asp:BoundField DataField="AmountText" HeaderText="Amount" />
                                        <asp:BoundField DataField="AdminFee" HeaderText="Administration Fee" />
                                        <asp:BoundField DataField="TotalAmountText" HeaderText="Total Amount" />
                                        <asp:BoundField DataField="TotalTransferText" HeaderText="Total Transfer" />
                                        <asp:BoundField DataField="Notes" HeaderText="Notes" />
                                        <asp:BoundField DataField="Status" HeaderText="Status" />
                                        <asp:BoundField DataField="CreatedDate" HeaderText="Created Date" />
                                        <asp:BoundField DataField="ModifiedDate" HeaderText="Updated Date" />
                                        <asp:TemplateField HeaderText="Action">
                                            <ItemTemplate>
                                                <asp:Button ID="btnUpload" CssClass="form-control btn btn-default" runat="server" Text="Upload Receipt" OnClick="btnReceipt_Click" CausesValidation="false"/>
                                                <asp:Button ID="btnCancelTransfer" CssClass="form-control btn btn-danger" runat="server" Text="Cancel Transfer" OnClick="btnCancelTransfer_Click" CausesValidation="false"/>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                            
                        </div>
                    </div>

                    <div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">

                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                    <h4 class="modal-title" id="myModalLabel">Cancel Transfer</h4>
                                </div>
                                <div class="modal-body">
                                    <h4><asp:Label runat="server" ID="lblCancelPayoutID" /></h4>
                                            <p>Are you sure want to cancel this transfer?</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <asp:Button runat="server" ID="btnCancelPayout" CssClass="btn btn-danger" Text="Yes, Cancel Transfer" OnClick="btnCancelPayout_Click" />
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Scripts" runat="server">
    
    <script type="text/javascript">
        $(window).load(function () {
            if (window.isColorbox) {
                $('#myModal').modal('show');
            }
        });

        function openModalCancel() {
            document.getElementById('myModal').modal('show');
        }

    </script>
</asp:Content>
