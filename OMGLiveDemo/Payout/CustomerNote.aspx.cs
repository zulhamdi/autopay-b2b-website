﻿using OMGLiveDemo.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.Payout
{
    public partial class CustomerNote : System.Web.UI.Page
    {
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    Do();
                }
            }
            else
            {
                if (Master!=null)
                {
                    Master.checkRole();
                }
               
            }
        }

        void Do()
        {
            var payid = Int64.Parse(Session["PayoutID"].ToString());
            
            var dt = db.vw_Payout.Where(p => p.PayoutID == payid).FirstOrDefault();
            lblStatus.Text = dt.Status;
            if (dt.Status != "completed")
            {
                lblStatus.Text = "Diterima";

                lblStatus.ForeColor = System.Drawing.Color.Green;
            }

            //lblAmount.Text = String.Format(new CultureInfo("ms-MY"), "{0:c}", float.Parse(dt.Amount)).Replace("RM", "RM ");
            if (dt.RemittanceID == 1) 
                lblAmount.Text = "IDR " + String.Format(new CultureInfo("id-ID"), "{0:c}", dt.TotalTransfer).Replace("Rp", "");
            else if (dt.RemittanceID == 2)
                lblAmount.Text = "MYR " + String.Format(new CultureInfo("ms-MY"), "{0:c}", dt.TotalTransfer).Replace("RM", "");

            lblBenfAccount.Text = dt.BeneficiaryAccount;
            lblBenfBank.Text = dt.BankName;
            lblBenfName.Text = dt.BeneficiaryName;
            lblSenderName.Text = dt.SenderName;
            lblReference.Text = getReference(dt.PayoutID, dt.RemittanceID.Value, dt.CreatedDate.Value);
            lblTransactionDate.Text = dt.CreatedDate.ToString();
            lblReprintedDate.Text = "Dicetak ulang pada " + DateTime.Now.ToString();
        }

        string getReference(long payoutid, long remittanceid, DateTime txndate)
        {
            string result = "";
            if(remittanceid == 1)
            {
                result = "AP-MY2IND-TXN-" + txndate.ToString("yyyyMMdd") + "-";
            }
            else if(remittanceid == 2)
            {
                result = "AP-IND2MY-TXN-" + txndate.ToString("yyyyMMdd") + "-";
            }

            result += String.Format("{0:00000000}", payoutid);

            return result;
        }
    }
}