﻿using Newtonsoft.Json;
using OMGLiveDemo.Models;
using OMGLiveDemo.Models.OMG;
using OMGLiveDemo.Scripts;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.Payout
{
    public partial class ApproveOrReject : System.Web.UI.Page
    {
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();
        float totalAmount = 0;
        OMG omg = new OMG();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    bind();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        void bind()
        {
            long agid = Convert.ToInt64(SessionLib.Current.AdminID);
            long payid = Convert.ToInt64(Session["PayoutID"].ToString());
            hfPayoutID.Value = Session["PayoutID"].ToString();

            var pay = db.vw_Payout_Admin.Where(p => p.PayoutID == payid && p.Status == "queued").FirstOrDefault();
            if (pay != null)
            {
                txtSenderName.Text = pay.SenderName;
                txtSenderPhone.Text = pay.SenderPhone;
                txtSenderEmail.Text = pay.SenderEmail;
                txtBenfName.Text = pay.BeneficiaryName;
                txtBenfAccount.Text = pay.BeneficiaryAccount;
                txtBenfBank.Text = pay.BankName;
                txtBenfEmail.Text = pay.BeneficiaryEmail;
                txtBenfPhone.Text = pay.BeneficiaryPhone;

                var id = pay.CreatedBy;
                if (pay.isAgent == 1)
                {
                    pay.CreatedName = db.tblM_Agent.Where(p => p.AgentID == id).Select(p => p.Fullname).FirstOrDefault();
                }
                else
                    pay.CreatedName = db.tblM_Admin.Where(p => p.AdminID == id).Select(p => p.Fullname).FirstOrDefault();

                txtAgent.Text = pay.CreatedName;

                if (pay.RemittanceID == 1)
                {
                    lblTotalAmount.Text = "MYR " + String.Format(new CultureInfo("ms-MY"), "{0:n}", pay.TotalAmount).Replace("RM", "RM ");
                    lblTotalTransfer.Text = "IDR " + String.Format(new CultureInfo("id-ID"), "{0:n}", pay.TotalTransfer).Replace("Rp", "Rp ");
                }
                else if (pay.RemittanceID == 2)
                {
                    lblTotalAmount.Text = "IDR " + String.Format(new CultureInfo("id-ID"), "{0:n}", pay.TotalAmount).Replace("Rp", "Rp ");
                    lblTotalTransfer.Text = "MYR " + String.Format(new CultureInfo("ms-MY"), "{0:n}", pay.TotalTransfer).Replace("RM", "RM ");
                }
                
                if (pay.SenderTypeID == 1)
                {
                    divpersonal.Visible = true;
                    txtNationalID.Text = pay.SenderFileID;
                    divcompany.Visible = false;

                    var spd = db.tblM_Sender_Personal_Detail.Where(p => p.SenderID == pay.SenderID).FirstOrDefault();
                    hfSenderIDImageURL.Value = "";
                    hfSenderWithIDImageURL.Value = "";
                    if (spd != null)
                    {
                        hfSenderIDImageURL.Value = spd.IDURL;
                        hfSenderWithIDImageURL.Value = spd.PictureURL;
                    }
                }
                else
                {
                    divpersonal.Visible = false;
                    divcompany.Visible = true;

                    var scd = db.tblM_Sender_Company_Detail.Where(p => p.SenderID == pay.SenderID).FirstOrDefault();

                    if(pay.RemittanceID == 1)
                    {
                        txtSSM.Text = pay.SenderFileID;
                        hfCompanySSMFileURL.Value = pay.SenderFileIDURL;
                    }
                    else
                    {
                        txtCompanyLegalitasNo.Text = scd.LegalitasNo;
                        hfCompanyLegalitasFileURL.Value = scd.LegalitasURL;
                    }
                }
                txtSenderType.Text = pay.SenderType;

                var psd = db.tblT_Payout_Supporting_Document.Where(p => p.PayoutID == pay.PayoutID).FirstOrDefault();
                if(psd != null)
                {
                    divSupportingDocument.Visible = true;

                    var sd = db.tblT_Supporting_Document.Where(p => p.TXNID == psd.SupportingDocumentID).FirstOrDefault();
                    hfSupportingDocumentFileURL.Value = "";
                    if(sd != null)
                    {
                        hfSupportingDocumentFileURL.Value = sd.FileURL;
                    }
                }

                txtNotes.Text = pay.Notes;
                imgURL.ImageUrl = pay.ReceiptURL;
            }
        }

        protected void btnError_ServerClick(object sender, EventArgs e)
        {
            diverror.Visible = false;
        }

        protected void linkProcess_ServerClick(object sender, EventArgs e)
        {

        }

        protected void lkbSSM_Click(object sender, EventArgs e)
        {
            if (hfCompanySSMFileURL.Value != null)
            {
                Session["ImageURL"] = hfCompanySSMFileURL.Value;
                Session["From"] = "Image";
                string txt = "";
                txt = "SSM : " + txtSSM.Text;
                Session["Text"] = txtSenderName.Text + " - " + txt;
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "colorBox", "var isColorbox = true", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('File not found!')", true);
            }
        }

        protected void lkbSenderIDFile_Click(object sender, EventArgs e)
        {
            if (hfSenderIDImageURL.Value != null)
            {
                Session["ImageURL"] = hfSenderIDImageURL.Value;
                Session["From"] = "Image";
                string txt = "";
                txt = "ID No : " + txtNationalID.Text;
                Session["Text"] = txtSenderName.Text + " - " + txt;
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "colorBox", "var isColorbox = true", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('File not found!')", true);
            }
        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            diverror.Visible = false;
            long payid = Convert.ToInt64(Session["PayoutID"].ToString());
            hfPayoutID.Value = hfPayoutID.Value;

            var pay = db.tblT_Payout.Where(p => p.PayoutID == payid && p.Status == "queued").FirstOrDefault();
            if (pay != null)
            {
                PayoutResponse prs = new PayoutResponse();
                //prs = omg.approvePayout(pay.Reference);
                prs = omg.approvePayout(Session["PayoutID"].ToString());

                if (prs != null)
                {
                    pay.Status = prs.Status;
                    pay.ModifiedDate = prs.DateModified;
                    pay.ApprovedBy = Convert.ToInt64(SessionLib.Current.AdminID);
                    pay.ApprovedDate = prs.DateModified;
                    db.SaveChanges();

                    if (prs.BeneficiaryBank == "bri" && prs.Status == "completed")
                    {
                        if (pay.isAgent == 1)
                        {
                            pay.Reference = prs.ReferenceID;

                            var agentid = pay.CreatedBy;
                            double margin = 0, pend = 0;
                            long cid = 0;
                            if (pay.RemittanceID == 1)
                            {
                                margin = (Convert.ToDouble(pay.RateGiven) - Convert.ToDouble(pay.Rate));
                                pend = margin * Convert.ToDouble(pay.Amount);
                                cid = 103;
                            }
                            else if (pay.RemittanceID == 2)
                            {
                                margin = (Convert.ToDouble(pay.Rate) - Convert.ToDouble(pay.RateGiven));
                                pend = margin * Convert.ToDouble(pay.TotalTransfer);
                                cid = 133;
                            }

                            var bal = db.tblM_Agent_Balance.Where(p => p.AgentID == agentid && p.CountryID == cid).FirstOrDefault();
                            if (bal != null)
                            {
                                bal.Balance = bal.Balance + pend;
                                bal.UpdatedDate = DateTime.Now;
                                bal.UpdatedBy = 0;
                                db.SaveChanges();

                                tblH_Agent_Balance_Update upbal = new tblH_Agent_Balance_Update();
                                upbal.BalanceID = bal.BalanceID;
                                upbal.AgentID = agentid;
                                upbal.Margin = margin;
                                upbal.Reference = pay.Reference;
                                upbal.PayoutID = pay.PayoutID.ToString();
                                upbal.Amount = pend;
                                upbal.UpdatedDate = bal.UpdatedDate;
                                upbal.Status = "Transaction";
                                upbal.UpdateBy = 0;
                                db.tblH_Agent_Balance_Update.Add(upbal);
                                db.SaveChanges();

                                omg.sendEmailAgent(pay);
                            }
                        }
                        else
                        {
                            pay.Reference = prs.ReferenceID;
                            db.SaveChanges();
                        }

                        String eventData = "Approving transfer [" + pay.Reference + "]  to [" + pay.BeneficiaryID + "]" + pay.BeneficiaryName + " by [" + SessionLib.Current.AdminID + "]" + SessionLib.Current.Name + ".";

                        tblS_Log eLog = new tblS_Log
                        {
                            EventDate = pay.ModifiedDate.Value,
                            EventBy = Convert.ToInt64(SessionLib.Current.AdminID),
                            EventType = "transaction",
                            EventData = eventData,
                        };

                        db.tblS_Log.Add(eLog);
                        db.SaveChanges();
                    }
                    Session["SuccessTask"] = "Success approve Payout ID " + hfPayoutID.Value + " and the amount will be transfered to the beneficiary immidiately";
                }
            }
            else
            {
                Session["ErrorTask"] = "Payout ID is not found!";
            }

            Response.Redirect("PendingTask");
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            diverror.Visible = false;
            if (txtRejectReason.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Please enter your reason to reject this trnasfer!";
                return;
            }

            long payid = Convert.ToInt64(Session["PayoutID"].ToString());
            hfPayoutID.Value = hfPayoutID.Value;

            var pay = db.tblT_Payout.Where(p => p.PayoutID == payid && p.Status == "queued").FirstOrDefault();
            if (pay != null)
            {
                if (pay.RemittanceID == 1)
                {
                    if (pay.BeneficiaryBank == "bri")
                    {
                        pay.Status = "rejected";
                        pay.ModifiedDate = DateTime.Now;
                        pay.RejectedBy = Convert.ToInt64(SessionLib.Current.AdminID);
                        pay.RejectedDate = pay.ModifiedDate;
                        pay.StatusMessage = txtRejectReason.Text.Trim();
                        db.SaveChanges();

                        String eventData = "Rejecting transfer [" + pay.PayoutID + "] by [" + SessionLib.Current.AdminID + "]" + SessionLib.Current.Name + ".";

                        tblS_Log eLog = new tblS_Log
                        {
                            EventDate = pay.ModifiedDate.Value,
                            EventBy = Convert.ToInt64(SessionLib.Current.AdminID),
                            EventType = "transaction",
                            EventData = eventData,
                        };

                        db.tblS_Log.Add(eLog);
                        db.SaveChanges();


                        Session["SuccessTask"] = "Success reject Payout ID " + hfPayoutID.Value + " with reason : " + txtRejectReason.Text;
                    }
                    else
                    {
                        PayoutResponse prs = new PayoutResponse();
                        prs = omg.rejectPayout("iris", pay.Reference, txtRejectReason.Text.Trim());

                        if (prs != null)
                        {
                            pay.Status = prs.Status;
                            pay.ModifiedDate = prs.DateModified;
                            pay.RejectedBy = Convert.ToInt64(SessionLib.Current.AdminID);
                            pay.RejectedDate = prs.DateModified;
                            pay.StatusMessage = txtRejectReason.Text.Trim();
                            db.SaveChanges();


                            Session["SuccessTask"] = "Success reject Payout ID " + hfPayoutID.Value + " with reason : " + txtRejectReason.Text;
                        }
                    }
                }
                else
                {
                    PayoutResponse prs = new PayoutResponse();
                    prs = omg.rejectPayout("amc", pay.Reference, txtRejectReason.Text.Trim());

                    if (prs != null)
                    {
                        pay.Status = prs.Status;
                        pay.ModifiedDate = prs.DateModified;
                        pay.RejectedBy = Convert.ToInt64(SessionLib.Current.AdminID);
                        pay.RejectedDate = prs.DateModified;
                        pay.StatusMessage = txtRejectReason.Text.Trim();
                        db.SaveChanges();

                        String eventData = "Rejecting transfer [" + pay.Reference + "] to [" + pay.BeneficiaryID + "]" + pay.BeneficiaryName + " by [" + SessionLib.Current.AdminID + "]" + SessionLib.Current.Name + ".";

                        tblS_Log eLog = new tblS_Log
                        {
                            EventDate = pay.ModifiedDate.Value,
                            EventBy = Convert.ToInt64(SessionLib.Current.AdminID),
                            EventType = "transaction",
                            EventData = eventData,
                        };

                        db.tblS_Log.Add(eLog);
                        db.SaveChanges();

                        Session["SuccessTask"] = "Success reject Payout ID " + hfPayoutID.Value + " with reason : " + txtRejectReason.Text;
                    }
                }

                omg.sendEmailAgent(pay);
            }
            else
            {
                Session["ErrorTask"] = "Payout ID is not found!";
            }

            Response.Redirect("PendingTask");
        }

        protected void lkbCompanyLegalitasFile_Click(object sender, EventArgs e)
        {
            if (hfCompanyLegalitasFileURL.Value != null)
            {
                Session["ImageURL"] = hfCompanyLegalitasFileURL.Value;
                Session["From"] = "Image";
                string txt = "";
                txt = "Legalitas No : " + txtCompanyLegalitasNo.Text;
                Session["Text"] = txtSenderName.Text + " - " + txt;
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "colorBox", "var isColorbox = true", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('File not found!')", true);
            }
        }

        protected void lkbSupportingDocument_Click(object sender, EventArgs e)
        {

        }
    }
}