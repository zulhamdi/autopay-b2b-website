﻿using OMGLiveDemo.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.Payout
{
    public partial class WaitingForReceipt : System.Web.UI.Page
    {
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    BindGV();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        void BindGV()
        {
            if (Session["SuccessReceipt"] != null) {
                lblAlertSuccess.Text = Session["SuccessReceipt"].ToString();
                divsuccess.Visible = true;
                Session["SuccessReceipt"] = null;
            }
            if (Session["ErrorReceipt"] != null)
            {
                lblAlertFailed.Text = Session["ErrorReceipt"].ToString();
                divfailed.Visible = true;
                Session["ErrorReceipt"] = null;
            }

            List<vw_Payout_Admin> dt = new List<vw_Payout_Admin>();
            long agid = Convert.ToInt64(SessionLib.Current.AdminID);
            if (SessionLib.Current.Role == Roles.Agent)
                dt = db.vw_Payout_Admin.Where(p => p.isAgent == 1 && p.CreatedBy == agid && p.Status == "created").OrderByDescending(p => p.PayoutID).ToList();
            else
                dt = db.vw_Payout_Admin.Where(p => p.isAgent == 0 && p.Status == "created").OrderByDescending(p => p.PayoutID).ToList();
            for (int i = 0; i < dt.Count; i++)
            {
                if (dt[i].RemittanceID == 1)
                {
                    dt[i].RemittanceTo = "Indonesia";
                    dt[i].AmountText = "MYR " + String.Format(new CultureInfo("ms-MY"), "{0:n}", dt[i].Amount).Replace("MYR", "MYR ");
                    dt[i].AdminFee = "MYR " + String.Format(new CultureInfo("ms-MY"), "{0:n}", float.Parse(dt[i].AdminFee)).Replace("MYR", "MYR ");
                    dt[i].RateText = "IDR " + String.Format(new CultureInfo("id-ID"), "{0:n}", float.Parse(dt[i].Rate)).Replace("IDR", "IDR ");
                    dt[i].TotalAmountText = "MYR " + String.Format(new CultureInfo("ms-MY"), "{0:n}", dt[i].TotalAmount).Replace("MYR", "MYR ");
                    dt[i].TotalTransferText = "IDR " + String.Format(new CultureInfo("id-ID"), "{0:n}", dt[i].TotalTransfer).Replace("IDR", "IDR ");
                    dt[i].Status = "Waiting for Receipt";
                }
                else
                {
                    dt[i].RemittanceTo = "Malaysia";
                    dt[i].AmountText = "IDR " + String.Format(new CultureInfo("id-ID"), "{0:n}", dt[i].Amount).Replace("IDR", "IDR ");
                    dt[i].AdminFee = "IDR " + String.Format(new CultureInfo("id-ID"), "{0:n}", float.Parse(dt[i].AdminFee)).Replace("IDR", "IDR ");
                    dt[i].RateText = "IDR " + String.Format(new CultureInfo("id-ID"), "{0:n}", float.Parse(dt[i].Rate)).Replace("IDR", "IDR ");
                    dt[i].TotalAmountText = "IDR " + String.Format(new CultureInfo("id-ID"), "{0:n}", dt[i].TotalAmount).Replace("IDR", "IDR ");
                    dt[i].TotalTransferText = "MYR " + String.Format(new CultureInfo("ms-MY"), "{0:n}", dt[i].TotalTransfer).Replace("MYR", "MYR ");
                    dt[i].Status = "Waiting for Receipt";
                }
            }
            //dt.Tables[0].Columns.Add("POText", typeof(String));

            //for (int i = 0; i < dt.Tables[0].Rows.Count; i++)
            //{
            //    if (dt.Tables[0].Rows[i]["isPO"].ToString() == "0")
            //    {
            //        dt.Tables[0].Rows[i]["POText"] = "No";
            //    }
            //    else
            //    {
            //        dt.Tables[0].Rows[i]["POText"] = "Yes";
            //    }
            //}
            //dt.AcceptChanges();
            gvListItem.DataSource = dt;
            gvListItem.DataBind();
        }

        protected void btnReceipt_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            Session["PayoutID"] = ((HiddenField)row.FindControl("hfPayoutID")).Value;
            Response.Redirect("UploadReceipt");
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "OpenWindow", "window.open('/Payout/Receipt','_newtab');", true);
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {

        }

        protected void btnAlertSuccess_ServerClick(object sender, EventArgs e)
        {
            divsuccess.Visible = false;
        }

        protected void btnALertFailed_ServerClick(object sender, EventArgs e)
        {
            divfailed.Visible = false;
        }

        protected void btnCancelTransfer_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            Session["PayoutID"] = ((HiddenField)row.FindControl("hfPayoutID")).Value;
            lblCancelPayoutID.Text = "Payout ID : " + Session["PayoutID"].ToString();
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "Pop", "var isColorbox = true", true);
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "Pop", "openModalCancel();", true);
            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", true);
            //upModal.Update();
        }

        protected void btnCancelPayout_Click(object sender, EventArgs e)
        {
            var payid = Convert.ToInt64(Session["PayoutID"].ToString());
            var pay = db.tblT_Payout.Where(p => p.PayoutID == payid).FirstOrDefault();
            if (pay.Status == "created") {
                pay.Status = "cancelled";
            }
            db.SaveChanges();

            Session["PayoutID"] = null;
            Session["SuccessReceipt"] = "Success cancel Payout ID : " + payid.ToString();

            Server.TransferRequest(Request.Url.AbsolutePath, false);
        }
    }
}