﻿using OMGLiveDemo.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.Payout
{
    public partial class PendingTask : System.Web.UI.Page
    {
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    BindGV();
                }

                Response.Expires = 0;
                Response.Buffer = true;
                Response.Clear();
                Response.AddHeader("Refresh", "60");
            }
            else
            {
                Master.checkRole();
            }
        }

        void BindGV()
        {
            if (Session["SuccessTask"] != null)
            {
                lblAlertSuccess.Text = Session["SuccessTask"].ToString();
                divsuccess.Visible = true;
                Session["SuccessTask"] = null;
            }
            if (Session["ErrorTask"] != null)
            {
                lblAlertFailed.Text = Session["ErrorTask"].ToString();
                divfailed.Visible = true;
                Session["ErrorTask"] = null;
            }

            var dt = db.tblT_Payout.Where(p => p.Status == "queued").ToList();
            for (int i = 0; i < dt.Count; i++)
            {
                var id = dt[i].CreatedBy;
                if (dt[i].isAgent == 1)
                    dt[i].CreatedName = db.tblM_Agent.Where(p => p.AgentID == id).Select(p => p.Fullname).FirstOrDefault();
                else
                    dt[i].CreatedName = db.tblM_Admin.Where(p => p.AdminID == id).Select(p => p.Fullname).FirstOrDefault();
                if (dt[i].RemittanceID == 1)
                {
                    dt[i].AmountText = "MYR " + String.Format(new CultureInfo("ms-MY"), "{0:n}", dt[i].Amount.Value).Replace("RM", "RM ");
                    dt[i].AdminFee = "MYR " + String.Format(new CultureInfo("ms-MY"), "{0:n}", float.Parse(dt[i].AdminFee)).Replace("RM", "RM ");
                    dt[i].RateGivenText = "IDR " + String.Format(new CultureInfo("id-ID"), "{0:n}", float.Parse(dt[i].RateGiven)).Replace("Rp", "Rp ");
                    dt[i].RateText = "IDR " + String.Format(new CultureInfo("id-ID"), "{0:n}", float.Parse(dt[i].Rate)).Replace("Rp", "Rp ");
                    dt[i].TotalAmountText = "MYR " + String.Format(new CultureInfo("ms-MY"), "{0:n}", dt[i].TotalAmount).Replace("RM", "RM ");
                    dt[i].TotalTransferText = "IDR " + String.Format(new CultureInfo("id-ID"), "{0:n}", dt[i].TotalTransfer).Replace("Rp", "Rp ");
                }
                else if (dt[i].RemittanceID == 2)
                {
                    dt[i].AmountText = "IDR " + String.Format(new CultureInfo("id-ID"), "{0:n}", dt[i].Amount.Value).Replace("Rp", "Rp ");
                    dt[i].AdminFee = "IDR " + String.Format(new CultureInfo("id-ID"), "{0:n}", float.Parse(dt[i].AdminFee)).Replace("Rp", "Rp ");
                    dt[i].RateGivenText = "IDR " + String.Format(new CultureInfo("id-ID"), "{0:n}", float.Parse(dt[i].RateGiven)).Replace("RM", "RM ");
                    dt[i].RateText = "IDR " + String.Format(new CultureInfo("id-ID"), "{0:n}", float.Parse(dt[i].Rate)).Replace("RM", "RM ");
                    dt[i].TotalAmountText = "IDR " + String.Format(new CultureInfo("id-ID"), "{0:n}", dt[i].TotalAmount).Replace("Rp", "Rp ");
                    dt[i].TotalTransferText = "MYR " + String.Format(new CultureInfo("ms-MY"), "{0:n}", dt[i].TotalTransfer).Replace("RM", "RM ");
                }
            }
            gvListItem.DataSource = dt;
            gvListItem.DataBind();
        }


        protected void btnReceipt_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            Session["PayoutID"] = ((HiddenField)row.FindControl("hfPayoutID")).Value;
            //Response.Redirect("EditCurrencyRate");
            Page.ClientScript.RegisterStartupScript(this.GetType(), "OpenWindow", "window.open('/Payout/Receipt','_newtab');", true);
        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            var refs = ((HiddenField)row.FindControl("hfReference")).Value;
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            var refs = ((HiddenField)row.FindControl("hfReference")).Value;
        }

        protected void btnAction_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            var refs = ((HiddenField)row.FindControl("hfPayoutID")).Value;

            Session["PayoutID"] = refs;


            if (SessionLib.Current.Role != Roles.Approver)
            {
                //string url = "CustomerNote.aspx";
                //Response.Write("<script type='text/javascript'>window.open('" + url + "');</script>");
                //Response.Redirect("CustomerNote");

                string queryString = "CustomerNote.aspx";
                string newWin = "window.open('" + queryString + "','Customer Transaction Note','width=900, height=800, titlebar=yes');";
                ClientScript.RegisterStartupScript(this.GetType(), "pop", newWin, true);
            }
            else
            {
                Response.Redirect("ApproveOrReject");
            }
        }

        protected void btnPrintCustomerNote_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            var refs = ((HiddenField)row.FindControl("hfPayoutID")).Value;

            Session["PayoutID"] = refs;
            Response.Redirect("CustomerNote");
        }

        protected void btnAlertSuccess_ServerClick(object sender, EventArgs e)
        {
            divsuccess.Visible = false;
        }

        protected void btnALertFailed_ServerClick(object sender, EventArgs e)
        {
            divfailed.Visible = false;
        }
    }
}