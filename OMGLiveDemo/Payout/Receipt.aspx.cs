﻿using OMGLiveDemo.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.Payout
{
    public partial class Receipt : System.Web.UI.Page
    {
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    Do();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        void Do()
        {
            var payid = Int64.Parse(Session["PayoutID"].ToString());

            var dt = db.tblT_Payout.Where(p => p.PayoutID == payid).FirstOrDefault();
            lblStatus.Text = dt.Status;
            if (dt.Status == "completed")
            {
                lblStatus.Text = "Berhasil";
                lblStatus.ForeColor = System.Drawing.Color.Green;
            }
            else if (dt.Status == "failed")
                lblStatus.ForeColor = System.Drawing.Color.Red;
                

            //lblAmount.Text = String.Format(new CultureInfo("ms-MY"), "{0:c}", float.Parse(dt.Amount)).Replace("RM", "RM ");
            if (dt.RemittanceID == 1) 
                lblAmount.Text = "IDR " + String.Format(new CultureInfo("id-ID"), "{0:c}", dt.TotalTransfer).Replace("Rp", "");
            else if (dt.RemittanceID == 2)
                lblAmount.Text = "MYR " + String.Format(new CultureInfo("ms-MY"), "{0:c}", dt.TotalTransfer).Replace("RM", "");

            lblBenfAccount.Text = dt.BeneficiaryAccount;
            lblBenfBank.Text = dt.BankName;
            lblBenfName.Text = dt.BeneficiaryName;
            lblReference.Text = dt.Reference;
            lblTransactionDate.Text = dt.CreatedDate.ToString();
            lblReprintedDate.Text = "Dicetak ulang pada " + DateTime.Now.ToString();
        }
    }
}