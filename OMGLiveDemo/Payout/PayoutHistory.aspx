﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PayoutHistory.aspx.cs" Inherits="OMGLiveDemo.Payout.PayoutHistory" MaintainScrollPositionOnPostback="true" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" />

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph">

                <div class="row x_title">
                    <div class="col-md-12">
                        <h3>Transfer History</h3>
                    </div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <%--<div class="form-group">
                        <asp:Label runat="server" ID="lblPartner" CssClass="control-label" Text="Partner : " />
                        <asp:DropDownList runat="server" ID="ddlPartner" CssClass="form-control" OnSelectedIndexChanged="ddlPartner_SelectedIndexChanged" AutoPostBack="true" />
                    </div>
                    <br />--%>
                    <div style="width: 100%;">
                        <div id="canvas_dahs" class="demo-placeholder" style="width: 100%; height: 100%;">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="alert alert-success alert-dismissible fade in" runat="server" id="divsuccess" visible="false">
                                    <button type="button" runat="server" id="btnAlertSuccess" onserverclick="btnAlertSuccess_ServerClick" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    <div class="centered"><asp:Label runat="server" ID="lblAlertSuccess" Font-Size="14pt" Text="test error" /></div>
                                </div>

                                <div class="alert alert-danger alert-dismissible fade in" runat="server" id="divfailed" visible="false">
                                    <button type="button" runat="server" id="btnALertFailed" onserverclick="btnALertFailed_ServerClick" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    <div class="centered"><asp:Label runat="server" ID="lblAlertFailed" Font-Size="14pt" Text="test error" /></div>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12" style="background-color: #f8f8f8; margin-bottom: 20px; padding-top: 10px">
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <asp:Label runat="server" CssClass="control-label" Text="Remittance Line" />
                                            <asp:DropDownList runat="server" ID="ddlRemittanceLine" CssClass="form-control" OnSelectedIndexChanged="ddlRemittanceLine_SelectedIndexChanged" AutoPostBack="true">
                                                <asp:ListItem Text="Malaysia to Indonesia" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Indonesia to Malaysia" Value="2" Selected="True"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="form-group" style="height: 5px"></div>

                                        <div class="form-group" runat="server" id="divstatus" visible="false">
                                            <asp:Label runat="server" ID="lblStatus" CssClass="control-label" Text="Status : " />
                                            <div style="margin-top: 5px">
                                                <asp:DropDownList runat="server" ID="ddlStatus" CssClass="form-control" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged" AutoPostBack="true" CausesValidation="true">
                                                    <asp:ListItem Enabled="true" Text="All" Value="all"></asp:ListItem>
                                                    <asp:ListItem Text="Pending" Value="pending"></asp:ListItem>
                                                    <asp:ListItem Text="Processed" Value="processed"></asp:ListItem>
                                                    <asp:ListItem Text="Completed" Value="completed"></asp:ListItem>
                                                    <asp:ListItem Text="Cancelled" Value="cancelled"></asp:ListItem>
                                                    <asp:ListItem Text="Rejected" Value="rejected"></asp:ListItem>
                                                    <asp:ListItem Text="Failed" Value="failed"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="form-group" style="height: 5px"></div>
                                        </div>

                                        <div class="form-group" runat="server" id="divSearchSender">
                                            <asp:Label runat="server" ID="Label6" CssClass="control-label" Text="Sender : " />
                                            <div style="margin-top: 5px">
                                                <asp:TextBox runat="server" CssClass="form-control" ID="txtSender" OnTextChanged="TextChanged" AutoPostBack="true"></asp:TextBox>
                                            </div>
                                            <div class="form-group" style="height: 5px"></div>
                                        </div>

                                        <div class="form-group" runat="server" id="divSearchBeneficiary">
                                            <asp:Label runat="server" ID="Label7" CssClass="control-label" Text="Beneficiary : " />
                                            <div style="margin-top: 5px">
                                                <asp:TextBox runat="server" CssClass="form-control" ID="txtBene" OnTextChanged="TextChanged" AutoPostBack="true"></asp:TextBox>
                                            </div>
                                            <div class="form-group" style="height: 5px"></div>
                                        </div>

                                        <div class="form-group" runat="server" id="divfilterbydate" visible="false">
                                            <asp:Label runat="server" ID="Label1" CssClass="control-label" Text="Filter by : " />
                                            <div style="margin-top: 5px">
                                                <asp:DropDownList runat="server" ID="ddlFilterByDate" CssClass="form-control" OnSelectedIndexChanged="ddlFilterByDate_SelectedIndexChanged" AutoPostBack="true" CausesValidation="true">
                                                    <asp:ListItem Enabled="true" Text="All" Value="all"></asp:ListItem>
                                                    <asp:ListItem Text="Date" Value="date"></asp:ListItem>
                                                    <asp:ListItem Text="Date Range" Value="daterange"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="form-group" style="height: 5px"></div>
                                        </div>

                                        <div class="form-group" runat="server" id="divsingledate" visible="false">
                                            <asp:Label runat="server" ID="Label2" CssClass="control-label" Text="Choose Date : " />
                                            <div style="margin-top: 5px">
                                                <asp:TextBox ID="datepurchased" CssClass="form-control" runat="server" />
                                                <div style="height: 2px"></div>
                                                <asp:Button runat="server" ID="btnSingleDate" Text="Check" CssClass="btn btn-success btn-sm" OnClick="btnSingleDate_Click" />
                                            </div>
                                            <div class="form-group" style="height: 5px"></div>
                                        </div>

                                        <div class="form-group" runat="server" id="divdaterange" visible="false">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="col-md-6 col-sm-12 col-xs-12">
                                                    <asp:Label runat="server" ID="Label4" CssClass="control-label" Text="Start Date : " />
                                                    <div style="margin-top: 5px">
                                                        <asp:TextBox ID="startdate" CssClass="form-control" runat="server" />
                                                        <div style="height: 2px"></div>
                                                        <asp:Button runat="server" ID="btnDateRange" Text="Check" CssClass="btn btn-success btn-sm" OnClick="btnDateRange_Click" />
                                                    </div>
                                                    <div class="form-group" style="height: 5px"></div>
                                                </div>
                                                <div class="col-md-6 col-sm-12 col-xs-12">
                                                    <asp:Label runat="server" ID="Label5" CssClass="control-label" Text="End Date : " />
                                                    <div style="margin-top: 5px">
                                                        <asp:TextBox ID="enddate" CssClass="form-control" runat="server" />
                                                    </div>
                                                    <div class="form-group" style="height: 5px"></div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <asp:Label runat="server" ID="lblTotal" CssClass="control-label" Text="Total Amount" Width="100%" Style="text-align: right;" />
                                            <br />
                                            <asp:Label runat="server" ID="lblTotalAmount" Font-Bold="true" Font-Size="X-Large" ForeColor="Red" Width="100%" Style="text-align: right;" />
                                        </div>

                                        <div class="form-group">
                                            <asp:Label runat="server" ID="Label3" CssClass="control-label" Text="Total Transfer" Width="100%" Style="text-align: right;" />
                                            <br />
                                            <asp:Label runat="server" ID="lblTotalTransfer" Font-Bold="true" Font-Size="X-Large" ForeColor="Red" Width="100%" Style="text-align: right;" />
                                        </div>
                                        <div class="form-group" style="height: 5px"></div>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-12 col-sm-12 col-xs-12" style="overflow-x: auto">
                                <div style="overflow-x:auto">
                                    <asp:GridView CssClass="table table-striped jambo_table bulk_action" ID="gvListItem" runat="server" AutoGenerateColumns="false" DataKeyNames="PayoutID" AllowPaging="True" PageSize="20" AllowCustomPaging="False" OnPageIndexChanging="gvListItem_PageIndexChanging">
                                        <PagerStyle HorizontalAlign="Center" CssClass="bs4-aspnet-pager" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="No.">
                                                <ItemTemplate>
                                                    <asp:HiddenField runat="server" ID="hfPayoutID" Value='<%# Eval("PayoutID") %>' />
                                                    <%--<asp:HiddenField runat="server" ID="hfReference" Value='<%# Eval("Reference") %>' />--%>
                                                    <asp:HiddenField runat="server" ID="hfAmount" Value='<%# Eval("Amount") %>' />
                                                    <asp:HiddenField runat="server" ID="hfRate" Value='<%# Eval("Rate") %>' />
                                                    <asp:HiddenField runat="server" ID="hfTotalAmount" Value='<%# Eval("TotalAmount") %>' />
                                                    <%# Container.DataItemIndex + 1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="SenderName" HeaderText="Sender Name" />
                                            <asp:BoundField DataField="BeneficiaryName" HeaderText="Beneficiary Name" />
                                            <asp:BoundField DataField="BeneficiaryAccount" HeaderText="Beneficiary Account" />
                                            <asp:BoundField DataField="BankName" HeaderText="Beneficiary Bank" />
                                            <asp:BoundField DataField="RateText" HeaderText="Rate" />
                                            <asp:BoundField DataField="AmountText" HeaderText="Amount" />
                                            <asp:BoundField DataField="AdminFee" HeaderText="Administration Fee" />
                                            <asp:BoundField DataField="TotalAmountText" HeaderText="Total Amount" />
                                            <asp:BoundField DataField="TotalTransferText" HeaderText="Total Transfer" />
                                            <asp:BoundField DataField="Notes" HeaderText="Notes" />
                                            <asp:BoundField DataField="Status" HeaderText="Status" />
                                            <asp:BoundField DataField="StatusMessage" HeaderText="Status Message" />
                                            <asp:BoundField DataField="CreatedDate" HeaderText="Created Date" />
                                            <asp:BoundField DataField="ModifiedDate" HeaderText="Updated Date" />
                                            <asp:TemplateField HeaderText="Receipt">
                                                <ItemTemplate>
                                                    <asp:Button ID="btnReceipt" CssClass="form-control" Visible='<%# IsCompleted((string)Eval("Status")) %>' runat="server" Text="Print Receipt" OnClick="btnReceipt_Click" CausesValidation="false" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                                
                                <asp:Button runat="server" ID="btnDownload" OnClick="btnDownload_Click" Text="Download Report" CssClass="btn btn-success" />
                            </div>
                            <div class="form-group" style="height: 30px"></div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Scripts" runat="server">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" />
    <script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript">
        // When the document is ready
        $(document).ready(function () {

            $("input[id*='datepurchased']").datepicker({
                format: "yyyy/mm/dd"
            });
            $("input[id*='startdate']").datepicker({
                format: "yyyy/mm/dd"
            });
            $("input[id*='enddate']").datepicker({
                format: "yyyy/mm/dd"
            });

        });
    </script>

    <style type="text/css">
        
    </style>
</asp:Content>
