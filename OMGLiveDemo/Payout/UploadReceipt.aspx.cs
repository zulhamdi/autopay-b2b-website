﻿using Newtonsoft.Json;
using OMGLiveDemo.Models;
using OMGLiveDemo.Models.OMG;
using OMGLiveDemo.Scripts;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.Payout
{
    public partial class UploadReceipt : System.Web.UI.Page
    {
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();
        float totalAmount = 0;
        OMG omg = new OMG();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    bind();
                    ShowCustomerNote();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        void ShowCustomerNote()
        {
            if (Session["ShowCustomerNote"] != null)
            {
                if(Session["ShowCustomerNote"].ToString() == "yes")
                {
                    string queryString = "CustomerNote.aspx";
                    string newWin = "window.open('" + queryString + "','Nota Transaksi','width=900, height=800, titlebar=yes');";
                    ClientScript.RegisterStartupScript(this.GetType(), "pop", newWin, true);
                    Session.Remove("ShowCustomerNote");
                }
            }
        }

        void bind() {
            long agid = Convert.ToInt64(SessionLib.Current.AdminID);
            long payid = Convert.ToInt64(Session["PayoutID"].ToString());
            hfPayoutID.Value = Session["PayoutID"].ToString();

            var pay = db.vw_Payout_Admin.Where(p => p.PayoutID == payid && p.Status == "created").FirstOrDefault();
            if (pay != null) {
                
                hfSenderName.Value = pay.SenderName;
                hfSenderFileID.Value = pay.SenderFileID;
                Session["SenderType"] = pay.SenderType;
                hfSenderImageURL.Value = pay.SenderFileIDURL;
                Session["ImageURL"] = hfSenderImageURL.Value;
                if (pay.RemittanceID == 1)
                {
                    lblAmount.Text = "Amount (in MYR)";
                    lblAmountX.Text = "RM " + String.Format(new CultureInfo("ms-MY"), "{0:n}", pay.Amount).Replace("RM", "RM ");
                    lblTotalAmount.Text = "RM " + String.Format(new CultureInfo("ms-MY"), "{0:n}", pay.TotalAmount).Replace("RM", "RM ");
                    lblTotalTransfer.Text = "Rp " + String.Format(new CultureInfo("id-ID"), "{0:n}", pay.TotalTransfer).Replace("Rp", "Rp ");
                }
                else if (pay.RemittanceID == 2)
                {
                    lblAmount.Text = "Amount (in IDR)";
                    lblAmountX.Text = "Rp " + String.Format(new CultureInfo("id-ID"), "{0:n}", pay.Amount).Replace("Rp", "Rp ");
                    lblTotalAmount.Text = "Rp " + String.Format(new CultureInfo("id-ID"), "{0:n}", pay.TotalAmount).Replace("Rp", "Rp ");
                    lblTotalTransfer.Text = "RM " + String.Format(new CultureInfo("ms-MY"), "{0:n}", pay.TotalTransfer).Replace("RM", "RM ");
                }
                lblNotesX.Text = pay.Notes;

                txtBankName.Text = pay.TransferToBankName;
                txtAccountHolder.Text = pay.AccountHolder;
                txtAccountNo.Text = pay.Account;
            }
        }

        protected void btnError_ServerClick(object sender, EventArgs e)
        {
            diverror.Visible = false;
        }

        protected void linkProcess_ServerClick(object sender, EventArgs e)
        {
            
        }

        protected void lkbViewImage_Click(object sender, EventArgs e)
        {
            if (hfSenderImageURL.Value != null)
            {
                Session["ImageURL"] = hfSenderImageURL.Value;
                Session["From"] = "Image";
                string txt = "";
                
                if (Session["SenderType"].ToString() == "Personal")
                {
                    txt = "NationalID/Passport : " + hfSenderFileID.Value;
                }
                else
                {
                    txt = "SSM : " + hfSenderFileID.Value;
                }
                Session["Text"] = hfSenderName.Value + " - " + txt;
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "colorBox", "var isColorbox = true", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('File not found!')", true);
            }
        }

        protected void btnProcess_Click(object sender, EventArgs e)
        {
            if (!fuReceipt.HasFile)
            {
                lblError.Text = "Please choose photo of your receipt first";
                diverror.Visible = true;
                return;
            }
            else
            {
                if (fuReceipt.PostedFile.ContentType != "image/jpg" && fuReceipt.PostedFile.ContentType != "image/jpeg" && fuReceipt.PostedFile.ContentType != "image/png")
                {
                    lblError.Text = "Only jpeg, jpg and png format are allowed";
                    diverror.Visible = true;
                    return;
                }
            }

            long payid = Convert.ToInt64(Session["PayoutID"].ToString());
            hfPayoutID.Value = hfPayoutID.Value;

            var pay = db.tblT_Payout.Where(p => p.PayoutID == payid && p.Status == "created").FirstOrDefault();
            if (pay != null)
            {
                if (pay.ReceiptURL == null || pay.ReceiptURL == "")
                {
                    string filename = hfPayoutID.Value + "_Receipt_" + Common.getTimeSpan() + "." + fuReceipt.PostedFile.ContentType.Replace("image/", "");
                    string targetPath = Server.MapPath("../Payout/Picture/Receipt/" + filename);
                    Stream strm = fuReceipt.PostedFile.InputStream;
                    Common.SaveImage(0.5, strm, targetPath);

                    pay.ReceiptURL = ConfigurationManager.AppSettings["URL_RECEIPT"].ToString() + filename;

                    db.SaveChanges();

                    PayoutRequest pr = new PayoutRequest();
                    pr.Currency = "MYR";
                    pr.RemittanceID = pay.RemittanceID.Value;
                    pr.AmountIDR = Convert.ToString(pay.TotalTransfer);
                    pr.Amount = pay.Amount.Value.ToString().Replace(",", ".");
                    pr.BeneficiaryName = pay.BeneficiaryName;
                    pr.BeneficiaryBank = pay.BeneficiaryBank;
                    pr.BeneficiaryAccount = pay.BeneficiaryAccount;
                    pr.BeneficiaryEmail = pay.BeneficiaryEmail;
                    pr.PartnerReferenceID = pay.PayoutID.ToString();
                    pr.Notes = pay.Notes;

                    var payben = db.tblT_Payout_Beneficiary.Where(p => p.PayoutID == payid).FirstOrDefault();
                    Beneficiary ben = new Beneficiary();
                    ben.Name = payben.BeneficiaryName;
                    ben.Address = payben.BeneficiaryAddress;
                    ben.Email = payben.BeneficiaryEmail;
                    ben.Phone = payben.BeneficiaryPhone;
                    ben.Identity = pr.BeneficiaryAccount;
                    pr.Beneficiary = ben;

                    var paysen = db.tblT_Payout_Sender.Where(p => p.PayoutID == payid).FirstOrDefault();
                    Models.OMG.Sender sen = new Models.OMG.Sender();
                    sen.Name = paysen.SenderName;
                    sen.Address = paysen.SenderAddress;
                    sen.Email = paysen.SenderEmail;
                    sen.Phone = paysen.SenderPhone;
                    sen.Identity = paysen.SenderFileID;
                    pr.Sender = sen;

                    var outp = omg.createPayout(pr);

                    if (outp.status == "success")
                    {
                        PayoutResponse prs = new PayoutResponse();
                        prs = JsonConvert.DeserializeObject<PayoutResponse>(outp.data.ToString());
                        if (prs != null)
                        {
                            pay.Reference = prs.ReferenceID;
                            pay.Status = prs.Status;
                            pay.ModifiedDate = prs.DateModified;
                            db.SaveChanges();

                            var snd = db.tblT_Payout_Sender.Where(p => p.PayoutID == pay.PayoutID).FirstOrDefault();
                            snd.Reference = pay.Reference;

                            var bnf = db.tblT_Payout_Beneficiary.Where(p => p.PayoutID == pay.PayoutID).FirstOrDefault();
                            bnf.Reference = pay.Reference;

                            db.SaveChanges();

                            var admin = db.tblM_Admin.Where(p => (p.RoleID == 1 || p.RoleID == 2 || p.RoleID == 3) && p.Email != null && p.Email != "" && p.isActive == 1).ToList();
                            omg.sendEmailApproval(admin, hfSenderName.Value, lblTotalAmount.Text);

                            String eventData = "Receipt upload for transfer [" + pay.Reference + "] to [" + bnf.PayoutBeneficiaryID + "]" + bnf.BeneficiaryName + " by [" + SessionLib.Current.AdminID + "]" + SessionLib.Current.Name + ".";

                            tblS_Log eLog = new tblS_Log
                            {
                                EventDate = pay.ModifiedDate.Value,
                                EventBy = Convert.ToInt64(SessionLib.Current.AdminID),
                                EventType = "transaction",
                                EventData = eventData,
                            };

                            db.tblS_Log.Add(eLog);
                            db.SaveChanges();

                            Session["SuccessReceipt"] = "Receipt of Payout ID : " + hfPayoutID.Value + " has been uploaded and need admin approval before send it to beneficiary";
                        }
                    }
                    else
                    {
                        lblError.Text = outp.message;
                        diverror.Visible = true;

                        dvBtnProcess.Attributes.Remove("display");
                        dvProcessing.Attributes.Add("style", "display:none");

                        pay.ReceiptURL = null;
                        db.SaveChanges();

                        return;
                    }
                }
                else
                {
                    Session["ErrorReceipt"] = "Receipt of Payout ID : " + hfPayoutID.Value + " is already uploaded";
                }
            }
            else
            {
                Session["ErrorReceipt"] = "Payout ID is not found!";
            }

            Response.Redirect("WaitingForReceipt");
        }

        protected void btnViewCustomerNote_Click(object sender, EventArgs e)
        {
            //string url = "CustomerNote.aspx";
            //Response.Write("<script type='text/javascript'>window.open('" + url + "');</script>");
            //Response.Redirect("CustomerNote");

            string queryString = "CustomerNote.aspx";
            string newWin = "window.open('" + queryString + "','Nota Transaksi','width=900, height=800, titlebar=yes');";
            ClientScript.RegisterStartupScript(this.GetType(), "pop", newWin, true);
        }
    }
}