﻿using OMGLiveDemo.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.Payout
{
    public partial class WaitingApproval : System.Web.UI.Page
    {
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    BindGV();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        void BindGV()
        {
            var dt = db.tblT_Payout.Where(p => p.Status == "queue").ToList();
            for (int i = 0; i < dt.Count; i++)
            {
                var id = dt[i].CreatedBy;
                if (dt[i].isAgent == 1)
                    dt[i].CreatedName = db.tblM_Agent.Where(p => p.AgentID == id).Select(p => p.Fullname).FirstOrDefault();
                else
                    dt[i].CreatedName = db.tblM_Admin.Where(p => p.AdminID == id).Select(p => p.Fullname).FirstOrDefault();
                if (dt[i].RemittanceID == 1)
                {
                    dt[i].AmountText = "RM " + String.Format(new CultureInfo("ms-MY"), "{0:n}", dt[i].Amount.Value).Replace("RM", "RM ");
                    dt[i].AdminFee = "RM " + dt[i].AdminFee;
                    dt[i].RateGivenText = "Rp " + String.Format(new CultureInfo("id-ID"), "{0:n}", float.Parse(dt[i].RateGiven)).Replace("Rp", "Rp ");
                    dt[i].RateText = "Rp " + String.Format(new CultureInfo("id-ID"), "{0:n}", float.Parse(dt[i].Rate)).Replace("Rp", "Rp ");
                    dt[i].TotalAmountText = "RM " + String.Format(new CultureInfo("ms-MY"), "{0:n}", dt[i].TotalAmount).Replace("RM", "RM ");
                    dt[i].TotalTransferText = "Rp " + String.Format(new CultureInfo("id-ID"), "{0:n}", dt[i].TotalTransfer).Replace("Rp", "Rp ");
                }
            }
            gvListItem.DataSource = dt;
            gvListItem.DataBind();
        }

        protected void btnReceipt_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            Session["PayoutID"] = ((HiddenField)row.FindControl("hfPayoutID")).Value;
            Page.ClientScript.RegisterStartupScript(this.GetType(), "OpenWindow", "window.open('/Payout/Receipt','_newtab');", true);
        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            var id = ((HiddenField)row.FindControl("hfPayoutID")).Value;
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            var id = ((HiddenField)row.FindControl("hfPayoutID")).Value;
        }
    }
}