﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.Payout
{
    public partial class ViewImage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            imgURL.ImageUrl = Session["ImageURL"].ToString();
            lblTraID.Text = Session["Text"].ToString();
        }
    }
}