﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WaitingApproval.aspx.cs" Inherits="OMGLiveDemo.Payout.WaitingApproval" MaintainScrollPositionOnPostback="true" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" />

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph">

                <div class="row x_title">
                    <div class="col-md-12">
                        <h3>WaitingApproval</h3>
                    </div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <%--<div class="form-group">
                        <asp:Label runat="server" ID="lblPartner" CssClass="control-label" Text="Partner : " />
                        <asp:DropDownList runat="server" ID="ddlPartner" CssClass="form-control" OnSelectedIndexChanged="ddlPartner_SelectedIndexChanged" AutoPostBack="true" />
                    </div>
                    <br />--%>
                    <div style="width: 100%;">
                        <div id="canvas_dahs" class="demo-placeholder" style="width: 100%; height: 100%;">
                            <asp:GridView CssClass="table table-striped jambo_table bulk_action" ID="gvListItem" runat="server" AutoGenerateColumns="false" DataKeyNames="PayoutID">
                                <Columns>
                                    <asp:TemplateField HeaderText="No.">
                                        <ItemTemplate>
                                            <asp:HiddenField runat="server" ID="hfPayoutID" Value='<%# Eval("PayoutID") %>' />
                                            <asp:HiddenField runat="server" ID="hfReference" Value='<%# Eval("Reference") %>' />
                                            <asp:HiddenField runat="server" ID="hfAmount" Value='<%# Eval("Amount") %>' />
                                            <asp:HiddenField runat="server" ID="hfRate" Value='<%# Eval("Rate") %>' />
                                            <asp:HiddenField runat="server" ID="hfTotalAmount" Value='<%# Eval("TotalAmount") %>' />
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="CreatedName" HeaderText="Agent/Admin" />
                                    <asp:BoundField DataField="BeneficiaryName" HeaderText="Beneficiary Name" />
                                    <asp:BoundField DataField="BeneficiaryAccount" HeaderText="Beneficiary Account" />
                                    <asp:BoundField DataField="BankName" HeaderText="Beneficiary Bank" />
                                    <asp:BoundField DataField="RateText" HeaderText="Rate" />
                                    <asp:BoundField DataField="AmountText" HeaderText="Amount" />
                                    <asp:BoundField DataField="AdminFee" HeaderText="Administration Fee" />
                                    <asp:BoundField DataField="TotalAmountText" HeaderText="Total Amount" />
                                    <asp:BoundField DataField="TotalTransferText" HeaderText="Total Transfer" />
                                    <asp:BoundField DataField="Notes" HeaderText="Notes" />
                                    <asp:BoundField DataField="Status" HeaderText="Status" />
                                    <asp:BoundField DataField="CreatedDate" HeaderText="Created Date" />
                                    <asp:BoundField DataField="ModifiedDate" HeaderText="Updated Date" />
                                    <asp:TemplateField HeaderText="Action">
                                        <ItemTemplate>
                                            <asp:Button ID="btnApprove" CssClass="button btn-success" runat="server" Text="Approve" OnClick="btnApprove_Click" CausesValidation="false"/>
                                            |
                                            <asp:Button ID="btnReject" CssClass="button btn-danger" runat="server" Text="Reject" OnClick="btnReject_Click" CausesValidation="false"/>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Scripts" runat="server">
</asp:Content>
