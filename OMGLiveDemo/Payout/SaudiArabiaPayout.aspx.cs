﻿using Newtonsoft.Json;
using OMGLiveDemo.Models;
using OMGLiveDemo.Models.OMG;
using OMGLiveDemo.Scripts;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.Payout
{
    public partial class SaudiArabiaPayout : System.Web.UI.Page
    {
        OMG omg = new OMG();
        List<Bank> bank = new List<Bank>();
        List<tblM_Beneficiary> benf = new List<tblM_Beneficiary>();
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();
        double amount = 0;
        double totalAmount = 0;
        double totalTransfer = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                handlePostback();

                if (!IsPostBack)
                {
                    bind();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        void handlePostback()
        {
            diverror.Visible = false;
            if (Session["Payout"] != null)
            {
                lblError.Text = Session["Payout"].ToString();
                diverror.Visible = true;
                Session["Payout"] = null;
            }

            if (SessionLib.Current.Role == Roles.MasterArea || SessionLib.Current.Role == Roles.TravelAgent)
            {
                getBalance();
            }
        }

        void checkModal()
        {
            if (hfModalCityOpen.Value == "yes")
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "Pop", "var isModalCity = true", true);
            else
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "Pop", "var isModalCity = false", true);
        }

        void getBalance()
        {
            long id = Convert.ToInt64(SessionLib.Current.AdminID);
            if (SessionLib.Current.Role == Roles.MasterArea)
            {
                var bal = db.tblM_Master_Area_Balance.Where(p => p.MasterAreaID == id && p.CountryID == 103).FirstOrDefault();
                hfBalance.Value = bal.Balance.ToString();
                divbalance.Visible = true;
                lblBalance.Text = bal.Currency + " " + String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToDouble(hfBalance.Value)).Replace("Rp", "");
            }
            else if (SessionLib.Current.Role == Roles.TravelAgent)
            {
                var bal = db.tblM_Travel_Agent_Balance.Where(p => p.TravelAgentID == id && p.CountryID == 103).FirstOrDefault();
                hfBalance.Value = bal.Balance.ToString();
                divbalance.Visible = true;
                lblBalance.Text = bal.Currency + " " + String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToDouble(hfBalance.Value)).Replace("Rp", "");
            }
        }

        void bindBank()
        {
            var banks = db.tblM_Bank.Where(p => p.CountryID == 194 && p.isActive == 1).ToList();
            if (banks != null)
            {
                ddlBank.DataSource = null;
                ddlBank.DataSource = banks;
                ddlBank.DataValueField = "Code";
                ddlBank.DataTextField = "Name";
                ddlBank.DataBind();
            }
        }

        void bindPurpose()
        {
            var pur = db.tblM_Payout_Purpose.ToList();
            if (pur != null)
            {
                ddlPurpose.DataSource = null;
                ddlPurpose.DataSource = pur;
                ddlPurpose.DataValueField = "ID";
                ddlPurpose.DataTextField = "Purpose";
                ddlPurpose.DataBind();
            }
        }

        void bind()
        {
            long agid = Convert.ToInt64(SessionLib.Current.AdminID);

            bindBank();
            bindPurpose();

            var bankacc = db.vw_Bank_Account.Where(p => p.CountryID == 103).ToList();
            if (bankacc != null)
            {
                for (int i = 0; i < bankacc.Count; i++)
                {
                    bankacc[i].NameBank = bankacc[i].AccountHolder + " - " + bankacc[i].BankName;
                }

                ddlBankAccount.DataSource = null;
                ddlBankAccount.DataSource = bankacc;
                ddlBankAccount.DataValueField = "BankAccountID";
                ddlBankAccount.DataTextField = "NameBank";
                ddlBankAccount.DataBind();
            }

            if (SessionLib.Current.Role == Roles.Agent)
                benf = db.tblM_Beneficiary.Where(p => p.AgentID == agid && p.CountryID == 194 && p.isAgent == 1 && p.isActive == 1).ToList();
            else if (SessionLib.Current.Role == Roles.MasterArea)
                benf = db.tblM_Beneficiary.Where(p => p.AgentID == agid && p.CountryID == 194 && p.isAgent == 2 && p.isActive == 1).ToList();
            else if (SessionLib.Current.Role == Roles.TravelAgent)
                benf = db.tblM_Beneficiary.Where(p => p.AgentID == agid && p.CountryID == 194 && p.isAgent == 3 && p.isActive == 1).ToList();
            else
                benf = db.tblM_Beneficiary.Where(p => p.CountryID == 194 && p.isAgent == 0 && p.isActive == 1).ToList();

            if (benf.Count > 0)
            {
                for (int i = 0; i < benf.Count; i++)
                {
                    benf[i].NameBankAccount = benf[i].Name + " - " + benf[i].Bank + " - " + benf[i].Account;
                }
                ddlBeneficiary.DataSource = null;
                ddlBeneficiary.DataSource = benf;
                ddlBeneficiary.DataValueField = "Account";
                ddlBeneficiary.DataTextField = "NameBankAccount";
                ddlBeneficiary.DataBind();
            }

            var sendertype = db.tblM_Sender_Type.ToList();
            ddlSenderType.DataSource = null;
            ddlSenderType.DataSource = sendertype;
            ddlSenderType.DataValueField = "ID";
            ddlSenderType.DataTextField = "Type";
            ddlSenderType.DataBind();

            List<tblM_Sender> sender = new List<tblM_Sender>();
            if (SessionLib.Current.Role == Roles.Agent)
                sender = db.tblM_Sender.Where(p => p.AgentID == agid && p.CountryID == 103 && p.isActive == 1 && p.isAgent == 1 && p.Status == "approved").ToList();
            else if (SessionLib.Current.Role == Roles.MasterArea)
                sender = db.tblM_Sender.Where(p => p.AgentID == agid && p.CountryID == 103 && p.isActive == 1 && p.isAgent == 2 && p.Status == "approved").ToList();
            else if (SessionLib.Current.Role == Roles.TravelAgent)
                sender = db.tblM_Sender.Where(p => p.AgentID == agid && p.CountryID == 103 && p.isActive == 1 && p.isAgent == 3 && p.Status == "approved").ToList();
            else
                sender = db.tblM_Sender.Where(p => p.CountryID == 103 && p.isActive == 1 && p.isAgent == 0 && p.Status == "approved").ToList();
            ddlSender.DataSource = null;
            ddlSender.DataSource = sender;
            ddlSender.DataValueField = "SenderID";
            ddlSender.DataTextField = "Fullname";
            ddlSender.DataBind();
            if (sender.Count != 0)
                setSender();


            var cur = omg.getRateIDRSAR();
            if (cur != null)
            {
                hfRate.Value = cur.Rate;
                hfRateOriginal.Value = cur.Rate;
            }

            if (SessionLib.Current.Role == Roles.Agent)
            {
                divrateall.Visible = false;
                var agentid = Int64.Parse(SessionLib.Current.AdminID);
                var rateagent = db.vw_Agent_Rate.Where(p => p.AgentID == agentid && p.RemittanceID == 3).FirstOrDefault();
                var rated = db.tblM_Rate.Where(p => p.RateID == rateagent.RateID).FirstOrDefault();
                hfRate.Value = (Double.Parse(hfRate.Value) + Double.Parse(rated.Margin)).ToString();
                hfMargin.Value = (Double.Parse(rateagent.AgentMargin)).ToString();
                if (rated != null)
                {
                    lblCurrency.Text = "1 SAR = " + String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(Int32.Parse(hfRate.Value) + Int32.Parse(hfMargin.Value))).Replace("Rp", "") + " IDR";
                    lblCurrencyUpdate.Text = "Last update : " + rated.UpdatedDate.Value.ToLongTimeString() + ", " + rated.UpdatedDate.Value.ToLongDateString();
                }

                hfAdminFee.Value = Convert.ToString(rateagent.AdminFee);
                lblAdminFee.Text = rateagent.AdminFeeCurrency + " " + rateagent.AdminFee;
            }
            else if (SessionLib.Current.Role == Roles.MasterArea)
            {
                divrateall.Visible = false;
                var rateagent = db.vw_Master_Area_Rate.Where(p => p.MasterAreaID == agid && p.RemittanceID == 3).FirstOrDefault();
                hfRate.Value = (Double.Parse(hfRate.Value) + Double.Parse(rateagent.Margin)).ToString();
                hfMargin.Value = (Double.Parse(rateagent.MasterAreaMargin)).ToString();
                lblCurrency.Text = "1 SAR = " + String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(Int32.Parse(hfRate.Value) + Int32.Parse(hfMargin.Value))).Replace("Rp", "") + " IDR";
                lblCurrencyUpdate.Text = "Last update : " + cur.DateModified.ToLongTimeString() + ", " + cur.DateModified.ToLongDateString();

                hfAdminFee.Value = rateagent.Fee.ToString();
                lblAdminFee.Text = "IDR " + String.Format(new CultureInfo("id-ID"), "{0:n}", Convert.ToDouble(rateagent.Fee));

                divtransferto.Visible = false;

                getBalance();
            }
            else if (SessionLib.Current.Role == Roles.TravelAgent)
            {
                divrateall.Visible = false;
                var rateagent = db.vw_Travel_Agent_Rate.Where(p => p.TravelAgentID == agid && p.RemittanceID == 3).FirstOrDefault();
                hfRate.Value = (Double.Parse(hfRate.Value) + Double.Parse(rateagent.Margin)).ToString();
                hfMargin.Value = (Double.Parse(rateagent.TravelAgentMargin)).ToString();
                lblCurrency.Text = "1 SAR = " + String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(Int32.Parse(hfRate.Value) + Int32.Parse(hfMargin.Value))).Replace("Rp", "") + " IDR";
                lblCurrencyUpdate.Text = "Last update : " + cur.DateModified.ToLongTimeString() + ", " + cur.DateModified.ToLongDateString();

                hfAdminFee.Value = rateagent.Fee.ToString();
                lblAdminFee.Text = "IDR " + String.Format(new CultureInfo("id-ID"), "{0:n}", Convert.ToDouble(rateagent.Fee));

                divtransferto.Visible = false;

                getBalance();
            }
            else
            {
                divrateagent.Visible = false;
                List<tblM_Rate> rate = new List<tblM_Rate>();
                rate = omg.getListRateIDRSAR();
                if (rate.Count > 0)
                {
                    for (int i = 0; i < rate.Count; i++)
                    {
                        rate[i].Rate = (Int32.Parse(hfRate.Value) + Int32.Parse(rate[i].Margin)).ToString();
                        rate[i].NameRate = rate[i].Name + " - Rp " + String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(Int32.Parse(rate[i].Rate))).Replace("Rp", "");
                        if (i == 0)
                            hfRate.Value = rate[i].Rate;
                    }
                    hfMargin.Value = "0";
                    ddlRate.DataSource = null;
                    ddlRate.DataSource = rate;
                    ddlRate.DataValueField = "Rate";
                    ddlRate.DataTextField = "NameRate";
                    ddlRate.DataBind();
                }

                divadminfee.Visible = true;
                var afee = db.tblM_Admin_Fee.Where(p => p.RemittanceID == 3).FirstOrDefault();
                hfAdminFee.Value = afee.Amount;
                lblAdminFees.Text = "IDR " + afee.Amount;
            }
        }

        bool checkBeforeTransfer()
        {
            if (!checkSender()) { return true; }

            if (!checkBeneficiary()) { return true; }

            if (!checkPayout()) { return true; }

            if (SessionLib.Current.Role == Roles.MasterArea || SessionLib.Current.Role == Roles.TravelAgent)
            {
                getBalance();
                var am = Convert.ToDouble(hfTotalAmount.Value) + Convert.ToDouble(hfAdminFee.Value);
                if (am > Convert.ToDouble(hfBalance.Value))
                {
                    getBalance();
                    diverror.Visible = true;
                    lblError.Text = "your amount is not enough to process this transfer. please note that you need to pay an admin fee for SAR " + hfAdminFee.Value;
                    return true;
                }
            }

            return false;
        }

        protected void btnProcess_Click(object sender, EventArgs e)
        {
            diverror.Visible = false;
            try
            {
                if (checkBeforeTransfer())
                    return;

                tblT_Payout pay = new tblT_Payout();
                pay.RemittanceID = 3;
                pay.BeneficiaryName = txtName.Text.Trim();
                pay.BeneficiaryAccount = txtAccount.Text.Trim();

                if (ddlAmountType.SelectedValue == "1")
                {
                    pay.Amount = Convert.ToDouble(hfAmount.Value);
                    pay.TotalAmount = Convert.ToDouble(hfTotalAmount.Value);
                    pay.TotalTransfer = Convert.ToDouble(hfTotalTransfer.Value);
                }
                else if (ddlAmountType.SelectedValue == "2")
                {
                    pay.Amount = Convert.ToDouble(hfAmount.Value);
                    pay.TotalAmount = Convert.ToDouble(hfTotalAmount.Value.ToString().Trim());
                    pay.TotalTransfer = Convert.ToDouble(hfTotalTransfer.Value);
                }
                pay.AdminFee = hfAdminFee.Value;
                pay.UniqueCode = "0";

                pay.BeneficiaryBank = ddlBank.SelectedValue;
                pay.BankName = ddlBank.SelectedItem.Text.Trim();
                pay.Currency = "SAR";
                if (txtNotes.Text.Trim().Length > 17)
                    pay.Notes = txtNotes.Text.Trim().Substring(0, 16);
                else
                    pay.Notes = txtNotes.Text.Trim();
                pay.isActive = 1;
                if (SessionLib.Current.Role == Roles.Agent)
                {
                    pay.BankAccountID = Convert.ToInt64(ddlBankAccount.SelectedValue);
                    pay.isAgent = 1;
                    pay.Rate = (Int32.Parse(hfRate.Value) + Int32.Parse(hfMargin.Value)).ToString();
                    pay.RateGiven = (Int32.Parse(hfRate.Value)).ToString();
                }
                else if (SessionLib.Current.Role == Roles.MasterArea)
                {
                    pay.BankAccountID = 0;
                    pay.isAgent = 2;
                    pay.Rate = (Int32.Parse(hfRate.Value) + Int32.Parse(hfMargin.Value)).ToString();
                    pay.RateGiven = (Int32.Parse(hfRate.Value)).ToString();
                }
                else if (SessionLib.Current.Role == Roles.MasterArea)
                {
                    pay.BankAccountID = 0;
                    pay.isAgent = 3;
                    pay.Rate = (Int32.Parse(hfRate.Value) + Int32.Parse(hfMargin.Value)).ToString();
                    pay.RateGiven = (Int32.Parse(hfRate.Value)).ToString();
                }
                else
                {
                    pay.BankAccountID = Convert.ToInt64(ddlBankAccount.SelectedValue);
                    pay.isAgent = 0;
                    pay.Rate = ddlRate.SelectedValue.ToString();
                    pay.RateGiven = (Int32.Parse(hfRateOriginal.Value)).ToString();
                }
                pay.CreatedBy = Int64.Parse(SessionLib.Current.AdminID);
                pay.CreatedDate = DateTime.Now;
                pay.Status = "created";
                pay.PurposeID = Convert.ToInt64(ddlPurpose.SelectedValue);
                db.SaveChanges();
                db.tblT_Payout.Add(pay);
                db.SaveChanges();

                if (SessionLib.Current.Role == Roles.MasterArea || SessionLib.Current.Role == Roles.TravelAgent)
                {
                    processBalance(Int64.Parse(SessionLib.Current.AdminID), pay.PayoutID);
                }
                
                tblT_Payout_Sender ps = new tblT_Payout_Sender();
                ps.PayoutID = pay.PayoutID;
                ps.CityID = Convert.ToInt64(hfCityID.Value);
                ps.SenderTypeID = Convert.ToInt32(ddlSenderType.SelectedValue);
                if (ps.SenderTypeID == 1)
                    ps.SenderType = "Personal";
                else
                    ps.SenderType = "Company";
                ps.SenderName = txtSenderName.Text;
                ps.SenderAddress = txtSenderAddress.Text;
                ps.SenderPhone = txtSenderPhone.Text;
                ps.SenderEmail = txtSenderEmail.Text;
                ps.SenderFileID = txtNationalID.Text;

                db.tblT_Payout_Sender.Add(ps);
                db.SaveChanges();

                if (ddlSenderType.SelectedValue == "1")
                {
                    tblT_Payout_Sender_FIle psf = new tblT_Payout_Sender_FIle();
                    psf.PayoutSenderID = ps.PayoutSenderID;
                    psf.FileTypeID = 3;
                    
                    psf.Value = txtNationalID.Text.Trim();
                    psf.URL = hfSenderImageURL.Value;
                    ps.SenderFileIDURL = hfSenderImageURL.Value;

                    db.tblT_Payout_Sender_FIle.Add(psf);
                    db.SaveChanges();
                }
                else
                {
                    var sendtype = db.tblM_Sender_File_Type.Where(p => p.CountryID == 103 && p.SenderTypeID == 2).ToList();
                    for (int i = 0; i < sendtype.Count; i++)
                    {
                        tblT_Payout_Sender_FIle psf = new tblT_Payout_Sender_FIle();
                        psf.PayoutSenderID = ps.PayoutSenderID;
                        psf.FileTypeID = sendtype[i].ID;

                        if (sendtype[i].ID == 4)
                        {
                            psf.Value = txtTDP.Text.Trim();
                            psf.URL = hfTDPImageURL.Value;
                            //default doc
                            ps.SenderFileIDURL = hfTDPImageURL.Value;

                        }
                        if (sendtype[i].ID == 5)
                        {
                            psf.Value = txtSKMenkumham.Text.Trim();
                            psf.URL = hfSKImageURL.Value;
                        }
                        if (sendtype[i].ID == 6)
                        {
                            psf.Value = txtNPWP.Text.Trim();
                            psf.URL = hfNPWPImageURL.Value;
                        }
                        if (sendtype[i].ID == 7)
                        {
                            psf.Value = txtSIUP.Text.Trim();
                            psf.URL = hfSIUPImageURL.Value;
                        }

                        db.tblT_Payout_Sender_FIle.Add(psf);
                        db.SaveChanges();
                    }
                }
                
                tblT_Payout_Beneficiary pb = new tblT_Payout_Beneficiary();
                pb.PayoutID = pay.PayoutID;
                pb.CityID = 0;
                pb.BeneficiaryName = pay.BeneficiaryName;
                pb.BeneficiaryBank = pay.BeneficiaryBank;
                pb.BeneficiaryAccount = pay.BeneficiaryAccount;
                pb.BeneficiaryEmail = txtEmail.Text.Trim();
                pb.BeneficiaryPhone = txtPhone.Text.Trim();
                pb.BeneficiaryAddress = txtAddress.Text.Trim();
                pb.BeneficiaryFileID = pb.BeneficiaryAccount;
                pb.BankName = pay.BankName;
                db.tblT_Payout_Beneficiary.Add(pb);

                db.SaveChanges();

                senders();
                beneficiary();

                if (SessionLib.Current.Role == Roles.MasterArea || SessionLib.Current.Role == Roles.TravelAgent)
                {
                    if (makePayout(pay))
                    {
                        Session["SuccessPayout"] = "Success created payout. Your request will update soon";
                        Response.Redirect("PayoutHistory", false);
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "Pop", "var isModal = false", true);
                        cancelTransfer();
                        return;
                    }
                }
                else
                {
                    Session["PayoutID"] = pay.PayoutID;
                    Response.Redirect("UploadReceipt", false);
                    // end changes //
                }
            }
            catch (Exception ex)
            {
                Session["Payout"] = "Error create payout. Message : " + ex.Message;
            }
        }

        void saveSenderFile()
        {
            if (ddlSenderType.SelectedValue == "1")
            {
                var sendtype = db.tblM_Sender_File_Type.Where(p => p.CountryID == 103 && p.SenderTypeID == 1).ToList();
                for (int i = 0; i < sendtype.Count; i++)
                {
                    if (sendtype[i].ID == 3)
                    {
                        if (hfSenderImageURL.Value == "" || hfSenderImageURL.Value == null)
                        {
                            string filename = "Temporary_NationalID_" + Common.getTimeSpan() + "." + fuNationalID.PostedFile.ContentType.Replace("image/", "");
                            string targetPath = Server.MapPath("../Sender/Picture/Indonesia/Personal/" + filename);
                            Stream strm = fuNationalID.PostedFile.InputStream;
                            Common.SaveImage(0.5, strm, targetPath);

                            hfSenderImageURL.Value = ConfigurationManager.AppSettings["URL_SENDER_INDONESIA_PERSONAL"].ToString() + filename;

                            txtNationalID.Enabled = false;
                            fuNationalID.Visible = false;
                            lkbNationalID.Visible = true;
                        }
                    }
                }
            }
            if (ddlSenderType.SelectedValue == "2")
            {
                var sendtype = db.tblM_Sender_File_Type.Where(p => p.CountryID == 103 && p.SenderTypeID == 2).ToList();
                for (int i = 0; i < sendtype.Count; i++)
                {
                    if (sendtype[i].ID == 4)
                    {
                        if (hfTDPImageURL.Value == "" || hfTDPImageURL.Value == null)
                        {
                            string filename = "Temporary_TDP_" + Common.getTimeSpan() + "." + fuTDP.PostedFile.ContentType.Replace("image/", "");
                            string targetPath = Server.MapPath("../Sender/Picture/Indonesia/Company/" + filename);
                            Stream strm = fuTDP.PostedFile.InputStream;
                            Common.SaveImage(0.5, strm, targetPath);

                            hfTDPImageURL.Value = ConfigurationManager.AppSettings["URL_SENDER_INDONESIA_PERSONAL"].ToString() + filename;

                            //default doc
                            hfSenderImageURL.Value = ConfigurationManager.AppSettings["URL_SENDER_INDONESIA_PERSONAL"].ToString() + filename;

                            txtTDP.Enabled = false;
                            fuTDP.Visible = false;
                            lkbTDP.Visible = true;
                        }
                    }
                    if (sendtype[i].ID == 5)
                    {
                        if (hfSKImageURL.Value == "" || hfSKImageURL.Value == null)
                        {
                            string filename = "Temporary_SKMenkumham_" + Common.getTimeSpan() + "." + fuSKMenkumham.PostedFile.ContentType.Replace("image/", "");
                            string targetPath = Server.MapPath("../Sender/Picture/Indonesia/Company/" + filename);
                            Stream strm = fuSKMenkumham.PostedFile.InputStream;
                            Common.SaveImage(0.5, strm, targetPath);

                            hfSKImageURL.Value = ConfigurationManager.AppSettings["URL_SENDER_INDONESIA_PERSONAL"].ToString() + filename;

                            txtSKMenkumham.Enabled = false;
                            fuSKMenkumham.Visible = false;
                            lkbSK.Visible = true;
                        }
                    }
                    if (sendtype[i].ID == 6)
                    {
                        if (hfNPWPImageURL.Value == "" || hfNPWPImageURL.Value == null)
                        {
                            string filename = "Temporary_NPWP_" + Common.getTimeSpan() + "." + fuNPWP.PostedFile.ContentType.Replace("image/", "");
                            string targetPath = Server.MapPath("../Sender/Picture/Indonesia/Company/" + filename);
                            Stream strm = fuNPWP.PostedFile.InputStream;
                            Common.SaveImage(0.5, strm, targetPath);

                            hfNPWPImageURL.Value = ConfigurationManager.AppSettings["URL_SENDER_INDONESIA_PERSONAL"].ToString() + filename;

                            txtNPWP.Enabled = false;
                            fuNPWP.Visible = false;
                            lkbNPWP.Visible = true;
                        }
                    }
                    if (sendtype[i].ID == 7)
                    {
                        if (hfSIUPImageURL.Value == "" || hfSIUPImageURL.Value == null)
                        {
                            string filename = "Temporary_SIUP_" + Common.getTimeSpan() + "." + fuSIUP.PostedFile.ContentType.Replace("image/", "");
                            string targetPath = Server.MapPath("../Sender/Picture/Indonesia/Company/" + filename);
                            Stream strm = fuSIUP.PostedFile.InputStream;
                            Common.SaveImage(0.5, strm, targetPath);

                            hfSIUPImageURL.Value = ConfigurationManager.AppSettings["URL_SENDER_INDONESIA_PERSONAL"].ToString() + filename;

                            txtSIUP.Enabled = false;
                            fuSIUP.Visible = false;
                            lkbSIUP.Visible = true;
                        }
                    }
                }
            }
        }

        void processBalance(long maid, long payid)
        {
            if (SessionLib.Current.Role == Roles.MasterArea)
            {
                var bal = db.tblM_Master_Area_Balance.Where(p => p.MasterAreaID == maid && p.CountryID == 103).FirstOrDefault();

                tblH_Master_Area_Balance_Update hbal = new tblH_Master_Area_Balance_Update();
                hbal.BalanceID = bal.BalanceID;
                hbal.Type = "Payout";
                hbal.RemittanceID = 3;
                hbal.MasterAreaID = maid;
                hbal.PayoutID = payid;
                hbal.BankName = ddlBank.SelectedItem.Text.Trim();
                hbal.Account = txtAccount.Text.Trim();
                hbal.Beneficiary = txtName.Text.Trim();
                hbal.Amount = Convert.ToDouble(hfAmount.Value);
                hbal.AdminFee = Convert.ToDouble(hfAdminFee.Value);
                hbal.DateCreated = DateTime.Now;

                hbal.BalancePrevious = bal.Balance;
                hbal.Balance = bal.Balance - (Convert.ToDouble(hfAmount.Value) + Convert.ToDouble(hfAdminFee.Value));
                bal.Balance = hbal.Balance;
                bal.UpdatedDate = DateTime.Now;
                hbal.BalanceID = bal.BalanceID;

                db.tblH_Master_Area_Balance_Update.Add(hbal);
            }
            else if (SessionLib.Current.Role == Roles.TravelAgent)
            {
                var bal = db.tblM_Travel_Agent_Balance.Where(p => p.TravelAgentID == maid && p.CountryID == 103).FirstOrDefault();

                tblH_Travel_Agent_Balance_Update hbal = new tblH_Travel_Agent_Balance_Update();
                hbal.BalanceID = bal.BalanceID;
                hbal.Type = "Payout";
                hbal.RemittanceID = 3;
                hbal.TravelAgentID = maid;
                hbal.PayoutID = payid;
                hbal.BankName = ddlBank.SelectedItem.Text.Trim();
                hbal.Account = txtAccount.Text.Trim();
                hbal.Beneficiary = txtName.Text.Trim();
                hbal.Amount = Convert.ToDouble(hfAmount.Value);
                hbal.AdminFee = Convert.ToDouble(hfAdminFee.Value);
                hbal.DateCreated = DateTime.Now;

                hbal.BalancePrevious = bal.Balance.Value;
                hbal.Balance = bal.Balance.Value - (Convert.ToDouble(hfAmount.Value) + Convert.ToDouble(hfAdminFee.Value));
                bal.Balance = hbal.Balance;
                bal.UpdatedDate = DateTime.Now;
                hbal.BalanceID = bal.BalanceID;

                db.tblH_Travel_Agent_Balance_Update.Add(hbal);
            }
            db.SaveChanges();
        }

        bool makePayout(tblT_Payout pay)
        {
            var payid = pay.PayoutID;
            PayoutRequest pr = new PayoutRequest();
            pr.RemittanceID = 3;
            pr.Currency = "SAR";
            pr.AmountIDR = Convert.ToString(pay.TotalTransfer);
            pr.Amount = pay.Amount.Value.ToString().Replace(",", ".");
            pr.BeneficiaryName = pay.BeneficiaryName;
            pr.BeneficiaryBank = pay.BeneficiaryBank;
            pr.BeneficiaryAccount = pay.BeneficiaryAccount;
            pr.BeneficiaryEmail = pay.BeneficiaryEmail;
            pr.PartnerReferenceID = pay.PayoutID.ToString();
            pr.Notes = pay.Notes;

            var payben = db.tblT_Payout_Beneficiary.Where(p => p.PayoutID == payid).FirstOrDefault();
            Beneficiary ben = new Beneficiary();
            ben.Name = payben.BeneficiaryName;
            ben.Address = payben.BeneficiaryAddress;
            ben.Email = payben.BeneficiaryEmail;
            ben.Phone = payben.BeneficiaryPhone;
            ben.Identity = pr.BeneficiaryAccount;
            pr.Beneficiary = ben;

            var paysen = db.tblT_Payout_Sender.Where(p => p.PayoutID == payid).FirstOrDefault();
            Models.OMG.Sender sen = new Models.OMG.Sender();
            sen.Name = paysen.SenderName;
            sen.Address = paysen.SenderAddress;
            sen.Email = paysen.SenderEmail;
            sen.Phone = paysen.SenderPhone;
            sen.Identity = paysen.SenderFileID;
            pr.Sender = sen;

            OutputModel output = omg.createApprovePayoutNew(pr);
            if (output.status != "error")
            {
                PayoutResponse prs = JsonConvert.DeserializeObject<PayoutResponse>(output.data.ToString());
                pay.Reference = prs.ReferenceID;
                pay.Status = prs.Status;
                pay.ModifiedDate = prs.DateModified;
                db.SaveChanges();

                var snd = db.tblT_Payout_Sender.Where(p => p.PayoutID == pay.PayoutID).FirstOrDefault();
                snd.Reference = pay.Reference;

                var bnf = db.tblT_Payout_Beneficiary.Where(p => p.PayoutID == pay.PayoutID).FirstOrDefault();
                bnf.Reference = pay.Reference;

                var hbal = db.tblH_Master_Area_Balance_Update.Where(p => p.PayoutID == pay.PayoutID).FirstOrDefault();
                hbal.Reference = pay.Reference;

                db.SaveChanges();

                if (SessionLib.Current.Role == Roles.MasterArea)
                    omg.sendEmailMasterArea(pay);
                else if (SessionLib.Current.Role == Roles.TravelAgent)
                    omg.sendEmailTravelAgent(pay);

                return true;
            }
            else
            {
                diverror.Visible = true;
                lblError.Text = output.message;
                return false;
            }
        }

        protected void ddlBank_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void ddlBeneficiaryType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlBeneficiaryType.SelectedValue == "-1")
            {
                divbenf.Visible = false;
            }
            if (ddlBeneficiaryType.SelectedValue == "1")
            {
                divbenf.Visible = true;
                divbeneficiary.Visible = true;
                txtName.Enabled = true;
                txtAccount.Enabled = false;
                ddlBank.Enabled = false;
                cbSaveBenfeficiary.Visible = false;
                cbUpdateBeneficiary.Visible = true;

                txtAddress.Enabled = false;
                
                setBeneficiary();
            }
            else if (ddlBeneficiaryType.SelectedValue == "2")
            {
                divbenf.Visible = true;
                divbeneficiary.Visible = false;
                txtName.Enabled = true;
                txtAccount.Enabled = true;
                ddlBank.Enabled = true;
                cbSaveBenfeficiary.Visible = true;
                cbUpdateBeneficiary.Visible = false;

                txtAddress.Enabled = true;
            }
        }

        protected void btnValidate_Click(object sender, EventArgs e)
        {
            BankAccount ba = new BankAccount();
            ba.CountryID = 103;
            ba.AccountNo = txtAccount.Text.Trim();
            ba.BankCode = ddlBank.SelectedValue;
            var acc = omg.validateAccount(ba);
            if (acc != null)
            {
                txtName.Text = acc.AccountName.Replace("  ", "");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Found " + acc.AccountName + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('The account of " + ba.AccountNo + " and bank of " + ddlBank.SelectedItem + " is not found')", true);
            }
        }

        void senders()
        {
            try
            {
                var id = Int64.Parse(SessionLib.Current.AdminID);
                int isAgent = 0;
                if (SessionLib.Current.Role == Roles.Agent)
                {
                    isAgent = 1;
                }
                if (SessionLib.Current.Role == Roles.MasterArea)
                {
                    isAgent = 2;
                }
                if (SessionLib.Current.Role == Roles.TravelAgent)
                {
                    isAgent = 3;
                }

                var check = db.tblM_Sender.Where(p => p.Phone == txtSenderPhone.Text.Trim() && p.AgentID == id && p.isAgent == isAgent && p.CountryID == 103).FirstOrDefault();
                if (check == null)
                {

                    tblM_Sender bnf = new tblM_Sender();
                    bnf.Fullname = txtSenderName.Text.Trim();
                    bnf.Address = txtSenderAddress.Text.Trim();
                    bnf.Phone = txtSenderPhone.Text.Trim();
                    bnf.Email = txtSenderEmail.Text.Trim();
                    bnf.CountryID = 103;
                    bnf.Country = "Indonesia";
                    bnf.Status = "created";
                    bnf.CityID = Convert.ToInt64(hfCityID.Value);
                    bnf.JoinDate = DateTime.Now;
                    bnf.AddedBy = Convert.ToInt64(SessionLib.Current.AdminID);
                    bnf.Email = txtEmail.Text.Trim();
                    bnf.SenderTypeID = Convert.ToInt64(ddlSenderType.SelectedValue);

                    bnf.isAgent = isAgent;
                    bnf.AgentID = id;
                    bnf.AddedBy = bnf.AgentID;
                    bnf.isActive = 1;
                    db.tblM_Sender.Add(bnf);
                    db.SaveChanges();

                    if (bnf.SenderTypeID == 1)
                    {
                        var ftype = db.tblM_Sender_File_Type.Where(p => p.SenderTypeID == bnf.SenderTypeID && p.CountryID == 103).FirstOrDefault();
                        tblM_Sender_File sf = new tblM_Sender_File();
                        sf.SenderID = bnf.SenderID;
                        sf.FileTypeID = ftype.ID;
                        sf.Value = txtNationalID.Text.ToString().Trim();
                        
                        sf.URL = hfSenderImageURL.Value;

                        db.tblM_Sender_File.Add(sf);
                        db.SaveChanges();
                    }
                    else
                    {
                        var ftype = db.tblM_Sender_File_Type.Where(p => p.SenderTypeID == bnf.SenderTypeID && p.CountryID == 103).ToList();
                        for (int i = 0; i < ftype.Count; i++)
                        {
                            tblM_Sender_File sf = new tblM_Sender_File();
                            sf.SenderID = bnf.SenderID;
                            sf.FileTypeID = ftype[i].ID;
                            if (sf.FileTypeID == 4)
                            {
                                sf.Value = txtTDP.Text.ToString().Trim();
                                
                                sf.URL = hfTDPImageURL.Value;
                            }
                            else if (sf.FileTypeID == 5)
                            {
                                sf.Value = txtSKMenkumham.Text.ToString().Trim();
                                
                                sf.URL = hfSKImageURL.Value;
                            }
                            else if (sf.FileTypeID == 6)
                            {
                                sf.Value = txtNPWP.Text.ToString().Trim();
                                
                                sf.URL = hfNPWPImageURL.Value;
                            }
                            else if (sf.FileTypeID == 7)
                            {
                                sf.Value = txtSIUP.Text.ToString().Trim();
                                
                                sf.URL = hfSIUPImageURL.Value;
                            }

                            db.tblM_Sender_File.Add(sf);
                            db.SaveChanges();
                        }
                    }

                    db.SaveChanges();
                }
                else
                {
                    Session["ErrorSender"] = "Failed save sender because the sender already registered";
                }
            }
            catch (Exception ex)
            {
                Session["ErrorSender"] = "Failed save sender for some reason : " + ex.Message.ToString();
            }
        }

        void beneficiary()
        {
            try
            {
                int cid = Int32.Parse(SessionLib.Current.AdminID);
                int isagnt = 0;
                if (SessionLib.Current.Role == Roles.Agent)
                    isagnt = 1;
                else if (SessionLib.Current.Role == Roles.MasterArea)
                    isagnt = 2;
                else if (SessionLib.Current.Role == Roles.TravelAgent)
                    isagnt = 3;

                tblM_Beneficiary bnf = new tblM_Beneficiary();
                if (ddlBeneficiaryType.SelectedValue == "1")
                {
                    if (cbUpdateBeneficiary.Checked)
                    {
                        bnf = db.tblM_Beneficiary.Where(p => p.Account == txtAccount.Text.Trim() && p.BankCode == ddlBank.SelectedValue && p.isAgent == isagnt && p.CreatedBy == cid && p.isActive == 1 && p.CountryID == 194).FirstOrDefault();
                        string paybef = JsonConvert.SerializeObject(benf);
                        bnf.Alias = txtAlias.Text.Trim();
                        bnf.Phone = txtPhone.Text.Trim();
                        bnf.Email = txtEmail.Text.Trim();
                        bnf.Address = txtAddress.Text.Trim();
                        bnf.UpdatedDate = DateTime.Now;
                        bnf.UpdatedBy = cid;
                        bnf.CityID = 0;
                        string payaft = JsonConvert.SerializeObject(benf);

                        tblH_Beneficiary_Update hbnf = new tblH_Beneficiary_Update();
                        hbnf.AdminID = cid;
                        hbnf.BeneficiaryID = bnf.BeneficiaryID;
                        hbnf.PayloadBefore = paybef;
                        hbnf.PayloadAfter = payaft;
                        hbnf.UpdatedDate = bnf.UpdatedDate;
                        db.tblH_Beneficiary_Update.Add(hbnf);

                        db.SaveChanges();
                    }
                }
                else if (ddlBeneficiaryType.SelectedValue == "2")
                {
                    if (cbSaveBenfeficiary.Checked)
                    {
                        bnf = db.tblM_Beneficiary.Where(p => p.Account == txtAccount.Text.Trim() && p.BankCode == ddlBank.SelectedValue && p.isAgent == isagnt && p.CreatedBy == cid && p.isActive == 1).FirstOrDefault();
                        if (bnf == null)
                        {
                            bnf.Name = txtName.Text.Trim();
                            bnf.Account = txtAccount.Text;
                            bnf.Bank = ddlBank.SelectedItem.Text;
                            bnf.BankCode = ddlBank.SelectedValue;
                            bnf.Alias = txtAlias.Text.Trim();
                            bnf.Phone = txtPhone.Text.Trim();
                            bnf.Email = txtEmail.Text.Trim();
                            bnf.Address = txtAddress.Text.Trim();
                            bnf.CityID = 0;
                            bnf.CountryID = 194;
                            bnf.Country = "Saudi Arabia";
                            bnf.isActive = 1;
                            bnf.isAgent = isagnt;
                            bnf.CreatedDate = DateTime.Now;
                            bnf.CreatedBy = cid;
                            db.tblM_Beneficiary.Add(bnf);
                            db.SaveChanges();
                        }
                        else
                        {
                            Session["ErrorBeneficiary"] = "Failed save beneficiary because the beneficiary already registered";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Session["ErrorBeneficiary"] = "Failed save beneficiary for some reason : " + ex.Message.ToString();
            }
        }

        protected void txtAmount_TextChanged(object sender, EventArgs e)
        {
            countAmount();
        }

        void countAmount()
        {
            if (txtAmount.Text != "")
            {
                if (ddlAmountType.SelectedValue == "2")
                {
                    double am = double.Parse(txtAmount.Text, CultureInfo.InvariantCulture.NumberFormat);
                    if (SessionLib.Current.Role == Roles.MasterArea || SessionLib.Current.Role == Roles.TravelAgent)
                    {
                        //totalAmount = am;
                        totalAmount = am + double.Parse(hfAdminFee.Value);
                    }
                    else
                        totalAmount = am + double.Parse(hfAdminFee.Value);
                    if (SessionLib.Current.Role == Roles.Agent || SessionLib.Current.Role == Roles.MasterArea || SessionLib.Current.Role == Roles.TravelAgent)
                    {
                        totalTransfer = am / (double.Parse(hfRate.Value) + double.Parse(hfMargin.Value));
                    }
                    else
                    {
                        totalTransfer = am / (double.Parse(hfRate.Value));
                    }
                    hfAmount.Value = am.ToString();
                    hfTotalAmount.Value = totalAmount.ToString();
                    hfTotalTransfer.Value = totalTransfer.ToString();
                    if (SessionLib.Current.Role == Roles.MasterArea || SessionLib.Current.Role == Roles.TravelAgent)
                    {
                        //lblTotalAmount.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToDouble(hfAmount.Value)).Replace("Rp", "Rp ");
                        lblTotalAmount.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToDouble(totalAmount)).Replace("Rp", "Rp ");
                    }
                    else
                        lblTotalAmount.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToDouble(totalAmount)).Replace("Rp", "Rp ");
                    lblTotalTransfer.Text = String.Format(new CultureInfo("ms-MY"), "{0:c}", Convert.ToDouble(totalTransfer)).Replace("RM", "RM ");
                }
                else if (ddlAmountType.SelectedValue == "1")
                {
                    double am = double.Parse(txtAmount.Text, CultureInfo.InvariantCulture.NumberFormat);
                    totalTransfer = am;
                    if (SessionLib.Current.Role == Roles.Agent || SessionLib.Current.Role == Roles.MasterArea || SessionLib.Current.Role == Roles.TravelAgent)
                        amount = am * (double.Parse(hfRate.Value) + double.Parse(hfMargin.Value));
                    else
                        amount = am * (double.Parse(hfRate.Value));

                    if (SessionLib.Current.Role == Roles.MasterArea || SessionLib.Current.Role == Roles.TravelAgent)
                    {
                        //totalAmount = amount;
                        totalAmount = amount + double.Parse(hfAdminFee.Value);
                    }
                    else
                        totalAmount = amount + double.Parse(hfAdminFee.Value);

                    hfAmount.Value = amount.ToString();
                    hfTotalAmount.Value = totalAmount.ToString();
                    hfTotalTransfer.Value = totalTransfer.ToString();
                    if (SessionLib.Current.Role == Roles.MasterArea || SessionLib.Current.Role == Roles.TravelAgent)
                    {
                        //lblTotalAmount.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToDouble(hfAmount.Value)).Replace("Rp", "Rp ");
                        lblTotalAmount.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToDouble(totalAmount)).Replace("Rp", "Rp ");
                    }
                    else
                        lblTotalAmount.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToDouble(totalAmount)).Replace("Rp", "Rp ");
                    lblTotalTransfer.Text = String.Format(new CultureInfo("ms-MY"), "{0:c}", Convert.ToDouble(totalTransfer)).Replace("RM", "RM ");
                }
            }
        }

        protected void btnError_ServerClick(object sender, EventArgs e)
        {
            diverror.Visible = false;
        }

        protected void ddlBeneficiary_SelectedIndexChanged(object sender, EventArgs e)
        {
            setBeneficiary();
        }

        void setBeneficiary()
        {
            tblM_Beneficiary bf = new tblM_Beneficiary();
            string account = ddlBeneficiary.SelectedValue;
            long id = Convert.ToInt64(SessionLib.Current.AdminID);

            if (SessionLib.Current.Role == Roles.Agent)
                bf = db.tblM_Beneficiary.Where(p => p.CountryID == 194 && p.Account == account && p.isAgent == 1 && p.AgentID == id && p.isActive == 1).FirstOrDefault();
            else if (SessionLib.Current.Role == Roles.MasterArea)
                bf = db.tblM_Beneficiary.Where(p => p.CountryID == 194 && p.Account == account && p.isAgent == 2 && p.AgentID == id && p.isActive == 1).FirstOrDefault();
            else if (SessionLib.Current.Role == Roles.TravelAgent)
                bf = db.tblM_Beneficiary.Where(p => p.CountryID == 194 && p.Account == account && p.isAgent == 3 && p.AgentID == id && p.isActive == 1).FirstOrDefault();
            else
                bf = db.tblM_Beneficiary.Where(p => p.CountryID == 194 && p.Account == account && p.isAgent == 0 && p.isActive == 1).FirstOrDefault();

            if (bf != null)
            {
                txtName.Text = bf.Name;
                txtAccount.Text = bf.Account;
                ddlBank.Text = bf.BankCode;
                txtAlias.Text = bf.Alias;
                txtPhone.Text = bf.Phone;
                txtEmail.Text = bf.Email;
                txtAddress.Text = bf.Address;
                //txtBeneficiaryNationalID.Text = bf.FileID;
            }
        }

        protected void ddlAmountType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlAmountType.SelectedValue == "1")
            {
                lblAmount.Text = "Amount (in SAR) *";
            }
            else if (ddlAmountType.SelectedValue == "2")
            {
                lblAmount.Text = "Amount (in IDR) *";
            }
            countAmount();
        }

        protected void ddlRate_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfRate.Value = ddlRate.SelectedValue;
            countAmount();
        }

        protected void ddlSenderType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSenderType.SelectedValue == "1")
            {
                divpersonal.Visible = true;
                divcompany.Visible = false;
                txtNationalID.Text = "";
            }
            else if (ddlSenderType.SelectedValue == "2")
            {
                divpersonal.Visible = false;
                divcompany.Visible = true;
                txtTDP.Text = "";
                txtSKMenkumham.Text = "";
                txtNPWP.Text = "";
                txtSIUP.Text = "";
            }
        }

        protected void ddlSender_SelectedIndexChanged(object sender, EventArgs e)
        {
            setSender();
        }

        protected void ddlChooseSender_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlChooseSender.SelectedValue == "-1")
            {
                divsender.Visible = false;
            }
            if (ddlChooseSender.SelectedValue == "1")
            {
                divsender.Visible = true;
                divchoosesender.Visible = true;
                txtSenderName.Enabled = false;
                txtSenderEmail.Enabled = false;
                txtSenderPhone.Enabled = false;
                txtSenderAddress.Enabled = false;
                ddlSenderType.Enabled = false;
                txtCity.Text = "";
                
                if (ddlSenderType.SelectedValue == "1")
                {
                    divpersonal.Visible = true;
                    txtNationalID.Enabled = false;
                    fuNationalID.Visible = false;
                    lkbNationalID.Visible = true;

                    divcompany.Visible = false;
                    txtTDP.Enabled = false;
                    fuTDP.Visible = false;
                    lkbTDP.Visible = true;

                    txtSKMenkumham.Enabled = false;
                    fuSKMenkumham.Visible = false;
                    lkbSK.Visible = true;

                    txtNPWP.Enabled = false;
                    fuNPWP.Visible = false;
                    lkbNPWP.Visible = true;

                    txtSIUP.Enabled = false;
                    fuSIUP.Visible = false;
                    lkbSIUP.Visible = true;
                }
                else
                {
                    divpersonal.Visible = false;
                    txtNationalID.Enabled = false;
                    fuNationalID.Visible = false;
                    lkbNationalID.Visible = false;

                    divcompany.Visible = true;
                    txtTDP.Enabled = false;
                    fuTDP.Visible = false;
                    lkbTDP.Visible = true;

                    txtSKMenkumham.Enabled = false;
                    fuSKMenkumham.Visible = false;
                    lkbSK.Visible = true;

                    txtNPWP.Enabled = false;
                    fuNPWP.Visible = false;
                    lkbNPWP.Visible = true;

                    txtSIUP.Enabled = false;
                    fuSIUP.Visible = false;
                    lkbSIUP.Visible = true;
                }

                hfSenderImageURL.Value = "";
                hfTDPImageURL.Value = "";
                hfSKImageURL.Value = "";
                hfNPWPImageURL.Value = "";
                hfSIUPImageURL.Value = "";

                if (ddlSender.SelectedItem != null)
                    setSender();
            }
            else if (ddlChooseSender.SelectedValue == "2")
            {
                divsender.Visible = true;
                divchoosesender.Visible = false;
                txtSenderName.Enabled = true;
                txtSenderName.Text = "";
                txtSenderEmail.Enabled = true;
                txtSenderEmail.Text = "";
                txtSenderPhone.Enabled = true;
                txtSenderPhone.Text = "";
                txtSenderAddress.Enabled = true;
                txtSenderAddress.Text = "";
                ddlSenderType.Enabled = true;
                txtCity.Text = "";
                
                divpersonal.Visible = true;
                txtNationalID.Enabled = true;
                txtNationalID.Text = "";
                fuNationalID.Visible = true;
                lkbNationalID.Visible = false;

                divcompany.Visible = false;
                txtTDP.Enabled = true;
                txtTDP.Text = "";
                fuTDP.Visible = true;
                lkbTDP.Visible = false;

                txtSKMenkumham.Enabled = true;
                txtSKMenkumham.Text = "";
                fuSKMenkumham.Visible = true;
                lkbSK.Visible = false;

                txtNPWP.Enabled = true;
                txtNPWP.Text = "";
                fuNPWP.Visible = true;
                lkbNPWP.Visible = false;

                txtSIUP.Enabled = true;
                txtSIUP.Text = "";
                fuSIUP.Visible = true;
                lkbSIUP.Visible = false;

                hfSenderImageURL.Value = "";
                hfTDPImageURL.Value = "";
                hfSKImageURL.Value = "";
                hfNPWPImageURL.Value = "";
                hfSIUPImageURL.Value = "";
            }
            
        }

        void setSender()
        {
            var id = Convert.ToInt64(ddlSender.SelectedValue);
            var agid = Convert.ToInt64(SessionLib.Current.AdminID);
            tblM_Sender bf = new tblM_Sender();
            if (SessionLib.Current.Role == Roles.Agent)
                bf = db.tblM_Sender.Where(p => p.CountryID == 103 && p.SenderID == id && p.isAgent == 1 && p.AgentID == agid).FirstOrDefault();
            else if (SessionLib.Current.Role == Roles.MasterArea)
                bf = db.tblM_Sender.Where(p => p.CountryID == 103 && p.SenderID == id && p.isAgent == 2 && p.AgentID == agid).FirstOrDefault();
            else if (SessionLib.Current.Role == Roles.TravelAgent)
                bf = db.tblM_Sender.Where(p => p.CountryID == 103 && p.SenderID == id && p.isAgent == 3 && p.AgentID == agid).FirstOrDefault();
            else
                bf = db.tblM_Sender.Where(p => p.CountryID == 103 && p.SenderID == id && p.isAgent == 0 && p.AgentID == 0).FirstOrDefault();
            if (bf != null)
            {
                txtSenderName.Text = bf.Fullname;
                ddlSenderType.Text = bf.SenderTypeID.ToString();
                txtSenderPhone.Text = bf.Phone;
                txtSenderEmail.Text = bf.Email;
                txtSenderAddress.Text = bf.Address;

                if (bf.CityID != null)
                {
                    var city = db.tblM_City.Where(p => p.ID == bf.CityID).FirstOrDefault();
                    hfCityID.Value = city.ID.ToString();
                    hfCityName.Value = city.City;
                    txtCity.Text = city.City;
                }

                if (bf.SenderTypeID == 1)
                {
                    var file = db.tblM_Sender_File.Where(p => p.SenderID == id).FirstOrDefault();

                    txtNationalID.Text = file.Value;
                    divpersonal.Visible = true;
                    divcompany.Visible = false;
                    txtNationalID.Enabled = false;
                    fuNationalID.Visible = false;
                    lkbNationalID.Visible = true;
                    hfSenderImageURL.Value = file.URL;
                }
                else
                {
                    divpersonal.Visible = false;
                    divcompany.Visible = true;

                    var file = db.tblM_Sender_File.Where(p => p.SenderID == id).ToList();
                    for (int i = 0; i < file.Count; i++)
                    {
                        if (file[i].FileTypeID == 4)
                        {
                            txtTDP.Text = file[i].Value;
                            txtTDP.Enabled = false;
                            fuTDP.Visible = false;
                            lkbTDP.Visible = true;
                            hfTDPImageURL.Value = file[i].URL;
                            //default doc
                            hfSenderImageURL.Value = file[i].URL;
                        }
                        if (file[i].FileTypeID == 5)
                        {
                            txtSKMenkumham.Text = file[i].Value;
                            txtSKMenkumham.Enabled = false;
                            fuSKMenkumham.Visible = false;
                            lkbSK.Visible = true;
                            hfSKImageURL.Value = file[i].URL;
                        }
                        if (file[i].FileTypeID == 6)
                        {
                            txtNPWP.Text = file[i].Value;
                            txtNPWP.Enabled = false;
                            fuNPWP.Visible = false;
                            lkbNPWP.Visible = true;
                            hfNPWPImageURL.Value = file[i].URL;
                        }
                        if (file[i].FileTypeID == 7)
                        {
                            txtSIUP.Text = file[i].Value;
                            txtSIUP.Enabled = false;
                            fuSIUP.Visible = false;
                            lkbSIUP.Visible = true;
                            hfSIUPImageURL.Value = file[i].URL;
                        }
                    }
                }
            }


        }

        void doTransfer()
        {
            btnProcess.Visible = false;
            btnBack2.Visible = false;
            dvProcessing.Visible = true;

            ddlAmountType.Enabled = false;
            txtAmount.Enabled = false;
            txtNotes.Enabled = false;
        }

        void cancelTransfer()
        {
            btnProcess.Visible = true;
            btnBack2.Visible = true;
            dvProcessing.Visible = false;

            ddlAmountType.Enabled = true;
            txtAmount.Enabled = true;
            txtNotes.Enabled = true;
        }

        protected void btnConfirmTransafer_Click(object sender, EventArgs e)
        {
            diverror.Visible = false;
            if (checkBeforeTransfer())
                return;

            ScriptManager.RegisterClientScriptBlock(this, GetType(), "Pop", "var isModal = true", true);
            doTransfer();
        }

        protected void btnCancelTransafer_Click(object sender, EventArgs e)
        {
            diverror.Visible = false;
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "Pop", "var isModal = false", true);
            cancelTransfer();
        }

        protected void btnResetFile_Click(object sender, EventArgs e)
        {
            resetFile();
        }

        void resetFile()
        {
            hfSenderImageURL.Value = "";
            txtNationalID.Text = "";
            txtNationalID.Enabled = true;
            fuNationalID.Visible = false;
            lkbNationalID.Visible = true;

            hfTDPImageURL.Value = "";
            txtTDP.Text = "";
            txtTDP.Enabled = true;
            fuTDP.Visible = false;
            lkbTDP.Visible = true;

            hfSKImageURL.Value = "";
            txtSKMenkumham.Text = "";
            txtSKMenkumham.Enabled = true;
            fuSKMenkumham.Visible = false;
            lkbSK.Visible = true;

            hfNPWPImageURL.Value = "";
            txtNPWP.Text = "";
            txtNPWP.Enabled = true;
            fuNPWP.Visible = false;
            lkbNPWP.Visible = true;

            hfSIUPImageURL.Value = "";
            txtSIUP.Text = "";
            txtSIUP.Enabled = true;
            fuSIUP.Visible = false;
            lkbSIUP.Visible = true;
        }

        protected void btnNext1_Click(object sender, EventArgs e)
        {
            diverror.Visible = false;
            if (checkSender())
            {
                saveSenderFile();
                divstep2.Visible = true;
                divstep1.Visible = false;
                divstep3.Visible = false;
            }
        }

        protected void btnNext2_Click(object sender, EventArgs e)
        {
            diverror.Visible = false;
            if (checkBeneficiary())
            {
                divstep3.Visible = true;
                divstep1.Visible = false;
                divstep2.Visible = false;
            }
        }

        bool checkSender()
        {
            if (ddlChooseSender.SelectedValue == "-1")
            {
                diverror.Visible = true;
                lblError.Text = "Please choose sender first!";
                return false;
            }
            else if (ddlChooseSender.SelectedValue == "1" || ddlChooseSender.SelectedValue == "2")
            {
                if (txtSenderName.Text == "")
                {
                    diverror.Visible = true;
                    lblError.Text = "Sender name cannot be empty!";
                    return false;
                }

                if (txtCity.Text == "")
                {
                    diverror.Visible = true;
                    lblError.Text = "Please choose sender's city first!";
                    return false;
                }

                if (txtSenderAddress.Text == "")
                {
                    diverror.Visible = true;
                    lblError.Text = "Sender address cannot be empty!";
                    return false;
                }

                if (txtSenderAddress.Text.Count() < 10)
                {
                    diverror.Visible = true;
                    lblError.Text = "Sender address is too short! Please add your address more than 10 chars.";
                    return false;
                }

                if (txtSenderPhone.Text == "")
                {
                    diverror.Visible = true;
                    lblError.Text = "Sender phone cannot be empty!";
                    return false;
                }

                if (txtSenderEmail.Text == "")
                {
                    diverror.Visible = true;
                    lblError.Text = "Sender email cannot be empty!";
                    return false;
                }

                if (ddlSenderType.SelectedValue == "1")
                {
                    if (txtNationalID.Text == "")
                    {
                        diverror.Visible = true;
                        lblError.Text = "KTP / Passport cannot be empty!";
                        return false;
                    }

                    if (ddlChooseSender.SelectedValue == "2")
                    {
                        if (hfSenderImageURL.Value == null || hfSenderImageURL.Value == "")
                        {
                            if (!fuNationalID.HasFile)
                            {
                                lblError.Text = "Please choose photo of your KTP / Passport first";
                                diverror.Visible = true;
                                return false;
                            }
                            else
                            {
                                if (fuNationalID.PostedFile.ContentType != "image/jpg" && fuNationalID.PostedFile.ContentType != "image/jpeg" && fuNationalID.PostedFile.ContentType != "image/png")
                                {
                                    lblError.Text = "Only jpeg, jpg and png format are allowed";
                                    diverror.Visible = true;
                                    return false;
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (txtTDP.Text == "")
                    {
                        diverror.Visible = true;
                        lblError.Text = "TDP Number cannot be empty!";
                        return false;
                    }

                    if (txtSKMenkumham.Text == "")
                    {
                        diverror.Visible = true;
                        lblError.Text = "SK Menkumham Number cannot be empty!";
                        return false;
                    }

                    if (txtNPWP.Text == "")
                    {
                        diverror.Visible = true;
                        lblError.Text = "NPWP Number cannot be empty!";
                        return false;
                    }

                    if (txtSIUP.Text == "")
                    {
                        diverror.Visible = true;
                        lblError.Text = "SIUP Number cannot be empty!";
                        return false;
                    }

                    if (ddlChooseSender.SelectedValue == "1" || ddlChooseSender.SelectedValue == "2")
                    {
                        if (hfTDPImageURL.Value == null || hfTDPImageURL.Value == "")
                        {
                            if (!fuTDP.HasFile)
                            {
                                diverror.Visible = true;
                                lblError.Text = "Please choose photo of your TDP first!";
                                return false;
                            }
                            else if (fuTDP.PostedFile.ContentType != "image/jpg" && fuTDP.PostedFile.ContentType != "image/jpeg" && fuTDP.PostedFile.ContentType != "image/png")
                            {
                                lblError.Text = "TDP : Only jpeg, jpg and png format are allowed";
                                diverror.Visible = true;
                                return false;
                            }
                        }

                        if (hfSKImageURL.Value == null || hfSKImageURL.Value == "")
                        {
                            if (!fuSKMenkumham.HasFile)
                            {
                                diverror.Visible = true;
                                lblError.Text = "Please choose photo of your SK Menkumham first!";
                                return false;
                            }
                            else if (fuSKMenkumham.PostedFile.ContentType != "image/jpg" && fuSKMenkumham.PostedFile.ContentType != "image/jpeg" && fuSKMenkumham.PostedFile.ContentType != "image/png")
                            {
                                lblError.Text = "SK Menkumham : Only jpeg, jpg and png format are allowed";
                                diverror.Visible = true;
                                return false;
                            }
                        }

                        if (hfNPWPImageURL.Value == null || hfNPWPImageURL.Value == "")
                        {
                            if (!fuNPWP.HasFile)
                            {
                                diverror.Visible = true;
                                lblError.Text = "Please choose photo of your NPWP first!";
                                return false;
                            }
                            else if (fuNPWP.PostedFile.ContentType != "image/jpg" && fuNPWP.PostedFile.ContentType != "image/jpeg" && fuNPWP.PostedFile.ContentType != "image/png")
                            {
                                lblError.Text = "NPWP : Only jpeg, jpg and png format are allowed";
                                diverror.Visible = true;
                                return false;
                            }
                        }

                        if (hfSIUPImageURL.Value == null || hfSIUPImageURL.Value == "")
                        {
                            if (!fuSIUP.HasFile)
                            {
                                diverror.Visible = true;
                                lblError.Text = "Please choose photo of your SIUP first!";
                                return false;
                            }
                            else if (fuSIUP.PostedFile.ContentType != "image/jpg" && fuSIUP.PostedFile.ContentType != "image/jpeg" && fuSIUP.PostedFile.ContentType != "image/png")
                            {
                                lblError.Text = "SIUP : Only jpeg, jpg and png format are allowed";
                                diverror.Visible = true;
                                return false;
                            }
                        }
                    }
                }
            }

            return true;
        }

        bool checkBeneficiary()
        {
            if (txtName.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Beneficiary Name cannot be empty!";
                return false;
            }

            if (txtAccount.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Beneficiary Account cannot be empty!";
                return false;
            }

            if (txtAddress.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Beneficiary Address cannot be empty!";
                return false;
            }

            if (txtEmail.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Beneficiary email cannot be empty!";
                return false;
            }

            if (txtPhone.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Beneficiary phone number cannot be empty!";
                return false;
            }

            return true;
        }

        bool checkPayout()
        {
            if (ddlAmountType.SelectedValue == "-1")
            {
                diverror.Visible = true;
                lblError.Text = "You should choose Amount Type first!";
                return false;
            }

            if (txtAmount.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Amount cannot be empty!";
                return false;
            }

            if (SessionLib.Current.Role != Roles.MasterArea)
            {
                if (ddlBankAccount.Text == "")
                {
                    diverror.Visible = true;
                    lblError.Text = "Please choose the bank account that you would transfer to";
                    return false;
                }
            }

            if (ddlPurpose.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Transfer purpose cannot be empty!";
                return false;
            }

            if (txtNotes.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Notes cannot be empty!";
                return false;
            }

            if (txtNotes.Text.Length > 17)
            {
                diverror.Visible = true;
                lblError.Text = "Only 17 characters allowed for Notes!";
                return false;
            }

            if (Convert.ToDouble(hfTotalTransfer.Value) < 100.00)
            {
                diverror.Visible = true;
                lblError.Text = "Minimum amount for transfer is RM 100 per transaction!";
                return false;
            }

            if (Convert.ToDouble(hfTotalTransfer.Value) > 100000000.00)
            {
                diverror.Visible = true;
                lblError.Text = "Maximum amount for transfer is RM 100.000 per transaction!";
                return false;
            }

            return true;
        }
        

        protected void lkbViewImage_Click(object sender, EventArgs e)
        {
            if (hfSenderImageURL.Value != null)
            {
                Session["ImageURL"] = hfSenderImageURL.Value;
                Session["From"] = "Image";
                string txt = "";
                txt = "KTP/Passport : " + txtNationalID.Text;
                Session["Text"] = txtSenderName.Text + " - " + txt;
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "colorBox", "var isColorbox = true", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('File not found!')", true);
            }
        }

        protected void lkbViewImageTDP_Click(object sender, EventArgs e)
        {
            if (hfTDPImageURL.Value != null)
            {
                Session["ImageURL"] = hfTDPImageURL.Value;
                Session["From"] = "Image";
                string txt = "";
                txt = "TDP : " + txtTDP.Text;
                Session["Text"] = txtSenderName.Text + " - " + txt;
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "colorBox", "var isColorbox = true", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('File not found!')", true);
            }
        }

        protected void lkbViewImageSK_Click(object sender, EventArgs e)
        {
            if (hfSKImageURL.Value != null)
            {
                Session["ImageURL"] = hfSKImageURL.Value;
                Session["From"] = "Image";
                string txt = "";
                txt = "SK Menkumham : " + txtSKMenkumham.Text;
                Session["Text"] = txtSenderName.Text + " - " + txt;
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "colorBox", "var isColorbox = true", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('File not found!')", true);
            }
        }

        protected void lkbViewImageNPWP_Click(object sender, EventArgs e)
        {
            if (hfNPWPImageURL.Value != null)
            {
                Session["ImageURL"] = hfNPWPImageURL.Value;
                Session["From"] = "Image";
                string txt = "";
                txt = "NPWP : " + txtNPWP.Text;
                Session["Text"] = txtSenderName.Text + " - " + txt;
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "colorBox", "var isColorbox = true", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('File not found!')", true);
            }
        }

        protected void lkbViewImageSIUP_Click(object sender, EventArgs e)
        {
            if (hfSIUPImageURL.Value != null)
            {
                Session["ImageURL"] = hfSIUPImageURL.Value;
                Session["From"] = "Image";
                string txt = "";
                txt = "NPWP : " + txtSIUP.Text;
                Session["Text"] = txtSenderName.Text + " - " + txt;
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "colorBox", "var isColorbox = true", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('File not found!')", true);
            }
        }

        protected void ddlBankAccount_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btnBack1_Click(object sender, EventArgs e)
        {
            diverror.Visible = false;
            divstep1.Visible = true;
            divstep2.Visible = false;
            divstep3.Visible = false;
        }

        protected void btnBack2_Click(object sender, EventArgs e)
        {
            diverror.Visible = false;
            divstep1.Visible = false;
            divstep2.Visible = true;
            divstep3.Visible = false;
        }

        protected void ddlRealtime_SelectedIndexChanged(object sender, EventArgs e)
        {
            bindBank();
        }

        protected void ddlPurpose_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btnCheckCity_Click(object sender, EventArgs e)
        {
            hfModalCityOpen.Value = "yes";
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "Pop", "var isModalCity = true", true);
        }

        protected void btnChooseCity_Click(object sender, EventArgs e)
        {
            System.Web.UI.WebControls.Button btn = (System.Web.UI.WebControls.Button)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            hfCityID.Value = ((HiddenField)row.FindControl("hfCityIDModal")).Value;
            hfCityName.Value = ((HiddenField)row.FindControl("hfCityNameModal")).Value;
            txtCity.Text = hfCityName.Value;
            txtFindCity.Text = "";
            gvListCity.DataSource = null;
            gvListCity.DataBind();
            gvListCity.Visible = false;
            hfModalCityOpen.Value = "no";
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "Pop", "var isModalCity = false", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "DoPostBack", "__doPostBack(sender, e)", true);
        }

        protected void btnFindCity_Click(object sender, EventArgs e)
        {
            var data = db.tblM_City.Where(p => p.City.Contains(txtFindCity.Text.Trim())).ToList();
            gvListCity.DataSource = data;
            gvListCity.DataBind();
            gvListCity.Visible = true;
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "Pop", "var isModalCity = true", true);
        }
    }
}