﻿using Newtonsoft.Json;
using OMGLiveDemo.Models;
using OMGLiveDemo.Models.OMG;
using OMGLiveDemo.Scripts;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.Payout
{
    public partial class CreatePayout : System.Web.UI.Page
    {
        OMG omg = new OMG();
        List<Bank> bank = new List<Bank>();
        List<tblM_Beneficiary> benf = new List<tblM_Beneficiary>();
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();
        float totalAmount = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    bind();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        void bind() {
            long agid = Convert.ToInt64(SessionLib.Current.AdminID);
            bank = omg.getListBank("indonesia");
            if (bank != null)
            {
                ddlBank.DataSource = null;
                ddlBank.DataSource = bank;
                ddlBank.DataValueField = "Code";
                ddlBank.DataTextField = "Name";
                ddlBank.DataBind();
            }
            benf = db.tblM_Beneficiary.Where(p => p.AgentID == agid && p.isAgent == 1 && p.isActive == 1).ToList();
            if (benf.Count > 0)
            {
                for (int i = 0; i < benf.Count; i++)
                {
                    benf[i].NameBankAccount = benf[i].Name + " - " + benf[i].Bank + " - " + benf[i].Account;
                }
                ddlBeneficiary.DataSource = null;
                ddlBeneficiary.DataSource = benf;
                ddlBeneficiary.DataValueField = "Account";
                ddlBeneficiary.DataTextField = "NameBankAccount";
                ddlBeneficiary.DataBind();
            }


            var mar = db.tblM_Rate.Where(p => p.RemittanceID == 1).First();
            hfMargin.Value = mar.Margin;

            var cur = omg.getRateMYR2IDR();
            if (cur != null)
            {
                hfRate.Value = cur.Rate;
            }

            List<tblM_Rate> rate = new List<tblM_Rate>();
            rate = omg.getListRateMYRIDR();
            if (rate.Count > 0)
            {
                for (int i = 0; i < rate.Count; i++)
                {
                    rate[i].Rate = (Int32.Parse(hfRate.Value) - Int32.Parse(rate[i].Margin)).ToString();
                    rate[i].NameRate = rate[i].Name + " - Rp " + String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(Int32.Parse(rate[i].Rate))).Replace("Rp", "");
                    if (i == 0)
                        hfRate.Value = rate[i].Rate;
                }
                ddlRate.DataSource = null;
                ddlRate.DataSource = rate;
                ddlRate.DataValueField = "Rate";
                ddlRate.DataTextField = "NameRate";
                ddlRate.DataBind();
            }
        }

        protected void btnProcess_Click(object sender, EventArgs e)
        {
            if (txtName.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Beneficiary Name cannot be empty!";
                return;
            }

            if (txtAccount.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Beneficiary Account cannot be empty!";
                return;
            }

            if (txtAmount.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Amount cannot be empty!";
                return;
            }

            if (txtNotes.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Notes cannot be empty!";
                return;
            }

            if (txtNotes.Text.Length > 17)
            {
                diverror.Visible = true;
                lblError.Text = "Only 17 characters allowed for Notes!";
                return;
            }

            tblT_Payout pay = new tblT_Payout();
            pay.BeneficiaryName = txtName.Text.Trim();
            pay.BeneficiaryAccount = txtAccount.Text.Trim();

            if (ddlBeneficiaryType.SelectedValue == "1")
            {
                pay.BeneficiaryBank = ddlBank.SelectedValue;
            }
            else
            {
                pay.BeneficiaryBank = ddlBank.SelectedValue;
            }

            pay.BankName = ddlBank.SelectedItem.Text.Trim();
            pay.Currency = "MYR";
            pay.Amount = Convert.ToDouble(txtAmount.Text.Trim().Replace(",","."));
            pay.Rate = (Int32.Parse(hfRate.Value) - Int32.Parse(hfMargin.Value)).ToString();
            pay.TotalAmount = Convert.ToDouble(hfTotalAmount.Value.Replace(",", "."));
            if (txtNotes.Text.Trim().Length > 17)
                pay.Notes = txtNotes.Text.Trim().Substring(0, 16);
            else
                pay.Notes = txtNotes.Text.Trim();
            pay.isActive = 1;
            pay.isAgent = 0;
            pay.CreatedBy = Int64.Parse(SessionLib.Current.AdminID);
            pay.CreatedDate = DateTime.Now;
            db.tblT_Payout.Add(pay);
            db.SaveChanges();

            PayoutRequest pr = new PayoutRequest();
            pr.Currency = "MYR";
            pr.AmountIDR = Convert.ToString(pay.TotalAmount);
            pr.Amount = pay.Amount.Value.ToString();
            pr.BeneficiaryName = pay.BeneficiaryName;
            pr.BeneficiaryBank = pay.BeneficiaryBank;
            pr.BeneficiaryAccount = pay.BeneficiaryAccount;
            pr.BeneficiaryEmail = pay.BeneficiaryEmail;
            pr.PartnerReferenceID = pay.PayoutID.ToString();
            pr.Notes = pay.Notes;

            PayoutResponse prs = new PayoutResponse();
            prs = omg.createApprovePayout(pr);

            if (prs != null)
            {
                pay.Reference = prs.ReferenceID;
                pay.Status = prs.Status;
                pay.ModifiedDate = prs.DateModified;
                db.SaveChanges();

                beneficiary();
                Response.Redirect("~/Payout/PayoutHistory");
            }
        }

        protected void ddlBank_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void ddlBeneficiaryType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlBeneficiaryType.SelectedValue == "-1")
            {
                divall.Visible = false;
            }
            if (ddlBeneficiaryType.SelectedValue == "1")
            {
                divall.Visible = true;
                divbeneficiary.Visible = true;
                divvalidate.Visible = false;
                txtName.Enabled = false;
                txtAccount.Enabled = false;
                ddlBank.Enabled = false;
                cbSaveBenfeficiary.Visible = false;
                cbUpdateBeneficiary.Visible = true;

                setBeneficiary();
            }
            else if (ddlBeneficiaryType.SelectedValue == "2")
            {
                divall.Visible = true;
                divbeneficiary.Visible = false;
                divvalidate.Visible = true;
                txtName.Enabled = false;
                txtAccount.Enabled = true;
                ddlBank.Enabled = true;
                cbSaveBenfeficiary.Visible = true;
                cbUpdateBeneficiary.Visible = false;
            }
        }

        protected void btnValidate_Click(object sender, EventArgs e)
        {
            BankAccount ba = new BankAccount();
            ba.AccountNo = txtAccount.Text.Trim();
            ba.BankCode = ddlBank.SelectedValue;
            var acc = omg.validateAccount(ba);
            if (acc != null)
            {
                txtName.Text = acc.AccountName;
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Found " + acc.AccountName + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('The account of "+ ba.AccountNo +" and bank of "+ ddlBank.SelectedItem +" is not found')", true);
            }
        }

        void beneficiary()
        {
            try
            {
                tblM_Beneficiary bnf = new tblM_Beneficiary();
                if (ddlBeneficiaryType.SelectedValue == "1")
                {
                    if (cbUpdateBeneficiary.Checked)
                    {
                        bnf = db.tblM_Beneficiary.Where(p => p.Account == txtAccount.Text.Trim() && p.BankCode == ddlBank.SelectedValue).FirstOrDefault();
                        string paybef = JsonConvert.SerializeObject(benf);
                        bnf.Alias = txtAlias.Text;
                        bnf.Phone = txtPhone.Text;
                        bnf.Email = txtEmail.Text;
                        bnf.UpdatedDate = DateTime.Now;
                        bnf.UpdatedBy = Int32.Parse(SessionLib.Current.AdminID);
                        string payaft = JsonConvert.SerializeObject(benf);

                        tblH_Beneficiary_Update hbnf = new tblH_Beneficiary_Update();
                        hbnf.AdminID = Int32.Parse(SessionLib.Current.AdminID);
                        hbnf.BeneficiaryID = bnf.BeneficiaryID;
                        hbnf.PayloadBefore = paybef;
                        hbnf.PayloadAfter = payaft;
                        hbnf.UpdatedDate = bnf.UpdatedDate;
                        db.tblH_Beneficiary_Update.Add(hbnf);

                        db.SaveChanges();
                    }
                }
                else if (ddlBeneficiaryType.SelectedValue == "2")
                {
                    if (cbSaveBenfeficiary.Checked)
                    {
                        bnf = db.tblM_Beneficiary.Where(p => p.Account == txtAccount.Text.Trim() && p.BankCode == ddlBank.SelectedValue).FirstOrDefault();
                        if (bnf == null)
                        {
                            bnf.Name = txtName.Text.Trim();
                            bnf.Account = txtAccount.Text;
                            bnf.Bank = ddlBank.SelectedItem.Text;
                            bnf.BankCode = ddlBank.SelectedValue;
                            bnf.Alias = txtAlias.Text.Trim();
                            bnf.Phone = txtPhone.Text.Trim();
                            bnf.Email = txtEmail.Text.Trim();
                            bnf.isActive = 1;
                            bnf.CreatedDate = DateTime.Now;
                            bnf.CreatedBy = Int32.Parse(SessionLib.Current.AdminID);
                            db.tblM_Beneficiary.Add(bnf);
                            db.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        protected void txtAmount_TextChanged(object sender, EventArgs e)
        {
            countAmount();
        }

        void countAmount()
        {
            if (txtAmount.Text != "")
            {
                if (ddlAmountType.SelectedValue == "1")
                {
                    float amount = float.Parse(txtAmount.Text, CultureInfo.InvariantCulture.NumberFormat);
                    totalAmount = amount * (float.Parse(hfRate.Value) + float.Parse(hfMargin.Value));
                    hfTotalAmount.Value = totalAmount.ToString();
                    lblTotalAmount.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToDouble(totalAmount)).Replace("Rp", "Rp ");
                }
                else if (ddlAmountType.SelectedValue == "2")
                {
                    float amount = float.Parse(txtAmount.Text, CultureInfo.InvariantCulture.NumberFormat);
                    totalAmount = amount / (float.Parse(hfRate.Value) + float.Parse(hfMargin.Value));
                    hfTotalAmount.Value = totalAmount.ToString();
                    lblTotalAmount.Text = String.Format(new CultureInfo("ms-MY"), "{0:c}", Convert.ToDouble(totalAmount)).Replace("RM", "RM ");
                }
            }
        }

        protected void btnError_ServerClick(object sender, EventArgs e)
        {
            diverror.Visible = false;
        }

        protected void ddlBeneficiary_SelectedIndexChanged(object sender, EventArgs e)
        {
            setBeneficiary();
        }

        void setBeneficiary()
        {
            string account = ddlBeneficiary.SelectedValue;
            var bf = db.tblM_Beneficiary.Where(p => p.Account == account).FirstOrDefault();
            if (bf != null)
            {
                txtName.Text = bf.Name;
                txtAccount.Text = bf.Account;
                ddlBank.Text = bf.BankCode;
                txtAlias.Text = bf.Alias;
                txtPhone.Text = bf.Phone;
                txtEmail.Text = bf.Email;
            }
        }

        protected void linkProcess_ServerClick(object sender, EventArgs e)
        {
            if (txtName.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Beneficiary Name cannot be empty!";
                return;
            }

            if (txtAccount.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Beneficiary Account cannot be empty!";
                return;
            }

            if (ddlAmountType.SelectedValue == "-1")
            {
                diverror.Visible = true;
                lblError.Text = "You should choose Amount Type first!";
                return;
            }

            if (txtAmount.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Amount cannot be empty!";
                return;
            }

            if (txtNotes.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Notes cannot be empty!";
                return;
            }

            if (txtNotes.Text.Length > 17)
            {
                diverror.Visible = true;
                lblError.Text = "Only 17 characters allowed for Notes!";
                return;
            }

            tblT_Payout pay = new tblT_Payout();
            pay.BeneficiaryName = txtName.Text.Trim();
            pay.BeneficiaryAccount = txtAccount.Text.Trim();

            if (ddlBeneficiaryType.SelectedValue == "1")
            {
                pay.BeneficiaryBank = hfBank.Value;
            }
            else
            {
                pay.BeneficiaryBank = ddlBank.SelectedValue;
            }

            pay.Currency = "MYR";
            pay.Rate = (float.Parse(hfRate.Value)).ToString();
            pay.RateGiven = pay.Rate;
            if (ddlAmountType.SelectedValue == "1")
            {
                pay.Amount = Convert.ToDouble(txtAmount.Text.Trim().Replace(",", "."));
                pay.TotalAmount = Convert.ToDouble(hfTotalAmount.Value.Replace(",", "."));
            }
            else if (ddlAmountType.SelectedValue == "2")
            {
                pay.TotalAmount = Convert.ToDouble(txtAmount.Text.Trim().Replace(",", "."));
                pay.Amount = Convert.ToDouble(hfTotalAmount.Value.Replace(",", "."));
            }
            pay.Notes = txtNotes.Text.Trim();
            pay.isActive = 1;
            pay.CreatedDate = DateTime.Now;
            db.tblT_Payout.Add(pay);
            db.SaveChanges();

            PayoutRequest pr = new PayoutRequest();
            pr.Currency = "MYR";
            pr.AmountIDR = Convert.ToString(pay.TotalAmount);
            pr.Amount = pay.Amount.Value.ToString();
            pr.BeneficiaryName = pay.BeneficiaryName;
            pr.BeneficiaryBank = pay.BeneficiaryBank;
            pr.BeneficiaryAccount = pay.BeneficiaryAccount;
            pr.BeneficiaryEmail = pay.BeneficiaryEmail;
            pr.PartnerReferenceID = pay.PayoutID.ToString();
            pr.Notes = pay.Notes;

            var outp = omg.createPayout(pr);

            if (outp.message == "success")
            {
                PayoutResponse prs = new PayoutResponse();
                prs = JsonConvert.DeserializeObject<PayoutResponse>(outp.data.ToString());
                if (prs != null)
                {
                    pay.Reference = prs.ReferenceID;
                    pay.Status = prs.Status;
                    pay.ModifiedDate = prs.DateModified;
                    db.SaveChanges();

                    beneficiary();

                    Response.Redirect("~/Payout/PayoutHistory");
                }
            }
            else
            {
                lblError.Text = outp.message;
                diverror.Visible = true;
                return;
            }
        }

        protected void ddlAmountType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlAmountType.SelectedValue == "1")
            {
                lblAmount.Text = "Amount (in MYR) *";
            }
            else if (ddlAmountType.SelectedValue == "2")
            {
                lblAmount.Text = "Amount (in IDR) *";
            }
            countAmount();
        }

        protected void ddlRate_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfRate.Value = ddlRate.SelectedValue;
            countAmount();
        }
    }
}