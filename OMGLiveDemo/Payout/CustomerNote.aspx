﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustomerNote.aspx.cs" Inherits="OMGLiveDemo.Payout.CustomerNote" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style3 {
            width: 1px;
        }

        .auto-style4 {
            width: 100px;
            height: 30px;
        }

        .auto-style5 {
            width: 169px;
        }

        .auto-style6 {
            width: 18px;
        }

        .auto-style7 {
            width: 101px;
        }

        .auto-style8 {
            width: 300px;
            height: 30px;
            
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div style="margin: 40px;">  <%--border:2px solid black--%>
            <img src='<%= ResolveUrl("~/Content/images/mm-logo-malindo.png")%>' alt="..." width="600px" height="140px"/>
            <br />
            <br />
            <br />
            <asp:Label runat="server" ID="lblReprintedDate" Text="Reprinted on " />
            <br />
            <br />
            <p>Permintaan kirim uang anda telah kami terima dengan detail permintaan transaksi :</p>
            <br />
            <br />
            <table>
                <tr>
                    <td class="auto-style8"><span style="font-size: 20px;">Status </span></td>
                    <td class="auto-style4"><span style="font-size: 20px; font-weight: bold">: </span></td>
                    <td>
                        <span style="font-size: 20px; font-weight: bold">
                            <asp:Label runat="server" ID="lblStatus" Text="xxxxxx" /></span>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style8"><span style="font-size: 20px;">Nomor Referensi </span></td>
                    <td class="auto-style4"><span style="font-size: 20px; font-weight: bold">: </span></td>
                    <td>
                        <span style="font-size: 20px; font-weight: bold">
                            <asp:Label runat="server" ID="lblReference" Text="xxxxxx" /></span>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style8"><span style="font-size: 20px;">Nama Pengirim </span></td>
                    <td class="auto-style4"><span style="font-size: 20px; font-weight: bold">: </span></td>
                    <td>
                        <span style="font-size: 20px; font-weight: bold">
                            <asp:Label runat="server" ID="lblSenderName" Text="xxxxxx" /></span>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style8"><span style="font-size: 20px;">Nama Penerima </span></td>
                    <td class="auto-style4"><span style="font-size: 20px; font-weight: bold">: </span></td>
                    <td>
                        <span style="font-size: 20px; font-weight: bold">
                            <asp:Label runat="server" ID="lblBenfName" Text="xxxxxx" /></span>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style8"><span style="font-size: 20px;">Bank Penerima </span></td>
                    <td class="auto-style4"><span style="font-size: 20px; font-weight: bold">: </span></td>
                    <td>
                        <span style="font-size: 20px; font-weight: bold">
                            <asp:Label runat="server" ID="lblBenfBank" Text="xxxxxx" /></span>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style8"><span style="font-size: 20px;">Nomor Akaun Penerima </span></td>
                    <td class="auto-style4"><span style="font-size: 20px; font-weight: bold">: </span></td>
                    <td>
                        <span style="font-size: 20px; font-weight: bold">
                            <asp:Label runat="server" ID="lblBenfAccount" Text="xxxxxx" /></span>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style8"><span style="font-size: 20px;">Tanggal Transaksi </span></td>
                    <td class="auto-style4"><span style="font-size: 20px; font-weight: bold">: </span></td>
                    <td>
                        <span style="font-size: 20px; font-weight: bold">
                            <asp:Label runat="server" ID="lblTransactionDate" Text="xxxxxx" /></span>
                    </td>
                </tr>
            </table>

            <div style="margin-top:20px"/>
            <table>
                <tr>
                    <td class="auto-style8"><span style="font-size: 20px;">Jumlah </span></td>
                    <td class="auto-style4"><span style="font-size: 20px; font-weight: bold">: </span></td>
                    <td>
                        <span style="font-size: 20px; font-weight: bold">
                            <asp:Label runat="server" ID="lblAmount" Text="xxxxxx" /></span>
                    </td>
                </tr>
            </table>
            <br />
            <br />
            <span style="font-size: 20px; font-weight: bold"> Harap catat no. referensi anda untuk digunakan apabila ada pertanyaan atau masalah.</span>
            <br />
            <br />
            <%--<span style="font-size: 20px;">Note : </span><span style="font-size: 20px; font-weight: bold"> This receipt is computer generated and no signature is required.</span>--%>
            <span style="font-size: 20px;">Catatan : </span><span style="font-size: 20px; font-weight: bold"> Tanda terima ini dibuat oleh komputer dan tidak perlu tanda tangan.</span>
        </div>
    </form>
</body>
</html>
