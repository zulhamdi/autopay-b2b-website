﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using OMGLiveDemo.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.Payout
{
    public partial class PayoutHistory : System.Web.UI.Page
    {
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();
        List<tblM_Sender> listSender;
        List<tblM_Beneficiary> listBeneficiary;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    BindGV();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        void BindGV()
        {
            if (Session["Payout"] != null)
            {
                lblAlertSuccess.Text = Session["Payout"].ToString();
                divsuccess.Visible = true;
                Session["Payout"] = null;
            }

            if (Session["PayoutHistoryStatus"] != null)
            {
                ddlStatus.SelectedValue = Session["PayoutHistoryStatus"].ToString();
                Session["PayoutHistoryStatus"] = null;
            }

            var rid = Convert.ToInt64(ddlRemittanceLine.SelectedValue);
            var status = ddlStatus.SelectedValue;
            if (SessionLib.Current.Role != Roles.Approver)
                divfilterbydate.Visible = true;

            var id = Convert.ToInt64(SessionLib.Current.AdminID);
            checkSort();
            var date = new DateTime();
            var datelast = new DateTime();
            if (divsingledate.Visible)
            {
                if (datepurchased.Text != "")
                {
                    date = Convert.ToDateTime(datepurchased.Text.ToString().Trim());
                    datelast = Convert.ToDateTime(datepurchased.Text.ToString().Trim()).AddHours(23).AddMinutes(59).AddSeconds(59);
                }
            }
            else if (divdaterange.Visible)
            {
                if (startdate.Text != "" && enddate.Text != "")
                {
                    date = Convert.ToDateTime(startdate.Text.ToString().Trim());
                    datelast = Convert.ToDateTime(enddate.Text.ToString().Trim()).AddHours(23).AddMinutes(59).AddSeconds(59);
                }
            }

            List<vw_Payout_Admin> dt = new List<vw_Payout_Admin>();
            if (SessionLib.Current.Role == Roles.Agent)
            {
                divstatus.Visible = true;
                if (status == "pending")
                {
                    if (ddlFilterByDate.SelectedValue == "all")
                        dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.isAgent == 1 && p.CreatedBy == id && (p.Status == "created" || p.Status == "queued" || p.Status == "approved")).OrderByDescending(p => p.PayoutID).ToList();
                    else
                    {
                        if (datepurchased.Text != "" || (startdate.Text != "" && enddate.Text != ""))
                            dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.isAgent == 1 && p.CreatedBy == id && (p.Status == "created" || p.Status == "queued" || p.Status == "approved") && p.CreatedDate >= date && p.CreatedDate <= datelast).OrderByDescending(p => p.PayoutID).ToList();
                        else
                            dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.isAgent == 1 && p.CreatedBy == id && (p.Status == "created" || p.Status == "queued" || p.Status == "approved")).OrderByDescending(p => p.PayoutID).ToList();
                    }
                }
                else if (status == "all")
                {
                    if (ddlFilterByDate.SelectedValue == "all")
                        dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.isAgent == 1 && p.CreatedBy == id).OrderByDescending(p => p.PayoutID).ToList();
                    else
                    {
                        if (datepurchased.Text != "" || (startdate.Text != "" && enddate.Text != ""))
                            dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.isAgent == 1 && p.CreatedBy == id && p.CreatedDate >= date && p.CreatedDate <= datelast).OrderByDescending(p => p.PayoutID).ToList();
                        else
                            dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.isAgent == 1 && p.CreatedBy == id).OrderByDescending(p => p.PayoutID).ToList();
                    }
                }
                else
                {
                    if (ddlFilterByDate.SelectedValue == "all")
                        dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.isAgent == 1 && p.CreatedBy == id && p.Status == status).OrderByDescending(p => p.PayoutID).ToList();
                    else
                    {
                        if (datepurchased.Text != "" || (startdate.Text != "" && enddate.Text != ""))
                            dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.isAgent == 1 && p.CreatedBy == id && p.Status == status && p.CreatedDate >= date && p.CreatedDate <= datelast).OrderByDescending(p => p.PayoutID).ToList();
                        else
                            dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.isAgent == 1 && p.CreatedBy == id && p.Status == status).OrderByDescending(p => p.PayoutID).ToList();
                    }
                }
            }
            else if (SessionLib.Current.Role == Roles.MasterArea)
            {
                divstatus.Visible = true;
                if (status == "pending")
                {
                    if (ddlFilterByDate.SelectedValue == "all")
                        dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.isAgent == 2 && p.CreatedBy == id && (p.Status == "created" || p.Status == "queued" || p.Status == "approved")).OrderByDescending(p => p.PayoutID).ToList();
                    else
                    {
                        if (datepurchased.Text != "" || (startdate.Text != "" && enddate.Text != ""))
                            dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.isAgent == 2 && p.CreatedBy == id && (p.Status == "created" || p.Status == "queued" || p.Status == "approved") && p.CreatedDate >= date && p.CreatedDate <= datelast).OrderByDescending(p => p.PayoutID).ToList();
                        else
                            dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.isAgent == 2 && p.CreatedBy == id && (p.Status == "created" || p.Status == "queued" || p.Status == "approved")).OrderByDescending(p => p.PayoutID).ToList();
                    }
                }
                else if (status == "all")
                {
                    if (ddlFilterByDate.SelectedValue == "all")
                        dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.isAgent == 2 && p.CreatedBy == id).OrderByDescending(p => p.PayoutID).ToList();
                    else
                    {
                        if (datepurchased.Text != "" || (startdate.Text != "" && enddate.Text != ""))
                            dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.isAgent == 2 && p.CreatedBy == id && p.CreatedDate >= date && p.CreatedDate <= datelast).OrderByDescending(p => p.PayoutID).ToList();
                        else
                            dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.isAgent == 2 && p.CreatedBy == id).OrderByDescending(p => p.PayoutID).ToList();
                    }
                }
                else
                {
                    if (ddlFilterByDate.SelectedValue == "all")
                        dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.isAgent == 2 && p.CreatedBy == id && p.Status == status).OrderByDescending(p => p.PayoutID).ToList();
                    else
                    {
                        if (datepurchased.Text != "" || (startdate.Text != "" && enddate.Text != ""))
                            dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.isAgent == 2 && p.CreatedBy == id && p.Status == status && p.CreatedDate >= date && p.CreatedDate <= datelast).OrderByDescending(p => p.PayoutID).ToList();
                        else
                            dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.isAgent == 2 && p.CreatedBy == id && p.Status == status).OrderByDescending(p => p.PayoutID).ToList();
                    }
                }
            }
            //else if (SessionLib.Current.Role == Roles.Approver)
            //{
            //    //dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.ApprovedBy == id || p.RejectedBy == id).OrderByDescending(p => p.PayoutID).ToList();
            //    dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid).OrderByDescending(p => p.PayoutID).ToList();
            //}
            //else if (SessionLib.Current.Role == Roles.Operator)
            else
            {
                divstatus.Visible = true;
                if (status == "pending")
                {
                    if (ddlFilterByDate.SelectedValue == "all")
                    {
                        //dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.isAgent == 0 && p.CreatedBy == id && (p.Status == "created" || p.Status == "queued" || p.Status == "approved")).OrderByDescending(p => p.PayoutID).ToList();
                        //dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && (p.Status == "created" || p.Status == "queued" || p.Status == "approved")).OrderByDescending(p => p.PayoutID).ToList();

                        //Sender
                        if (string.IsNullOrEmpty(txtSender.Text))
                        {
                            //all sender

                            //Bene
                            if (string.IsNullOrEmpty(txtBene.Text))
                            {
                                //all bene
                                dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && (p.Status == "created" || p.Status == "queued" || p.Status == "approved")).OrderByDescending(p => p.PayoutID).ToList();
                            }
                            else
                            {
                                //selected bene
                                dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.BeneficiaryName.Contains(txtBene.Text) && (p.Status == "created" || p.Status == "queued" || p.Status == "approved")).OrderByDescending(p => p.PayoutID).ToList();
                            }
                        }
                        else
                        {
                            //selected sender

                            //Bene
                            if (string.IsNullOrEmpty(txtBene.Text))
                            {
                                //all bene
                                dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.SenderName.Contains(txtSender.Text) && (p.Status == "created" || p.Status == "queued" || p.Status == "approved")).OrderByDescending(p => p.PayoutID).ToList();
                            }
                            else
                            {
                                //selected bene
                                dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.SenderName.Contains(txtSender.Text) && p.BeneficiaryName.Contains(txtBene.Text) && (p.Status == "created" || p.Status == "queued" || p.Status == "approved")).OrderByDescending(p => p.PayoutID).ToList();
                            }
                        }
                    }
                    else
                    {
                        if (datepurchased.Text != "" || (startdate.Text != "" && enddate.Text != ""))
                        {
                            //dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.isAgent == 0 && p.CreatedBy == id && (p.Status == "created" || p.Status == "queued" || p.Status == "approved") && p.CreatedDate >= date && p.CreatedDate <= datelast).OrderByDescending(p => p.PayoutID).ToList();
                            //dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && (p.Status == "created" || p.Status == "queued" || p.Status == "approved") && p.CreatedDate >= date && p.CreatedDate <= datelast).OrderByDescending(p => p.PayoutID).ToList();

                            //Sender
                            if (string.IsNullOrEmpty(txtSender.Text))
                            {
                                //all sender

                                //Bene
                                if (string.IsNullOrEmpty(txtBene.Text))
                                {
                                    //all bene
                                    dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && (p.Status == "created" || p.Status == "queued" || p.Status == "approved") && p.CreatedDate >= date && p.CreatedDate <= datelast).OrderByDescending(p => p.PayoutID).ToList();
                                }
                                else
                                {
                                    //selected bene
                                    dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.BeneficiaryName.Contains(txtBene.Text) && (p.Status == "created" || p.Status == "queued" || p.Status == "approved") && p.CreatedDate >= date && p.CreatedDate <= datelast).OrderByDescending(p => p.PayoutID).ToList();
                                }
                            }
                            else
                            {
                                //selected sender

                                //Bene
                                if (string.IsNullOrEmpty(txtBene.Text))
                                {
                                    //all bene
                                    dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.SenderName.Contains(txtSender.Text) && (p.Status == "created" || p.Status == "queued" || p.Status == "approved") && p.CreatedDate >= date && p.CreatedDate <= datelast).OrderByDescending(p => p.PayoutID).ToList();
                                }
                                else
                                {
                                    //selected bene
                                    dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.SenderName.Contains(txtSender.Text) && p.BeneficiaryName.Contains(txtBene.Text) && (p.Status == "created" || p.Status == "queued" || p.Status == "approved") && p.CreatedDate >= date && p.CreatedDate <= datelast).OrderByDescending(p => p.PayoutID).ToList();
                                }
                            }
                        }
                        else
                        {
                            //dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.isAgent == 0 && p.CreatedBy == id && (p.Status == "created" || p.Status == "queued" || p.Status == "approved")).OrderByDescending(p => p.PayoutID).ToList();
                            //dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && (p.Status == "created" || p.Status == "queued" || p.Status == "approved")).OrderByDescending(p => p.PayoutID).ToList();

                            //Sender
                            if (string.IsNullOrEmpty(txtSender.Text))
                            {
                                //all sender

                                //Bene
                                if (string.IsNullOrEmpty(txtBene.Text))
                                {
                                    //all bene
                                    dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && (p.Status == "created" || p.Status == "queued" || p.Status == "approved")).OrderByDescending(p => p.PayoutID).ToList();
                                }
                                else
                                {
                                    //selected bene
                                    dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.BeneficiaryName.Contains(txtBene.Text) && (p.Status == "created" || p.Status == "queued" || p.Status == "approved")).OrderByDescending(p => p.PayoutID).ToList();
                                }
                            }
                            else
                            {
                                //selected sender

                                //Bene
                                if (string.IsNullOrEmpty(txtBene.Text))
                                {
                                    //all bene
                                    dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.SenderName.Contains(txtSender.Text) && (p.Status == "created" || p.Status == "queued" || p.Status == "approved")).OrderByDescending(p => p.PayoutID).ToList();
                                }
                                else
                                {
                                    //selected bene
                                    dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.SenderName.Contains(txtSender.Text) && p.BeneficiaryName.Contains(txtBene.Text) && (p.Status == "created" || p.Status == "queued" || p.Status == "approved")).OrderByDescending(p => p.PayoutID).ToList();
                                }
                            }
                        }
                    }
                }
                else if (status == "all")
                {
                    if (ddlFilterByDate.SelectedValue == "all")
                    {
                        //dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.isAgent == 0 && p.CreatedBy == id && (p.Status == "created" || p.Status == "queued" || p.Status == "approved")).OrderByDescending(p => p.PayoutID).ToList();
                        //dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && (p.Status == "created" || p.Status == "queued" || p.Status == "approved")).OrderByDescending(p => p.PayoutID).ToList();

                        //Sender
                        if (string.IsNullOrEmpty(txtSender.Text))
                        {
                            //all sender

                            //Bene
                            if (string.IsNullOrEmpty(txtBene.Text))
                            {
                                //all bene
                                dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid).OrderByDescending(p => p.PayoutID).ToList();
                            }
                            else
                            {
                                //selected bene
                                dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.BeneficiaryName.Contains(txtBene.Text)).OrderByDescending(p => p.PayoutID).ToList();
                            }
                        }
                        else
                        {
                            //selected sender

                            //Bene
                            if (string.IsNullOrEmpty(txtBene.Text))
                            {
                                //all bene
                                dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.SenderName.Contains(txtSender.Text)).OrderByDescending(p => p.PayoutID).ToList();
                            }
                            else
                            {
                                //selected bene
                                dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.SenderName.Contains(txtSender.Text) && p.BeneficiaryName.Contains(txtBene.Text)).OrderByDescending(p => p.PayoutID).ToList();
                            }
                        }
                    }
                    else
                    {
                        if (datepurchased.Text != "" || (startdate.Text != "" && enddate.Text != ""))
                        {
                            //dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.isAgent == 0 && p.CreatedBy == id && (p.Status == "created" || p.Status == "queued" || p.Status == "approved") && p.CreatedDate >= date && p.CreatedDate <= datelast).OrderByDescending(p => p.PayoutID).ToList();
                            //dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && (p.Status == "created" || p.Status == "queued" || p.Status == "approved") && p.CreatedDate >= date && p.CreatedDate <= datelast).OrderByDescending(p => p.PayoutID).ToList();

                            //Sender
                            if (string.IsNullOrEmpty(txtSender.Text))
                            {
                                //all sender

                                //Bene
                                if (string.IsNullOrEmpty(txtBene.Text))
                                {
                                    //all bene
                                    dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.CreatedDate >= date && p.CreatedDate <= datelast).OrderByDescending(p => p.PayoutID).ToList();
                                }
                                else
                                {
                                    //selected bene
                                    dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.BeneficiaryName.Contains(txtBene.Text) && p.CreatedDate >= date && p.CreatedDate <= datelast).OrderByDescending(p => p.PayoutID).ToList();
                                }
                            }
                            else
                            {
                                //selected sender

                                //Bene
                                if (string.IsNullOrEmpty(txtBene.Text))
                                {
                                    //all bene
                                    dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.SenderName.Contains(txtSender.Text) && p.CreatedDate >= date && p.CreatedDate <= datelast).OrderByDescending(p => p.PayoutID).ToList();
                                }
                                else
                                {
                                    //selected bene
                                    dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.SenderName.Contains(txtSender.Text) && p.BeneficiaryName.Contains(txtBene.Text) && p.CreatedDate >= date && p.CreatedDate <= datelast).OrderByDescending(p => p.PayoutID).ToList();
                                }
                            }
                        }
                        else
                        {
                            //dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.isAgent == 0 && p.CreatedBy == id && (p.Status == "created" || p.Status == "queued" || p.Status == "approved")).OrderByDescending(p => p.PayoutID).ToList();
                            //dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && (p.Status == "created" || p.Status == "queued" || p.Status == "approved")).OrderByDescending(p => p.PayoutID).ToList();

                            //Sender
                            if (string.IsNullOrEmpty(txtSender.Text))
                            {
                                //all sender

                                //Bene
                                if (string.IsNullOrEmpty(txtBene.Text))
                                {
                                    //all bene
                                    dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid).OrderByDescending(p => p.PayoutID).ToList();
                                }
                                else
                                {
                                    //selected bene
                                    dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.BeneficiaryName.Contains(txtBene.Text)).OrderByDescending(p => p.PayoutID).ToList();
                                }
                            }
                            else
                            {
                                //selected sender

                                //Bene
                                if (string.IsNullOrEmpty(txtBene.Text))
                                {
                                    //all bene
                                    dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.SenderName.Contains(txtSender.Text)).OrderByDescending(p => p.PayoutID).ToList();
                                }
                                else
                                {
                                    //selected bene
                                    dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.SenderName.Contains(txtSender.Text) && p.BeneficiaryName.Contains(txtBene.Text)).OrderByDescending(p => p.PayoutID).ToList();
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (ddlFilterByDate.SelectedValue == "all")
                    {
                        //dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.isAgent == 0 && p.CreatedBy == id && (p.Status == "created" || p.Status == "queued" || p.Status == "approved")).OrderByDescending(p => p.PayoutID).ToList();
                        //dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && (p.Status == "created" || p.Status == "queued" || p.Status == "approved")).OrderByDescending(p => p.PayoutID).ToList();

                        //Sender
                        if (string.IsNullOrEmpty(txtSender.Text))
                        {
                            //all sender

                            //Bene
                            if (string.IsNullOrEmpty(txtBene.Text))
                            {
                                //all bene
                                dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.Status == status).OrderByDescending(p => p.PayoutID).ToList();
                            }
                            else
                            {
                                //selected bene
                                dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.BeneficiaryName.Contains(txtBene.Text) && p.Status == status).OrderByDescending(p => p.PayoutID).ToList();
                            }
                        }
                        else
                        {
                            //selected sender

                            //Bene
                            if (string.IsNullOrEmpty(txtBene.Text))
                            {
                                //all bene
                                dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.SenderName.Contains(txtSender.Text) && p.Status == status).OrderByDescending(p => p.PayoutID).ToList();
                            }
                            else
                            {
                                //selected bene
                                dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.SenderName.Contains(txtSender.Text) && p.BeneficiaryName.Contains(txtBene.Text) && p.Status == status).OrderByDescending(p => p.PayoutID).ToList();
                            }
                        }
                    }
                    else
                    {
                        if (datepurchased.Text != "" || (startdate.Text != "" && enddate.Text != ""))
                        {
                            //dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.isAgent == 0 && p.CreatedBy == id && (p.Status == "created" || p.Status == "queued" || p.Status == "approved") && p.CreatedDate >= date && p.CreatedDate <= datelast).OrderByDescending(p => p.PayoutID).ToList();
                            //dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && (p.Status == "created" || p.Status == "queued" || p.Status == "approved") && p.CreatedDate >= date && p.CreatedDate <= datelast).OrderByDescending(p => p.PayoutID).ToList();

                            //Sender
                            if (string.IsNullOrEmpty(txtSender.Text))
                            {
                                //all sender

                                //Bene
                                if (string.IsNullOrEmpty(txtBene.Text))
                                {
                                    //all bene
                                    dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.CreatedDate >= date && p.CreatedDate <= datelast && p.Status == status).OrderByDescending(p => p.PayoutID).ToList();
                                }
                                else
                                {
                                    //selected bene
                                    dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.BeneficiaryName.Contains(txtBene.Text) && p.CreatedDate >= date && p.CreatedDate <= datelast && p.Status == status).OrderByDescending(p => p.PayoutID).ToList();
                                }
                            }
                            else
                            {
                                //selected sender

                                //Bene
                                if (string.IsNullOrEmpty(txtBene.Text))
                                {
                                    //all bene
                                    dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.SenderName.Contains(txtSender.Text) && p.CreatedDate >= date && p.CreatedDate <= datelast && p.Status == status).OrderByDescending(p => p.PayoutID).ToList();
                                }
                                else
                                {
                                    //selected bene
                                    dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.SenderName.Contains(txtSender.Text) && p.BeneficiaryName.Contains(txtBene.Text) && p.CreatedDate >= date && p.CreatedDate <= datelast && p.Status == status).OrderByDescending(p => p.PayoutID).ToList();
                                }
                            }
                        }
                        else
                        {
                            //dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.isAgent == 0 && p.CreatedBy == id && (p.Status == "created" || p.Status == "queued" || p.Status == "approved")).OrderByDescending(p => p.PayoutID).ToList();
                            //dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && (p.Status == "created" || p.Status == "queued" || p.Status == "approved")).OrderByDescending(p => p.PayoutID).ToList();

                            //Sender
                            if (string.IsNullOrEmpty(txtSender.Text))
                            {
                                //all sender

                                //Bene
                                if (string.IsNullOrEmpty(txtBene.Text))
                                {
                                    //all bene
                                    dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.Status == status).OrderByDescending(p => p.PayoutID).ToList();
                                }
                                else
                                {
                                    //selected bene
                                    dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.BeneficiaryName.Contains(txtBene.Text) && p.Status == status).OrderByDescending(p => p.PayoutID).ToList();
                                }
                            }
                            else
                            {
                                //selected sender

                                //Bene
                                if (string.IsNullOrEmpty(txtBene.Text))
                                {
                                    //all bene
                                    dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.SenderName.Contains(txtSender.Text) && p.Status == status).OrderByDescending(p => p.PayoutID).ToList();
                                }
                                else
                                {
                                    //selected bene
                                    dt = db.vw_Payout_Admin.Where(p => p.RemittanceID == rid && p.SenderName.Contains(txtSender.Text) && p.BeneficiaryName.Contains(txtBene.Text) && p.Status == status).OrderByDescending(p => p.PayoutID).ToList();
                                }
                            }
                        }
                    }
                }
            }

            if (dt.Count > 0)
            {
                for (int i = 0; i < dt.Count; i++)
                {
                    if (dt[i].RemittanceID == 1)
                    {
                        dt[i].AmountText = "MYR " + String.Format(new CultureInfo("ms-MY"), "{0:n}", dt[i].Amount.Value);
                        if (dt[i].isAgent == 2)
                            dt[i].AdminFee = "IDR " + String.Format(new CultureInfo("id-ID"), "{0:n}", float.Parse(dt[i].AdminFee));
                        else
                            dt[i].AdminFee = "MYR " + dt[i].AdminFee;
                        dt[i].RateGivenText = "IDR " + String.Format(new CultureInfo("id-ID"), "{0:n}", float.Parse(dt[i].RateGiven));
                        dt[i].RateText = "IDR " + String.Format(new CultureInfo("id-ID"), "{0:n}", float.Parse(dt[i].Rate));
                        dt[i].TotalAmountText = "MYR " + String.Format(new CultureInfo("ms-MY"), "{0:n}", dt[i].TotalAmount);
                        dt[i].TotalTransferText = "IDR " + String.Format(new CultureInfo("id-ID"), "{0:n}", dt[i].TotalTransfer);

                        lblTotalAmount.Text = "MYR " + String.Format(new CultureInfo("ms-MY"), "{0:n}", dt.Select(p => p.Amount).Sum());
                        lblTotalTransfer.Text = "IDR " + String.Format(new CultureInfo("id-ID"), "{0:n}", dt.Select(p => p.TotalTransfer).Sum());
                    }
                    else if (dt[i].RemittanceID == 2)
                    {
                        dt[i].AmountText = "IDR " + String.Format(new CultureInfo("id-ID"), "{0:n}", dt[i].Amount.Value);
                        if (dt[i].isAgent == 2)
                            dt[i].AdminFee = "MYR " + String.Format(new CultureInfo("ms-MY"), "{0:n}", float.Parse(dt[i].AdminFee));
                        else
                            dt[i].AdminFee = "IDR " + dt[i].AdminFee;
                        dt[i].RateGivenText = "IDR " + String.Format(new CultureInfo("id-ID"), "{0:n}", float.Parse(dt[i].RateGiven));
                        dt[i].RateText = "IDR " + String.Format(new CultureInfo("id-ID"), "{0:n}", float.Parse(dt[i].Rate));
                        dt[i].TotalAmountText = "IDR " + String.Format(new CultureInfo("id-ID"), "{0:n}", dt[i].TotalAmount);
                        dt[i].TotalTransferText = "MYR " + String.Format(new CultureInfo("ms-MY"), "{0:n}", dt[i].TotalTransfer);

                        lblTotalAmount.Text = "IDR " + String.Format(new CultureInfo("id-ID"), "{0:n}", dt.Select(p => p.Amount).Sum());
                        lblTotalTransfer.Text = "MYR " + String.Format(new CultureInfo("ms-MY"), "{0:n}", dt.Select(p => p.TotalTransfer).Sum());
                    }
                }
            }
            else
            {
                lblTotalAmount.Text = "IDR 0,00";
                lblTotalTransfer.Text = "MYR 0.00";
            }


            gvListItem.DataSource = dt;
            gvListItem.DataBind();

            checkDownload();
        }

        void checkDownload()
        {
            if (gvListItem.Rows.Count != 0)
                btnDownload.Visible = true;
            else
                btnDownload.Visible = false;
        }

        protected void btnReceipt_Click(object sender, EventArgs e)
        {
            System.Web.UI.WebControls.Button btn = (System.Web.UI.WebControls.Button)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            Session["PayoutID"] = ((HiddenField)row.FindControl("hfPayoutID")).Value;
            //Response.Redirect("EditCurrencyRate");
            Page.ClientScript.RegisterStartupScript(this.GetType(), "OpenWindow", "window.open('/Payout/Receipt','_newtab');", true);
        }

        protected void btnAlertSuccess_ServerClick(object sender, EventArgs e)
        {
            divsuccess.Visible = false;
        }

        protected void btnALertFailed_ServerClick(object sender, EventArgs e)
        {
            divfailed.Visible = false;
        }

        protected Boolean IsCompleted(string status)
        {
            if (status == "completed")
                return true;
            else
                return false;
        }

        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGV();
        }

        protected void ddlFilterByDate_SelectedIndexChanged(object sender, EventArgs e)
        {
            checkSort();
        }

        void checkSort()
        {
            if (ddlFilterByDate.SelectedValue == "all")
            {
                divsingledate.Visible = false;
                datepurchased.Text = "";
                divdaterange.Visible = false;
                startdate.Text = "";
                enddate.Text = "";
            }
            else if (ddlFilterByDate.SelectedValue == "date")
            {
                divsingledate.Visible = true;
                divdaterange.Visible = false;
                startdate.Text = "";
                enddate.Text = "";
            }
            else
            {
                divsingledate.Visible = false;
                divdaterange.Visible = true;
            }
        }

        protected void btnSingleDate_Click(object sender, EventArgs e)
        {
            BindGV();
        }

        protected void btnDateRange_Click(object sender, EventArgs e)
        {
            BindGV();
        }

        protected void gvListItem_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvListItem.PageIndex = e.NewPageIndex;
            BindGV();
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            gvListItem.AllowPaging = false;
            gvListItem.Columns[15].Visible = false;
            BindGV();

            Common.ExportExcel(gvListItem, "Payout History");

            gvListItem.AllowPaging = true;
            gvListItem.Columns[15].Visible = true;
            BindGV();
        }

        protected void ddlRemittanceLine_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGV();
        }
        protected void TextChanged(object sender, EventArgs e)
        {
            BindGV();
        }
    }
}