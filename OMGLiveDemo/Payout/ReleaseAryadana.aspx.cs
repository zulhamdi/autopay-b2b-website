﻿using OMGLiveDemo.Models;
using OMGLiveDemo.Scripts;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.Payout
{
    public partial class ReleaseAryadana : System.Web.UI.Page
    {
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();
        float totalAmount = 0;
        OMG omg = new OMG();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                  bind();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        void bind()
        {
            long agid = Convert.ToInt64(SessionLib.Current.AdminID);
            long payid = Convert.ToInt64(Session["PayoutID"].ToString());
            hfPayoutID.Value = Session["PayoutID"].ToString();

            var pay = db.vw_Payout_Admin.Where(p => p.PayoutID == payid && p.Status == "queued").FirstOrDefault();
            if (pay != null)
            {
                txtSenderName.Text = pay.SenderName;
                txtSenderPhone.Text = pay.SenderPhone;
                txtSenderEmail.Text = pay.SenderEmail;
                txtBenfName.Text = pay.BeneficiaryName;
                txtBenfAccount.Text = pay.BeneficiaryAccount;
                txtBenfBank.Text = pay.BankName;
                txtBenfEmail.Text = pay.BeneficiaryEmail;
                txtBenfPhone.Text = pay.BeneficiaryPhone;

                var id = pay.CreatedBy;
                if (pay.isAgent == 1)
                {
                    pay.CreatedName = db.tblM_Agent.Where(p => p.AgentID == id).Select(p => p.Fullname).FirstOrDefault();
                }
                else
                    pay.CreatedName = db.tblM_Admin.Where(p => p.AdminID == id).Select(p => p.Fullname).FirstOrDefault();

                txtAgent.Text = pay.CreatedName;

                if (pay.RemittanceID == 1)
                {
                    lblTotalAmount.Text = "MYR " + String.Format(new CultureInfo("ms-MY"), "{0:n}", pay.TotalAmount).Replace("RM", "RM ");
                    lblTotalTransfer.Text = "IDR " + String.Format(new CultureInfo("id-ID"), "{0:n}", pay.TotalTransfer).Replace("Rp", "Rp ");
                }
                else if (pay.RemittanceID == 2)
                {
                    lblTotalAmount.Text = "IDR " + String.Format(new CultureInfo("id-ID"), "{0:n}", pay.TotalAmount).Replace("Rp", "Rp ");
                    lblTotalTransfer.Text = "MYR " + String.Format(new CultureInfo("ms-MY"), "{0:n}", pay.TotalTransfer).Replace("RM", "RM ");
                }

                if (pay.SenderTypeID == 1)
                {
                    divpersonal.Visible = true;
                    txtNationalID.Text = pay.SenderFileID;
                    divcompany.Visible = false;

                    var spd = db.tblM_Sender_Personal_Detail.Where(p => p.SenderID == pay.SenderID).FirstOrDefault();
                    hfSenderIDImageURL.Value = "";
                    hfSenderWithIDImageURL.Value = "";
                    if (spd != null)
                    {
                        hfSenderIDImageURL.Value = spd.IDURL;
                        hfSenderWithIDImageURL.Value = spd.PictureURL;
                    }
                }
                else
                {
                    divpersonal.Visible = false;
                    divcompany.Visible = true;

                    var scd = db.tblM_Sender_Company_Detail.Where(p => p.SenderID == pay.SenderID).FirstOrDefault();

                    if (pay.RemittanceID == 1)
                    {
                        txtSSM.Text = pay.SenderFileID;
                        hfCompanySSMFileURL.Value = pay.SenderFileIDURL;
                    }
                    else
                    {
                        txtCompanyLegalitasNo.Text = scd.LegalitasNo;
                        hfCompanyLegalitasFileURL.Value = scd.LegalitasURL;
                    }
                }
                txtSenderType.Text = pay.SenderType;

                var psd = db.tblT_Payout_Supporting_Document.Where(p => p.PayoutID == pay.PayoutID).FirstOrDefault();
                if (psd != null)
                {
                    divSupportingDocument.Visible = true;

                    var sd = db.tblT_Supporting_Document.Where(p => p.TXNID == psd.SupportingDocumentID).FirstOrDefault();
                    hfSupportingDocumentFileURL.Value = "";
                    if (sd != null)
                    {
                        hfSupportingDocumentFileURL.Value = sd.FileURL;
                    }
                }

                txtNotes.Text = pay.Notes;
                imgURL.ImageUrl = pay.ReceiptURL;
            }
        }

    }
}