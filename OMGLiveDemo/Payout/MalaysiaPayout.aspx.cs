﻿using Aryadana.API;
using Aryadana.API.Model;
using Newtonsoft.Json;
using OMGLiveDemo.Models;
using OMGLiveDemo.Models.OMG;
using OMGLiveDemo.Scripts;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.Payout
{
    public partial class MalaysiaPayout : System.Web.UI.Page
    {
        OMG omg = new OMG();
        List<Bank> bank = new List<Bank>();
        List<tblM_Beneficiary> benf = new List<tblM_Beneficiary>();
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();
        double amount = 0;
        double totalAmount = 0;
        double totalTransfer = 0;
        AryadanaServices aryadana_channel;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
              
                handlePostback();

                if (!IsPostBack)
                {
                    SetupPaymentChannel();
                    bind();
                  
                }

            }
            else
            {
                Master.checkRole();
            }
        }
        void SetupPaymentChannel()
        {
            string aryadana_endpoint = ConfigurationManager.AppSettings["ARYADANA_ENDPOINT"].ToString();
            string aryadana_username = ConfigurationManager.AppSettings["ARYADANA_USERNAME"].ToString();
            string aryadana_pswd = ConfigurationManager.AppSettings["ARYADANA_PASSWORD"].ToString();

            aryadana_channel = new AryadanaServices(aryadana_endpoint,aryadana_username,aryadana_pswd);
        }
        void resetSupportingDocument()
        {
            Session.Remove("SuportingDocumentFile");
            if (!fuSupportingDocument.HasFile)
            {
                lblSuportingDocument.Text = "Uploaded File = NONE";
            }
        }

        void manageSupportingDocument()
        {
            if (!fuSupportingDocument.HasFile)
            {
                lblSuportingDocument.Text = "Uploaded File = NONE";
            }

            if (Session["SuportingDocumentFile"] == null && fuSupportingDocument.HasFile)
            {
                Session["SuportingDocumentFile"] = fuSupportingDocument;
                lblSuportingDocument.Text = "Uploaded File = " + fuSupportingDocument.FileName;
            }
            // Next time submit and Session has values but FileUpload is Blank
            // Return the values from session to FileUpload
            else if (Session["SuportingDocumentFile"] != null && (!fuSupportingDocument.HasFile))
            {
                fuSupportingDocument = (FileUpload)Session["SuportingDocumentFile"];
                lblSuportingDocument.Text = "Uploaded File = " + fuSupportingDocument.FileName;
            }
            // Now there could be another situation when Session has File but user want to change the file
            // In this case we have to change the file in session object
            else if (fuSupportingDocument.HasFile)
            {
                Session["SuportingDocumentFile"] = fuSupportingDocument;
                lblSuportingDocument.Text = "Uploaded File = " + fuSupportingDocument.FileName;
            }
        }

        void handlePostback()
        {
            diverror.Visible = false;
            if (Session["Payout"] != null)
            {
                lblError.Text = Session["Payout"].ToString();
                diverror.Visible = true;
                Session["Payout"] = null;
            }

            if (SessionLib.Current.Role == Roles.MasterArea || SessionLib.Current.Role == Roles.TravelAgent)
            {
                getBalance();
            }

            manageSupportingDocument();

            //checkModal();
        }

        void checkModal()
        {
            if (hfModalCityOpen.Value == "yes")
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "Pop", "var isModalCity = true", true);
            else
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "Pop", "var isModalCity = false", true);
        }

        void getBalance()
        {
            long id = Convert.ToInt64(SessionLib.Current.AdminID);
            if (SessionLib.Current.Role == Roles.MasterArea)
            {
                var bal = db.tblM_Master_Area_Balance.Where(p => p.MasterAreaID == id && p.CountryID == 103).FirstOrDefault();
                hfBalance.Value = bal.Balance.ToString();
                divbalance.Visible = true;
                lblBalance.Text = bal.Currency + " " + String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToDouble(hfBalance.Value)).Replace("Rp", "");
            }
            else if (SessionLib.Current.Role == Roles.TravelAgent)
            {
                var bal = db.tblM_Travel_Agent_Balance.Where(p => p.TravelAgentID == id && p.CountryID == 103).FirstOrDefault();
                hfBalance.Value = bal.Balance.ToString();
                divbalance.Visible = true;
                lblBalance.Text = bal.Currency + " " + String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToDouble(hfBalance.Value)).Replace("Rp", "");
            }
        }

        void bindBank()
        {
            var banks = db.tblM_Bank.Where(p => p.CountryID == 133 && p.isActive == 1).ToList();
            if (banks != null)
            {
                ddlBank.DataSource = null;
                ddlBank.DataSource = banks;
                ddlBank.DataValueField = "Code";
                ddlBank.DataTextField = "Name";
                ddlBank.DataBind();
            }
        }

        void bindPurpose()
        {
            var pur = db.tblM_Payout_Purpose.ToList();
            if (pur != null)
            {
                ddlPurpose.DataSource = null;
                ddlPurpose.DataSource = pur;
                ddlPurpose.DataValueField = "ID";
                ddlPurpose.DataTextField = "Purpose";
                ddlPurpose.DataBind();
            }
        }

       async void bind()
        {
            long agid = Convert.ToInt64(SessionLib.Current.AdminID);

            bindBank();
            bindPurpose();

            var bankacc = db.vw_Bank_Account.Where(p => p.CountryID == 103).ToList();
            if (bankacc != null)
            {
                for (int i = 0; i < bankacc.Count; i++)
                {
                    bankacc[i].NameBank = bankacc[i].AccountHolder + " - " + bankacc[i].BankName;
                }

                ddlBankAccount.DataSource = null;
                ddlBankAccount.DataSource = bankacc;
                ddlBankAccount.DataValueField = "BankAccountID";
                ddlBankAccount.DataTextField = "NameBank";
                ddlBankAccount.DataBind();
            }

            if (SessionLib.Current.Role == Roles.Agent)
                benf = db.tblM_Beneficiary.Where(p => p.AgentID == agid && p.CountryID == 133 && p.isAgent == 1 && p.isActive == 1 && p.Status == "approved").ToList();
            else if (SessionLib.Current.Role == Roles.MasterArea)
                benf = db.tblM_Beneficiary.Where(p => p.AgentID == agid && p.CountryID == 133 && p.isAgent == 2 && p.isActive == 1 && p.Status == "approved").ToList();
            else if (SessionLib.Current.Role == Roles.TravelAgent)
                benf = db.tblM_Beneficiary.Where(p => p.AgentID == agid && p.CountryID == 133 && p.isAgent == 3 && p.isActive == 1 && p.Status == "approved").ToList();
            else
                benf = db.tblM_Beneficiary.Where(p => p.CountryID == 133 && p.isAgent == 0 && p.isActive == 1 && p.Status == "approved").ToList();

            if (benf.Count > 0)
            {
                for (int i = 0; i < benf.Count; i++)
                {
                    //benf[i].Index = i;
                    benf[i].NameBankAccount = benf[i].Name + " - " + benf[i].Bank + " - " + benf[i].Account;
                }
                ddlBeneficiary.DataSource = null;
                ddlBeneficiary.DataSource = benf;
                ddlBeneficiary.DataValueField = "Account";
                ddlBeneficiary.DataTextField = "NameBankAccount";
                ddlBeneficiary.DataBind();
            }

            var sendertype = db.tblM_Sender_Type.ToList();
            ddlSenderType.DataSource = null;
            ddlSenderType.DataSource = sendertype;
            ddlSenderType.DataValueField = "ID";
            ddlSenderType.DataTextField = "Type";
            ddlSenderType.DataBind();

            List<tblM_Sender> sender = new List<tblM_Sender>();
            if (SessionLib.Current.Role == Roles.Agent)
                sender = db.tblM_Sender.Where(p => p.AgentID == agid && p.CountryID == 103 && p.isActive == 1 && p.isAgent == 1 && p.Status == "approved").ToList();
            else if (SessionLib.Current.Role == Roles.MasterArea)
                sender = db.tblM_Sender.Where(p => p.AgentID == agid && p.CountryID == 103 && p.isActive == 1 && p.isAgent == 2 && p.Status == "approved").ToList();
            else if (SessionLib.Current.Role == Roles.TravelAgent)
                sender = db.tblM_Sender.Where(p => p.AgentID == agid && p.CountryID == 103 && p.isActive == 1 && p.isAgent == 3 && p.Status == "approved").ToList();
            else
                sender = db.tblM_Sender.Where(p => p.CountryID == 103 && p.isActive == 1 && p.isAgent == 0 && p.Status == "approved").ToList();
            ddlSender.DataSource = null;
            ddlSender.DataSource = sender;
            ddlSender.DataValueField = "SenderID";
            ddlSender.DataTextField = "Fullname";
            ddlSender.DataBind();
            if (sender.Count != 0)
                setSender();


            // CALL API ARYADANA
            CheckRatesRequest request = new CheckRatesRequest()
            {
                TypeOfTransaction = "CTA",
                OriginCurrency = "IDR",
                DestinationCurrency = "MYR"
            };
            CheckRatesResponse output = await aryadana_channel.CheckRates(request);
            if (output != null)
            {
                hfRate.Value = output.ForeignExchange.ToString();
                hfRateOriginal.Value = output.ForeignExchange.ToString();
            }
            //var cur = omg.getRateIDRMYR();
            //if (cur != null)
            //{
            //    hfRate.Value = cur.Rate;
            //    hfRateOriginal.Value = cur.Rate;
            //}

            if (SessionLib.Current.Role == Roles.Agent)
            {
                divrateall.Visible = false;
                var agentid = Int64.Parse(SessionLib.Current.AdminID);
                var rateagent = db.vw_Agent_Rate.Where(p => p.AgentID == agentid && p.RemittanceID == 2).FirstOrDefault();
                var rated = db.tblM_Rate.Where(p => p.RateID == rateagent.RateID).FirstOrDefault();
                hfRate.Value = (Double.Parse(hfRate.Value) + Double.Parse(rated.Margin)).ToString();
                hfMargin.Value = (Double.Parse(rateagent.AgentMargin)).ToString();
                if (rated != null)
                {
                    lblCurrency.Text = "1 MYR = " + String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(Int32.Parse(hfRate.Value) + Int32.Parse(hfMargin.Value))).Replace("Rp", "") + " IDR";
                    lblCurrencyUpdate.Text = "Last update : " + rated.UpdatedDate.Value.ToLongTimeString() + ", " + rated.UpdatedDate.Value.ToLongDateString();
                }

                hfAdminFee.Value = Convert.ToString(rateagent.AdminFee);
                lblAdminFee.Text = rateagent.AdminFeeCurrency + " " + rateagent.AdminFee;
            }
            else if (SessionLib.Current.Role == Roles.MasterArea)
            {
                divrateall.Visible = false;
                var rateagent = db.vw_Master_Area_Rate.Where(p => p.MasterAreaID == agid && p.RemittanceID == 2).FirstOrDefault();
                hfRate.Value = (Double.Parse(hfRate.Value) + Double.Parse(rateagent.Margin)).ToString();
                hfMargin.Value = (Double.Parse(rateagent.MasterAreaMargin)).ToString();
                lblCurrency.Text = "1 MYR = " + String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(Int32.Parse(hfRate.Value) + Int32.Parse(hfMargin.Value))).Replace("Rp", "") + " IDR";
                lblCurrencyUpdate.Text = "Last update : " + DateTime.Now.ToLongTimeString() + ", " + DateTime.Now.ToLongDateString();

                hfAdminFee.Value = rateagent.Fee.ToString();
                lblAdminFee.Text = "IDR " + String.Format(new CultureInfo("id-ID"), "{0:n}", Convert.ToDouble(rateagent.Fee));

                divtransferto.Visible = false;

                getBalance();
            }
            else if (SessionLib.Current.Role == Roles.TravelAgent)
            {
                divrateall.Visible = false;
                var rateagent = db.vw_Travel_Agent_Rate.Where(p => p.TravelAgentID == agid && p.RemittanceID == 2).FirstOrDefault();
                hfRate.Value = (Double.Parse(hfRate.Value) + Double.Parse(rateagent.Margin)).ToString();
                hfMargin.Value = (Double.Parse(rateagent.TravelAgentMargin)).ToString();
                lblCurrency.Text = "1 MYR = " + String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(Int32.Parse(hfRate.Value) + Int32.Parse(hfMargin.Value))).Replace("Rp", "") + " IDR";
               // lblCurrencyUpdate.Text = "Last update : " + cur.DateModified.ToLongTimeString() + ", " + cur.DateModified.ToLongDateString();
                lblCurrencyUpdate.Text = "Last update : " + DateTime.Now.ToLongTimeString() + ", " + DateTime.Now.ToLongDateString();

                hfAdminFee.Value = rateagent.Fee.ToString();
                lblAdminFee.Text = "IDR " + String.Format(new CultureInfo("id-ID"), "{0:n}", Convert.ToDouble(rateagent.Fee));

                divtransferto.Visible = false;

                getBalance();
            }
            else
            {
                divrateagent.Visible = false;

               
                // Update zul 24/19/2019
                List<tblM_Rate> rate = new List<tblM_Rate>();
                string margin = "0" ;
                rate = omg.getListRateIDRMYR();
               
                if (rate.Count > 0)
                {
                    tblM_Rate selected_rate = rate.Where(p => p.Name.Equals("Public")).FirstOrDefault();
                    margin = selected_rate != null ? selected_rate.Margin : "0";


                    //for (int i = 0; i < rate.Count; i++)
                    //{
                    //    rate[i].Rate = (Int32.Parse(hfRate.Value) + Int32.Parse(rate[i].Margin)).ToString();
                    //    rate[i].NameRate = rate[i].Name + " - Rp " + String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(Int32.Parse(rate[i].Rate))).Replace("Rp", "");
                    //    if (i == 0)
                    //        hfRate.Value = rate[i].Rate;
                    //}
                    //hfMargin.Value = "0";
                    //ddlRate.DataSource = null;
                    //ddlRate.DataSource = rate;
                    //ddlRate.DataValueField = "Rate";
                    //ddlRate.DataTextField = "NameRate";
                    //ddlRate.DataBind();
                }

                if (!hfRateOriginal.Value.Equals("0"))
                {
                    lblRatesMitra.Text = "1 MYR = " + String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(Int32.Parse(hfRateOriginal.Value) + Int32.Parse(margin))).Replace("Rp", "") + " IDR  (Selling Rate)";
                    lblRatesMitraValue.Text = "Last update : " + DateTime.Now.ToLongTimeString() + ", " + DateTime.Now.ToLongDateString();
                }
                else
                {
                    lblRatesMitra.Text = "1 MYR = " + String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(Int32.Parse(hfRateOriginal.Value))).Replace("Rp", "") + " IDR  (Selling Rate)";
                    lblRatesMitraValue.Text = "Sorry, Can't conect to Aryadana API : " + DateTime.Now.ToLongTimeString() + ", " + DateTime.Now.ToLongDateString();
                }
            

                hfRate.Value = (Double.Parse(hfRateOriginal.Value) + Double.Parse(margin)).ToString();

                divadminfee.Visible = true;
                var afee = db.tblM_Admin_Fee.Where(p => p.RemittanceID == 2).FirstOrDefault();
                hfAdminFee.Value = afee.Amount;
                lblAdminFees.Text = "IDR " + afee.Amount;
            }
        }

        bool checkBeforeTransfer()
        {

            if (!checkSender()) { return true; }

            if (!checkBeneficiary()) { return true; }

            if (!checkPayout()) { return true; }

            if (!checkAccumulatedLimit()) { return true; }

            if (SessionLib.Current.Role == Roles.MasterArea || SessionLib.Current.Role == Roles.TravelAgent)
            {
                getBalance();
                //var am = Convert.ToDouble(hfTotalTransfer.Value) + Convert.ToDouble(hfAdminFee.Value);
                var am = Convert.ToDouble(hfTotalAmount.Value) + Convert.ToDouble(hfAdminFee.Value);
                if (am > Convert.ToDouble(hfBalance.Value))
                {
                    getBalance();
                    diverror.Visible = true;
                    lblError.Text = "your amount is not enough to process this transfer. please note that you need to pay an admin fee for MYR " + hfAdminFee.Value;
                    return true;
                }
            }

            return false;
        }

        protected void btnProcess_Click(object sender, EventArgs e)
        {
            diverror.Visible = false;

            if (Session["APTXNID"] == null)
            {
                Session["APTXNID"] = Guid.NewGuid().ToString();
            }

            string txnID = Session["APTXNID"].ToString();

            try
            {
                if (checkBeforeTransfer())
                    return;

                tblT_Payout pay = new tblT_Payout();
                pay.RemittanceID = 2;
                pay.BeneficiaryID = Convert.ToInt64(hfBeneficiaryID.Value);
                pay.BeneficiaryName = txtBeneficiaryName.Text.Trim();
                pay.BeneficiaryAccount = txtBeneficiaryAccount.Text.Trim();

                if (ddlAmountType.SelectedValue == "1")
                {
                    pay.Amount = Convert.ToDouble(hfAmount.Value);
                    pay.TotalAmount = Convert.ToDouble(hfTotalAmount.Value);
                    pay.TotalTransfer = Convert.ToDouble(hfTotalTransfer.Value);
                }
                else if (ddlAmountType.SelectedValue == "2")
                {
                    pay.Amount = Convert.ToDouble(hfAmount.Value);
                    pay.TotalAmount = Convert.ToDouble(hfTotalAmount.Value.ToString().Trim());
                    pay.TotalTransfer = Convert.ToDouble(hfTotalTransfer.Value);
                }
                pay.AdminFee = hfAdminFee.Value;
                pay.UniqueCode = "0";

                pay.BeneficiaryBank = ddlBank.SelectedValue;
                pay.BankName = ddlBank.SelectedItem.Text.Trim();
                pay.Currency = "MYR";
                if (txtNotes.Text.Trim().Length > 17)
                    pay.Notes = txtNotes.Text.Trim().Substring(0, 16);
                else
                    pay.Notes = txtNotes.Text.Trim();
                pay.isActive = 1;

                if (SessionLib.Current.Role == Roles.Agent)
                {
                    pay.BankAccountID = Convert.ToInt64(ddlBankAccount.SelectedValue);
                    pay.isAgent = 1;
                    pay.Rate = (Int32.Parse(hfRate.Value) + Int32.Parse(hfMargin.Value)).ToString();
                    pay.RateGiven = (Int32.Parse(hfRate.Value)).ToString();
                }
                else if (SessionLib.Current.Role == Roles.MasterArea)
                {
                    pay.BankAccountID = 0;
                    pay.isAgent = 2;
                    pay.Rate = (Int32.Parse(hfRate.Value) + Int32.Parse(hfMargin.Value)).ToString();
                    pay.RateGiven = (Int32.Parse(hfRate.Value)).ToString();
                }
                else if (SessionLib.Current.Role == Roles.TravelAgent)
                {
                    pay.BankAccountID = 0;
                    pay.isAgent = 3;
                    pay.Rate = (Int32.Parse(hfRate.Value) + Int32.Parse(hfMargin.Value)).ToString();
                    pay.RateGiven = (Int32.Parse(hfRate.Value)).ToString();
                }
                else
                {
                    pay.BankAccountID = Convert.ToInt64(ddlBankAccount.SelectedValue);
                    pay.isAgent = 0;
                    //update - zul
                    pay.Rate = hfRate.Value;
                  //  pay.Rate = ddlRate.SelectedValue.ToString();
                    pay.RateGiven = (Int32.Parse(hfRateOriginal.Value)).ToString();
                }

                pay.CreatedBy = Int64.Parse(SessionLib.Current.AdminID);
                pay.CreatedDate = DateTime.Now;
                pay.Status = "created";
                pay.PurposeID = Convert.ToInt64(ddlPurpose.SelectedValue);


                db.SaveChanges();
                db.tblT_Payout.Add(pay);
                db.SaveChanges();

                if (fuSupportingDocument.HasFile)
                {

                    tblT_Supporting_Document sDoc = new tblT_Supporting_Document();
                    sDoc.TXNID = txnID;
                    sDoc.FileURL = saveSupportingDocument(txnID);

                    db.tblT_Supporting_Document.Add(sDoc);
                    db.SaveChanges();
                
                    tblT_Payout_Supporting_Document payoutSD = new tblT_Payout_Supporting_Document();
                    payoutSD.PayoutID = pay.PayoutID;
                    payoutSD.SupportingDocumentID = sDoc.TXNID;

                    db.tblT_Payout_Supporting_Document.Add(payoutSD);
                    db.SaveChanges();

                    Session.Remove("fuSupportingDocument");
                }

                if (SessionLib.Current.Role == Roles.MasterArea || SessionLib.Current.Role == Roles.TravelAgent)
                {
                    processBalance(Int64.Parse(SessionLib.Current.AdminID), pay.PayoutID);
                }
                
                tblT_Payout_Sender ps = new tblT_Payout_Sender();
                ps.PayoutID = pay.PayoutID;
                ps.SenderID = Convert.ToInt64(hfSenderID.Value);
                ps.CityID = Convert.ToInt64(hfCityID.Value);
                ps.SenderTypeID = Convert.ToInt32(ddlSenderType.SelectedValue);
                if (ps.SenderTypeID == 1)
                    ps.SenderType = "Personal";
                else
                    ps.SenderType = "Company";
                ps.SenderName = txtSenderName.Text;
                ps.SenderAddress = txtSenderAddress.Text;
                ps.SenderPhone = txtSenderPhone.Text;
                ps.SenderEmail = txtSenderEmail.Text;
                ps.SenderFileID = txtNationalID.Text;

                db.tblT_Payout_Sender.Add(ps);
                db.SaveChanges();

                /* old payout sender file
                if (ddlSenderType.SelectedValue == "1")
                {
                    tblT_Payout_Sender_FIle psf = new tblT_Payout_Sender_FIle();
                    psf.PayoutSenderID = ps.PayoutSenderID;
                    psf.FileTypeID = 3;
                    psf.Value = txtNationalID.Text.Trim();
                    psf.URL = hfSenderImageURL.Value;
                    ps.SenderFileIDURL = hfSenderImageURL.Value;

                    db.tblT_Payout_Sender_FIle.Add(psf);
                    db.SaveChanges();
                }
                else
                {
                    var sendtype = db.tblM_Sender_File_Type.Where(p => p.CountryID == 103 && p.SenderTypeID == 2).ToList();
                    for (int i = 0; i < sendtype.Count; i++)
                    {
                        tblT_Payout_Sender_FIle psf = new tblT_Payout_Sender_FIle();
                        psf.PayoutSenderID = ps.PayoutSenderID;
                        psf.FileTypeID = sendtype[i].ID;

                        if (sendtype[i].ID == 4)
                        {
                            psf.Value = txtTDP.Text.Trim();
                            psf.URL = hfTDPImageURL.Value;
                            //default doc
                            ps.SenderFileIDURL = hfTDPImageURL.Value;

                        }
                        if (sendtype[i].ID == 5)
                        {
                            psf.Value = txtSKMenkumham.Text.Trim();
                            psf.URL = hfSKImageURL.Value;
                        }
                        if (sendtype[i].ID == 6)
                        {

                            psf.Value = txtNPWP.Text.Trim();
                            psf.URL = hfNPWPImageURL.Value;
                        }
                        if (sendtype[i].ID == 7)
                        {

                            psf.Value = txtSIUP.Text.Trim();
                            psf.URL = hfSIUPImageURL.Value;
                        }

                        db.tblT_Payout_Sender_FIle.Add(psf);
                        db.SaveChanges();
                    }
                }
                */

                tblT_Payout_Beneficiary pb = new tblT_Payout_Beneficiary();
                pb.PayoutID = pay.PayoutID;
                pb.BeneficiaryID = pay.BeneficiaryID;
                pb.CityID = 0;
                pb.BeneficiaryName = pay.BeneficiaryName;
                pb.BeneficiaryBank = pay.BeneficiaryBank;
                pb.BeneficiaryAccount = pay.BeneficiaryAccount;
                pb.BeneficiaryEmail = txtBeneficiaryEmail.Text.Trim();
                pb.BeneficiaryPhone = txtBeneficiaryPhone.Text.Trim();
                pb.BeneficiaryAddress = txtBeneficiaryAddress.Text.Trim();
                pb.BeneficiaryFileID = pb.BeneficiaryAccount;
                pb.BankName = pay.BankName;
                db.tblT_Payout_Beneficiary.Add(pb);

                db.SaveChanges();

                //senders();
                //beneficiary();
                
                String eventData = "Create transfer [" + pay.PayoutID + "] from [" + ps.PayoutSenderID + "]" + ps.SenderName + " to [" + pb.BeneficiaryID + "]" + pb.BeneficiaryName + " by [" + SessionLib.Current.AdminID + "]" + SessionLib.Current.Name + ".";

                tblS_Log eLog = new tblS_Log
                {
                    EventDate = pay.CreatedDate.Value,
                    EventBy = Convert.ToInt64(SessionLib.Current.AdminID),
                    EventType = "transaction",
                    EventData = eventData,
                };

                db.tblS_Log.Add(eLog);
                db.SaveChanges();

                Session["ShowCustomerNote"] = "yes";

                if (SessionLib.Current.Role == Roles.MasterArea || SessionLib.Current.Role == Roles.TravelAgent)
                {
                    if (makePayout(pay))
                    {
                        Session["SuccessPayout"] = "Success created payout. Your request will update soon";
                        Response.Redirect("PayoutHistory", false);
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "Pop", "var isModal = false", true);
                        cancelTransfer();
                        return;
                    }
                }
                else
                {
                    Session["PayoutID"] = pay.PayoutID;
                    //string queryString = "CustomerNote.aspx";
                    //string newWin = "window.open('" + queryString + "','Nota Transaksi','width=900, height=800, titlebar=yes');";
                    //ClientScript.RegisterStartupScript(this.GetType(), "pop", newWin, true);
                    //ScriptManager.RegisterClientScriptBlock(this, GetType(), "Customer_Transaction_Note", "window.open('CustomerNote.aspx', 'Customer Transaction Note', 'width=300,height=100,left=100,top=100,resizable=yes');", true);

                    Response.Redirect("UploadReceipt", false);
                    // end changes //
                }
            }
            catch (Exception ex)
            {
                Session["Payout"] = "Error create payout. Message : " + ex.Message;
            }
        }

        /* old saving sender file
        void saveSenderFile()
        {
            if (ddlSenderType.SelectedValue == "1")
            {
                var sendtype = db.tblM_Sender_File_Type.Where(p => p.CountryID == 103 && p.SenderTypeID == 1).ToList();
                for (int i = 0; i < sendtype.Count; i++)
                {
                    if (sendtype[i].ID == 3)
                    {
                        if (hfSenderImageURL.Value == "" || hfSenderImageURL.Value == null)
                        {
                            string filename = "Temporary_NationalID_" + Common.getTimeSpan() + "." + fuNationalID.PostedFile.ContentType.Replace("image/", "");
                            string targetPath = Server.MapPath("../Sender/Picture/Indonesia/Personal/" + filename);
                            Stream strm = fuNationalID.PostedFile.InputStream;
                            Common.SaveImage(0.5, strm, targetPath);

                            hfSenderImageURL.Value = ConfigurationManager.AppSettings["URL_SENDER_INDONESIA_PERSONAL"].ToString() + filename;

                            txtNationalID.Enabled = false;
                            fuNationalID.Visible = false;
                            lkbNationalID.Visible = true;
                        }
                    }
                }
            }
            if (ddlSenderType.SelectedValue == "2")
            {
                var sendtype = db.tblM_Sender_File_Type.Where(p => p.CountryID == 103 && p.SenderTypeID == 2).ToList();
                for (int i = 0; i < sendtype.Count; i++)
                {
                    if (sendtype[i].ID == 4)
                    {
                        if (hfTDPImageURL.Value == "" || hfTDPImageURL.Value == null)
                        {
                            string filename = "Temporary_TDP_" + Common.getTimeSpan() + "." + fuTDP.PostedFile.ContentType.Replace("image/", "");
                            string targetPath = Server.MapPath("../Sender/Picture/Indonesia/Company/" + filename);
                            Stream strm = fuTDP.PostedFile.InputStream;
                            Common.SaveImage(0.5, strm, targetPath);

                            hfTDPImageURL.Value = ConfigurationManager.AppSettings["URL_SENDER_INDONESIA_PERSONAL"].ToString() + filename;

                            //default doc
                            hfSenderImageURL.Value = ConfigurationManager.AppSettings["URL_SENDER_INDONESIA_PERSONAL"].ToString() + filename;

                            txtTDP.Enabled = false;
                            fuTDP.Visible = false;
                            lkbTDP.Visible = true;
                        }
                    }
                    if (sendtype[i].ID == 5)
                    {
                        if (hfSKImageURL.Value == "" || hfSKImageURL.Value == null)
                        {
                            string filename = "Temporary_SKMenkumham_" + Common.getTimeSpan() + "." + fuSKMenkumham.PostedFile.ContentType.Replace("image/", "");
                            string targetPath = Server.MapPath("../Sender/Picture/Indonesia/Company/" + filename);
                            Stream strm = fuSKMenkumham.PostedFile.InputStream;
                            Common.SaveImage(0.5, strm, targetPath);

                            hfSKImageURL.Value = ConfigurationManager.AppSettings["URL_SENDER_INDONESIA_PERSONAL"].ToString() + filename;

                            txtSKMenkumham.Enabled = false;
                            fuSKMenkumham.Visible = false;
                            lkbSK.Visible = true;
                        }
                    }
                    if (sendtype[i].ID == 6)
                    {
                        if (hfNPWPImageURL.Value == "" || hfNPWPImageURL.Value == null)
                        {
                            string filename = "Temporary_NPWP_" + Common.getTimeSpan() + "." + fuNPWP.PostedFile.ContentType.Replace("image/", "");
                            string targetPath = Server.MapPath("../Sender/Picture/Indonesia/Company/" + filename);
                            Stream strm = fuNPWP.PostedFile.InputStream;
                            Common.SaveImage(0.5, strm, targetPath);

                            hfNPWPImageURL.Value = ConfigurationManager.AppSettings["URL_SENDER_INDONESIA_PERSONAL"].ToString() + filename;

                            txtNPWP.Enabled = false;
                            fuNPWP.Visible = false;
                            lkbNPWP.Visible = true;
                        }
                    }
                    if (sendtype[i].ID == 7)
                    {
                        if (hfSIUPImageURL.Value == "" || hfSIUPImageURL.Value == null)
                        {
                            string filename = "Temporary_SIUP_" + Common.getTimeSpan() + "." + fuSIUP.PostedFile.ContentType.Replace("image/", "");
                            string targetPath = Server.MapPath("../Sender/Picture/Indonesia/Company/" + filename);
                            Stream strm = fuSIUP.PostedFile.InputStream;
                            Common.SaveImage(0.5, strm, targetPath);

                            hfSIUPImageURL.Value = ConfigurationManager.AppSettings["URL_SENDER_INDONESIA_PERSONAL"].ToString() + filename;

                            txtSIUP.Enabled = false;
                            fuSIUP.Visible = false;
                            lkbSIUP.Visible = true;
                        }
                    }
                }
            }
        }
        */

        string saveSupportingDocument(string txnid)
        {
            try
            {
                string filename = txnid + "." + fuSupportingDocument.PostedFile.ContentType.Replace("image/", "");
                string targetPath = Server.MapPath("../Payout/Document/" + filename);
                Stream strm = fuSupportingDocument.PostedFile.InputStream;
                Common.SaveImage(0.5, strm, targetPath);

                return ConfigurationManager.AppSettings["URL_SUPPORTING_DOCUMENT"].ToString() + filename;
            } catch (Exception ex)
            {
                lblError.Text = "Error encountered while saving transaction supporting document: " + ex.Message;
                diverror.Visible = true;

                return "";
            }
            
        }

        void processBalance(long maid, long payid)
        {
            if (SessionLib.Current.Role == Roles.MasterArea)
            {
                var bal = db.tblM_Master_Area_Balance.Where(p => p.MasterAreaID == maid && p.CountryID == 103).FirstOrDefault();

                tblH_Master_Area_Balance_Update hbal = new tblH_Master_Area_Balance_Update();
                hbal.BalanceID = bal.BalanceID;
                hbal.Type = "Payout";
                hbal.RemittanceID = 2;
                hbal.MasterAreaID = maid;
                hbal.PayoutID = payid;
                hbal.BankName = ddlBank.SelectedItem.Text.Trim();
                hbal.Account = txtBeneficiaryAccount.Text.Trim();
                hbal.Beneficiary = txtBeneficiaryName.Text.Trim();
                hbal.Amount = Convert.ToDouble(hfAmount.Value);
                hbal.AdminFee = Convert.ToDouble(hfAdminFee.Value);
                hbal.DateCreated = DateTime.Now;

                hbal.BalancePrevious = bal.Balance;
                hbal.Balance = bal.Balance - (Convert.ToDouble(hfAmount.Value) + Convert.ToDouble(hfAdminFee.Value));
                bal.Balance = hbal.Balance;
                bal.UpdatedDate = DateTime.Now;
                hbal.BalanceID = bal.BalanceID;

                db.tblH_Master_Area_Balance_Update.Add(hbal);
            }
            else if (SessionLib.Current.Role == Roles.TravelAgent)
            {
                var bal = db.tblM_Travel_Agent_Balance.Where(p => p.TravelAgentID == maid && p.CountryID == 103).FirstOrDefault();

                tblH_Travel_Agent_Balance_Update hbal = new tblH_Travel_Agent_Balance_Update();
                hbal.BalanceID = bal.BalanceID;
                hbal.Type = "Payout";
                hbal.RemittanceID = 2;
                hbal.TravelAgentID = maid;
                hbal.PayoutID = payid;
                hbal.BankName = ddlBank.SelectedItem.Text.Trim();
                hbal.Account = txtBeneficiaryAccount.Text.Trim();
                hbal.Beneficiary = txtBeneficiaryName.Text.Trim();
                hbal.Amount = Convert.ToDouble(hfAmount.Value);
                hbal.AdminFee = Convert.ToDouble(hfAdminFee.Value);
                hbal.DateCreated = DateTime.Now;

                hbal.BalancePrevious = bal.Balance.Value;
                hbal.Balance = bal.Balance.Value - (Convert.ToDouble(hfAmount.Value) + Convert.ToDouble(hfAdminFee.Value));
                bal.Balance = hbal.Balance;
                bal.UpdatedDate = DateTime.Now;
                hbal.BalanceID = bal.BalanceID;

                db.tblH_Travel_Agent_Balance_Update.Add(hbal);
            }
            db.SaveChanges();
        }

        bool makePayout(tblT_Payout pay)
        {
            var payid = pay.PayoutID;
            PayoutRequest pr = new PayoutRequest();
            pr.RemittanceID = 2;
            pr.Currency = "MYR";
            pr.AmountIDR = Convert.ToString(pay.TotalTransfer);
            pr.Amount = pay.Amount.Value.ToString().Replace(",", ".");
            pr.BeneficiaryName = pay.BeneficiaryName;
            pr.BeneficiaryBank = pay.BeneficiaryBank;
            pr.BeneficiaryAccount = pay.BeneficiaryAccount;
            pr.BeneficiaryEmail = pay.BeneficiaryEmail;
            pr.PartnerReferenceID = pay.PayoutID.ToString();
            pr.Notes = pay.Notes;

            var payben = db.tblT_Payout_Beneficiary.Where(p => p.PayoutID == payid).FirstOrDefault();
            Beneficiary ben = new Beneficiary();
            ben.Name = payben.BeneficiaryName;
            ben.Address = payben.BeneficiaryAddress;
            ben.Email = payben.BeneficiaryEmail;
            ben.Phone = payben.BeneficiaryPhone;
            //ben.Identity = payben.BeneficiaryFileID;
            ben.Identity = pr.BeneficiaryAccount;
            pr.Beneficiary = ben;

            var paysen = db.tblT_Payout_Sender.Where(p => p.PayoutID == payid).FirstOrDefault();
            Models.OMG.Sender sen = new Models.OMG.Sender();
            sen.Name = paysen.SenderName;
            sen.Address = paysen.SenderAddress;
            sen.Email = paysen.SenderEmail;
            sen.Phone = paysen.SenderPhone;
            sen.Identity = paysen.SenderFileID;
            pr.Sender = sen;

            OutputModel output = omg.createApprovePayoutNew(pr);
            if (output.status != "error")
            {
                PayoutResponse prs = JsonConvert.DeserializeObject<PayoutResponse>(output.data.ToString());
                pay.Reference = prs.ReferenceID;
                pay.Status = prs.Status;
                pay.ModifiedDate = prs.DateModified;
                db.SaveChanges();

                var snd = db.tblT_Payout_Sender.Where(p => p.PayoutID == pay.PayoutID).FirstOrDefault();
                snd.Reference = pay.Reference;

                var bnf = db.tblT_Payout_Beneficiary.Where(p => p.PayoutID == pay.PayoutID).FirstOrDefault();
                bnf.Reference = pay.Reference;

                var hbal = db.tblH_Master_Area_Balance_Update.Where(p => p.PayoutID == pay.PayoutID).FirstOrDefault();
                hbal.Reference = pay.Reference;

                db.SaveChanges();

                omg.sendEmailMasterArea(pay);

                return true;
            }
            else
            {
                diverror.Visible = true;
                lblError.Text = output.message;
                return false;
            }
        }

        protected void ddlBank_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void ddlBeneficiaryType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlBeneficiaryType.SelectedValue == "-1")
            {
                divbenf.Visible = false;
            }
            if (ddlBeneficiaryType.SelectedValue == "1")
            {
                divbenf.Visible = true;
                divbeneficiary.Visible = true;
                txtBeneficiaryName.Enabled = false;
                txtBeneficiaryAccount.Enabled = false;
                ddlBank.Enabled = false;
                cbSaveBenfeficiary.Visible = false;
                cbUpdateBeneficiary.Visible = false;
                txtBeneficiaryAddress.Enabled = false;
                txtBeneficiaryAlias.Enabled = false;
                txtBeneficiaryEmail.Enabled = false;
                txtBeneficiaryPhone.Enabled = false;

                setBeneficiary();
            }
            else if (ddlBeneficiaryType.SelectedValue == "2")
            {
                Response.Redirect("~/Beneficiaries/AddMalaysianBeneficiaries");
            }
        }

        protected void btnValidate_Click(object sender, EventArgs e)
        {
            BankAccount ba = new BankAccount();
            ba.CountryID = 103;
            ba.AccountNo = txtBeneficiaryAccount.Text.Trim();
            ba.BankCode = ddlBank.SelectedValue;
            var acc = omg.validateAccount(ba);
            if (acc != null)
            {
                txtBeneficiaryName.Text = acc.AccountName.Replace("  ", "");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Found " + acc.AccountName + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('The account of " + ba.AccountNo + " and bank of " + ddlBank.SelectedItem + " is not found')", true);
            }
        }

        /* old save sender
        void senders()
        {
            try
            {
                //if (cbSaveSender.Checked)
                //{
                var id = Int64.Parse(SessionLib.Current.AdminID);
                int isAgent = 0;
                if (SessionLib.Current.Role == Roles.Agent)
                {
                    isAgent = 1;
                }
                if (SessionLib.Current.Role == Roles.MasterArea)
                {
                    isAgent = 2;
                }
                if (SessionLib.Current.Role == Roles.TravelAgent)
                {
                    isAgent = 3;
                }

                var check = db.tblM_Sender.Where(p => p.Phone == txtSenderPhone.Text.Trim() && p.AgentID == id && p.isAgent == isAgent && p.CountryID == 103).FirstOrDefault();
                if (check == null)
                {

                    tblM_Sender bnf = new tblM_Sender();
                    bnf.Fullname = txtSenderName.Text.Trim();
                    bnf.Address = txtSenderAddress.Text.Trim();
                    bnf.Phone = txtSenderPhone.Text.Trim();
                    bnf.Email = txtSenderEmail.Text.Trim();
                    bnf.CountryID = 103;
                    bnf.Country = "Indonesia";
                    bnf.Status = "created";
                    bnf.CityID = Convert.ToInt64(hfCityID.Value);
                    bnf.JoinDate = DateTime.Now;
                    bnf.AddedBy = Convert.ToInt64(SessionLib.Current.AdminID);
                    bnf.Email = txtBeneficiaryEmail.Text.Trim();
                    bnf.SenderTypeID = Convert.ToInt64(ddlSenderType.SelectedValue);

                    bnf.isAgent = isAgent;
                    bnf.AgentID = id;
                    bnf.AddedBy = bnf.AgentID;
                    bnf.isActive = 1;
                    db.tblM_Sender.Add(bnf);
                    db.SaveChanges();

                    if (bnf.SenderTypeID == 1)
                    {
                        var ftype = db.tblM_Sender_File_Type.Where(p => p.SenderTypeID == bnf.SenderTypeID && p.CountryID == 103).FirstOrDefault();
                        tblM_Sender_File sf = new tblM_Sender_File();
                        sf.SenderID = bnf.SenderID;
                        sf.FileTypeID = ftype.ID;
                        sf.Value = txtNationalID.Text.ToString().Trim();
                        sf.URL = hfSenderImageURL.Value;

                        db.tblM_Sender_File.Add(sf);
                        db.SaveChanges();
                    }
                    else
                    {
                        var ftype = db.tblM_Sender_File_Type.Where(p => p.SenderTypeID == bnf.SenderTypeID && p.CountryID == 103).ToList();
                        for (int i = 0; i < ftype.Count; i++)
                        {
                            tblM_Sender_File sf = new tblM_Sender_File();
                            sf.SenderID = bnf.SenderID;
                            sf.FileTypeID = ftype[i].ID;
                            if (sf.FileTypeID == 4)
                            {
                                sf.Value = txtTDP.Text.ToString().Trim();
                                sf.URL = hfTDPImageURL.Value;
                            }
                            else if (sf.FileTypeID == 5)
                            {
                                sf.Value = txtSKMenkumham.Text.ToString().Trim();
                                sf.URL = hfSKImageURL.Value;
                            }
                            else if (sf.FileTypeID == 6)
                            {
                                sf.Value = txtNPWP.Text.ToString().Trim();

                                sf.URL = hfNPWPImageURL.Value;
                            }
                            else if (sf.FileTypeID == 7)
                            {
                                sf.Value = txtSIUP.Text.ToString().Trim();
                                
                                sf.URL = hfSIUPImageURL.Value;
                            }

                            db.tblM_Sender_File.Add(sf);
                            db.SaveChanges();
                        }
                    }

                    db.SaveChanges();
                }
                else
                {
                    Session["ErrorSender"] = "Failed save sender because the sender already registered";
                }
                //}
            }
            catch (Exception ex)
            {
                Session["ErrorSender"] = "Failed save sender for some reason : " + ex.Message.ToString();
            }
        }
        */

        /* old save bene
        void beneficiary()
        {
            try
            {
                int cid = Int32.Parse(SessionLib.Current.AdminID);
                int isagnt = 0;
                if (SessionLib.Current.Role == Roles.Agent)
                    isagnt = 1;
                else if (SessionLib.Current.Role == Roles.MasterArea)
                    isagnt = 2;
                else if (SessionLib.Current.Role == Roles.TravelAgent)
                    isagnt = 3;

                tblM_Beneficiary bnf = new tblM_Beneficiary();
                if (ddlBeneficiaryType.SelectedValue == "1")
                {
                    if (cbUpdateBeneficiary.Checked)
                    {
                        bnf = db.tblM_Beneficiary.Where(p => p.Account == txtBeneficiaryAccount.Text.Trim() && p.BankCode == ddlBank.SelectedValue && p.isAgent == isagnt && p.CreatedBy == cid && p.isActive == 1 && p.CountryID == 133).FirstOrDefault();
                        string paybef = JsonConvert.SerializeObject(benf);
                        bnf.Alias = txtBeneficiaryAlias.Text.Trim();
                        bnf.Phone = txtBeneficiaryPhone.Text.Trim();
                        bnf.Email = txtBeneficiaryEmail.Text.Trim();
                        bnf.Address = txtBeneficiaryAddress.Text.Trim();
                        bnf.UpdatedDate = DateTime.Now;
                        bnf.UpdatedBy = cid;
                        bnf.CityID = 0;
                        string payaft = JsonConvert.SerializeObject(benf);

                        tblH_Beneficiary_Update hbnf = new tblH_Beneficiary_Update();
                        hbnf.AdminID = cid;
                        hbnf.BeneficiaryID = bnf.BeneficiaryID;
                        hbnf.PayloadBefore = paybef;
                        hbnf.PayloadAfter = payaft;
                        hbnf.UpdatedDate = bnf.UpdatedDate;
                        db.tblH_Beneficiary_Update.Add(hbnf);

                        db.SaveChanges();
                    }
                }
                else if (ddlBeneficiaryType.SelectedValue == "2")
                {
                    if (cbSaveBenfeficiary.Checked)
                    {
                        bnf = db.tblM_Beneficiary.Where(p => p.Account == txtBeneficiaryAccount.Text.Trim() && p.BankCode == ddlBank.SelectedValue && p.isAgent == isagnt && p.CreatedBy == cid && p.isActive == 1).FirstOrDefault();
                        if (bnf == null)
                        {
                            bnf.Name = txtBeneficiaryName.Text.Trim();
                            bnf.Account = txtBeneficiaryAccount.Text;
                            bnf.Bank = ddlBank.SelectedItem.Text;
                            bnf.BankCode = ddlBank.SelectedValue;
                            bnf.Alias = txtBeneficiaryAlias.Text.Trim();
                            bnf.Phone = txtBeneficiaryPhone.Text.Trim();
                            bnf.Email = txtBeneficiaryEmail.Text.Trim();
                            bnf.Address = txtBeneficiaryAddress.Text.Trim();
                            bnf.CityID = 0;
                            bnf.CountryID = 133;
                            bnf.Country = "Malaysia";
                            bnf.isActive = 1;
                            bnf.isAgent = isagnt;
                            bnf.CreatedDate = DateTime.Now;
                            bnf.CreatedBy = cid;
                            db.tblM_Beneficiary.Add(bnf);
                            db.SaveChanges();
                        }
                        else
                        {
                            Session["ErrorBeneficiary"] = "Failed save beneficiary because the beneficiary already registered";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Session["ErrorBeneficiary"] = "Failed save beneficiary for some reason : " + ex.Message.ToString();
            }
        }
        */
        protected void txtAmount_TextChanged(object sender, EventArgs e)
        {
            if(ddlAmountType.SelectedValue == "-1")
            {
                diverror.Visible = true;
                lblError.Text = "Please select Amount Type first";
                txtAmount.Text = ""; 
                return;
            }

            countAmount();

            if (!checkAccumulatedLimit())
            {
                divSupportingDocument.Visible = true;
            }
            else
            {
                divSupportingDocument.Visible = false;
            }
        }

        void countAmount()
        {
            if (txtAmount.Text != "")
            {
                
                if (ddlAmountType.SelectedValue == "2") // IDR
                {
                    double am = double.Parse(txtAmount.Text, CultureInfo.InvariantCulture.NumberFormat);
                    if (SessionLib.Current.Role == Roles.MasterArea || SessionLib.Current.Role == Roles.TravelAgent)
                    {
                        //totalAmount = am;
                        totalAmount = am + double.Parse(hfAdminFee.Value);
                    }
                    else
                        totalAmount = am + double.Parse(hfAdminFee.Value);
                    if (SessionLib.Current.Role == Roles.Agent || SessionLib.Current.Role == Roles.MasterArea || SessionLib.Current.Role == Roles.TravelAgent)
                    {
                        totalTransfer = am / (double.Parse(hfRate.Value) + double.Parse(hfMargin.Value));
                    }
                    else
                    {
                        totalTransfer = am / (double.Parse(hfRate.Value));
                    }
                    hfAmount.Value = am.ToString();
                    hfTotalAmount.Value = totalAmount.ToString();
                    hfTotalTransfer.Value = totalTransfer.ToString();
                    if (SessionLib.Current.Role == Roles.MasterArea || SessionLib.Current.Role == Roles.TravelAgent)
                    {
                        //lblTotalAmount.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToDouble(hfAmount.Value)).Replace("Rp", "Rp ");
                        lblTotalAmount.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToDouble(totalAmount)).Replace("Rp", "Rp ");
                    }
                    else
                        lblTotalAmount.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToDouble(totalAmount)).Replace("Rp", "Rp ");
                    lblTotalTransfer.Text = String.Format(new CultureInfo("ms-MY"), "{0:c}", Convert.ToDouble(totalTransfer)).Replace("RM", "RM ");
                }
                else if (ddlAmountType.SelectedValue == "1") //MYR
                {
                    double am = double.Parse(txtAmount.Text, CultureInfo.InvariantCulture.NumberFormat);
                    totalTransfer = am;
                    if (SessionLib.Current.Role == Roles.Agent || SessionLib.Current.Role == Roles.MasterArea || SessionLib.Current.Role == Roles.TravelAgent)
                        amount = am * (double.Parse(hfRate.Value) + double.Parse(hfMargin.Value));
                    else
                        amount = am * (double.Parse(hfRate.Value));

                    if (SessionLib.Current.Role == Roles.MasterArea || SessionLib.Current.Role == Roles.TravelAgent)
                    {
                        //totalAmount = amount;
                        totalAmount = amount + double.Parse(hfAdminFee.Value);
                    }
                    else
                        totalAmount = amount + double.Parse(hfAdminFee.Value);

                    hfAmount.Value = amount.ToString();
                    hfTotalAmount.Value = totalAmount.ToString();
                    hfTotalTransfer.Value = totalTransfer.ToString();
                    if (SessionLib.Current.Role == Roles.MasterArea || SessionLib.Current.Role == Roles.TravelAgent)
                    {
                        //lblTotalAmount.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToDouble(hfAmount.Value)).Replace("Rp", "Rp ");
                        lblTotalAmount.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToDouble(totalAmount)).Replace("Rp", "Rp ");
                    }
                    else
                        lblTotalAmount.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToDouble(totalAmount)).Replace("Rp", "Rp ");
                    lblTotalTransfer.Text = String.Format(new CultureInfo("ms-MY"), "{0:c}", Convert.ToDouble(totalTransfer)).Replace("RM", "RM ");
                }
            }

        }

        double getAccumulatedLimit()
        {
            return 350000000.00;
        }

        bool checkAccumulatedLimit()
        {
            DateTime curdate = DateTime.Now;
            int curmon = curdate.Month;
            int curyear = curdate.Year;

            DateTime firstd = getFirstDate(curmon, curyear);
            DateTime lastd = getLastDate(firstd);

            var data = db.vw_Payout_IDR.Where(p => p.RemittanceID == 2 && p.Status == "completed" && p.CreatedDate >= firstd && p.CreatedDate <= lastd)
                    .GroupBy(p => new { p.RemittanceID, p.SenderID })
                    .Select(p =>
                     new
                     {
                         RemittanceID = 2,
                         SenderName = p.Key.SenderID,
                         TotalTransfer = p.Sum(s => s.IDRTotalTransfer), // "IDR " + String.Format("{0:n}", p.Sum(s => s.Amount)),
                         Volume = p.Count(),
                     })
                    .FirstOrDefault();

            double baseAmountIDR = Convert.ToDouble(hfTotalAmount.Value) - double.Parse(hfAdminFee.Value);
            double accumulatedAmountIDR = 0.0;

            if (data != null)
            {
                accumulatedAmountIDR = data.TotalTransfer.Value + baseAmountIDR;
            } else
            {
                accumulatedAmountIDR = baseAmountIDR;
            }

            if (accumulatedAmountIDR >= getAccumulatedLimit() && !fuSupportingDocument.HasFile)
            {
                diverror.Visible = true;
                lblError.Text = "The accumulated transfer amount for this sender will exceed the monthly USD25,000 threshold! Please include a supporting document.";
                return false;
            }

            return true;
        }

        DateTime getFirstDate(int mon, int year)
        {
            return new DateTime(year, mon, 1);
        }

        DateTime getLastDate(DateTime fd)
        {
            return fd.AddMonths(1).AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59);
        }

        protected void btnError_ServerClick(object sender, EventArgs e)
        {
            diverror.Visible = false;
        }

        protected void ddlBeneficiary_SelectedIndexChanged(object sender, EventArgs e)
        {
            setBeneficiary();
        }

        void setBeneficiary()
        {
            tblM_Beneficiary bf = new tblM_Beneficiary();
            string account = ddlBeneficiary.SelectedValue;
            long id = Convert.ToInt64(SessionLib.Current.AdminID);

            if (SessionLib.Current.Role == Roles.Agent)
                bf = db.tblM_Beneficiary.Where(p => p.CountryID == 133 && p.Account == account && p.isAgent == 1 && p.AgentID == id && p.isActive == 1).FirstOrDefault();
            else if (SessionLib.Current.Role == Roles.MasterArea)
                bf = db.tblM_Beneficiary.Where(p => p.CountryID == 133 && p.Account == account && p.isAgent == 2 && p.AgentID == id && p.isActive == 1).FirstOrDefault();
            else if (SessionLib.Current.Role == Roles.TravelAgent)
                bf = db.tblM_Beneficiary.Where(p => p.CountryID == 133 && p.Account == account && p.isAgent == 3 && p.AgentID == id && p.isActive == 1).FirstOrDefault();
            else
                bf = db.tblM_Beneficiary.Where(p => p.CountryID == 133 && p.Account == account && p.isAgent == 0 && p.isActive == 1).FirstOrDefault();

            if (bf != null)
            {
                hfBeneficiaryID.Value = Convert.ToString(bf.BeneficiaryID);
                txtBeneficiaryName.Text = bf.Name;
                txtBeneficiaryAccount.Text = bf.Account;
                ddlBank.Text = bf.BankCode;
                txtBeneficiaryAlias.Text = bf.Alias;
                txtBeneficiaryAlias.Text = bf.Phone;
                txtBeneficiaryEmail.Text = bf.Email;
                txtBeneficiaryAddress.Text = bf.Address;
                txtBeneficiaryPhone.Text = bf.Phone;
            }
        }

        protected void ddlAmountType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlAmountType.SelectedValue == "1")
            {
                lblAmount.Text = "Amount (in MYR) *";
            }
            else if (ddlAmountType.SelectedValue == "2")
            {
                lblAmount.Text = "Amount (in IDR) *";
            }
            countAmount();
        }

        protected void ddlRate_SelectedIndexChanged(object sender, EventArgs e)
        {
            //hfRate.Value = ddlRate.SelectedValue;
           // countAmount();
        }

        protected void ddlSenderType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSenderType.SelectedValue == "1")
            {
                divpersonal.Visible = true;
                divcompany.Visible = false;
            }
            else if (ddlSenderType.SelectedValue == "2")
            {
                divpersonal.Visible = false;
                divcompany.Visible = true;
            }
        }

        protected void ddlSender_SelectedIndexChanged(object sender, EventArgs e)
        {
            setSender();
        }

        protected void ddlChooseSender_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlChooseSender.SelectedValue == "-1")
            {
                divsender.Visible = false;
            }
            if (ddlChooseSender.SelectedValue == "1")
            {
                divsender.Visible = true;
                divchoosesender.Visible = true;
                txtSenderName.Enabled = false;
                txtSenderEmail.Enabled = false;
                txtSenderPhone.Enabled = false;
                txtSenderAddress.Enabled = false;
                ddlSenderType.Enabled = false;
                txtCity.Text = "";

                if (ddlSenderType.SelectedValue == "1")
                {
                    divpersonal.Visible = true;
                    //txtNationalID.Enabled = false;
                    //fuNationalID.Visible = false;
                    //lkbNationalID.Visible = true;

                    divcompany.Visible = false;
                    //txtTDP.Enabled = false;
                    //fuTDP.Visible = false;
                    //lkbTDP.Visible = true;

                    //txtSKMenkumham.Enabled = false;
                    //fuSKMenkumham.Visible = false;
                    //lkbSK.Visible = true;

                    //txtNPWP.Enabled = false;
                    //fuNPWP.Visible = false;
                    //lkbNPWP.Visible = true;

                    //txtSIUP.Enabled = false;
                    //fuSIUP.Visible = false;
                    //lkbSIUP.Visible = true;
                }
                else
                {
                    divpersonal.Visible = false;
                    //txtNationalID.Enabled = false;
                    //fuNationalID.Visible = false;
                    //lkbNationalID.Visible = false;

                    divcompany.Visible = true;
                    //txtTDP.Enabled = false;
                    //fuTDP.Visible = false;
                    //lkbTDP.Visible = true;

                    //txtSKMenkumham.Enabled = false;
                    //fuSKMenkumham.Visible = false;
                    //lkbSK.Visible = true;

                    //txtNPWP.Enabled = false;
                    //fuNPWP.Visible = false;
                    //lkbNPWP.Visible = true;

                    //txtSIUP.Enabled = false;
                    //fuSIUP.Visible = false;
                    //lkbSIUP.Visible = true;
                }

                hfSenderImageURL.Value = "";
                hfSenderWithIDImageURL.Value = "";
                hfLegalitasImageURL.Value = "";
                hfCompanyNPWPImageURL.Value = "";
                hfPICIDImageURL.Value = "";

                if (ddlSender.SelectedItem != null)
                    setSender();
            }
            else if (ddlChooseSender.SelectedValue == "2")
            {
                Response.Redirect("~/Sender/AddIndonesianSender");
            }
        }

        void setSender()
        {
            var id = Convert.ToInt64(ddlSender.SelectedValue);
            var agid = Convert.ToInt64(SessionLib.Current.AdminID);
            tblM_Sender bf = new tblM_Sender();
            if (SessionLib.Current.Role == Roles.Agent)
                bf = db.tblM_Sender.Where(p => p.CountryID == 103 && p.SenderID == id && p.isAgent == 1 && p.AgentID == agid).FirstOrDefault();
            else if (SessionLib.Current.Role == Roles.MasterArea)
                bf = db.tblM_Sender.Where(p => p.CountryID == 103 && p.SenderID == id && p.isAgent == 2 && p.AgentID == agid).FirstOrDefault();
            else if (SessionLib.Current.Role == Roles.TravelAgent)
                bf = db.tblM_Sender.Where(p => p.CountryID == 103 && p.SenderID == id && p.isAgent == 3 && p.AgentID == agid).FirstOrDefault();
            else
                bf = db.tblM_Sender.Where(p => p.CountryID == 103 && p.SenderID == id && p.isAgent == 0 && p.AgentID == 0).FirstOrDefault();

            if (bf != null)
            {
                hfSenderID.Value = Convert.ToString(bf.SenderID);
                txtSenderName.Text = bf.Fullname;
                ddlSenderType.Text = bf.SenderTypeID.ToString();
                txtSenderPhone.Text = bf.Phone;
                txtSenderEmail.Text = bf.Email;
                txtSenderAddress.Text = bf.Address;

                if (bf.CityID != null)
                {
                    var city = db.tblM_City.Where(p => p.ID == bf.CityID).FirstOrDefault();
                    hfCityID.Value = city.ID.ToString();
                    hfCityName.Value = city.City;
                    txtCity.Text = city.City;
                }

                if (bf.SenderTypeID == 1)
                {
                    divpersonal.Visible = true;
                    divcompany.Visible = false;

                    //var file = db.tblM_Sender_File.Where(p => p.SenderID == id).FirstOrDefault();
                    var spd = db.tblM_Sender_Personal_Detail.Where(p => p.SenderID == id).FirstOrDefault();

                    if (spd != null)
                    {
                        txtNationalID.Text = spd.IDNo;

                        var idType = db.tblM_Person_ID_Type.Where(p => p.ID == spd.IDType).FirstOrDefault();
                        if (idType != null)
                        {
                            txtNationalIDType.Text = idType.Name;
                        }

                        if(spd.IDValidityType == "Seumur Hidup")
                        {
                            txtNationalIDValidity.Text = "Seumur Hidup";
                        }
                        else
                        {
                            txtNationalIDValidity.Text = spd.IDValidity.Value.ToString("dd/MM/yyyy");
                        }

                        hfSenderImageURL.Value = spd.IDURL;
                        hfSenderWithIDImageURL.Value = spd.PictureURL;
                    }

                    txtNationalID.Enabled = false;
                    fuNationalID.Visible = false;
                    lkbNationalID.Visible = true;
                    txtNationalIDType.Enabled = false;
                    txtNationalIDValidity.Enabled = false;
                }
                else
                {
                    divpersonal.Visible = false;
                    divcompany.Visible = true;

                    var file = db.tblM_Sender_File.Where(p => p.SenderID == id).ToList();

                    var scd = db.vw_Company_Sender_Detail.Where(p => p.SenderID == id).FirstOrDefault();

                    if(scd != null)
                    {
                        txtLegalitasType.Text = scd.LegalitasType;

                        txtLegalitas.Text = scd.LegalitasNo;
                        hfLegalitasImageURL.Value = scd.LegalitasURL;

                        txtCompanyNPWP.Text = scd.NPWPNo;
                        hfCompanyNPWPImageURL.Value = scd.NPWPURL;

                        txtCompanyBBH.Text = scd.BBHName;
                        txtCompanyBusinessType.Text = scd.BusinessFieldName;

                        txtPICName.Text = scd.PICName;
                        txtPICIDNo.Text = scd.PICIDNo;
                    }

                    txtLegalitasType.Enabled = false;
                    txtLegalitas.Enabled = false;
                    fuLegalitasFile.Visible = false;
                    lkbLegalitasFile.Visible = true;

                    txtCompanyNPWP.Enabled = false;
                    fuCompanyNPWPFile.Visible = false;
                    lkbCompanyNPWP.Visible = true;

                    txtCompanyBBH.Enabled = false;
                    txtCompanyBusinessType.Enabled = false;

                    txtPICName.Enabled = false;
                    txtPICIDNo.Enabled = false;

                }
            }


        }

        void doTransfer()
        {
            lblSummarySenderName.Text = txtSenderName.Text;
            lblSummaryBeneficiaryName.Text = txtBeneficiaryName.Text;
            lblSummaryTotalAmount.Text = lblTotalAmount.Text;
            lblSummaryTotalTransfer.Text = lblTotalTransfer.Text;
            lblSummaryBeneficiaryAccount.Text = ddlBank.SelectedItem.Text + ": " + txtBeneficiaryAccount.Text;

            btnProcess.Visible = false;
            btnBack2.Visible = false;
            dvProcessing.Visible = true;

            ddlAmountType.Enabled = false;
            txtAmount.Enabled = false;
            txtNotes.Enabled = false;
        }

        void cancelTransfer()
        {
            btnProcess.Visible = true;
            btnBack2.Visible = true;
            dvProcessing.Visible = false;

            ddlAmountType.Enabled = true;
            txtAmount.Enabled = true;
            txtNotes.Enabled = true;
        }

        protected void btnConfirmTransafer_Click(object sender, EventArgs e)
        {
            diverror.Visible = false;
            if (checkBeforeTransfer())
                return;

            ScriptManager.RegisterClientScriptBlock(this, GetType(), "Pop", "var isModal = true", true);
            doTransfer();
        }

        protected void btnCancelTransafer_Click(object sender, EventArgs e)
        {
            diverror.Visible = false;
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "Pop", "var isModal = false", true);
            cancelTransfer();
        }

        protected void btnNext1_Click(object sender, EventArgs e)
        {
            diverror.Visible = false;
            if (checkSender())
            {
                divstep2.Visible = true;
                divstep1.Visible = false;
                divstep3.Visible = false;
            }


        }

        protected void btnNext2_Click(object sender, EventArgs e)
        {
            diverror.Visible = false;
            if (checkBeneficiary())
            {
                divstep3.Visible = true;
                divstep1.Visible = false;
                divstep2.Visible = false;
            }
        }

        bool checkSender()
        {
            if (ddlChooseSender.SelectedValue == "-1")
            {
                diverror.Visible = true;
                lblError.Text = "Please choose sender first!";
                return false;
            }

            return true;
        }

        bool checkBeneficiary()
        {
            if (txtBeneficiaryName.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Beneficiary Name cannot be empty!";
                return false;
            }

            if (txtBeneficiaryAccount.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Beneficiary Account cannot be empty!";
                return false;
            }

            if (txtBeneficiaryAddress.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Beneficiary Address cannot be empty!";
                return false;
            }

            if (txtBeneficiaryEmail.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Beneficiary email cannot be empty!";
                return false;
            }

            if (txtBeneficiaryPhone.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Beneficiary phone number cannot be empty!";
                return false;
            }
            return true;
        }

        bool checkPayout()
        {
            if (ddlAmountType.SelectedValue == "-1")
            {
                diverror.Visible = true;
                lblError.Text = "You should choose Amount Type first!";
                return false;
            }

            if (txtAmount.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Amount cannot be empty!";
                return false;
            }

            if (SessionLib.Current.Role != Roles.MasterArea || SessionLib.Current.Role != Roles.TravelAgent)
            {
                if (ddlBankAccount.Text == "")
                {
                    diverror.Visible = true;
                    lblError.Text = "Please choose the bank account that you would transfer to";
                    return false;
                }
            }

            if (ddlPurpose.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Transfer purpose cannot be empty!";
                return false;
            }

            if (txtNotes.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Notes cannot be empty!";
                return false;
            }

            if (txtNotes.Text.Length > 17)
            {
                diverror.Visible = true;
                lblError.Text = "Only 17 characters allowed for Notes!";
                return false;
            }

            if (Convert.ToDouble(hfTotalTransfer.Value) < 100.00)
            {
                diverror.Visible = true;
                lblError.Text = "Minimum amount for transfer is RM 100 per transaction!";
                return false;
            }

            if (Convert.ToDouble(hfTotalTransfer.Value) > 100000000.00)
            {
                diverror.Visible = true;
                lblError.Text = "Maximum amount for transfer is RM 100.000 per transaction!";
                return false;
            }

            return true;
        }

        protected void lkbViewSenderIDImage_Click(object sender, EventArgs e)
        {
            if (hfSenderImageURL.Value != null)
            {
                Session["ImageURL"] = hfSenderImageURL.Value;
                Session["From"] = "Image";
                string txt = "";
                txt = "Sender ID No : " + txtNationalID.Text;
                Session["Text"] = txtSenderName.Text + " - " + txt;
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "colorBox", "var isColorbox = true", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('File not found!')", true);
            }
        }

        protected void lkbViewImageLegalitas_Click(object sender, EventArgs e)
        {
            if (hfLegalitasImageURL.Value != null)
            {
                Session["ImageURL"] = hfLegalitasImageURL.Value;
                Session["From"] = "Image";
                string txt = "";
                txt = "Legalitas No : " + txtLegalitas.Text;
                Session["Text"] = txtSenderName.Text + " - " + txt;
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "colorBox", "var isColorbox = true", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('File not found!')", true);
            }
        }

        protected void lkbViewImageCompanyNPWP_Click(object sender, EventArgs e)
        {
            if (hfCompanyNPWPImageURL.Value != null)
            {
                Session["ImageURL"] = hfCompanyNPWPImageURL.Value;
                Session["From"] = "Image";
                string txt = "";
                txt = "NPWP : " + txtCompanyNPWP.Text;
                Session["Text"] = txtSenderName.Text + " - " + txt;
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "colorBox", "var isColorbox = true", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('File not found!')", true);
            }
        }

        protected void ddlBankAccount_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btnBack1_Click(object sender, EventArgs e)
        {
            diverror.Visible = false;
            divstep1.Visible = true;
            divstep2.Visible = false;
            divstep3.Visible = false;
        }

        protected void btnBack2_Click(object sender, EventArgs e)
        {
            diverror.Visible = false;
            divstep1.Visible = false;
            divstep2.Visible = true;
            divstep3.Visible = false;
        }

        protected void ddlRealtime_SelectedIndexChanged(object sender, EventArgs e)
        {
            bindBank();
        }

        protected void ddlPurpose_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btnCheckCity_Click(object sender, EventArgs e)
        {
            hfModalCityOpen.Value = "yes";
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "Pop", "var isModalCity = true", true);
        }

        protected void btnChooseCity_Click(object sender, EventArgs e)
        {
            System.Web.UI.WebControls.Button btn = (System.Web.UI.WebControls.Button)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            hfCityID.Value = ((HiddenField)row.FindControl("hfCityIDModal")).Value;
            hfCityName.Value = ((HiddenField)row.FindControl("hfCityNameModal")).Value;
            txtCity.Text = hfCityName.Value;
            txtFindCity.Text = "";
            gvListCity.DataSource = null;
            gvListCity.DataBind();
            gvListCity.Visible = false;
            hfModalCityOpen.Value = "no";
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "Pop", "var isModalCity = false", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "DoPostBack", "__doPostBack(sender, e)", true);
        }

        protected void btnFindCity_Click(object sender, EventArgs e)
        {
            var data = db.tblM_City.Where(p => p.City.Contains(txtFindCity.Text.Trim())).ToList();
            gvListCity.DataSource = data;
            gvListCity.DataBind();
            gvListCity.Visible = true;
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "Pop", "var isModalCity = true", true);
        }

        protected void lkbSenderWithID_Click(object sender, EventArgs e)
        {
            if (hfSenderWithIDImageURL.Value != null)
            {
                Session["ImageURL"] = hfSenderWithIDImageURL.Value;
                Session["From"] = "Image";
                string txt = "";
                txt = "Sender With ID Picture : " + txtNationalID.Text;
                Session["Text"] = txtSenderName.Text + " - " + txt;
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "colorBox", "var isColorbox = true", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('File not found!')", true);
            }
        }

        protected void lkbPICIDImage_Click(object sender, EventArgs e)
        {
            if (hfPICIDImageURL.Value != null)
            {
                Session["ImageURL"] = hfPICIDImageURL.Value;
                Session["From"] = "Image";
                string txt = "";
                txt = "PIC ID No : " + txtPICIDNo.Text;
                Session["Text"] = txtPICName.Text + " - " + txt;
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "colorBox", "var isColorbox = true", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('File not found!')", true);
            }
        }
    }
}