﻿using Newtonsoft.Json;
using OMGLiveDemo.Models;
using OMGLiveDemo.Models.OMG;
using OMGLiveDemo.Scripts;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.Payout
{
    public partial class IndonesiaPayout : System.Web.UI.Page
    {
        OMG omg = new OMG();
        List<Bank> bank = new List<Bank>();
        List<tblM_Beneficiary> benf = new List<tblM_Beneficiary>();
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();
        double amount = 0;
        double totalAmount = 0;
        double totalTransfer = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                diverror.Visible = false;
                if (Session["Payout"] != null)
                {
                    lblError.Text = Session["Payout"].ToString();
                    diverror.Visible = true;
                    Session["Payout"] = null;
                }

                if (SessionLib.Current.Role == Roles.MasterArea)
                {
                    getBalance();
                }

                if (!IsPostBack)
                {
                    bind();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        void getBalance()
        {
            long maid = Convert.ToInt64(SessionLib.Current.AdminID);
            var bal = db.tblM_Master_Area_Balance.Where(p => p.MasterAreaID == maid && p.CountryID == 103).FirstOrDefault();
            hfBalance.Value = bal.Balance.ToString();
            divbalance.Visible = true;
            lblBalance.Text = bal.Currency + " " + String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToDouble(hfBalance.Value)).Replace("Rp", "");
        }

        void bindBank() {
            //bank = omg.getListBank("indonesia");
            var isrtt = Convert.ToInt32(ddlRealtime.SelectedValue);
            var banks = db.tblM_Bank.Where(p => p.CountryID == 103 && p.isActive == 1 && p.isRealtime == isrtt).ToList();
            if (banks != null)
            {
                ddlBank.DataSource = null;
                ddlBank.DataSource = banks;
                ddlBank.DataValueField = "Code";
                ddlBank.DataTextField = "Name";
                ddlBank.DataBind();
            }
        }

        void bindPurpose()
        {
            var pur = db.tblM_Payout_Purpose.ToList();
            if (pur != null)
            {
                ddlPurpose.DataSource = null;
                ddlPurpose.DataSource = pur;
                ddlPurpose.DataValueField = "ID";
                ddlPurpose.DataTextField = "Purpose";
                ddlPurpose.DataBind();
            }
        }

        void bind()
        {
            long agid = Convert.ToInt64(SessionLib.Current.AdminID);

            bindBank();
            bindPurpose();

            var bankacc = db.vw_Bank_Account.Where(p => p.CountryID == 133).ToList();
            if (bankacc != null)
            {
                for (int i = 0; i < bankacc.Count; i++)
                {
                    bankacc[i].NameBank = bankacc[i].AccountHolder + " - " + bankacc[i].BankName;
                }

                ddlBankAccount.DataSource = null;
                ddlBankAccount.DataSource = bankacc;
                ddlBankAccount.DataValueField = "BankAccountID";
                ddlBankAccount.DataTextField = "NameBank";
                ddlBankAccount.DataBind();
            }

            if (SessionLib.Current.Role == Roles.Agent)
                benf = db.tblM_Beneficiary.Where(p => p.AgentID == agid && p.CountryID == 103 && p.isAgent == 1 && p.isActive == 1).ToList();
            else if (SessionLib.Current.Role == Roles.MasterArea)
                benf = db.tblM_Beneficiary.Where(p => p.AgentID == agid && p.CountryID == 103 && p.isAgent == 2 && p.isActive == 1).ToList();
            else
                benf = db.tblM_Beneficiary.Where(p => p.CountryID == 103 && p.isAgent == 0 && p.isActive == 1).ToList();

            if (benf.Count > 0)
            {
                for (int i = 0; i < benf.Count; i++)
                {
                    //benf[i].Index = i;
                    benf[i].NameBankAccount = benf[i].Name + " - " + benf[i].Bank + " - " + benf[i].Account;
                }
                ddlBeneficiary.DataSource = null;
                ddlBeneficiary.DataSource = benf;
                //ddlBeneficiary.DataValueField = "Index";
                ddlBeneficiary.DataValueField = "Account";
                ddlBeneficiary.DataTextField = "NameBankAccount";
                ddlBeneficiary.DataBind();
            }

            List<tblM_Sender> sender = new List<tblM_Sender>();
            if (SessionLib.Current.Role == Roles.Agent)
                sender = db.tblM_Sender.Where(p => p.AgentID == agid && p.CountryID == 133 && p.isActive == 1 && p.isAgent == 1 && p.Status == "approved").ToList();
            else if (SessionLib.Current.Role == Roles.MasterArea)
                sender = db.tblM_Sender.Where(p => p.AgentID == agid && p.CountryID == 133 && p.isActive == 1 && p.isAgent == 2 && p.Status == "approved").ToList();
            else
                sender = db.tblM_Sender.Where(p => p.CountryID == 133 && p.isActive == 1 && p.isAgent == 0 && p.Status == "approved").ToList();
            ddlSender.DataSource = null;
            ddlSender.DataSource = sender;
            ddlSender.DataValueField = "SenderID";
            ddlSender.DataTextField = "Fullname";
            ddlSender.DataBind();
            if (sender.Count != 0)
                setSender();

            var sendertype = db.tblM_Sender_Type.ToList();
            ddlSenderType.DataSource = null;
            ddlSenderType.DataSource = sendertype;
            ddlSenderType.DataValueField = "ID";
            ddlSenderType.DataTextField = "Type";
            ddlSenderType.DataBind();


            var cur = omg.getRateMYR2IDR();
            if (cur != null)
            {
                hfRate.Value = cur.Rate;
                hfRateOriginal.Value = cur.Rate;
            }

            if (SessionLib.Current.Role == Roles.Agent)
            {
                divrateall.Visible = false;
                var agentid = Int64.Parse(SessionLib.Current.AdminID);
                var rateagent = db.vw_Agent_Rate.Where(p => p.AgentID == agentid && p.RemittanceID == 1).FirstOrDefault();
                var rated = db.tblM_Rate.Where(p => p.RateID == rateagent.RateID).FirstOrDefault();
                hfRate.Value = (Double.Parse(hfRate.Value) - Double.Parse(rated.Margin)).ToString();
                hfMargin.Value = (Double.Parse(rateagent.AgentMargin)).ToString();
                if (rated != null)
                {
                    lblCurrency.Text = "1 MYR = " + String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(Int32.Parse(hfRate.Value) - Int32.Parse(hfMargin.Value))).Replace("Rp", "") + " IDR";
                    lblCurrencyUpdate.Text = "Last update : " + rated.UpdatedDate.Value.ToLongTimeString() + ", " + rated.UpdatedDate.Value.ToLongDateString();
                    //lblCurrency.Text = "1 MYR = " + String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(intcur)) + " IDR";
                }

                hfAdminFee.Value = Convert.ToString(rateagent.AdminFee);
                lblAdminFee.Text = rateagent.AdminFeeCurrency + " " + rateagent.AdminFee;
            }
            else if (SessionLib.Current.Role == Roles.MasterArea)
            {
                divrateall.Visible = false;
                var rateagent = db.vw_Master_Area_Rate.Where(p => p.MasterAreaID == agid && p.RemittanceID == 1).FirstOrDefault();
                hfRate.Value = (Double.Parse(hfRate.Value) - Double.Parse(rateagent.Margin)).ToString();
                hfMargin.Value = (Double.Parse(rateagent.MasterAreaMargin)).ToString();
                lblCurrency.Text = "1 MYR = " + String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(Int32.Parse(hfRate.Value) - Int32.Parse(hfMargin.Value))).Replace("Rp", "") + " IDR";
                lblCurrencyUpdate.Text = "Last update : " + cur.DateModified.ToLongTimeString() + ", " + cur.DateModified.ToLongDateString();

                hfAdminFee.Value = rateagent.Fee.ToString();
                lblAdminFee.Text = rateagent.FeeCurrency + " " + rateagent.Fee;

                divtransferto.Visible = false;

                getBalance();
            }
            else
            {
                //var mar = db.tblM_Rate.Where(p => p.RemittanceID == 1).First();
                //hfMargin.Value = mar.Margin;

                divrateagent.Visible = false;
                List<tblM_Rate> rate = new List<tblM_Rate>();
                rate = omg.getListRateMYRIDR();
                if (rate.Count > 0)
                {
                    for (int i = 0; i < rate.Count; i++)
                    {
                        rate[i].Rate = (Int32.Parse(hfRate.Value) - Int32.Parse(rate[i].Margin)).ToString();
                        rate[i].NameRate = rate[i].Name + " - Rp " + String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(Int32.Parse(rate[i].Rate))).Replace("Rp", "");
                        if (i == 0)
                            hfRate.Value = rate[i].Rate;
                    }
                    hfMargin.Value = "0";
                    ddlRate.DataSource = null;
                    ddlRate.DataSource = rate;
                    ddlRate.DataValueField = "Rate";
                    ddlRate.DataTextField = "NameRate";
                    ddlRate.DataBind();
                }

                divadminfee.Visible = true;
                var afee = db.tblM_Admin_Fee.Where(p => p.RemittanceID == 1).FirstOrDefault();
                hfAdminFee.Value = afee.Amount;
                lblAdminFees.Text = "RM " + afee.Amount;
            }
        }

        bool checkBeforeTransfer()
        {
            if (!checkSender()) { return true; }

            if (!checkBeneficiary()) { return true; }

            if (!checkPayout()) { return true; }

            if (SessionLib.Current.Role == Roles.MasterArea)
            {
                getBalance();
                var am = Convert.ToDouble(hfTotalTransfer.Value) + Convert.ToDouble(hfAdminFee.Value);
                if (am > Convert.ToDouble(hfBalance.Value))
                {
                    getBalance();
                    diverror.Visible = true;
                    lblError.Text = "your amount is not enough to process this transfer. please note that you need to pay an admin fee for IDR " + hfAdminFee.Value;
                    return true;
                }
            }

            return false;
        }

        protected void btnProcess_Click(object sender, EventArgs e)
        {
            diverror.Visible = false;
            try
            {
                if (checkBeforeTransfer())
                    return;

                tblT_Payout pay = new tblT_Payout();
                pay.RemittanceID = 1;
                pay.BeneficiaryName = txtName.Text.Replace("-", " ").Replace("&", " ").Replace("(", " ").Replace(")", " ").Replace("  ", " ").Trim();
                pay.BeneficiaryAccount = txtAccount.Text.Trim();

                if (ddlAmountType.SelectedValue == "1")
                {
                    pay.Amount = Convert.ToDouble(hfAmount.Value);
                    //pay.Amount = Convert.ToDouble(txtAmount.Text.Trim().Replace(",", "."));
                    pay.TotalAmount = Convert.ToDouble(hfTotalAmount.Value);
                    pay.TotalTransfer = Convert.ToDouble(hfTotalTransfer.Value);
                }
                else if (ddlAmountType.SelectedValue == "2")
                {
                    pay.Amount = Convert.ToDouble(hfAmount.Value);
                    pay.TotalAmount = Convert.ToDouble(hfTotalAmount.Value.ToString().Trim());
                    pay.TotalTransfer = Convert.ToDouble(hfTotalTransfer.Value);
                }
                pay.AdminFee = hfAdminFee.Value;
                pay.UniqueCode = "0";

                pay.BeneficiaryBank = ddlBank.SelectedValue;
                pay.BankName = ddlBank.SelectedItem.Text.Trim();
                pay.Currency = "MYR";
                if (txtNotes.Text.Trim().Length > 17)
                    pay.Notes = txtNotes.Text.Trim().Substring(0, 16);
                else
                    pay.Notes = txtNotes.Text.Trim();
                pay.isActive = 1;
                if (SessionLib.Current.Role == Roles.Agent)
                {
                    pay.BankAccountID = Convert.ToInt64(ddlBankAccount.SelectedValue);
                    pay.isAgent = 1;
                    pay.Rate = (Int32.Parse(hfRate.Value) - Int32.Parse(hfMargin.Value)).ToString();
                    pay.RateGiven = (Int32.Parse(hfRate.Value)).ToString();
                }
                else if (SessionLib.Current.Role == Roles.MasterArea)
                {
                    pay.BankAccountID = 0;
                    pay.isAgent = 2;
                    pay.Rate = (Int32.Parse(hfRate.Value) - Int32.Parse(hfMargin.Value)).ToString();
                    pay.RateGiven = (Int32.Parse(hfRate.Value)).ToString();
                }
                else
                {
                    pay.BankAccountID = Convert.ToInt64(ddlBankAccount.SelectedValue);
                    pay.isAgent = 0;
                    //pay.Rate = (Int32.Parse(hfRate.Value) - Int32.Parse(hfMargin.Value)).ToString();
                    pay.Rate = ddlRate.SelectedValue.ToString();
                    pay.RateGiven = (Int32.Parse(hfRateOriginal.Value)).ToString();
                }
                pay.CreatedBy = Int64.Parse(SessionLib.Current.AdminID);
                pay.CreatedDate = DateTime.Now;
                pay.Status = "created";
                pay.PurposeID = Convert.ToInt64(ddlPurpose.SelectedValue);
                db.SaveChanges();
                db.tblT_Payout.Add(pay);
                db.SaveChanges();

                if (SessionLib.Current.Role == Roles.MasterArea)
                {
                    masterAreaBalance(Int64.Parse(SessionLib.Current.AdminID), pay.PayoutID);
                }

                
                tblT_Payout_Sender ps = new tblT_Payout_Sender();
                ps.PayoutID = pay.PayoutID;
                ps.CityID = 0;
                ps.SenderTypeID = Convert.ToInt32(ddlSenderType.SelectedValue);
                if (ps.SenderTypeID == 1)
                    ps.SenderType = "Personal";
                else
                    ps.SenderType = "Company";
                ps.SenderName = txtSenderName.Text;
                ps.SenderAddress = txtSenderAddress.Text;
                ps.SenderPhone = txtSenderPhone.Text;
                ps.SenderEmail = txtSenderEmail.Text;
                ps.SenderFileID = txtNationalID.Text;

                db.tblT_Payout_Sender.Add(ps);
                db.SaveChanges();

                if (ddlChooseSender.SelectedValue == "2")
                {
                    if (ddlSenderType.SelectedValue == "1")
                    {
                        tblT_Payout_Sender_FIle psf = new tblT_Payout_Sender_FIle();
                        psf.PayoutSenderID = ps.PayoutSenderID;
                        psf.FileTypeID = 1;

                 
                        psf.Value = txtNationalID.Text.Trim();
                        ps.SenderFileIDURL = hfSenderImageURL.Value;
                        psf.URL = hfSenderImageURL.Value;

                        db.tblT_Payout_Sender_FIle.Add(psf);
                        db.SaveChanges();

                    }
                    else
                    {
                        tblT_Payout_Sender_FIle psf = new tblT_Payout_Sender_FIle();
                        psf.PayoutSenderID = ps.PayoutSenderID;
                        psf.FileTypeID = 2;

                 
                        psf.Value = txtSSM.Text.Trim();
                        ps.SenderFileIDURL = hfSenderImageURL.Value;
                        psf.URL = hfSenderImageURL.Value;

                        db.tblT_Payout_Sender_FIle.Add(psf);
                        db.SaveChanges();
                    }
                }
                else
                {
                    ps.SenderFileIDURL = hfSenderImageURL.Value;
                }
                
                tblT_Payout_Beneficiary pb = new tblT_Payout_Beneficiary();
                pb.PayoutID = pay.PayoutID;
                pb.CityID = Convert.ToInt64(hfCityID.Value);
                pb.BeneficiaryName = pay.BeneficiaryName;
                pb.BeneficiaryBank = pay.BeneficiaryBank;
                pb.BeneficiaryAccount = pay.BeneficiaryAccount;
                pb.BeneficiaryEmail = txtEmail.Text.Trim();
                pb.BeneficiaryPhone = txtPhone.Text.Trim();
                pb.BeneficiaryAddress = txtAddress.Text.Trim();
                pb.BeneficiaryFileID = pb.BeneficiaryAccount;
                pb.BankName = pay.BankName;
                db.tblT_Payout_Beneficiary.Add(pb);

                db.SaveChanges();

                senders();
                beneficiary();

                if (SessionLib.Current.Role == Roles.MasterArea)
                {
                    if (makePayout(pay))
                    {
                        Session["SuccessPayout"] = "Success created payout. Your request will update soon";
                        Response.Redirect("PayoutHistory", false);
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "Pop", "var isModal = false", true);
                        cancelTransfer();
                        return;
                    }
                }
                else
                {
                    Session["PayoutID"] = pay.PayoutID;
                    Response.Redirect("UploadReceipt", false);
                    // end changes //
                }
            }
            catch (Exception ex)
            {
                Session["Payout"] = "Error create payout. Message : " + ex.Message;
            }
        }

        void masterAreaBalance(long maid, long payid) {
            var bal = db.tblM_Master_Area_Balance.Where(p => p.MasterAreaID == maid && p.CountryID == 103).FirstOrDefault();
            
            tblH_Master_Area_Balance_Update hbal = new tblH_Master_Area_Balance_Update();
            hbal.BalanceID = bal.BalanceID;
            hbal.Type = "Payout";
            hbal.RemittanceID = 1;
            hbal.MasterAreaID = maid;
            hbal.PayoutID = payid;
            hbal.BankName = ddlBank.SelectedItem.Text.Trim();
            hbal.Account = txtAccount.Text.Trim();
            hbal.Beneficiary = txtName.Text.Trim();
            hbal.Amount = Convert.ToDouble(hfTotalTransfer.Value);
            hbal.AdminFee = Convert.ToDouble(hfAdminFee.Value);
            hbal.DateCreated = DateTime.Now;

            hbal.BalancePrevious = bal.Balance;
            hbal.Balance = bal.Balance - (Convert.ToDouble(hfTotalTransfer.Value) + Convert.ToDouble(hfAdminFee.Value));
            bal.Balance = hbal.Balance;
            bal.UpdatedDate = DateTime.Now;
            hbal.BalanceID = bal.BalanceID;

            db.tblH_Master_Area_Balance_Update.Add(hbal);
            db.SaveChanges();
        }

        bool makePayout(tblT_Payout pay)
        {
            var payid = pay.PayoutID;
            PayoutRequest pr = new PayoutRequest();
            pr.RemittanceID = 1;
            pr.Currency = "MYR";
            pr.AmountIDR = Convert.ToString(pay.TotalTransfer);
            pr.Amount = pay.Amount.Value.ToString().Replace(",", ".");
            pr.BeneficiaryName = pay.BeneficiaryName;
            pr.BeneficiaryBank = pay.BeneficiaryBank;
            pr.BeneficiaryAccount = pay.BeneficiaryAccount;
            pr.BeneficiaryEmail = pay.BeneficiaryEmail;
            pr.PartnerReferenceID = pay.PayoutID.ToString();
            pr.Notes = pay.Notes;

            var payben = db.tblT_Payout_Beneficiary.Where(p => p.PayoutID == payid).FirstOrDefault();
            Beneficiary ben = new Beneficiary();
            ben.Name = payben.BeneficiaryName;
            ben.Address = payben.BeneficiaryAddress;
            ben.Email = payben.BeneficiaryEmail;
            ben.Phone = payben.BeneficiaryPhone;
            //ben.Identity = payben.BeneficiaryFileID;
            ben.Identity = pr.BeneficiaryAccount;
            pr.Beneficiary = ben;

            var paysen = db.tblT_Payout_Sender.Where(p => p.PayoutID == payid).FirstOrDefault();
            Models.OMG.Sender sen = new Models.OMG.Sender();
            sen.Name = paysen.SenderName;
            sen.Address = paysen.SenderAddress;
            sen.Email = paysen.SenderEmail;
            sen.Phone = paysen.SenderPhone;
            sen.Identity = paysen.SenderFileID;
            pr.Sender = sen;

            OutputModel output = omg.createApprovePayoutNew(pr);
            if (output.status != "error")
            {
                PayoutResponse prs = JsonConvert.DeserializeObject<PayoutResponse>(output.data.ToString());
                pay.Reference = prs.ReferenceID;
                pay.Status = prs.Status;
                pay.ModifiedDate = prs.DateModified;
                db.SaveChanges();

                var snd = db.tblT_Payout_Sender.Where(p => p.PayoutID == pay.PayoutID).FirstOrDefault();
                snd.Reference = pay.Reference;

                var bnf = db.tblT_Payout_Beneficiary.Where(p => p.PayoutID == pay.PayoutID).FirstOrDefault();
                bnf.Reference = pay.Reference;

                var hbal = db.tblH_Master_Area_Balance_Update.Where(p => p.PayoutID == pay.PayoutID).FirstOrDefault();
                hbal.Reference = pay.Reference;

                db.SaveChanges();

                omg.sendEmailMasterArea(pay);

                return true;
            }
            else
            {
                diverror.Visible = true;
                lblError.Text = output.message;
                return false;
            }
            
        }

        protected void ddlBank_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void ddlBeneficiaryType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlBeneficiaryType.SelectedValue == "-1")
            {
                divbenf.Visible = false;
            }
            if (ddlBeneficiaryType.SelectedValue == "1")
            {
                divbenf.Visible = true;
                divbeneficiary.Visible = true;
                divvalidate.Visible = false;
                txtName.Enabled = false;
                txtAccount.Enabled = false;
                ddlBank.Enabled = false;
                txtCity.Text = "";
                cbSaveBenfeficiary.Visible = false;
                cbUpdateBeneficiary.Visible = true;

                txtAddress.Enabled = false;
                ddlRealtime.Enabled = false;

                if (ddlBeneficiary.SelectedItem != null)
                    setBeneficiary();
            }
            else if (ddlBeneficiaryType.SelectedValue == "2")
            {
                divbenf.Visible = true;
                divbeneficiary.Visible = false;
                divvalidate.Visible = true;
                txtName.Enabled = false;
                txtAccount.Enabled = true;
                ddlBank.Enabled = true;
                cbSaveBenfeficiary.Visible = true;
                cbUpdateBeneficiary.Visible = false;

                txtAddress.Enabled = true;
                ddlRealtime.Enabled = true;
                txtCity.Text = "";
            }
        }

        protected void btnValidate_Click(object sender, EventArgs e)
        {
            BankAccount ba = new BankAccount();
            ba.CountryID = 103;
            ba.AccountNo = txtAccount.Text.Trim();
            ba.BankCode = ddlBank.SelectedValue;
            var acc = omg.validateAccount(ba);
            if (acc != null)
            {
                txtName.Text = acc.AccountName.Replace("-", " ").Replace("&", " ").Replace("(", " ").Replace(")", " ").Replace("  ", " ").Trim();
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Found " + acc.AccountName + "')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('The account of " + ba.AccountNo + " and bank of " + ddlBank.SelectedItem + " is not found')", true);
            }
        }

        void senders()
        {
            try
            {
                var id = Int64.Parse(SessionLib.Current.AdminID);
                int isAgent = 0;
                if (SessionLib.Current.Role == Roles.Agent)
                {
                    isAgent = 1;
                }
                if (SessionLib.Current.Role == Roles.MasterArea)
                {
                    isAgent = 2;
                }

                var check = db.tblM_Sender.Where(p => p.Phone == txtSenderPhone.Text.Trim() && p.AgentID == id && p.isAgent == isAgent && p.CountryID == 133).FirstOrDefault();
                if (check == null)
                {

                    tblM_Sender bnf = new tblM_Sender();
                    bnf.Fullname = txtSenderName.Text.Trim();
                    bnf.Address = txtSenderAddress.Text.Trim();
                    bnf.Phone = txtSenderPhone.Text.Trim();
                    bnf.Email = txtSenderEmail.Text.Trim();
                    bnf.CountryID = 133;
                    bnf.Country = "Malaysia";
                    bnf.JoinDate = DateTime.Now;
                    bnf.AddedBy = Convert.ToInt64(SessionLib.Current.AdminID);
                    bnf.Email = txtEmail.Text.Trim();
                    bnf.SenderTypeID = Convert.ToInt64(ddlSenderType.SelectedValue);
                    bnf.CityID = 0;

                    bnf.isAgent = isAgent;
                    bnf.AgentID = id;
                    bnf.AddedBy = bnf.AgentID;
                    bnf.isActive = 1;
                    bnf.Status = "created";
                    db.tblM_Sender.Add(bnf);
                    db.SaveChanges();

                    if (bnf.SenderTypeID == 1)
                    {
                        var ftype = db.tblM_Sender_File_Type.Where(p => p.SenderTypeID == bnf.SenderTypeID && p.CountryID == 133).FirstOrDefault();
                        tblM_Sender_File sf = new tblM_Sender_File();
                        sf.SenderID = bnf.SenderID;
                        sf.FileTypeID = ftype.ID;
                        sf.Value = txtNationalID.Text.ToString().Trim();

                        
                        sf.URL = hfSenderImageURL.Value;

                        db.tblM_Sender_File.Add(sf);
                    }
                    else
                    {
                        var ftype = db.tblM_Sender_File_Type.Where(p => p.SenderTypeID == bnf.SenderTypeID && p.CountryID == 133).FirstOrDefault();
                        tblM_Sender_File sf = new tblM_Sender_File();
                        sf.SenderID = bnf.SenderID;
                        sf.FileTypeID = ftype.ID;
                        sf.Value = txtSSM.Text.ToString().Trim();
                        sf.URL = hfSenderImageURL.Value;

                        db.tblM_Sender_File.Add(sf);
                    }

                    db.SaveChanges();
                }
                else
                {
                    Session["ErrorSender"] = "Failed save sender because the sender already registered";
                }
                //}
            }
            catch (Exception ex)
            {
                Session["ErrorSender"] = "Failed save sender for some reason : " + ex.Message.ToString();
            }
        }

        void beneficiary()
        {
            try
            {
                int cid = Int32.Parse(SessionLib.Current.AdminID);
                int isagnt = 0;
                if (SessionLib.Current.Role == Roles.Agent)
                    isagnt = 1;
                else if (SessionLib.Current.Role == Roles.MasterArea)
                    isagnt = 2;

                tblM_Beneficiary bnf = new tblM_Beneficiary();
                if (ddlBeneficiaryType.SelectedValue == "1")
                {
                    if (cbUpdateBeneficiary.Checked)
                    {
                        bnf = db.tblM_Beneficiary.Where(p => p.Account == txtAccount.Text.Trim() && p.BankCode == ddlBank.SelectedValue && p.isAgent == isagnt && p.CreatedBy == cid && p.isActive == 1).FirstOrDefault();
                        string paybef = JsonConvert.SerializeObject(benf);
                        bnf.Alias = txtAlias.Text.Trim();
                        bnf.Phone = txtPhone.Text.Trim();
                        bnf.Email = txtEmail.Text.Trim();
                        bnf.CityID = Convert.ToInt64(hfCityID.Value);
                        bnf.Address = txtAddress.Text.Trim();
                        bnf.UpdatedDate = DateTime.Now;
                        bnf.UpdatedBy = cid;
                        string payaft = JsonConvert.SerializeObject(benf);

                        tblH_Beneficiary_Update hbnf = new tblH_Beneficiary_Update();
                        hbnf.AdminID = cid;
                        hbnf.BeneficiaryID = bnf.BeneficiaryID;
                        hbnf.PayloadBefore = paybef;
                        hbnf.PayloadAfter = payaft;
                        hbnf.UpdatedDate = bnf.UpdatedDate;
                        db.tblH_Beneficiary_Update.Add(hbnf);

                        db.SaveChanges();
                    }
                }
                else if (ddlBeneficiaryType.SelectedValue == "2")
                {
                    if (cbSaveBenfeficiary.Checked)
                    {
                        bnf = db.tblM_Beneficiary.Where(p => p.Account == txtAccount.Text.Trim() && p.BankCode == ddlBank.SelectedValue && p.isAgent == isagnt && p.CreatedBy == cid && p.isActive == 1).FirstOrDefault();
                        if (bnf == null)
                        {
                            bnf.Name = txtName.Text.Trim();
                            bnf.Account = txtAccount.Text;
                            bnf.Bank = ddlBank.SelectedItem.Text;
                            bnf.BankCode = ddlBank.SelectedValue;
                            bnf.CityID = Convert.ToInt64(hfCityID.Value);
                            bnf.Alias = txtAlias.Text.Trim();
                            bnf.Phone = txtPhone.Text.Trim();
                            bnf.Email = txtEmail.Text.Trim();
                            bnf.Address = txtAddress.Text.Trim();
                            bnf.CountryID = 103;
                            bnf.isActive = 1;
                            bnf.isAgent = isagnt;
                            bnf.CreatedDate = DateTime.Now;
                            bnf.CreatedBy = cid;
                            db.tblM_Beneficiary.Add(bnf);
                            db.SaveChanges();
                        }
                        else
                        {
                            Session["ErrorBeneficiary"] = "Failed save beneficiary because the beneficiary already registered";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Session["ErrorBeneficiary"] = "Failed save beneficiary for some reason : " + ex.Message.ToString();
            }
        }

        protected void txtAmount_TextChanged(object sender, EventArgs e)
        {
            countAmount();
        }

        void countAmount()
        {
            if (txtAmount.Text != "")
            {
                if (ddlAmountType.SelectedValue == "1")
                {
                    double am = double.Parse(txtAmount.Text, CultureInfo.InvariantCulture.NumberFormat);
                    if (SessionLib.Current.Role == Roles.MasterArea)
                        totalAmount = am;
                    else
                        totalAmount = am + double.Parse(hfAdminFee.Value);
                    if (SessionLib.Current.Role == Roles.Agent || SessionLib.Current.Role == Roles.MasterArea)
                    {
                        totalTransfer = am * (double.Parse(hfRate.Value) - double.Parse(hfMargin.Value));
                    }
                    else
                    {
                        totalTransfer = am * (double.Parse(hfRate.Value));
                    }
                    hfAmount.Value = am.ToString();
                    hfTotalAmount.Value = totalAmount.ToString();
                    hfTotalTransfer.Value = totalTransfer.ToString();
                    if (SessionLib.Current.Role == Roles.MasterArea)
                        lblTotalAmount.Text = String.Format(new CultureInfo("ms-MY"), "{0:c}", Convert.ToDouble(hfAmount.Value)).Replace("RM", "RM ");
                    else
                        lblTotalAmount.Text = String.Format(new CultureInfo("ms-MY"), "{0:c}", Convert.ToDouble(totalAmount)).Replace("RM", "RM ");
                    lblTotalTransfer.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToDouble(totalTransfer)).Replace("Rp", "Rp ");
                }
                else if (ddlAmountType.SelectedValue == "2")
                {
                    double am = double.Parse(txtAmount.Text, CultureInfo.InvariantCulture.NumberFormat);
                    totalTransfer = am;
                    if (SessionLib.Current.Role == Roles.Agent || SessionLib.Current.Role == Roles.MasterArea)
                        amount = am / (double.Parse(hfRate.Value) - double.Parse(hfMargin.Value));
                    else
                        amount = am / (double.Parse(hfRate.Value));

                    if (SessionLib.Current.Role == Roles.MasterArea)
                        totalAmount = amount;
                    else
                        totalAmount = amount + double.Parse(hfAdminFee.Value);

                    hfAmount.Value = amount.ToString();
                    hfTotalAmount.Value = totalAmount.ToString();
                    hfTotalTransfer.Value = totalTransfer.ToString();
                    if (SessionLib.Current.Role == Roles.MasterArea)
                        lblTotalAmount.Text = String.Format(new CultureInfo("ms-MY"), "{0:c}", Convert.ToDouble(hfAmount.Value)).Replace("RM", "RM ");
                    else
                        lblTotalAmount.Text = String.Format(new CultureInfo("ms-MY"), "{0:c}", Convert.ToDouble(totalAmount)).Replace("RM", "RM ");
                    lblTotalTransfer.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToDouble(totalTransfer)).Replace("Rp", "Rp ");
                }
            }
        }

        protected void btnError_ServerClick(object sender, EventArgs e)
        {
            diverror.Visible = false;
        }

        protected void ddlBeneficiary_SelectedIndexChanged(object sender, EventArgs e)
        {
            setBeneficiary();
        }

        void setBeneficiary()
        {
            tblM_Beneficiary bf = new tblM_Beneficiary();
            string account = ddlBeneficiary.SelectedValue;
            long id = Convert.ToInt64(SessionLib.Current.AdminID);
            
            if (SessionLib.Current.Role == Roles.Agent)
                bf = db.tblM_Beneficiary.Where(p => p.CountryID == 103 && p.Account == account && p.isAgent == 1 && p.AgentID == id && p.isActive == 1).FirstOrDefault();
            else if (SessionLib.Current.Role == Roles.MasterArea)
                bf = db.tblM_Beneficiary.Where(p => p.CountryID == 103 && p.Account == account && p.isAgent == 2 && p.AgentID == id && p.isActive == 1).FirstOrDefault();
            else
                bf = db.tblM_Beneficiary.Where(p => p.CountryID == 103 && p.Account == account && p.isAgent == 0 && p.isActive == 1).FirstOrDefault();

            if (bf != null)
            {
                txtName.Text = bf.Name;
                txtAccount.Text = bf.Account;
                ddlBank.Text = bf.BankCode;
                txtAlias.Text = bf.Alias;
                txtPhone.Text = bf.Phone;
                txtEmail.Text = bf.Email;
                txtAddress.Text = bf.Address;
                if (bf.CityID != null)
                {
                    var city = db.tblM_City.Where(p => p.ID == bf.CityID).FirstOrDefault();
                    hfCityID.Value = city.ID.ToString();
                    hfCityName.Value = city.City;
                    txtCity.Text = city.City;
                }
                //txtBeneficiaryNationalID.Text = bf.FileID;
            }
        }

        protected void ddlAmountType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlAmountType.SelectedValue == "1")
            {
                lblAmount.Text = "Amount (in MYR) *";
            }
            else if (ddlAmountType.SelectedValue == "2")
            {
                lblAmount.Text = "Amount (in IDR) *";
            }
            countAmount();
        }

        protected void ddlRate_SelectedIndexChanged(object sender, EventArgs e)
        {
            hfRate.Value = ddlRate.SelectedValue;
            countAmount();
        }

        protected void ddlSenderType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSenderType.SelectedValue == "1")
            {
                divpersonal.Visible = true;
                divcompany.Visible = false;
                txtNationalID.Text = "";
            }
            else if (ddlSenderType.SelectedValue == "2")
            {
                divpersonal.Visible = false;
                divcompany.Visible = true;
                txtSSM.Text = "";
            }
        }

        protected void ddlSender_SelectedIndexChanged(object sender, EventArgs e)
        {
            setSender();
        }

        protected void ddlChooseSender_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlChooseSender.SelectedValue == "-1")
            {
                divsender.Visible = false;
            }
            if (ddlChooseSender.SelectedValue == "1")
            {
                divsender.Visible = true;
                divchoosesender.Visible = true;
                txtSenderName.Enabled = false;
                txtSenderEmail.Enabled = false;
                txtSenderPhone.Enabled = false;
                txtSenderAddress.Enabled = false;
                ddlSenderType.Enabled = false;
                
                divpersonal.Visible = true;
                txtNationalID.Enabled = false;
                fuNationalID.Visible = false;
                lkbNationalID.Visible = true;

                divcompany.Visible = false;
                txtSSM.Enabled = false;
                fuSSM.Visible = false;
                lkbSSM.Visible = true;
                hfSenderImageURL.Value = "";
                if (ddlSender.SelectedItem != null)
                    setSender();
            }
            else if (ddlChooseSender.SelectedValue == "2")
            {
                divsender.Visible = true;
                divchoosesender.Visible = false;
                txtSenderName.Enabled = true;
                txtSenderName.Text = "";
                txtSenderEmail.Enabled = true;
                txtSenderEmail.Text = "";
                txtSenderPhone.Enabled = true;
                txtSenderPhone.Text = "";
                txtSenderAddress.Enabled = true;
                txtSenderAddress.Text = "";
                ddlSenderType.Enabled = true;
                
                divpersonal.Visible = true;
                txtNationalID.Enabled = true;
                txtNationalID.Text = "";
                fuNationalID.Visible = true;
                lkbNationalID.Visible = false;

                divcompany.Visible = false;
                txtSSM.Enabled = true;
                txtSSM.Text = "";
                fuSSM.Visible = true;
                lkbSSM.Visible = false;
                hfSenderImageURL.Value = "";
            }
        }

        void setSender()
        {
            var id = Convert.ToInt64(ddlSender.SelectedValue);
            var agid = Convert.ToInt64(SessionLib.Current.AdminID);
            tblM_Sender bf = new tblM_Sender();
            if (SessionLib.Current.Role == Roles.Agent)
                bf = db.tblM_Sender.Where(p => p.CountryID == 133 && p.SenderID == id && p.isAgent == 1 && p.AgentID == agid).FirstOrDefault();
            else if (SessionLib.Current.Role == Roles.MasterArea)
                bf = db.tblM_Sender.Where(p => p.CountryID == 133 && p.SenderID == id && p.isAgent == 2 && p.AgentID == agid).FirstOrDefault();
            else
                bf = db.tblM_Sender.Where(p => p.CountryID == 133 && p.SenderID == id && p.isAgent == 0).FirstOrDefault();
            if (bf != null)
            {
                txtSenderName.Text = bf.Fullname;
                ddlSenderType.Text = bf.SenderTypeID.ToString();
                txtSenderPhone.Text = bf.Phone;
                txtSenderEmail.Text = bf.Email;
                txtSenderAddress.Text = bf.Address;

                var file = db.tblM_Sender_File.Where(p => p.SenderID == id).FirstOrDefault();
                if (file != null)
                {
                    if (file.FileTypeID == 1)
                    {
                        txtNationalID.Text = file.Value;
                        divpersonal.Visible = true;
                        divcompany.Visible = false;
                        txtNationalID.Enabled = false;
                        fuNationalID.Visible = false;
                        lkbNationalID.Visible = true;
                    }
                    else
                    {
                        txtSSM.Text = file.Value;
                        divpersonal.Visible = false;
                        divcompany.Visible = true;
                        txtSSM.Enabled = false;
                        fuSSM.Visible = false;
                        lkbSSM.Visible = true;
                    }
                    hfSenderImageURL.Value = file.URL;
                }
            }
        }

        void doTransfer() {
            btnProcess.Visible = false;
            btnBack2.Visible = false;
            dvProcessing.Visible = true;

            ddlAmountType.Enabled = false;
            txtAmount.Enabled = false;
            txtNotes.Enabled = false;
        }

        void cancelTransfer() {
            btnProcess.Visible = true;
            btnBack2.Visible = true;
            dvProcessing.Visible = false;

            ddlAmountType.Enabled = true;
            txtAmount.Enabled = true;
            txtNotes.Enabled = true;
        }

        protected void btnConfirmTransafer_Click(object sender, EventArgs e)
        {
            diverror.Visible = false;
            if (checkBeforeTransfer())
                return;

            ScriptManager.RegisterClientScriptBlock(this, GetType(), "Pop", "var isModal = true", true);
            doTransfer();
        }

        protected void btnCancelTransafer_Click(object sender, EventArgs e)
        {
            diverror.Visible = false;
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "Pop", "var isModal = false", true);
            cancelTransfer();
        }

        protected void btnNext1_Click(object sender, EventArgs e)
        {
            diverror.Visible = false;
            if (checkSender())
            {
                saveSenderFile();
                divstep2.Visible = true;
                divstep1.Visible = false;
                divstep3.Visible = false;
            }
        }

        protected void btnNext2_Click(object sender, EventArgs e)
        {
            diverror.Visible = false;
            if (checkBeneficiary())
            {
                divstep3.Visible = true;
                divstep1.Visible = false;
                divstep2.Visible = false;
            }
        }

        bool checkSender()
        {
            try
            {
                if (ddlChooseSender.SelectedValue == "-1")
                {
                    diverror.Visible = true;
                    lblError.Text = "Please choose sender first!";
                    return false;
                }
                else if (ddlChooseSender.SelectedValue == "1" || ddlChooseSender.SelectedValue == "2")
                {
                    if (txtSenderName.Text == "")
                    {
                        diverror.Visible = true;
                        lblError.Text = "Sender name cannot be empty!";
                        return false;
                    }

                    if (txtSenderAddress.Text == "")
                    {
                        diverror.Visible = true;
                        lblError.Text = "Sender address cannot be empty!";
                        return false;
                    }

                    if (txtSenderAddress.Text.Count() < 10)
                    {
                        diverror.Visible = true;
                        lblError.Text = "Sender address is too short! Please add your address more than 10 chars.";
                        return false;
                    }

                    if (txtSenderPhone.Text == "")
                    {
                        diverror.Visible = true;
                        lblError.Text = "Sender phone cannot be empty!";
                        return false;
                    }

                    if (txtSenderEmail.Text == "")
                    {
                        diverror.Visible = true;
                        lblError.Text = "Sender email cannot be empty!";
                        return false;
                    }

                    if (ddlSenderType.SelectedValue == "1")
                    {
                        if (txtNationalID.Text == "")
                        {
                            diverror.Visible = true;
                            lblError.Text = "National ID / Passport cannot be empty!";
                            return false;
                        }

                        if (ddlChooseSender.SelectedValue == "2")
                        {
                            if (hfSenderImageURL.Value == null || hfSenderImageURL.Value == "")
                            {
                                if (!fuNationalID.HasFile)
                                {
                                    lblError.Text = "Please choose photo of your KTP / Passport first";
                                    diverror.Visible = true;
                                    return false;
                                }
                                else
                                {
                                    if (fuNationalID.PostedFile.ContentType != "image/jpg" && fuNationalID.PostedFile.ContentType != "image/jpeg" && fuNationalID.PostedFile.ContentType != "image/png")
                                    {
                                        lblError.Text = "Only jpeg, jpg and png format are allowed";
                                        diverror.Visible = true;
                                        return false;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (txtSSM.Text == "")
                        {
                            diverror.Visible = true;
                            lblError.Text = "SSM Number cannot be empty!";
                            return false;
                        }

                        if (ddlChooseSender.SelectedValue == "2")
                        {
                            if (hfSenderImageURL.Value == null || hfSenderImageURL.Value == "")
                            {
                                if (!fuSSM.HasFile)
                                {
                                    lblError.Text = "Please choose photo of your SSM first";
                                    diverror.Visible = true;
                                    return false;
                                }
                                else
                                {
                                    if (fuSSM.PostedFile.ContentType != "image/jpg" && fuSSM.PostedFile.ContentType != "image/jpeg" && fuSSM.PostedFile.ContentType != "image/png")
                                    {
                                        lblError.Text = "Only jpeg, jpg and png format are allowed";
                                        diverror.Visible = true;
                                        return false;
                                    }
                                }
                            }
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        bool checkBeneficiary()
        {
            if (txtName.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Beneficiary Name cannot be empty!";
                return false;
            }

            if (txtAccount.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Beneficiary Account cannot be empty!";
                return false;
            }

            if (txtCity.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Please choose beneficiary's city first!";
                return false;
            }

            if (txtAddress.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Beneficiary Address cannot be empty!";
                return false;
            }

            if (txtEmail.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Beneficiary email cannot be empty!";
                return false;
            }

            if (txtPhone.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Beneficiary phone number cannot be empty!";
                return false;
            }
            
            return true;
        }

        bool checkPayout()
        {
            if (ddlAmountType.SelectedValue == "-1")
            {
                diverror.Visible = true;
                lblError.Text = "You should choose Amount Type first!";
                return false;
            }

            if (txtAmount.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Amount cannot be empty!";
                return false;
            }

            if (SessionLib.Current.Role != Roles.MasterArea)
            {
                if (ddlBankAccount.Text == "")
                {
                    diverror.Visible = true;
                    lblError.Text = "Please choose the bank account that you would transfer to";
                    return false;
                }
            }

            if (ddlPurpose.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Transfer purpose cannot be empty!";
                return false;
            }

            if (txtNotes.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Notes cannot be empty!";
                return false;
            }

            if (txtNotes.Text.Length > 17)
            {
                diverror.Visible = true;
                lblError.Text = "Only 17 characters allowed for Notes!";
                return false;
            }

            if (Convert.ToDouble(hfTotalTransfer.Value) < 10000.00)
            {
                diverror.Visible = true;
                lblError.Text = "Minimum amount for transfer is Rp 10.000 per transaction!";
                return false;
            }

            if (ddlBank.SelectedValue == "bri" && Convert.ToDouble(hfTotalTransfer.Value) > 100000000.00)
            {
                diverror.Visible = true;
                lblError.Text = "Maximum amount for transfer to BRI account is Rp 100.000.000 per transaction!";
                return false;
            }

            return true;
        }

        protected void lkbViewImage_Click(object sender, EventArgs e)
        {
            if (hfSenderImageURL.Value != null)
            {
                Session["ImageURL"] = hfSenderImageURL.Value;
                Session["From"] = "Image";
                string txt = "";
                if (ddlSenderType.SelectedValue == "1")
                {
                    txt = "NationalID/Passport : " + txtNationalID.Text;
                }
                else
                {
                    txt = "SSM : " + txtSSM.Text;
                }
                Session["Text"] = txtSenderName.Text + " - " + txt;
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "colorBox", "var isColorbox = true", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('File not found!')", true);
            }
        }

        protected void ddlBankAccount_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btnBack1_Click(object sender, EventArgs e)
        {
            diverror.Visible = false;
            divstep1.Visible = true;
            divstep2.Visible = false;
            divstep3.Visible = false;
        }

        protected void btnBack2_Click(object sender, EventArgs e)
        {
            diverror.Visible = false;
            divstep1.Visible = false;
            divstep2.Visible = true;
            divstep3.Visible = false;
        }

        protected void ddlRealtime_SelectedIndexChanged(object sender, EventArgs e)
        {
            bindBank();
        }

        protected void ddlPurpose_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btnCheckCity_Click(object sender, EventArgs e)
        {
            hfModalCityOpen.Value = "yes";
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "Pop", "var isModalCity = true", true);
        }

        protected void btnChooseCity_Click(object sender, EventArgs e)
        {
            System.Web.UI.WebControls.Button btn = (System.Web.UI.WebControls.Button)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            hfCityID.Value = ((HiddenField)row.FindControl("hfCityIDModal")).Value;
            hfCityName.Value = ((HiddenField)row.FindControl("hfCityNameModal")).Value;
            txtCity.Text = hfCityName.Value;
            txtFindCity.Text = "";
            gvListCity.DataSource = null;
            gvListCity.DataBind();
            gvListCity.Visible = false;
            hfModalCityOpen.Value = "no";
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "Pop", "var isModalCity = false", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "DoPostBack", "__doPostBack(sender, e)", true);
        }

        protected void btnFindCity_Click(object sender, EventArgs e)
        {
            var data = db.tblM_City.Where(p => p.City.Contains(txtFindCity.Text.Trim())).ToList();
            gvListCity.DataSource = data;
            gvListCity.DataBind();
            gvListCity.Visible = true;
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "Pop", "var isModalCity = true", true);
        }

        void saveSenderFile()
        {
            if (ddlSenderType.SelectedValue == "1")
            {
                var sendtype = db.tblM_Sender_File_Type.Where(p => p.CountryID == 133 && p.SenderTypeID == 1).ToList();
                for (int i = 0; i < sendtype.Count; i++)
                {
                    if (sendtype[i].ID == 1)
                    {
                        if (hfSenderImageURL.Value == "" || hfSenderImageURL.Value == null)
                        {
                            string filename = "Temporary_NationalID_" + Common.getTimeSpan() + "." + fuNationalID.PostedFile.ContentType.Replace("image/", "");
                            string targetPath = Server.MapPath("../Sender/Picture/Malaysia/Personal/" + filename);
                            Stream strm = fuNationalID.PostedFile.InputStream;
                            Common.SaveImage(0.5, strm, targetPath);

                            hfSenderImageURL.Value = ConfigurationManager.AppSettings["URL_SENDER_MALYSIA_PERSONAL"].ToString() + filename;

                            txtNationalID.Enabled = false;
                            fuNationalID.Visible = false;
                            lkbNationalID.Visible = true;
                        }
                    }
                }
            }
            if (ddlSenderType.SelectedValue == "2")
            {
                var sendtype = db.tblM_Sender_File_Type.Where(p => p.CountryID == 133 && p.SenderTypeID == 2).ToList();
                for (int i = 0; i < sendtype.Count; i++)
                {
                    if (sendtype[i].ID == 2)
                    {
                        if (hfSenderImageURL.Value == "" || hfSenderImageURL.Value == null)
                        {
                            string filename = "Temporary_SSM_" + Common.getTimeSpan() + "." + fuSSM.PostedFile.ContentType.Replace("image/", "");
                            string targetPath = Server.MapPath("../Sender/Picture/Malaysia/Company/" + filename);
                            Stream strm = fuSSM.PostedFile.InputStream;
                            Common.SaveImage(0.5, strm, targetPath);

                            hfSenderImageURL.Value = ConfigurationManager.AppSettings["URL_SENDER_MALYSIA_COMPANY"].ToString() + filename;

                            //default doc
                            hfSenderImageURL.Value = ConfigurationManager.AppSettings["URL_SENDER_MALYSIA_COMPANY"].ToString() + filename;

                            txtSSM.Enabled = false;
                            fuSSM.Visible = false;
                            lkbSSM.Visible = true;
                        }
                    }
                }
            }
        }
    }
}