﻿using OMGLiveDemo.Models;
using OMGLiveDemo.Scripts;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo
{
    public partial class Calculator : System.Web.UI.Page
    {
        OMG omg = new OMG();
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();
        tblM_Rate rate = new tblM_Rate();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    checkRemit();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        void checkRemit()
        {
            var id = Int64.Parse(SessionLib.Current.AdminID);

            if (SessionLib.Current.Role == Roles.Agent)
            {
                var rl = db.vw_Agent_Rate.Where(p => p.AgentID == id).ToList();
                for (int i = 0; i < rl.Count; i++)
                {
                    if (rl[i].RemittanceID == 1)
                    {
                        rl[i].RemittanceLine = "Malaysia to Indonesia";
                        countMYR();
                        if (i == 0)
                            divmyr.Visible = true;
                    }
                    if (rl[i].RemittanceID == 2)
                    {
                        rl[i].RemittanceLine = "Indonesia to Malaysia";
                        countIDR();
                        if (i == 0)
                            dividr.Visible = true;
                    }
                }
                ddlRemittanceLine.DataSource = null;
                ddlRemittanceLine.DataSource = rl;
                ddlRemittanceLine.DataValueField = "RemittanceID";
                ddlRemittanceLine.DataTextField = "RemittanceLine";
                ddlRemittanceLine.DataBind();
            }
            else if (SessionLib.Current.Role == Roles.MasterArea)
            {
                var rl = db.vw_Master_Area_Rate.Where(p => p.MasterAreaID == id).ToList();
                for (int i = 0; i < rl.Count; i++)
                {
                    if (rl[i].RemittanceID == 1)
                    {
                        rl[i].RemittanceLine = "Malaysia to Indonesia";
                        countMYR();
                        if (i == 0)
                            divmyr.Visible = true;
                    }
                    if (rl[i].RemittanceID == 2)
                    {
                        rl[i].RemittanceLine = "Indonesia to Malaysia";
                        countIDR();
                        if (i == 0)
                            dividr.Visible = true;
                    }
                }
                ddlRemittanceLine.DataSource = null;
                ddlRemittanceLine.DataSource = rl;
                ddlRemittanceLine.DataValueField = "RemittanceID";
                ddlRemittanceLine.DataTextField = "RemittanceLine";
                ddlRemittanceLine.DataBind();
            }

        }

        void countMYR()
        {
            var cur = omg.getRateMYR2IDR();
            if (cur != null)
            {
                hfRateMYR.Value = cur.Rate;
            }

            var agentid = Int64.Parse(SessionLib.Current.AdminID);
            if (SessionLib.Current.Role == Roles.Agent)
            {
                var rateagent = db.tblM_Agent_Rate.Where(p => p.AgentID == agentid && p.RemittanceID == 1).FirstOrDefault();
                rate = db.tblM_Rate.Where(p => p.RateID == rateagent.RateID).FirstOrDefault();
                hfRateMYR.Value = (Double.Parse(hfRateMYR.Value) - Double.Parse(rate.Margin)).ToString();
                if (rateagent != null)
                {
                    lblCurrencyMYR.Text = rate.Currency;

                    lblBaseRateMYR.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(hfRateMYR.Value)).Replace("Rp", "Rp ");
                    lblCountryMYR.Text = "Malaysia to Indonesia";

                    lblBaseRateMYR.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64((Int32.Parse(hfRateMYR.Value) - Int32.Parse(rateagent.Margin)))).Replace("Rp", "Rp ");
                    hfRateMYR.Value = (Convert.ToInt64((Int32.Parse(hfRateMYR.Value) - Int32.Parse(rateagent.Margin)))).ToString();
                }
            }
            else if (SessionLib.Current.Role == Roles.MasterArea)
            {
                var rateagent = db.tblM_Master_Area_Rate.Where(p => p.MasterAreaID == agentid && p.RemittanceID == 1).FirstOrDefault();
                rate = db.tblM_Rate.Where(p => p.RateID == rateagent.RateID).FirstOrDefault();
                hfRateMYR.Value = (Double.Parse(hfRateMYR.Value) - Double.Parse(rate.Margin)).ToString();
                if (rateagent != null)
                {
                    lblCurrencyMYR.Text = rate.Currency;

                    lblBaseRateMYR.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(hfRateMYR.Value)).Replace("Rp", "Rp ");
                    lblCountryMYR.Text = "Malaysia to Indonesia";

                    lblBaseRateMYR.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64((Int32.Parse(hfRateMYR.Value) - Int32.Parse(rateagent.Margin)))).Replace("Rp", "Rp ");
                    hfRateMYR.Value = (Convert.ToInt64((Int32.Parse(hfRateMYR.Value) - Int32.Parse(rateagent.Margin)))).ToString();
                }
            }

            countRateMYR();
        }

        void countRateMYR()
        {
            //lblRetailRateMYR.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64((Int32.Parse(hfRateMYR.Value) - Int32.Parse(txtMarginMYR.Text)))).Replace("Rp", "Rp ");
            
        }

        protected void txtIndonesiaIDR_TextChanged(object sender, EventArgs e)
        {
            if (txtIndonesiaIDR.Text != "")
            {
                txtIndonesiaMYR.Text = (Convert.ToDouble(txtIndonesiaIDR.Text) / Convert.ToDouble(hfRateMYR.Value)).ToString();
            }
            else
            {
                txtIndonesiaMYR.Text = "Undefined";
            }
        }

        protected void txtIndonesiaMYR_TextChanged(object sender, EventArgs e)
        {
            if (txtIndonesiaMYR.Text != "")
            {
                txtIndonesiaIDR.Text = (Convert.ToDouble(txtIndonesiaMYR.Text) * Convert.ToDouble(hfRateMYR.Value)).ToString();
            }
            else
            {
                txtIndonesiaIDR.Text = "Undefined";
            }
        }

        protected void btnErrorMYR_ServerClick(object sender, EventArgs e)
        {
            diverrorMYR.Visible = false;
        }

        

        void countIDR()
        {
            var cur = omg.getRateIDRMYR();
            if (cur != null)
            {
                hfRateIDR.Value = cur.Rate;
            }

            var agentid = Int64.Parse(SessionLib.Current.AdminID);
            if (SessionLib.Current.Role == Roles.Agent)
            {
                var rateagent = db.tblM_Agent_Rate.Where(p => p.AgentID == agentid && p.RemittanceID == 2).FirstOrDefault();
                rate = db.tblM_Rate.Where(p => p.RateID == rateagent.RateID).FirstOrDefault();
                hfRateIDR.Value = (Double.Parse(hfRateIDR.Value) + Double.Parse(rate.Margin)).ToString();
                if (rateagent != null)
                {
                    lblCurrencyIDR.Text = rate.Currency;

                    lblBaseRateIDR.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(hfRateIDR.Value)).Replace("Rp", "Rp ");
                    lblCountryIDR.Text = "Indonesia to Malaysia";

                    lblBaseRateIDR.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64((Int32.Parse(hfRateIDR.Value) + Int32.Parse(rateagent.Margin)))).Replace("Rp", "Rp ");
                    hfRateIDR.Value = (Convert.ToInt64((Int32.Parse(hfRateIDR.Value) + Int32.Parse(rateagent.Margin)))).ToString();
                }
            }
            else if (SessionLib.Current.Role == Roles.MasterArea)
            {
                var rateagent = db.tblM_Master_Area_Rate.Where(p => p.MasterAreaID == agentid && p.RemittanceID == 2).FirstOrDefault();
                rate = db.tblM_Rate.Where(p => p.RateID == rateagent.RateID).FirstOrDefault();
                hfRateIDR.Value = (Double.Parse(hfRateIDR.Value) + Double.Parse(rate.Margin)).ToString();
                if (rateagent != null)
                {
                    lblCurrencyIDR.Text = rate.Currency;

                    lblBaseRateIDR.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(hfRateIDR.Value)).Replace("Rp", "Rp ");
                    lblCountryIDR.Text = "Indonesia to Malaysia";

                    lblBaseRateIDR.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64((Int32.Parse(hfRateIDR.Value) + Int32.Parse(rateagent.Margin)))).Replace("Rp", "Rp ");
                    hfRateIDR.Value = (Convert.ToInt64((Int32.Parse(hfRateIDR.Value) + Int32.Parse(rateagent.Margin)))).ToString();
                }
            }
            countRateIDR();
        }

        void countRateIDR()
        {
            //lblRetailRateIDR.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64((Int32.Parse(hfRateIDR.Value) + Int32.Parse(txtMarginIDR.Text)))).Replace("Rp", "Rp ");
            
        }

        protected void txtMalaysiaIDR_TextChanged(object sender, EventArgs e)
        {
            if (txtMalaysiaIDR.Text != "")
            {
                txtMalaysiaMYR.Text = (Convert.ToDouble(txtMalaysiaIDR.Text) / Convert.ToDouble(hfRateIDR.Value)).ToString();
            }
            else
            {
                txtMalaysiaMYR.Text = "Undefined";
            }
        }

        protected void txtMalaysiaMYR_TextChanged(object sender, EventArgs e)
        {
            if (txtMalaysiaMYR.Text != "")
            {
                txtMalaysiaIDR.Text = (Convert.ToDouble(txtMalaysiaMYR.Text) * Convert.ToDouble(hfRateIDR.Value)).ToString();
            }
            else
            {
                txtMalaysiaIDR.Text = "Undefined";
            }
        }

        protected void btnErrorIDR_ServerClick(object sender, EventArgs e)
        {
            diverrorIDR.Visible = false;
        }

        protected void ddlRemittanceLine_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlRemittanceLine.SelectedValue == "1")
            {
                divmyr.Visible = true;
                dividr.Visible = false;
            }
            else if (ddlRemittanceLine.SelectedValue == "2")
            {
                divmyr.Visible = false;
                dividr.Visible = true;
            }
        }
    }
}