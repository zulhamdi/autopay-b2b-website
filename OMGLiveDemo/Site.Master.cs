﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using OMGLiveDemo.Models;
using OMGLiveDemo.Models.OMG;
using OMGLiveDemo.Scripts;
using System.Globalization;

namespace OMGLiveDemo
{
    public partial class SiteMaster : MasterPage
    {
        private const string AntiXsrfTokenKey = "__AntiXsrfToken";
        private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
        private string _antiXsrfTokenValue;

        MasterCount mc = new MasterCount();

        protected void Page_Init(object sender, EventArgs e)
        {
            // The code below helps to protect against XSRF attacks
            var requestCookie = Request.Cookies[AntiXsrfTokenKey];
            Guid requestCookieGuidValue;
            if (requestCookie != null && Guid.TryParse(requestCookie.Value, out requestCookieGuidValue))
            {
                // Use the Anti-XSRF token from the cookie
                _antiXsrfTokenValue = requestCookie.Value;
                Page.ViewStateUserKey = _antiXsrfTokenValue;
            }
            else
            {
                // Generate a new Anti-XSRF token and save to the cookie
                _antiXsrfTokenValue = Guid.NewGuid().ToString("N");
                Page.ViewStateUserKey = _antiXsrfTokenValue;

                var responseCookie = new HttpCookie(AntiXsrfTokenKey)
                {
                    HttpOnly = true,
                    Value = _antiXsrfTokenValue
                };
                if (FormsAuthentication.RequireSSL && Request.IsSecureConnection)
                {
                    responseCookie.Secure = true;
                }
                Response.Cookies.Set(responseCookie);
            }

            Page.PreLoad += master_Page_PreLoad;
        }

        protected void master_Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Set Anti-XSRF token
                ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;
                ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? String.Empty;
            }
            else
            {
                // Validate the Anti-XSRF token
                if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue
                    || (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? String.Empty))
                {
                    throw new InvalidOperationException("Validation of Anti-XSRF token failed.");
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (SessionLib.Current.AdminID != null) {
                lblFullName.Text = SessionLib.Current.Name;
                lblRole.Text = SessionLib.Current.Role;
                lblUsername.Text = SessionLib.Current.Name;

                setMenu();
                checkPendingApprovals();
            }
        }

        void checkRemittanceLine()
        {
            var id = Convert.ToInt64(SessionLib.Current.AdminID);
            if (SessionLib.Current.Role == Models.Roles.MasterArea)
            {
                var rl = mc.getMasterAreaRemittanceList(id);
                for (int i = 0; i < rl.Count; i++)
                {
                    if (rl[i].RemittanceID == 1)
                    {
                        liAddMalaysianSender.Visible = true;
                        liAddIndonesianBenef.Visible = true;
                        liIndonesiaPayout.Visible = true;
                    }
                    if (rl[i].RemittanceID == 2)
                    {
                        liAddIndonesianSender.Visible = true;
                        liAddMalaysianBenef.Visible = true;
                        liMalaysiaPayout.Visible = true;
                    }
                }
            }
            else if (SessionLib.Current.Role == Models.Roles.TravelAgent)
            {
                var rl = mc.getTravelAgentRemittanceList(id);
                for (int i = 0; i < rl.Count; i++)
                {
                    if (rl[i].RemittanceID == 1)
                    {
                        liAddMalaysianSender.Visible = true;
                        liAddIndonesianBenef.Visible = true;
                        liIndonesiaPayout.Visible = true;
                    }
                    if (rl[i].RemittanceID == 2)
                    {
                        liAddIndonesianSender.Visible = true;
                        liAddMalaysianBenef.Visible = true;
                        liMalaysiaPayout.Visible = true;
                    }
                    if (rl[i].RemittanceID == 3)
                    {
                        liAddIndonesianSender.Visible = true;
                        liAddSaudiArabianBenef.Visible = true;
                        liSaudiArabiaPayout.Visible = true;
                    }
                }
            }
            else if (SessionLib.Current.Role == Models.Roles.Agent)
            {
                var rl = mc.getAgentRemittanceList(id);
                for (int i = 0; i < rl.Count; i++)
                {
                    if (rl[i].RemittanceID == 1)
                    {
                        liAddMalaysianSender.Visible = true;
                        liAddIndonesianBenef.Visible = true;
                        liIndonesiaPayout.Visible = true;
                    }
                    if (rl[i].RemittanceID == 2)
                    {
                        liAddIndonesianSender.Visible = true;
                        liAddMalaysianBenef.Visible = true;
                        liMalaysiaPayout.Visible = true;
                    }
                }
            }
        }

        void checkPendingApprovals()
        {
            if(mc.BeneficiaryAllPending() > 0)
            {
                spancountbeneficiary.Visible = true;
                //spanapprovebeneficiarylist.Visible = true;
            } else
            {
                spancountbeneficiary.Visible = false;
                //spanapprovebeneficiarylist.Visible = false;
            }

            if(mc.SenderAllPending() > 0)
            {
                spancountsender.Visible = true;
                //spanapprovesenderlist.Visible = true;
            }
            else
            {
                spancountsender.Visible = false;
                //spanapprovesenderlist.Visible = false;
            }
        }

        void setMenu()
        {
            if (SessionLib.Current.Role == Models.Roles.SuperAdmin || SessionLib.Current.Role == Models.Roles.Admin)
            {
                liDashboard.Visible = true;

                // Sender Menu
                liSender.Visible = true;
                //spancountsender.Visible = true;
                //liAddMalaysianSender.Visible = true;
                //liAddIndonesianSender.Visible = true;
                //liApproveSender.Visible = true;
                liSenderList.Visible = true;

                // Beneficiary Menu
                liBeneficiaries.Visible = true;
                //liAddMalaysianBenef.Visible = true;
                //liAddIndonesianBenef.Visible = true;
                //liApproveBeneficiary.Visible = true;
                liBeneficiaryList.Visible = true;

                // Transfer Menu
                liPayout.Visible = true;
                //liIndonesiaPayout.Visible = true;
                //liMalaysiaPayout.Visible = true;
                //liPendingTask.Visible = true;
                //liWaitingForReceipt.Visible = true;

                //Screening Menu
                liScreening.Visible = true;
                liUmrahHajj.Visible = true;
                liOFAC.Visible = true;
                liUNSanction.Visible = true;
                liPEP.Visible = true;

                // Agent Menu
                //liAgent.Visible = true;
                //liAddAgent.Visible = true;
                //liListAgent.Visible = true;

                // Report Menu
                liReport.Visible = true;

                // Rate Menu
                liRate.Visible = true;
                liManageRate.Visible = true;
                liRateHistory.Visible = true;

                // Administration Menu
                liAdminFee.Visible = true;

                // Tools Menu
                //liTools.Visible = true;
                //liCalculator.Visible = true;

                // Settings Menu
                liAdmin.Visible = true;
                liAuditLog.Visible = true;
                liNotification.Visible = true;
                liBankAccount.Visible = true;

                // Master Area Menu
                /*liMasterArea.Visible = true;
                spancountmasterarea.Visible = true;
                liMasterAreaAdd.Visible = true;
                liMasterAreaList.Visible = true;
                liTopupRequestMA.Visible = true;
                liTopupPendingMA.Visible = true;
                liTopupHistoryMA.Visible = true;
                liSettlementMAs.Visible = true;*/

                // Travel Agent Menu
                /*liTravelAgent.Visible = true;
                spancounttravelagent.Visible = true;
                liTravelAgentAdd.Visible = true;
                liTravelAgentList.Visible = true;
                liTopupRequestTA.Visible = true;
                liTopupPendingTA.Visible = true;
                liTopupHistoryTA.Visible = true;
                liSettlementTA.Visible = true;*/
            }
            if (SessionLib.Current.Role == Models.Roles.Operator)
            {
                liDashboardOperator.Visible = true;
                
                // Sender Menu
                liSender.Visible = true;
                //spancountsender.Visible = true;
                //liAddMalaysianSender.Visible = true;
                liAddIndonesianSender.Visible = true;
                //liApproveSender.Visible = true;
                liSenderList.Visible = true;

                // Beneficiary Menu
                liBeneficiaries.Visible = true;
                liAddMalaysianBenef.Visible = true;
                //liAddIndonesianBenef.Visible = true;
                liBeneficiaryList.Visible = true;

                // Master Area Menu
                //liMasterArea.Visible = true;
                //spancountmasterarea.Visible = true;
                //liTopupRequestMA.Visible = true;
                //liTopupHistoryMA.Visible = true;

                // Travel Agent Menu
                //liTravelAgent.Visible = true;
                //spancounttravelagent.Visible = true;
                //liTopupRequestTA.Visible = true;
                //liTopupHistoryTA.Visible = true;

                // Transfer Menu
                liPayout.Visible = true;
                //liIndonesiaPayout.Visible = true;
                liMalaysiaPayout.Visible = true;
                liPendingTask.Visible = true;
                liWaitingForReceipt.Visible = true;
                //liPayoutHistory.Visible = true;

                //Screening Menu
                liScreening.Visible = true;
                liUmrahHajj.Visible = true;
                liOFAC.Visible = true;
                liUNSanction.Visible = true;
                liPEP.Visible = true;

                // Rate Menu
                liRate.Visible = true;
                liManageRate.Visible = true;
                liRateHistory.Visible = true;

                // Tools Menu
                //liTools.Visible = true;
                //liCalculator.Visible = true;
            }
            if (SessionLib.Current.Role == Models.Roles.Checker)
            {
                liDashboardChecker.Visible = true;

                // Sender Menu
                liSender.Visible = true;
                spancountsender.Visible = true;
                //liAddMalaysianSender.Visible = true;
                //liAddIndonesianSender.Visible = true;
                liApproveSender.Visible = true;
                liSenderList.Visible = true;

                // Beneficiary Menu
                liBeneficiaries.Visible = true;
                spancountbeneficiary.Visible = true;
                //liAddMalaysianBenef.Visible = true;
                //liAddIndonesianBenef.Visible = true;
                liApproveBeneficiary.Visible = true;
                liBeneficiaryList.Visible = true;

                // Master Area Menu
                //liMasterArea.Visible = true;
                //spancountmasterarea.Visible = true;
                //liTopupRequestMA.Visible = true;
                //liTopupHistoryMA.Visible = true;

                // Travel Agent Menu
                //liTravelAgent.Visible = true;
                //spancounttravelagent.Visible = true;
                //liTopupRequestTA.Visible = true;
                //liTopupHistoryTA.Visible = true;

                // Transfer Menu
                liPayout.Visible = true;
                //liIndonesiaPayout.Visible = true;
                //liMalaysiaPayout.Visible = true;
                //liPendingTask.Visible = true;
                //liWaitingForReceipt.Visible = true;
                //liPayoutHistory.Visible = true;

                //Screening Menu
                liScreening.Visible = true;
                liUmrahHajj.Visible = true;
                liOFAC.Visible = true;
                liUNSanction.Visible = true;
                liPEP.Visible = true;

                // Report Menu
                liReport.Visible = true;

                // Rate Menu
                //liRate.Visible = true;

                // Tools Menu
                //liTools.Visible = true;
                //liCalculator.Visible = true;
            }
            if (SessionLib.Current.Role == Models.Roles.Agent)
            {
                liDashboardAgent.Visible = true;
                
                //Topup
                liBalanceHistory.Visible = true;

                //Transfer
                liPayout.Visible = true;
                liMalaysiaPayout.Visible = true;
                liWaitingForReceipt.Visible = true;

                //Screening Menu
                liScreening.Visible = true;
                liUmrahHajj.Visible = true;
                liOFAC.Visible = true;
                liUNSanction.Visible = true;
                liPEP.Visible = true;

                //Sender
                liSender.Visible = true;
                liSenderList.Visible = true;

                //Bene
                liBeneficiaries.Visible = true;
                liBeneficiaryList.Visible = true;
                
                //Tools
                liCalculator.Visible = true;

                // Rate Menu
                liRate.Visible = true;
                liRateHistory.Visible = true;
                liRateAgent.Visible = true;

                checkRemittanceLine();
            }
            if (SessionLib.Current.Role == Models.Roles.MasterArea)
            {
                liRateAgent.Visible = true;
                liPayout.Visible = true;
                liSender.Visible = true;
                liSenderList.Visible = true;
                liBeneficiaries.Visible = true;
                liDashboardAgent.Visible = true;
                liCalculator.Visible = true;
                liTopup.Visible = true;
                liSettlementMA.Visible = true;
                checkRemittanceLine();
            }
            if (SessionLib.Current.Role == Models.Roles.TravelAgent)
            {
                liRateAgent.Visible = true;
                liPayout.Visible = true;
                liSender.Visible = true;
                liSenderList.Visible = true;
                liBeneficiaries.Visible = true;
                liDashboardAgent.Visible = true;
                liCalculator.Visible = true;
                liTopup.Visible = true;
                liSettlementMA.Visible = true;
                checkRemittanceLine();
            }
            if (SessionLib.Current.Role == Models.Roles.Approver)
            {
                liDashboardApprover.Visible = true;

                // Sender Menu
                liSender.Visible = true;
                //spancountsender.Visible = true;
                //liAddMalaysianSender.Visible = true;
                //liAddIndonesianSender.Visible = true;
                //liApproveSender.Visible = true;
                liSenderList.Visible = true;

                // Beneficiary Menu
                liBeneficiaries.Visible = true;
                //liAddMalaysianBenef.Visible = true;
                //liAddIndonesianBenef.Visible = true;
                //liApproveBeneficiary.Visible = true;
                liBeneficiaryList.Visible = true;

                // Transfer Menu
                liPayout.Visible = true;
                //liIndonesiaPayout.Visible = true;
                //liMalaysiaPayout.Visible = true;
                liPendingTask.Visible = true;
                liWaitingForReceipt.Visible = true;

                //Screening Menu
                liScreening.Visible = true;
                liUmrahHajj.Visible = true;
                liOFAC.Visible = true;
                liUNSanction.Visible = true;
                liPEP.Visible = true;

                // Agent Menu
                //liAgent.Visible = true;
                //liPendingAgentTopupRequest.Visible = true;
                //liAgentTopupHistory.Visible = true;

                // Rate Menu
                liRate.Visible = true;
                liRateHistory.Visible = true;

                // Report Menu
                liReport.Visible = true;

                // Tools Menu
                //liTools.Visible = true;
                //liCalculator.Visible = true;
            }

            count();
        }

        void count() {
            var id = Convert.ToInt64(SessionLib.Current.AdminID);

            if (SessionLib.Current.Role == Models.Roles.Agent)
            {
                var ur = mc.PayoutAgentCount("created");
                lblCountUploadReceipt.Text = ur.ToString();
                lblCountPayout.Text = ur.ToString();
            }
            else if (SessionLib.Current.Role == Models.Roles.MasterArea)
            {
                var ur = mc.PayoutMasterAreaCount("created");
                lblCountUploadReceipt.Text = ur.ToString();
                lblCountPayout.Text = ur.ToString();
            }
            else if (SessionLib.Current.Role == Models.Roles.TravelAgent)
            {
                var ur = mc.PayoutTravelAgentCount("created");
                lblCountUploadReceipt.Text = ur.ToString();
                lblCountPayout.Text = ur.ToString();
            }
            else if (SessionLib.Current.Role == Models.Roles.Approver)
            {
                var pt = mc.PayoutAllCount("queued");
                lblCountPendingTask.Text = pt.ToString();
                lblCountPayout.Text = pt.ToString();

                var sn = mc.SenderAllPending();
                lblCountApproveSender.Text = sn.ToString();
                lblCountSender.Text = sn.ToString();

                var topup = mc.TopupMAAllPending();
                lblCountMasterArea.Text = topup.ToString();
                lblCountPendingTopupMA.Text = topup.ToString();

                var topupta = mc.TopupTAAllPending();
                lblCountTravelAgent.Text = topupta.ToString();
                lblCountPendingTopupTA.Text = topupta.ToString();
            }
            else if (SessionLib.Current.Role == Models.Roles.Checker)
            {
                var sn = mc.SenderAllPending();
                lblCountApproveSender.Text = sn.ToString();
                lblCountSender.Text = sn.ToString();

                var bn = mc.BeneficiaryAllPending();
                lblCountApproveBeneficiary.Text = bn.ToString();
                lblCountBeneficiary.Text = bn.ToString();
            }
            else if (SessionLib.Current.Role == Models.Roles.Admin || SessionLib.Current.Role == Models.Roles.SuperAdmin)
            {
                var pt = mc.PayoutAllCount("queued");
                lblCountPendingTask.Text = pt.ToString();

                var ur = mc.PayoutAdminCount("created");
                lblCountUploadReceipt.Text = ur.ToString();
                
                lblCountPayout.Text = (pt + ur).ToString();

                var topup = mc.TopupMAAllPending();
                lblCountMasterArea.Text = topup.ToString();
                lblCountPendingTopupMA.Text = topup.ToString();

                var topupta = mc.TopupTAAllPending();
                lblCountTravelAgent.Text = topupta.ToString();
                lblCountPendingTopupTA.Text = topupta.ToString();
            }
            else if (SessionLib.Current.Role == Models.Roles.Operator)
            {
                var pt = mc.PayoutAllCount("queued");
                lblCountPendingTask.Text = pt.ToString();
                lblCountPayout.Text = pt.ToString();

                var ur = mc.PayoutAdminCount("created");
                lblCountUploadReceipt.Text = ur.ToString();
            }
            else
            {
                var pt = mc.PayoutAllCount("queued");
                lblCountPendingTask.Text = pt.ToString();
                lblCountPayout.Text = pt.ToString();
            }
        }

        protected void Unnamed_LoggingOut(object sender, LoginCancelEventArgs e)
        {
            Context.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
        }

        protected void lbtLogout_Click(object sender, EventArgs e)
        {
            SessionLib.Current.AdminID = string.Empty;
            SessionLib.Current.Name = string.Empty;
            SessionLib.Current.Email = string.Empty;
            SessionLib.Current.HP = string.Empty;
            SessionLib.Current.Role = string.Empty;
            Response.Redirect("~/Login");
        }

        public void checkRole()
        {
            string xxx = SessionLib.Current.Role;
            if (xxx == Models.Roles.Admin) { Response.Redirect("~/Default"); }
            else if (xxx == Models.Roles.SuperAdmin) { Response.Redirect("~/Default"); }
            else if (xxx == Models.Roles.Agent) { Response.Redirect("~/AgentDashboard"); }
            else if (xxx == Models.Roles.MasterArea) { Response.Redirect("~/AgentDashboard"); }
            else if (xxx == Models.Roles.Approver) { Response.Redirect("~/Payout/PendingTask"); }
            else if (xxx == Models.Roles.Operator) { Response.Redirect("~/OperatorDashboard"); }
            else if (xxx == Models.Roles.Operator) { Response.Redirect("~/CheckerDashboard"); }
            else { Response.Redirect("~/Login"); }
        }

        public MasterCount getMasterCount()
        {
            return mc;
        }
    }

}