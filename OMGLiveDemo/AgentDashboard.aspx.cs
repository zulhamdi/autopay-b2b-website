﻿using OMGLiveDemo.Models;
using OMGLiveDemo.Scripts;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo
{
    public partial class AgentDashboard : Page
    {
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();
        OMG omg = new OMG();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    bindBalance();
                    BindGV();
                    count();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        void bindBalance()
        {
            long id = Int64.Parse(SessionLib.Current.AdminID);
            if (SessionLib.Current.Role == Roles.Agent)
            {
                var dt = db.tblM_Agent_Balance.Where(p => p.isActive == 1 && p.AgentID == id).ToList();
                ddlBalance.DataSource = null;
                ddlBalance.DataSource = dt;
                ddlBalance.DataValueField = "BalanceID";
                ddlBalance.DataTextField = "Currency";
                ddlBalance.DataBind();
            }
            else if (SessionLib.Current.Role == Roles.MasterArea)
            {
                var dt = db.tblM_Master_Area_Balance.Where(p => p.isActive == 1 && p.MasterAreaID == id).ToList();
                ddlBalance.DataSource = null;
                ddlBalance.DataSource = dt;
                ddlBalance.DataValueField = "BalanceID";
                ddlBalance.DataTextField = "Currency";
                ddlBalance.DataBind();
            }
            else if (SessionLib.Current.Role == Roles.TravelAgent)
            {
                var dt = db.tblM_Travel_Agent_Balance.Where(p => p.isActive == 1 && p.TravelAgentID == id).ToList();
                ddlBalance.DataSource = null;
                ddlBalance.DataSource = dt;
                ddlBalance.DataValueField = "BalanceID";
                ddlBalance.DataTextField = "Currency";
                ddlBalance.DataBind();
            }
        }

        void BindGV()
        {
            var agid = Convert.ToInt64(SessionLib.Current.AdminID);
            List<vw_Payout> dt = new List<vw_Payout>();
            if (SessionLib.Current.Role == Roles.Agent)
                dt = db.vw_Payout.Where(p => p.isAgent == 1 && p.CreatedBy == agid).OrderByDescending(p => p.PayoutID).Take(20).ToList();
            else if (SessionLib.Current.Role == Roles.MasterArea)
                dt = db.vw_Payout.Where(p => p.isAgent == 2 && p.CreatedBy == agid).OrderByDescending(p => p.PayoutID).Take(20).ToList();
            else if (SessionLib.Current.Role == Roles.TravelAgent)
                dt = db.vw_Payout.Where(p => p.isAgent == 3 && p.CreatedBy == agid).OrderByDescending(p => p.PayoutID).Take(20).ToList();


            for (int i = 0; i < dt.Count; i++)
            {
                if (dt[i].RemittanceID == 1)
                {
                    dt[i].AmountText = "MYR " + String.Format(new CultureInfo("ms-MY"), "{0:n}", dt[i].Amount.Value);
                    dt[i].TotalAmountText = "MYR " + String.Format(new CultureInfo("ms-MY"), "{0:n}", dt[i].TotalAmount);
                    dt[i].TotalTransferText = "IDR " + String.Format(new CultureInfo("id-ID"), "{0:n}", dt[i].TotalTransfer);

                    if (dt[i].isAgent == 2)
                        dt[i].AdminFee = "IDR " + String.Format(new CultureInfo("id-ID"), "{0:n}", float.Parse(dt[i].AdminFee));
                    else
                        dt[i].AdminFee = "MYR " + String.Format(new CultureInfo("ms-MY"), "{0:n}", float.Parse(dt[i].AdminFee));
                }
                else if (dt[i].RemittanceID == 2)
                {
                    dt[i].AmountText = "IDR " + String.Format(new CultureInfo("id-ID"), "{0:n}", dt[i].Amount.Value);
                    dt[i].TotalAmountText = "IDR " + String.Format(new CultureInfo("id-ID"), "{0:n}", dt[i].TotalAmount);
                    dt[i].TotalTransferText = "MYR " + String.Format(new CultureInfo("ms-MY"), "{0:n}", dt[i].TotalTransfer);

                    if (dt[i].isAgent == 2)
                        dt[i].AdminFee = "MYR " + String.Format(new CultureInfo("ms-MY"), "{0:n}", float.Parse(dt[i].AdminFee));
                    else
                        dt[i].AdminFee = "IDR " + String.Format(new CultureInfo("id-ID"), "{0:n}", float.Parse(dt[i].AdminFee));
                }
                else if (dt[i].RemittanceID == 3)
                {
                    dt[i].AmountText = "IDR " + String.Format(new CultureInfo("id-ID"), "{0:n}", dt[i].Amount.Value);
                    dt[i].TotalAmountText = "IDR " + String.Format(new CultureInfo("id-ID"), "{0:n}", dt[i].TotalAmount);
                    dt[i].TotalTransferText = "SAR " + String.Format(new CultureInfo("ar-SA"), "{0:n}", dt[i].TotalTransfer);

                    if (dt[i].isAgent == 2)
                        dt[i].AdminFee = "SAR " + String.Format(new CultureInfo("ar-SA"), "{0:n}", float.Parse(dt[i].AdminFee));
                    else
                        dt[i].AdminFee = "IDR " + String.Format(new CultureInfo("id-ID"), "{0:n}", float.Parse(dt[i].AdminFee));
                }
                dt[i].RateText = "Rp " + String.Format(new CultureInfo("id-ID"), "{0:n}", float.Parse(dt[i].Rate));
                dt[i].RateGivenText = "Rp " + String.Format(new CultureInfo("id-ID"), "{0:n}", float.Parse(dt[i].RateGiven));
            }
            gvListItem.DataSource = dt;
            gvListItem.DataBind();

            
        }
        

        public void count()
        {
            var agid = Convert.ToInt64(SessionLib.Current.AdminID);

            if (SessionLib.Current.Role == Roles.Agent)
            {
                var rid = 0;
                if (ddlBalance.SelectedItem.Text == "IDR")
                {
                    rid = 1;
                    lkbBalance.Text = ddlBalance.SelectedItem.Text + " " + String.Format(new CultureInfo("id-ID"), "{0:n}", db.tblM_Agent_Balance.Where(p => p.AgentID == agid && p.CountryID == 103).Select(p => p.Balance).FirstOrDefault().Value).Replace("Rp", "Rp ");
                }
                else if (ddlBalance.SelectedItem.Text == "MYR")
                {
                    rid = 2;
                    lkbBalance.Text = "IDR " + String.Format(new CultureInfo("id-ID"), "{0:n}", db.tblM_Agent_Balance.Where(p => p.AgentID == agid && p.CountryID == 133).Select(p => p.Balance).FirstOrDefault().Value).Replace("RM", "RM ");
                }

                var rl = db.vw_Agent_Rate.Where(p => p.AgentID == agid).ToList();

                for (int i = 0; i < rl.Count; i++)
                {
                    if (rl[i].RemittanceID == 1)
                    {
                        var curmyr = omg.getRateMYR2IDR();
                        if (curmyr != null)
                        {
                            var marginmyr = db.vw_Agent_Rate.Where(p => p.AgentID == agid && p.RemittanceID == 1).FirstOrDefault();
                            lblRate.Text = "1 MYR = " + String.Format(new CultureInfo("id-ID"), "{0:c}", (Convert.ToInt64(curmyr.Rate) - Convert.ToInt64(marginmyr.Margin))).Replace("Rp", "") + " IDR";
                            lblRateUpdate.Text = "Last update : " + curmyr.DateModified.ToLongTimeString() + ", " + curmyr.DateModified.ToLongDateString();
                            //lblRateUpdate.Text = "Last update : " + marginmyr.UpdatedDate.Value.ToLongTimeString() + ", " + marginmyr.UpdatedDate.Value.ToLongDateString();
                        }
                        if (rl.Count > 1)
                            divrate1.Attributes.Add("class", "col-md-6 col-sm-12 col-xs-12");
                        else
                            divrate1.Attributes.Add("class", "col-md-12 col-sm-12 col-xs-12");
                        divrate1.Visible = true;
                    }

                    if (rl[i].RemittanceID == 2)
                    {
                        // rate idr //
                        var curidr = omg.getRateIDRMYR();
                        if (curidr != null)
                        {
                            var marginidr = db.vw_Agent_Rate.Where(p => p.AgentID == agid && p.RemittanceID == 2).FirstOrDefault();
                            lblRateIDR.Text = "1 MYR = " + String.Format(new CultureInfo("id-ID"), "{0:c}", (Convert.ToInt64(curidr.Rate) + Convert.ToInt64(marginidr.Margin))).Replace("Rp", "") + " IDR";
                            lblRateUpdateIDR.Text = "Last update : " + curidr.DateModified.ToLongTimeString() + ", " + curidr.DateModified.ToLongDateString();
                            //lblRateUpdateIDR.Text = "Last update : " + marginidr.UpdatedDate.Value.ToLongTimeString() + ", " + marginidr.UpdatedDate.Value.ToLongDateString();
                        }
                        if (rl.Count > 1)
                            divrate2.Attributes.Add("class", "col-md-6 col-sm-12 col-xs-12");
                        else
                            divrate2.Attributes.Add("class", "col-md-12 col-sm-12 col-xs-12");
                        divrate2.Visible = true;
                        // rate idr //
                    }
                }

                var tra = db.tblT_Payout.Where(p => p.isAgent == 1 && p.CreatedBy == agid && p.RemittanceID == rid).Count();

                if (db.tblT_Payout.Where(p => (p.Status == "created" || p.Status == "queued") && p.isAgent == 1 && p.CreatedBy == agid && p.RemittanceID == rid).Count() != 0)
                {
                    lbkTraPending.Text = ddlBalance.SelectedItem.Text + " " + String.Format(new CultureInfo("id-ID"), "{0:n}", db.tblT_Payout.Where(p => (p.Status == "created" || p.Status == "queued") && p.isAgent == 1 && p.CreatedBy == agid && p.RemittanceID == rid).Select(p => p.TotalTransfer).Sum()).Replace("Rp", "Rp ");
                    lblTraPendingPercent.Text = ((db.tblT_Payout.Where(p => (p.Status == "created" || p.Status == "queued") && p.isAgent == 1 && p.CreatedBy == agid && p.RemittanceID == rid).Count() * 100) / tra).ToString();
                }
                else
                {
                    lbkTraPending.Text = ddlBalance.SelectedItem.Text + " 0";
                }

                if (db.tblT_Payout.Where(p => p.Status == "completed" && p.isAgent == 1 && p.CreatedBy == agid && p.RemittanceID == rid).Count() != 0)
                {
                    lbkTraSuccess.Text = ddlBalance.SelectedItem.Text + " " + String.Format(new CultureInfo("id-ID"), "{0:n}", db.tblT_Payout.Where(p => p.Status == "completed" && p.isAgent == 1 && p.CreatedBy == agid && p.RemittanceID == rid).Select(p => p.TotalTransfer).Sum()).Replace("Rp", "Rp ");
                    lblTraSuccessPrecent.Text = ((db.tblT_Payout.Where(p => p.Status == "completed" && p.isAgent == 1 && p.CreatedBy == agid && p.RemittanceID == rid).Count() * 100) / tra).ToString();
                }
                else
                {
                    lbkTraSuccess.Text = ddlBalance.SelectedItem.Text + " 0";
                }

                if (db.tblT_Payout.Where(p => p.Status == "rejected" && p.isAgent == 1 && p.CreatedBy == agid && p.RemittanceID == rid).Count() != 0)
                {
                    lbkTraRejected.Text = ddlBalance.SelectedItem.Text + " " + String.Format(new CultureInfo("id-ID"), "{0:n}", db.tblT_Payout.Where(p => p.Status == "rejected" && p.isAgent == 1 && p.CreatedBy == agid && p.RemittanceID == rid).Select(p => p.TotalTransfer).Sum()).Replace("Rp", "Rp ");
                    lblTraRejectPercent.Text = ((db.tblT_Payout.Where(p => p.Status == "rejected" && p.isAgent == 1 && p.CreatedBy == agid && p.RemittanceID == rid).Count() * 100) / tra).ToString();
                }
                else
                {
                    lbkTraRejected.Text = ddlBalance.SelectedItem.Text + " 0";
                }

                if (db.tblT_Payout.Where(p => (p.Status == "processed" || p.Status == "approved") && p.isAgent == 1 && p.CreatedBy == agid && p.RemittanceID == rid).Count() != 0)
                {
                    lbkTraProcess.Text = ddlBalance.SelectedItem.Text + " " + String.Format(new CultureInfo("id-ID"), "{0:n}", db.tblT_Payout.Where(p => (p.Status == "processed" || p.Status == "approved") && p.isAgent == 1 && p.CreatedBy == agid && p.RemittanceID == rid).Select(p => p.TotalTransfer).Sum()).Replace("Rp", "Rp ");
                    lblTraProcessPrecent.Text = ((db.tblT_Payout.Where(p => (p.Status == "processed" || p.Status == "approved") && p.isAgent == 1 && p.CreatedBy == agid && p.RemittanceID == rid).Count() * 100) / tra).ToString();
                }
                else
                {
                    lbkTraProcess.Text = ddlBalance.SelectedItem.Text + " 0";
                }

                if (db.tblT_Payout.Where(p => p.Status == "failed" && p.isAgent == 1 && p.CreatedBy == agid && p.RemittanceID == rid).Count() != 0)
                {
                    lbkTraFailed.Text = ddlBalance.SelectedItem.Text + " " + String.Format(new CultureInfo("id-ID"), "{0:n}", db.tblT_Payout.Where(p => p.Status == "failed" && p.isAgent == 1 && p.CreatedBy == agid && p.RemittanceID == rid).Select(p => p.TotalTransfer).Sum()).Replace("Rp", "Rp ");
                    lblTraFailedPercent.Text = ((db.tblT_Payout.Where(p => p.Status == "failed" && p.isAgent == 1 && p.CreatedBy == agid && p.RemittanceID == rid).Count() * 100) / tra).ToString();
                }
                else
                {
                    lbkTraFailed.Text = ddlBalance.SelectedItem.Text + " 0";
                }
            }
            else if (SessionLib.Current.Role == Roles.MasterArea)
            {
                var rl = db.vw_Master_Area_Rate.Where(p => p.MasterAreaID == agid).ToList();

                for (int i = 0; i < rl.Count; i++)
                {
                    if (rl[i].RemittanceID == 1)
                    {
                        var curmyr = omg.getRateMYR2IDR();
                        if (curmyr != null)
                        {
                            var marginmyr = db.vw_Master_Area_Rate.Where(p => p.MasterAreaID == agid && p.RemittanceID == 1).FirstOrDefault();
                            lblRate.Text = "1 MYR = " + String.Format(new CultureInfo("id-ID"), "{0:c}", (Convert.ToInt64(curmyr.Rate) - Convert.ToInt64(marginmyr.Margin))).Replace("Rp", "") + " IDR";
                            lblRateUpdate.Text = "Last update : " + curmyr.DateModified.ToLongTimeString() + ", " + curmyr.DateModified.ToLongDateString();
                            //lblRateUpdate.Text = "Last update : " + marginmyr.UpdatedDate.Value.ToLongTimeString() + ", " + marginmyr.UpdatedDate.Value.ToLongDateString();
                        }
                        if (rl.Count > 1)
                            divrate1.Attributes.Add("class", "col-md-6 col-sm-12 col-xs-12");
                        else
                            divrate1.Attributes.Add("class", "col-md-12 col-sm-12 col-xs-12");
                        divrate1.Visible = true;
                    }

                    if (rl[i].RemittanceID == 2)
                    {
                        // rate idr //
                        var curidr = omg.getRateIDRMYR();
                        if (curidr != null)
                        {
                            var marginidr = db.vw_Master_Area_Rate.Where(p => p.MasterAreaID == agid && p.RemittanceID == 2).FirstOrDefault();
                            lblRateIDR.Text = "1 MYR = " + String.Format(new CultureInfo("id-ID"), "{0:c}", (Convert.ToInt64(curidr.Rate) + Convert.ToInt64(marginidr.Margin))).Replace("Rp", "") + " IDR";
                            lblRateUpdateIDR.Text = "Last update : " + curidr.DateModified.ToLongTimeString() + ", " + curidr.DateModified.ToLongDateString();
                            //lblRateUpdateIDR.Text = "Last update : " + marginidr.UpdatedDate.Value.ToLongTimeString() + ", " + marginidr.UpdatedDate.Value.ToLongDateString();
                        }
                        if (rl.Count > 1)
                            divrate2.Attributes.Add("class", "col-md-6 col-sm-12 col-xs-12");
                        else
                            divrate2.Attributes.Add("class", "col-md-12 col-sm-12 col-xs-12");
                        divrate2.Visible = true;
                        // rate idr //
                    }
                }
                
                lblBalance.Text = "Balance";
                var rid = 0;
                if (ddlBalance.SelectedItem.Text == "IDR")
                {
                    rid = 1;
                    lkbBalance.Text = ddlBalance.SelectedItem.Text + " " + String.Format(new CultureInfo("id-ID"), "{0:n}", db.tblM_Master_Area_Balance.Where(p => p.MasterAreaID == agid && p.CountryID == 103).Select(p => p.Balance).FirstOrDefault().Value).Replace("Rp", "Rp ");
                }
                else if (ddlBalance.SelectedItem.Text == "MYR")
                {
                    rid = 2;
                    lkbBalance.Text = ddlBalance.SelectedItem.Text + " " + String.Format(new CultureInfo("id-ID"), "{0:n}", db.tblM_Master_Area_Balance.Where(p => p.MasterAreaID == agid && p.CountryID == 133).Select(p => p.Balance).FirstOrDefault().Value).Replace("RM", "RM ");
                }

                var tra = db.tblT_Payout.Where(p => p.isAgent == 2 && p.CreatedBy == agid && p.RemittanceID == rid).Count();

                if (db.tblT_Payout.Where(p => (p.Status == "created" || p.Status == "queued") && p.isAgent == 2 && p.CreatedBy == agid && p.RemittanceID == rid).Count() != 0)
                {
                    lbkTraPending.Text = ddlBalance.SelectedItem.Text + " " + String.Format(new CultureInfo("id-ID"), "{0:n}", db.tblT_Payout.Where(p => (p.Status == "created" || p.Status == "queued") && p.isAgent == 2 && p.CreatedBy == agid && p.RemittanceID == rid).Select(p => p.TotalTransfer).Sum()).Replace("Rp", "Rp ");
                    lblTraPendingPercent.Text = ((db.tblT_Payout.Where(p => (p.Status == "created" || p.Status == "queued") && p.isAgent == 2 && p.CreatedBy == agid && p.RemittanceID == rid).Count() * 100) / tra).ToString();
                }
                else
                {
                    lbkTraPending.Text = ddlBalance.SelectedItem.Text + " 0";
                }

                if (db.tblT_Payout.Where(p => p.Status == "completed" && p.isAgent == 2 && p.CreatedBy == agid && p.RemittanceID == rid).Count() != 0)
                {
                    lbkTraSuccess.Text = ddlBalance.SelectedItem.Text + " " + String.Format(new CultureInfo("id-ID"), "{0:n}", db.tblT_Payout.Where(p => p.Status == "completed" && p.isAgent == 2 && p.CreatedBy == agid && p.RemittanceID == rid).Select(p => p.TotalTransfer).Sum()).Replace("Rp", "Rp ");
                    lblTraSuccessPrecent.Text = ((db.tblT_Payout.Where(p => p.Status == "completed" && p.isAgent == 2 && p.CreatedBy == agid && p.RemittanceID == rid).Count() * 100) / tra).ToString();
                }
                else
                {
                    lbkTraSuccess.Text = ddlBalance.SelectedItem.Text + " 0";
                }

                if (db.tblT_Payout.Where(p => p.Status == "rejected" && p.isAgent == 2 && p.CreatedBy == agid && p.RemittanceID == rid).Count() != 0)
                {
                    lbkTraRejected.Text = ddlBalance.SelectedItem.Text + " " + String.Format(new CultureInfo("id-ID"), "{0:n}", db.tblT_Payout.Where(p => p.Status == "rejected" && p.isAgent == 2 && p.CreatedBy == agid && p.RemittanceID == rid).Select(p => p.TotalTransfer).Sum()).Replace("Rp", "Rp ");
                    lblTraRejectPercent.Text = ((db.tblT_Payout.Where(p => p.Status == "rejected" && p.isAgent == 2 && p.CreatedBy == agid && p.RemittanceID == rid).Count() * 100) / tra).ToString();
                }
                else
                {
                    lbkTraRejected.Text = ddlBalance.SelectedItem.Text + " 0";
                }

                if (db.tblT_Payout.Where(p => (p.Status == "processed" || p.Status == "approved") && p.isAgent == 2 && p.CreatedBy == agid && p.RemittanceID == rid).Count() != 0)
                {
                    lbkTraProcess.Text = ddlBalance.SelectedItem.Text + " " + String.Format(new CultureInfo("id-ID"), "{0:n}", db.tblT_Payout.Where(p => (p.Status == "processed" || p.Status == "approved") && p.isAgent == 2 && p.CreatedBy == agid && p.RemittanceID == rid).Select(p => p.TotalTransfer).Sum()).Replace("Rp", "Rp ");
                    lblTraProcessPrecent.Text = ((db.tblT_Payout.Where(p => (p.Status == "processed" || p.Status == "approved") && p.isAgent == 2 && p.CreatedBy == agid && p.RemittanceID == rid).Count() * 100) / tra).ToString();
                }
                else
                {
                    lbkTraProcess.Text = ddlBalance.SelectedItem.Text + " 0";
                }

                if (db.tblT_Payout.Where(p => p.Status == "failed" && p.isAgent == 2 && p.CreatedBy == agid && p.RemittanceID == rid).Count() != 0)
                {
                    lbkTraFailed.Text = ddlBalance.SelectedItem.Text + " " + String.Format(new CultureInfo("id-ID"), "{0:n}", db.tblT_Payout.Where(p => p.Status == "failed" && p.isAgent == 2 && p.CreatedBy == agid && p.RemittanceID == rid).Select(p => p.TotalTransfer).Sum()).Replace("Rp", "Rp ");
                    lblTraFailedPercent.Text = ((db.tblT_Payout.Where(p => p.Status == "failed" && p.isAgent == 2 && p.CreatedBy == agid && p.RemittanceID == rid).Count() * 100) / tra).ToString();
                }
                else
                {
                    lbkTraFailed.Text = ddlBalance.SelectedItem.Text + " 0";
                }
            }
            else if (SessionLib.Current.Role == Roles.TravelAgent)
            {
                var rl = db.vw_Travel_Agent_Rate.Where(p => p.TravelAgentID == agid).ToList();

                var va = db.tblM_Travel_Agent_VA.Where(p => p.TravelAgentID == agid).FirstOrDefault();
                if (va != null)
                {
                    divva.Visible = true;
                    lblVA.Text = va.VA;
                    lblVAName.Text = va.Name;
                }
                bool divvaset = false;

                for (int i = 0; i < rl.Count; i++)
                {
                    if (rl[i].RemittanceID == 3)
                    {
                        var curmyr = omg.getRateIDRSAR();
                        if (curmyr != null)
                        {
                            lblRate1.Text = "Indonesia to Saudi Arabia";
                            var marginsar = db.vw_Travel_Agent_Rate.Where(p => p.TravelAgentID == agid && p.RemittanceID == 3).FirstOrDefault();
                            lblRate.Text = "1 SAR = " + String.Format(new CultureInfo("id-ID"), "{0:c}", (Convert.ToInt64(curmyr.Rate) - Convert.ToInt64(marginsar.Margin))).Replace("Rp", "") + " IDR";
                            lblRateUpdate.Text = "Last update : " + curmyr.DateModified.ToLongTimeString() + ", " + curmyr.DateModified.ToLongDateString();
                            //lblRateUpdate.Text = "Last update : " + marginsar.UpdatedDate.Value.ToLongTimeString() + ", " + marginsar.UpdatedDate.Value.ToLongDateString();
                        }
                        if (rl.Count > 1)
                        {
                            divrate1.Attributes.Add("class", "col-md-4 col-sm-12 col-xs-12");
                            if (!divvaset)
                                divva.Attributes.Add("class", "col-md-4 col-sm-12 col-xs-12");
                        }
                        else
                        {
                            divrate1.Attributes.Add("class", "col-md-6 col-sm-12 col-xs-12");
                            if (!divvaset)
                                divva.Attributes.Add("class", "col-md-6 col-sm-12 col-xs-12");
                        }
                        divrate1.Visible = true;
                        divvaset = true;
                    }

                    if (rl[i].RemittanceID == 2)
                    {
                        // rate idr //
                        var curidr = omg.getRateIDRMYR();
                        if (curidr != null)
                        {
                            var marginidr = db.vw_Travel_Agent_Rate.Where(p => p.TravelAgentID == agid && p.RemittanceID == 2).FirstOrDefault();
                            lblRateIDR.Text = "1 MYR = " + String.Format(new CultureInfo("id-ID"), "{0:c}", (Convert.ToInt64(curidr.Rate) + Convert.ToInt64(marginidr.Margin))).Replace("Rp", "") + " IDR";
                            lblRateUpdateIDR.Text = "Last update : " + curidr.DateModified.ToLongTimeString() + ", " + curidr.DateModified.ToLongDateString();
                            //lblRateUpdateIDR.Text = "Last update : " + marginidr.UpdatedDate.Value.ToLongTimeString() + ", " + marginidr.UpdatedDate.Value.ToLongDateString();
                        }
                        if (rl.Count > 1)
                        {
                            divrate2.Attributes.Add("class", "col-md-4 col-sm-12 col-xs-12");
                            if (!divvaset)
                                divva.Attributes.Add("class", "col-md-4 col-sm-12 col-xs-12");
                        }
                        else
                        {
                            divrate2.Attributes.Add("class", "col-md-12 col-sm-12 col-xs-12");
                            if (!divvaset)
                                divva.Attributes.Add("class", "col-md-6 col-sm-12 col-xs-12");
                        }
                        divrate2.Visible = true;
                        divvaset = true;
                        // rate idr //
                    }
                }
                lblBalance.Text = "Balance";
                var rid = 0;
                if (ddlBalance.SelectedItem.Text == "IDR")
                {
                    rid = 1;
                    lkbBalance.Text = ddlBalance.SelectedItem.Text + " " + String.Format(new CultureInfo("id-ID"), "{0:n}", db.tblM_Travel_Agent_Balance.Where(p => p.TravelAgentID == agid && p.CountryID == 103).Select(p => p.Balance).FirstOrDefault().Value).Replace("IDR", "IDR ");
                }
                else if (ddlBalance.SelectedItem.Text == "MYR")
                {
                    rid = 2;
                    lkbBalance.Text = ddlBalance.SelectedItem.Text + " " + String.Format(new CultureInfo("id-ID"), "{0:n}", db.tblM_Travel_Agent_Balance.Where(p => p.TravelAgentID == agid && p.CountryID == 133).Select(p => p.Balance).FirstOrDefault().Value).Replace("MYR", "MYR ");
                }
                else if (ddlBalance.SelectedItem.Text == "IDR")
                {
                    rid = 3;
                    lkbBalance.Text = ddlBalance.SelectedItem.Text + " " + String.Format(new CultureInfo("id-ID"), "{0:n}", db.tblM_Travel_Agent_Balance.Where(p => p.TravelAgentID == agid && p.CountryID == 103).Select(p => p.Balance).FirstOrDefault().Value).Replace("IDR", "IDR ");
                }

                ddlBalance.Visible = false;

                var tra = db.tblT_Payout.Where(p => p.isAgent == 3 && p.CreatedBy == agid && p.RemittanceID == rid).Count();

                if (db.tblT_Payout.Where(p => (p.Status == "created" || p.Status == "queued") && p.isAgent == 3 && p.CreatedBy == agid && p.RemittanceID == rid).Count() != 0)
                {
                    lbkTraPending.Text = ddlBalance.SelectedItem.Text + " " + String.Format(new CultureInfo("id-ID"), "{0:n}", db.tblT_Payout.Where(p => (p.Status == "created" || p.Status == "queued") && p.isAgent == 3 && p.CreatedBy == agid && p.RemittanceID == rid).Select(p => p.TotalTransfer).Sum()).Replace("Rp", "Rp ");
                    lblTraPendingPercent.Text = ((db.tblT_Payout.Where(p => (p.Status == "created" || p.Status == "queued") && p.isAgent == 3 && p.CreatedBy == agid && p.RemittanceID == rid).Count() * 100) / tra).ToString();
                }
                else
                {
                    lbkTraPending.Text = ddlBalance.SelectedItem.Text + " 0";
                }

                if (db.tblT_Payout.Where(p => p.Status == "completed" && p.isAgent == 3 && p.CreatedBy == agid && p.RemittanceID == rid).Count() != 0)
                {
                    lbkTraSuccess.Text = ddlBalance.SelectedItem.Text + " " + String.Format(new CultureInfo("id-ID"), "{0:n}", db.tblT_Payout.Where(p => p.Status == "completed" && p.isAgent == 3 && p.CreatedBy == agid && p.RemittanceID == rid).Select(p => p.TotalTransfer).Sum()).Replace("Rp", "Rp ");
                    lblTraSuccessPrecent.Text = ((db.tblT_Payout.Where(p => p.Status == "completed" && p.isAgent == 3 && p.CreatedBy == agid && p.RemittanceID == rid).Count() * 100) / tra).ToString();
                }
                else
                {
                    lbkTraSuccess.Text = ddlBalance.SelectedItem.Text + " 0";
                }

                if (db.tblT_Payout.Where(p => p.Status == "rejected" && p.isAgent == 3 && p.CreatedBy == agid && p.RemittanceID == rid).Count() != 0)
                {
                    lbkTraRejected.Text = ddlBalance.SelectedItem.Text + " " + String.Format(new CultureInfo("id-ID"), "{0:n}", db.tblT_Payout.Where(p => p.Status == "rejected" && p.isAgent == 3 && p.CreatedBy == agid && p.RemittanceID == rid).Select(p => p.TotalTransfer).Sum()).Replace("Rp", "Rp ");
                    lblTraRejectPercent.Text = ((db.tblT_Payout.Where(p => p.Status == "rejected" && p.isAgent == 3 && p.CreatedBy == agid && p.RemittanceID == rid).Count() * 100) / tra).ToString();
                }
                else
                {
                    lbkTraRejected.Text = ddlBalance.SelectedItem.Text + " 0";
                }

                if (db.tblT_Payout.Where(p => (p.Status == "processed" || p.Status == "approved") && p.isAgent == 3 && p.CreatedBy == agid && p.RemittanceID == rid).Count() != 0)
                {
                    lbkTraProcess.Text = ddlBalance.SelectedItem.Text + " " + String.Format(new CultureInfo("id-ID"), "{0:n}", db.tblT_Payout.Where(p => (p.Status == "processed" || p.Status == "approved") && p.isAgent == 3 && p.CreatedBy == agid && p.RemittanceID == rid).Select(p => p.TotalTransfer).Sum()).Replace("Rp", "Rp ");
                    lblTraProcessPrecent.Text = ((db.tblT_Payout.Where(p => (p.Status == "processed" || p.Status == "approved") && p.isAgent == 3 && p.CreatedBy == agid && p.RemittanceID == rid).Count() * 100) / tra).ToString();
                }
                else
                {
                    lbkTraProcess.Text = ddlBalance.SelectedItem.Text + " 0";
                }

                if (db.tblT_Payout.Where(p => p.Status == "failed" && p.isAgent == 3 && p.CreatedBy == agid && p.RemittanceID == rid).Count() != 0)
                {
                    lbkTraFailed.Text = ddlBalance.SelectedItem.Text + " " + String.Format(new CultureInfo("id-ID"), "{0:n}", db.tblT_Payout.Where(p => p.Status == "failed" && p.isAgent == 3 && p.CreatedBy == agid && p.RemittanceID == rid).Select(p => p.TotalTransfer).Sum()).Replace("Rp", "Rp ");
                    lblTraFailedPercent.Text = ((db.tblT_Payout.Where(p => p.Status == "failed" && p.isAgent == 3 && p.CreatedBy == agid && p.RemittanceID == rid).Count() * 100) / tra).ToString();
                }
                else
                {
                    lbkTraFailed.Text = ddlBalance.SelectedItem.Text + " 0";
                }
            }
        }

        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Payout/PayoutHistory");
        }

        protected void btnUpdateMargin_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/RateAgent");
        }

        protected void btnReceipt_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            Session["PayoutID"] = ((HiddenField)row.FindControl("hfPayoutID")).Value;
            //Response.Redirect("EditCurrencyRate");
            Page.ClientScript.RegisterStartupScript(this.GetType(), "OpenWindow", "window.open('/Payout/Receipt','_newtab');", true);
        }

        protected void btnCalc_Click(object sender, EventArgs e)
        {
            Response.Redirect("Calculator");
        }

        protected Boolean IsCompleted(string status)
        {
            if (status == "completed")
                return true;
            else
                return false;
        }

        void RedirectToTransferHistory(string status)
        {
            Session["PayoutHistoryStatus"] = status;
            Response.Redirect("Payout/PayoutHistory");
        }

        protected void btnDivBalance_Click(object sender, EventArgs e)
        {
            Response.Redirect("Agent/BalanceHistory");
        }

        protected void btnDivPendingTransaction_Click(object sender, EventArgs e)
        {
            RedirectToTransferHistory("pending");
        }

        protected void btnDivSuccessTransaction_Click(object sender, EventArgs e)
        {
            RedirectToTransferHistory("completed");
        }

        protected void btnDivProcessTransaction_Click(object sender, EventArgs e)
        {
            RedirectToTransferHistory("processed");
        }

        protected void btnDivRejectedTransaction_Click(object sender, EventArgs e)
        {
            RedirectToTransferHistory("rejected");
        }

        protected void btnDivFailedTransaction_Click(object sender, EventArgs e)
        {
            RedirectToTransferHistory("failed");
        }

        protected void ddlBalance_SelectedIndexChanged(object sender, EventArgs e)
        {
            count();
        }
    }
}