﻿using OMGLiveDemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo
{
    public partial class Login : System.Web.UI.Page
    {
        Common cm = new Common();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {

            if (txtHP.Text != "" && txtPassword.Text != "")
            {
                bool checkLogin = cm.checkLogin(txtHP.Text, txtPassword.Text);
                if (checkLogin)
                {
                    checkRole();
                    //Master.checkRole();
                }
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Phone and Password cannot be empty!')", true);
            }
        }

        public void checkRole()
        {
            string xxx = SessionLib.Current.Role;
            if (xxx == Models.Roles.Admin) { Response.Redirect("~/Default"); }
            else if (xxx == Models.Roles.SuperAdmin) { Response.Redirect("~/Default"); }
            else if (xxx == Models.Roles.Agent) { Response.Redirect("~/AgentDashboard"); }
            else if (xxx == Models.Roles.MasterArea) { Response.Redirect("~/AgentDashboard"); }
            else if (xxx == Models.Roles.TravelAgent) { Response.Redirect("~/AgentDashboard"); }
            else if (xxx == Models.Roles.Approver) { Response.Redirect("~/ApproverDashboard"); }
            else if (xxx == Models.Roles.Operator) { Response.Redirect("~/OperatorDashboard"); }
            else if (xxx == Models.Roles.Checker) { Response.Redirect("~/CheckerDashboard"); }
            else { Response.Redirect("~/Login"); }
        }
    }
}