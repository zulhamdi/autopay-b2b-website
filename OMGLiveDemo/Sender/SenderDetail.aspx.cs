﻿using OMGLiveDemo.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.Sender
{
    public partial class SenderDetail : System.Web.UI.Page
    {
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();

        protected void Page_Load(object sender, EventArgs e)
        {
            //tmp
            //if (!IsPostBack)
            //{
            //    Do();
            //}

            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    Do();
                }
            }
            else
            {
                Master.checkRole();
            }
            
        }

        void Do()
        {
            long senderID = 0;
            if(Session["ViewInfoSenderID"] != null)
            {
                if (Session["ViewInfoSenderID"].ToString().Trim() == "")
                {

                }
                else
                {
                    senderID = Convert.ToInt64(Session["ViewInfoSenderID"].ToString());
                }
            }

            if(senderID > 0)
            {
                tblM_Sender sender = db.tblM_Sender.Where(p => p.SenderID == senderID).FirstOrDefault();
                if(sender != null)
                {
                    populateSenderDetail(sender);
                }
                else
                {
                    showBlank();
                }
                
            }
            else
            {
                showBlank();
            }
            
        }

        void showBlank()
        {
            txtName.Text = "Sender Not Found!";
        }

        void showBlankPersonalDetail()
        {

        }

        void showBlankCompanyDetail()
        {

        }

        void populateSenderDetail(tblM_Sender sender)
        {
            txtName.Text = sender.Fullname;
            txtPhone.Text = sender.Phone;
            txtEmail.Text = sender.Email;
            txtAddress.Text = sender.Address;
            txtProvince.Text = sender.Province;
            txtPostcode.Text = sender.Postcode;

            if(sender.SenderTypeID == 1)
            {
                txtSenderType.Text = "Personal";
            }
            else if(sender.SenderTypeID == 2)
            {
                txtSenderType.Text = "Company";
            }

            tblM_City city = db.tblM_City.Where(p => p.ID == sender.CityID).FirstOrDefault();
            if(city != null)
            {
                txtCity.Text = city.City;
            }
            
            txtCountry.Text = sender.Country;
            //cbIsActive.Checked = (psd.isActive == 1) ? true : false;
            txtIsActive.Text = (sender.isActive == 1) ? "Active" : "InActive";

            if (sender.SenderTypeID == 1)
            {
                //personal
                divPersonal.Visible = true;
                divCompany.Visible = false;

                vw_Personal_Sender_Detail psd = db.vw_Personal_Sender_Detail.Where(p => p.SenderID == sender.SenderID).FirstOrDefault();

                if(psd != null)
                {
                    populatePersonalDetail(psd);
                }
            }
            else
            {
                //company
                divCompany.Visible = true;
                divPersonal.Visible = false;

                vw_Company_Sender_Detail csd = db.vw_Company_Sender_Detail.Where(p => p.SenderID == sender.SenderID).FirstOrDefault();

                if(csd != null)
                {
                    populateCompanyDetail(csd);
                }
            }
        }

        void populatePersonalDetail(vw_Personal_Sender_Detail psd)
        {
            txtPersonalIDType.Text = psd.IDTypeName;
            txtPersonalIDNo.Text = psd.IDNo;
            hfPersonalIDFileURL.Value = psd.IDURL;
            hfPictureWithIDURL.Value = psd.PictureURL;
            txtPersonalGender.Text = psd.Gender;
            txtPersonalOccupation.Text = psd.Occupation;
            txtPersonalDOB.Text = psd.DOB.Value.ToString("dd/MM/yyyy");
            txtPersonalPOB.Text = psd.POB;

            if(psd.IDValidityType != null)
            {
                if(psd.IDValidityType == "Seumur Hidup")
                {
                    txtPersonalIDValidity.Text = psd.IDValidityType;
                }
                else
                {
                    if(psd.IDValidity != null)
                    {
                        txtPersonalIDValidity.Text = psd.IDValidity.Value.ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        txtPersonalIDValidity.Text = "Unable to parse ID Validity Date";
                    }
                }
            }
            

        }

        void populateCompanyDetail(vw_Company_Sender_Detail csd)
        {
            txtLegalitasType.Text = csd.LegalitasType;
            txtLegalitasNo.Text = csd.LegalitasNo;
            hfLegalitasFile.Value = csd.LegalitasURL;
            txtCompanyNPWP.Text = csd.NPWPNo;
            hfCompanyNPWPFile.Value = csd.NPWPURL;
            txtCompanyBBH.Text = csd.BBHName;
            txtBusinessType.Text = csd.BusinessFieldName;
            txtBusinessReference.Text = csd.BusinessRef;
            txtCompanyPOI.Text = csd.IncorporationPlace;
            txtCompanyDOI.Text = csd.IncorporationDate.Value.ToString("dd/MM/yyyy");

            txtBank.Text = csd.BankName;

            /*tblM_Bank bank = db.tblM_Bank.Where(p => p.BankID == csd.BankID).First();
            if(bank != null)
            {
                txtBank.Text = bank.Name;
            }*/

            txtBankAccountNo.Text = csd.BankAccNo;
            txtPICName.Text = csd.PICName;
            txtPICDepartment.Text = csd.PICDepartment;
            txtPICIDType.Text = csd.PICIDTypeName;
            txtPICIDNo.Text = csd.PICIDNo;
            hfPICIDFile.Value = csd.PICIDURL;

            if(csd.PICIDValidityType != null)
            {
                if(csd.PICIDValidityType == "Seumur Hidup")
                {
                    txtPICIDValidity.Text = csd.PICIDValidityType;
                }
                else
                {
                    if(csd.PICIDExpiry != null)
                    {
                        txtPICIDValidity.Text = csd.PICIDExpiry.Value.ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        txtPICIDValidity.Text = "Unable to parse PIC ID Expiry Date";
                    }
                }
            }

            populateCompanyDirectorTable(csd.SenderID);
            populateCompanyShareholderTable(csd.SenderID);
        }

        void populateCompanyDirectorTable(long companyID)
        {
            List<vw_Sender_Company_Director> scd = new List<vw_Sender_Company_Director>();
            scd = db.vw_Sender_Company_Director.Where(p => p.CompanyID == companyID).ToList();
            if(scd != null)
            {
                gvCDI.DataSource = scd;
                gvCDI.DataBind();
            }
        }

        void populateCompanyShareholderTable(long companyID)
        {
            List<vw_Sender_Company_Shareholder> scs = new List<vw_Sender_Company_Shareholder>();
            scs = db.vw_Sender_Company_Shareholder.Where(p => p.CompanyID == companyID).ToList();
            if (scs != null)
            {
                gvSHI.DataSource = scs;
                gvSHI.DataBind();
            }
        }

        protected void lkbPersonalIDFile_Click(object sender, EventArgs e)
        {
            if (hfPersonalIDFileURL.Value != null)
            {
                Session["ImageURL"] = hfPersonalIDFileURL.Value;
                Session["From"] = "Image";
                string txt = "ID No : " + txtPersonalIDNo.Text;
                Session["Text"] = txtName.Text + " - " + txt;
                string queryString = "../Payout/ViewImage.aspx";
                string newWin = "window.open('" + queryString + "','Personal ID','width=900, height=400, titlebar=yes');";
                ClientScript.RegisterStartupScript(this.GetType(), "pop", newWin, true);
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alertMessage", "alert('File not found!')", true);
            }
        }

        protected void lkbPictureWithIDFile_Click(object sender, EventArgs e)
        {
            if (hfPictureWithIDURL.Value != null)
            {
                Session["ImageURL"] = hfPictureWithIDURL.Value;
                Session["From"] = "Image";
                string txt = "ID No : " + txtPersonalIDNo.Text;
                Session["Text"] = txtName.Text + " - " + txt;
                string queryString = "../Payout/ViewImage.aspx";
                string newWin = "window.open('" + queryString + "','Picture with ID','width=900, height=400, titlebar=yes');";
                ClientScript.RegisterStartupScript(this.GetType(), "pop", newWin, true);
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alertMessage", "alert('File not found!')", true);
            }
        }

        protected void lkbLegalitasFile_Click(object sender, EventArgs e)
        {
            if (hfLegalitasFile.Value != null)
            {
                Session["ImageURL"] = hfLegalitasFile.Value;
                Session["From"] = "Image";
                string txt = "Legalitas No : " + txtLegalitasNo.Text;
                Session["Text"] = txtName.Text + " - " + txt;
                string queryString = "../Payout/ViewImage.aspx";
                string newWin = "window.open('" + queryString + "','Legalitas File','width=900, height=400, titlebar=yes');";
                ClientScript.RegisterStartupScript(this.GetType(), "pop", newWin, true);
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alertMessage", "alert('File not found!')", true);
            }
        }

        protected void lkbCompanyNPWPFile_Click(object sender, EventArgs e)
        {
            if (hfCompanyNPWPFile.Value != null)
            {
                Session["ImageURL"] = hfCompanyNPWPFile.Value;
                Session["From"] = "Image";
                string txt = "NPWP No : " + txtCompanyNPWP.Text;
                Session["Text"] = txtName.Text + " - " + txt;
                string queryString = "../Payout/ViewImage.aspx";
                string newWin = "window.open('" + queryString + "','NPWP File','width=900, height=400, titlebar=yes');";
                ClientScript.RegisterStartupScript(this.GetType(), "pop", newWin, true);
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alertMessage", "alert('File not found!')", true);
            }
        }

        protected void lkbPICIDFile_Click(object sender, EventArgs e)
        {
            if (hfPICIDFile.Value != null)
            {
                Session["ImageURL"] = hfPICIDFile.Value;
                Session["From"] = "Image";
                string txt = "ID No : " + txtPICIDNo.Text;
                Session["Text"] = txtPICName.Text + " - " + txt;
                string queryString = "../Payout/ViewImage.aspx";
                string newWin = "window.open('" + queryString + "','NPWP File','width=900, height=400, titlebar=yes');";
                ClientScript.RegisterStartupScript(this.GetType(), "pop", newWin, true);
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alertMessage", "alert('File not found!')", true);
            }
        }

        protected void gvCDI_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }

        protected void gvSHI_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            
        }

        protected void gvSHI_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void gvCDI_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        /*protected void lkbViewShareholderDetail_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            Session["ViewInfoShareholderID"] = ((HiddenField)row.FindControl("hfSHIShareholderID")).Value;

            string queryString = Session["ViewInfoShareholderID"].ToString() + "www.google.com";
            string newWin = "window.open('" + queryString + "','NPWP File','width=900, height=800, titlebar=yes');";
            ClientScript.RegisterStartupScript(this.GetType(), "pop", newWin, true);
        }

        protected void lkbViewDirectorDetail_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            Session["ViewInfoDirectorID"] = ((HiddenField)row.FindControl("hfCDIDirectorID")).Value;

            string queryString = Session["ViewInfoDirectorID"].ToString() + "www.google.com";
            string newWin = "window.open('" + queryString + "','NPWP File','width=900, height=800, titlebar=yes');";
            ClientScript.RegisterStartupScript(this.GetType(), "pop", newWin, true);
        }*/

        protected void btnViewDirector_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            Session["ViewInfoDirectorID"] = ((HiddenField)row.FindControl("hfCDIDirectorID")).Value;

            string queryString = "DirectorDetail.aspx";
            string newWin = "window.open('" + queryString + "','Director Detail','width=900, height=800, titlebar=yes');";
            ClientScript.RegisterStartupScript(this.GetType(), "pop", newWin, true);
        }

        protected void btnViewShareholder_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            Session["ViewInfoShareholderID"] = ((HiddenField)row.FindControl("hfSHIShareholderID")).Value;

            string queryString = "ShareholderDetail.aspx";
            string newWin = "window.open('" + queryString + "','Shareholder Detail','width=900, height=800, titlebar=yes');";
            ClientScript.RegisterStartupScript(this.GetType(), "pop", newWin, true);
        }
    }
}