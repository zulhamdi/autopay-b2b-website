﻿using OMGLiveDemo.Models;
using OMGLiveDemo.Models.OMG;
using OMGLiveDemo.Scripts;
using Oppal.Sec;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.Sender
{
    public partial class ApproveSender : System.Web.UI.Page
    {
        OMG omg = new OMG();
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    bind();
                    bindCDI();
                    bindSHI();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        void bind()
        {
            hfSenderID.Value = Session["SenderID"].ToString();
            var id = Convert.ToInt64(hfSenderID.Value);
            var sender = db.tblM_Sender.Where(p => p.SenderID == id).FirstOrDefault();
            if (sender != null)
            {
                txtName.Text = sender.Fullname;
                txtAddress.Text = sender.Address;
                txtPhone.Text = sender.Phone;
                txtEmail.Text = sender.Email;

                if (sender.CountryID == 103 && sender.CityID != null)
                {
                    var city = db.tblM_City.Where(p => p.ID == sender.CityID).FirstOrDefault();
                    txtCity.Text = city != null ? city.City : string.Empty;
                }

                if (sender.SenderTypeID == 1)
                    txtSenderType.Text = "Personal";
                else
                    txtSenderType.Text = "Company";

                hfCountryID.Value = sender.CountryID.ToString();

                if (txtSenderType.Text == "Personal")
                {
                    var file = db.tblM_Sender_File.Where(p => p.SenderID == id).FirstOrDefault();
                    divpersonal.Visible = true;
                    txtNationalID.Text = file != null ? file.Value : string.Empty;
                    divcompany.Visible = false;
                    hfSenderImageURL.Value = file != null ? file.URL : string.Empty;

                    var spd = db.vw_Personal_Sender_Detail.Where(p => p.SenderID == id).FirstOrDefault();
                    if (spd != null)
                    {
                        hfSenderPicURL.Value = spd.PictureURL;
                        txtNationalIDExpiry.Text = spd.IDValidity.ToString();
                        txtNationality.Text = spd.CountryName;
                        if (spd.Sex == 1)
                        {
                            txtSex.Text = "Male";
                        }
                        else
                        {
                            txtSex.Text = "Female";
                        }
                        txtPOB.Text = spd.POB;
                        txtOccupation.Text = spd.Occupation;
                    }
                }
                else
                {

                    divpersonal.Visible = false;
                    divcompany.Visible = true;
                    divLegalitas.Visible = true;
                    divCompanyNPWP.Visible = true;
                    divCompanyPIC.Visible = true;
                    divCompanyDirector.Visible = true;
                    divCompanyShareholder.Visible = true;

                    if (hfCountryID.Value == "133")
                    {
                        var file = db.tblM_Sender_File.Where(p => p.SenderID == id).FirstOrDefault();
                        divSSM.Visible = true;
                        txtSSM.Text = file != null ? file.Value : string.Empty;
                        hfSenderImageURL.Value = file != null ? file.URL : string.Empty;
                    }
                    else if (hfCountryID.Value == "103")
                    {

                        var files = db.tblM_Sender_File.Where(p => p.SenderID == id).ToList();
                        if (files != null)
                        {
                            for (int i = 0; i < files.Count; i++)
                            {
                                if (files[i].FileTypeID == 4)
                                {
                                    divTDP.Visible = true;
                                    txtTDP.Text = files[i].Value;
                                    hfTDPImageURL.Value = files[i].URL;
                                }
                                else if (files[i].FileTypeID == 5)
                                {
                                    divskmenkumham.Visible = true;
                                    txtSKMenkumham.Text = files[i].Value;
                                    hfSKImageURL.Value = files[i].URL;
                                }
                                else if (files[i].FileTypeID == 6)
                                {
                                    divnpwp.Visible = true;
                                    txtNPWP.Text = files[i].Value;
                                    hfNPWPImageURL.Value = files[i].URL;
                                }
                                else if (files[i].FileTypeID == 7)
                                {
                                    divsiup.Visible = true;
                                    txtSIUP.Text = files[i].Value;
                                    hfSIUPImageURL.Value = files[i].URL;
                                }
                            }
                        }

                        var scd = db.vw_Company_Sender_Detail.Where(p => p.SenderID == id).FirstOrDefault();
                        if (scd != null)
                        {
                            txtCompanyNPWP.Text = scd.NPWPNo;
                            hfCompanyNPWPURL.Value = scd.NPWPURL;
                            txtCompanyBBH.Text = scd.BBHName;
                            txtLegalitas.Text = scd.LegalitasType;
                            hfCompanyLegalitasFileURL.Value = scd.LegalitasURL;
                            txtCompanyBusinessType.Text = scd.BusinessFieldName;
                            txtBusinessRef.Text = scd.BusinessRef;
                            txtCompanyPOI.Text = scd.IncorporationPlace;
                            txtCompanyDOI.Text = scd.IncorporationDate.ToString();
                            txtCompanyBank.Text = scd.BankName;
                            txtCompanyBankAccNo.Text = scd.BankAccNo;
                            txtCompanyPICName.Text = scd.PICName;
                            txtCompanyPICIDNo.Text = scd.PICIDNo;
                            txtCompanyPICDepartment.Text = scd.PICDepartment;
                            txtCompanyPICIDExpiry.Text = scd.PICIDExpiry.ToString();
                            hfCompanyPICIDURL.Value = scd.PICIDURL;
                        }
                    }
                }
            }
        }

        void bindCDI()
        {
            long companyID = Convert.ToInt64(hfSenderID.Value);
            List<vw_Sender_Company_Director> vwCD = new List<vw_Sender_Company_Director>();
            vwCD = db.vw_Sender_Company_Director.Where(p => p.CompanyID == companyID).ToList();

            gvCDI.DataSource = vwCD;
            gvCDI.DataBind();
        }

        void bindSHI()
        {
            long companyID = Convert.ToInt64(hfSenderID.Value);
            List<vw_Sender_Company_Shareholder> vwCS = new List<vw_Sender_Company_Shareholder>();
            vwCS = db.vw_Sender_Company_Shareholder.Where(p => p.CompanyID == companyID).ToList();

            gvSHI.DataSource = vwCS;
            gvSHI.DataBind();

        }

        protected void lkbViewImage_Click(object sender, EventArgs e)
        {
            if (hfSenderImageURL.Value != null)
            {
                Session["ImageURL"] = hfSenderImageURL.Value;
                Session["From"] = "Image";
                string txt = "";
                if (txtSenderType.Text == "Personal")
                {
                    txt = "NationalID/Passport : " + txtNationalID.Text;
                }
                else
                {
                    txt = "SSM : " + txtSSM.Text;
                }
                Session["Text"] = txtName.Text + " - " + txt;
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "colorBox", "var isColorbox = true", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('File not found!')", true);
            }
        }

        protected void lkbViewImageTDP_Click(object sender, EventArgs e)
        {
            if (hfSenderImageURL.Value != null)
            {
                Session["ImageURL"] = hfTDPImageURL.Value;
                Session["From"] = "Image";
                string txt = "TDP : " + txtTDP.Text;
                Session["Text"] = txtName.Text + " - " + txt;
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "colorBox", "var isColorbox = true", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('File not found!')", true);
            }
        }

        protected void lkbViewImageSK_Click(object sender, EventArgs e)
        {
            if (hfSenderImageURL.Value != null)
            {
                Session["ImageURL"] = hfSKImageURL.Value;
                Session["From"] = "Image";
                string txt = "SK : " + txtTDP.Text;
                Session["Text"] = txtName.Text + " - " + txt;
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "colorBox", "var isColorbox = true", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('File not found!')", true);
            }
        }

        protected void lkbViewImageNPWP_Click(object sender, EventArgs e)
        {
            if (hfSenderImageURL.Value != null)
            {
                Session["ImageURL"] = hfNPWPImageURL.Value;
                Session["From"] = "Image";
                string txt = "NPWP : " + txtTDP.Text;
                Session["Text"] = txtName.Text + " - " + txt;
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "colorBox", "var isColorbox = true", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('File not found!')", true);
            }
        }

        protected void lkbViewImageSIUP_Click(object sender, EventArgs e)
        {
            if (hfSenderImageURL.Value != null)
            {
                Session["ImageURL"] = hfSIUPImageURL.Value;
                Session["From"] = "Image";
                string txt = "SIUP : " + txtTDP.Text;
                Session["Text"] = txtName.Text + " - " + txt;
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "colorBox", "var isColorbox = true", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('File not found!')", true);
            }
        }

        protected void btnError_ServerClick(object sender, EventArgs e)
        {
            diverror.Visible = false;
        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            diverror.Visible = false;

            var id = Convert.ToInt64(hfSenderID.Value);
            var aaa = db.tblM_Sender.Where(p => p.SenderID == id).FirstOrDefault();
            aaa.Status = "approved";
            aaa.ApprovedBy = Convert.ToInt64(SessionLib.Current.AdminID);
            aaa.ApprovedDate = DateTime.Now;
            db.SaveChanges();

            string eMsg = "Approve sender: [" + aaa.SenderID + "] " + aaa.Fullname + " by user [" + aaa.ApprovedBy + "] " + SessionLib.Current.Name + ".";
            tblS_Log sLog = new tblS_Log();
            sLog.EventDate = aaa.ApprovedDate.Value;
            sLog.EventBy = aaa.ApprovedBy.Value;
            sLog.EventType = "operation";
            sLog.EventData = eMsg.Trim();

            db.tblS_Log.Add(sLog);
            db.SaveChanges();

            Session["SuccessApproveSender"] = "Success approve sender : " + txtName.Text;
            Response.Redirect("~/Sender/ApproveSenderList");
        }
        protected void btnAmend_Click(object sender, EventArgs e)
        {
            diverror.Visible = false;

            var id = Convert.ToInt64(hfSenderID.Value);
            var aaa = db.tblM_Sender.Where(p => p.SenderID == id).FirstOrDefault();
            aaa.Status = "amend";
            aaa.AmendBy = Convert.ToInt64(SessionLib.Current.AdminID);
            aaa.AmendDate = DateTime.Now;
            db.SaveChanges();

            string eMsg = "Ammend sender: [" + aaa.SenderID + "] " + aaa.Fullname + " by user [" + aaa.AmendBy + "] " + SessionLib.Current.Name + ".";
            tblS_Log sLog = new tblS_Log();
            sLog.EventDate = aaa.AmendDate.Value;
            sLog.EventBy = aaa.AmendBy.Value;
            sLog.EventType = "operation";
            sLog.EventData = eMsg.Trim();

            db.tblS_Log.Add(sLog);
            db.SaveChanges();

            Session["SuccessApproveSender"] = "Success ammend sender : " + txtName.Text;
            Response.Redirect("~/Sender/ApproveSenderList");
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            diverror.Visible = false;
            if (txtRejectReason.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Please fill the reason why this sender rejected";
                return;
            }

            var id = Convert.ToInt64(hfSenderID.Value);
            var aaa = db.tblM_Sender.Where(p => p.SenderID == id).FirstOrDefault();
            aaa.Status = "rejected";
            aaa.RejectedBy = Convert.ToInt64(SessionLib.Current.AdminID);
            aaa.RejectedDate = DateTime.Now;
            aaa.Message = txtRejectReason.Text.Trim();
            db.SaveChanges();

            string eMsg = "Reject sender: [" + aaa.SenderID + "] " + aaa.Fullname + " by user [" + aaa.RejectedBy + "] " + SessionLib.Current.Name + ".";
            tblS_Log sLog = new tblS_Log();
            sLog.EventDate = aaa.RejectedDate.Value;
            sLog.EventBy = aaa.RejectedBy.Value;
            sLog.EventType = "operation";
            sLog.EventData = eMsg.Trim();

            db.tblS_Log.Add(sLog);
            db.SaveChanges();


            Session["SuccessApproveSender"] = "Success reject sender with reason : " + txtRejectReason.Text;
            Response.Redirect("~/Sender/ApproveSenderList");
        }

        protected void lkbSenderPic_Click(object sender, EventArgs e)
        {
            if (hfSenderPicURL.Value != null)
            {
                Session["ImageURL"] = hfSenderPicURL.Value;
                Session["From"] = "Image";
                string txt = "Sender Picture : " + txtNationalID.Text;
                Session["Text"] = txtName.Text + " - " + txt;
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "colorBox", "var isColorbox = true", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('File not found!')", true);
            }
        }

        protected void lkbLegalitasFile_Click(object sender, EventArgs e)
        {

        }

        protected void lkbCompanyLegalitasFile_Click(object sender, EventArgs e)
        {
            if (hfCompanyLegalitasFileURL.Value != null)
            {
                Session["ImageURL"] = hfCompanyLegalitasFileURL.Value;
                Session["From"] = "Image";
                string txt = "Company Legalitas Document : " + txtLegalitas.Text;
                Session["Text"] = txtName.Text + " - " + txt;
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "colorBox", "var isColorbox = true", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('File not found!')", true);
            }
        }

        protected void lkbCompanyNPWPFile_Click(object sender, EventArgs e)
        {
            if (hfCompanyNPWPURL.Value != null)
            {
                Session["ImageURL"] = hfCompanyNPWPURL.Value;
                Session["From"] = "Image";
                string txt = "Company NPWP : " + txtCompanyNPWP.Text;
                Session["Text"] = txtName.Text + " - " + txt;
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "colorBox", "var isColorbox = true", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('File not found!')", true);
            }
        }

        protected void lkbCompanyPICIDFile_Click(object sender, EventArgs e)
        {
            if (hfCompanyPICIDURL.Value != null)
            {
                Session["ImageURL"] = hfCompanyPICIDURL.Value;
                Session["From"] = "Image";
                string txt = "Company PIC ID : " + txtCompanyPICIDNo.Text;
                Session["Text"] = txtName.Text + " - " + txt;
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "colorBox", "var isColorbox = true", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('File not found!')", true);
            }
        }

        protected void btnViewShareholder_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            Session["ViewInfoShareholderID"] = ((HiddenField)row.FindControl("hfSHIShareholderID")).Value;

            string queryString = "ShareholderDetail.aspx";
            string newWin = "window.open('" + queryString + "','Shareholder Detail','width=900, height=800, titlebar=yes');";
            ClientScript.RegisterStartupScript(this.GetType(), "pop", newWin, true);
        }

        protected void btnViewDirector_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            Session["ViewInfoDirectorID"] = ((HiddenField)row.FindControl("hfCDIDirectorID")).Value;

            string queryString = "DirectorDetail.aspx";
            string newWin = "window.open('" + queryString + "','Director Detail','width=900, height=800, titlebar=yes');";
            ClientScript.RegisterStartupScript(this.GetType(), "pop", newWin, true);
        }

        protected void gvSHI_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvSHI.PageIndex = e.NewPageIndex;
            bindSHI();
        }

        protected void gvCDI_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvCDI.PageIndex = e.NewPageIndex;
            bindCDI();
        }
    }
}