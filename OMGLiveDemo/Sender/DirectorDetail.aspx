﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DirectorDetail.aspx.cs" Inherits="OMGLiveDemo.Sender.DirectorDetail" %>
<%@ MasterType VirtualPath="~/Site.Master" %>

<!DOCTYPE html>

<html lang="en">
<head runat="server" aria-expanded="false">
    <title>AutoPay - Sender's Detail View</title>

    <!-- Bootstrap -->
    <link href="../Content/bootstrap.min.css" rel="stylesheet"/>
    <!-- Font Awesome -->
    <link href="../Content/font-awesome.min.css" rel="stylesheet"/>

    <!-- Custom Theme Style 
    <link href="../Content/base-custom.min.css" rel="stylesheet" />
    -->
    <link href="../Content/custom-alt.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server" class="form-horizontal form-label-left">
        <asp:ScriptManager runat="server" />
        <div class="container body">
            <div class="main_container">
                <div class="right_col" role="main">
                    <div class="page-title">
                        <div class="title_left">
                            <h3>Sender's Details</h3>
                        </div>

                    </div>

                    <div class="clearfix"></div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h4>Shareholder Information:</h4>
                                </div>
                                <div class="x_content">
                                    
                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label9" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="Sender Name :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtName" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                        
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label15" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="Nationality :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtNationality" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                        
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label2" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="ID Type :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtIDType" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                        
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label6" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="ID No :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtIDNo" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                            <asp:HiddenField runat="server" ID="hfIDFileURL" />
                                            <asp:LinkButton ID="lkbIDFile" runat="server" Text="View ID File" ForeColor="Green" OnClick="lkbIDFile_Click" />
                                        </div>
                                        
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label7" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="ID's Country of Issuance :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtIDCountry" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                        
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label8" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="ID's Validity :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtIDValidity" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                        
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label1" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="Phone :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtPhone" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                        
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label3" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="Address :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtAddress" CssClass="form-control" TextMode="MultiLine" Rows="3" Enabled="false" />
                                        </div>
                                        
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label4" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="City :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtCity" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                        
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label16" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="Province :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtProvince" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                        
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label17" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="Postcode :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtPostcode" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                        
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label10" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="Country :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtCountry" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                        
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label21" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="Address Status :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtAddressStatus" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                        
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label11" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="NPWP No :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtNPWP" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                        
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label12" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="Gender :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtGender" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                        
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label13" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="Date of Birth :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtDOB" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                        
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label14" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="Place of Birth :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtPOB" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                        
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label18" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="Religion :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtReligion" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                        
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label19" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="Marital Status :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtMaritalStatus" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                        
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="Label20" CssClass="control-label col-md-3 col-sm-3 col-xs-12" Text="Education Level :" />
                                        <div class="col-md-6 col-sm-6 col-xs-12" >
                                            <asp:TextBox runat="server" ID="txtEducation" CssClass="form-control col-md-7 col-xs-12" Enabled="false" />
                                        </div>
                                        
                                    </div>

                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
