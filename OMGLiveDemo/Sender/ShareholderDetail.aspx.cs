﻿using OMGLiveDemo.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.Sender
{
    public partial class ShareholderDetail : System.Web.UI.Page
    {
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();

        protected void Page_Load(object sender, EventArgs e)
        {
            //tmp
            //Session["ViewInfoShareholderID"] = "11";
            //if (!IsPostBack)
            //{
            //    Do();
            //}

            
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    Do();
                }
            }
            else
            {
                Master.checkRole();
            }
            
        }

        void Do()
        {
            long shareholderID = 0;
            if(Session["ViewInfoShareholderID"] != null)
            {
                if (Session["ViewInfoShareholderID"].ToString().Trim() == "")
                {

                }
                else
                {
                    shareholderID = Convert.ToInt64(Session["ViewInfoShareholderID"].ToString());
                }
            }

            if(shareholderID > 0)
            {
                vw_Shareholder_Detail shareholder = db.vw_Shareholder_Detail.Where(p => p.ID == shareholderID).First();
                if(shareholder != null)
                {
                    populateShareholderDetail(shareholder);
                }
                else
                {
                    showBlank();
                }
                
            }
            else
            {
                showBlank();
            }
            
        }

        void showBlank()
        {
            txtName.Text = "Shareholder Not Found!";
        }

        void populateShareholderDetail(vw_Shareholder_Detail shareholder)
        {
            txtName.Text = shareholder.Name;
            txtHoldingType.Text = shareholder.HoldingTypeName;
            txtNationality.Text = shareholder.ShareholderNationality;
            txtPhone.Text = shareholder.Phone;
            txtAddress.Text = shareholder.Address;
            txtCity.Text = shareholder.City;
            txtProvince.Text = shareholder.Province;
            txtPostcode.Text = shareholder.Postcode;
            txtCountry.Text = shareholder.ShareholderCountry;
            txtIDType.Text = shareholder.IDTypeName;
            txtIDNo.Text = shareholder.IDNo;
            txtIDCountry.Text = shareholder.IDCountry;
            txtAddressStatus.Text = shareholder.ShareholderAddressStatus;

            if(shareholder.IDValidityType == "Seumur Hidup")
            {
                txtIDValidity.Text = "Seumur Hidup";
            }
            else
            {
                if(shareholder.IDExpiryDate != null)
                {
                    txtIDValidity.Text = shareholder.IDExpiryDate.Value.ToString("dd/MM/yyyy");
                }
                else
                {
                    txtIDValidity.Text = "Unable to parse ID Validity Date";
                }
            }

            txtNPWP.Text = shareholder.NPWP;
            txtGender.Text = shareholder.Gender;

            if(shareholder.DOB != null)
            {
                txtDOB.Text = shareholder.DOB.Value.ToString("dd/MM/yyyy");
            }
            else
            {
                txtDOB.Text = "Unable to parse ID Validity Date";
            }

            txtPOB.Text = shareholder.BirthPlace;
            txtReligion.Text = shareholder.ShareholderReligion;
            txtMaritalStatus.Text = shareholder.ShareholderMaritalStatus;
            txtEducation.Text = shareholder.ShareholderEducationLevel;

        }

        protected void lkbIDFile_Click(object sender, EventArgs e)
        {
            if (hfIDFileURL.Value != null)
            {
                Session["ImageURL"] = hfIDFileURL.Value;
                Session["From"] = "Image";
                string txt = "ID No : " + txtIDNo.Text;
                Session["Text"] = txtName.Text + " - " + txt;
                string queryString = "../Payout/ViewImage.aspx";
                string newWin = "window.open('" + queryString + "','Personal ID','width=900, height=400, titlebar=yes');";
                ClientScript.RegisterStartupScript(this.GetType(), "pop", newWin, true);
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alertMessage", "alert('File not found!')", true);
            }
        }
    }
}