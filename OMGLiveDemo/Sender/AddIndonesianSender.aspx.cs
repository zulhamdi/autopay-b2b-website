﻿using OMGLiveDemo.Models;
using OMGLiveDemo.Models.OMG;
using OMGLiveDemo.Scripts;
using Oppal.Sec;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.Sender
{
    public partial class AddIndonesianSender : System.Web.UI.Page
    {
        OMG omg = new OMG();
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    setTMPCompanyID();
                    bind();
                    bindCDI();
                    bindSHI();
                    resetFileUpload();
                }
                else
                {
                    manageFileUpload();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        void resetFileUpload()
        {
            Session.Remove("SenderIDFile");
            if (!fuNationalID.HasFile)
            {
                lblUploadedNationalIDFile.Text = "Uploaded File = NONE";
            }

            Session.Remove("SenderWithIDFile");
            if (!fuSenderWithID.HasFile)
            {
                lblUploadedSenderWithIDFile.Text = "Uploaded File = NONE";
            }

            Session.Remove("CompanyLegalitasFile");
            if (!fuLegalitas.HasFile)
            {
                lblUploadedLegalitasFile.Text = "Uploaded File = NONE";
            }

            Session.Remove("CompanyNPWPFile");
            if (!fuCompanyNPWP.HasFile)
            {
                lblUploadedCompanyNPWP.Text = "Uploaded File = NONE";
            }

            Session.Remove("PICIDFile");
            if (!fuPICID.HasFile)
            {
                lblUploadedPICIDFile.Text = "Uploaded File = NONE";
            }

            Session.Remove("DirectorIDFile");
            if (!fuCompanyDirectorIDFile.HasFile)
            {
                lblUploadedCompanyDirectorIDFile.Text = "Uploaded File = NONE";
            }

            Session.Remove("ShareholderIDFile");
            if (!fuCompanyShareholderIDFile.HasFile)
            {
                lblUploadedShareholderIDFile.Text = "Uploaded File = NONE";
            }
        }

        void manageFileUpload()
        {
            if(ddlSenderType.SelectedValue == "1")
            {
                managePersonalSenderFileUpload();
            }
            else
            {
                manageCompanySenderFileUpload();
            }
        }

        void managePersonalSenderFileUpload()
        {
            if (!fuNationalID.HasFile)
            {
                lblUploadedNationalIDFile.Text = "Uploaded File = NONE";
            }

            //ID File
            if (Session["SenderIDFile"] == null && fuNationalID.HasFile)
            {
                Session["SenderIDFile"] = fuNationalID;
                lblUploadedNationalIDFile.Text = "Uploaded File = " + fuNationalID.FileName;
            }
            // Next time submit and Session has values but FileUpload is Blank
            // Return the values from session to FileUpload
            else if (Session["SenderIDFile"] != null && (!fuNationalID.HasFile))
            {
                fuNationalID = (FileUpload)Session["SenderIDFile"];
                lblUploadedNationalIDFile.Text = "Uploaded File = " + fuNationalID.FileName;
            }
            // Now there could be another situation when Session has File but user want to change the file
            // In this case we have to change the file in session object
            else if (fuNationalID.HasFile)
            {
                Session["SenderIDFile"] = fuNationalID;
                lblUploadedNationalIDFile.Text = "Uploaded File = " + fuNationalID.FileName;
            }

            if(!fuSenderWithID.HasFile)
            {
                lblUploadedSenderWithIDFile.Text = "Uploaded File = NONE";
            }

            //Sender With ID Picture File
            if (Session["SenderWithIDFile"] == null && fuSenderWithID.HasFile)
            {
                Session["SenderWithIDFile"] = fuSenderWithID;
                lblUploadedSenderWithIDFile.Text = "Uploaded File = " + fuSenderWithID.FileName;
            }
            // Next time submit and Session has values but FileUpload is Blank
            // Return the values from session to FileUpload
            else if (Session["SenderWithIDFile"] != null && (!fuSenderWithID.HasFile))
            {
                fuSenderWithID = (FileUpload)Session["SenderWithIDFile"];
                lblUploadedSenderWithIDFile.Text = "Uploaded File = " + fuSenderWithID.FileName;
            }
            // Now there could be another sictution when Session has File but user want to change the file
            // In this case we have to change the file in session object
            else if (fuSenderWithID.HasFile)
            {
                Session["SenderWithIDFile"] = fuSenderWithID;
                lblUploadedSenderWithIDFile.Text = "Uploaded File = " + fuSenderWithID.FileName;
            }
        }

        void manageCompanySenderFileUpload()
        {
            refreshCompanyFilesInfo();

            //Legalitas File
            if (Session["CompanyLegalitasFile"] == null && fuLegalitas.HasFile)
            {
                Session["CompanyLegalitasFile"] = fuLegalitas;
                lblUploadedLegalitasFile.Text = "Uploaded File = " + fuLegalitas.FileName;
            }
            // Next time submit and Session has values but FileUpload is Blank
            // Return the values from session to FileUpload
            else if (Session["CompanyLegalitasFile"] != null && (!fuLegalitas.HasFile))
            {
                fuLegalitas = (FileUpload)Session["CompanyLegalitasFile"];
                lblUploadedLegalitasFile.Text = "Uploaded File = " + fuLegalitas.FileName;
            }
            // Now there could be another sictution when Session has File but user want to change the file
            // In this case we have to change the file in session object
            else if (fuLegalitas.HasFile)
            {
                Session["CompanyLegalitasFile"] = fuLegalitas;
                lblUploadedLegalitasFile.Text = "Uploaded File = " + fuLegalitas.FileName;
            }

            //NPWP File
            if (Session["CompanyNPWPFile"] == null && fuCompanyNPWP.HasFile)
            {
                Session["CompanyNPWPFile"] = fuCompanyNPWP;
                lblUploadedCompanyNPWP.Text = "Uploaded File = " + fuCompanyNPWP.FileName;
            }
            // Next time submit and Session has values but FileUpload is Blank
            // Return the values from session to FileUpload
            else if (Session["CompanyNPWPFile"] != null && (!fuCompanyNPWP.HasFile))
            {
                fuCompanyNPWP = (FileUpload)Session["CompanyNPWPFile"];
                lblUploadedCompanyNPWP.Text = "Uploaded File = " + fuCompanyNPWP.FileName;
            }
            // Now there could be another sictution when Session has File but user want to change the file
            // In this case we have to change the file in session object
            else if (fuCompanyNPWP.HasFile)
            {
                Session["CompanyNPWPFile"] = fuCompanyNPWP;
                lblUploadedCompanyNPWP.Text = "Uploaded File = " + fuCompanyNPWP.FileName;
            }

            //PIC ID File
            if (Session["PICIDFile"] == null && fuPICID.HasFile)
            {
                Session["PICIDFile"] = fuPICID;
                lblUploadedPICIDFile.Text = "Uploaded File = " + fuPICID.FileName;
            }
            // Next time submit and Session has values but FileUpload is Blank
            // Return the values from session to FileUpload
            else if (Session["PICIDFile"] != null && (!fuPICID.HasFile))
            {
                fuPICID = (FileUpload)Session["PICIDFile"];
                lblUploadedPICIDFile.Text = "Uploaded File = " + fuPICID.FileName;
            }
            // Now there could be another sictution when Session has File but user want to change the file
            // In this case we have to change the file in session object
            else if (fuPICID.HasFile)
            {
                Session["PICIDFile"] = fuPICID;
                lblUploadedPICIDFile.Text = "Uploaded File = " + fuPICID.FileName;
            }

            //Director ID File
            if (Session["DirectorIDFile"] == null && fuCompanyDirectorIDFile.HasFile)
            {
                Session["DirectorIDFile"] = fuCompanyDirectorIDFile;
                lblUploadedCompanyDirectorIDFile.Text = "Uploaded File = " + fuCompanyDirectorIDFile.FileName;
            }
            // Next time submit and Session has values but FileUpload is Blank
            // Return the values from session to FileUpload
            else if (Session["DirectorIDFile"] != null && (!fuCompanyDirectorIDFile.HasFile))
            {
                fuCompanyDirectorIDFile = (FileUpload)Session["DirectorIDFile"];
                lblUploadedCompanyDirectorIDFile.Text = "Uploaded File = " + fuCompanyDirectorIDFile.FileName;
            }
            // Now there could be another sictution when Session has File but user want to change the file
            // In this case we have to change the file in session object
            else if (fuCompanyDirectorIDFile.HasFile)
            {
                Session["DirectorIDFile"] = fuCompanyDirectorIDFile;
                lblUploadedCompanyDirectorIDFile.Text = "Uploaded File = " + fuCompanyDirectorIDFile.FileName;
            }

            //Shareholder ID File
            if (Session["ShareholderIDFile"] == null && fuCompanyShareholderIDFile.HasFile)
            {
                Session["ShareholderIDFile"] = fuCompanyShareholderIDFile;
                lblUploadedShareholderIDFile.Text = "Uploaded File = " + fuCompanyShareholderIDFile.FileName;
            }
            // Next time submit and Session has values but FileUpload is Blank
            // Return the values from session to FileUpload
            else if (Session["ShareholderIDFile"] != null && (!fuCompanyShareholderIDFile.HasFile))
            {
                fuCompanyShareholderIDFile = (FileUpload)Session["ShareholderIDFile"];
                lblUploadedShareholderIDFile.Text = "Uploaded File = " + fuCompanyShareholderIDFile.FileName;
            }
            // Now there could be another sictution when Session has File but user want to change the file
            // In this case we have to change the file in session object
            else if (fuCompanyShareholderIDFile.HasFile)
            {
                Session["ShareholderIDFile"] = fuCompanyShareholderIDFile;
                lblUploadedShareholderIDFile.Text = "Uploaded File = " + fuCompanyShareholderIDFile.FileName;
            }
        }

        void refreshCompanyFilesInfo()
        {
            if (!fuLegalitas.HasFile)
            {
                lblUploadedLegalitasFile.Text = "Uploaded File = NONE";
            }

            if (!fuCompanyNPWP.HasFile)
            {
                lblUploadedCompanyNPWP.Text = "Uploaded File = NONE";
            }

            if (!fuPICID.HasFile)
            {
                lblUploadedPICIDFile.Text = "Uploaded File = NONE";
            }

            if (!fuCompanyDirectorIDFile.HasFile)
            {
                lblUploadedCompanyDirectorIDFile.Text = "Uploaded File = NONE";
            }

            if (!fuCompanyShareholderIDFile.HasFile)
            {
                lblUploadedShareholderIDFile.Text = "Uploaded File = NONE";
            }
        }

        void setTMPCompanyID()
        {
            if (Session["TMPCompanyID"] == null)
            {
                //new company definition
                Session["TMPCompanyID"] = Guid.NewGuid().ToString();
            }
        }

        void bind()
        {
            var indonesianIDType = db.tblM_Person_ID_Type.Where(p => p.CountryM49 == 360 || p.ID == 2).ToList();
            ddlNationalIDType.DataSource = indonesianIDType;
            ddlNationalIDType.DataValueField = "ID";
            ddlNationalIDType.DataTextField = "Name";
            ddlNationalIDType.DataBind();

            var sender = db.tblM_Sender_Type.ToList();
            ddlSenderType.DataSource = null;
            ddlSenderType.DataSource = sender;
            ddlSenderType.DataValueField = "ID";
            ddlSenderType.DataTextField = "Type";
            ddlSenderType.DataBind();

            var compType = db.tblM_Company_Type.ToList();
            ddlLegalitasType.DataSource = null;
            ddlLegalitasType.DataSource = compType;
            ddlLegalitasType.DataValueField = "ID";
            ddlLegalitasType.DataTextField = "Type";
            ddlLegalitasType.DataBind();

            var bbh = db.tblM_Bentuk_Badan_Hukum.ToList();
            ddlBBH.DataSource = null;
            ddlBBH.DataSource = bbh;
            ddlBBH.DataValueField = "ID";
            ddlBBH.DataTextField = "Type";
            ddlBBH.DataBind();

            var busField = db.tblM_Business_Field.ToList();
            ddlBusinessField.DataSource = null;
            ddlBusinessField.DataSource = busField;
            ddlBusinessField.DataValueField = "ID";
            ddlBusinessField.DataTextField = "Type";
            ddlBusinessField.DataBind();

            var bank = db.tblM_Bank.Where(p => p.CountryID == 103).ToList();
            ddlBank.DataSource = null;
            ddlBank.DataSource = bank;
            ddlBank.DataValueField = "BankID";
            ddlBank.DataTextField = "Name";
            ddlBank.DataBind();

            var idType = db.tblM_Person_ID_Type.ToList();
            ddlDirectorIDType.DataSource = null;
            ddlDirectorIDType.DataSource = idType;
            ddlDirectorIDType.DataValueField = "ID";
            ddlDirectorIDType.DataTextField = "Name";
            ddlDirectorIDType.DataBind();

            ddlShareholderIDType.DataSource = null;
            ddlShareholderIDType.DataSource = idType;
            ddlShareholderIDType.DataValueField = "ID";
            ddlShareholderIDType.DataTextField = "Name";
            ddlShareholderIDType.DataBind();

            ddlPICIDType.DataSource = null;
            ddlPICIDType.DataSource = idType;
            ddlPICIDType.DataValueField = "ID";
            ddlPICIDType.DataTextField = "Name";
            ddlPICIDType.DataBind();

            var country = db.tblM_Country.ToList();

            ddlSenderNationality.DataSource = null;
            ddlSenderNationality.DataSource = country;
            ddlSenderNationality.DataValueField = "M49";
            ddlSenderNationality.DataTextField = "Name";
            ddlSenderNationality.DataBind();

            ddlDirectorNationality.DataSource = null;
            ddlDirectorNationality.DataSource = country;
            ddlDirectorNationality.DataValueField = "M49";
            ddlDirectorNationality.DataTextField = "Name";
            ddlDirectorNationality.DataBind();

            ddlShareholderNationality.DataSource = null;
            ddlShareholderNationality.DataSource = country;
            ddlShareholderNationality.DataValueField = "M49";
            ddlShareholderNationality.DataTextField = "Name";
            ddlShareholderNationality.DataBind();

            ddlDirectorCountryM49.DataSource = null;
            ddlDirectorCountryM49.DataSource = country;
            ddlDirectorCountryM49.DataValueField = "M49";
            ddlDirectorCountryM49.DataTextField = "Name";
            ddlDirectorCountryM49.DataBind();

            ddlShareholderCountryM49.DataSource = null;
            ddlShareholderCountryM49.DataSource = country;
            ddlShareholderCountryM49.DataValueField = "M49";
            ddlShareholderCountryM49.DataTextField = "Name";
            ddlShareholderCountryM49.DataBind();

            var religion = db.tblM_Religion.ToList();

            ddlDirectorReligion.DataSource = null;
            ddlDirectorReligion.DataSource = religion;
            ddlDirectorReligion.DataValueField = "ID";
            ddlDirectorReligion.DataTextField = "Name";
            ddlDirectorReligion.DataBind();

            ddlShareholderReligion.DataSource = null;
            ddlShareholderReligion.DataSource = religion;
            ddlShareholderReligion.DataValueField = "ID";
            ddlShareholderReligion.DataTextField = "Name";
            ddlShareholderReligion.DataBind();

            var education = db.tblM_Education_Level.ToList();

            ddlDirectorEducationLevel.DataSource = null;
            ddlDirectorEducationLevel.DataSource = education;
            ddlDirectorEducationLevel.DataValueField = "ID";
            ddlDirectorEducationLevel.DataTextField = "Name";
            ddlDirectorEducationLevel.DataBind();

            ddlShareholderEducationLevel.DataSource = null;
            ddlShareholderEducationLevel.DataSource = education;
            ddlShareholderEducationLevel.DataValueField = "ID";
            ddlShareholderEducationLevel.DataTextField = "Name";
            ddlShareholderEducationLevel.DataBind();

            /*var city = db.tblM_City.ToList();

            ddlDirectorCity.DataSource = null;
            ddlDirectorCity.DataSource = city;
            ddlDirectorCity.DataValueField = "ID";
            ddlDirectorCity.DataTextField = "City";
            ddlDirectorCity.DataBind();

            ddlShareholderCity.DataSource = null;
            ddlShareholderCity.DataSource = city;
            ddlShareholderCity.DataValueField = "ID";
            ddlShareholderCity.DataTextField = "City";
            ddlShareholderCity.DataBind();*/
        }

        void bindCDI()
        {

            List<vw_TMP_Company_Director> vwTCD = new List<vw_TMP_Company_Director>();
            var tmpCompanyID = Session["TMPCompanyID"].ToString();
            vwTCD = db.vw_TMP_Company_Director.Where(p => p.TMPCompanyID == tmpCompanyID).ToList();

            gvCDI.DataSource = vwTCD;
            gvCDI.DataBind();

        }

        void bindSHI()
        {

            List<vw_TMP_Company_Shareholder> vwTCS = new List<vw_TMP_Company_Shareholder>();
            var tmpCompanyID = Session["TMPCompanyID"].ToString();
            vwTCS = db.vw_TMP_Company_Shareholder.Where(p => p.TMPCompanyID == tmpCompanyID).ToList();

            gvSHI.DataSource = vwTCS;
            gvSHI.DataBind();

        }

        void setIDValidityVisibility()
        {
            //Personal Sender
            divNationalIDValidityDate.Visible = !cbNationalIDValidityType.Checked;

            //PIC
            divPICIDValidityDate.Visible = !cbPICIDValidityType.Checked;
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            diverror.Visible = false;
            var id = Int64.Parse(SessionLib.Current.AdminID);

            DateTime eDate = DateTime.Now;

            if (txtName.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Name cannot be empty!";
                return;
            }

            if (txtCity.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Please choose sender's city first!";
                return;
            }

            if (txtAddress.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Address cannot be empty!";
                return;
            }

            if (txtAddress.Text.Count() < 10)
            {
                diverror.Visible = true;
                lblError.Text = "Your address is too short! Please add your address more than 10 chars.";
                return;
            }

            if (txtPhone.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Phone cannot be empty!";
                return;
            }

            if (txtEmail.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Email cannot be empty!";
                return;
            }

            if (ddlSenderType.SelectedValue == "1")
            {
                if (txtNationalID.Text == "")
                {
                    diverror.Visible = true;
                    lblError.Text = "KTP / Passport cannot be empty!";
                    return;
                }

                if(ddlNationalIDType.SelectedValue == "-1")
                {
                    diverror.Visible = true;
                    lblError.Text = "Please select Sender ID Type";
                    return;
                }

                if (!fuNationalID.HasFile)
                {
                    lblError.Text = "Please choose photo of your KTP / Passport first";
                    diverror.Visible = true;
                    return;
                }
                else
                {
                    if (fuNationalID.PostedFile.ContentType != "image/jpg" && fuNationalID.PostedFile.ContentType != "image/jpeg" && fuNationalID.PostedFile.ContentType != "image/png")
                    {
                        lblError.Text = "Only jpeg, jpg and png format are allowed";
                        diverror.Visible = true;
                        return;
                    }
                }

                if (!fuSenderWithID.HasFile)
                {
                    lblError.Text = "Please choose photo of you with your KTP / Passport";
                    diverror.Visible = true;
                    return;
                }
                else
                {
                    if (fuSenderWithID.PostedFile.ContentType != "image/jpg" && fuSenderWithID.PostedFile.ContentType != "image/jpeg" && fuSenderWithID.PostedFile.ContentType != "image/png")
                    {
                        lblError.Text = "Only jpeg, jpg and png format are allowed";
                        diverror.Visible = true;
                        return;
                    }
                }

                if(!cbNationalIDValidityType.Checked)
                {
                    if (nationalIDExpiry.Text == "")
                    {
                        diverror.Visible = true;
                        lblError.Text = "Sender ID Validity Date cannot be empty";
                        return;
                    }
                }

                if (ddlSenderNationality.SelectedValue == "-1")
                {
                    diverror.Visible = true;
                    lblError.Text = "Please select sender nationality";
                    return;
                }

                if (txtSenderOccupation.Text == "")
                {
                    diverror.Visible = true;
                    lblError.Text = "Sender occupation cannot be empty!";
                    return;
                }

                if (senderDOB.Text == "")
                {
                    diverror.Visible = true;
                    lblError.Text = "Sender date of birth cannot be empty!";
                    return;
                }

                if (txtPOB.Text == "")
                {
                    diverror.Visible = true;
                    lblError.Text = "Sender place of birth cannot be empty!";
                    return;
                }
            } 
            else
            {
                if(txtPICName.Text == "")
                {
                    diverror.Visible = true;
                    lblError.Text = "PIC name cannot be empty";
                    return;
                }

                if (ddlPICIDType.Text == "")
                {
                    diverror.Visible = true;
                    lblError.Text = "Please select PIC ID Type";
                    return;
                }

                if(!cbPICIDValidityType.Checked)
                {
                    if(picIDExpiry.Text == "")
                    {
                        diverror.Visible = true;
                        lblError.Text = "Company PIC Document ID Validity Date cannot be empty";
                        return;
                    }
                }

                if (txtPICIDNo.Text == "")
                {
                    diverror.Visible = true;
                    lblError.Text = "PIC ID No cannot be empty";
                    return;
                }

                if (!fuPICID.HasFile)
                {
                    diverror.Visible = true;
                    lblError.Text = "Please upload PIC ID image";
                    return;
                }
                else
                {
                    if (fuPICID.PostedFile.ContentType != "image/jpg" && fuPICID.PostedFile.ContentType != "image/jpeg" && fuPICID.PostedFile.ContentType != "image/png")
                    {
                        lblError.Text = "Only jpeg, jpg and png format are allowed";
                        diverror.Visible = true;
                        return;
                    }
                }

                if (ddlLegalitasType.SelectedValue == "")
                {
                    diverror.Visible = true;
                    lblError.Text = "Please select Company Type";
                    return;
                }

                if(ddlBank.SelectedValue == "")
                {
                    diverror.Visible = true;
                    lblError.Text = "Please select Company Bank Name";
                    return;
                }

                if (txtBankAccount.Text == "")
                {
                    diverror.Visible = true;
                    lblError.Text = "Bank Account number cannot be empty";
                    return;
                }

                if(gvCDI.Rows.Count < 1)
                {
                    diverror.Visible = true;
                    lblError.Text = "Please add at least 1 Company Director";
                    return;
                }
            }

            tblM_Sender check;

            if (SessionLib.Current.Role == Roles.Agent)
            {
                check = db.tblM_Sender.Where(p => p.AgentID == id && p.Phone == txtPhone.Text.Trim() && p.isAgent == 1).FirstOrDefault();
            }
            else if (SessionLib.Current.Role == Roles.MasterArea)
            {
                check = db.tblM_Sender.Where(p => p.AgentID == id && p.Phone == txtPhone.Text.Trim() && p.isAgent == 2).FirstOrDefault();
            }
            else
            {
                check = db.tblM_Sender.Where(p => p.Phone == txtPhone.Text.Trim() && p.isAgent == 0).FirstOrDefault();
            }

            if (check == null)
            {

                tblM_Sender bnf = new tblM_Sender();
                bnf.Fullname = txtName.Text.Trim();
                bnf.CityID = Convert.ToInt64(hfCityID.Value);
                bnf.Address = txtAddress.Text.Trim();
                bnf.Province = txtProvince.Text.Trim();
                bnf.Postcode = txtPostcode.Text.Trim();
                bnf.Phone = txtPhone.Text.Trim();
                bnf.Email = txtEmail.Text.Trim();
                bnf.CountryID = 103;
                bnf.Country = "Indonesia";
                bnf.JoinDate = eDate;
                bnf.AddedBy = Convert.ToInt64(SessionLib.Current.AdminID);
                bnf.Email = txtEmail.Text.Trim();
                bnf.SenderTypeID = Convert.ToInt64(ddlSenderType.SelectedValue);
                if (SessionLib.Current.Role == Roles.Agent)
                {
                    bnf.AgentID = id;
                    bnf.isAgent = 1;
                }
                else if (SessionLib.Current.Role == Roles.MasterArea)
                {
                    bnf.AgentID = id;
                    bnf.isAgent = 2;
                }
                else
                {
                    bnf.AgentID = 0;
                    bnf.isAgent = 0;
                }
                bnf.AddedBy = bnf.AgentID;
                bnf.isActive = 1;
                bnf.Status = "created";
                db.tblM_Sender.Add(bnf);
                db.SaveChanges();

                if (bnf.SenderTypeID == 1)
                {
                    var ftype = db.tblM_Sender_File_Type.Where(p => p.SenderTypeID == bnf.SenderTypeID && p.CountryID == 103).FirstOrDefault();
                    tblM_Sender_File sf = new tblM_Sender_File();
                    sf.SenderID = bnf.SenderID;
                    sf.FileTypeID = ftype.ID;
                    sf.Value = txtNationalID.Text.ToString().Trim();

                    string filename = bnf.SenderID + "_NID_" + Common.getTimeSpan() + "." + fuNationalID.PostedFile.ContentType.Replace("image/", "");
                    string targetPath = Server.MapPath("../Sender/Picture/Indonesia/Personal/" + filename);
                    Stream strm = fuNationalID.PostedFile.InputStream;
                    Common.SaveImage(0.5, strm, targetPath);

                    sf.URL = ConfigurationManager.AppSettings["URL_SENDER_INDONESIA_PERSONAL"].ToString() + filename;

                    db.tblM_Sender_File.Add(sf);
                    db.SaveChanges();

                    string imgFilename = bnf.SenderID + "_IMG_" + Common.getTimeSpan() + "." + fuSenderWithID.PostedFile.ContentType.Replace("image/", "");
                    string imgTargetPath = Server.MapPath("../Sender/Picture/Indonesia/Personal/" + imgFilename);
                    Stream imgStrm = fuSenderWithID.PostedFile.InputStream;
                    Common.SaveImage(0.5, imgStrm, imgTargetPath);

                    tblM_Sender_Personal_Detail spd = new tblM_Sender_Personal_Detail();
                    spd.SenderID = sf.SenderID.Value;
                    spd.Nationality = Convert.ToInt64(ddlSenderNationality.SelectedValue.Trim());
                    spd.Sex = Convert.ToInt64(ddlSenderSex.SelectedValue.Trim());
                    spd.Occupation = txtSenderOccupation.Text.Trim();
                    spd.IDType = Convert.ToInt64(ddlNationalIDType.SelectedValue.Trim());
                    spd.IDNo = txtNationalID.Text.Trim();
                    spd.IDURL = sf.URL;
                    spd.DOB = Convert.ToDateTime(senderDOB.Text.Trim());
                    spd.POB = txtPOB.Text.Trim();
                    if (!cbNationalIDValidityType.Checked)
                    {
                        spd.IDValidity = DateTime.Parse(nationalIDExpiry.Text.Trim());
                        spd.IDValidityType = "Tanggal";
                    }
                    else
                    {
                        spd.IDValidity = null;
                        spd.IDValidityType = "Seumur Hidup";
                    }
                    spd.PictureURL = ConfigurationManager.AppSettings["URL_SENDER_INDONESIA_PERSONAL"].ToString() + imgFilename;
                    spd.CreatedBy = Convert.ToInt64(SessionLib.Current.AdminID);
                    spd.CreatedDate = eDate;
                    spd.UpdatedBy = null;
                    spd.UpdatedDate = null;

                    db.tblM_Sender_Personal_Detail.Add(spd);
                    db.SaveChanges();

                    string eMsg = "New personal sender: [" + bnf.SenderID + "] " + bnf.Fullname + "created by user [" + spd.CreatedBy + "] " + SessionLib.Current.Name + ".";
                    tblS_Log sLog = new tblS_Log();
                    sLog.EventDate = eDate;
                    sLog.EventBy = spd.CreatedBy.Value;
                    sLog.EventType = "operation";
                    sLog.EventData = eMsg.Trim();

                    db.tblS_Log.Add(sLog);
                    db.SaveChanges();
                }
                else
                {

                    tblM_Sender_Company_Detail scd = new tblM_Sender_Company_Detail();
                    scd.SenderID = bnf.SenderID;
                    scd.CompanyType = Convert.ToInt64(ddlLegalitasType.SelectedValue);
                    scd.LegalitasNo = txtLegalitasNo.Text.Trim();

                    string compLegalitasFile = bnf.SenderID + "_LEG_" + Common.getTimeSpan() + "." + fuLegalitas.PostedFile.ContentType.Replace("image/", "");
                    string compLegalitasTargetPath = Server.MapPath("../Sender/Picture/Indonesia/Company/" + compLegalitasFile);
                    Stream compLegalitasStrm = fuLegalitas.PostedFile.InputStream;
                    Common.SaveImage(0.5, compLegalitasStrm, compLegalitasTargetPath);
                    //fuLegalitas.SaveAs(compLegalitasTargetPath);

                    scd.LegalitasURL = ConfigurationManager.AppSettings["URL_SENDER_INDONESIA_COMPANY"].ToString() + compLegalitasFile;

                    scd.NPWPNo = txtCompanyNPWPNo.Text.Trim();

                    string compNPWPFile = bnf.SenderID + "_NPWP_" + Common.getTimeSpan() + "." + fuCompanyNPWP.PostedFile.ContentType.Replace("image/", "");
                    string compNPWPTargetPath = Server.MapPath("../Sender/Picture/Indonesia/Company/" + compNPWPFile);
                    Stream compNPWPStrm = fuCompanyNPWP.PostedFile.InputStream;
                    Common.SaveImage(0.5, compNPWPStrm, compNPWPTargetPath);
                    //fuCompanyNPWP.SaveAs(compNPWPTargetPath);

                    scd.NPWPURL = ConfigurationManager.AppSettings["URL_SENDER_INDONESIA_COMPANY"].ToString() + compNPWPFile;

                    scd.BBHType = Convert.ToInt64(ddlBBH.SelectedValue);
                    scd.BusinessField = Convert.ToInt64(ddlBusinessField.SelectedValue);
                    scd.BusinessRef = txtBusinessReference.Text.Trim();
                    scd.IncorporationPlace = txtPlaceOfIncorporation.Text.Trim();
                    scd.IncorporationDate = DateTime.Parse(incorporationDate.Text);
                    scd.BankID = Convert.ToInt64(ddlBank.SelectedValue);
                    scd.BankAccNo = txtBankAccount.Text.Trim();

                    scd.PICName = txtPICName.Text.Trim();
                    scd.PICDepartment = txtJabatanPIC.Text.Trim();
                    scd.PICIDType = Convert.ToInt64(ddlPICIDType.SelectedValue);
                    scd.PICIDNo = txtPICIDNo.Text.Trim();
                    if(!cbPICIDValidityType.Checked)
                    {
                        scd.PICIDExpiry = DateTime.Parse(picIDExpiry.Text);
                        scd.PICIDValidityType = "Tanggal";
                    }
                    else
                    {
                        scd.PICIDExpiry = null;
                        scd.PICIDValidityType = "Seumur Hidup";
                    }

                    string compPICIDFile = bnf.SenderID + "_PICID_" + Common.getTimeSpan() + "." + fuPICID.PostedFile.ContentType.Replace("image/", "");
                    string compPICIDTargetPath = Server.MapPath("../Sender/Picture/Indonesia/Company/" + compPICIDFile);
                    Stream compPICIDStrm = fuPICID.PostedFile.InputStream;
                    Common.SaveImage(0.5, compPICIDStrm, compPICIDTargetPath);
                    //fuPICID.SaveAs(compPICIDTargetPath);

                    scd.PICIDURL = ConfigurationManager.AppSettings["URL_SENDER_INDONESIA_COMPANY"].ToString() + compPICIDFile;

                    scd.CreatedBy = Convert.ToInt64(SessionLib.Current.AdminID);
                    scd.CreatedDate = eDate;
                    scd.UpdatedBy = null;
                    scd.UpdatedBy = null;

                    db.tblM_Sender_Company_Detail.Add(scd);
                    db.SaveChanges();

                    string tmpCompID = Session["TMPCompanyID"].ToString().Trim();

                    var tmpDirectorList = db.tblTMP_Company_Director.Where(p => p.TMPCompanyID == tmpCompID).ToList();
                    foreach (var companydirector in tmpDirectorList)
                    {
                        tblM_Company_Director cd = new tblM_Company_Director();
                        cd.CompanyID = bnf.SenderID;
                        cd.DirectorID = companydirector.DirectorID;

                        db.tblM_Company_Director.Add(cd);
                        db.SaveChanges();

                        db.tblTMP_Company_Director.Remove(companydirector);

                    }

                    var tmpShareholderList = db.tblTMP_Company_Shareholder.Where(p => p.TMPCompanyID == tmpCompID).ToList();
                    foreach (var companyshareholder in tmpShareholderList)
                    {
                        tblM_Company_Shareholder cs = new tblM_Company_Shareholder();
                        cs.CompanyID = bnf.SenderID;
                        cs.ShareholderID = companyshareholder.ShareholderID;

                        db.tblM_Company_Shareholder.Add(cs);
                        db.SaveChanges();

                        db.tblTMP_Company_Shareholder.Remove(companyshareholder);
                        db.SaveChanges();
                    }

                    string eMsg = "New company sender: [" + bnf.SenderID + "] " + bnf.Fullname + "created by user [" + scd.CreatedBy + "] " + SessionLib.Current.Name + ".";
                    tblS_Log sLog = new tblS_Log();
                    sLog.EventDate = eDate;
                    sLog.EventBy = scd.CreatedBy.Value;
                    sLog.EventType = "operation";
                    sLog.EventData = eMsg.Trim();

                    db.tblS_Log.Add(sLog);
                    db.SaveChanges();
                }

                Session["SuccessSender"] = "Success add " + bnf.Fullname + " to database and need an admin approval before this sender could create transaction";

                Session.Remove("TMPCompanyID");
                
                Response.Redirect("~/Sender/SenderList");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + bnf.Fullname + " added to database')", true);
            }
            else
            {
                diverror.Visible = true;
                lblError.Text = "This sender with phone number " + txtPhone.Text + " already registered";
            }
        }

        protected void btnAddDirector_Click(object sender, EventArgs e)
        {
            if (Session["TMPCompanyID"] == null)
            {
                //new company definition
                Session["TMPCompanyID"] = Guid.NewGuid().ToString();
            }

            if (txtDirectorName.Text == "")
            {
                lblError.Text = "Company Director Name cannot be empty!";
                diverror.Visible = true;
                //txtDirectorName.BorderColor = System.Drawing.Color.Red;
                return;
            }

            if (ddlDirectorIDType.SelectedValue == "")
            {
                lblError.Text = "Company Director ID Type must be selected!";
                diverror.Visible = true;
                //ddlDirectorIDType.BorderColor = System.Drawing.Color.Red;
                return;
            }

            if (txtDirectorIdentityNo.Text == "")
            {
                lblError.Text = "Company Director Identity Number cannot be empty!";
                diverror.Visible = true;
                return;
            }

            if(!cbDirectorIDValidityType.Checked) //Type=Tanggal
            {
                if (companyDirectorIDExpiry.Text == "")
                {
                    lblError.Text = "Company Director ID Expiry cannot be empty!";
                    diverror.Visible = true;
                    return;
                }
            }

            if (fuCompanyDirectorIDFile.PostedFile.ContentType != "image/jpg" && fuCompanyDirectorIDFile.PostedFile.ContentType != "image/jpeg" && fuCompanyDirectorIDFile.PostedFile.ContentType != "image/png")
            {
                lblError.Text = "Company Director ID Attachment : Only jpeg, jpg and png format are allowed";
                diverror.Visible = true;
                return;
            }

            if (companyDirectorDOB.Text.Trim() == "")
            {
                lblError.Text = "Company Director's Date of Birth cannot be empty!";
                diverror.Visible = true;
                return;
            }

            if (ddlDirectorNationality.SelectedValue == "-1")
            {
                lblError.Text = "Please select Company Director's nationality!";
                diverror.Visible = true;
                return;
            }

            if (ddlDirectorReligion.SelectedValue == "-1")
            {
                lblError.Text = "Please select Company Director's religion!";
                diverror.Visible = true;
                return;
            }

            if (ddlDirectorMaritalStatus.SelectedValue == "-1")
            {
                lblError.Text = "Please select Company Director's marital status!";
                diverror.Visible = true;
                return;
            }

            if (ddlDirectorEducationLevel.SelectedValue == "-1")
            {
                lblError.Text = "Please select Company Director's education level!";
                diverror.Visible = true;
                return;
            }

            if (ddlDirectorAddressStatus.SelectedValue == "-1")
            {
                lblError.Text = "Please select Company Director's address status!";
                diverror.Visible = true;
                return;
            }

            if (txtDirectorAddress.Text == "")
            {
                lblError.Text = "Please key in Company Director's address!";
                diverror.Visible = true;
                return;
            }

            if (ddlDirectorCountryM49.SelectedValue == "-1")
            {
                lblError.Text = "Please select Company Director's address country!";
                diverror.Visible = true;
                return;
            }

            if (txtDirectorPostcode.Text == "")
            {
                lblError.Text = "Please key in Company Director's Postcode!";
                diverror.Visible = true;
                return;
            }

            if (txtDirectorPhone.Text == "")
            {
                lblError.Text = "Please key in Company Director's Phone Number!";
                diverror.Visible = true;
                return;
            }

            try
            {
                tblM_Director newDirector = new tblM_Director();

                newDirector.IDType = long.Parse(ddlDirectorIDType.SelectedValue.Trim());
                newDirector.IDNo = txtDirectorIdentityNo.Text;

                if(!cbDirectorIDValidityType.Checked)
                {
                    var directorIDExpiryDate = new DateTime();
                    directorIDExpiryDate = Convert.ToDateTime(companyDirectorIDExpiry.Text.ToString().Trim());
                    newDirector.IDExpiryDate = directorIDExpiryDate;
                    newDirector.IDValidityType = "Tanggal";
                }
                else
                {
                    newDirector.IDExpiryDate = null;
                    newDirector.IDValidityType = "Seumur Hidup";
                }

                newDirector.Name = txtDirectorName.Text;
                newDirector.NPWP = txtDirectorNPWPNo.Text;
                newDirector.Sex = long.Parse(ddlDirectorSex.SelectedValue.Trim());
                newDirector.BirthPlace = txtDirectorPOB.Text.Trim();
                if(companyDirectorDOB.Text != null && companyDirectorDOB.Text.Trim() != "")
                {
                    newDirector.DOB = Convert.ToDateTime(companyDirectorDOB.Text.ToString().Trim());
                }
                newDirector.Nationality = long.Parse(ddlDirectorNationality.SelectedValue.Trim());
                newDirector.Religion = long.Parse(ddlDirectorReligion.SelectedValue.Trim());
                newDirector.MaritalStatus = long.Parse(ddlDirectorMaritalStatus.SelectedValue.Trim());
                newDirector.EducationLevel = long.Parse(ddlDirectorEducationLevel.SelectedValue.Trim());
                newDirector.AddressStatus = long.Parse(ddlDirectorAddressStatus.SelectedValue.Trim());
                newDirector.Address = txtDirectorAddress.Text.Trim();
                newDirector.City = txtDirectorCity.Text.Trim();
                newDirector.Province = txtDirectorProvince.Text.Trim();
                newDirector.CountryM49 = long.Parse(ddlDirectorCountryM49.SelectedValue.Trim());
                newDirector.Postcode = txtDirectorPostcode.Text.Trim();
                newDirector.Phone = txtDirectorPhone.Text.Trim();
                newDirector.CreatedBy = Convert.ToInt64(SessionLib.Current.AdminID);
                newDirector.CreatedDate = DateTime.Now;

                db.tblM_Director.Add(newDirector);
                db.SaveChanges();

                string directorIDFilename = newDirector.ID + "_ID_" + Common.getTimeSpan() + "." + fuCompanyDirectorIDFile.PostedFile.ContentType.Replace("image/", "");
                string directorIDTargetPath = Server.MapPath("../Sender/Picture/Indonesia/Company/Director/" + directorIDFilename);
                Stream directorIDStream = fuCompanyDirectorIDFile.PostedFile.InputStream;
                Common.SaveImage(0.5, directorIDStream, directorIDTargetPath);
                newDirector.IDURL = ConfigurationManager.AppSettings["URL_SENDER_INDONESIA_COMPANY"].ToString() + "Director/" + directorIDFilename;

                db.SaveChanges();

                tblTMP_Company_Director tcd = new tblTMP_Company_Director();
                tcd.TMPCompanyID = Session["TMPCompanyID"].ToString().Trim();
                tcd.DirectorID = newDirector.ID;

                db.tblTMP_Company_Director.Add(tcd);
                db.SaveChanges();

                //clear director form
                clearDirectorForm();

            } catch (Exception ex)
            {

                lblError.Text = "Failed save Company Director with the following error message: " + ex.Message.ToString();
                diverror.Visible = true;
                return;
                //Session["ErrorSender"] = "Failed save Company Director with the following error message: " + ex.Message.ToString();
            }

            bindCDI();
        }

        protected void clearDirectorForm()
        {
            Session.Remove("DirectorIDFile");
            lblUploadedCompanyDirectorIDFile.Text = "Uploaded File = NONE";
        }

        protected void btnAddShareholder_Click(object sender, EventArgs e)
        {
            if (Session["TMPCompanyID"] == null)
            {
                //new company definition
                Session["TMPCompanyID"] = Guid.NewGuid().ToString();
            }

            if (txtShareholderName.Text == "")
            {
                lblError.Text = "Company Shareholder's name cannot be empty!";
                diverror.Visible = true;
                return;
            }

            if (ddlShareholderIDType.SelectedValue == "")
            {
                lblError.Text = "Company Shareholder ID Type must be selected!";
                diverror.Visible = true;
                //ddlDirectorIDType.BorderColor = System.Drawing.Color.Red;
                return;
            }

            if (txtShareholderIDNo.Text == "")
            {
                lblError.Text = "Company Shareholder's Identity Number cannot be empty!";
                diverror.Visible = true;
                return;
            }

            if(!cbShareholderIDValidityType.Checked) //Type=Tanggal
            {
                if (companyShareholderIDExpiry.Text == "")
                {
                    lblError.Text = "Company Shareholder's ID Expiry cannot be empty!";
                    diverror.Visible = true;
                    return;
                }
            }

            if (fuCompanyShareholderIDFile.PostedFile.ContentType != "image/jpg" && fuCompanyShareholderIDFile.PostedFile.ContentType != "image/jpeg" && fuCompanyShareholderIDFile.PostedFile.ContentType != "image/png")
            {
                lblError.Text = "Company Director ID Attachment : Only jpeg, jpg and png format are allowed";
                diverror.Visible = true;
                return;
            }

            if (lblShareholderDOB.Text.Trim() == "")
            {
                lblError.Text = "Company Shareholder's Date of Birth cannot be empty!";
                diverror.Visible = true;
                return;
            }

            if (ddlShareholderNationality.SelectedValue == "-1")
            {
                lblError.Text = "Please select Company Shareholders's nationality!";
                diverror.Visible = true;
                return;
            }

            if (ddlShareholderReligion.SelectedValue == "-1")
            {
                lblError.Text = "Please select Company Shareholder's religion!";
                diverror.Visible = true;
                return;
            }

            if (ddlShareholderMaritalStatus.SelectedValue == "-1")
            {
                lblError.Text = "Please select Company Shareholder's marital status!";
                diverror.Visible = true;
                return;
            }

            if (ddlShareholderEducationLevel.SelectedValue == "-1")
            {
                lblError.Text = "Please select Company Shareholder's education level!";
                diverror.Visible = true;
                return;
            }

            if (ddlShareholderAddressStatus.SelectedValue == "-1")
            {
                lblError.Text = "Please select Company Shareholder's address status!";
                diverror.Visible = true;
                return;
            }

            if (txtShareholderAddress.Text == "")
            {
                lblError.Text = "Company Shareholder's Address cannot be empty!";
                diverror.Visible = true;
                return;
            }

            if (ddlShareholderCountryM49.SelectedValue == "-1")
            {
                lblError.Text = "Please select Company Shareholder's address country!";
                diverror.Visible = true;
                return;
            }

            if (txtShareholderPostcode.Text == "")
            {
                lblError.Text = "Please key in Company Shareholder's Postcode!";
                diverror.Visible = true;
                return;
            }

            if (txtShareholderPhone.Text == "")
            {
                lblError.Text = "Please key in Company Shareholder's Phone Number!";
                diverror.Visible = true;
                return;
            }

            try
            {
                tblM_Shareholder newShareholder = new tblM_Shareholder();

                newShareholder.IDType = long.Parse(ddlShareholderIDType.SelectedValue.Trim());
                newShareholder.IDNo = txtDirectorIdentityNo.Text;

                if(!cbShareholderIDValidityType.Checked)
                {
                    var shareholderIDExpiryDate = new DateTime();
                    shareholderIDExpiryDate = Convert.ToDateTime(companyShareholderIDExpiry.Text.ToString().Trim());
                    newShareholder.IDExpiryDate = shareholderIDExpiryDate;
                    newShareholder.IDValidityType = "Tanggal";
                }
                else
                {
                    newShareholder.IDExpiryDate = null;
                    newShareholder.IDValidityType = "Seumur Hidup";
                }

                newShareholder.Name = txtShareholderName.Text;
                newShareholder.HoldingType = long.Parse(ddlHoldingType.SelectedValue);
                newShareholder.NPWP = txtShareholderNPWPNo.Text;
                newShareholder.Sex = long.Parse(ddlShareholderSex.SelectedValue.Trim());
                newShareholder.BirthPlace = txtShareholderPOB.Text;

                if(companyShareholderDOB.Text != null && companyShareholderDOB.Text.Trim() != "")
                {
                    newShareholder.DOB = Convert.ToDateTime(companyShareholderDOB.Text.ToString().Trim());
                }
                
                newShareholder.Nationality = long.Parse(ddlShareholderNationality.SelectedValue.Trim());
                newShareholder.Religion = long.Parse(ddlShareholderReligion.SelectedValue.Trim());
                newShareholder.MaritalStatus = long.Parse(ddlShareholderMaritalStatus.SelectedValue.Trim());
                newShareholder.EducationLevel = long.Parse(ddlShareholderEducationLevel.SelectedValue.Trim());
                newShareholder.AddressStatus = long.Parse(ddlShareholderAddressStatus.SelectedValue.Trim());
                newShareholder.Address = txtShareholderAddress.Text.Trim();
                newShareholder.City = txtShareholderCity.Text.Trim();
                newShareholder.Province = txtShareholderProvince.Text.Trim();
                newShareholder.CountryM49 = long.Parse(ddlShareholderCountryM49.SelectedValue.Trim());
                newShareholder.Postcode = txtShareholderPostcode.Text.Trim();
                newShareholder.Phone = txtShareholderPhone.Text.Trim();
                newShareholder.CreatedBy = Convert.ToInt64(SessionLib.Current.AdminID);
                newShareholder.CreatedDate = DateTime.Now;

                db.tblM_Shareholder.Add(newShareholder);
                db.SaveChanges();

                string shareholderIDFilename = newShareholder.ID + "_ID_" + Common.getTimeSpan() + "." + fuCompanyShareholderIDFile.PostedFile.ContentType.Replace("image/", "");
                string shareholderIDTargetPath = Server.MapPath("../Sender/Picture/Indonesia/Company/Shareholder/" + shareholderIDFilename);
                Stream shareholderIDStream = fuCompanyShareholderIDFile.PostedFile.InputStream;
                Common.SaveImage(0.5, shareholderIDStream, shareholderIDTargetPath);
                newShareholder.IDURL = ConfigurationManager.AppSettings["URL_SENDER_INDONESIA_COMPANY"].ToString() + "Shareholder/" + shareholderIDFilename;

                db.SaveChanges();

                tblTMP_Company_Shareholder tcs = new tblTMP_Company_Shareholder();
                tcs.TMPCompanyID = Session["TMPCompanyID"].ToString().Trim();
                tcs.ShareholderID = newShareholder.ID;

                db.tblTMP_Company_Shareholder.Add(tcs);
                db.SaveChanges();

                //clear director form
                clearShareholderForm();

            } catch (Exception ex)
            {
                lblError.Text = "Failed save Company Shareholder with the following error message: " + ex.Message.ToString();
                diverror.Visible = true;
                return;
            }

            bindSHI();
        }

        protected void clearShareholderForm()
        {
            Session.Remove("ShareholderIDFile");
            lblUploadedShareholderIDFile.Text = "Uploaded File = NONE";
        }

        protected void btnError_ServerClick(object sender, EventArgs e)
        {
            diverror.Visible = false;
        }
        
        protected void ddlSenderType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSenderType.SelectedValue == "1")
            {
                divpersonal.Visible = true;
                divcompany.Visible = false;
            }
            else if (ddlSenderType.SelectedValue == "2")
            {
                divpersonal.Visible = false;
                divcompany.Visible = true;
            }
        }

        protected void ddlLegalitasType_SelectedIndexChanged(object sender, EventArgs e)
        {
            //TODO
        }

        protected void ddlBBH_SelectedIndexChanged(object sender, EventArgs e)
        {
            //TODO
        }

        protected void ddlBusinessField_SelectedIndexChanged(object sender, EventArgs e)
        {
            //TODO
        }

        protected void ddlBank_SelectedIndexChanged(object sender, EventArgs e)
        {
            //TODO
        }

        protected void btnCheckCity_Click(object sender, EventArgs e)
        {
            hfModalCityOpen.Value = "yes";
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "Pop", "var isModalCity = true", true);
        }

        protected void btnChooseCity_Click(object sender, EventArgs e)
        {
            System.Web.UI.WebControls.Button btn = (System.Web.UI.WebControls.Button)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            hfCityID.Value = ((HiddenField)row.FindControl("hfCityIDModal")).Value;
            hfCityName.Value = ((HiddenField)row.FindControl("hfCityNameModal")).Value;
            txtCity.Text = hfCityName.Value;
            txtFindCity.Text = "";
            gvListCity.DataSource = null;
            gvListCity.DataBind();
            gvListCity.Visible = false;
            hfModalCityOpen.Value = "no";
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "Pop", "var isModalCity = false", true);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "DoPostBack", "__doPostBack(sender, e)", true);
        }

        protected void btnFindCity_Click(object sender, EventArgs e)
        {
            var data = db.tblM_City.Where(p => p.City.Contains(txtFindCity.Text.Trim())).ToList();
            gvListCity.DataSource = data;
            gvListCity.DataBind();
            gvListCity.Visible = true;
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "Pop", "var isModalCity = true", true);
        }

        protected void gvCDI_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvCDI.PageIndex = e.NewPageIndex;
            bindCDI();
        }

        protected void gvSHI_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvSHI.PageIndex = e.NewPageIndex;
            bindSHI();
        }

        protected void btnRemoveShareholder_Click(object sender, EventArgs e)
        {
            System.Web.UI.WebControls.Button btn = (System.Web.UI.WebControls.Button)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            string tmpCompIDStr = ((HiddenField)row.FindControl("hfSHICompanyID")).Value;
            string shareholderIDStr = ((HiddenField)row.FindControl("hfSHIShareholderID")).Value;
            long shareholderID = Convert.ToInt64(shareholderIDStr);
            tblTMP_Company_Shareholder tcs = new tblTMP_Company_Shareholder { TMPCompanyID = tmpCompIDStr, ShareholderID = Convert.ToInt64(shareholderIDStr) };

            var cs = db.tblTMP_Company_Shareholder.Where(p => p.TMPCompanyID == tmpCompIDStr && p.ShareholderID == shareholderID).First();

            if(cs != null)
            {
                db.tblTMP_Company_Shareholder.Remove(cs);
                db.SaveChanges();
            }

            var shareholder = db.tblM_Shareholder.Where(p => p.ID == shareholderID).First();

            if(shareholder != null)
            {
                db.tblM_Shareholder.Remove(shareholder);
                db.SaveChanges();
            }

            bindSHI();
        }

        protected void btnRemoveDirector_Click(object sender, EventArgs e)
        {
            System.Web.UI.WebControls.Button btn = (System.Web.UI.WebControls.Button)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            string tmpCompIDStr = ((HiddenField)row.FindControl("hfCDICompanyID")).Value;
            string directorIDStr = ((HiddenField)row.FindControl("hfCDIDirectorID")).Value;
            long directorID = Convert.ToInt64(directorIDStr);
            //tblTMP_Company_Director tcd = new tblTMP_Company_Director { TMPCompanyID = tmpCompIDStr, DirectorID = Convert.ToInt64(directorIDStr) };

            var cd = db.tblTMP_Company_Director.Where(p => p.TMPCompanyID == tmpCompIDStr && p.DirectorID == directorID).First();

            if(cd != null)
            {
                db.tblTMP_Company_Director.Remove(cd);
                db.SaveChanges();
            }

            var director = db.tblM_Director.Where(p => p.ID == directorID).First();

            if(director != null)
            {
                db.tblM_Director.Remove(director);
                db.SaveChanges();
            }

            bindCDI();
        }

        protected void ddlNationalIDType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(ddlNationalIDType.SelectedValue == "3" || ddlNationalIDType.SelectedValue == "4")
            {
                divNationalIDValidityType.Visible = true; //show validity date expiry field
                ddlSenderNationality.SelectedValue = "360"; //Indonesia
            }
            else
            {
                divNationalIDValidityType.Visible = false;
            }
        }

        protected void ddlPICIDType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(ddlPICIDType.SelectedValue == "1" || ddlPICIDType.SelectedValue == "3" || ddlPICIDType.SelectedValue == "4")
            {
                divPICIDValidityType.Visible = true;
            }
            else if(ddlPICIDType.SelectedValue == "5")
            {
                //NRIC
                divPICIDValidityType.Visible = false;
                cbPICIDValidityType.Checked = true;
                divPICIDValidityDate.Visible = false;
            }
            else
            {
                divPICIDValidityType.Visible = false;
            }
        }

        protected void ddlDirectorIDType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(ddlDirectorIDType.SelectedValue == "1" || ddlDirectorIDType.SelectedValue == "3" || ddlDirectorIDType.SelectedValue == "4")
            {
                divDirectoryIDValidityType.Visible = true;
                ddlDirectorNationality.SelectedValue = "360";
            }
            else if(ddlDirectorIDType.SelectedValue == "5")
            {
                divDirectoryIDValidityType.Visible = false;
                cbDirectorIDValidityType.Checked = true;
                divDirectorIDValidityDate.Visible = false;
            }
            else
            {
                divDirectoryIDValidityType.Visible = false;
            }
        }

        protected void ddlShareholderIDType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(ddlShareholderIDType.SelectedValue == "1" || ddlShareholderIDType.SelectedValue == "3" || ddlShareholderIDType.SelectedValue == "4")
            {
                divShareholderIDValidityType.Visible = true;
                ddlShareholderNationality.SelectedValue = "360";
            }
            else if(ddlShareholderIDType.SelectedValue == "5")
            {
                divShareholderIDValidityType.Visible = false;
                cbShareholderIDValidityType.Checked = true;
                divShareholderIDValidityDate.Visible = false;
            }
            else
            {
                divShareholderIDValidityType.Visible = false;
            }
        }

        protected void cbNationalIDValidityType_CheckedChanged(object sender, EventArgs e)
        {
            divNationalIDValidityDate.Visible = !cbNationalIDValidityType.Checked;
        }

        protected void cbPICIDValidityType_CheckedChanged(object sender, EventArgs e)
        {
            divPICIDValidityDate.Visible = !cbPICIDValidityType.Checked;
        }

        protected void cbDirectorIDValidityType_CheckedChanged(object sender, EventArgs e)
        {
            divDirectorIDValidityDate.Visible = !cbDirectorIDValidityType.Checked;
        }

        protected void cbShareholderIDValidityType_CheckedChanged(object sender, EventArgs e)
        {
            divShareholderIDValidityDate.Visible = !cbShareholderIDValidityType.Checked;
        }
    }
}