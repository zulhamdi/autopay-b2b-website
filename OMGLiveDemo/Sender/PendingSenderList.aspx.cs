﻿using OMGLiveDemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.Sender
{
    public partial class PendingSenderList : System.Web.UI.Page
    {
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (Common.allowedPage(this.GetType().BaseType.Name))
            //{
            //    if (!IsPostBack)
            //    {
                    BindGV();
            //    }
            //}
            //else
            //{
            //    Master.checkRole();
            //}
        }

        void BindGV()
        {
            if (Session["SuccessSender"] != null)
            {
                lblAlertSuccess.Text = Session["SuccessSender"].ToString();
                divsuccess.Visible = true;
                Session["SuccessSender"] = null;
            }

            if (Session["ErrorSender"] != null)
            {
                lblAlertFailed.Text = Session["ErrorSender"].ToString();
                divfailed.Visible = true;
                Session["ErrorSender"] = null;
            }

            var id = Int64.Parse(SessionLib.Current.AdminID);
            
            List<vw_Sender> dt;
            if (SessionLib.Current.Role == Roles.Agent)
            {
                dt = db.vw_Sender.Where(p => p.AgentID == id && p.isAgent == 1 && p.isActive == 1 && (p.Status == "created" || p.Status == "rejected")).ToList();
            }
            else
            {
                dt = db.vw_Sender.Where(p => p.AgentID == 0 && p.isAgent == 0 && p.isActive == 1 && (p.Status == "created" || p.Status == "rejected")).ToList();
            }

            gvListItem.DataSource = dt;
            gvListItem.DataBind();
        }

        protected void lbtViewFiles_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            Session["SenderID"] = ((HiddenField)row.FindControl("hfSenderID")).Value;
            
            Response.Redirect("~/Sender/UpdateSender");
        }

        protected void btnAddSender_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddMalaysianSender");
        }

        protected void btnAlertFailed_ServerClick(object sender, EventArgs e)
        {
            divfailed.Visible = false;
        }

        protected void btnAlertSuccess_ServerClick(object sender, EventArgs e)
        {
            divsuccess.Visible = false;
        }

        protected Boolean IsCreated(string status)
        {
            if (status == "created")
                return false;
            else
                return true;
        }
    }
}