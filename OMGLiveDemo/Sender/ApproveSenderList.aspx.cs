﻿using OMGLiveDemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.Sender
{
    public partial class ApproveSenderList : System.Web.UI.Page
    {
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    BindGV();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        void BindGV()
        {
            if (Session["SuccessApproveSender"] != null)
            {
                lblAlertSuccess.Text = Session["SuccessApproveSender"].ToString();
                divsuccess.Visible = true;
                Session["SuccessApproveSender"] = null;
            }

            if (Session["ErrorApproveSender"] != null)
            {
                lblAlertFailed.Text = Session["ErrorApproveSender"].ToString();
                divfailed.Visible = true;
                Session["ErrorApproveSender"] = null;
            }

            /*
            var dt = db.vw_Sender.Where(p => p.isActive == 1 && (p.Status == "created" || p.Status == "recreated") && p.Status != null).ToList();

            List<vw_Sender> dtnew = new List<vw_Sender>();
            for (int i = 0; i < dt.Count; i++)
            {
                if (dtnew.Where(p => p.SenderID == dt[i].SenderID).FirstOrDefault() == null)
                {
                    dtnew.Add(dt[i]);
                }
            }

            gvListItem.DataSource = dtnew;
            gvListItem.DataBind();
            */
            //var dt = db.tblM_Sender.Where(p => p.isActive == 1 && p.Status == "created").ToList();
            var dt = db.tblM_Sender.Where(p => p.isActive == 1 && p.Status == "created").ToList();
            foreach (var item in dt)
            {
                if (item.SenderTypeID.HasValue)
                    item.SenderType = db.tblM_Sender_Type.Where(d => d.ID.Equals(item.SenderTypeID.Value)).Select(d => d.Type).SingleOrDefault();
            }
            gvListItem.DataSource = dt;
            gvListItem.DataBind();
        }

        protected void lbtViewFiles_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            Session["SenderID"] = ((HiddenField)row.FindControl("hfSenderID")).Value;
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "colorBox", "var isColorbox = true", true);

            Response.Redirect("~/Sender/ApproveSender");
        }

        protected void btnAddSender_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddMalaysianSender");
        }

        protected void btnAlertFailed_ServerClick(object sender, EventArgs e)
        {
            divfailed.Visible = false;
        }

        protected void btnAlertSuccess_ServerClick(object sender, EventArgs e)
        {
            divsuccess.Visible = false;
        }
    }
}