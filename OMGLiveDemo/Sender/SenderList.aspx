﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SenderList.aspx.cs" Inherits="OMGLiveDemo.Sender.SenderList" MaintainScrollPositionOnPostback="true" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" />

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph">

                <div class="row x_title">
                    <div class="col-md-12">
                        <h3>Senders List</h3>
                    </div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div style="width: 100%; margin-bottom: 5px">
                        <div id="canvas_dahs" class="demo-placeholder" style="width: 100%; height: 100%;">

                            <div class="alert alert-success alert-dismissible fade in" runat="server" id="divsuccess" visible="false">
                                <button type="button" runat="server" id="btnAlertSuccess" onserverclick="btnAlertSuccess_ServerClick" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <center><asp:Label runat="server" ID="lblAlertSuccess" Font-Size="14pt" Text="test error" /></center>
                            </div>

                            <div class="alert alert-danger alert-dismissible fade in" runat="server" id="divfailed" visible="false">
                                <button type="button" runat="server" id="btnAlertFailed" onserverclick="btnAlertFailed_ServerClick" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <center><asp:Label runat="server" ID="lblAlertFailed" Font-Size="14pt" Text="test error" /></center>
                            </div>
                            <div class="row" style="margin-bottom: 13px">
                                <div class="col-md-2">
                                    <asp:Label runat="server" CssClass="control-label" Text="Choose Country" />
                                    <asp:DropDownList runat="server" ID="ddlChooseCountry" CssClass="form-control" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true">
                                        <asp:ListItem Text="Indonesia" Value="103"></asp:ListItem>
                                        <asp:ListItem Text="Malaysia" Value="133"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="col-md-2">
                                    <asp:Label runat="server" CssClass="control-label" Text="Status Registration" />
                                    <asp:DropDownList runat="server" ID="ddlFilter" CssClass="form-control" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true">
                                        <asp:ListItem Text="Approved" Value="approved"></asp:ListItem>
                                        <asp:ListItem Text="Pending" Value="created"></asp:ListItem>
                                        <asp:ListItem Text="Amend" Value="amend"></asp:ListItem>
                                        <asp:ListItem Text="Rejected" Value="rejected"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="col-md-2">
                                    <asp:Label runat="server" CssClass="control-label" Text="Type" />
                                    <asp:DropDownList runat="server" ID="ddlType" CssClass="form-control" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true">
                                        <asp:ListItem Text="Personal" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Company" Value="2"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>

                                <%-- <div class="col-md-2">
                                    <asp:Label runat="server" CssClass="control-label" Text="Start date" />
                                    <asp:TextBox ID="startdate" CssClass="form-control" runat="server" OnTextChanged="TextChanged"/>
                                </div>

                                 <div class="col-md-2">
                                    <asp:Label runat="server" CssClass="control-label" Text="End date" />
                                    <asp:TextBox ID="enddate" CssClass="form-control" runat="server" OnTextChanged="TextChanged"/>
                                </div>--%>

                                <div class="col-md-6">
                                    <asp:Label runat="server" CssClass="control-label" Text="Name" />
                                    <asp:TextBox runat="server" CssClass="form-control" ID="txtName" OnTextChanged="TextChanged" AutoPostBack="true"></asp:TextBox>
                                </div>
                            </div>

                            <asp:GridView CssClass="table table-striped jambo_table bulk_action" ID="gvListItem" runat="server" AutoGenerateColumns="false" DataKeyNames="SenderID" OnRowDataBound="GridView_RowDataBound">
                                <Columns>
                                    <asp:TemplateField HeaderText="No.">
                                        <ItemTemplate>
                                            <asp:HiddenField runat="server" ID="hfSenderID" Value='<%# Eval("SenderID") %>' />
                                            <asp:HiddenField runat="server" ID="hfFullname" Value='<%# Eval("Fullname") %>' />
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Fullname" HeaderText="Sender Name" />
                                    <asp:BoundField DataField="Country" HeaderText="Country" />
                                    <asp:BoundField DataField="SenderType" HeaderText="Type" />
                                    <asp:BoundField DataField="IDType" HeaderText="ID Type"/>
                                    <asp:BoundField DataField="IDNumber" HeaderText="ID Number" />
                                    <asp:BoundField DataField="LegalitasType" HeaderText="Legalitas Type" />
                                    <asp:BoundField DataField="LegalitasNumber" HeaderText="Legalitas Number" />
                                    <asp:BoundField DataField="Phone" HeaderText="Phone" />
                                    <asp:BoundField DataField="JoinDate" HeaderText="Registration Date" />
                                    <asp:BoundField DataField="ApprovedDate" HeaderText="Approved Date" />
                                    <asp:TemplateField HeaderText="Action">
                                        <ItemTemplate>
                                            <asp:Button runat="server" ID="btnViewSender" CssClass="btn btn-default" runat="server" Text="View" OnClick="btnViewSender_Click" CausesValidation="false" />
                                            <% if (OMGLiveDemo.Models.SessionLib.Current.Role == OMGLiveDemo.Models.Roles.Operator && (ddlFilter.SelectedValue == "created" || ddlFilter.SelectedValue == "amend")) { %>
                                                <asp:Button runat="server" ID="btnUpdateSender" CssClass="btn btn-info" runat="server" Text="Update" CausesValidation="false" />
                                            <% } %>
                                            <% if (OMGLiveDemo.Models.SessionLib.Current.Role == OMGLiveDemo.Models.Roles.Operator && (ddlFilter.SelectedValue == "amend")) { %>
                                                <asp:Button runat="server" ID="btnReSubmitSender" CssClass="btn btn-success" runat="server" Text="Re-Submit" CausesValidation="false" />
                                            <% } %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <asp:Button runat="server" ID="btnAddSender" OnClick="btnAddSender_Click" Text="Add Sender" CssClass="btn btn-success" Style="padding-top: 5px" />
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Scripts" runat="server">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" />
    <script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript">
        function ShowProgressBar() {
            document.getElementById('dvProcessing').style.visibility = 'visible';
            document.getElementById('dvBtnProcess').style.visibility = 'hidden';
        }

        function HideProgressBar() {
            document.getElementById('dvProgressBar').style.visibility = "hidden";
        }
    </script>

    <script type="text/javascript">
        $(window).load(function () {
            if (window.isColorbox) {
                $.colorbox({ href: "../Payout/ViewImage.aspx", iframe: true, width: "80%", height: "80%" });
            }
        });
        function OpenCBox() {
            $.colorbox({ href: "../Payout/ViewImage.aspx", iframe: true, width: "80%", height: "80%" });
        }

         $("input[id*='startdate']").datepicker({
                format: "yyyy/mm/dd"
            });
            $("input[id*='enddate']").datepicker({
                format: "yyyy/mm/dd"
            });
    </script>
</asp:Content>
