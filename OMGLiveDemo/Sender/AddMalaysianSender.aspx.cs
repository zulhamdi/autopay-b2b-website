﻿using OMGLiveDemo.Models;
using OMGLiveDemo.Models.OMG;
using OMGLiveDemo.Scripts;
using Oppal.Sec;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.Sender
{
    public partial class AddMalaysianSender : System.Web.UI.Page
    {
        OMG omg = new OMG();
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    bind();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        void bind()
        {
            var sender = db.tblM_Sender_Type.ToList();
            ddlSenderType.DataSource = null;
            ddlSenderType.DataSource = sender;
            ddlSenderType.DataValueField = "ID";
            ddlSenderType.DataTextField = "Type";
            ddlSenderType.DataBind();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            diverror.Visible = false;
            var id = Int64.Parse(SessionLib.Current.AdminID);
            if (txtName.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Name cannot be empty!";
                return;
            }

            if (txtAddress.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Address cannot be empty!";
                return;
            }

            if (txtPhone.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Phone cannot be empty!";
                return;
            }

            if (txtEmail.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Email cannot be empty!";
                return;
            }

            if (txtAddress.Text.Count() < 10)
            {
                diverror.Visible = true;
                lblError.Text = "Your address is too short! Please add your address more than 10 chars.";
                return;
            }

            if (ddlSenderType.SelectedValue == "1")
            {
                if (txtNationalID.Text == "")
                {
                    diverror.Visible = true;
                    lblError.Text = "National ID / Passport cannot be empty!";
                    return;
                }

                if (!fuNationalID.HasFile)
                {
                    lblError.Text = "Please choose photo of your National ID / Passport first";
                    diverror.Visible = true;
                    return;
                }
                else
                {
                    if (fuNationalID.PostedFile.ContentType != "image/jpg" && fuNationalID.PostedFile.ContentType != "image/jpeg" && fuNationalID.PostedFile.ContentType != "image/png")
                    {
                        lblError.Text = "Only jpeg, jpg and png format are allowed";
                        diverror.Visible = true;
                        return;
                    }
                }
            } 
            else
            {
                if (txtSSM.Text == "")
                {
                    diverror.Visible = true;
                    lblError.Text = "SSM Number cannot be empty!";
                    return;
                }

                if (!fuSSM.HasFile)
                {
                    lblError.Text = "Please choose photo of your SSM first";
                    diverror.Visible = true;
                    return;
                }
                else
                {
                    if (fuSSM.PostedFile.ContentType != "image/jpg" && fuSSM.PostedFile.ContentType != "image/jpeg" && fuSSM.PostedFile.ContentType != "image/png")
                    {
                        lblError.Text = "Only jpeg, jpg and png format are allowed";
                        diverror.Visible = true;
                        return;
                    }
                }
            }

            tblM_Sender check;

            if (SessionLib.Current.Role == Roles.Agent)
            {
                check = db.tblM_Sender.Where(p => p.AgentID == id && p.Phone == txtPhone.Text.Trim() && p.isAgent == 1).FirstOrDefault();
            }
            else if (SessionLib.Current.Role == Roles.MasterArea)
            {
                check = db.tblM_Sender.Where(p => p.AgentID == id && p.Phone == txtPhone.Text.Trim() && p.isAgent == 2).FirstOrDefault();
            }
            else
            {
                check = db.tblM_Sender.Where(p => p.Phone == txtPhone.Text.Trim() && p.isAgent == 0).FirstOrDefault();
            }
            if (check == null)
            {

                tblM_Sender bnf = new tblM_Sender();
                bnf.Fullname = txtName.Text.Trim();
                bnf.Address = txtAddress.Text.Trim();
                bnf.Phone = txtPhone.Text.Trim();
                bnf.Email = txtEmail.Text.Trim();
                bnf.CityID = 0;
                bnf.CountryID = 133;
                bnf.Country = "Malaysia";
                bnf.JoinDate = DateTime.Now;
                bnf.AddedBy = Convert.ToInt64(SessionLib.Current.AdminID);
                bnf.Email = txtEmail.Text.Trim();
                bnf.SenderTypeID = Convert.ToInt64(ddlSenderType.SelectedValue);
                if (SessionLib.Current.Role == Roles.Agent)
                {
                    bnf.AgentID = Int64.Parse(SessionLib.Current.AdminID);
                    bnf.isAgent = 1;
                }
                else if (SessionLib.Current.Role == Roles.MasterArea)
                {
                    bnf.AgentID = Int64.Parse(SessionLib.Current.AdminID);
                    bnf.isAgent = 2;
                }
                else
                {
                    bnf.AgentID = 0;
                    bnf.isAgent = 0;
                }
                bnf.AddedBy = bnf.AgentID;
                bnf.isActive = 1;
                bnf.Status = "created";
                db.tblM_Sender.Add(bnf);
                db.SaveChanges();

                if (bnf.SenderTypeID == 1)
                {
                    var ftype = db.tblM_Sender_File_Type.Where(p => p.SenderTypeID == bnf.SenderTypeID && p.CountryID == 133).FirstOrDefault();
                    tblM_Sender_File sf = new tblM_Sender_File();
                    sf.SenderID = bnf.SenderID;
                    sf.FileTypeID = ftype.ID;
                    sf.Value = txtNationalID.Text.ToString().Trim();

                    string filename = bnf.SenderID + "_NationalID_" + Common.getTimeSpan() + "." + fuNationalID.PostedFile.ContentType.Replace("image/", "");
                    string targetPath = Server.MapPath("../Sender/Picture/Malaysia/Personal/" + filename);
                    Stream strm = fuNationalID.PostedFile.InputStream;
                    Common.SaveImage(0.5, strm, targetPath);

                    sf.URL = ConfigurationManager.AppSettings["URL_SENDER_MALAYSIA_PERSONAL"].ToString() + filename;

                    db.tblM_Sender_File.Add(sf);
                }
                else
                {
                    var ftype = db.tblM_Sender_File_Type.Where(p => p.SenderTypeID == bnf.SenderTypeID && p.CountryID == 133).FirstOrDefault();
                    tblM_Sender_File sf = new tblM_Sender_File();
                    sf.SenderID = bnf.SenderID;
                    sf.FileTypeID = ftype.ID;
                    sf.Value = txtSSM.Text.ToString().Trim();

                    string filename = bnf.SenderID + "_SSM_" + Common.getTimeSpan() + "." + fuSSM.PostedFile.ContentType.Replace("image/", "");
                    string targetPath = Server.MapPath("../Sender/Picture/Malaysia/Company/" + filename);
                    Stream strm = fuSSM.PostedFile.InputStream;
                    Common.SaveImage(0.5, strm, targetPath);

                    sf.URL = ConfigurationManager.AppSettings["URL_SENDER_MALAYSIA_COMPANY"].ToString() + filename;

                    db.tblM_Sender_File.Add(sf);
                }

                db.SaveChanges();

                var admin = db.tblM_Admin.Where(p => (p.RoleID == 1 || p.RoleID == 2 || p.RoleID == 3) && p.Email != null && p.Email != "" && p.isActive == 1).ToList();
                omg.sendEmailSenderApproval(admin, SessionLib.Current.Name);

                Session["SuccessSender"] = "Success add " + bnf.Fullname + " to database and need an admin approval before this sender could create transaction";
                Response.Redirect("~/Sender/SenderList");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + bnf.Fullname + " added to database')", true);
            }
            else
            {
                diverror.Visible = true;
                lblError.Text = "This sender with phone number " + txtPhone.Text + " already registered";
            }
        }

        protected void btnError_ServerClick(object sender, EventArgs e)
        {
            diverror.Visible = false;
        }
        
        protected void ddlSenderType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSenderType.SelectedValue == "1")
            {
                divpersonal.Visible = true;
                divcompany.Visible = false;
            }
            else if (ddlSenderType.SelectedValue == "2")
            {
                divpersonal.Visible = false;
                divcompany.Visible = true;
            }
        }
    }
}