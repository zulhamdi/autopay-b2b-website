﻿using OMGLiveDemo.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.Sender
{
    public partial class DirectorDetail : System.Web.UI.Page
    {
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();

        protected void Page_Load(object sender, EventArgs e)
        {
            //tmp
            //Session["ViewInfoDirectorID"] = "20";
            //if (!IsPostBack)
            //{
            //    Do();
            //}

            
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    Do();
                }
            }
            else
            {
                Master.checkRole();
            }
            
        }

        void Do()
        {
            long directorID = 0;
            if(Session["ViewInfoDirectorID"] != null)
            {
                if (Session["ViewInfoDirectorID"].ToString().Trim() == "")
                {

                }
                else
                {
                    directorID = Convert.ToInt64(Session["ViewInfoDirectorID"].ToString());
                }
            }

            if(directorID > 0)
            {
                vw_Director_Detail director = db.vw_Director_Detail.Where(p => p.ID == directorID).First();
                if(director != null)
                {
                    populateShareholderDetail(director);
                }
                else
                {
                    showBlank();
                }
                
            }
            else
            {
                showBlank();
            }
            
        }

        void showBlank()
        {
            txtName.Text = "Shareholder Not Found!";
        }

        void populateShareholderDetail(vw_Director_Detail director)
        {
            txtName.Text = director.Name;
            txtNationality.Text = director.DirectorNationality;
            txtPhone.Text = director.Phone;
            txtAddress.Text = director.Address;
            txtCity.Text = director.City;
            txtProvince.Text = director.Province;
            txtPostcode.Text = director.Postcode;
            txtCountry.Text = director.DirectorCountry;
            txtIDType.Text = director.IDTypeName;
            txtIDNo.Text = director.IDNo;
            txtIDCountry.Text = director.IDCountry;
            txtAddressStatus.Text = director.DirectorAddressStatus;

            if(director.IDValidityType == "Seumur Hidup")
            {
                txtIDValidity.Text = "Seumur Hidup";
            }
            else
            {
                if(director.IDExpiryDate != null)
                {
                    txtIDValidity.Text = director.IDExpiryDate.Value.ToString("dd/MM/yyyy");
                }
                else
                {
                    txtIDValidity.Text = "Unable to parse ID Validity Date";
                }
            }

            txtNPWP.Text = director.NPWP;
            txtGender.Text = director.Gender;

            if(director.DOB != null)
            {
                txtDOB.Text = director.DOB.Value.ToString("dd/MM/yyyy");
            }
            else
            {
                txtDOB.Text = "Unable to parse ID Validity Date";
            }

            txtPOB.Text = director.BirthPlace;
            txtReligion.Text = director.DirectorReligion;
            txtMaritalStatus.Text = director.DirectorMaritalStatus;
            txtEducation.Text = director.DirectorEducationLevel;

        }

        protected void lkbIDFile_Click(object sender, EventArgs e)
        {
            if (hfIDFileURL.Value != null)
            {
                Session["ImageURL"] = hfIDFileURL.Value;
                Session["From"] = "Image";
                string txt = "ID No : " + txtIDNo.Text;
                Session["Text"] = txtName.Text + " - " + txt;
                string queryString = "../Payout/ViewImage.aspx";
                string newWin = "window.open('" + queryString + "','Personal ID','width=900, height=400, titlebar=yes');";
                ClientScript.RegisterStartupScript(this.GetType(), "pop", newWin, true);
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alertMessage", "alert('File not found!')", true);
            }
        }
    }
}