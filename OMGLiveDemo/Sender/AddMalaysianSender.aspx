﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddMalaysianSender.aspx.cs" Inherits="OMGLiveDemo.Sender.AddMalaysianSender" MaintainScrollPositionOnPostback="true" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" />

    <div class="row">
        <div class="row x_title">
            <div class="col-md-12">
                <h3>Add Malaysian Sender</h3>
            </div>
        </div>

        <div class="col-md-3 col-sm-12 col-xs-12"></div>
        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="dashboard_graph">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div style="width: 100%;">
                        <div id="canvas_dahs" class="demo-placeholder" style="width: 100%; height: 100%;">
                            <div class="form-group" style="height: 30px"></div>

                            <div class="alert alert-danger alert-dismissible fade in" runat="server" id="diverror" visible="false">
                                <button type="button" runat="server" id="btnError" onserverclick="btnError_ServerClick" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <center><asp:Label runat="server" ID="lblError" /></center>
                            </div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblName" CssClass="control-label" Text="Full Name *" />
                                <asp:TextBox runat="server" ID="txtName" CssClass="form-control" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblAddress" CssClass="control-label" Text="Address *" />
                                <asp:TextBox runat="server" ID="txtAddress" CssClass="form-control" TextMode="MultiLine" Rows="3"/>
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblPhone" CssClass="control-label" Text="Phone *" />
                                <asp:TextBox runat="server" ID="txtPhone" CssClass="form-control" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblEmail" CssClass="control-label" Text="Email *" />
                                <asp:TextBox runat="server" ID="txtEmail" CssClass="form-control" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblSenderType" CssClass="control-label" Text="Sender Type *" />
                                <asp:DropDownList runat="server" ID="ddlSenderType" CssClass="form-control" OnSelectedIndexChanged="ddlSenderType_SelectedIndexChanged" AutoPostBack="true" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div id="divpersonal" runat="server" class="form-group">
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblNationalID" CssClass="control-label" Text="National ID / Passport *" />
                                    <asp:TextBox runat="server" ID="txtNationalID" CssClass="form-control" />
                                    <asp:FileUpload ID="fuNationalID" runat="server" CssClass="form-control" /> 
                                </div>
                                <div class="form-group" style="height: 5px"></div>
                            </div>

                            <div id="divcompany" runat="server" class="form-group" visible="false">
                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblSSM" CssClass="control-label" Text="SSM No. *" />
                                    <asp:TextBox runat="server" ID="txtSSM" CssClass="form-control" />
                                    <asp:FileUpload ID="fuSSM" runat="server" CssClass="form-control" /> 
                                </div>
                                <div class="form-group" style="height: 5px"></div>
                            </div>

                            <div class="form-group">
                                <center><asp:Button runat="server" ID="btnAdd" Text="Add Sender" CssClass="btn btn-success btn-lg" OnClick="btnAdd_Click" Width="100%" /></center>
                                <%--<asp:Button runat="server" ID="btnThumbnail" Text="Buat Thumbnail" CssClass="btn btn-success" OnClick="btnThumbnail_Click" />--%>
                            </div>

                            <div class="form-group" style="height: 30px"></div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12"></div>
    </div>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Scripts" runat="server">
</asp:Content>
