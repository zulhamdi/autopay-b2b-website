﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ApproveSender.aspx.cs" Inherits="OMGLiveDemo.Sender.ApproveSender" MaintainScrollPositionOnPostback="true" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" />

    <div class="row">
        <div class="row x_title">
            <div class="col-md-12">
                <h3>Approve Sender</h3>
            </div>
        </div>

        <div class="col-md-3 col-sm-12 col-xs-12"></div>
        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="dashboard_graph">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div style="width: 100%;">
                        <div id="canvas_dahs" class="demo-placeholder" style="width: 100%; height: 100%;">
                            <div class="form-group" style="height: 30px"></div>
                            
                            <asp:HiddenField runat="server" ID="hfSenderImageURL" />
                            <asp:HiddenField runat="server" ID="hfSenderPicURL" />
                            <asp:HiddenField runat="server" ID="hfTDPImageURL" />
                            <asp:HiddenField runat="server" ID="hfSKImageURL" />
                            <asp:HiddenField runat="server" ID="hfNPWPImageURL" />
                            <asp:HiddenField runat="server" ID="hfSIUPImageURL" />
                            <asp:HiddenField runat="server" ID="hfSenderID" />
                            <asp:HiddenField runat="server" ID="hfCountryID" />
                            <asp:HiddenField runat="server" ID="hfCompanyPICIDURL" />
                            <asp:HiddenField runat="server" ID="hfCompanyNPWPURL" />
                            <asp:HiddenField runat="server" ID="hfCompanyLegalitasFileURL" />

                            <div class="alert alert-danger alert-dismissible fade in" runat="server" id="diverror" visible="false">
                                <button type="button" runat="server" id="btnError" onserverclick="btnError_ServerClick" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <center><asp:Label runat="server" ID="lblError" /></center>
                            </div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblName" CssClass="control-label" Text="Full Name *" />
                                <asp:TextBox runat="server" ID="txtName" CssClass="form-control" Enabled="false" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="Label21" CssClass="control-label" Text="City *" />
                                <asp:TextBox runat="server" ID="txtCity" CssClass="form-control" Enabled="false" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblAddress" CssClass="control-label" Text="Address *" />
                                <asp:TextBox runat="server" ID="txtAddress" CssClass="form-control" TextMode="MultiLine" Rows="3" Enabled="false" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblPhone" CssClass="control-label" Text="Phone *" />
                                <asp:TextBox runat="server" ID="txtPhone" CssClass="form-control" Enabled="false" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblEmail" CssClass="control-label" Text="Email " />
                                <asp:TextBox runat="server" ID="txtEmail" CssClass="form-control" Enabled="false" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblSenderType" CssClass="control-label" Text="Sender Type *" />
                                <asp:TextBox runat="server" ID="txtSenderType" CssClass="form-control" Enabled="false" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <!-- ######################### PERSONAL ######################### -->
                            <!-- ######################### PERSONAL ######################### -->
                            <div id="divpersonal" runat="server" class="form-group">

                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblNationalID" CssClass="control-label" Text="National ID / Passport *" />
                                    <asp:TextBox runat="server" ID="txtNationalID" CssClass="form-control" Enabled="false"/>
                                    <u>
                                        <asp:LinkButton ID="lkbNationalID" runat="server" Text="View Image" ForeColor="Green" OnClick="lkbViewImage_Click" />
                                    </u>
                                </div>
                                <div class="form-group" style="height: 5px"></div>

                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblSenderPicture" CssClass="control-label" Text="Sender Picture with ID *" />
                                    <u>
                                        <asp:LinkButton ID="lkbSenderPic" runat="server" Text="View Image" ForeColor="Green" OnClick="lkbSenderPic_Click" />
                                    </u>
                                </div>
                                <div class="form-group" style="height: 5px"></div>

                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblNationalIDExpiry" CssClass="control-label" Text="National ID / Passport Expiry Date *" />
                                    <asp:TextBox runat="server" ID="txtNationalIDExpiry" CssClass="form-control" Enabled="false"/>
                                </div>
                                <div class="form-group" style="height: 5px"></div>

                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblNationality" CssClass="control-label" Text="Nationality *" />
                                    <asp:TextBox runat="server" ID="txtNationality" CssClass="form-control" Enabled="false"/>
                                </div>
                                <div class="form-group" style="height: 5px"></div>

                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblSex" CssClass="control-label" Text="Sex *" />
                                    <asp:TextBox runat="server" ID="txtSex" CssClass="form-control" Enabled="false"/>
                                </div>
                                <div class="form-group" style="height: 5px"></div>

                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblPOB" CssClass="control-label" Text="Place of Birth *" />
                                    <asp:TextBox runat="server" ID="txtPOB" CssClass="form-control" Enabled="false"/>
                                </div>
                                <div class="form-group" style="height: 5px"></div>

                                <div class="form-group">
                                    <asp:Label runat="server" ID="lblOccupation" CssClass="control-label" Text="Occupation *" />
                                    <asp:TextBox runat="server" ID="txtOccupation" CssClass="form-control" Enabled="false"/>
                                </div>
                                <div class="form-group" style="height: 5px"></div>

                            </div>

                            <!-- ######################### COMPANY ######################### -->
                            <!-- ######################### COMPANY ######################### -->
                            <div id="divcompany" runat="server" class="form-group" visible="false">
                                <div class="form-group" runat="server" id="divSSM" visible="false">
                                    <asp:Label runat="server" ID="lblSSM" CssClass="control-label" Text="SSM No. *" />
                                    <asp:TextBox runat="server" ID="txtSSM" CssClass="form-control" Enabled="false"/>
                                    <u>
                                        <asp:LinkButton ID="lkbSSM" runat="server" Text="View SSM Image" ForeColor="Green" OnClick="lkbViewImage_Click" /></u>
                                    <div class="form-group" style="height: 5px"></div>
                                </div>

                                <div class="form-group" runat="server" id="divLegalitas" visible="false">
                                    <asp:Label runat="server" ID="Label7" CssClass="control-label" Text="Jenis Legalitas :" />
                                    <asp:TextBox runat="server" ID="txtLegalitas" CssClass="form-control" Enabled="false"/>
                                    <u>
                                        <asp:LinkButton ID="lkbCompanyLegalitasFile" runat="server" Text="View Legalitas Document" ForeColor="Green" OnClick="lkbCompanyLegalitasFile_Click" /></u>
                                    <div class="form-group" style="height: 5px"></div>
                                </div>

                                <div class="form-group" runat="server" id="divCompanyNPWP" visible="false">
                                    <asp:Label runat="server" ID="Label8" CssClass="control-label" Text="NPWP No :" />
                                    <asp:TextBox runat="server" ID="txtCompanyNPWP" CssClass="form-control" Enabled="false"/>
                                    <u>
                                        <asp:LinkButton ID="lkbCompanyNPWPFile" runat="server" Text="View NPWP Document" ForeColor="Green" OnClick="lkbCompanyNPWPFile_Click" /></u>
                                    <div class="form-group" style="height: 5px"></div>
                                </div>

                                <div class="form-group">
                                    <asp:Label runat="server" ID="Label9" CssClass="control-label" Text="Bentuk Badan Hukum : " />
                                    <asp:TextBox runat="server" ID="txtCompanyBBH" CssClass="form-control" Enabled="false" />
                                </div>

                                <div class="form-group">
                                    <asp:Label runat="server" ID="Label10" CssClass="control-label" Text="Business Type : " />
                                    <asp:TextBox runat="server" ID="txtCompanyBusinessType" CssClass="form-control" Enabled="false" />
                                </div>

                                <div class="form-group">
                                    <asp:Label runat="server" ID="Label11" CssClass="control-label" Text="Business Reference : " />
                                    <asp:TextBox runat="server" ID="txtBusinessRef" CssClass="form-control" Enabled="false" />
                                </div>

                                <div class="form-group">
                                    <asp:Label runat="server" ID="Label12" CssClass="control-label" Text="Place of Incorporation : " />
                                    <asp:TextBox runat="server" ID="txtCompanyPOI" CssClass="form-control" Enabled="false" />
                                </div>

                                <div class="form-group">
                                    <asp:Label runat="server" ID="Label13" CssClass="control-label" Text="Date of Incorporation : " />
                                    <asp:TextBox runat="server" ID="txtCompanyDOI" CssClass="form-control" Enabled="false" />
                                </div>

                                <div class="form-group">
                                    <asp:Label runat="server" ID="Label14" CssClass="control-label" Text="Bank Name : " />
                                    <asp:TextBox runat="server" ID="txtCompanyBank" CssClass="form-control" Enabled="false" />
                                </div>

                                <div class="form-group">
                                    <asp:Label runat="server" ID="Label15" CssClass="control-label" Text="Bank Account No : " />
                                    <asp:TextBox runat="server" ID="txtCompanyBankAccNo" CssClass="form-control" Enabled="false" />
                                </div>

                                <div class="form-group" runat="server" id="divCompanyPIC" visible="false">
                                    <asp:Label runat="server" ID="Label16" CssClass="control-label" Text="Person-in-Charge Name : " />
                                    <asp:TextBox runat="server" ID="txtCompanyPICName" CssClass="form-control" Enabled="false" />

                                    <asp:Label runat="server" ID="Label18" CssClass="control-label" Text="Person-in-Charge Department : " />
                                    <asp:TextBox runat="server" ID="txtCompanyPICDepartment" CssClass="form-control" Enabled="false" />
                                    <div class="form-group" style="height: 2px"></div>

                                    <asp:Label runat="server" ID="Label17" CssClass="control-label" Text="Person-in-Charge ID No : " />
                                    <asp:TextBox runat="server" ID="txtCompanyPICIDNo" CssClass="form-control" Enabled="false" />
                                    <asp:Label runat="server" ID="Label19" CssClass="control-label" Text="Person-in-Charge ID Expiry : " />
                                    <asp:TextBox runat="server" ID="txtCompanyPICIDExpiry" CssClass="form-control" Enabled="false" />
                                    <u>
                                        <asp:LinkButton ID="lkbCompanyPICIDFile" runat="server" Text="View PIC ID" ForeColor="Green" OnClick="lkbCompanyPICIDFile_Click" /></u>
                                    <div class="form-group" style="height: 5px"></div>
                                </div>

                                <div class="form-group" runat="server" id="divCompanyDirector" visible="false">
                                    <asp:GridView CssClass="table table-striped jambo_table bulk_action" ID="gvCDI" runat="server" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" DataKeyNames="DirectorID" AllowPaging="True" PageSize="20" AllowCustomPaging="False" OnPageIndexChanging="gvCDI_PageIndexChanging">
                                        <PagerStyle HorizontalAlign="Center" CssClass="bs4-aspnet-pager" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="No.">
                                                <ItemTemplate>
                                                    <asp:HiddenField runat="server" ID="hfCDICompanyID" Value='<%# Eval("CompanyID") %>' />
                                                    <asp:HiddenField runat="server" ID="hfCDIDirectorID" Value='<%# Eval("DirectorID") %>' />
                                                    <%# Container.DataItemIndex + 1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Name" HeaderText="Director Name" />
                                            <asp:BoundField DataField="IDtype" HeaderText="Identity Type" />
                                            <asp:BoundField DataField="IDNo" HeaderText="Identity No." />
                                            <asp:BoundField DataField="Phone" HeaderText="Phone No." />
                                            <asp:TemplateField HeaderText="Action">
                                                <ItemTemplate>
                                                    <asp:Button ID="btnViewDirector" CssClass="form-control" runat="server" Text="View" OnClick="btnViewDirector_Click" CausesValidation="false" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>

                                <div class="form-group" runat="server" id="divCompanyShareholder" visible="false">
                                    <asp:GridView CssClass="table table-striped jambo_table bulk_action" ID="gvSHI" runat="server" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" DataKeyNames="ShareholderID" AllowPaging="True" PageSize="20" AllowCustomPaging="False" OnPageIndexChanging="gvSHI_PageIndexChanging">
                                        <PagerStyle HorizontalAlign="Center" CssClass="bs4-aspnet-pager" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="No.">
                                                <ItemTemplate>
                                                    <asp:HiddenField runat="server" ID="hfSHICompanyID" Value='<%# Eval("CompanyID") %>' />
                                                    <asp:HiddenField runat="server" ID="hfSHIShareholderID" Value='<%# Eval("ShareholderID") %>' />
                                                    <%# Container.DataItemIndex + 1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Name" HeaderText="Shareholder Name" />
                                            <asp:BoundField DataField="IDtype" HeaderText="Identity Type" />
                                            <asp:BoundField DataField="IDNo" HeaderText="Identity No." />
                                            <asp:BoundField DataField="Phone" HeaderText="Phone No." />
                                            <asp:TemplateField HeaderText="Action">
                                                <ItemTemplate>
                                                    <asp:Button ID="btnViewShareholder" CssClass="form-control" runat="server" Text="View" OnClick="btnViewShareholder_Click" CausesValidation="false" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>

                                <!--
                                <div class="form-group" runat="server" id="divTDP" visible="false">
                                    <asp:Label runat="server" ID="Label2" CssClass="control-label" Text="TDP No. *" />
                                    <asp:TextBox runat="server" ID="txtTDP" CssClass="form-control" Enabled="false"/>
                                    <u>
                                        <asp:LinkButton ID="lkbTDP" runat="server" Text="View TDP Image" ForeColor="Green" OnClick="lkbViewImageTDP_Click" /></u>
                                    <div class="form-group" style="height: 5px"></div>
                                </div>

                                <div class="form-group" runat="server" id="divskmenkumham" visible="false">
                                    <asp:Label runat="server" ID="Label3" CssClass="control-label" Text="SK Menkumham No. *" />
                                    <asp:TextBox runat="server" ID="txtSKMenkumham" CssClass="form-control" Enabled="false"/>
                                    <u>
                                        <asp:LinkButton ID="lkbSKMenkumham" runat="server" Text="View SK Menkumham Image" ForeColor="Green" OnClick="lkbViewImageSK_Click" /></u>
                                    <div class="form-group" style="height: 5px"></div>
                                </div>

                                <div class="form-group" runat="server" id="divnpwp" visible="false">
                                    <asp:Label runat="server" ID="Label5" CssClass="control-label" Text="SK Menkumham No. *" />
                                    <asp:TextBox runat="server" ID="txtNPWP" CssClass="form-control" Enabled="false"/>
                                    <u>
                                        <asp:LinkButton ID="lkbNPWP" runat="server" Text="View NPWP Image" ForeColor="Green" OnClick="lkbViewImageNPWP_Click" /></u>
                                    <div class="form-group" style="height: 5px"></div>
                                </div>

                                <div class="form-group" runat="server" id="divsiup" visible="false">
                                    <asp:Label runat="server" ID="Label6" CssClass="control-label" Text="SK Menkumham No. *" />
                                    <asp:TextBox runat="server" ID="txtSIUP" CssClass="form-control" Enabled="false"/>
                                    <u>
                                        <asp:LinkButton ID="lkbSIUP" runat="server" Text="View SIUP Image" ForeColor="Green" OnClick="lkbViewImageSIUP_Click" /></u>
                                    <div class="form-group" style="height: 5px"></div>
                                </div>
                                -->

                            </div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="Label4" CssClass="control-label" Text="Reject reason* : " />
                                <asp:TextBox runat="server" ID="txtRejectReason" CssClass="form-control" Enabled="true" />
                            </div>

                            <div class="form-group" id="dvBtnProcess">
                                <asp:Button runat="server" ID="btnApprove" Text="Approve" CssClass="btn btn-success btn-lg" OnClick="btnApprove_Click" OnClientClick="javascript:ShowProgressBar()" />
                                <asp:Button runat="server" ID="btnAmend" Text="Amend" CssClass="btn btn-info btn-lg" OnClick="btnAmend_Click" OnClientClick="javascript:ShowProgressBar()" />
                                <asp:Button runat="server" ID="btnReject" Text="Reject" CssClass="btn btn-danger btn-lg" OnClick="btnReject_Click" OnClientClick="javascript:ShowProgressBar()" />
                            </div>

                            <div class="form-group" id="dvProcessing" style="display: none;">
                                <center><asp:Label runat="server" ID="Label1" Text="Processing" Font-Size="X-Large" ForeColor="Green" /></center>
                            </div>

                            <div class="form-group" style="height: 30px"></div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12"></div>
    </div>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Scripts" runat="server">
    <script type="text/javascript">
        function ShowProgressBar() {
            document.getElementById('dvProcessing').style.display = 'normal';
            document.getElementById('dvBtnProcess').style.display = 'none';
        }

        function HideProgressBar() {
            document.getElementById('dvProgressBar').style.display = "none";
        }
    </script>

    <script type="text/javascript">
        $(window).load(function () {
            if (window.isColorbox) {
                $.colorbox({ href: "../Payout/ViewImage.aspx", iframe: true, width: "80%", height: "80%" });
            }
        });
        function OpenCBox() {
            $.colorbox({ href: "../Payout/ViewImage.aspx", iframe: true, width: "80%", height: "80%" });
        }

    </script>

</asp:Content>
