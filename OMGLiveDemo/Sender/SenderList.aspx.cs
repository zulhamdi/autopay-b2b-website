﻿using OMGLiveDemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.Sender
{
    public partial class SenderList : System.Web.UI.Page
    {
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    BindGV();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        void BindGV()
        {
            if (Session["SuccessSender"] != null)
            {
                lblAlertSuccess.Text = Session["SuccessSender"].ToString();
                divsuccess.Visible = true;
                Session["SuccessSender"] = null;
            }

            if (Session["ErrorSender"] != null)
            {
                lblAlertFailed.Text = Session["ErrorSender"].ToString();
                divfailed.Visible = true;
                Session["ErrorSender"] = null;
            }
            if (Session["Status"] != null)
            {
                ddlFilter.SelectedValue = Session["Status"].ToString();
            }

            var id = Int64.Parse(SessionLib.Current.AdminID);
            var cid = Int64.Parse(ddlChooseCountry.SelectedValue);
            var status = ddlFilter.SelectedValue;
            var type = Int64.Parse(ddlType.SelectedValue);
            var search = txtName.Text;

            List<tblM_Sender> dt;
            if (SessionLib.Current.Role == Roles.Agent)
            {
                dt = db.tblM_Sender.Where(p => p.AgentID == id && p.isAgent == 1 && p.isActive == 1 && p.Status == status && p.CountryID == cid && p.SenderTypeID == type && (!string.IsNullOrEmpty(search) ? p.Fullname.Contains(search) : true)).ToList();
            }
            else if (SessionLib.Current.Role == Roles.MasterArea)
            {
                dt = db.tblM_Sender.Where(p => p.AgentID == id && p.isAgent == 2 && p.isActive == 1 && p.Status == status && p.CountryID == cid && p.SenderTypeID == type && (!string.IsNullOrEmpty(search) ? p.Fullname.Contains(search) : true)).ToList();
            }
            else if (SessionLib.Current.Role == Roles.Operator)
            {
                dt = db.tblM_Sender.Where(p => p.isActive == 1 && p.Status == status && p.CountryID == cid && p.SenderTypeID == type && (!string.IsNullOrEmpty(search) ? p.Fullname.Contains(search) : true)).ToList();
            }
            else
            {
                dt = db.tblM_Sender.Where(p => p.isAgent == 0 && p.isActive == 1 && p.Status == status && p.CountryID == cid && p.SenderTypeID == type && (!string.IsNullOrEmpty(search) ? p.Fullname.Contains(search) : true)).ToList();
            }

            if (dt.Count != 0)
            {
                for (int i = 0; i < dt.Count; i++)
                {
                    var senderId = dt[i].SenderID;
                    if (dt[i].SenderTypeID == 1)
                    {
                        dt[i].SenderType = "Personal";
                        var personal = db.tblM_Sender_Personal_Detail.Where(d => d.SenderID == senderId).FirstOrDefault();
                        if (personal != null)
                        {
                            if (personal.IDType.HasValue)
                            {
                                var personType = db.tblM_Person_ID_Type.Where(d => d.ID == personal.IDType.Value).FirstOrDefault();
                                if (personType != null)
                                {
                                    dt[i].IDType = personType.Name;
                                    dt[i].IDNumber = personal.IDNo;
                                }
                            }
                        }
                    }
                    else
                    {
                        dt[i].SenderType = "Company";
                        var company = db.tblM_Sender_Company_Detail.Where(d => d.SenderID == senderId).FirstOrDefault();
                        if (company != null)
                        {
                            if (company.CompanyType.HasValue)
                            {
                                var companyType = db.tblM_Person_ID_Type.Where(d => d.ID == company.CompanyType.Value).FirstOrDefault();
                                if (companyType != null)
                                {
                                    dt[i].LegalitasType = companyType.Name;
                                    dt[i].LegalitasNumber = company.LegalitasNo;
                                }
                            }
                        }
                    }
                }
            }

            gvListItem.DataSource = dt;
            gvListItem.DataBind();
        }

        protected void lbtViewFiles_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            Session["ImageURL"] = ((HiddenField)row.FindControl("hfImageURL")).Value;
            Session["Text"] = ((HiddenField)row.FindControl("hfFullname")).Value + " - " + ((HiddenField)row.FindControl("hfFileValue")).Value;
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "colorBox", "var isColorbox = true", true);
        }

        protected void btnAddSender_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddIndonesianSender");
        }

        protected void btnAlertFailed_ServerClick(object sender, EventArgs e)
        {
            divfailed.Visible = false;
        }

        protected void btnAlertSuccess_ServerClick(object sender, EventArgs e)
        {
            divsuccess.Visible = false;
        }

        protected void SelectedIndexChanged(object sender, EventArgs e)
        {
            txtName.Text = string.Empty;
            BindGV();
        }
        protected void TextChanged(object sender, EventArgs e)
        {
            BindGV();
        }
        protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (Int64.Parse(ddlType.SelectedValue) == 1)
            {
                e.Row.Cells[4].Visible = true;
                e.Row.Cells[5].Visible = true;
                e.Row.Cells[6].Visible = false;
                e.Row.Cells[7].Visible = false;
            }
            else
            {
                e.Row.Cells[4].Visible = false;
                e.Row.Cells[5].Visible = false;
                e.Row.Cells[6].Visible = true;
                e.Row.Cells[7].Visible = true;
            }
            if (ddlFilter.SelectedValue == "approved")
                e.Row.Cells[10].Visible = true;
            else
                e.Row.Cells[10].Visible = false;
        }

        protected void btnViewSender_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            Session["ViewInfoSenderID"] = ((HiddenField)row.FindControl("hfSenderID")).Value;

            string queryString = "SenderDetail.aspx";
            string newWin = "window.open('" + queryString + "','Sender Detail','width=900, height=800, titlebar=yes');";
            ClientScript.RegisterStartupScript(this.GetType(), "pop", newWin, true);
        }
    }
}