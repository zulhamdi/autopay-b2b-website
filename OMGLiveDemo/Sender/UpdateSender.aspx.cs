﻿using OMGLiveDemo.Models;
using OMGLiveDemo.Models.OMG;
using OMGLiveDemo.Scripts;
using Oppal.Sec;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.Sender
{
    public partial class UpdateSender : System.Web.UI.Page
    {
        OMG omg = new OMG();
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    bind();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        void bind()
        {
            var sendertype = db.tblM_Sender_Type.ToList();
            ddlSenderType.DataSource = null;
            ddlSenderType.DataSource = sendertype;
            ddlSenderType.DataValueField = "ID";
            ddlSenderType.DataTextField = "Type";
            ddlSenderType.DataBind();
 
            hfSenderID.Value = Session["SenderID"].ToString();
            var id = Convert.ToInt64(hfSenderID.Value);
            var sender = db.vw_Sender.Where(p => p.SenderID == id).FirstOrDefault();
            txtName.Text = sender.Fullname;
            txtAddress.Text = sender.Address;
            txtPhone.Text = sender.Phone;
            txtEmail.Text = sender.Email;
            ddlSenderType.Text = sender.SenderType;
            if (sender.SenderType == "Personal")
            {
                divpersonal.Visible = true;
                txtNationalID.Text = sender.FileValue;
                divcompany.Visible = false;
            }
            else
            {
                divcompany.Visible = true;
                txtSSM.Text = sender.FileValue;
                divpersonal.Visible = false;
            }
            hfSenderImageURL.Value = sender.FileURL;
        }

        protected void lkbViewImage_Click(object sender, EventArgs e)
        {
            if (hfSenderImageURL.Value != null)
            {
                Session["ImageURL"] = hfSenderImageURL.Value;
                Session["From"] = "Image";
                string txt = "";
                if (ddlSenderType.SelectedValue == "1")
                {
                    txt = "NationalID/Passport : " + txtNationalID.Text;
                }
                else
                {
                    txt = "SSM : " + txtSSM.Text;
                }
                Session["Text"] = txtName.Text + " - " + txt;
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "colorBox", "var isColorbox = true", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('File not found!')", true);
            }
        }

        protected void btnError_ServerClick(object sender, EventArgs e)
        {
            diverror.Visible = false;
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {

            diverror.Visible = false;

            var id = Int64.Parse(SessionLib.Current.AdminID);
            if (txtName.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Name cannot be empty!";
                return;
            }

            if (txtPhone.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Phone cannot be empty!";
                return;
            }

            if (txtAddress.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Address cannot be empty!";
                return;
            }

            if (txtAddress.Text.Count() < 10)
            {
                diverror.Visible = true;
                lblError.Text = "Your address is too short! Please add your address more than 10 chars.";
                return;
            }

            if (ddlSenderType.SelectedValue == "1")
            {
                if (txtNationalID.Text == "")
                {
                    diverror.Visible = true;
                    lblError.Text = "National ID / Passport cannot be empty!";
                    return;
                }

                if (!fuNationalID.HasFile)
                {
                    lblError.Text = "Please choose photo of your National ID / Passport first";
                    diverror.Visible = true;
                    return;
                }
                else
                {
                    if (fuNationalID.PostedFile.ContentType != "image/jpg" && fuNationalID.PostedFile.ContentType != "image/jpeg" && fuNationalID.PostedFile.ContentType != "image/png")
                    {
                        lblError.Text = "Only jpeg, jpg and png format are allowed";
                        diverror.Visible = true;
                        return;
                    }
                }
            }
            else
            {
                if (txtSSM.Text == "")
                {
                    diverror.Visible = true;
                    lblError.Text = "SSM Number cannot be empty!";
                    return;
                }

                if (!fuSSM.HasFile)
                {
                    lblError.Text = "Please choose photo of your SSM first";
                    diverror.Visible = true;
                    return;
                }
                else
                {
                    if (fuSSM.PostedFile.ContentType != "image/jpg" && fuSSM.PostedFile.ContentType != "image/jpeg" && fuSSM.PostedFile.ContentType != "image/png")
                    {
                        lblError.Text = "Only jpeg, jpg and png format are allowed";
                        diverror.Visible = true;
                        return;
                    }
                }
            }

            var aaa = db.tblM_Sender.Where(p => p.SenderID == id).FirstOrDefault();
            aaa.Status = "recreated";
            aaa.ApprovedBy = Convert.ToInt64(SessionLib.Current.AdminID);
            aaa.ApprovedDate = DateTime.Now;
            db.SaveChanges();
            
            Session["SuccessApproveSender"] = "Success update and resubmit sender : " + txtName.Text;
            Response.Redirect("~/Sender/PendingSenderList");
        }
    }
}