﻿using OMGLiveDemo.Models;
using OMGLiveDemo.Scripts;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo
{
    public partial class ApproverDashboard : Page
    {
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();
        OMG omg = new OMG();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    BindGV();
                    count();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        void BindGV()
        {
            var agid = Convert.ToInt64(SessionLib.Current.AdminID);
            /*var dt = db.vw_Payout.Where(p => p.isAgent == 2 && p.CreatedBy == agid).OrderByDescending(p => p.PayoutID).Take(20).ToList();
            for (int i = 0; i < dt.Count; i++)
            {
                dt[i].AmountText = "RM " + String.Format(new CultureInfo("ms-MY"), "{0:n}", dt[i].Amount.Value).Replace("RM", "RM ");
                dt[i].AdminFee = "RM " + dt[i].AdminFee;
                dt[i].TotalAmountText = "RM " + String.Format(new CultureInfo("ms-MY"), "{0:n}", dt[i].TotalAmount).Replace("RM", "RM ");
                dt[i].RateText = "Rp " + String.Format(new CultureInfo("id-ID"), "{0:n}", float.Parse(dt[i].Rate)).Replace("Rp", "Rp ");
                dt[i].RateGivenText = "Rp " + String.Format(new CultureInfo("id-ID"), "{0:n}", float.Parse(dt[i].RateGiven)).Replace("Rp", "Rp ");
                dt[i].TotalTransferText = "Rp " + String.Format(new CultureInfo("id-ID"), "{0:n}", dt[i].TotalTransfer).Replace("Rp", "Rp ");
            }
            gvListItem.DataSource = dt;
            gvListItem.DataBind();*/
        }

        public void count()
        {
            var agid = Convert.ToInt64(SessionLib.Current.AdminID);

            var curmyr = omg.getRateMYR2IDR();

            if (curmyr != null)
            {
                //var marginmyr = db.vw_Agent_Rate.Where(p => p.AgentID == agid && p.RemittanceID == 1).FirstOrDefault();

                /*
                 * temporary use super admin rate to view rate... need to clarify the relationship between admin vs agent... from the db schema rate is always part of agent domain
                 */
                var marginmyr = db.vw_Agent_Rate.Where(p => p.AgentID == 1 && p.RemittanceID == 1).FirstOrDefault();
                lblRateI2M.Text = "1 MYR = " + String.Format(new CultureInfo("id-ID"), "{0:c}", (Convert.ToInt64(curmyr.Rate) - Convert.ToInt64(marginmyr.Margin))).Replace("Rp", "") + " IDR";
                lblRateUpdateDate.Text = "Last update : " + marginmyr.UpdatedDate.Value.ToLongTimeString() + ", " + marginmyr.UpdatedDate.Value.ToLongDateString();
            }

            lbtnSendSuccReg.Text = Master.getMasterCount().SenderAllApproved().ToString();
            lbtnBeneSuccReg.Text = Master.getMasterCount().BeneficiaryAllApproved().ToString();

            lbtnSendPendReg.Text = Master.getMasterCount().SenderAllPending().ToString();
            lbtnBenePendReg.Text = Master.getMasterCount().BeneficiaryAllPending().ToString();

            lbtnSendAmendReg.Text = Master.getMasterCount().SenderAllAmend().ToString();
            lbtnBeneAmendReg.Text = Master.getMasterCount().BeneficiaryAllAmend().ToString();

            lbtnSendFailReg.Text = Master.getMasterCount().SenderAllRejected().ToString();
            lbtnBeneFailReg.Text = Master.getMasterCount().BeneficiaryAllRejected().ToString();


            /*lkbSuccessfulRegistration.Text = "Rp " + String.Format(new CultureInfo("id-ID"), "{0:n}", db.tblM_Master_Area_Balance.Where(p => p.MasterAreaID == agid).Select(p => p.Balance).FirstOrDefault().Value).Replace("Rp", "Rp ");

            var curmyr = omg.getRateMYRIDR();
            if (curmyr != null)
            {
                var marginmyr = db.vw_Agent_Rate.Where(p => p.AgentID == agid && p.RemittanceID == 1).FirstOrDefault();
                lblRate.Text = "1 MYR = " + String.Format(new CultureInfo("id-ID"), "{0:c}", (Convert.ToInt64(curmyr.Rate) - Convert.ToInt64(marginmyr.Margin))).Replace("Rp", "") + " IDR";
                lblRateUpdate.Text = "Last update : " + marginmyr.UpdatedDate.Value.ToLongTimeString() + ", " + marginmyr.UpdatedDate.Value.ToLongDateString();
            }

            var tra = db.tblT_Payout.Where(p => p.isAgent == 2 && p.CreatedBy == agid).Count();

            if (db.tblT_Payout.Where(p => (p.Status == "created" || p.Status == "queued") && p.isAgent == 2 && p.CreatedBy == agid).Count() != 0)
            {
                lkbPendingRegistration.Text = "Rp " + String.Format(new CultureInfo("id-ID"), "{0:n}", db.tblT_Payout.Where(p => (p.Status == "created" || p.Status == "queued") && p.isAgent == 2 && p.CreatedBy == agid).Select(p => p.TotalTransfer).Sum()).Replace("Rp", "Rp ");
                lblTraPendingPercent.Text = ((db.tblT_Payout.Where(p => (p.Status == "created" || p.Status == "queued") && p.isAgent == 2 && p.CreatedBy == agid).Count() * 100) / tra).ToString();
            }
            else
            {
                lkbPendingRegistration.Text = "Rp 0";
            }

            if (db.tblT_Payout.Where(p => p.Status == "completed" && p.isAgent == 2 && p.CreatedBy == agid).Count() != 0)
            {
                lkbFailedRegistration.Text = "Rp " + String.Format(new CultureInfo("id-ID"), "{0:n}", db.tblT_Payout.Where(p => p.Status == "completed" && p.isAgent == 2 && p.CreatedBy == agid).Select(p => p.TotalTransfer).Sum()).Replace("Rp", "Rp ");
                lblTraSuccessPrecent.Text = ((db.tblT_Payout.Where(p => p.Status == "completed" && p.isAgent == 2 && p.CreatedBy == agid).Count() * 100) / tra).ToString();
            }
            else
            {
                lkbFailedRegistration.Text = "Rp 0";
            }

            if (db.tblT_Payout.Where(p => p.Status == "rejected" && p.isAgent == 2 && p.CreatedBy == agid).Count() != 0)
            {
                lbkTraRejected.Text = "Rp " + String.Format(new CultureInfo("id-ID"), "{0:n}", db.tblT_Payout.Where(p => p.Status == "rejected" && p.isAgent == 2 && p.CreatedBy == agid).Select(p => p.TotalTransfer).Sum()).Replace("Rp", "Rp ");
                lblTraRejectPercent.Text = ((db.tblT_Payout.Where(p => p.Status == "rejected" && p.isAgent == 2 && p.CreatedBy == agid).Count() * 100) / tra).ToString();
            }
            else
            {
                lbkTraRejected.Text = "Rp 0";
            }

            if (db.tblT_Payout.Where(p => p.Status == "processed" && p.isAgent == 2 && p.CreatedBy == agid).Count() != 0)
            {
                lbkTraProcess.Text = "Rp " + String.Format(new CultureInfo("id-ID"), "{0:n}", db.tblT_Payout.Where(p => p.Status == "processed" && p.isAgent == 2 && p.CreatedBy == agid).Select(p => p.TotalTransfer).Sum()).Replace("Rp", "Rp ");
                lblTraProcessPrecent.Text = ((db.tblT_Payout.Where(p => p.Status == "processed" && p.isAgent == 2 && p.CreatedBy == agid).Count() * 100) / tra).ToString();
            }
            else
            {
                lbkTraProcess.Text = "Rp 0";
            }

            if (db.tblT_Payout.Where(p => p.Status == "failed" && p.isAgent == 2 && p.CreatedBy == agid).Count() != 0)
            {
                lbkTraFailed.Text = "Rp " + String.Format(new CultureInfo("id-ID"), "{0:n}", db.tblT_Payout.Where(p => p.Status == "failed" && p.isAgent == 2 && p.CreatedBy == agid).Select(p => p.TotalTransfer).Sum()).Replace("Rp", "Rp ");
                lblTraFailedPercent.Text = ((db.tblT_Payout.Where(p => p.Status == "failed" && p.isAgent == 2 && p.CreatedBy == agid).Count() * 100) / tra).ToString();
            }
            else
            {
                lblTraFailedPercent.Text = "Rp 0";
            }*/
        }

        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Payout/PayoutHistory");
        }

        protected void btnUpdateMargin_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/RateAgent");
        }

        protected void btnReceipt_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            Session["PayoutID"] = ((HiddenField)row.FindControl("hfPayoutID")).Value;
            //Response.Redirect("EditCurrencyRate");
            Page.ClientScript.RegisterStartupScript(this.GetType(), "OpenWindow", "window.open('/Payout/Receipt','_newtab');", true);
        }

        protected void btnCalc_Click(object sender, EventArgs e)
        {
            Response.Redirect("Calculator");
        }

        protected Boolean IsCompleted(string status)
        {
            if (status == "completed")
                return true;
            else
                return false;
        }

        void RedirectToTransferHistory(string status)
        {
            Session["PayoutHistoryStatus"] = status;
            Response.Redirect("Payout/PayoutHistory");
        }

        protected void btnDivBalance_Click(object sender, EventArgs e)
        {
            Response.Redirect("Agent/BalanceHistory");
        }

        protected void btnDivPendingTransaction_Click(object sender, EventArgs e)
        {
            RedirectToTransferHistory("pending");
        }

        protected void btnDivSuccessTransaction_Click(object sender, EventArgs e)
        {
            RedirectToTransferHistory("completed");
        }

        protected void btnDivProcessTransaction_Click(object sender, EventArgs e)
        {
            RedirectToTransferHistory("processed");
        }

        protected void btnDivRejectedTransaction_Click(object sender, EventArgs e)
        {
            RedirectToTransferHistory("rejected");
        }

        protected void btnDivFailedTransaction_Click(object sender, EventArgs e)
        {
            RedirectToTransferHistory("failed");
        }

        protected void btnViewSenderClick(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            Session["Status"] = btn.CommandArgument.ToString();
            Response.Redirect("Sender/SenderList");
        }
        protected void btnViewBeneficiaryClick(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            Session["Status"] = btn.CommandArgument.ToString();
            Response.Redirect("Beneficiaries/BeneficiariesList");
        }
    }
}