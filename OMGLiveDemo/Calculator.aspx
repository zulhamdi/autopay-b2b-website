﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Calculator.aspx.cs" Inherits="OMGLiveDemo.Calculator" MaintainScrollPositionOnPostback="true" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" />
    <div class="row">
        <div class="row x_title">
            <div class="col-md-12">
                <h3>Calculator</h3>
            </div>
        </div>
        
        <div class="col-md-3 col-sm-12 col-xs-12">
        </div>

        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="dashboard_graph">

                <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:20px; margin-bottom:20px">
                    <asp:Label runat="server" CssClass="control-label" Text="Remittance Line" />
                    <asp:DropDownList runat="server" ID="ddlRemittanceLine" CssClass="form-control" OnSelectedIndexChanged="ddlRemittanceLine_SelectedIndexChanged" AutoPostBack="true"/>
                </div>

                <div id="divmyr" runat="server" visible="false">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div style="width: 100%;">
                            <div id="canvas_dahs" class="demo-placeholder" style="width: 100%; height: 100%;">
                                <div class="form-group" style="height: 30px"></div>

                                <asp:HiddenField runat="server" ID="hfRateMYR" />

                                <div class="alert alert-danger alert-dismissible fade in" runat="server" id="diverrorMYR" visible="false">
                                    <button type="button" runat="server" id="btnErrorMYR" onserverclick="btnErrorMYR_ServerClick" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    <center><asp:Label runat="server" ID="lblErrorMYR" /></center>
                                </div>

                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="control-label" Text="Country : " />
                                    <br />
                                    <asp:Label runat="server" ID="lblCountryMYR" Font-Bold="true" Font-Size="X-Large" />
                                </div>
                                <div class="form-group" style="height: 5px"></div>

                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="control-label" Text="Currency : " />
                                    <br />
                                    <asp:Label runat="server" ID="lblCurrencyMYR" Font-Bold="true" Font-Size="X-Large" />
                                </div>
                                <div class="form-group" style="height: 5px"></div>

                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="control-label" Text="Rate : " />
                                    <br />
                                    <asp:Label runat="server" ID="lblBaseRateMYR" Font-Bold="true" Font-Size="X-Large" />
                                </div>
                                <div class="form-group" style="height: 5px"></div>

                                <div class="form-group">
                                    <asp:Label runat="server" ID="Label1" CssClass="control-label" Text="Rupiah :" />
                                    <br />
                                    <asp:TextBox runat="server" ID="txtIndonesiaIDR" Font-Size="20pt" CssClass="form-control" OnTextChanged="txtIndonesiaIDR_TextChanged" AutoPostBack="true" TextMode="MultiLine" Rows="1" />
                                </div>

                                <div class="form-group">
                                    <asp:Label runat="server" ID="Label2" CssClass="control-label" Text="Ringgit :" />
                                    <br />
                                    <asp:TextBox runat="server" ID="txtIndonesiaMYR" Font-Size="20pt" CssClass="form-control" OnTextChanged="txtIndonesiaMYR_TextChanged" AutoPostBack="true" TextMode="MultiLine" Rows="1" />
                                </div>

                                <div class="form-group" style="height: 30px"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="dividr" runat="server" visible="false">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div style="width: 100%;">
                            <div id="canvas_dahs1" class="demo-placeholder" style="width: 100%; height: 100%;">
                                <div class="form-group" style="height: 30px"></div>

                                <asp:HiddenField runat="server" ID="hfRateIDR" />

                                <div class="alert alert-danger alert-dismissible fade in" runat="server" id="diverrorIDR" visible="false">
                                    <button type="button" runat="server" id="btnErrorIDR" onserverclick="btnErrorIDR_ServerClick" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    <center><asp:Label runat="server" ID="lblErrorIDR" /></center>
                                </div>

                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="control-label" Text="Country : " />
                                    <br />
                                    <asp:Label runat="server" ID="lblCountryIDR" Font-Bold="true" Font-Size="X-Large" />
                                </div>
                                <div class="form-group" style="height: 5px"></div>

                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="control-label" Text="Currency : " />
                                    <br />
                                    <asp:Label runat="server" ID="lblCurrencyIDR" Font-Bold="true" Font-Size="X-Large" />
                                </div>
                                <div class="form-group" style="height: 5px"></div>

                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="control-label" Text="Base Rate : " />
                                    <br />
                                    <asp:Label runat="server" ID="lblBaseRateIDR" Font-Bold="true" Font-Size="X-Large" />
                                </div>
                                <div class="form-group" style="height: 5px"></div>

                                <div class="form-group">
                                    <asp:Label runat="server" ID="Label3" CssClass="control-label" Text="Rupiah :" />
                                    <br />
                                    <asp:TextBox runat="server" ID="txtMalaysiaIDR" Font-Size="20pt" CssClass="form-control" OnTextChanged="txtMalaysiaIDR_TextChanged" AutoPostBack="true" TextMode="MultiLine" Rows="1" />
                                </div>

                                <div class="form-group">
                                    <asp:Label runat="server" ID="Label4" CssClass="control-label" Text="Ringgit :" />
                                    <br />
                                    <asp:TextBox runat="server" ID="txtMalaysiaMYR" Font-Size="20pt" CssClass="form-control" OnTextChanged="txtMalaysiaMYR_TextChanged" AutoPostBack="true" TextMode="MultiLine" Rows="1" />
                                </div>

                                <div class="form-group" style="height: 30px"></div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="col-md-3 col-sm-12 col-xs-12">
        </div>
    </div>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Scripts" runat="server">
</asp:Content>
