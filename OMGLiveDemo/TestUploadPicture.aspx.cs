﻿using OMGLiveDemo.Models;
using OMGLiveDemo.Scripts;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo
{
    public partial class TestUploadPicture : System.Web.UI.Page
    {
        OMG omg = new OMG();
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();
        tblM_Rate rate = new tblM_Rate();
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void btnError_ServerClick(object sender, EventArgs e)
        {
            diverror.Visible = false;
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            if (!fuPhoto.HasFile)
            {
                lblError.Text = "Please choose photo first";
                diverror.Visible = true;
                return;
            }
            else
            {
                if (fuPhoto.PostedFile.ContentType != "image/jpg" || fuPhoto.PostedFile.ContentType != "image/jpeg" || fuPhoto.PostedFile.ContentType != "image/png")
                {
                    lblError.Text = "Only jpeg, jpg and png format are allowed";
                    diverror.Visible = true;
                    return;
                }
            }
            //string filename = Path.GetFileName(fuPhoto.PostedFile.FileName);
            string filename = "KYC" + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + DateTime.Now.Hour + DateTime.Now.Minute + DateTime.Now.Second + DateTime.Now.Millisecond + ".jpg";
            string targetPath = Server.MapPath("Sender/Picture/" + filename);
            Stream strm = fuPhoto.PostedFile.InputStream;
            var targetFile = targetPath;

            Common.SaveImage(0.5, strm, targetFile);
            lblError.Text = "Success upload photo with name " + filename;
            diverror.Visible = true;

        }
    }
}