﻿using Newtonsoft.Json;
using OMGLiveDemo.Models;
using OMGLiveDemo.Models.OMG;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace OMGLiveDemo.Scripts
{
    public class OMG
    {
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();
        private string credentials() {
            //var asas = Convert.ToBase64String(Encoding.ASCII.GetBytes(ConfigurationManager.AppSettings["OMG_USERNAME"].ToString() + ":" + ConfigurationManager.AppSettings["OMG_PASSWORD"].ToString()));
            return Convert.ToBase64String(Encoding.ASCII.GetBytes(ConfigurationManager.AppSettings["OMG_USERNAME"].ToString() + ":" + ConfigurationManager.AppSettings["OMG_PASSWORD"].ToString()));
        }

        public static string EncryptAPP(string data)
        {
            RijndaelManaged rijndaelCipher = new RijndaelManaged();
            rijndaelCipher.Mode = CipherMode.CBC; //remember this parameter
            rijndaelCipher.Padding = PaddingMode.PKCS7; //remember this parameter

            rijndaelCipher.KeySize = 0x80;
            rijndaelCipher.BlockSize = 0x80;
            byte[] pwdBytes = Encoding.UTF8.GetBytes("0pP4Lk3y");
            byte[] keyBytes = new byte[0x10];
            int len = pwdBytes.Length;

            if (len > keyBytes.Length)
            {
                len = keyBytes.Length;
            }

            Array.Copy(pwdBytes, keyBytes, len);
            rijndaelCipher.Key = keyBytes;
            rijndaelCipher.IV = keyBytes;
            ICryptoTransform transform = rijndaelCipher.CreateEncryptor();
            byte[] plainText = Encoding.UTF8.GetBytes(data);

            return Convert.ToBase64String
            (transform.TransformFinalBlock(plainText, 0, plainText.Length));
        }


        public List<Bank> getListBank(string country)
        {
            OutputModel output = new OutputModel();
            List<Bank> banks = new List<Bank>();

            try
            {
                string path = "/api/v1/bank/list/"+country;
                //string inputJson = (new JavaScriptSerializer()).Serialize(input);
                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                client.Headers[HttpRequestHeader.Authorization] = string.Format("Basic {0}", credentials());
                client.Encoding = Encoding.UTF8;
                ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
                output = JsonConvert.DeserializeObject<OutputModel>(client.DownloadString(ConfigurationManager.AppSettings["URL_OMG_DEV"].ToString() + path));

                if (output.status.Equals("success"))
                {
                    banks = JsonConvert.DeserializeObject<List<Bank>>(output.data.ToString());
                }
                else
                {
                    banks = null;
                }

                return banks;
            }
            catch (Exception ex)
            {
                return banks = null;
            }
        }

        public List<tblM_Rate> getListRateMYRIDR()
        {
            OutputModel output = new OutputModel();
            List<tblM_Rate> rate = new List<tblM_Rate>();

            try
            {
                return rate = db.tblM_Rate.Where(p => p.RemittanceID == 1).ToList();
            }
            catch (Exception ex)
            {
                return rate = null;
            }
        }

        public List<tblM_Rate> getListRateIDRMYR()
        {
            OutputModel output = new OutputModel();
            List<tblM_Rate> rate = new List<tblM_Rate>();

            try
            {
                return rate = db.tblM_Rate.Where(p => p.RemittanceID == 2).ToList();
            }
            catch (Exception ex)
            {
                return rate = null;
            }
        }

        public List<tblM_Rate> getListRateIDRSAR()
        {
            OutputModel output = new OutputModel();
            List<tblM_Rate> rate = new List<tblM_Rate>();

            try
            {
                return rate = db.tblM_Rate.Where(p => p.RemittanceID == 3).ToList();
            }
            catch (Exception ex)
            {
                return rate = null;
            }
        }

        public Rates getRateMYR2IDR()
        {
            OutputModel output = new OutputModel();
            Rates rate = new Rates();

            try
            {
                //string path = "/api/v1/rate/myr";
                string path = "/api/v2/rate/remittance/1";
                //string inputJson = (new JavaScriptSerializer()).Serialize(input);
                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                client.Headers[HttpRequestHeader.Authorization] = string.Format("Basic {0}", credentials());
                client.Encoding = Encoding.UTF8;
                ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
                output = JsonConvert.DeserializeObject<OutputModel>(client.DownloadString(ConfigurationManager.AppSettings["URL_OMG_DEV"].ToString() + path));

                if (output.status.Equals("success"))
                {
                    rate = JsonConvert.DeserializeObject<Rates>(output.data.ToString());
                }
                else
                {
                    rate = null;
                }

                return rate;
            }
            catch (Exception ex)
            {
                return rate = null;
            }
        }

        public bool updateRateMYRIDR(UpdateRate upr)
        {
            OutputModel output = new OutputModel();

            try
            {
                upr.RateBase = EncryptAPP(upr.RateBase);
                upr.Margin = EncryptAPP(upr.Margin);
                upr.Rate = EncryptAPP(upr.Rate);

                string path = "/api/admin/rate/update";
                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                //client.Headers[HttpRequestHeader.Authorization] = string.Format("Basic {0}", credentials());
                client.Encoding = Encoding.UTF8;
                ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
                output = JsonConvert.DeserializeObject<OutputModel>(client.UploadString(ConfigurationManager.AppSettings["URL_OMG_DEV"].ToString() + path, JsonConvert.SerializeObject(upr)));

                if (output.status.Equals("success"))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public List<Rates> getRateIDByPartnerID()
        {
            OutputModel output = new OutputModel();
            List<Rates> rate = new List<Rates>();

            try
            {
                string path = "/api/v2/rate/all";
                //string inputJson = (new JavaScriptSerializer()).Serialize(input);
                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                client.Headers[HttpRequestHeader.Authorization] = string.Format("Basic {0}", credentials());
                client.Encoding = Encoding.UTF8;
                ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
                output = JsonConvert.DeserializeObject<OutputModel>(client.DownloadString(ConfigurationManager.AppSettings["URL_OMG_DEV"].ToString() + path));

                if (output.status.Equals("success"))
                {
                    rate = JsonConvert.DeserializeObject<List<Rates>>(output.data.ToString());
                }
                else
                {
                    rate = null;
                }

                return rate;
            }
            catch (Exception ex)
            {
                return rate = null;
            }
        }

        public bool createAndApproveTopup(TopupModel tup)
        {
            OutputModel output = new OutputModel();

            try
            {
                tup.PartnerID = 1;
                tup.CreatedBy = 1;
                tup.BankAccountID = 1;

                string path = "/api/admin/topup/request/approve/" + 1;
                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                //client.Headers[HttpRequestHeader.Authorization] = string.Format("Basic {0}", credentials());
                client.Encoding = Encoding.UTF8;
                ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
                output = JsonConvert.DeserializeObject<OutputModel>(client.UploadString(ConfigurationManager.AppSettings["URL_OMG_DEV"].ToString() + path, JsonConvert.SerializeObject(tup)));

                if (output.status.Equals("success"))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool createAndApproveTopupMalindo(TopupModel tup)
        {
            OutputModel output = new OutputModel();

            try
            {
                tup.PartnerID = 1;
                tup.CreatedBy = 1;

                string path = "/api/admin/topup/request/approve/malindo/" + 1;
                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                //client.Headers[HttpRequestHeader.Authorization] = string.Format("Basic {0}", credentials());
                client.Encoding = Encoding.UTF8;
                ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
                output = JsonConvert.DeserializeObject<OutputModel>(client.UploadString(ConfigurationManager.AppSettings["URL_OMG_DEV"].ToString() + path, JsonConvert.SerializeObject(tup)));

                if (output.status.Equals("success"))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public Rates getRateIDRMYR()
        {
            OutputModel output = new OutputModel();
            Rates rate = new Rates();

            try
            {
                //string path = "/api/v1/rate/idr";
                string path = "/api/v2/rate/remittance/2";
                //string inputJson = (new JavaScriptSerializer()).Serialize(input);
                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                client.Headers[HttpRequestHeader.Authorization] = string.Format("Basic {0}", credentials());
                client.Encoding = Encoding.UTF8;
                ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
                output = JsonConvert.DeserializeObject<OutputModel>(client.DownloadString(ConfigurationManager.AppSettings["URL_OMG_DEV"].ToString() + path));

                if (output.status.Equals("success"))
                {
                    rate = JsonConvert.DeserializeObject<Rates>(output.data.ToString());
                }
                else
                {
                    rate = null;
                }

                return rate;
            }
            catch (Exception ex)
            {
                return rate = null;
            }
        }

        public Rates getRateIDRSAR()
        {
            OutputModel output = new OutputModel();
            Rates rate = new Rates();

            try
            {
                string path = "/api/v2/rate/remittance/3";
                //string inputJson = (new JavaScriptSerializer()).Serialize(input);
                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                client.Headers[HttpRequestHeader.Authorization] = string.Format("Basic {0}", credentials());
                client.Encoding = Encoding.UTF8;
                ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
                output = JsonConvert.DeserializeObject<OutputModel>(client.DownloadString(ConfigurationManager.AppSettings["URL_OMG_DEV"].ToString() + path));

                if (output.status.Equals("success"))
                {
                    rate = JsonConvert.DeserializeObject<Rates>(output.data.ToString());
                }
                else
                {
                    rate = null;
                }

                return rate;
            }
            catch (Exception ex)
            {
                return rate = null;
            }
        }

        public BankAccount validateAccount(BankAccount ba)
        {
            OutputModel output = new OutputModel();
            BankAccount bar = new BankAccount();

            try
            {
                string path = "/api/v1/bank/check";
                //string inputJson = JsonConvert.SerializeObject(prq);
                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                client.Headers[HttpRequestHeader.Authorization] = string.Format("Basic {0}", credentials());
                client.Encoding = Encoding.UTF8;
                ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
                output = JsonConvert.DeserializeObject<OutputModel>(client.UploadString(ConfigurationManager.AppSettings["URL_OMG_DEV"].ToString() + path, JsonConvert.SerializeObject(ba)));

                if (output.status.Equals("success"))
                {
                    bar = JsonConvert.DeserializeObject<BankAccount>(output.data.ToString());
                }
                else
                {
                    bar = null;
                }

                return bar;
            }
            catch (Exception ex)
            {
                return bar = null;
            }
        }

        public PayoutResponse createApprovePayout(PayoutRequest prq)
        {
            OutputModel output = new OutputModel();
            PayoutResponse prs = new PayoutResponse();

            try
            {
                string path = "/api/v1/payout";
                //string inputJson = JsonConvert.SerializeObject(prq);
                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                client.Headers[HttpRequestHeader.Authorization] = string.Format("Basic {0}", credentials());
                client.Encoding = Encoding.UTF8;
                ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
                output = JsonConvert.DeserializeObject<OutputModel>(client.UploadString(ConfigurationManager.AppSettings["URL_OMG_DEV"].ToString() + path, JsonConvert.SerializeObject(prq)));

                if (output.status.Equals("success"))
                {
                    prs = JsonConvert.DeserializeObject<PayoutResponse>(output.data.ToString());
                }
                else
                {
                    prs = null;
                }

                return prs;
            }
            catch (Exception ex)
            {
                return prs = null;
            }
        }

        public OutputModel createApprovePayoutNew(PayoutRequest prq)
        {
            OutputModel output = new OutputModel();
            PayoutResponse prs = new PayoutResponse();

            try
            {
                string path = "/api/v2/payout";
                //string inputJson = JsonConvert.SerializeObject(prq);
                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                client.Headers[HttpRequestHeader.Authorization] = string.Format("Basic {0}", credentials());
                client.Encoding = Encoding.UTF8;
                ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
                return output = JsonConvert.DeserializeObject<OutputModel>(client.UploadString(ConfigurationManager.AppSettings["URL_OMG_DEV"].ToString() + path, JsonConvert.SerializeObject(prq)));

            }
            catch (Exception ex)
            {
                output.message = ex.ToString();
                output.status = "error";
                return output;
            }
        }

        public OutputModel createBNIVA(CreateVAModel va)
        {
            OutputModel output = new OutputModel();
            
            try
            {
                string path = "/api/admin/va/bni/create";
                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                //client.Headers[HttpRequestHeader.Authorization] = string.Format("Basic {0}", credentials());
                client.Encoding = Encoding.UTF8;
                ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
                return output = JsonConvert.DeserializeObject<OutputModel>(client.UploadString(ConfigurationManager.AppSettings["URL_OMG_DEV"].ToString() + path, JsonConvert.SerializeObject(va)));

            }
            catch (Exception ex)
            {
                output.message = ex.ToString();
                output.status = "error";
                return output;
            }
        }

        //public PayoutResponse createPayout(PayoutRequest prq)
        //{
        //    OutputModel output = new OutputModel();
        //    PayoutResponse prs = new PayoutResponse();

        //    try
        //    {
        //        string path = "/api/v2/payout/create";
        //        //string inputJson = JsonConvert.SerializeObject(prq);
        //        WebClient client = new WebClient();
        //        client.Headers["Content-type"] = "application/json";
        //        client.Headers[HttpRequestHeader.Authorization] = string.Format("Basic {0}", credentials());
        //        client.Encoding = Encoding.UTF8;
        //        ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
        //        var data = JsonConvert.SerializeObject(prq);
        //        output = JsonConvert.DeserializeObject<OutputModel>(client.UploadString(ConfigurationManager.AppSettings["URL_OMG_DEV"].ToString() + path, JsonConvert.SerializeObject(prq)));

        //        if (output.status.Equals("success"))
        //        {
        //            prs = JsonConvert.DeserializeObject<PayoutResponse>(output.data.ToString());
        //        }
        //        else
        //        {
        //            prs = null;
        //        }

        //        return prs;
        //    }
        //    catch (Exception ex)
        //    {
        //        return prs = null;
        //    }
        //}

        public OutputModel createPayout(PayoutRequest prq)
        {
            OutputModel output = new OutputModel();

            try
            {
                string path = "/api/v2/payout/create";
                //string inputJson = JsonConvert.SerializeObject(prq);
                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                client.Headers[HttpRequestHeader.Authorization] = string.Format("Basic {0}", credentials());
                client.Encoding = Encoding.UTF8;
                ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
                var data = JsonConvert.SerializeObject(prq);
                return output = JsonConvert.DeserializeObject<OutputModel>(client.UploadString(ConfigurationManager.AppSettings["URL_OMG_DEV"].ToString() + path, JsonConvert.SerializeObject(prq)));
                
            }
            catch (Exception ex)
            {
                output.status = "error";
                output.message = ex.ToString();
                return output;
            }
        }

        public PayoutResponse approvePayout(string refid)
        {
            OutputModel output = new OutputModel();
            PayoutResponse prs = new PayoutResponse();

            try
            {
                string path = "/api/v2/payout/approve/"+refid;
                //string inputJson = JsonConvert.SerializeObject(prq);
                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                client.Headers[HttpRequestHeader.Authorization] = string.Format("Basic {0}", credentials());
                client.Encoding = Encoding.UTF8;
                ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
                output = JsonConvert.DeserializeObject<OutputModel>(client.DownloadString(ConfigurationManager.AppSettings["URL_OMG_DEV"].ToString() + path));

                if (output.status.Equals("success"))
                {
                    prs = JsonConvert.DeserializeObject<PayoutResponse>(output.data.ToString());
                }
                else
                {
                    prs = null;
                }

                return prs;
            }
            catch (Exception ex)
            {
                return prs = null;
            }
        }



        public PayoutResponse rejectPayout(string channel, string refid, string reason)
        {
            OutputModel output = new OutputModel();
            PayoutResponse prs = new PayoutResponse();

            try
            {
                string path = "";
                if (channel == "iris")
                    path = "/api/v2/payout/reject/"+refid;
                else if (channel == "amc")
                    path = "/api/v2/payout/reject/amc/" + refid;
                //string inputJson = JsonConvert.SerializeObject("{\"Message\":\""+reason+"\"}");
                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                client.Headers[HttpRequestHeader.Authorization] = string.Format("Basic {0}", credentials());
                client.Encoding = Encoding.UTF8;
                ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
                //var message = JsonConvert.SerializeObject("{\"Message\":\"" + reason + "\"}");
                var message = "{\"Message\":\"" + reason + "\"}";
                output = JsonConvert.DeserializeObject<OutputModel>(client.UploadString(ConfigurationManager.AppSettings["URL_OMG_DEV"].ToString() + path, message));

                if (output.status.Equals("success"))
                {
                    prs = JsonConvert.DeserializeObject<PayoutResponse>(output.data.ToString());
                }
                else
                {
                    prs = null;
                }

                return prs;
            }
            catch (Exception ex)
            {
                return prs = null;
            }
        }

        public Profile getProfile()
        {
            OutputModel output = new OutputModel();
            Profile prs = new Profile();

            try
            {
                string path = "/api/v1/profile/" + ConfigurationManager.AppSettings["OMG_USERNAME"].ToString();
                //string inputJson = JsonConvert.SerializeObject(prq);
                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                client.Headers[HttpRequestHeader.Authorization] = string.Format("Basic {0}", credentials());
                client.Encoding = Encoding.UTF8;
                ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
                output = JsonConvert.DeserializeObject<OutputModel>(client.DownloadString(ConfigurationManager.AppSettings["URL_OMG_DEV"].ToString() + path));

                if (output.status.Equals("success"))
                {
                    prs = JsonConvert.DeserializeObject<Profile>(output.data.ToString());
                }
                else
                {
                    prs = null;
                }

                return prs;
            }
            catch (Exception ex)
            {
                return prs = null;
            }
        }

        public bool updateProfileCallbackURL(string url)
        {
            OutputModel output = new OutputModel();

            try
            {
                string path = "/api/v1/profile/" + ConfigurationManager.AppSettings["OMG_USERNAME"].ToString() + "/update/callback";
                //string inputJson = JsonConvert.SerializeObject(prq);
                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                client.Headers[HttpRequestHeader.Authorization] = string.Format("Basic {0}", credentials());
                client.Encoding = Encoding.UTF8;
                ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;

                string data = "{\"CallbackURL\":\"" + url + "\"}";

                output = JsonConvert.DeserializeObject<OutputModel>(client.UploadString(ConfigurationManager.AppSettings["URL_OMG_DEV"].ToString() + path, data));

                if (output.status.Equals("success"))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public void sendEmailApproval(List<tblM_Admin> list, string name, string amount)
        {
            for (int i = 0; i < list.Count; i++)
            {
                using (SmtpClient smtpClient = new SmtpClient())
                {
                    //var basicCredential = new NetworkCredential("omgatewayid@gmail.com", "Arshavin23");
                    var basicCredential = new NetworkCredential(ConfigurationManager.AppSettings["MAIL_USER"].ToString(), ConfigurationManager.AppSettings["MAIL_PWD"].ToString());
                    using (MailMessage message = new MailMessage())
                    {
                        //MailAddress fromAddress = new MailAddress("omgatewayid@gmail.com");
                        MailAddress fromAddress = new MailAddress(ConfigurationManager.AppSettings["MAIL_ADDRESS"].ToString());

                        smtpClient.EnableSsl = true;
                        smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network; // [2] Added this
                        //smtpClient.Port = 587;
                        smtpClient.Port = Convert.ToInt32(ConfigurationManager.AppSettings["MAIL_PORT"].ToString());
                        smtpClient.UseDefaultCredentials = false;
                        smtpClient.Credentials = basicCredential;
                        //smtpClient.Host = "smtp.gmail.com";
                        smtpClient.Host = ConfigurationManager.AppSettings["MAIL_SERVER"].ToString();

                        message.From = fromAddress;
                        message.Subject = "Transfer Approval Request";
                        // Set IsBodyHtml to true means you can send HTML email.
                        message.IsBodyHtml = true;
                        message.Body = "Hi " + list[i].Fullname + ", <br/><br/>You have pending task that need to be approve from " + name + " and the amount is " + amount + ".<br/>Please log in into http://app.malindogateway.com/Payout/PendingTask to take action of the transaction. <br/><br/>Thank You!";
                        message.To.Add(list[i].Email);

                        try
                        {
                            smtpClient.Send(message);
                        }
                        catch (Exception ex)
                        {
                            //Error, could not send the message
                            //Response.Write(ex.Message);
                        }
                    }
                }
            }
        }

        public void sendEmailSenderApproval(List<tblM_Admin> list, string name)
        {
            for (int i = 0; i < list.Count; i++)
            {
                using (SmtpClient smtpClient = new SmtpClient())
                {
                    var basicCredential = new NetworkCredential(ConfigurationManager.AppSettings["MAIL_USER"].ToString(), ConfigurationManager.AppSettings["MAIL_PWD"].ToString());
                    using (MailMessage message = new MailMessage())
                    {
                        //MailAddress fromAddress = new MailAddress("omgatewayid@gmail.com");
                        MailAddress fromAddress = new MailAddress(ConfigurationManager.AppSettings["MAIL_ADDRESS"].ToString());

                        smtpClient.EnableSsl = true;
                        smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network; // [2] Added this
                        //smtpClient.Port = 587;
                        smtpClient.Port = Convert.ToInt32(ConfigurationManager.AppSettings["MAIL_PORT"].ToString());
                        smtpClient.UseDefaultCredentials = false;
                        smtpClient.Credentials = basicCredential;
                        //smtpClient.Host = "smtp.gmail.com";
                        smtpClient.Host = ConfigurationManager.AppSettings["MAIL_SERVER"].ToString();

                        message.From = fromAddress;
                        message.Subject = "Sender Approval Request";
                        // Set IsBodyHtml to true means you can send HTML email.
                        message.IsBodyHtml = true;
                        message.Body = "Hi " + list[i].Fullname + ", <br/><br/>You have pending task that need approve sender from " + name + ".<br/>Please log in into http://app.malindogateway.com/Sender/ApproveSenderList to take action of the sender. <br/><br/>Thank You!";
                        message.To.Add(list[i].Email);

                        try
                        {
                            smtpClient.Send(message);
                        }
                        catch (Exception ex)
                        {
                            //Error, could not send the message
                            //Response.Write(ex.Message);
                        }
                    }
                }
            }
        }

        public void sendEmailTopupApproval(List<tblM_Admin> list, string name)
        {
            for (int i = 0; i < list.Count; i++)
            {
                using (SmtpClient smtpClient = new SmtpClient())
                {
                    var basicCredential = new NetworkCredential(ConfigurationManager.AppSettings["MAIL_USER"].ToString(), ConfigurationManager.AppSettings["MAIL_PWD"].ToString());
                    using (MailMessage message = new MailMessage())
                    {
                        //MailAddress fromAddress = new MailAddress("omgatewayid@gmail.com");
                        MailAddress fromAddress = new MailAddress(ConfigurationManager.AppSettings["MAIL_ADDRESS"].ToString());

                        smtpClient.EnableSsl = true;
                        smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network; // [2] Added this
                        //smtpClient.Port = 587;
                        smtpClient.Port = Convert.ToInt32(ConfigurationManager.AppSettings["MAIL_PORT"].ToString());
                        smtpClient.UseDefaultCredentials = false;
                        smtpClient.Credentials = basicCredential;
                        //smtpClient.Host = "smtp.gmail.com";
                        smtpClient.Host = ConfigurationManager.AppSettings["MAIL_SERVER"].ToString();

                        message.From = fromAddress;
                        message.Subject = "Topup Approval Request";
                        // Set IsBodyHtml to true means you can send HTML email.
                        message.IsBodyHtml = true;
                        message.Body = "Hi " + list[i].Fullname + ", <br/><br/>You have pending task that need approve topup from " + name + ".<br/>Please log in into http://app.malindogateway.com/Topup/PendingTopupRequest to take action of the request. <br/><br/>Thank You!";
                        message.To.Add(list[i].Email);

                        try
                        {
                            smtpClient.Send(message);
                        }
                        catch (Exception ex)
                        {
                            //Error, could not send the message
                            //Response.Write(ex.Message);
                        }
                    }
                }
            }
        }

        public void sendEmailAgent(tblT_Payout pay)
        {
            using (SmtpClient smtpClient = new SmtpClient())
            {
                var basicCredential = new NetworkCredential(ConfigurationManager.AppSettings["MAIL_USER"].ToString(), ConfigurationManager.AppSettings["MAIL_PWD"].ToString());
                using (MailMessage message = new MailMessage())
                {
                    string agent = "";
                    string email = "";
                    if (pay.isAgent == 1)
                    {
                        var ag = db.tblM_Agent.Where(p => p.AgentID == pay.CreatedBy).FirstOrDefault();
                        agent = ag.Fullname;
                        email = ag.Email;
                    }
                    else
                    {
                        var ad = db.tblM_Admin.Where(p => p.AdminID == pay.CreatedBy).FirstOrDefault();
                        agent = ad.Fullname;
                        email = ad.Email;
                    }

                    MailAddress fromAddress = new MailAddress(ConfigurationManager.AppSettings["MAIL_ADDRESS"].ToString());

                    smtpClient.EnableSsl = true;
                    smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network; // [2] Added this
                    //smtpClient.Port = 587;
                    smtpClient.Port = Convert.ToInt32(ConfigurationManager.AppSettings["MAIL_PORT"].ToString());
                    smtpClient.UseDefaultCredentials = false;
                    smtpClient.Credentials = basicCredential;
                    smtpClient.Host = ConfigurationManager.AppSettings["MAIL_SERVER"].ToString();

                    message.From = fromAddress;
                    message.Subject = "Transfer Notification: " + pay.Status.ToUpper();
                    // Set IsBodyHtml to true means you can send HTML email.
                    message.IsBodyHtml = true;
                    string Body = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("~/EmailReceipt.htm"));
                    Body = Body.Replace("#ReceiptName#", agent);
                    Body = Body.Replace("#AccountNo#", pay.BeneficiaryAccount);
                    Body = Body.Replace("#AccountHolder#", pay.BeneficiaryName);
                    Body = Body.Replace("#AmountTransfer#", String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(pay.TotalTransfer)).Replace("Rp", "Rp "));
                    Body = Body.Replace("#BankName#", pay.BankName);
                    Body = Body.Replace("#Status#", pay.Status);
                    Body = Body.Replace("#Notes#", pay.Notes);
                    Body = Body.Replace("#PayoutDate#", pay.ModifiedDate.ToString());
                    if (pay.Status == "completed" || pay.Status == "approved")
                    {
                        Body = Body.Replace("#StatusColor#", "#929497");
                    }
                    else if (pay.Status == "rejected" || pay.Status == "failed")
                    {
                        Body = Body.Replace("#StatusColor#", "#ff0000");
                    }

                    message.Body = Body;
                    message.To.Add(email);

                    try
                    {
                        smtpClient.Send(message);
                    }
                    catch (Exception ex)
                    {
                        //Error, could not send the message
                        //Response.Write(ex.Message);
                    }
                }
            }
        }

        public void sendEmailMasterArea(tblT_Payout pay)
        {
            using (SmtpClient smtpClient = new SmtpClient())
            {
                //var basicCredential = new NetworkCredential("omgatewayid@gmail.com", "Arshavin23");
                var basicCredential = new NetworkCredential(ConfigurationManager.AppSettings["MAIL_USER"].ToString(), ConfigurationManager.AppSettings["MAIL_PWD"].ToString());

                using (MailMessage message = new MailMessage())
                {
                    string ma = "";
                    string email = "";
                    if (pay.isAgent == 2)
                    {
                        var ag = db.tblM_Master_Area.Where(p => p.MasterAreaID == pay.CreatedBy).FirstOrDefault();
                        ma = ag.Fullname;
                        email = ag.Email;
                    }
                    else
                    {
                        var ad = db.tblM_Admin.Where(p => p.AdminID == pay.CreatedBy).FirstOrDefault();
                        ma = ad.Fullname;
                        email = ad.Email;
                    }

                    //MailAddress fromAddress = new MailAddress("omgatewayid@gmail.com");
                    MailAddress fromAddress = new MailAddress(ConfigurationManager.AppSettings["MAIL_ADDRESS"].ToString());

                    smtpClient.EnableSsl = true;
                    smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network; // [2] Added this
                    //smtpClient.Port = 587;
                    smtpClient.Port = Convert.ToInt32(ConfigurationManager.AppSettings["MAIL_PORT"].ToString());
                    smtpClient.UseDefaultCredentials = false;
                    smtpClient.Credentials = basicCredential;
                    //smtpClient.Host = "smtp.gmail.com";
                    smtpClient.Host = ConfigurationManager.AppSettings["MAIL_SERVER"].ToString();

                    message.From = fromAddress;
                    message.Subject = "Transfer Notification: " + pay.Status.ToUpper();
                    // Set IsBodyHtml to true means you can send HTML email.
                    message.IsBodyHtml = true;
                    string Body = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("~/EmailReceipt.htm"));
                    Body = Body.Replace("#ReceiptName#", ma);
                    Body = Body.Replace("#AccountNo#", pay.BeneficiaryAccount);
                    Body = Body.Replace("#AccountHolder#", pay.BeneficiaryName);
                    Body = Body.Replace("#AmountTransfer#", String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(pay.TotalTransfer)).Replace("Rp", "Rp "));
                    Body = Body.Replace("#BankName#", pay.BankName);
                    Body = Body.Replace("#Status#", pay.Status);
                    Body = Body.Replace("#Notes#", pay.Notes);
                    Body = Body.Replace("#PayoutDate#", pay.ModifiedDate.ToString());
                    if (pay.Status == "completed" || pay.Status == "approved")
                    {
                        Body = Body.Replace("#StatusColor#", "#929497");
                    }
                    else if (pay.Status == "rejected" || pay.Status == "failed")
                    {
                        Body = Body.Replace("#StatusColor#", "#ff0000");
                    }

                    message.Body = Body;
                    message.To.Add(email);

                    try
                    {
                        smtpClient.Send(message);
                    }
                    catch (Exception ex)
                    {
                        //Error, could not send the message
                        //Response.Write(ex.Message);
                    }
                }
            }
        }

        public void sendEmailTravelAgent(tblT_Payout pay)
        {
            using (SmtpClient smtpClient = new SmtpClient())
            {
                var basicCredential = new NetworkCredential(ConfigurationManager.AppSettings["MAIL_USER"].ToString(), ConfigurationManager.AppSettings["MAIL_PWD"].ToString());
                //var basicCredential = new NetworkCredential("omgatewayid@gmail.com", "Arshavin23");
                using (MailMessage message = new MailMessage())
                {
                    string ma = "";
                    string email = "";
                    if (pay.isAgent == 3)
                    {
                        var ag = db.tblM_Travel_Agent.Where(p => p.TravelAgentID == pay.CreatedBy).FirstOrDefault();
                        ma = ag.Name;
                        email = ag.Email;
                    }
                    else
                    {
                        var ad = db.tblM_Admin.Where(p => p.AdminID == pay.CreatedBy).FirstOrDefault();
                        ma = ad.Fullname;
                        email = ad.Email;
                    }

                    MailAddress fromAddress = new MailAddress(ConfigurationManager.AppSettings["MAIL_ADDRESS"].ToString());
                    //MailAddress fromAddress = new MailAddress("omgatewayid@gmail.com");

                    smtpClient.EnableSsl = true;
                    smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network; // [2] Added this
                    smtpClient.Port = Convert.ToInt32(ConfigurationManager.AppSettings["MAIL_PORT"].ToString());
                    //smtpClient.Port = 587;
                    smtpClient.UseDefaultCredentials = false;
                    smtpClient.Credentials = basicCredential;
                    smtpClient.Host = ConfigurationManager.AppSettings["MAIL_SERVER"].ToString();
                    //smtpClient.Host = "smtp.gmail.com";

                    message.From = fromAddress;
                    message.Subject = "Transfer Notification: " + pay.Status.ToUpper();
                    // Set IsBodyHtml to true means you can send HTML email.
                    message.IsBodyHtml = true;
                    string Body = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("~/EmailReceipt.htm"));
                    Body = Body.Replace("#ReceiptName#", ma);
                    Body = Body.Replace("#AccountNo#", pay.BeneficiaryAccount);
                    Body = Body.Replace("#AccountHolder#", pay.BeneficiaryName);
                    Body = Body.Replace("#AmountTransfer#", String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(pay.TotalTransfer)).Replace("Rp", "Rp "));
                    Body = Body.Replace("#BankName#", pay.BankName);
                    Body = Body.Replace("#Status#", pay.Status);
                    Body = Body.Replace("#Notes#", pay.Notes);
                    Body = Body.Replace("#PayoutDate#", pay.ModifiedDate.ToString());
                    if (pay.Status == "completed" || pay.Status == "approved")
                    {
                        Body = Body.Replace("#StatusColor#", "#929497");
                    }
                    else if (pay.Status == "rejected" || pay.Status == "failed")
                    {
                        Body = Body.Replace("#StatusColor#", "#ff0000");
                    }

                    message.Body = Body;
                    message.To.Add(email);

                    try
                    {
                        smtpClient.Send(message);
                    }
                    catch (Exception ex)
                    {
                        //Error, could not send the message
                        //Response.Write(ex.Message);
                    }
                }
            }
        }

        public void sendEmailUserAccountCreated(tblM_Admin user, string tmpPWD)
        {
            using (SmtpClient smtpClient = new SmtpClient())
            {
                var basicCredential = new NetworkCredential(ConfigurationManager.AppSettings["MAIL_USER"].ToString(), ConfigurationManager.AppSettings["MAIL_PWD"].ToString());
                using (MailMessage message = new MailMessage())
                {
                    string ma = "";
                    string email = "";
                    email = user.Email;

                    MailAddress fromAddress = new MailAddress(ConfigurationManager.AppSettings["MAIL_ADDRESS"].ToString());

                    smtpClient.EnableSsl = true;
                    smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network; // [2] Added this
                    smtpClient.Port = Convert.ToInt32(ConfigurationManager.AppSettings["MAIL_PORT"].ToString());
                    smtpClient.UseDefaultCredentials = false;
                    smtpClient.Credentials = basicCredential;
                    smtpClient.Host = ConfigurationManager.AppSettings["MAIL_SERVER"].ToString();

                    message.From = fromAddress;
                    message.Subject = "Your AutoPay Account Has Been Created";
                    // Set IsBodyHtml to true means you can send HTML email.
                    message.IsBodyHtml = true;
                    string Body = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("~/EmailAccountCreation.htm"));
                    Body = Body.Replace("#UserName#", user.Fullname);
                    Body = Body.Replace("#UserLogin#", user.Phone);
                    Body = Body.Replace("#UserPassword#", tmpPWD);
                    message.Body = Body;
                    message.To.Add(email);

                    try
                    {
                        smtpClient.Send(message);
                    }
                    catch (Exception ex)
                    {
                        //Error, could not send the message
                        //Response.Write(ex.Message);
                    }
                }
            }
        }

        public void sendEmailPasswordChanged(tblM_Admin user, string tmpPWD)
        {
            using (SmtpClient smtpClient = new SmtpClient())
            {
                var basicCredential = new NetworkCredential(ConfigurationManager.AppSettings["MAIL_USER"].ToString(), ConfigurationManager.AppSettings["MAIL_PWD"].ToString());
                using (MailMessage message = new MailMessage())
                {
                    string ma = "";
                    string email = "";
                    email = user.Email;

                    MailAddress fromAddress = new MailAddress(ConfigurationManager.AppSettings["MAIL_ADDRESS"].ToString());

                    smtpClient.EnableSsl = true;
                    smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network; // [2] Added this
                    smtpClient.Port = Convert.ToInt32(ConfigurationManager.AppSettings["MAIL_PORT"].ToString());
                    smtpClient.UseDefaultCredentials = false;
                    smtpClient.Credentials = basicCredential;
                    smtpClient.Host = ConfigurationManager.AppSettings["MAIL_SERVER"].ToString();

                    message.From = fromAddress;
                    message.Subject = "Your AutoPay Account Password Has Been Changed";
                    // Set IsBodyHtml to true means you can send HTML email.
                    message.IsBodyHtml = true;
                    string Body = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("~/EmailPasswordChanged.htm"));
                    Body = Body.Replace("#UserName#", user.Fullname);
                    message.Body = Body;
                    message.To.Add(email);

                    try
                    {
                        smtpClient.Send(message);
                    }
                    catch (Exception ex)
                    {
                        //Error, could not send the message
                        //Response.Write(ex.Message);
                    }
                }
            }
        }

        public void writeAuditLog(DateTime eDate, string eType, long eUser, string eData)
        {
            tblS_Log eLog = new tblS_Log
            {
                EventDate = eDate,
                EventBy = eUser,
                EventType = eType,
                EventData = eData,
            };

            db.tblS_Log.Add(eLog);
            db.SaveChanges();
        }
    }
}