﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CheckerDashboard.aspx.cs" Inherits="OMGLiveDemo.CheckerDashboard" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" />

    <hr />

    <h3>Checker Dashboard</h3>

    <div class="row top_tiles">
        <div class="animated flipInY col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <div class="tile-block">
                <div class="icon-wide"><i class="fa fa-users"></i></div>
                <div class="tile-header">
                    Successful Registration
                </div>
                <h3>Sender
                    <asp:LinkButton runat="server" ID="lbtnSendSuccReg" Text="0" CommandArgument="approved" OnClick="btnViewSenderClick" />
                </h3>
                <h3>Beneficiary
                    <asp:LinkButton runat="server" ID="lbtnBeneSuccReg" Text="0" CommandArgument="approved" OnClick="btnViewBeneficiaryClick" />
                </h3>
            </div>
        </div>
        <div class="animated flipInY col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <div class="tile-block">
                <div class="icon-wide"><i class="fa fa-users"></i></div>
                <div class="tile-header">
                    Pending Registration
                </div>
                <h3>Sender
                    <asp:LinkButton runat="server" ID="lbtnSendPendReg" Text="0" CommandArgument="created" OnClick="btnViewSenderClick" />
                </h3>
                <h3>Beneficiary
                    <asp:LinkButton runat="server" ID="lbtnBenePendReg" Text="0" CommandArgument="created" OnClick="btnViewBeneficiaryClick" />
                </h3>
            </div>
        </div>
        <div class="animated flipInY col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <div class="tile-block">
                <div class="icon-wide"><i class="fa fa-users"></i></div>
                <div class="tile-header">
                    Amend Registration
                </div>
                <h3>Sender
                    <asp:LinkButton runat="server" ID="lbtnSendAmendReg" Text="0" CommandArgument="amend" OnClick="btnViewSenderClick" />
                </h3>
                <h3>Beneficiary
                    <asp:LinkButton runat="server" ID="lbtnBeneAmendReg" Text="0" CommandArgument="amend" OnClick="btnViewBeneficiaryClick" />
                </h3>
            </div>
        </div>
        <div class="animated flipInY col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <div class="tile-block">
                <div class="icon-wide"><i class="fa fa-users"></i></div>
                <div class="tile-header">
                    Rejected Registration
                </div>
                <h3>Sender
                    <asp:LinkButton runat="server" ID="lbtnSendFailReg" Text="0" CommandArgument="rejected" OnClick="btnViewSenderClick" />
                </h3>
                <h3>Beneficiary
                    <asp:LinkButton runat="server" ID="lbtnBeneFailReg" Text="0" CommandArgument="rejected" OnClick="btnViewBeneficiaryClick" />
                </h3>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <div class="alert alert-danger fade in">
                    <div class="centered">
                        <asp:Label runat="server" ID="lblRateI2MTitle" Font-Bold="true" Text="Indonesia to Malaysia" Font-Size="X-Large" />
                    </div>
                    <div class="centered">
                        Current Rate
                    </div>
                    <div class="centered">
                        <asp:Label runat="server" ID="lblRateI2M" Font-Bold="true" Text="1 MYR = x.xxx IDR" Font-Size="Medium" />
                    </div>
                    <div class="centered">
                        <asp:Label runat="server" ID="lblRateUpdateDate" Text="Last update : " />
                    </div>
                </div>
            </div>
        </div>
    </div>


</asp:Content>
