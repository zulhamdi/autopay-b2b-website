﻿using OMGLiveDemo.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.Agent
{
    public partial class BalanceHistory : System.Web.UI.Page
    {
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    checkAgent();
                    //BindGV();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        void checkAgent()
        {
            if (SessionLib.Current.Role == Roles.Agent) {
                divagent.Visible = false;
                bindBalance();
            }
            else
            {
                bindAgent();
            }
        }

        long getAgentID()
        {
            if (SessionLib.Current.Role == Roles.Agent)
                return Convert.ToInt64(SessionLib.Current.AdminID);
            else
                return Int64.Parse(ddlAgent.SelectedValue);
        }

        void bindAgent()
        {
            var dt = db.vw_Agent.Where(p => p.isActive == 1).ToList();
            ddlAgent.DataSource = null;
            ddlAgent.DataSource = dt;
            ddlAgent.DataValueField = "AgentID";
            ddlAgent.DataTextField = "Fullname";
            ddlAgent.DataBind();
            bindBalance();
        }

        void bindBalance()
        {
            long id = getAgentID();
            var dt = db.tblM_Agent_Balance.Where(p => p.isActive == 1 && p.AgentID == id).ToList();
            ddlBalance.DataSource = null;
            ddlBalance.DataSource = dt;
            ddlBalance.DataValueField = "BalanceID";
            ddlBalance.DataTextField = "Currency";
            ddlBalance.DataBind();
            BindGV();
        }
        
        void BindGV()
        {
            long id = getAgentID();
            var balid = Convert.ToInt64(ddlBalance.SelectedValue);
            var dt = db.vw_Agent_Balance_Update.Where(p => p.AgentID == id && p.BalanceID == balid).OrderByDescending(p => p.UpdatedDate).ToList();
            gvListItem.DataSource = dt;
            gvListItem.DataBind();
        }

        protected void btnAddAgent_Click(object sender, EventArgs e)
        {

        }

        protected void lblEdit_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            Session["AgentID"] = ((HiddenField)row.FindControl("hfAgentID")).Value;
            Session["RateLevel"] = row.Cells[3].Text;
            Response.Redirect("UpdateAgent");
        }

        protected void ddlBalance_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGV();
        }

        protected void ddlAgent_SelectedIndexChanged(object sender, EventArgs e)
        {
            bindBalance();
        }
    }
}