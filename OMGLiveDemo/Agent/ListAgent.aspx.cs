﻿using OMGLiveDemo.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.Agent
{
    public partial class ListAgent : System.Web.UI.Page
    {
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    BindGV();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        void BindGV()
        {
            if (Session["SuccessAgent"] != null)
            {
                lblAlertSuccess.Text = Session["SuccessAgent"].ToString();
                divsuccess.Visible = true;
                Session["SuccessAgent"] = null;
            }

            if (Session["ErrorAgent"] != null)
            {
                lblAlertFailed.Text = Session["ErrorAgent"].ToString();
                divfailed.Visible = true;
                Session["ErrorAgent"] = null;
            }

            var rid = Convert.ToInt64(ddlRemittanceLine.SelectedValue);
            gvListItem.DataSource = null;
            var dt = db.vw_Agent.Where(p => p.RemittanceID == rid).ToList();

            gvListItem.DataSource = dt;
            gvListItem.DataBind();
        }

        protected void btnAddAgent_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddAgent");
        }

        protected void lblEdit_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            Session["AgentID"] = ((HiddenField)row.FindControl("hfAgentID")).Value;
            Session["RemittanceID"] = ((HiddenField)row.FindControl("hfRemittanceID")).Value;
            Session["RateLevel"] = row.Cells[3].Text;
            Response.Redirect("UpdateAgent");
        }

        protected void btnAlertFailed_ServerClick(object sender, EventArgs e)
        {
            divfailed.Visible = false;
        }

        protected void btnAlertSuccess_ServerClick(object sender, EventArgs e)
        {
            divsuccess.Visible = false;
        }

        protected void ddlRemittanceLine_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGV();
        }

        protected void gvListItem_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[6].Text == "1")
                {
                    e.Row.Cells[6].Text = "Enabled";
                }
                else
                {
                    e.Row.Cells[6].Text = "Disabled";
                }
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            List<vw_Agent> agents = new List<vw_Agent>();
            var rid = Convert.ToInt64(ddlRemittanceLine.SelectedValue);

            if (txtAgentName.Text == "")
            {
                if (txtAgentPhone.Text == "")
                {
                    agents = db.vw_Agent.Where(p => p.RemittanceID == rid).ToList();
                }
                else
                {
                    agents = db.vw_Agent.Where(p => p.RemittanceID == rid && p.Phone.Contains(txtAgentPhone.Text.Trim())).ToList();
                }
            }
            else
            {
                if(txtAgentPhone.Text == "")
                {
                    agents = db.vw_Agent.Where(p => p.RemittanceID == rid && p.Fullname.Contains(txtAgentName.Text.Trim())).ToList();
                }
                else
                {
                    agents = db.vw_Agent.Where(p => p.RemittanceID == rid && p.Fullname.Contains(txtAgentName.Text.Trim()) && p.Phone.Contains(txtAgentPhone.Text.Trim())).ToList();
                }
            }

            gvListItem.DataSource = null;
            gvListItem.DataSource = agents;
            gvListItem.DataBind();
        }

        protected void btnSearchByPhone_Click(object sender, EventArgs e)
        {

        }

        protected void btnSearchByName_Click(object sender, EventArgs e)
        {

        }
    }
}