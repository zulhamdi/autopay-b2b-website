﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ListAgent.aspx.cs" Inherits="OMGLiveDemo.Agent.ListAgent" MaintainScrollPositionOnPostback="true" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" />

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph">

                <div class="row x_title">
                    <div class="col-md-12">
                        <h3>List Agent</h3>
                    </div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div style="width: 100%;">
                        <div id="canvas_dahs" class="demo-placeholder" style="width: 100%; height: 100%;">
                            <div class="alert alert-success alert-dismissible fade in" runat="server" id="divsuccess" visible="false">
                                <button type="button" runat="server" id="btnAlertSuccess" onserverclick="btnAlertSuccess_ServerClick" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <center><asp:Label runat="server" ID="lblAlertSuccess" Font-Size="14pt" Text="test error" /></center>
                            </div>

                            <div class="alert alert-danger alert-dismissible fade in" runat="server" id="divfailed" visible="false">
                                <button type="button" runat="server" id="btnAlertFailed" onserverclick="btnAlertFailed_ServerClick" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <center><asp:Label runat="server" ID="lblAlertFailed" Font-Size="14pt" Text="test error" /></center>
                            </div>

                            <div class="form-group" runat="server" id="divstatus" visible="true">
                                <asp:Label runat="server" ID="lblRemittanceLine" CssClass="control-label" Text="Remittance Line : " />
                                <div style="margin-top: 5px">
                                    <asp:DropDownList runat="server" ID="ddlRemittanceLine" CssClass="form-control" OnSelectedIndexChanged="ddlRemittanceLine_SelectedIndexChanged" AutoPostBack="true" CausesValidation="true">
                                        <asp:ListItem Enabled="true" Text="Malaysia to Indonesia" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Indonesia to Malaysia" Value="2"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="form-group" style="height: 5px"></div>
                            </div>

                            <div class="form-group">
                                <asp:Label runat="server" CssClass="control-label" Text="Name : " />
                                <asp:TextBox ID="txtAgentName" CssClass="form-control" runat="server" />
                                <div class="form-group" style="height: 2px"></div>
                                <asp:Label runat="server" CssClass="control-label" Text="Phone No : " />
                                <asp:TextBox ID="txtAgentPhone" CssClass="form-control" runat="server" />
                                <div class="form-group" style="height: 2px"></div>
                                <asp:Button runat="server" ID="btnSearch" OnClick="btnSearch_Click" Text="Search" CssClass="btn btn-success" />
                            </div>
                            
                            <div class="form-group" style="height: 5px"></div>
                            <asp:GridView CssClass="table table-striped jambo_table bulk_action" ID="gvListItem" runat="server" AutoGenerateColumns="false" DataKeyNames="AgentID" OnRowDataBound="gvListItem_RowDataBound">
                                <Columns>
                                    <asp:TemplateField HeaderText="No.">
                                        <ItemTemplate>
                                            <asp:HiddenField runat="server" ID="hfAgentID" Value='<%# Eval("AgentID") %>' />
                                            <asp:HiddenField runat="server" ID="hfRemittanceID" Value='<%# Eval("RemittanceID") %>' />
                                            <asp:HiddenField runat="server" ID="hfRateID" Value='<%# Eval("RateID") %>' />
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Fullname" HeaderText="Full Name" />
                                    <asp:BoundField DataField="Phone" HeaderText="Phone" />
                                    <asp:BoundField DataField="Email" HeaderText="Email" />
                                    <asp:BoundField DataField="RateName" HeaderText="Rate Level" />
                                    <asp:BoundField DataField="JoinDate" HeaderText="Join Date" />
                                    <asp:BoundField DataField="isActive" HeaderText="Status" />
                                    <asp:BoundField DataField="AdminName" HeaderText="Added By" />
                                    <asp:TemplateField HeaderText="Aksi">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lblEdit" runat="server" Text="Edit" CausesValidation="false" OnClick="lblEdit_Click" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <asp:Button runat="server" ID="btnAddAgent" OnClick="btnAddAgent_Click" Text="Add Agent" CssClass="btn btn-success" />
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Scripts" runat="server">
</asp:Content>
