﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BalanceHistory.aspx.cs" Inherits="OMGLiveDemo.Agent.BalanceHistory" MaintainScrollPositionOnPostback="true" %>
<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" />

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph">

                <div class="row x_title">
                    <div class="col-md-12">
                        <h3>Balance History</h3>
                    </div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group" runat="server" id="divagent" visible="false">
                        <asp:Label runat="server" ID="lblAgent" CssClass="control-label" Text="Agent : " />
                        <div style="margin-top: 5px">
                            <asp:DropDownList runat="server" ID="ddlAgent" CssClass="form-control" OnSelectedIndexChanged="ddlAgent_SelectedIndexChanged" AutoPostBack="true" />
                        </div>
                        <div class="form-group" style="height: 5px"></div>
                    </div>

                    <div class="form-group">
                        <asp:Label runat="server" ID="lblPartner" CssClass="control-label" Text="Balance : " />
                        <asp:DropDownList runat="server" ID="ddlBalance" CssClass="form-control" OnSelectedIndexChanged="ddlBalance_SelectedIndexChanged" AutoPostBack="true" />
                    </div>

                    <br />
                    <div style="width: 100%;">
                        <div id="canvas_dahs" class="demo-placeholder" style="width: 100%; height: 100%;">
                            <asp:GridView CssClass="table table-striped jambo_table bulk_action" ID="gvListItem" runat="server" AutoGenerateColumns="false" DataKeyNames="ID">
                                <Columns>
                                    <asp:TemplateField HeaderText="No.">
                                        <ItemTemplate>
                                            <asp:HiddenField runat="server" ID="hfID" Value='<%# Eval("ID") %>' />
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Reference" HeaderText="Reference" />
                                    <asp:BoundField DataField="Margin" HeaderText="Margin" />
                                    <asp:BoundField DataField="Amount" HeaderText="Amount" />
                                    <asp:BoundField DataField="Status" HeaderText="Status" />
                                    <asp:BoundField DataField="UpdatedDate" HeaderText="Date Updated" />
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Scripts" runat="server">
</asp:Content>
