﻿using OMGLiveDemo.Models;
using OMGLiveDemo.Models.OMG;
using OMGLiveDemo.Scripts;
using Oppal.Sec;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.Agent
{
    public partial class UpdateAgent : System.Web.UI.Page
    {
        OMG omg = new OMG();
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    bind();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        void bind()
        {
            var agentid = Int64.Parse(Session["AgentID"].ToString());
            var ratelev = Session["RateLevel"].ToString();
            var rid = Convert.ToInt64(Session["RemittanceID"].ToString());
            hfRemittanceID.Value = rid.ToString();
            string rateback = "";

            var agent = db.tblM_Agent.Where(p => p.AgentID == agentid).First();
            if (agent != null)
            {
                txtName.Text = agent.Fullname;
                txtPhone.Text = agent.Phone;
                txtEmail.Text = agent.Email;

                if (agent.isActive == 1)
                {
                    cbEnable.Checked = true;
                    //cbTest.Checked = true;
                }
                else
                {
                    cbEnable.Checked = false;
                    //cbTest.Checked = false;
                }
            }

            if (rid == 1)
                bindMYRtoIDR(agentid, rid, ratelev, rateback);
            else if (rid == 2)
                bindIDRtoMYR(agentid, rid, ratelev, rateback);

            
        }

        void bindMYRtoIDR(long agentid, long rid, string ratelev, string rateback)
        {
            lblRemittanceName.Text = "Malaysia to Indonesia";
            string rts = null;
            var cur = omg.getRateMYR2IDR();
            if (cur != null)
            {
                rts = cur.Rate;
            }

            List<tblM_Rate> rate = new List<tblM_Rate>();
            rate = omg.getListRateMYRIDR();
            if (rate.Count > 0)
            {
                for (int i = 0; i < rate.Count; i++)
                {
                    rate[i].Rate = (Int32.Parse(rts) - Int32.Parse(rate[i].Margin)).ToString();
                    rate[i].NameRate = rate[i].Name + " - Rp " + String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(Int32.Parse(rate[i].Rate))).Replace("Rp", "");
                    if (ratelev == rate[i].Name)
                        rateback = rate[i].RateID.ToString();
                    if (i == 0)
                        rts = rate[i].Rate;
                }
                ddlRate.DataSource = null;
                ddlRate.DataSource = rate;
                ddlRate.DataValueField = "RateID";
                ddlRate.DataTextField = "NameRate";
                ddlRate.DataBind();
            }

            var marate = db.tblM_Agent_Rate.Where(p => p.AgentID == agentid && p.RemittanceID == rid).FirstOrDefault();
            hfRateID.Value = Convert.ToString(marate.RateID);
            ddlRate.SelectedValue = hfRateID.Value;
            var mafee = db.tblM_Agent_Fee.Where(p => p.RateID == marate.ID).FirstOrDefault();
            hfFee.Value = Convert.ToString(mafee.Fee);
            txtFee.Text = hfFee.Value;

            ddlRate.Text = rateback;
        }

        void bindIDRtoMYR(long agentid, long rid, string ratelev, string rateback)
        {
            lblRemittanceName.Text = "Indonesia to Malaysia";
            string rts = null;
            var cur = omg.getRateIDRMYR();
            if (cur != null)
            {
                rts = cur.Rate;
            }

            List<tblM_Rate> rate = new List<tblM_Rate>();
            rate = omg.getListRateIDRMYR();
            if (rate.Count > 0)
            {
                for (int i = 0; i < rate.Count; i++)
                {
                    rate[i].Rate = (Int32.Parse(rts) + Int32.Parse(rate[i].Margin)).ToString();
                    rate[i].NameRate = rate[i].Name + " - Rp " + String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(Int32.Parse(rate[i].Rate))).Replace("Rp", "");
                    if (ratelev == rate[i].Name)
                        rateback = rate[i].RateID.ToString();
                    if (i == 0)
                        rts = rate[i].Rate;
                }
                ddlRate.DataSource = null;
                ddlRate.DataSource = rate;
                ddlRate.DataValueField = "RateID";
                ddlRate.DataTextField = "NameRate";
                ddlRate.DataBind();

                var marate = db.tblM_Agent_Rate.Where(p => p.AgentID == agentid && p.RemittanceID == rid).FirstOrDefault();
                hfRateID.Value = Convert.ToString(marate.RateID);
                ddlRate.SelectedValue = hfRateID.Value;
                var mafee = db.tblM_Agent_Fee.Where(p => p.RateID == marate.ID).FirstOrDefault();
                hfFee.Value = Convert.ToString(mafee.Fee);
                txtFee.Text = hfFee.Value;
            }

            ddlRate.Text = rateback;
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            int enableStatus = 0;

            if(cbEnable.Checked)
            {
                enableStatus = 1;
            }

            if (txtName.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Name cannot be empty!";
                return;
            }

            if (txtPhone.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Phone cannot be empty!";
                return;
            }

            if (hfRemittanceID.Value == "2")
            {
                if (txtFee.Text == "")
                {
                    diverror.Visible = true;
                    lblError.Text = "Admin fee cannot be empty!";
                    return;
                }

                if (Convert.ToDouble(txtFee.Text) < 7500)
                {
                    diverror.Visible = true;
                    lblError.Text = "Minimum admin fee is IDR 7500!";
                    return;
                }
            }
            else if (hfRemittanceID.Value == "1")
            {
                if (txtFee.Text == "")
                {
                    diverror.Visible = true;
                    lblError.Text = "Admin fee cannot be empty!";
                    return;
                }

                if (Convert.ToDouble(txtFee.Text) < 2.5)
                {
                    diverror.Visible = true;
                    lblError.Text = "Minimum admin fee is MYR 2.5!";
                    return;
                }
            }

            string result = "";

            var agent = db.tblM_Agent.Where(p => p.Phone == txtPhone.Text.Trim()).FirstOrDefault();
            if (agent != null)
            {
                agent.Fullname = txtName.Text.Trim();
                agent.Email = txtEmail.Text.Trim();
                long rid = Convert.ToInt64(hfRemittanceID.Value);

                var rate = db.tblM_Agent_Rate.Where(p => p.AgentID == agent.AgentID && p.RemittanceID == rid).FirstOrDefault();
                if (hfRateID.Value != ddlRate.SelectedValue)
                {
                    rate.RateID = Int64.Parse(ddlRate.SelectedValue);
                    rate.UpdatedDate = DateTime.Now;
                    result = "rate level updated to " + ddlRate.SelectedItem.Text + ", ";
                }

                if (hfFee.Value != txtFee.Text.Trim())
                {
                    var fee = db.tblM_Agent_Fee.Where(p => p.RateID == rate.ID && p.RemittanceID == rid).FirstOrDefault();
                    fee.Fee = Int64.Parse(txtFee.Text.Trim());
                    fee.DateModified = DateTime.Now;
                    fee.ModifiedBy = Convert.ToInt64(SessionLib.Current.AdminID);
                    result += "admin fee updated to " + fee.Currency + " " + txtFee.Text;
                }

                String eventData = "Updating agent [" + agent.AgentID + "]" + agent.Fullname + " by [" + SessionLib.Current.AdminID + "]" + SessionLib.Current.Name + ".";
                var eDate = DateTime.Now;

                if (agent.isActive == 0 && enableStatus == 1)
                {
                    eventData += " Enabling agent [" + agent.AgentID + "]" + agent.Fullname + " from disabled state by [" + SessionLib.Current.AdminID + "]" + SessionLib.Current.Name + ".";
                }
                else if (agent.isActive == 1 && enableStatus == 0)
                {
                    eventData += " Disabling agent [" + agent.AgentID + "]" + agent.Fullname + " from enabled state by [" + SessionLib.Current.AdminID + "]" + SessionLib.Current.Name + ".";
                }
                agent.isActive = enableStatus;

                db.SaveChanges();

                Session["SuccessAgent"] = "Successfully updated for " + agent.Fullname + " : " + result;

                tblS_Log eLog = new tblS_Log
                {
                    EventDate = eDate,
                    EventBy = Convert.ToInt64(SessionLib.Current.AdminID),
                    EventType = "administration",
                    EventData = eventData,
                };

                db.tblS_Log.Add(eLog);
                db.SaveChanges();

                Response.Redirect("~/Agent/ListAgent");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + agent.Fullname + " updated')", true);
            }
            else
            {
                diverror.Visible = true;
                lblError.Text = "No agent found with this phone number";
            }
        }

        protected void btnError_ServerClick(object sender, EventArgs e)
        {
            diverror.Visible = false;
        }

        protected void ddlRate_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}