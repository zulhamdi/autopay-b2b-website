﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(OMGLiveDemo.Startup))]
namespace OMGLiveDemo
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
