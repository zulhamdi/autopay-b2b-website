﻿using OMGLiveDemo.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo
{
    public partial class RateHistory : System.Web.UI.Page
    {
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    BindGV();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        void BindGV()
        {
            //List<tblT_Rate_History> dt = new List<tblT_Rate_History>();
            //dt = db.tblT_Rate_History.ToList();

            //List<vw_Rate_History> rh = new List<vw_Rate_History>();
            List<vw_Rate_History> rh = new List<vw_Rate_History>();

            if(ddlRemittance.SelectedValue == "0")
            {
                rh = db.vw_Rate_History.OrderByDescending(p => p.ModifiedDate).ToList();
            }
            else
            {
                long rid = Convert.ToInt64(ddlRemittance.SelectedValue);
                rh = db.vw_Rate_History.Where(p => p.RemittanceID == rid).OrderByDescending(p => p.ModifiedDate).ToList();
            }
            
            gvListItem.DataSource = null;
            gvListItem.DataSource = rh;
            gvListItem.DataBind();
        }

        protected void gvListItem_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvListItem.PageIndex = e.NewPageIndex;
            BindGV();
        }

        protected void gvListItem_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[2].Text == "1")
                {
                    e.Row.Cells[2].Text = "MYR to IDR";
                }
                else if (e.Row.Cells[2].Text == "2")
                {
                    e.Row.Cells[2].Text = "IDR 2 MYR";
                }
                else if (e.Row.Cells[2].Text == "3")
                {
                    e.Row.Cells[2].Text = "IDR 2 SAR";
                }
            }
        }

        protected void ddlRemittance_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<vw_Rate_History> rh = new List<vw_Rate_History>();

            if(ddlFilterByDate.SelectedValue == "date")
            {
                doSingleDateDisplay();
            }
            else if (ddlFilterByDate.SelectedValue == "daterange")
            {
                doDateRangeDisplay();
            }
            else
            {
                BindGV();
            }
        }

        protected void ddlFilterByDate_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(ddlFilterByDate.SelectedValue == "date")
            {
                divsingledate.Visible = true;
                divdaterange.Visible = false;
                startdate.Text = "";
                enddate.Text = "";
            }
            else if (ddlFilterByDate.SelectedValue == "daterange") {
                divsingledate.Visible = false;
                divdaterange.Visible = true;
                searchdate.Text = "";
            }
            else
            {
                divsingledate.Visible = false;
                divdaterange.Visible = false;
                startdate.Text = "";
                enddate.Text = "";
                searchdate.Text = "";

                BindGV();
            }
        }

        protected void btnSingleDate_Click(object sender, EventArgs e)
        {
            doSingleDateDisplay();
        }

        void doSingleDateDisplay()
        {
            List<vw_Rate_History> rh = new List<vw_Rate_History>();
            DateTime dS = new DateTime();
            DateTime dE = new DateTime();

            if (searchdate.Text.Trim() != "")
            {
                dS = Convert.ToDateTime(searchdate.Text.ToString().Trim());
                dE = dS.AddHours(23).AddMinutes(59).AddSeconds(59);
            }
            else
            {
                return;
            }

            if (ddlRemittance.SelectedValue == "0")
            {
                rh = db.vw_Rate_History.Where(p => p.ModifiedDate >= dS && p.ModifiedDate <= dE).OrderByDescending(o => o.ModifiedDate).ToList();
            }
            else
            {
                long rid = Convert.ToInt64(ddlRemittance.SelectedValue);
                rh = db.vw_Rate_History.Where(p => p.RemittanceID == rid && p.ModifiedDate >= dS && p.ModifiedDate <= dE).OrderByDescending(o => o.ModifiedDate).ToList();
            }

            gvListItem.DataSource = null;
            gvListItem.DataSource = rh;
            gvListItem.DataBind();
        }

        protected void btnDateRange_Click(object sender, EventArgs e)
        {
            doDateRangeDisplay();
        }

        void doDateRangeDisplay()
        {
            List<vw_Rate_History> rh = new List<vw_Rate_History>();

            DateTime dS = new DateTime();
            DateTime dE = new DateTime();

            if (startdate.Text.Trim() != "" && enddate.Text.Trim() != "")
            {
                dS = Convert.ToDateTime(startdate.Text.ToString().Trim());
                dE = Convert.ToDateTime(enddate.Text.ToString().Trim()).AddHours(23).AddMinutes(59).AddSeconds(59);
            }
            else
            {
                return;
            }

            if (ddlRemittance.SelectedValue == "0")
            {
                rh = db.vw_Rate_History.Where(p => p.ModifiedDate >= dS && p.ModifiedDate <= dE).OrderByDescending(o => o.ModifiedDate).ToList();
            }
            else
            {
                long rid = Convert.ToInt64(ddlRemittance.SelectedValue);
                rh = db.vw_Rate_History.Where(p => p.RemittanceID == rid && p.ModifiedDate >= dS && p.ModifiedDate <= dE).OrderByDescending(o => o.ModifiedDate).ToList();
            }

            gvListItem.DataSource = null;
            gvListItem.DataSource = rh;
            gvListItem.DataBind();
        }
    }
}