﻿using OMGLiveDemo.Models;
using OMGLiveDemo.Models.OMG;
using OMGLiveDemo.Scripts;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo
{
    public partial class Rate : System.Web.UI.Page
    {
        OMG omg = new OMG();
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();
        //tblM_Rate rate = new tblM_Rate();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                divsuccess.Visible = false;
                diverror.Visible = false;
                if (!IsPostBack)
                {
                    getRateID();
                }
            }
            else
            {
                Master.checkRole();
            }

            setComponentVisibility();
        }

        void setComponentVisibility()
        {
            if (SessionLib.Current.Role == Models.Roles.Admin || SessionLib.Current.Role == Models.Roles.SuperAdmin)
            {
                divUpdateMYR2IDRBaseRate.Visible = true;
                divUpdateMYR2IDRPlatinumMargin.Visible = true;
                divUpdateMYR2IDRGoldMargin.Visible = true;
                divUpdateMYR2IDRSilverMargin.Visible = true;
                divUpdateMYR2IDRBronzeMargin.Visible = true;
                divUpdateMYR2IDRCustomMargin.Visible = true;

                divUpdateIDR2MYRBaseRate.Visible = true;
                divUpdateIDR2MYRPlatinumMargin.Visible = true;
                divUpdateIDR2MYRGoldMargin.Visible = true;
                divUpdateIDR2MYRCustomMargin.Visible = true;

                divUpdateIDR2SARBaseRate.Visible = true;
                divUpdateIDR2SARPublicMargin.Visible = true;

                divUpdateMarginBtn.Visible = true;
            }
        }

        void getRateID()
        {
            try
            {
                var rt = omg.getRateIDByPartnerID();
                if (rt != null)
                {
                    for (int i = 0; i < rt.Count; i++)
                    {
                        if (rt[i].RemittanceID == 1)
                            hfRateIDMYRIDR.Value = rt[i].RateID.ToString();
                        if (rt[i].RemittanceID == 2)
                            hfRateIDIDRMYR.Value = rt[i].RateID.ToString();
                        if (rt[i].RemittanceID == 3)
                            hfRateIDIDRSAR.Value = rt[i].RateID.ToString();
                    }

                    count();
                    countIDR();
                    countIDRSAR();
                }
            }
            catch (Exception ex)
            {
            }
        }

        protected void ddlChooseRemittance_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlChooseRemittance.SelectedValue == "1")
            {
                divMYR2IDR.Visible = true;
                divIDR2MYR.Visible = false;
                divIDR2SAR.Visible = false;
            }
            else if (ddlChooseRemittance.SelectedValue == "2")
            {
                divMYR2IDR.Visible = false;
                divIDR2MYR.Visible = true;
                divIDR2SAR.Visible = false;
            }
            else if (ddlChooseRemittance.SelectedValue == "3")
            {
                divMYR2IDR.Visible = false;
                divIDR2MYR.Visible = false;
                divIDR2SAR.Visible = true;
            }
        }

        void count()
        {
            var rate = db.tblM_Rate.Where(p => p.RemittanceID == 1).ToList();
            
            var cur = omg.getRateMYR2IDR();
            if (cur != null)
            {
                hfRate.Value = cur.Rate;
                lblBaseRateMYR2IDR.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(hfRate.Value)).Replace("Rp", "Rp ");
                lblRateMYR2IDR.Text = "1 MYR = " + String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(hfRate.Value)).Replace("Rp", "") + " IDR";
                lblRateUpdateDateMYR2IDR.Text = "Last update : " + cur.DateModified.ToLongTimeString() + ", " + cur.DateModified.ToLongDateString();
            }

            lblCurrencyMYR2IDR.Text = "MYR";

            for (int i = 0; i < rate.Count; i++)
            {
                if (rate[i].Name == "Platinum")
                {
                    txtPlatinumMarginMYR2IDR.Text = rate[i].Margin;
                }
                if (rate[i].Name == "Gold")
                {
                    txtGoldMarginMYR2IDR.Text = rate[i].Margin;
                }
                if (rate[i].Name == "Silver")
                {
                    txtSilverMarginMYR2IDR.Text = rate[i].Margin;
                }
                if (rate[i].Name == "Bronze")
                {
                    txtBronzeMarginMYR2IDR.Text = rate[i].Margin;
                }
                if (rate[i].Name == "Customize")
                {
                    txtCustomizeMarginMYR2IDR.Text = rate[i].Margin;
                }
            }
            
            countRate();
        }

        void countRate()
        {
            lblPlatinumRetailRateMYR2IDR.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64((Int32.Parse(hfRate.Value) - Int32.Parse(txtPlatinumMarginMYR2IDR.Text)))).Replace("Rp", "Rp ");
            lblGoldRetailRateMYR2IDR.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64((Int32.Parse(hfRate.Value) - Int32.Parse(txtGoldMarginMYR2IDR.Text)))).Replace("Rp", "Rp ");
            lblSilverRetailRateMYR2IDR.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64((Int32.Parse(hfRate.Value) - Int32.Parse(txtSilverMarginMYR2IDR.Text)))).Replace("Rp", "Rp ");
            lblBronzeRetailRateMYR2IDR.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64((Int32.Parse(hfRate.Value) - Int32.Parse(txtBronzeMarginMYR2IDR.Text)))).Replace("Rp", "Rp ");
            lblCustomizeRetailRateMYR2IDR.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64((Int32.Parse(hfRate.Value) - Int32.Parse(txtCustomizeMarginMYR2IDR.Text)))).Replace("Rp", "Rp ");
        }

        protected void btnUpdateBaseRateMYR_Click(object sender, EventArgs e)
        {
            divsuccess.Visible = false;
            diverror.Visible = false;
            if (txtUpdateRateMYR2IDR.Text == "")
            {
                lblError.Text = "New base rate cannot be empty!";
                diverror.Visible = true;
                return;
            }

            updateRateMYR();
        }

        void updateRateMYR()
        {
            UpdateRate upr = new UpdateRate();
            upr.RateID = Convert.ToInt32(hfRateIDMYRIDR.Value);
            upr.RateBase = hfRate.Value;
            upr.Rate = txtUpdateRateMYR2IDR.Text.Trim();
            upr.ModifiedBy = Convert.ToInt32(SessionLib.Current.AdminID);

            tblT_Rate_History newRateHistory = new tblT_Rate_History();
            newRateHistory.RateID = upr.RateID;
            newRateHistory.RemittanceID = 1;
            newRateHistory.BaseRate = double.Parse(upr.RateBase);
            newRateHistory.Rate = double.Parse(upr.Rate);
            newRateHistory.ModifiedBy = upr.ModifiedBy;
            newRateHistory.ModifiedDate = DateTime.Now;

            //after this then the value in upr is already encrypted
            var update = omg.updateRateMYRIDR(upr);

            if (!update)
            {
                lblError.Text = "Failed update base rate";
                diverror.Visible = true;
                divsuccess.Visible = false;
                return;
            }
            else
            {
                lblSuccess.Text = "Success update base rate";
                divsuccess.Visible = true;
                diverror.Visible = false;

                db.tblT_Rate_History.Add(newRateHistory);
                db.SaveChanges();

                count();
                return;
            }
        }

        protected void btnError_ServerClick(object sender, EventArgs e)
        {
            diverror.Visible = false;
        }

        protected void btnSuccess_ServerClick(object sender, EventArgs e)
        {
            divsuccess.Visible = false;
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            diverror.Visible = false;

            if (ddlChooseRemittance.SelectedValue == "1")
            {
                if (txtPlatinumMarginMYR2IDR.Text == "")
                {
                    diverror.Visible = true;
                    lblError.Text = "Platinum margin cannot be empty!";
                    return;
                }
                if (txtGoldMarginMYR2IDR.Text == "")
                {
                    diverror.Visible = true;
                    lblError.Text = "Gold margin cannot be empty!";
                    return;
                }
                if (txtSilverMarginMYR2IDR.Text == "")
                {
                    diverror.Visible = true;
                    lblError.Text = "Silver margin cannot be empty!";
                    return;
                }
                if (txtBronzeMarginMYR2IDR.Text == "")
                {
                    diverror.Visible = true;
                    lblError.Text = "Bronze margin cannot be empty!";
                    return;
                }
                if (txtCustomizeMarginMYR2IDR.Text == "")
                {
                    diverror.Visible = true;
                    lblError.Text = "Customize margin cannot be empty!";
                    return;
                }

                updateMarginMYR();
            }
            else if (ddlChooseRemittance.SelectedValue == "2")
            {
                if (txtPlatinumMarginIDR2MYR.Text == "")
                {
                    diverror.Visible = true;
                    lblError.Text = "Platinum margin cannot be empty!";
                    return;
                }
                if (txtGoldMarginIDR2MYR.Text == "")
                {
                    diverror.Visible = true;
                    lblError.Text = "Gold margin cannot be empty!";
                    return;
                }
                if (txtCustomizeMarginIDR2MYR.Text == "")
                {
                    diverror.Visible = true;
                    lblError.Text = "Customize margin cannot be empty!";
                    return;
                }

                updateMarginIDR();
            }
        }

        void updateMarginMYR() {
            var rate = db.tblM_Rate.Where(p => p.RemittanceID == 1).ToList();
            if (rate != null)
            {
                for (int i = 0; i < rate.Count; i++)
                {
                    if (rate[i].Name == "Platinum")
                    {
                        rate[i].Margin = txtPlatinumMarginMYR2IDR.Text;
                        rate[i].UpdatedDate = DateTime.Now;
                        //rate.UpdatedBy = Int64.Parse(SessionLib.Current.AdminID);
                    }
                    if (rate[i].Name == "Gold")
                    {
                        rate[i].Margin = txtGoldMarginMYR2IDR.Text;
                        rate[i].UpdatedDate = DateTime.Now;
                        //rate.UpdatedBy = Int64.Parse(SessionLib.Current.AdminID);
                    }
                    if (rate[i].Name == "Silver")
                    {
                        rate[i].Margin = txtSilverMarginMYR2IDR.Text;
                        rate[i].UpdatedDate = DateTime.Now;
                        //rate.UpdatedBy = Int64.Parse(SessionLib.Current.AdminID);
                    }
                    if (rate[i].Name == "Bronze")
                    {
                        rate[i].Margin = txtBronzeMarginMYR2IDR.Text;
                        rate[i].UpdatedDate = DateTime.Now;
                        //rate.UpdatedBy = Int64.Parse(SessionLib.Current.AdminID);
                    }
                    if (rate[i].Name == "Customize")
                    {
                        rate[i].Margin = txtCustomizeMarginMYR2IDR.Text;
                        rate[i].UpdatedDate = DateTime.Now;
                        //rate.UpdatedBy = Int64.Parse(SessionLib.Current.AdminID);
                    }
                }
                db.SaveChanges();

                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Success update all margins in Malaysia to Indonesia Remittance')", true);
                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Success update margin become " + String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(txtMargin.Text)).Replace("Rp", "Rp ") + "')", true);
            }
        }

        protected void txtMargin_TextChanged(object sender, EventArgs e)
        {
            
        }

        protected void txtPlatinumMargin_TextChanged(object sender, EventArgs e)
        {
            if (txtPlatinumMarginMYR2IDR.Text != "")
            {
                lblPlatinumRetailRateMYR2IDR.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64((Int32.Parse(hfRate.Value) - Int32.Parse(txtPlatinumMarginMYR2IDR.Text)))).Replace("Rp", "Rp ");
            }
            else
            {
                lblPlatinumRetailRateMYR2IDR.Text = "Undefined";
            }
        }

        protected void txtGoldMargin_TextChanged(object sender, EventArgs e)
        {
            if (txtGoldMarginMYR2IDR.Text != "")
            {
                lblGoldRetailRateMYR2IDR.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64((Int32.Parse(hfRate.Value) - Int32.Parse(txtGoldMarginMYR2IDR.Text)))).Replace("Rp", "Rp ");
            }
            else
            {
                lblGoldRetailRateMYR2IDR.Text = "Undefined";
            }
        }

        protected void txtSilverMargin_TextChanged(object sender, EventArgs e)
        {
            if (txtSilverMarginMYR2IDR.Text != "")
            {
                lblSilverRetailRateMYR2IDR.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64((Int32.Parse(hfRate.Value) - Int32.Parse(txtSilverMarginMYR2IDR.Text)))).Replace("Rp", "Rp ");
            }
            else
            {
                lblSilverRetailRateMYR2IDR.Text = "Undefined";
            }
        }

        protected void txtBronzeMargin_TextChanged(object sender, EventArgs e)
        {
            if (txtBronzeMarginMYR2IDR.Text != "")
            {
                lblBronzeRetailRateMYR2IDR.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64((Int32.Parse(hfRate.Value) - Int32.Parse(txtBronzeMarginMYR2IDR.Text)))).Replace("Rp", "Rp ");
            }
            else
            {
                lblBronzeRetailRateMYR2IDR.Text = "Undefined";
            }
        }

        protected void txtCustomizeMargin_TextChanged(object sender, EventArgs e)
        {
            if (txtCustomizeMarginMYR2IDR.Text != "")
            {
                lblCustomizeRetailRateMYR2IDR.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64((Int32.Parse(hfRate.Value) - Int32.Parse(txtCustomizeMarginMYR2IDR.Text)))).Replace("Rp", "Rp ");
            }
            else
            {
                lblCustomizeRetailRateMYR2IDR.Text = "Undefined";
            }
        }


        //========================================================== Indonesia to Malaysia =============================================================================//
        //========================================================== Indonesia to Malaysia =============================================================================//
        //========================================================== Indonesia to Malaysia =============================================================================//
        //========================================================== Indonesia to Malaysia =============================================================================//
        //========================================================== Indonesia to Malaysia =============================================================================//
        //========================================================== Indonesia to Malaysia =============================================================================//


        void countIDR()
        {
            var rate = db.tblM_Rate.Where(p => p.RemittanceID == 2).ToList();

            var cur = omg.getRateIDRMYR();
            if (cur != null)
            {
                hfRateIDR.Value = cur.Rate;
                lblBaseRateIDR2MYR.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(hfRateIDR.Value)).Replace("Rp", "Rp ");
                lblRateIDR2MYR.Text = "1 MYR = " + String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(hfRateIDR.Value)).Replace("Rp", "") + " IDR";
                lblRateUpdateDateIDR2MYR.Text = "Last update : " + cur.DateModified.ToLongTimeString() + ", " + cur.DateModified.ToLongDateString();
            }

            lblCurrencyIDR2MYR.Text = "IDR";

            for (int i = 0; i < rate.Count; i++)
            {
                if (rate[i].Name == "Platinum")
                {
                    txtPlatinumMarginIDR2MYR.Text = rate[i].Margin;
                }
                if (rate[i].Name == "Gold")
                {
                    txtGoldMarginIDR2MYR.Text = rate[i].Margin;
                }
                if (rate[i].Name == "Public")
                {
                    txtCustomizeMarginIDR2MYR.Text = rate[i].Margin;
                }
            }

            countRateIDR();
        }

        void countRateIDR()
         {
            lblPlatinumRetailRateIDR2MYR.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64((Int32.Parse(hfRateIDR.Value) + Int32.Parse(txtPlatinumMarginIDR2MYR.Text)))).Replace("Rp", "Rp ");
            lblGoldRetailRateIDR2MYR.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64((Int32.Parse(hfRateIDR.Value) + Int32.Parse(txtGoldMarginIDR2MYR.Text)))).Replace("Rp", "Rp ");
            lblCustomizeRetailRateIDR2MYR.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64((Int32.Parse(hfRateIDR.Value) + Int32.Parse(txtCustomizeMarginIDR2MYR.Text)))).Replace("Rp", "Rp ");
        }

        protected void btnUpdateBaseRateIDR_Click(object sender, EventArgs e)
        {
            divsuccess.Visible = false;
            diverror.Visible = false;
            if (txtUpdateRateIDR2MYR.Text == "")
            {
                lblError.Text = "New base rate cannot be empty!";
                diverror.Visible = true;
                return;
            }

            updateRateIDR();
        }

        void updateRateIDR()
        {
            UpdateRate upr = new UpdateRate();
            upr.RateID = Convert.ToInt32(hfRateIDIDRMYR.Value);
            upr.RateBase = hfRateIDR.Value;
            upr.Rate = txtUpdateRateIDR2MYR.Text.Trim();
            upr.ModifiedBy = Convert.ToInt32(SessionLib.Current.AdminID);

            tblT_Rate_History newRateHistory = new tblT_Rate_History();
            newRateHistory.RateID = upr.RateID;
            newRateHistory.RemittanceID = 2;
            newRateHistory.BaseRate = double.Parse(upr.RateBase);
            newRateHistory.Rate = double.Parse(upr.Rate);
            newRateHistory.ModifiedBy = upr.ModifiedBy;
            newRateHistory.ModifiedDate = DateTime.Now;

            var update = omg.updateRateMYRIDR(upr);

            if (!update)
            {
                lblError.Text = "Failed update base rate";
                diverror.Visible = true;
                divsuccess.Visible = false;
                return;
            }
            else
            {
                lblSuccess.Text = "Success update base rate";
                divsuccess.Visible = true;
                diverror.Visible = false;

                db.tblT_Rate_History.Add(newRateHistory);
                db.SaveChanges();

                countIDR();
                return;
            }
        }

        protected void txtPlatinumMarginIDR_TextChanged(object sender, EventArgs e)
        {
            if (txtPlatinumMarginIDR2MYR.Text != "")
            {
                lblPlatinumRetailRateIDR2MYR.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64((Int32.Parse(hfRateIDR.Value) + Int32.Parse(txtPlatinumMarginIDR2MYR.Text)))).Replace("Rp", "Rp ");
            }
            else
            {
                lblPlatinumRetailRateIDR2MYR.Text = "Undefined";
            }
        }

        protected void txtGoldMarginIDR_TextChanged(object sender, EventArgs e)
        {
            if (txtGoldMarginIDR2MYR.Text != "")
            {
                lblGoldRetailRateIDR2MYR.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64((Int32.Parse(hfRateIDR.Value) + Int32.Parse(txtGoldMarginIDR2MYR.Text)))).Replace("Rp", "Rp ");
            }
            else
            {
                lblGoldRetailRateIDR2MYR.Text = "Undefined";
            }
        }

        protected void txtCustomizeMarginIDR_TextChanged(object sender, EventArgs e)
        {
            if (txtCustomizeMarginIDR2MYR.Text != "")
            {
                lblCustomizeRetailRateIDR2MYR.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64((Int32.Parse(hfRateIDR.Value) + Int32.Parse(txtCustomizeMarginIDR2MYR.Text)))).Replace("Rp", "Rp ");
            }
            else
            {
                lblCustomizeRetailRateIDR2MYR.Text = "Undefined";
            }
        }

        void updateMarginIDR()
        {
            var rate = db.tblM_Rate.Where(p => p.RemittanceID == 2).ToList();
            if (rate != null)
            {
                for (int i = 0; i < rate.Count; i++)
                {
                    if (rate[i].Name == "Platinum")
                    {
                        rate[i].Margin = txtPlatinumMarginIDR2MYR.Text;
                        rate[i].UpdatedDate = DateTime.Now;
                        //rate.UpdatedBy = Int64.Parse(SessionLib.Current.AdminID);
                    }
                    if (rate[i].Name == "Gold")
                    {
                        rate[i].Margin = txtGoldMarginIDR2MYR.Text;
                        rate[i].UpdatedDate = DateTime.Now;
                        //rate.UpdatedBy = Int64.Parse(SessionLib.Current.AdminID);
                    }
                    if (rate[i].Name == "Customize")
                    {
                        rate[i].Margin = txtCustomizeMarginIDR2MYR.Text;
                        rate[i].UpdatedDate = DateTime.Now;
                        //rate.UpdatedBy = Int64.Parse(SessionLib.Current.AdminID);
                    }
                }
                db.SaveChanges();

                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Success update all margins in Indonesia to Malaysia Remittance')", true);
                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Success update margin become " + String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(txtMargin.Text)).Replace("Rp", "Rp ") + "')", true);
            }
        }

        //========================================================== Indonesia to Saudi Arabia =============================================================================//
        //========================================================== Indonesia to Saudi Arabia =============================================================================//
        //========================================================== Indonesia to Saudi Arabia =============================================================================//
        //========================================================== Indonesia to Saudi Arabia =============================================================================//
        //========================================================== Indonesia to Saudi Arabia =============================================================================//
        //========================================================== Indonesia to Saudi Arabia =============================================================================//


        void countIDRSAR()
        {
            var rate = db.tblM_Rate.Where(p => p.RemittanceID == 3).ToList();

            var cur = omg.getRateIDRSAR();
            if (cur != null)
            {
                hfRateSAR.Value = cur.Rate;
                lblBaseRateIDR2SAR.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(hfRateSAR.Value)).Replace("Rp", "Rp ");
                lblRateIDR2SAR.Text = "1 SAR = " + String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(hfRateSAR.Value)).Replace("Rp", "") + " IDR";
                lblRateUpdateDateIDR2SAR.Text = "Last update : " + cur.DateModified.ToLongTimeString() + ", " + cur.DateModified.ToLongDateString();
            }

            lblCurrencyIDR2SAR.Text = "IDR";

            for (int i = 0; i < rate.Count; i++)
            {
                if (rate[i].Name == "Public")
                {
                    txtPublicMarginIDR2SAR.Text = rate[i].Margin;
                }
            }

            countRateIDRSAR();
        }

        void countRateIDRSAR()
        {
            lblPublicRetailRateIDR2SAR.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64((Int32.Parse(hfRateSAR.Value) + Int32.Parse(txtPublicMarginIDR2SAR.Text)))).Replace("Rp", "Rp ");
        }

        protected void btnUpdateBaseRateSAR_Click(object sender, EventArgs e)
        {
            divsuccess.Visible = false;
            diverror.Visible = false;
            if (txtUpdateRateIDR2SAR.Text == "")
            {
                lblError.Text = "New base rate cannot be empty!";
                diverror.Visible = true;
                return;
            }

            updateRateIDRSAR();
        }

        void updateRateIDRSAR()
        {
            UpdateRate upr = new UpdateRate();
            upr.RateID = Convert.ToInt32(hfRateIDIDRSAR.Value);
            upr.RateBase = hfRateSAR.Value;
            upr.Rate = txtUpdateRateIDR2SAR.Text.Trim();
            upr.ModifiedBy = Convert.ToInt32(SessionLib.Current.AdminID);

            tblT_Rate_History newRateHistory = new tblT_Rate_History();
            newRateHistory.RateID = upr.RateID;
            newRateHistory.RemittanceID = 2;
            newRateHistory.BaseRate = double.Parse(upr.RateBase);
            newRateHistory.Rate = double.Parse(upr.Rate);
            newRateHistory.ModifiedBy = upr.ModifiedBy;
            newRateHistory.ModifiedDate = DateTime.Now;

            var update = omg.updateRateMYRIDR(upr);

            if (!update)
            {
                lblError.Text = "Failed update base rate";
                diverror.Visible = true;
                divsuccess.Visible = false;
                return;
            }
            else
            {
                lblSuccess.Text = "Success update base rate";
                divsuccess.Visible = true;
                diverror.Visible = false;

                db.tblT_Rate_History.Add(newRateHistory);
                db.SaveChanges();

                countIDRSAR();
                return;
            }
        }

        protected void txtPublicMarginSAR_TextChanged(object sender, EventArgs e)
        {
            if (txtPublicMarginIDR2SAR.Text != "")
            {
                lblPublicRetailRateIDR2SAR.Text = String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64((Int32.Parse(hfRateSAR.Value) + Int32.Parse(txtPublicMarginIDR2SAR.Text)))).Replace("Rp", "Rp ");
            }
            else
            {
                lblPublicRetailRateIDR2SAR.Text = "Undefined";
            }
        }

        void updateMarginIDRSAR()
        {
            var rate = db.tblM_Rate.Where(p => p.RemittanceID == 3).ToList();
            if (rate != null)
            {
                for (int i = 0; i < rate.Count; i++)
                {
                    if (rate[i].Name == "Public")
                    {
                        rate[i].Margin = txtPublicMarginIDR2SAR.Text;
                        rate[i].UpdatedDate = DateTime.Now;
                        //rate.UpdatedBy = Int64.Parse(SessionLib.Current.AdminID);
                    }
                }
                db.SaveChanges();

                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Success update all margins in Indonesia to Saudi Arabia Remittance')", true);
                //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Success update margin become " + String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(txtMargin.Text)).Replace("Rp", "Rp ") + "')", true);
            }
        }
    }
}