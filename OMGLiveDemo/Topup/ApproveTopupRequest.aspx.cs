﻿using OMGLiveDemo.Models;
using OMGLiveDemo.Models.OMG;
using OMGLiveDemo.Scripts;
using Oppal.Sec;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.Topup
{
    public partial class ApproveTopupRequest : System.Web.UI.Page
    {
        OMG omg = new OMG();
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    bind();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        void bind()
        {
            hfTopupID.Value = Session["TopupID"].ToString();
            hfTopupName.Value = Session["TopupName"].ToString();
            var id = hfTopupID.Value;
            var tup = db.vw_Topup.Where(p => p.TopupID == id).FirstOrDefault();
            if (hfTopupName.Value == "Master Area")
                txtName.Text = db.tblM_Master_Area.Where(p => p.MasterAreaID == tup.UserID).Select(p => p.Fullname).FirstOrDefault();
            else if (hfTopupName.Value == "Travel Agent")
                txtName.Text = db.tblM_Travel_Agent.Where(p => p.TravelAgentID == tup.UserID).Select(p => p.Name).FirstOrDefault();
            lblTotalAmount.Text = "Rp " + String.Format(new CultureInfo("id-ID"), "{0:n}", tup.TotalAmount).Replace("Rp", "Rp ");
            txtBankName.Text = tup.BankName;
            txtAccount.Text = tup.Account;
            txtAccountHolder.Text = tup.AccountHolder;
            hfCountryID.Value = tup.CountryID.ToString();
            hfTopupReceiptURL.Value = tup.ReceiptURL;
            hfMasterAreaID.Value = Convert.ToString(tup.UserID);
            hfBalanceID.Value = Convert.ToString(tup.BalanceID);
        }

        protected void lkbViewImage_Click(object sender, EventArgs e)
        {
            if (hfTopupReceiptURL.Value != null)
            {
                Session["ImageURL"] = hfTopupReceiptURL.Value;
                Session["From"] = "Image";
                string txt = lblTotalAmount.Text;
                Session["Text"] = txtName.Text + " - " + txt;
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "colorBox", "var isColorbox = true", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('File not found!')", true);
            }
        }

        protected void btnError_ServerClick(object sender, EventArgs e)
        {
            diverror.Visible = false;
        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            diverror.Visible = false;

            try
            {

                var adminid = Convert.ToInt64(SessionLib.Current.AdminID);
                var id = hfTopupID.Value;

                
                var aaa = db.tblT_Topup.Where(p => p.TopupID == id).FirstOrDefault();
                aaa.Status = "approved";
                aaa.ApprovedBy = adminid;
                aaa.ApprovedDate = DateTime.Now;
                aaa.ModifiedDate = aaa.ApprovedDate;

                TopupModel tup = new TopupModel();
                tup.Amount = aaa.Amount.Value;
                tup.Notes = hfTopupName.Value  + " topup : " + aaa.UserID + " - " + aaa.TopupID;
                if (omg.createAndApproveTopup(tup))
                {

                    db.SaveChanges();

                    var balid = Convert.ToInt64(hfBalanceID.Value);
                    var maid = Convert.ToInt64(hfMasterAreaID.Value);

                    var bal = db.tblM_Master_Area_Balance.Where(p => p.MasterAreaID == maid && p.BalanceID == balid).FirstOrDefault();
                    var balpref = bal.Balance;
                    bal.Balance += aaa.TotalAmount;
                    bal.UpdatedBy = adminid;
                    bal.UpdatedDate = DateTime.Now;
                    db.SaveChanges();

                    var bacc = db.vw_Bank_Account.Where(p => p.BankAccountID == aaa.BankAccountID).FirstOrDefault();

                    tblH_Master_Area_Balance_Update hbal = new tblH_Master_Area_Balance_Update();
                    hbal.MasterAreaID = Convert.ToInt64(hfMasterAreaID.Value);
                    hbal.BalanceID = Convert.ToInt64(hfBalanceID.Value);
                    hbal.RemittanceID = 0;
                    hbal.Reference = aaa.TopupID;
                    hbal.Type = "Topup";
                    hbal.BankID = bacc.BankID;
                    hbal.Beneficiary = bacc.AccountHolder;
                    hbal.Account = bacc.Account;
                    hbal.BankName = bacc.BankName;
                    hbal.BalancePrevious = balpref;
                    hbal.Amount = aaa.Amount;
                    hbal.AdminFee = aaa.AdminFee;
                    hbal.Balance = bal.Balance;
                    hbal.DateCreated = DateTime.Now;
                    db.tblH_Master_Area_Balance_Update.Add(hbal);
                    db.SaveChanges();


                    Session["SuccessApproveTopup"] = "Success approve topup : " + hfTopupID.Value;
                    if (hfTopupName.Value == "Master Area")
                        Response.Redirect("~/MasterArea/PendingTopupRequest");
                    else if (hfTopupName.Value == "Travel Agent")
                        Response.Redirect("~/TravelAgent/PendingTopupRequest");

                }
                else
                {
                    diverror.Visible = true;
                    lblError.Text = "Failed create and approve topup in partner.malindogateway.com. Please ask IT for more.";
                    return;
                }
            }
            catch (Exception ex)
            {
                diverror.Visible = true;
                lblError.Text = ex.Message;
                return;
            }
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            diverror.Visible = false;
            if (txtRejectReason.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Please fill the reason why this topup request rejected";
                return;
            }

            var id = hfTopupID.Value;
            var aaa = db.tblT_Topup.Where(p => p.TopupID == id).FirstOrDefault();
            aaa.Status = "rejected";
            aaa.RejectedBy = Convert.ToInt64(SessionLib.Current.AdminID);
            aaa.RejectedDate = DateTime.Now;
            aaa.StatusMessage = txtRejectReason.Text.Trim();
            db.SaveChanges();


            Session["SuccessApproveSender"] = "Success reject topup request for Topup ID " + hfTopupID.Value + " : " + txtRejectReason.Text;
            Response.Redirect("~/MasterArea/PendingTopupRequest");
        }
    }
}