﻿using OMGLiveDemo.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.Topup
{
    public partial class TopupHistory : System.Web.UI.Page
    {
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    BindGV();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        void BindGV()
        {
            var id = Convert.ToInt64(SessionLib.Current.AdminID);
            if (Session["SuccessTopup"] != null)
            {
                lblAlertSuccess.Text = Session["SuccessTopup"].ToString();
                divsuccess.Visible = true;
                Session["SuccessTopup"] = null;
            }

            if (Session["ErrorTopup"] != null)
            {
                lblAlertFailed.Text = Session["ErrorTopup"].ToString();
                divfailed.Visible = true;
                Session["ErrorTopup"] = null;
            }

            var dt = new List<vw_Topup>();

            if (SessionLib.Current.Role == Roles.MasterArea)
            {
                dt = db.vw_Topup.Where(p => p.UserType == "MA" & p.UserID == id).ToList();
            }
            else if (SessionLib.Current.Role == Roles.MasterArea)
            {
                dt = db.vw_Topup.Where(p => p.UserType == "TA" & p.UserID == id).ToList();
            }

            for (int i = 0; i < dt.Count; i++)
            {
                dt[i].AmountText = "IDR " + String.Format(new CultureInfo("id-ID"), "{0:n}", Convert.ToDouble(dt[i].Amount)).Replace("Rp", "Rp ");
                if (SessionLib.Current.Role == Roles.MasterArea)
                {
                    dt[i].Fullname = db.tblM_Master_Area.Where(p => p.MasterAreaID == id).Select(p => p.Fullname).FirstOrDefault();
                }
                else if (SessionLib.Current.Role == Roles.MasterArea)
                {
                    dt[i].Fullname = db.tblM_Travel_Agent.Where(p => p.TravelAgentID == id).Select(p => p.Name).FirstOrDefault();
                }
            }
            gvListItem.DataSource = dt;
            gvListItem.DataBind();
        }

        protected void btnAddAgent_Click(object sender, EventArgs e)
        {

        }

        protected void lblEdit_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            Session["AgentID"] = ((HiddenField)row.FindControl("hfAgentID")).Value;
            Session["RateLevel"] = row.Cells[3].Text;
            Response.Redirect("UpdateAgent");
        }

        protected void btnAlertSuccess_ServerClick(object sender, EventArgs e)
        {
            divsuccess.Visible = false;
        }

        protected void btnAlertFailed_ServerClick(object sender, EventArgs e)
        {
            divfailed.Visible = false;
        }

        protected void gvListItem_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvListItem.PageIndex = e.NewPageIndex;
            BindGV();
        }
    }
}