//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OMGLiveDemo
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblM_Master_Area
    {
        public long MasterAreaID { get; set; }
        public string Fullname { get; set; }
        public string Password { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public Nullable<int> isActive { get; set; }
        public Nullable<System.DateTime> JoinDate { get; set; }
        public Nullable<long> AddedBy { get; set; }
    }
}
