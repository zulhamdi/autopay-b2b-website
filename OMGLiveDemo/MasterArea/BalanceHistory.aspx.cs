﻿using OMGLiveDemo.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.MasterArea
{
    public partial class BalanceHistory : System.Web.UI.Page
    {
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    BindGV();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        void BindGV()
        {
            var id = Convert.ToInt64(SessionLib.Current.AdminID);
            var dt = db.tblH_Master_Area_Balance_Update.Where(p => p.MasterAreaID == id).ToList();
            gvListItem.DataSource = dt;
            gvListItem.DataBind();
        }

        protected void btnAddAgent_Click(object sender, EventArgs e)
        {

        }

        protected void lblEdit_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            Session["AgentID"] = ((HiddenField)row.FindControl("hfAgentID")).Value;
            Session["RateLevel"] = row.Cells[3].Text;
            Response.Redirect("UpdateAgent");
        }
    }
}