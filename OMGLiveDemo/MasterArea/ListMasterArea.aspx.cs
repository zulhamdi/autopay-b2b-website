﻿using OMGLiveDemo.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.MasterArea
{
    public partial class ListMasterArea : System.Web.UI.Page
    {
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    BindGV();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        void BindGV()
        {
            if (Session["SuccessMasterArea"] != null)
            {
                lblAlertSuccess.Text = Session["SuccessMasterArea"].ToString();
                divsuccess.Visible = true;
                Session["SuccessMasterArea"] = null;
            }

            if (Session["ErrorMasterArea"] != null)
            {
                lblAlertFailed.Text = Session["ErrorMasterArea"].ToString();
                divfailed.Visible = true;
                Session["ErrorMasterArea"] = null;
            }

            var rid = Convert.ToInt64(ddlRemittanceLine.SelectedValue);
            gvListItem.DataSource = null;
            var dt = db.vw_Master_Area.Where(p => p.RemittanceID == rid).ToList();
            for (int i = 0; i < dt.Count; i++)
            {
                if (dt[i].RemittanceID == 1)
                    dt[i].BalanceText = "IDR " + String.Format(new CultureInfo("id-ID"), "{0:n}", dt[i].Balance).Replace("Rp", "Rp ");
                else if (dt[i].RemittanceID == 2)
                    dt[i].BalanceText = "MYR " + String.Format(new CultureInfo("id-ID"), "{0:n}", dt[i].Balance).Replace("Rp", "Rp ");
            }
            gvListItem.DataSource = dt;
            gvListItem.DataBind();
        }

        protected void btnAddAgent_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddMasterArea");
        }

        protected void lblEdit_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            Session["MasterAreaID"] = ((HiddenField)row.FindControl("hfMasterAreaID")).Value;
            Session["RemittanceID"] = ((HiddenField)row.FindControl("hfRemittanceID")).Value;
            Session["RateLevel"] = row.Cells[3].Text;
            Response.Redirect("UpdateMasterArea");
        }

        protected void btnAlertFailed_ServerClick(object sender, EventArgs e)
        {
            divfailed.Visible = false;
        }

        protected void btnAlertSuccess_ServerClick(object sender, EventArgs e)
        {
            divsuccess.Visible = false;
        }

        protected void ddlRemittanceLine_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindGV();
        }
    }
}