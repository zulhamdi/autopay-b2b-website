﻿using OMGLiveDemo.Models;
using OMGLiveDemo.Models.OMG;
using OMGLiveDemo.Scripts;
using Oppal.Sec;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.MasterArea
{
    public partial class AddMasterArea : System.Web.UI.Page
    {
        OMG omg = new OMG();
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    bind();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        void bind()
        {
            bindMYRtoIDR();
            bindIDRtoMYR();
        }

        void bindMYRtoIDR()
        {
            string rts = null;
            var cur = omg.getRateMYR2IDR();
            if (cur != null)
            {
                rts = cur.Rate;
            }

            List<tblM_Rate> rate = new List<tblM_Rate>();
            rate = omg.getListRateMYRIDR();
            if (rate.Count > 0)
            {
                for (int i = 0; i < rate.Count; i++)
                {
                    rate[i].Rate = (Int32.Parse(rts) - Int32.Parse(rate[i].Margin)).ToString();
                    rate[i].NameRate = rate[i].Name + " - Rp " + String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(Int32.Parse(rate[i].Rate))).Replace("Rp", "");
                    if (i == 0)
                        rts = rate[i].Rate;
                }
                ddlRate.DataSource = null;
                ddlRate.DataSource = rate;
                ddlRate.DataValueField = "RateID";
                ddlRate.DataTextField = "NameRate";
                ddlRate.DataBind();
            }
        }

        void bindIDRtoMYR()
        {
            string rts = null;
            var cur = omg.getRateIDRMYR();
            if (cur != null)
            {
                rts = cur.Rate;
            }

            List<tblM_Rate> rate = new List<tblM_Rate>();
            rate = omg.getListRateIDRMYR();
            if (rate.Count > 0)
            {
                for (int i = 0; i < rate.Count; i++)
                {
                    rate[i].Rate = (Int32.Parse(rts) + Int32.Parse(rate[i].Margin)).ToString();
                    rate[i].NameRate = rate[i].Name + " - Rp " + String.Format(new CultureInfo("id-ID"), "{0:c}", Convert.ToInt64(Int32.Parse(rate[i].Rate))).Replace("Rp", "");
                    if (i == 0)
                        rts = rate[i].Rate;
                }
                ddlRateIDR.DataSource = null;
                ddlRateIDR.DataSource = rate;
                ddlRateIDR.DataValueField = "RateID";
                ddlRateIDR.DataTextField = "NameRate";
                ddlRateIDR.DataBind();
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            if (txtName.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Name cannot be empty!";
                return;
            }

            if (txtPassword.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Password cannot be empty!";
                return;
            }

            if (txtPhone.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Phone cannot be empty!";
                return;
            }

            if (txtEmail.Text == "")
            {
                diverror.Visible = true;
                lblError.Text = "Email cannot be empty!";
                return;
            }

            if (ddlMYRtoIDR.SelectedValue == "no" && ddlIDRtoMYR.SelectedValue == "no")
            {
                diverror.Visible = true;
                lblError.Text = "You should choose minimum one remittance line to add master area!";
                return;
            }

            if (ddlMYRtoIDR.SelectedValue == "yes")
            {
                if (Convert.ToDouble(txtFee.Text) < 7500)
                {
                    diverror.Visible = true;
                    lblError.Text = "Minimum admin fee is IDR 7500!";
                    return;
                }

                if (txtFee.Text == "")
                {
                    diverror.Visible = true;
                    lblError.Text = "Admin fee cannot be empty!";
                    return;
                }
            }

            if (ddlIDRtoMYR.SelectedValue == "yes")
            {

                if (Convert.ToDouble(txtFeeIDR.Text) < 7500)
                {
                    diverror.Visible = true;
                    lblError.Text = "Minimum admin fee is IDR 7500!";
                    return;
                }

                if (txtFeeIDR.Text == "")
                {
                    diverror.Visible = true;
                    lblError.Text = "Admin fee cannot be empty!";
                    return;
                }
            }

            var adm = db.tblM_Admin.Where(p => p.Phone == txtPhone.Text.Trim() && p.isActive == 1).FirstOrDefault();
            var check = db.tblM_Agent.Where(p => p.Phone == txtPhone.Text.Trim() && p.isActive == 1).FirstOrDefault();
            var checkma = db.tblM_Master_Area.Where(p => p.Phone == txtPhone.Text.Trim() && p.isActive == 1).FirstOrDefault();
            var checkad = db.tblM_Admin.Where(p => p.Phone == txtPhone.Text.Trim() && p.isActive == 1).FirstOrDefault();
            if (check == null && checkma == null && adm == null)
            {
                long adminid = Convert.ToInt64(SessionLib.Current.AdminID);

                tblM_Master_Area bnf = new tblM_Master_Area();
                bnf.Fullname = txtName.Text.Trim();
                bnf.Phone = txtPhone.Text.Trim();
                bnf.Password = Crypto.EncryptDB(txtPassword.Text.Trim());
                bnf.Email = txtEmail.Text;
                bnf.JoinDate = DateTime.Now;
                bnf.AddedBy = adminid;
                bnf.Email = txtEmail.Text.Trim();
                bnf.isActive = 1;
                db.tblM_Master_Area.Add(bnf);
                db.SaveChanges();

                if (ddlMYRtoIDR.SelectedValue == "yes")
                {
                    createMYRtoIDR(adminid, bnf);
                }
                if (ddlIDRtoMYR.SelectedValue == "yes")
                {
                    createIDRtoMYR(adminid, bnf);
                }

                Response.Redirect("~/MasterArea/ListMasterArea");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" + bnf.Fullname + " added to database')", true);
            }
            else
            {
                diverror.Visible = true;
                lblError.Text = "This phone number already registered";
            }
        }

        void createMYRtoIDR(long adminid, tblM_Master_Area bnf)
        {
            tblM_Master_Area_Balance bal = new tblM_Master_Area_Balance();
            bal.MasterAreaID = bnf.MasterAreaID;
            bal.CountryID = 103;
            bal.Currency = "IDR";
            bal.Balance = 0;
            bal.isActive = 1;
            bal.CreatedBy = adminid;
            bal.CreatedDate = DateTime.Now;
            db.tblM_Master_Area_Balance.Add(bal);
            db.SaveChanges();

            tblM_Master_Area_Rate art = new tblM_Master_Area_Rate();
            art.MasterAreaID = bnf.MasterAreaID;
            art.RemittanceID = 1;
            art.RateID = Int64.Parse(ddlRate.SelectedValue);
            art.Margin = "0";
            art.CreatedDate = bnf.JoinDate;
            art.UpdatedDate = bnf.JoinDate;
            art.CreatedBy = bnf.AddedBy;
            db.tblM_Master_Area_Rate.Add(art);
            db.SaveChanges();

            tblM_Master_Area_Fee fee = new tblM_Master_Area_Fee();
            fee.PartnerID = bnf.MasterAreaID;
            fee.RateID = art.ID;
            fee.RemittanceID = 1;
            fee.Currency = "IDR";
            fee.Fee = float.Parse(txtFee.Text);
            fee.isActive = 1;
            fee.DateCreated = DateTime.Now;
            db.tblM_Master_Area_Fee.Add(fee);
            db.SaveChanges();
        }

        void createIDRtoMYR(long adminid, tblM_Master_Area bnf)
        {
            tblM_Master_Area_Balance bal = new tblM_Master_Area_Balance();
            bal.MasterAreaID = bnf.MasterAreaID;
            bal.CountryID = 133;
            bal.Currency = "MYR";
            bal.Balance = 0;
            bal.isActive = 1;
            bal.CreatedBy = adminid;
            bal.CreatedDate = DateTime.Now;
            db.tblM_Master_Area_Balance.Add(bal);
            db.SaveChanges();

            tblM_Master_Area_Rate art = new tblM_Master_Area_Rate();
            art.MasterAreaID = bnf.MasterAreaID;
            art.RemittanceID = 2;
            art.RateID = Int64.Parse(ddlRateIDR.SelectedValue);
            art.Margin = "0";
            art.CreatedDate = bnf.JoinDate;
            art.UpdatedDate = bnf.JoinDate;
            art.CreatedBy = bnf.AddedBy;
            db.tblM_Master_Area_Rate.Add(art);
            db.SaveChanges();

            tblM_Master_Area_Fee fee = new tblM_Master_Area_Fee();
            fee.PartnerID = bnf.MasterAreaID;
            fee.RateID = art.ID;
            fee.RemittanceID = 2;
            fee.Currency = "IDR";
            fee.Fee = float.Parse(txtFeeIDR.Text);
            fee.isActive = 1;
            fee.DateCreated = DateTime.Now;
            db.tblM_Master_Area_Fee.Add(fee);
            db.SaveChanges();
        }

        protected void btnError_ServerClick(object sender, EventArgs e)
        {
            diverror.Visible = false;
        }

        protected void ddlRate_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void ddlMYRtoIDR_SelectedIndexChanged(object sender, EventArgs e)
        {
            checkRate();
        }

        protected void ddlIDRtoMYR_SelectedIndexChanged(object sender, EventArgs e)
        {
            checkRate();
        }

        void checkRate()
        {
            if (ddlMYRtoIDR.SelectedValue == "yes")
                divmyrtoidr.Visible = true;
            else
                divmyrtoidr.Visible = false;

            if (ddlIDRtoMYR.SelectedValue == "yes")
                dividrtomyr.Visible = true;
            else
                dividrtomyr.Visible = false;
        }

        protected void ddlRateIDR_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}