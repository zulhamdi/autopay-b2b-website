﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TopupRequest.aspx.cs" Inherits="OMGLiveDemo.MasterArea.TopupRequest" MaintainScrollPositionOnPostback="true" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" />

    <div class="row">
        <div class="row x_title">
            <div class="col-md-12">
                <h3>Topup Request</h3>
            </div>
        </div>

        <div class="col-md-3 col-sm-12 col-xs-12"></div>
        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="dashboard_graph">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div style="width: 100%;">
                        <div id="canvas_dahs" class="demo-placeholder" style="width: 100%; height: 100%;">
                            <div class="form-group" style="height: 30px"></div>

                            <div class="alert alert-danger alert-dismissible fade in" runat="server" id="diverror" visible="false">
                                <button type="button" runat="server" id="btnError" onserverclick="btnError_ServerClick" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <center><asp:Label runat="server" ID="lblError" /></center>
                            </div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="Label2" CssClass="control-label" Text="Master Area Name *" />
                                <asp:DropDownList runat="server" ID="ddlMasterArea" CssClass="form-control" OnSelectedIndexChanged="ddlMasterArea_SelectedIndexChanged" AutoPostBack="true" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group" runat="server">
                                <asp:Label runat="server" ID="lblBalance" CssClass="control-label" Text="Balance : " />
                                <div style="margin-top: 5px">
                                    <asp:DropDownList runat="server" ID="ddlBalance" CssClass="form-control" OnSelectedIndexChanged="ddlBalance_SelectedIndexChanged" AutoPostBack="true" />
                                </div>
                                <div class="form-group" style="height: 5px"></div>
                            </div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblAmount" CssClass="control-label" Text="Amount (in IDR) *" />
                                <asp:TextBox runat="server" ID="txtAmount" CssClass="form-control" Font-Size="20pt" TextMode="MultiLine" Rows="1"/>
                            </div><div class="form-group" style="height: 5px"></div>
                            
                            <div class="form-group">
                                <asp:Label runat="server" ID="lblBankAccount" CssClass="control-label" Text="Bank Account *" />
                                <asp:DropDownList runat="server" ID="ddlBankAccount" CssClass="form-control" OnSelectedIndexChanged="ddlBankAccount_SelectedIndexChanged" AutoPostBack="true" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblNotes" CssClass="control-label" Text="Notes *" />
                                <asp:TextBox runat="server" ID="txtNotes" CssClass="form-control"/>
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                    <asp:Label runat="server" ID="lblReceipt" CssClass="control-label" Text="Receipt" />
                                    <asp:FileUpload ID="fuReceipt" runat="server" CssClass="form-control" /> 
                                </div>
                                <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <center><asp:Button runat="server" ID="btnRequest" Text="Request Topup" CssClass="btn btn-success btn-lg" OnClick="btnRequest_Click" Width="100%" /></center>
                            </div>

                            <div class="form-group" style="height: 30px"></div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12"></div>
    </div>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Scripts" runat="server">
</asp:Content>
