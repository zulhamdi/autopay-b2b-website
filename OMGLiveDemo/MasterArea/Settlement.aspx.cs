﻿using OMGLiveDemo.Models;
using OMGLiveDemo.Scripts;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.MasterArea
{
    public partial class Settlement : System.Web.UI.Page
    {
        OMG omg = new OMG();
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    bindMasterArea();
                    BindGV();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        protected void btnAction_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            var refs = ((HiddenField)row.FindControl("hfPayoutID")).Value;

            Session["PayoutID"] = refs;
            Response.Redirect("Payout/ApproveOrReject");
        }

        void BindGV()
        {
            var id = Convert.ToInt64(SessionLib.Current.AdminID);

            checkSort();

            if (SessionLib.Current.Role == Roles.MasterArea)
            {
                divpartner.Visible = false;
                bindBalance();
            }
            else
            {
                id = Convert.ToInt64(ddlPartner.SelectedValue);
            }

            getSettlementReport(id);
        }

        void bindMasterArea()
        {
            divpartner.Visible = true;
            ddlPartner.Visible = true;
            var dt = db.tblM_Master_Area.Where(p => p.isActive == 1).ToList();
            ddlPartner.DataSource = null;
            ddlPartner.DataSource = dt;
            ddlPartner.DataValueField = "MasterAreaID";
            ddlPartner.DataTextField = "Fullname";
            ddlPartner.DataBind();
            bindBalance();
        }

        void bindBalance()
        {
            long id = 0;
            if (SessionLib.Current.Role == Roles.MasterArea)
            {
                id = Int64.Parse(SessionLib.Current.AdminID);
            }
            else
            {
                id = Int64.Parse(ddlPartner.SelectedValue);
            }
            var dt = db.tblM_Master_Area_Balance.Where(p => p.isActive == 1 && p.MasterAreaID == id).ToList();
            ddlBalance.DataSource = null;
            ddlBalance.DataSource = dt;
            ddlBalance.DataValueField = "BalanceID";
            ddlBalance.DataTextField = "Currency";
            ddlBalance.DataBind();
        }

        void getSettlementReport(long id)
        {
            var date = new DateTime();
            var datelast = new DateTime();
            if (divsingledate.Visible)
            {
                if (datepurchased.Text != "")
                {
                    date = Convert.ToDateTime(datepurchased.Text.ToString().Trim());
                    datelast = Convert.ToDateTime(datepurchased.Text.ToString().Trim()).AddHours(23).AddMinutes(59).AddSeconds(59);
                }
            }
            else if (divdaterange.Visible)
            {
                if (startdate.Text != "" && enddate.Text != "")
                {
                    date = Convert.ToDateTime(startdate.Text.ToString().Trim());
                    datelast = Convert.ToDateTime(enddate.Text.ToString().Trim()).AddHours(23).AddMinutes(59).AddSeconds(59);
                }
            }

            var balid = Convert.ToInt64(ddlBalance.SelectedValue);

            List<vw_Master_Area_Settlement> dt = new List<vw_Master_Area_Settlement>();
            if (datepurchased.Text != "" || (startdate.Text != "" && enddate.Text != ""))
                dt = db.vw_Master_Area_Settlement.Where(p => p.MasterAreaID == id && p.BalanceID == balid && p.DateCreated >= date && p.DateCreated <= datelast).OrderBy(p => p.ID).ToList();
            else
                dt = db.vw_Master_Area_Settlement.Where(p => p.MasterAreaID == id && p.BalanceID == balid).OrderBy(p => p.ID).ToList();
            for (int i = 0; i < dt.Count; i++)
            {
                if (dt[i].Type == "Topup" || dt[i].Type == "Refund")
                {
                    dt[i].Status = "completed";
                }

                if (ddlBalance.SelectedItem.Text == "IDR")
                {
                    dt[i].AmountText = ddlBalance.SelectedItem.Text + " " + String.Format(new CultureInfo("id-ID"), "{0:n}", dt[i].Amount).Replace("Rp", "Rp ");
                    dt[i].BalancePreviousText = ddlBalance.SelectedItem.Text + " " + String.Format(new CultureInfo("id-ID"), "{0:n}", dt[i].BalancePrevious).Replace("Rp", "Rp ");
                    dt[i].BalanceText = ddlBalance.SelectedItem.Text + " " + String.Format(new CultureInfo("id-ID"), "{0:n}", dt[i].Balance).Replace("Rp", "Rp ");
                    dt[i].AdminFeeText = ddlBalance.SelectedItem.Text + " " + String.Format(new CultureInfo("id-ID"), "{0:n}", dt[i].AdminFee).Replace("Rp", "Rp ");
                }
                else if (ddlBalance.SelectedItem.Text == "MYR")
                {
                    dt[i].AmountText = ddlBalance.SelectedItem.Text + " " + String.Format(new CultureInfo("ms-MY"), "{0:n}", dt[i].Amount).Replace("RM", "RM ");
                    dt[i].BalancePreviousText = ddlBalance.SelectedItem.Text + " " + String.Format(new CultureInfo("ms-MY"), "{0:n}", dt[i].BalancePrevious).Replace("RM", "RM ");
                    dt[i].BalanceText = ddlBalance.SelectedItem.Text + " " + String.Format(new CultureInfo("ms-MY"), "{0:n}", dt[i].Balance).Replace("RM", "RM ");
                    dt[i].AdminFeeText = ddlBalance.SelectedItem.Text + " " + String.Format(new CultureInfo("ms-MY"), "{0:n}", dt[i].AdminFee).Replace("RM", "RM ");
                }
            }
            gvListItem.DataSource = dt;
            gvListItem.DataBind();

            if (ddlBalance.SelectedItem.Text == "IDR")
            {
                lblTotalTransfer.Text = ddlBalance.SelectedItem.Text + " " + String.Format(new CultureInfo("id-ID"), "{0:n}", dt.Where(p => p.Type == "Payout").Select(p => p.Amount).Sum()).Replace("Rp", "Rp ");
                lblTotalTopup.Text = ddlBalance.SelectedItem.Text + " " + String.Format(new CultureInfo("id-ID"), "{0:n}", dt.Where(p => p.Type == "Topup").Select(p => p.Amount).Sum()).Replace("Rp", "Rp ");
                lblTotalRefund.Text = ddlBalance.SelectedItem.Text + " " + String.Format(new CultureInfo("id-ID"), "{0:n}", dt.Where(p => p.Type == "Refund").Select(p => p.Amount).Sum()).Replace("Rp", "Rp ");
            }
            else if (ddlBalance.SelectedItem.Text == "MYR")
            {
                lblTotalTransfer.Text = ddlBalance.SelectedItem.Text + " " + String.Format(new CultureInfo("ms-MY"), "{0:n}", dt.Where(p => p.Type == "Payout").Select(p => p.Amount).Sum()).Replace("RM", "RM ");
                lblTotalTopup.Text = ddlBalance.SelectedItem.Text + " " + String.Format(new CultureInfo("ms-MY"), "{0:n}", dt.Where(p => p.Type == "Topup").Select(p => p.Amount).Sum()).Replace("RM", "RM ");
                lblTotalRefund.Text = ddlBalance.SelectedItem.Text + " " + String.Format(new CultureInfo("ms-MY"), "{0:n}", dt.Where(p => p.Type == "Refund").Select(p => p.Amount).Sum()).Replace("RM", "RM ");
            }

            checkDownload();
        }

        protected void btnReceipt_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            Session["PayoutID"] = ((HiddenField)row.FindControl("hfPayoutID")).Value;
            Page.ClientScript.RegisterStartupScript(this.GetType(), "OpenWindow", "window.open('/Payout/Receipt','_newtab');", true);
        }

        protected Boolean IsCompleted(string status)
        {
            if (status == "completed")
                return true;
            else
                return false;
        }

        void RedirectToTransferHistory(string status)
        {
            Session["PayoutHistoryStatus"] = status;
            Response.Redirect("Payout/PayoutHistory");
        }
        protected void btnView_Click(object sender, EventArgs e)
        {

        }

        protected void ddlPartner_SelectedIndexChanged(object sender, EventArgs e)
        {
            bindBalance();
            getSettlementReport(getMasterAreaID());
        }

        protected void ddlSortBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            checkSort();
        }

        void checkSort()
        {
            if (ddlSortBy.SelectedValue == "all")
            {
                divsingledate.Visible = false;
                datepurchased.Text = "";
                divdaterange.Visible = false;
                startdate.Text = "";
                enddate.Text = "";
            }
            else if (ddlSortBy.SelectedValue == "date")
            {
                divsingledate.Visible = true;
                divdaterange.Visible = false;
                startdate.Text = "";
                enddate.Text = "";
            }
            else
            {
                divsingledate.Visible = false;
                divdaterange.Visible = true;
            }
        }

        protected void btnSingleDate_Click(object sender, EventArgs e)
        {
            BindGV();
        }

        protected void btnDateRange_Click(object sender, EventArgs e)
        {
            BindGV();
        }

        protected void gvListItem_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvListItem.PageIndex = e.NewPageIndex;
            getSettlementReport(getMasterAreaID());
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            gvListItem.AllowPaging = false;
            BindGV();
            
            Common.ExportExcel(gvListItem, "Settlement Report");

            gvListItem.AllowPaging = true;
            BindGV();
        }

        void checkDownload()
        {
            if (gvListItem.Rows.Count != 0)
                btnDownload.Visible = true;
            else
                btnDownload.Visible = false;
        }

        protected void ddlBalance_SelectedIndexChanged(object sender, EventArgs e)
        {
            getSettlementReport(getMasterAreaID());
        }

        long getMasterAreaID() {
            if (SessionLib.Current.Role == Roles.MasterArea)
                return Convert.ToInt64(SessionLib.Current.AdminID);
            else
                return Convert.ToInt64(ddlPartner.SelectedValue);
        }
    }
}