﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UpdateMasterArea.aspx.cs" Inherits="OMGLiveDemo.MasterArea.UpdateMasterArea" MaintainScrollPositionOnPostback="true" %>

<%@ MasterType VirtualPath="~/Site.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" />

    <div class="row">
        <div class="row x_title">
            <div class="col-md-12">
                <h3>Update Master Area</h3>
            </div>
        </div>

        <div class="col-md-3 col-sm-12 col-xs-12"></div>
        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="dashboard_graph">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div style="width: 100%;">
                        <div id="canvas_dahs" class="demo-placeholder" style="width: 100%; height: 100%;">
                            <div class="form-group" style="height: 30px"></div>

                            <asp:HiddenField ID="hfAgentID" runat="server" />
                            <asp:HiddenField ID="hfRateID" runat="server" />
                            <asp:HiddenField ID="hfRemittanceID" runat="server" />
                            <asp:HiddenField ID="hfFee" runat="server" />

                            <div class="alert alert-danger alert-dismissible fade in" runat="server" id="diverror" visible="false">
                                <button type="button" runat="server" id="btnError" onserverclick="btnError_ServerClick" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <center><asp:Label runat="server" ID="lblError" /></center>
                            </div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblName" CssClass="control-label" Text="Agent Full Name *" />
                                <asp:TextBox runat="server" ID="txtName" CssClass="form-control" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblPhone" CssClass="control-label" Text="Phone *" />
                                <asp:TextBox runat="server" ID="txtPhone" CssClass="form-control" Enabled="false" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="form-group">
                                <asp:Label runat="server" ID="lblEmail" CssClass="control-label" Text="Email " />
                                <asp:TextBox runat="server" ID="txtEmail" CssClass="form-control" />
                            </div>
                            <div class="form-group" style="height: 5px"></div>

                            <div class="col-md-12 col-sm-12 col-xs-12" style="background-color: #f8f8f8; margin-bottom: 20px; padding-top: 5px">
                                <h4><asp:Label runat="server" ID="lblRemittanceName" CssClass="control-label" Text="" /></h4>

                                <div class="form-group" runat="server" id="divmyrtoidr">
                                    <div class="form-group">
                                        <asp:Label runat="server" ID="lblRate" CssClass="control-label" Text="Rate Level *" />
                                        <asp:DropDownList runat="server" ID="ddlRate" CssClass="form-control" OnSelectedIndexChanged="ddlRate_SelectedIndexChanged" AutoPostBack="true" />
                                    <div class="form-group" style="height: 5px"></div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label runat="server" ID="lblFee" CssClass="control-label" Text="Admin Fee (in IDR) *" />
                                        <asp:TextBox runat="server" ID="txtFee" CssClass="form-control" Font-Size="20pt" TextMode="MultiLine" Rows="1" />
                                    <div class="form-group" style="height: 5px"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <center><asp:Button runat="server" ID="btnAdd" Text="Update Master Area" CssClass="btn btn-success btn-lg" OnClick="btnAdd_Click" Width="100%" /></center>
                            </div>

                            <div class="form-group" style="height: 30px"></div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12"></div>
    </div>
    <br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Scripts" runat="server">
</asp:Content>
