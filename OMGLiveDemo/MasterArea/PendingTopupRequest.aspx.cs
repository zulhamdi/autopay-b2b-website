﻿using OMGLiveDemo.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OMGLiveDemo.MasterArea
{
    public partial class PendingTopupRequest : System.Web.UI.Page
    {
        OMGLiveDemoEntities db = new OMGLiveDemoEntities();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Common.allowedPage(this.GetType().BaseType.Name))
            {
                if (!IsPostBack)
                {
                    BindGV();
                }
            }
            else
            {
                Master.checkRole();
            }
        }

        void BindGV()
        {
            var id = Convert.ToInt64(SessionLib.Current.AdminID);
            if (Session["SuccessTopup"] != null)
            {
                lblAlertSuccess.Text = Session["SuccessTopup"].ToString();
                divsuccess.Visible = true;
                Session["SuccessTopup"] = null;
            }

            if (Session["SuccessApproveTopup"] != null)
            {
                lblAlertSuccess.Text = Session["SuccessApproveTopup"].ToString();
                divsuccess.Visible = true;
                Session["SuccessApproveTopup"] = null;
            }

            if (Session["ErrorTopup"] != null)
            {
                lblAlertFailed.Text = Session["ErrorTopup"].ToString();
                divfailed.Visible = true;
                Session["ErrorTopup"] = null;
            }

            if (Session["FailedApproveTopup"] != null)
            {
                lblAlertFailed.Text = Session["FailedApproveTopup"].ToString();
                divfailed.Visible = true;
                Session["FailedApproveTopup"] = null;
            }

            var dt = db.vw_Topup.Where(p => p.UserType == "MA" & p.Status == "created").ToList();
            for (int i = 0; i < dt.Count; i++)
            {
                dt[i].AmountText = "IDR " + String.Format(new CultureInfo("id-ID"), "{0:n}", Convert.ToDouble(dt[i].Amount)).Replace("Rp", "Rp ");
            }
            gvListItem.DataSource = dt;
            gvListItem.DataBind();
        }

        protected void lblEdit_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            Session["AgentID"] = ((HiddenField)row.FindControl("hfAgentID")).Value;
            Session["RateLevel"] = row.Cells[3].Text;
            Response.Redirect("UpdateAgent");
        }

        protected void btnAlertSuccess_ServerClick(object sender, EventArgs e)
        {
            divsuccess.Visible = false;
        }

        protected void btnAlertFailed_ServerClick(object sender, EventArgs e)
        {
            divfailed.Visible = false;
        }

        protected void btnAction_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            GridViewRow row = (GridViewRow)btn.NamingContainer;
            var refs = ((HiddenField)row.FindControl("hfTopupID")).Value;

            Session["TopupID"] = refs;
            Response.Redirect("ApproveTopupRequest");
        }
    }
}