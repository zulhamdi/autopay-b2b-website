﻿using Aryadana.API.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aryadana.API.Test
{
    class Program
    {
        static AryadanaServices obj = new AryadanaServices();
        static void Main(string[] args)
        {
            //CheckRates();
            //GetDataMaster();
            GetTransactionStatus();
            System.Console.ReadKey();
        }

        static async Task CallSignIn()
        {

            string token = await obj.SigIn();
            System.Console.WriteLine(token);
        }

        static async Task CheckRates()
        {
            var request = new CheckRatesRequest()
            {
                TypeOfTransaction = "CTA",
                OriginCurrency = "IDR",
                DestinationCurrency = "MYR"
            };
            CheckRatesResponse output = await obj.CheckRates(request);
            System.Console.WriteLine("Currency : {0}", output.Currency);
            System.Console.WriteLine("ForeignExchange : {0}", output.ForeignExchange);

        }

        static async Task CheckListBank()
        {
            var request = new CheckListRequest()
            {
                CheckList = "BANK",
                Country = "MYS"
            };
            CheckListBankResponse output = await obj.CheckListBank(request);
            System.Console.WriteLine("Country : {0}", output.Country);
            System.Console.WriteLine("Bank Code : {0}", output.ListOfBank[0].BankCode);
            System.Console.WriteLine("Bank Name : {0}", output.ListOfBank[0].BankName);

        }


        static async Task CheckListState()
        {
            var request = new CheckListRequest()
            {
                CheckList = "STATES",
                Country = "MYS"
            };
            CheckListStateResponse output = await obj.CheckListState(request);
            System.Console.WriteLine("Country : {0}", output.Country);
            System.Console.WriteLine("States Code : {0}", output.ListOfStates[0].StatesCode);
            System.Console.WriteLine("States Name : {0}", output.ListOfStates[0].StatesName);

        }

        static async Task CheckListPurposeOfTransaction()
        {
            var request = new CheckListRequest()
            {
                CheckList = "PURPOSE",
                Country = ""
            };
            PurposeOfTransactionResponse output = await obj.CheckListPurposeOfTransaction(request);
            System.Console.WriteLine("Response : {0}", output.Response);
            System.Console.WriteLine("PurposeOfTransaction : {0}", output.ListOfPurposeOfTransaction[0].PurposeOfTransaction);
            

        }


        static async Task CheckStatus()
        {
            string referenceNumber = "936827770";

            CheckStatusResponse output = await obj.CheckStatus(referenceNumber);
            System.Console.WriteLine("Reference Number : {0}", output.ReferenceNumber);
            System.Console.WriteLine("Transaction Date : {0}", output.TransactionDate);
            System.Console.WriteLine("UpdatedStatus Date : {0}", output.UpdatedStatusDate);
            System.Console.WriteLine("Status : {0}", output.Status);
        }

        static async Task SendMoney()
        {
            var request = new SendMoneyRequest()
            {
                TypeOfTransaction = "CTA",
                PaymentMode = "STL",
                SenderType = "INDIVIDUAL",
                SenderIDType = "GOVERNMENT_ID",
                SenderIDNumber = "1311333355557777",
                SenderIDIssuedDate = "2001-10-18",
                SenderIDExpiredDate = "2020-10-20",
                SenderIDIssuedCountry = "IDN",
                SenderName = "SENDER ZUL",
                SenderGender = "MALE",
                SenderPlaceOfBirth = "BOGOR",
                SenderDateOfBirth = "1990-01-29",
                SenderAddress = "JALAN BANDA NO 40",
                SenderCity = "BANDUNG",
                SenderProvState = "32",
                SenderZipCode = "40121",
                SenderCountry = "IDN",
                SenderNationality = "IDN",
                SenderOccupation = "PRIVATE EMPLOYEE",
                SenderPhoneNo = "6285294623111",
                RecipientType = "INDIVIDUAL",
                RecipientIDType = "GOVERNMENT_ID",
                RecipientIDNumber = "111222333",
                RecipientName = "RECIPIENT HAMDI",
                RecipientAddress = "JALAN JOHOR NO 40",
                RecipientCity = "JOHOR",
                RecipientPhoneNo = "608219489221",
                BeneficiaryBankCode = "5097",
                BeneficiaryBankName = "BANK ISLAM MALAYSIA BHD",
                BeneficiaryAccNumber = "8602360512",
                AgentReferenceNumber = "132233445001",
                DestinationCountry = "MYS",
                DestinationCurrency = "MYR",
                DestinationAmount = 9000.00,
                TransactionNote = "TEST AUTOPAY",
                PurposeOfTransaction = "INTEGRATION SUPPORT",
                SourceOfFunds = "SAVINGS",
                ReceiverRelationship = "FAMILY"
            };

            SendMoneyResponse output = await obj.SendMoney(request);
            System.Console.WriteLine("Status : {0}", output.Status);
            System.Console.WriteLine("AgentReferenceNumber : {0}", output.AgentReferenceNumber);
            System.Console.WriteLine("ReferenceNumber : {0}", output.ReferenceNumber);
            System.Console.WriteLine("DestinationCurrency : {0}", output.DestinationCurrency);
            System.Console.WriteLine("DestinationAmount : {0}", output.DestinationAmount);
            System.Console.WriteLine("OriginCurrency : {0}", output.OriginCurrency);
            System.Console.WriteLine("ForeignExchange : {0}", output.ForeignExchange);
            System.Console.WriteLine("OriginAmount : {0}", output.OriginAmount);
            System.Console.WriteLine("OriginCost : {0}", output.OriginCost);
            System.Console.WriteLine("OriginTotal : {0}", output.OriginTotal);


        }

        static void GetDataMaster()
        {
            DataMasterModel output = obj.GetDataMaster();
            System.Console.WriteLine("PaymentMode : {0}", output.PaymentMode[0].Code);
            System.Console.WriteLine("Description : {0}", output.PaymentMode[0].Description);
        }
        static void GetTransactionStatus()
        {
            List<TransactionStatus> output = obj.GetTransactionStatus();
            System.Console.WriteLine("Status : {0}", output[0].Status);
            System.Console.WriteLine("Description : {0}", output[0].Description);
        }
        
    }
}
