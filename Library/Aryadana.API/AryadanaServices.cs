﻿using Aryadana.API.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Aryadana.API
{
    public class AryadanaServices
    {
        private string stagingUrl = "http://202.157.177.206:8080";
        private string username = "APIMMIUSERTEST";
        private string password = "fmxdfrKRWBN2Ybj";

        public string EndPoint { get; set; }
        private string Username { get; set; }
        private string Password { get; set; }
        private string Token { get; set; }

        public AryadanaServices()
        {
            this.EndPoint = stagingUrl;
            this.Username = username;
            this.Password = password;
        }
        public AryadanaServices(string _endpoint, string _username, string _password)
        {
            this.EndPoint = _endpoint;
            this.Username = _username;
            this.Password = _password;
        }

        public async Task<string> SigIn()
        {

            object input = new
            {
                Username = this.Username,
                Password = this.Password
            };
            using (HttpClient httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri(this.EndPoint);
                httpClient.DefaultRequestHeaders.Accept.Clear();
                HttpContent httpContent = new StringContent(JsonConvert.SerializeObject(input), Encoding.UTF8);
                httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                string ingestPath = "/session/sign-in";
                var response = await httpClient.PostAsync(ingestPath, httpContent);
                if (response.IsSuccessStatusCode)
                {
                    string jsonResponse = await response.Content.ReadAsStringAsync();
                    JObject accessToken = JsonConvert.DeserializeObject<JObject>(jsonResponse);
                    string token = accessToken["Token"].ToString();
                    this.Token = token;
                    return token;
                }

                return string.Empty;
            }
        }
        
        
        public async Task<CheckRatesResponse> CheckRates(CheckRatesRequest input)
        {
            CheckRatesResponse returnData = new CheckRatesResponse();
            if (String.IsNullOrEmpty(this.Token))
            {
                this.Token = await this.SigIn();
            }
            using (HttpClient httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri(this.EndPoint);
                httpClient.DefaultRequestHeaders.Accept.Clear();
                string bearer_token = String.Format("Bearer {0}",this.Token);
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", this.Token);
                HttpContent httpContent = new StringContent(JsonConvert.SerializeObject(input), Encoding.UTF8);
                httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                string ingestPath = "/check-rates";
                var response = await httpClient.PostAsync(ingestPath, httpContent);
                if (response.IsSuccessStatusCode)
                {
                    string jsonResponse = await response.Content.ReadAsStringAsync();
                    if (jsonResponse.Contains("000200"))
                    {
                        returnData = JsonConvert.DeserializeObject<CheckRatesResponse>(jsonResponse);
                    }

                    return returnData;
                }
                else
                {
                    returnData.Currency = "IDR";
                    returnData.ForeignExchange = 0;
                    returnData.Response = response.StatusCode.ToString();
                }

                return returnData;
            }
        }

        public async Task<CheckListBankResponse> CheckListBank(CheckListRequest input)
        {
            CheckListBankResponse returnData = new CheckListBankResponse();
            if (String.IsNullOrEmpty(this.Token))
            {
                this.Token = await this.SigIn();
            }
            using (HttpClient httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri(this.EndPoint);
                httpClient.DefaultRequestHeaders.Accept.Clear();
                string bearer_token = String.Format("Bearer {0}", this.Token);
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", this.Token);
                HttpContent httpContent = new StringContent(JsonConvert.SerializeObject(input), Encoding.UTF8);
                httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                string ingestPath = "/check-list";
                var response = await httpClient.PostAsync(ingestPath, httpContent);
                if (response.IsSuccessStatusCode)
                {
                    string jsonResponse = await response.Content.ReadAsStringAsync();
                    if (jsonResponse.Contains("000200"))
                    {
                        returnData = JsonConvert.DeserializeObject<CheckListBankResponse>(jsonResponse);
                    }

                    return returnData;
                }

                return returnData;
            }
        }

        public async Task<CheckListStateResponse> CheckListState(CheckListRequest input)
        {
            CheckListStateResponse returnData = new CheckListStateResponse();
            if (String.IsNullOrEmpty(this.Token))
            {
                this.Token = await this.SigIn();
            }
            using (HttpClient httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri(this.EndPoint);
                httpClient.DefaultRequestHeaders.Accept.Clear();
                string bearer_token = String.Format("Bearer {0}", this.Token);
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", this.Token);
                HttpContent httpContent = new StringContent(JsonConvert.SerializeObject(input), Encoding.UTF8);
                httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                string ingestPath = "/check-list";
                var response = await httpClient.PostAsync(ingestPath, httpContent);
                if (response.IsSuccessStatusCode)
                {
                    string jsonResponse = await response.Content.ReadAsStringAsync();
                    if (jsonResponse.Contains("000200"))
                    {
                        returnData = JsonConvert.DeserializeObject<CheckListStateResponse>(jsonResponse);
                    }

                    return returnData;
                }

                return returnData;
            }
        }


        public async Task<PurposeOfTransactionResponse> CheckListPurposeOfTransaction(CheckListRequest input)
        {
            PurposeOfTransactionResponse returnData = new PurposeOfTransactionResponse();
            if (String.IsNullOrEmpty(this.Token))
            {
                this.Token = await this.SigIn();
            }
            using (HttpClient httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri(this.EndPoint);
                httpClient.DefaultRequestHeaders.Accept.Clear();
                string bearer_token = String.Format("Bearer {0}", this.Token);
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", this.Token);
                HttpContent httpContent = new StringContent(JsonConvert.SerializeObject(input), Encoding.UTF8);
                httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                string ingestPath = "/check-list";
                var response = await httpClient.PostAsync(ingestPath, httpContent);
                if (response.IsSuccessStatusCode)
                {
                    string jsonResponse = await response.Content.ReadAsStringAsync();
                    if (jsonResponse.Contains("000200"))
                    {
                        returnData = JsonConvert.DeserializeObject<PurposeOfTransactionResponse>(jsonResponse);
                    }

                    return returnData;
                }

                return returnData;
            }
        }

        public async Task<CheckStatusResponse> CheckStatus(string referenceNumber)
        {
            CheckStatusResponse returnData = new CheckStatusResponse();
            if (String.IsNullOrEmpty(this.Token))
            {
                this.Token = await this.SigIn();
            }
            using (HttpClient httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri(this.EndPoint);
                httpClient.DefaultRequestHeaders.Accept.Clear();
               
                string ingestPath = String.Format("/send-money/check-status/{0}", referenceNumber);
                var response = await httpClient.GetAsync(ingestPath);
                if (response.IsSuccessStatusCode)
                {
                    string jsonResponse = await response.Content.ReadAsStringAsync();
                    if (jsonResponse.Contains("000200"))
                    {
                        returnData = JsonConvert.DeserializeObject<CheckStatusResponse>(jsonResponse);
                    }

                    return returnData;
                }

                return returnData;
            }
        }


        public async Task<SendMoneyResponse> SendMoney(SendMoneyRequest input)
        {
            SendMoneyResponse returnData = new SendMoneyResponse();
            if (String.IsNullOrEmpty(this.Token))
            {
                this.Token = await this.SigIn();
            }
            using (HttpClient httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri(this.EndPoint);
                httpClient.DefaultRequestHeaders.Accept.Clear();
                string bearer_token = String.Format("Bearer {0}", this.Token);
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", this.Token);
                HttpContent httpContent = new StringContent(JsonConvert.SerializeObject(input), Encoding.UTF8);
                httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                string ingestPath = "/send-money";
                var response = await httpClient.PostAsync(ingestPath, httpContent);
                if (response.IsSuccessStatusCode)
                {
                    string jsonResponse = await response.Content.ReadAsStringAsync();
                    if (jsonResponse.Contains("000200"))
                    {
                        returnData = JsonConvert.DeserializeObject<SendMoneyResponse>(jsonResponse);
                    }

                    return returnData;
                }

                return returnData;
            }
        }

        public DataMasterModel GetDataMaster()
        {
            var assembly = Assembly.GetExecutingAssembly();
            var resourceName = "Aryadana.API.data-master.json";
            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            using (StreamReader reader = new StreamReader(stream))
            {
                string json = reader.ReadToEnd();
                var items = JsonConvert.DeserializeObject<DataMasterModel>(json);
                return items;
            }
          
        }

        public List<TransactionStatus> GetTransactionStatus()
        {
            var assembly = Assembly.GetExecutingAssembly();
            var resourceName = "Aryadana.API.transaction-status.json";
            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            using (StreamReader reader = new StreamReader(stream))
            {
                string json = reader.ReadToEnd();
                var items = JsonConvert.DeserializeObject<List<TransactionStatus>>(json);
                return items;
            }
           
        }

    }
}
