﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aryadana.API.Model
{
    class SendMoneyModel
    {
    }

    public class CheckStatusResponse
    {
        public string Response { get; set; }
        public string ReferenceNumber { get; set; }
        public string TransactionDate { get; set; }
        public string UpdatedStatusDate { get; set; }
        public string Status { get; set; }
    }

    public class SendMoneyRequest
    {
        public string TypeOfTransaction { get; set; }
        public string PaymentMode { get; set; }
        public string SenderType { get; set; }
        public string SenderIDType { get; set; }
        public string SenderIDNumber { get; set; }
        public string SenderIDIssuedDate { get; set; }
        public string SenderIDExpiredDate { get; set; }
        public string SenderIDIssuedCountry { get; set; }
        public string SenderName { get; set; }
        public string SenderGender { get; set; }
        public string SenderPlaceOfBirth { get; set; }
        public string SenderDateOfBirth { get; set; }
        public string SenderAddress { get; set; }
        public string SenderCity { get; set; }
        public string SenderProvState { get; set; }
        public string SenderZipCode { get; set; }
        public string SenderCountry { get; set; }
        public string SenderNationality { get; set; }
        public string SenderOccupation { get; set; }
        public string SenderPhoneNo { get; set; }
        public string RecipientType { get; set; }
        public string RecipientIDType { get; set; }
        public string RecipientIDNumber { get; set; }
        public string RecipientName { get; set; }
        public string RecipientAddress { get; set; }
        public string RecipientCity { get; set; }
        public string RecipientPhoneNo { get; set; }
        public string BeneficiaryBankCode { get; set; }
        public string BeneficiaryBankName { get; set; }
        public string BeneficiaryAccNumber { get; set; }
        public string AgentReferenceNumber { get; set; }
        public string DestinationCountry { get; set; }
        public string DestinationCurrency { get; set; }
        public double DestinationAmount { get; set; }
        public string TransactionNote { get; set; }
        public string PurposeOfTransaction { get; set; }
        public string SourceOfFunds { get; set; }
        public string ReceiverRelationship { get; set; }
    }


    public class SendMoneyResponse
    {
        public string Response { get; set; }
        public object TransactionDate { get; set; }
        public string Status { get; set; }
        public string AgentReferenceNumber { get; set; }
        public string ReferenceNumber { get; set; }
        public string DestinationCurrency { get; set; }
        public int DestinationAmount { get; set; }
        public string OriginCurrency { get; set; }
        public int ForeignExchange { get; set; }
        public int OriginAmount { get; set; }
        public int OriginCost { get; set; }
        public int OriginTotal { get; set; }
    }
}
