﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aryadana.API.Model
{
    class CheckListModel
    {
    }
    public class CheckListRequest
    {
        public string CheckList { get; set; }
        public string Country { get; set; }
    }

    public class ListOfBank
    {
        public string BankCode { get; set; }
        public string BankName { get; set; }
    }

    public class CheckListBankResponse
    {
        public string Response { get; set; }
        public string Country { get; set; }
        public List<ListOfBank> ListOfBank { get; set; }
    }


    public class ListOfState
    {
        public string StatesCode { get; set; }
        public string StatesName { get; set; }
    }

    public class CheckListStateResponse
    {
        public string Response { get; set; }
        public string Country { get; set; }
        public List<ListOfState> ListOfStates { get; set; }
    }



    public class ListOfPurposeOfTransaction
    {
        public string PurposeOfTransaction { get; set; }
    }

    public class PurposeOfTransactionResponse
    {
        public string Response { get; set; }
        public List<ListOfPurposeOfTransaction> ListOfPurposeOfTransaction { get; set; }
    }


}
