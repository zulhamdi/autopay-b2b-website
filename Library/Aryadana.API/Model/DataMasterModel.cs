﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aryadana.API.Model
{
    
    public class TypeOfTransaction
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }

    public class PaymentMode
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }

    public class SenderType
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }

    public class SenderIDType
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }

    public class DataMasterModel
    {
        public List<TypeOfTransaction> TypeOfTransaction { get; set; }
        public List<PaymentMode> PaymentMode { get; set; }
        public List<SenderType> SenderType { get; set; }
        public List<SenderIDType> SenderIDType { get; set; }
    }

    public class TransactionStatus
    {
        public string Status { get; set; }
        public string Description { get; set; }
    }
}
