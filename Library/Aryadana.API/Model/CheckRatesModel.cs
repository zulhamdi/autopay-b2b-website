﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aryadana.API.Model
{
    class CheckRatesModel
    {
    }

    public class CheckRatesRequest
    {
        public string TypeOfTransaction { get; set; }
        public string OriginCurrency { get; set; }
        public string DestinationCurrency { get; set; }
    }

    public class CheckRatesResponse
    {
        public string Response { get; set; }
        public string Currency { get; set; }
        public int ForeignExchange { get; set; }
    }
}
