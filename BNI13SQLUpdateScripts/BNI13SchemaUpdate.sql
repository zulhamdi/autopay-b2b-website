/****** Script for BNI 13 Requirement Schema Changes  ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

UPDATE [dbo].[tblM_Admin_Role]
SET Role='Operator'
WHERE Role='Creator';
GO

INSERT INTO [dbo].[tblM_Admin_Role]
(Role,isActive,CreatedBy)
VALUES ('Checker',1,1);
GO

ALTER TABLE [dbo].[tblM_Beneficiary]
ADD
	[Status] NVARCHAR (50) NULL,
	[ApprovedBy] [bigint] NULL,
	[ApprovedDate] [datetime] NULL;
GO

ALTER TABLE [dbo].[tblM_Admin]
ADD
    [UpdatedDate] [datetime] NULL,
    [UpdatedBy] [bigint];
GO

ALTER TABLE [dbo].[tblM_Admin] ADD  CONSTRAINT [DF_tblM_Admin_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO

CREATE TABLE [dbo].[tblM_Address_Status](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Status] [nvarchar](50) NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_tblM_Address_Status] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[tblM_Address_Status] ADD  CONSTRAINT [DF_tblM_Address_Status_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[tblM_Address_Status] ADD  CONSTRAINT [DF_tblM_Address_Status_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO

CREATE TABLE [dbo].[tblM_Bentuk_Badan_Hukum](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Type] [nvarchar](50) NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_tblM_Bentuk_Badan_Hukum] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[tblM_Bentuk_Badan_Hukum] ADD  CONSTRAINT [DF_tblM_Bentuk_Badan_Hukum_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[tblM_Bentuk_Badan_Hukum] ADD  CONSTRAINT [DF_tblM_Bentuk_Badan_Hukum_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO

CREATE TABLE [dbo].[tblM_Business_Field](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Type] [nvarchar](50) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
 CONSTRAINT [PK_tblM_Business_Field] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[tblM_Business_Field] ADD  CONSTRAINT [DF_tblM_Business_Field_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

CREATE TABLE [dbo].[tblM_Company_Type](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Type] [nvarchar](50) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
 CONSTRAINT [PK_tblM_Company_Type] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[tblM_Company_Type] ADD  CONSTRAINT [DF_tblM_Company_Type_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

CREATE TABLE [dbo].[tblM_Company_Director](
	[IDType] [bigint] NOT NULL,
	[IDNo] [nvarchar](50) NOT NULL,
	[IDExpiryDate] [datetime] NOT NULL,
	[IDURL] [nvarchar](max) NULL,
	[Name] [nvarchar](50) NOT NULL,
	[NPWP] [nvarchar](50) NULL,
	[Sex] [bigint] NULL,
	[BirthPlace] [nvarchar](50) NULL,
	[BOD] [datetime] NULL,
	[Nationality] [bigint] NULL,
	[Religion] [bigint] NULL,
	[MaritalStatus] [bigint] NULL,
	[EducationLevel] [bigint] NULL,
	[AddressStatus] [bigint] NULL,
	[Address] [nvarchar](max) NULL,
	[City] [nvarchar](50) NULL,
	[Province] [bigint] NULL,
	[Postcode] [nvarchar](50) NULL,
	[CountryM49] [bigint] NULL,
	[Phone] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_tblM_Company_Director] PRIMARY KEY CLUSTERED 
(
	[IDType] ASC,
	[IDNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[tblM_Company_Director] ADD  CONSTRAINT [DF_tblM_Company_Director_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[tblM_Company_Director] ADD  CONSTRAINT [DF_tblM_Company_Director_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO

CREATE TABLE [dbo].[tblM_Company_Share_Holder](
	[IDType] [bigint] NOT NULL,
	[IDNo] [nvarchar](50) NOT NULL,
	[IDExpiryDate] [datetime] NOT NULL,
	[IDURL] [nvarchar](max) NULL,
	[HoldingType] [bigint] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[NPWP] [nvarchar](50) NULL,
	[Sex] [bigint] NULL,
	[BirthPlace] [nvarchar](50) NULL,
	[BOD] [datetime] NULL,
	[Nationality] [bigint] NULL,
	[Religion] [bigint] NULL,
	[MaritalStatus] [bigint] NULL,
	[EducationLevel] [bigint] NULL,
	[AddressStatus] [bigint] NULL,
	[Address] [nvarchar](max) NULL,
	[City] [nvarchar](50) NULL,
	[Province] [bigint] NULL,
	[Postcode] [nvarchar](50) NULL,
	[CountryM49] [bigint] NULL,
	[Phone] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_tblM_Company_Share_Holder] PRIMARY KEY CLUSTERED 
(
	[IDType] ASC,
	[IDNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[tblM_Company_Share_Holder] ADD  CONSTRAINT [DF_tblM_Company_Share_Holder_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[tblM_Company_Share_Holder] ADD  CONSTRAINT [DF_tblM_Company_Share_Holder_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO

CREATE TABLE [dbo].[tblM_Company_Type](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Type] [nvarchar](50) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
 CONSTRAINT [PK_tblM_Company_Type] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[tblM_Company_Type] ADD  CONSTRAINT [DF_tblM_Company_Type_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

CREATE TABLE [dbo].[tblM_Country](
	[M49] [bigint] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[ISO3166_A2] [nvarchar](2) NOT NULL,
	[ISO3166_A3] [nvarchar](3) NOT NULL,
 CONSTRAINT [PK_tblM_Country] PRIMARY KEY CLUSTERED 
(
	[M49] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[tblM_Education_Level](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_tblM_Education_Level] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[tblM_Education_Level] ADD  CONSTRAINT [DF_tblM_Education_Level_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[tblM_Education_Level] ADD  CONSTRAINT [DF_tblM_Education_Level_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO

CREATE TABLE [dbo].[tblM_Marital_Status](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Status] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_tblM_Marital_Status] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[tblM_Marital_Status] ADD  CONSTRAINT [DF_tblM_Marital_Status_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[tblM_Marital_Status] ADD  CONSTRAINT [DF_tblM_Marital_Status_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO

CREATE TABLE [dbo].[tblM_Person_Identity_Type](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[CountryID] [bigint] NOT NULL,
	[IdentityType] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_tblM_Person_Identity_Type] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[tblM_Person_Identity_Type] ADD  CONSTRAINT [DF_tblM_Person_Identity_Type_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[tblM_Person_Identity_Type] ADD  CONSTRAINT [DF_tblM_Person_Identity_Type_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO

CREATE TABLE [dbo].[tblM_Province](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[CountryM49] [bigint] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_tblM_Province] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[tblM_Province] ADD  CONSTRAINT [DF_tblM_Indonesia_Provinces_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[tblM_Province] ADD  CONSTRAINT [DF_tblM_Indonesia_Provinces_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO

CREATE TABLE [dbo].[tblM_Religion](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_tblM_Religion] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[tblM_Religion] ADD  CONSTRAINT [DF_tblM_Religion_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[tblM_Religion] ADD  CONSTRAINT [DF_tblM_Religion_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO

CREATE TABLE [dbo].[tblM_Sender_Bank_Account](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[SenderID] [bigint] NULL,
	[BankID] [bigint] NOT NULL,
	[AccountNumber] [nvarchar](50) NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_tblM_Sender_Bank_Account] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[tblM_Sender_Bank_Account] ADD  CONSTRAINT [DF_tblM_Sender_Bank_Account_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[tblM_Sender_Bank_Account] ADD  CONSTRAINT [DF_tblM_Sender_Bank_Account_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO

CREATE TABLE [dbo].[tblM_Sex](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Type] [nvarchar](50) NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_tblM_Sex] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[tblM_Sex] ADD  CONSTRAINT [DF_tblM_Sex_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[tblM_Sex] ADD  CONSTRAINT [DF_tblM_Sex_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO

CREATE TABLE [dbo].[tblM_Share_Holding_Type](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[HoldingType] [nvarchar](50) NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [bigint] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [bigint] NULL,
 CONSTRAINT [PK_tblM_Share_Holding_Type] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[tblM_Share_Holding_Type] ADD  CONSTRAINT [DF_tblM_Share_Holding_Type_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[tblM_Share_Holding_Type] ADD  CONSTRAINT [DF_tblM_Share_Holding_Type_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO

CREATE TABLE [dbo].[tblS_Log](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[EventDate] [datetime] NOT NULL,
	[EventType] [nvarchar](50) NOT NULL,
	[EventBy] [bigint] NOT NULL,
	[EventData] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_tblS_Log] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[tblS_Log] ADD  CONSTRAINT [DF_tblS_Log_EventDate]  DEFAULT (getdate()) FOR [EventDate]
GO



/********************************
 * Views
 *******************************/

CREATE VIEW [dbo].[vw_Payout_IDR]
AS
SELECT        PayoutID, RemittanceID, Currency, Amount, AdminFee, UniqueCode, Rate, RateGiven, TotalAmount, TotalTransfer, PurposeID, Purpose, Notes, SenderID, SenderTypeID, SenderCityID, SenderType, SenderName, SenderPhone, 
                         SenderEmail, SenderAddress, SenderFileID, SenderFileIDURL, ResponsiblePerson, ResponsiblePersonID, ResponsiblePersonIDURL, SenderCity, SenderCodeBI, SenderCodePPATK, BeneficiaryID, BeneficiaryCityID, 
                         BeneficiaryName, BeneficiaryAccount, BeneficiaryBank, BeneficiaryAddress, BankName, BeneficiaryEmail, BeneficiaryPhone, BeneficiaryFileID, BeneficiaryFIleIDURL, BeneficiaryCity, BeneficiaryCodeBI, 
                         BeneficiaryCodePPATK, Status, StatusMessage, isActive, CreatedBy, CreatedDate, isAgent, ApprovedBy, ApprovedDate, ApprovedName, RejectedBy, RejectedDate, RejectedName, ModifiedDate, ReceiptURL, Reference, 
                         BankAccountID, BankID, TransferToBankName, BankCode, Account, AccountHolder, CASE WHEN RemittanceID = 1 THEN TotalTransfer ELSE CAST(Rate AS FLOAT) * TotalTransfer END AS IDRTotalTransfer
FROM            dbo.vw_Payout
GO

CREATE VIEW [dbo].[vw_Log_ByAdmin]
AS
SELECT        dbo.tblS_Log.ID, dbo.tblS_Log.EventDate, dbo.tblS_Log.EventType, dbo.tblS_Log.EventBy, dbo.tblM_Admin.Fullname AS AdminName, dbo.tblS_Log.EventData
FROM            dbo.tblS_Log INNER JOIN
                         dbo.tblM_Admin ON dbo.tblS_Log.EventBy = dbo.tblM_Admin.AdminID
GO